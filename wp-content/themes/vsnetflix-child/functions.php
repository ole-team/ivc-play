<?php 
function ivcplay_scripts_styles() {
	global $wp_styles;
	wp_enqueue_style( 'ivclive-style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'ivcplay_scripts_styles' );

add_shortcode('videoDLM', function($atts){
    $html = '';
    $idDLM = $atts['id'];
    $typeDLM = $atts['type'];
    $ttDLM = $atts['title'];
    if(!empty($idDLM)){
        $html .= '<div class="video_wrapper_responsive"><div id="videofull-DLM" data-id="'.$idDLM.'"></div></div>';
    wp_enqueue_script('DM-api', 'https://api.dmcdn.net/all.js',1);
    wp_enqueue_script('custom-video-dm', get_stylesheet_directory_uri() . '/js/custom-video-dm.js',array('jquery'),'1.0',true);
    }  
    return $html;
});
?>