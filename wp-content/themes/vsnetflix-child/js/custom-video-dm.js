jQuery(document).ready( function($) {
	if( vsdetails.videotype == "shortcode" ) {
		var itemvideod = $('#videofull-DLM');
        var idDM = itemvideod.attr('data-id');
        if(idDM){
            var playerContent = DM.player(document.getElementById('videofull-DLM'), {
                video: idDM, 
                width: "100%", 
                height: "100%", 
                params: { 
                    autoplay: false, 
                    mute: false, 
                    "queue-enable": false, 
                    "queue-autoplay-next": false, 
                    "sharing-enable":false,
                "ads_params":"category%3Ddeportes" 
                } 
            }); 
            jQuery('#vs-play-video').click(function() {
				playerContent.play();
			});
        }
	}
});