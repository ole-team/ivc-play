<?php
function wpvs_filter_update_checks($queryArgs) {
    $wpvs_themes = get_option('rvs-theme-access');
    if( ! empty($wpvs_themes) && in_array('vs-netflix', $wpvs_themes)) {
        $queryArgs['has_rvs_access'] = true;
        $queryArgs['site'] = home_url();
    }
    return $queryArgs;
}
