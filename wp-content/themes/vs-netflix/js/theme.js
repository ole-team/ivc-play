jQuery(document).ready(function() {
    jQuery("label#menuOpen").click(function () {
        jQuery(this).toggleClass('menu-button-open');
        jQuery('nav#mobile').toggleClass('show-mobile-menu');
        jQuery('header#header #logo, #wrapper, footer').toggleClass('move-content-left');
        jQuery('header#header .header-icons').toggleClass('move-icons-left');
    });

    jQuery("nav#mobile .mobile-arrow").click(function () {
        jQuery(this).prev('.sub-menu').slideToggle();
    });

    jQuery("nav#mobile .userArrow").click(function () {
        jQuery('ul#user-sub-menu').slideToggle();
    });

    jQuery("nav#desktop .sub-arrow").click(function () {
        jQuery(this).prev('.sub-menu').slideToggle();
    });

    jQuery(window).scroll( function() {
       if(jQuery(window).scrollTop() > 50) {
           jQuery('#header').addClass('header-background');
           if( jQuery('.category-top').length > 0 ) {
               jQuery('.category-top').addClass('hug-header');
           }
       } else {
           jQuery('#header').removeClass('header-background');
           if( jQuery('.category-top').length > 0 ) {
               jQuery('.category-top').removeClass('hug-header');
           }
       }
    });

    jQuery('#open-sub-video-cats').click(function() {
        jQuery('#select-sub-category').slideToggle();
    });

    jQuery('nav#desktop ul li').hover( function() {
        var this_menu_item = jQuery(this);
        jQuery('.wpvs-theme-full-menu ul.sub-menu').removeClass('show-full-width-menu');
        if( this_menu_item.hasClass('wpvs-theme-full-menu') ) {
       		this_menu_item.find('ul.sub-menu').addClass('show-full-width-menu');
        }
    });

      jQuery('#header').mouseleave( function() {
        jQuery('ul.sub-menu').removeClass('show-full-width-menu');
      });

    jQuery('a[href*="#"]').not('[href="#"]').not('[href="#0"]').click(function(event) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                event.preventDefault();
                if( (target.selector == '#comments' && jQuery('#wpvs-video-reviews-container').length > 0) || (target.selector == '#respond') ) {

                } else {
                    jQuery('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                }
            }
        }
    });

    //wpvs_theme_ajax_load_videos_request();
});

jQuery(window).resize(wpvs_reset_main_menu);

function wpvs_reset_main_menu() {
    if(jQuery(window).width() >= 768) {
        jQuery('ul#user-sub-menu').slideUp();
    }
}

/*function wpvs_theme_ajax_load_videos_request() {
    var wpvs_term_request_url = '/wp-json/wp/v2/wpvsvideos?wpvsgenres=';


    var wpvs_term_slider = jQuery(this);
    var wpvs_get_term_videos_url = wpvs_term_request_url + wpvs_term_slider.data('wpvs-term-id');
    var wpvs_term_slider_container = wpvs_term_slider.find('.video-list-slider');
    jQuery.ajax({
        url: wpvs_get_term_videos_url,
        type: "GET",
        success:function(wpvs_term_videos) {
            if( wpvs_term_videos.length > 0 ) {
                wpvs_term_slider_container.data('items', wpvs_term_videos.length).attr('data-items', wpvs_term_videos.length);
                var wpvs_add_video_slide = "";
                var video_counter = 0;
                jQuery.each(wpvs_term_videos, function(index, video) {
                    var open_new_tab = (video.new_tab == 1) ? 'target="_blank"' : '';
                    wpvs_add_video_slide += '<a class="video-slide" href="'+video.link+'" '+open_new_tab+'><div class="video-slide-image border-box">';
                    if( video.images.thumbnail ) {
                        wpvs_add_video_slide += '<img src="'+video.images.thumbnail+'" alt="'+video.title.rendered+'" ';
                        if( video.images.srcset ) {
                            wpvs_add_video_slide += 'srcset="'+video.images.srcset+'" ';
                        }
                        wpvs_add_video_slide += '/>';
                    } else {
                        wpvs_add_video_slide += '<div class="wpvs-no-slide-image"></div>';
                    }

                    if( video.user_data.percent_complete && video.user_data.percent_complete != 0 ) {
                        wpvs_add_video_slide += '<span class="wpvs-cw-progress-bar border-box" style="width: '+video.user_data.percent_complete+'%"></span>';
                    }
                    wpvs_add_video_slide += '</div>';
                    wpvs_add_video_slide += '<div class="video-slide-details border-box"><h4>'+video.title.rendered+'</h4>'+video.excerpt.rendered+'</div>';
                    //if(rvsajax.dropdown) {
                        wpvs_add_video_slide += '<label class="show-vs-drop ease3" data-video="'+video.id+'" data-type="video"><span class="dashicons dashicons-arrow-down-alt2"></span></label>';
                    //}
                    wpvs_add_video_slide += '</a>';
                });
                wpvs_term_slider_container.append(wpvs_add_video_slide);
            }
        },
        error: function(response){
        }
    });
}*/
