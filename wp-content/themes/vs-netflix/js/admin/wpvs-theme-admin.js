jQuery(document).ready(function() {
    jQuery('.wpvs-rating-select ').click( function() {
        jQuery('.wpvs-rating-select ').removeClass('selected-rating');
        jQuery(this).addClass('selected-rating');
    });
    
    jQuery('#rvs-dropdown-menu').click( function() {
        jQuery('#rvs-admin-menu').slideToggle();
    });

    jQuery('body').delegate('#rvs-activate-user', 'click',  function() {
        activate_rvs_user();
    });

    jQuery('body').delegate('#rvs-deactivate-site', 'click',  function() {
        deactivate_rvs_user();
    });

    jQuery('.wpvs-open-icon-set').click(function() {
        var this_update_button = jQuery(this);
        var icon_box = this_update_button.next('.wpvs-icon-set').slideDown();
    });

    jQuery('.wpvs-dashicon-option').click(function() {
        var selected_icon = jQuery(this);
        var parent_icon_box = selected_icon.parents('.wpvs-icon-set');
        var icon_input = parent_icon_box.find('.wpvs-icon-update-input');
        var update_icon_box = parent_icon_box.prev('.wpvs-open-icon-set').find('.wpvs-icon-update');
        var set_new_icon = selected_icon.find('.badge').html();
        var new_span_icon_html = '<span class="dashicons dashicons-'+set_new_icon+'"></span>';
        icon_input.val(set_new_icon);
        update_icon_box.html(new_span_icon_html);
    });

});

function activate_rvs_user() {
    jQuery('.rvs-activation-errors').html("").hide();
    if(!check_rvs_user_activate()) {
        jQuery('#rvs-activate-user').html('<span class="rvs-spin dashicons dashicons-update"></span> Activating...');
        var rvs_email = jQuery('#rvs-username-access').val();
        var rvs_password = jQuery('#rvs-password-access').val();
        jQuery.ajax({
            url: ajaxurl,
            type: "POST",
            data: {
                'action': 'wpvs_theme_activate_customer_access',
                'email': rvs_email,
                'password': rvs_password
            },
            success:function(response) {
                jQuery('#rvs-activate-user').hide();
                jQuery('#rvs-website-activated').html('<label class="rvs-site-is-activated rvs-success">Website Activated</label><label id="rvs-deactivate-site" class="rvs-button rvs-error-button">Deactivate</label>');
            },
            error: function(response){
                show_rvs_error(response.responseText);
                jQuery('.rvs-activation-errors').html(response.responseText).show();
                jQuery('#rvs-website-activated').html('<label id="rvs-activate-user" class="rvs-button">Activate</label>');
            }
        });
    }
}

function deactivate_rvs_user() {
    jQuery('.rvs-activation-errors').html("").hide();
    if(!check_rvs_user_activate()) {
        jQuery('#rvs-deactivate-site').html('<span class="rvs-spin dashicons dashicons-update"></span> Deactivating...');
        var rvs_email = jQuery('#rvs-username-access').val();
        var rvs_password = jQuery('#rvs-password-access').val();
        jQuery.ajax({
            url: ajaxurl,
            type: "POST",
            data: {
                'action': 'wpvs_theme_deactivate_users_access',
                'email': rvs_email,
                'password': rvs_password
            },
            success:function(response) {
                jQuery('#rvs-activate-user').hide();
                jQuery('#rvs-website-activated').html('<label id="rvs-activate-user" class="rvs-button">Activate</label>');
            },
            error: function(response){
                show_rvs_error(response.responseText);
                jQuery('.rvs-activation-errors').html(response.responseText).show();
                jQuery('#rvs-website-activated').html('<label class="rvs-site-is-activated rvs-success">Website Activated</label><label id="rvs-deactivate-site" class="rvs-button rvs-error-button">Deactivate</label>');
            }
        });
    }
}

function check_rvs_user_activate() {
    var field_errors = false;

    jQuery('.rvs-edit-form input').removeClass('rvs-field-error');

    var rvs_email = jQuery('#rvs-username-access');
    var rvs_password = jQuery('#rvs-password-access');


    if(rvs_email.val() == "") {
        rvs_email.addClass('rvs-field-error');
        field_errors = true;
    }

    if(rvs_password.val() == "") {
        rvs_password.addClass('rvs-field-error');
        field_errors = true;
    }

    return field_errors;

}
