<?php /* Template Name: Full Width Stretched */
wp_enqueue_style( 'wpvs-full-stretched-css', get_template_directory_uri() . '/css/full-width-stretched.css' );
get_header();
global $post;
wpvs_do_featured_slider($post->ID);
$wpvs_theme_remove_top_spacing = get_post_meta($post->ID, '_vs_top_spacing', true);
$wpvs_theme_add_page_top_spacing = false;
$wpvs_page_featured_area_type = get_post_meta( $post->ID, 'wpvs_featured_area_slider_type', true );
if( (empty($wpvs_page_featured_area_type) || $wpvs_page_featured_area_type == "none" ) && ! $wpvs_theme_remove_top_spacing ) {
    $wpvs_theme_add_page_top_spacing = true;
}
if($wpvs_theme_add_page_top_spacing) {
 echo '<div class="page-container">';
} if ( have_posts() ) { ?>
<div class="full-width-stretched">
    <div class="col-12">
    <?php while ( have_posts() ) : the_post();
        the_content();
        if( comments_open() ) {
            comments_template();
        }
    endwhile; ?>
    </div>
</div>
<?php } else {
    get_template_part('nothing-found');
}
if($wpvs_theme_add_page_top_spacing) {
    echo '</div>';
}
get_footer(); ?>
