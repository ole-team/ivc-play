<div class="wrap">
    <?php include('wpvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <h3>Restrict Video Access</h3>
        <p>Purchase our <a href="https://www.wpvideosubscriptions.com/video-memberships//" target="_blank">Memberships Add-On</a> to restrict access to your videos to members only.</p>
        <p>The add-on allows you to create a subscription (daily, weekly, monthly or annual) based website like Netflix. <a href="https://www.wpvideosubscriptions.com/video-memberships/" target="_blank">More Details</a></p>
    </div>
</div>
