<?php

function wpvs_theme_check_customer_product_access() {
    global $rvs_user_has_access;
    $rvs_check_access_time = get_option('rvs-access-check-time', null);
    $wpvs_run_access_check = false;
    if( empty($rvs_check_access_time) && $rvs_user_has_access ) {
        $wpvs_run_access_check = true;
    }
    if( ! empty($rvs_check_access_time) && $rvs_check_access_time < time()) {
        $wpvs_run_access_check = true;
    }
    if($wpvs_run_access_check) {

        $user_email = urlencode(get_option('rvs-username-access'));
        $wpvs_access_check_token = urlencode(get_option('wpvs-access-check-token'));
        $this_site_url = urlencode(get_bloginfo('url'));
        $activation_url = 'https://www.wpvideosubscriptions.com/?rvs-user-activation=wpva&rvsuser='.$user_email.'&accesstoken='.$wpvs_access_check_token.'&site='.$this_site_url.'&check=1';
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $activation_url,
            CURLOPT_RETURNTRANSFER => true
        ));
        $has_access_json = curl_exec($ch);
        curl_close($ch);
        $has_access = json_decode($has_access_json, true);

        if( ! $has_access['access'] ) {
            update_option('rvs-activated', false);
            update_option('rvs-access-check-time', null);
        } else {
            if(isset($has_access['themes']) && !empty($has_access['themes'])) {
                $theme_access = $has_access['themes'];
                update_option('rvs-theme-access', $theme_access);
            } else {
                update_option('rvs-theme-access', null);
            }
            if(isset($has_access['plugins']) && !empty($has_access['plugins'])) {
                $plugin_access = $has_access['plugins'];
                update_option('rvs-plugin-access', $plugin_access);
            } else {
                update_option('rvs-plugin-access', null);
            }
            $current_time = time();
            $new_check_time = time() + 3600;
            if( isset($has_access['access_expires']) && ! empty($has_access['access_expires']) && ($has_access['access_expires'] != "never") && (intval($has_access['access_expires']) > $current_time) ) {
                $new_check_time = intval($has_access['access_expires']);
            }
            update_option('rvs-access-check-time', $new_check_time);
        }
        if( isset($has_access['owned_themes']) && ! empty($has_access['owned_themes'])) {
            update_option('wpvs-owned-themes', $has_access['owned_themes']);
        }
    }
}
if( ! get_option('is-wp-videos-multi-site')) {
add_action( 'admin_init', 'wpvs_theme_check_customer_product_access');
}

function wpvs_theme_version_updates_check() {
    if( wpvs_check_for_membership_add_on() ) {
        $wpvs_memberships_current_version = get_option('rvs_memberships_version');
        if( ! empty($wpvs_memberships_current_version) ) {
            $wpvs_membership_updates_version = intval(str_replace(".","",$wpvs_memberships_current_version));
            if($wpvs_membership_updates_version < 314) {
                $wpvs_membership_update_required = true;
                add_action( 'admin_notices', 'wpvs_wpvideos_update_message_314' );
                function wpvs_wpvideos_update_message_314() {
                  echo '<div class="update-nag">';
                  _e( 'IMPORTANT: WP Video Memberships needs an update. Please <a href="'.admin_url('plugins.php').'">upgrade to version 3.1.4</a>', 'wpvs-theme' );
                  echo '</div>';
                }
            }
        }
    }
}
add_action('admin_init', 'wpvs_theme_version_updates_check');

if( ! function_exists('wpvs_generate_secure_random_bytes') ) {
function wpvs_generate_secure_random_bytes($length) {
    if( function_exists('random_bytes') ) {
        return bin2hex(random_bytes(intval($length)));
    } else {
        $cry_strong = true;
        $random_bytes = openssl_random_pseudo_bytes(intval($length), $cry_strong);
        return bin2hex($random_bytes);
    }
}
}
