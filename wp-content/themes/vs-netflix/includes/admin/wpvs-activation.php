<?php global $rvs_user_has_access; ?>

<div class="wrap">
    <?php include('wpvs-admin-menu.php'); ?>
    <div class="vimeosync">
            <div class="rvs-container">
                <div class="rvs-col-6 rvs-edit-form">
                    <h3>WP Video Subscriptions Login</h3>
                    <p>Enter the Email and Password for your account at <a href="https://www.wpvideosubscriptions.com/account" target="_blank">wpvideosubscriptions.com</a></p>
                    <label>Email</label>
                    <input type="text" name="rvs-username-access" id="rvs-username-access" value="<?php echo esc_attr( get_option('rvs-username-access') ); ?>" autocomplete="off" placeholder="Email"/>
                    <label>Password</label>
                    <input type="password" name="rvs-password-access" id="rvs-password-access" value="" autocomplete="off" placeholder="Password"/>
                    <div class="rvs-activation-errors"></div>
                    <hr>
                    <div id="rvs-website-activated">
                    <?php if($rvs_user_has_access) : ?>
                        <label class="rvs-site-is-activated rvs-success">Website Activated</label>
                        <label id="rvs-deactivate-site" class="rvs-button rvs-error-button">Deactivate</label>
                    <?php else : ?>
                        <label id="rvs-activate-user" class="rvs-button">Activate</label>
                    <?php endif; ?>
                    </div>

                </div>
                <div class="rvs-col-6">
                    <h3>What Is This?</h3>
                    <p>If you have an account at <a href="https://www.wpvideosubscriptions.com" target="_blank">wpvideosubscriptions.com</a> this page allows you to activate:
                    <ul>
                        <li><a href="https://www.wpvideosubscriptions.com/video-memberships" target="_blank">WP Video Memberships</a></li>
                        <li><a href="https://www.wpvideosubscriptions.com/wordpress-netflix-theme" target="_blank">Themes</a> by WP Video Subscriptions</li>
                    </ul>
                </div>
            </div>
    </div>
</div>
