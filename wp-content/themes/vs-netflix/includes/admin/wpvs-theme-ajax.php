<?php

add_action( 'wp_ajax_wpvs_theme_video_post_save_meta_ajax_request', 'wpvs_theme_video_post_save_meta_ajax_request' );

function wpvs_theme_video_post_save_meta_ajax_request() {
    if ( isset($_REQUEST) ) {
        $postId = $_REQUEST['postId'];
        $postTitle = get_the_title( $postId );
        $video_id = $_REQUEST['videoid'];
        $newiFrameSrc = 'https://player.vimeo.com/video/' . $video_id;
        $newiFrame = '<iframe class="wpvs-vimeo-video-player" src="' . $newiFrameSrc . '" width="1280" height="720" frameborder="0" title="' . $postTitle . '" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" allow="autoplay"></iframe>';
        update_post_meta($postId, 'rvs_video_post_vimeo_html', $newiFrame);
        update_post_meta($postId, 'rvs_video_post_vimeo_id', $video_id);
        echo $newiFrame;
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_theme_activate_customer_access', 'wpvs_theme_activate_customer_access' );

function wpvs_theme_activate_customer_access() {
    if ( isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['password']) && !empty($_POST['password']) ) {
        $user_email = urlencode($_POST['email']);
        $user_password = urlencode($_POST['password']);
        $this_site_url = urlencode(get_bloginfo('url'));
        $activation_url = 'https://www.wpvideosubscriptions.com/?rvs-user-activation=wpva&rvsuser='.$user_email.'&rvspassword='.$user_password.'&site='.$this_site_url;
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $activation_url,
            CURLOPT_RETURNTRANSFER => true
        ));
        $has_access_json = curl_exec($ch);
        if(curl_error($ch)){
            $error_message = curl_error($ch);
        }
        curl_close($ch);
        $has_access = json_decode($has_access_json, true);
        if( isset($has_access['owned_themes']) && ! empty($has_access['owned_themes'])) {
            update_option('wpvs-owned-themes', $has_access['owned_themes']);
        }
        if($has_access['access']) {
            update_option('rvs-username-access', $_POST['email']);
            update_option('rvs-activated', true);
            if( isset($has_access['access_token']) && ! empty($has_access['access_token']) ) {
                update_option('wpvs-access-check-token', $has_access['access_token']);
            }

            if(isset($has_access['themes']) && !empty($has_access['themes'])) {
                $theme_access = $has_access['themes'];
                update_option('rvs-theme-access', $theme_access);
            } else {
                update_option('rvs-theme-access', null);
            }
            if(isset($has_access['plugins']) && !empty($has_access['plugins'])) {
                $plugin_access = $has_access['plugins'];
                update_option('rvs-plugin-access', $plugin_access);
            } else {
                update_option('rvs-plugin-access', null);
            }
            $current_time = time();
            $new_check_time = time() + 3600;
            if( isset($has_access['access_expires']) && ! empty($has_access['access_expires']) && ($has_access['access_expires'] != "never") && (intval($has_access['access_expires']) > $current_time) ) {
                $new_check_time = intval($has_access['access_expires']);
            }
            update_option('rvs-access-check-time', $new_check_time);
        } else {
            update_option('rvs-activated', false);
            if(empty($error_message)) {
                $error_message = "Something went wrong activating your site";
            }
            status_header(400);
            if(!empty($has_access['error'])) {
                if($has_access['error'] == "max_sites_reached") {
                    $error_message = "You've hit your website activation limit";
                }

                if($has_access['error'] == "user_not_found" || $has_access['error'] == "password_incorrect") {
                    $error_message = "Email or password is incorrect.";
                }

                if($has_access['error'] == "trial_expired") {
                    $error_message = 'Trial period ended. <a href="https://www.wpvideosubscriptions.com/account/subscriptions" target="_blank">Check Your Subscriptions Here</a>';
                }

                if($has_access['error'] == "no_subscription") {
                    $error_message = 'You do not have an active subscription. <a href="https://www.wpvideosubscriptions.com/account/subscriptions" target="_blank">Check Your Subscriptions Here</a>';
                }
            }

            echo $error_message;
            exit;
        }
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_theme_deactivate_users_access', 'wpvs_theme_deactivate_users_access' );

function wpvs_theme_deactivate_users_access() {
    if ( isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['password']) && !empty($_POST['password']) ) {
        $user_email = urlencode($_POST['email']);
        $user_password = urlencode($_POST['password']);
        $this_site_url = urlencode(get_bloginfo('url'));
        $deactivation_url = 'https://www.wpvideosubscriptions.com/?rvs-user-activation=wpva&rvsuser='.$user_email.'&rvspassword='.$user_password.'&site='.$this_site_url.'&deactivate=1';
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $deactivation_url,
            CURLOPT_RETURNTRANSFER => true
        ));
        $site_deactivated_json = curl_exec($ch);
        curl_close($ch);
        $site_deactivated = json_decode($site_deactivated_json, true);
        if($site_deactivated['site']) {
            update_option('rvs-activated', false);
            update_option('rvs-access-check-time', null);
        }
        update_option('rvs-activated', false);
        update_option('rvs-access-check-time', null);
        update_option('rvs-theme-access', null);
        update_option('rvs-plugin-access', null);
        update_option('wpvs-access-check-token', null);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_theme_create_video_html_request', 'wpvs_theme_create_video_html_request' );

function wpvs_theme_create_video_html_request() {
    if ( isset($_POST['videosrc']) && ! empty($_POST['videosrc']) && isset($_POST['videowidth']) && ! empty($_POST['videowidth']) && isset($_POST['videoheight']) && ! empty($_POST['videoheight']) && isset($_POST['videotype']) && ! empty($_POST['videotype'])) {
        $video_src = $_POST['videosrc'];
        $video_width = $_POST['videowidth'];
        $video_height = $_POST['videoheight'];
        $video_type = $_POST['videotype'];
        $video_shortcode = '[video width="'.$video_width.'" height="'.$video_height.'" '.$video_type.'="'.$video_src.'"][/video]';
        echo do_shortcode( $video_shortcode );
    }
    wp_die();
}
