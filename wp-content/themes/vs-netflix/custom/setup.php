<?php
require_once('wpvs-slug-settings.php');
require_once('custom-meta-functions.php');
require_once('wpvs-content-type-functions.php');
require_once('scripts.php');
require_once('admin/scripts.php');

require_once('videos/config.php');
require_once('terms/config.php');
