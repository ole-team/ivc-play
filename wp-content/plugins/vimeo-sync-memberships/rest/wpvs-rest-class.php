<?php

namespace WPVSRest;

class WPVS_REST_Controller {
    
    protected $client_id;
    protected $secret;
    
    public function __construct() {
        $this->client_id = get_option('wpvs_rest_api_client_id', "");
        $this->secret = get_option('wpvs_rest_api_secret', "");
    }
    
    public function validateAuthRequest($client_id, $secret) {
        $valid_request = false;
        if( $client_id == $this->client_id && $secret == $this->secret) {
            $valid_request = true;
        }
        return $valid_request;
    }
    
    public function wpvs_create_api_access_token($user_id) {
        $new_access_details = null;
        if( ! empty($user_id) ) {
            $user_api_access = get_user_meta($user_id, 'wpvs_api_access_token', true);
            if( empty($user_api_access) ) {
                $new_access_token = $this->wpvs_rest_api_generate_secure_token(50);
                $new_refresh_token = $this->wpvs_rest_api_generate_secure_token(50);
                $token_expires = strtotime('+1 day', current_time('timestamp', 1));
                $new_access_details = wpvs_rest_api_add_new_access($user_id, $new_access_token, $new_refresh_token, $token_expires);
            } else {
                $new_access_details = $user_api_access;
            }
        }
        return $new_access_details;
    }
    
    public function wpvs_refresh_api_access_token($user_id, $refresh_token) {
        $new_access_details = null;
        if( ! empty($user_id) && ! empty($refresh_token) ) {
            $user_api_access = get_user_meta($user_id, 'wpvs_api_access_token', true);
            if( ! empty($user_api_access) && isset($user_api_access['refresh_token']) && $user_api_access['refresh_token'] == $refresh_token ) {
                $new_access_token = $this->wpvs_rest_api_generate_secure_token(50);
                $token_expires = strtotime('+1 day', current_time('timestamp', 1));
                $new_access_details = wpvs_rest_api_update_access_token($user_id, $new_access_token, $refresh_token, $token_expires);
            }
        }
        return $new_access_details;
    }
        
    public function wpvs_validate_rest_api_access_token($token) {
        $access_is_valid = array('valid' => false, 'error' => 'Invalid token');
        $user_id = wpvs_rest_api_get_access_token_user_id($token);
        $current_time = current_time('timestamp', 1);
        if( ! empty($user_id) ) {
            $user_api_access = get_user_meta($user_id, 'wpvs_api_access_token', true);
            if( ! empty($user_api_access) && isset($user_api_access['token']) && isset($user_api_access['expires']) ) {
                if( intval($user_api_access['expires']) < $current_time ) {
                    $access_is_valid['error'] = __('Token expired', 'vimeo-sync-memberships');
                    return $access_is_valid;
                }
                if( $user_api_access['token'] == $token ) {
                    $access_is_valid['valid'] = true;
                    $access_is_valid['user_id'] = $user_id;
                }
            }
        }
        return $access_is_valid;
    }
    
    public function wpvs_rest_api_generate_secure_token($length) {
        if( function_exists('random_bytes') ) {
            return bin2hex(random_bytes(intval($length)));
        } else {
            $cry_strong = true;
            $random_bytes = openssl_random_pseudo_bytes(intval($length), $cry_strong);
            return bin2hex($random_bytes);
        }
    }

      
}