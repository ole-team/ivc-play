<?php
use WPVSRest\WPVS_REST_Controller;
if( ! function_exists('wpvs_memberships_rest_api_video_meta_fields') ) {
function wpvs_memberships_rest_api_video_meta_fields() {
    global $is_valid_token;
    global $api_user_id;
    $api_user_id = null;
    $is_valid_token = wpvs_verify_api_access_token();

    // REST DATA REQUIRES VALID TOKEN
    if( ! empty($is_valid_token) && $is_valid_token['valid'] && isset($is_valid_token['user_id']) ) {
        $api_user_id = intval($is_valid_token['user_id']);
        register_rest_field( 'rvs_video', 'video_download_link', array(
            'get_callback' => function( $video_object ) {
                global $api_user_id;
                $video_id = $video_object['id'];
                $user_has_access = wpvs_rest_api_user_has_video_access($api_user_id, $video_id);
                if($user_has_access) {
                    $video_download_link = get_post_meta( $video_id, 'rvs_video_download_link', true );
                    //$members_can_download = get_post_meta( $video_id, 'wpvs_members_can_download', true );
                }

                if( empty($video_download_link) ) {
                    $video_download_link = "";
                }
                return (string) $video_download_link;
            },
            'update_callback' => null,
            'schema' => array(
                'description' => __( 'Video Download Link.' ),
                'type'        => 'string'
            )
        ) );

        register_rest_field( 'rvs_video', 'video_html', array(
            'get_callback' => function( $video_object ) {
                global $api_user_id;
                $video_html = "";
                $video_id = $video_object['id'];
                $user_has_access = wpvs_rest_api_user_has_video_access($api_user_id, $video_id);
                if($user_has_access) {
                    $video_type = get_post_meta($video_id, '_rvs_video_type', true);
                    if($video_type == "vimeo" || $video_type == "youtube") {
                        $video_html = get_post_meta($video_id, 'rvs_video_post_vimeo_html', true);
                    }
                    if($video_type == "custom") {
                        $video_html = get_post_meta($video_id, 'rvs_video_custom_code', true);
                    }
                }
                if( empty($video_html) ) {
                    $video_html = "";
                }
                return (string) $video_html;
            },
            'update_callback' => null,
            'schema' => array(
                'description' => __( 'Video HTML code.' ),
                'type'        => 'string'
            )
        ) );

        register_rest_field( 'rvs_video', 'wpvs_has_access', array(
            'get_callback' => function( $video_object ) {
                global $api_user_id;
                $video_id = $video_object['id'];
                $user_has_access = wpvs_rest_api_user_has_video_access($api_user_id, $video_id);
                return (boolean) $user_has_access;
            },
            'update_callback' => null,
            'schema' => array(
                'description' => __( 'WPVS customer has video access.' ),
                'type'        => 'boolean'
            )
        ) );
    } else {
        register_rest_field( 'rvs_video', 'wpvs_rest_error', array(
            'get_callback' => function( $video_object ) {
                global $is_valid_token;
                $wpvs_rest_api_error = $is_valid_token['error'];
                return (string) $wpvs_rest_api_error;
            },
            'update_callback' => null,
            'schema' => array(
                'description' => __( 'WPVS REST ERROR' ),
                'type'        => 'string'
            )
        ) );
    }

    // WP VIDEO MEMBERSHIPS REST API FIELDS
    register_rest_field( 'rvs_video', 'wpvs_access_options', array(
        'get_callback' => function( $video_object ) {
            $video_id = $video_object['id'];
            $wpvs_access_options = array();
            $video_memberships = get_post_meta($video_id, '_rvs_memberships', true );
            $wpvs_free_for_users = get_post_meta($video_id, '_rvs_membership_users_free', true );
            $video_onetime_price = get_post_meta($video_id, '_rvs_onetime_price', true );
            $video_rental_price = get_post_meta($video_id, 'rvs_rental_price', true );
            $video_rental_expires = get_post_meta($video_id, 'rvs_rental_expires', true );
            $video_rental_type = get_post_meta($video_id, 'rvs_rental_type', true );

            // add memberships
            if( ! empty($video_memberships) ) {
                $wpvs_access_options['membership_ids'] = $video_memberships;
            }
            // add free for registered users
            if( ! empty($wpvs_free_for_users) ) {
                $wpvs_access_options['free_for_users'] = true;
            } else {
                $wpvs_access_options['free_for_users'] = false;
            }

            // purchase price
            if( ! empty($video_onetime_price) ) {
                $wpvs_access_options['purchase_price'] = $video_onetime_price;
            }

            // rental details
            if( ! empty($video_rental_price) ) {
                $wpvs_access_options['rental_details'] = array(
                    'rental_price' => $video_rental_price,
                    'rental_type' => $video_rental_type,
                    'rental_expires' => $video_rental_expires
                );
            }
            return (array) $wpvs_access_options;
        },
        'update_callback' => null,
        'schema' => array(
            'description' => __( 'WPVS Video Access Options' ),
            'type'        => 'array'
        )
    ) );

}
add_action( 'rest_api_init', 'wpvs_memberships_rest_api_video_meta_fields' );
}

if( ! function_exists('wpvs_memberships_register_api_routes') ) {
function wpvs_memberships_register_api_routes() {
    register_rest_route( 'wpvs-memberships/v1', '/login', array(
            'methods'  => 'POST',
            'callback' => 'wpvs_api_login',
        )
    );

    register_rest_route( 'wpvs-memberships/v1', '/auth', array(
            'methods'  => 'POST',
            'callback' => 'wpvs_api_create_auth_token',
        )
    );

    register_rest_route( 'wpvs-memberships/v1', '/refresh', array(
            'methods'  => 'POST',
            'callback' => 'wpvs_api_refresh_access_token',
        )
    );

    function wpvs_api_login() {
        $return_access = array();
        $username = null;

        $wpvs_login_nonce = urldecode($_POST['token']);
        if ( ! wp_verify_nonce( $wpvs_login_nonce, 'wpvs-api-login-token' ) ) {
            return array('error' => __('Invalid Access Token', 'vimeo-sync-memberships'));
        } else {
            if ( ! isset($_POST['username']) || ! isset($_POST['pass']) ) {
                return array('error' => __('Missing required fields', 'vimeo-sync-memberships'));
            } else {
                $username = urldecode($_POST['username']);
                $password = urldecode($_POST['pass']);
                if( ! empty($username) && ! empty($password) ) {
                    $user = wp_authenticate( $username, $password );
                    if( ! is_wp_error($user) ) {
                        $wpvs_rest_controller = new WPVS_REST_Controller();
                        $wpvs_api_access = $wpvs_rest_controller->wpvs_create_api_access_token($user->ID);
                        if( ! empty($wpvs_api_access) ) {
                            if( isset($wpvs_api_access['token']) ) {
                                $return_access['token'] = $wpvs_api_access['token'];
                            }
                            if( isset($wpvs_api_access['refresh_token']) ) {
                                $return_access['refresh_token'] = $wpvs_api_access['refresh_token'];
                            }
                            if( isset($wpvs_api_access['expires']) ) {
                                $return_access['expires'] = $wpvs_api_access['expires'];
                            }
                        } else {
                            return array('error' => __('Access token creation failed', 'vimeo-sync-memberships'));
                        }
                    } else {
                        return array('error' => __('User authentication failed', 'vimeo-sync-memberships'));
                    }
                } else {
                    return array('error' => __('Missing required fields', 'vimeo-sync-memberships'));
                }
                return $return_access;
            }
        }
    }

    function wpvs_api_create_auth_token() {
        $valid_auth_request = array();
        $client_id = null;
        $secret = null;
        $wpvs_request_headers = getallheaders();
        if( ! empty($wpvs_request_headers) ) {
            if( isset($wpvs_request_headers['WPVS-ClientID']) ) {
                $client_id = $wpvs_request_headers['WPVS-ClientID'];
            }
            if( empty($client_id) && isset($wpvs_request_headers['Wpvs-Clientid']) ) {
                $client_id = $wpvs_request_headers['Wpvs-Clientid'];
            }
            if( isset($wpvs_request_headers['WPVS-Secret']) ) {
                $secret = $wpvs_request_headers['WPVS-Secret'];
            }
            if( empty($secret) && isset($wpvs_request_headers['Wpvs-Secret']) ) {
                $secret = $wpvs_request_headers['Wpvs-Secret'];
            }
        }
        if( ! empty($client_id) && ! empty($secret) ) {
            $wpvs_rest_controller = new WPVS_REST_Controller();
            $request_is_valid = $wpvs_rest_controller->validateAuthRequest($client_id, $secret);
            if( $request_is_valid ) {
                $wpvs_create_nonce = wp_create_nonce('wpvs-api-login-token'); // CHANGE TO UNIQUE CUSTOM API CLIENT KEY (WP ADMIN AREA)
                return array('token' => $wpvs_create_nonce);
            } else {
                return array('error' => __('Invalid API Credentials', 'vimeo-sync-memberships'));
            }

        } else {
            return array('error' => __('Missing API Credentials', 'vimeo-sync-memberships'));
        }
    }

    function wpvs_api_refresh_access_token() {
        $return_access = array();
        $username = null;
        $password = null;
        $refresh_token = null;
        if ( ! isset($_POST['username']) || ! isset($_POST['pass']) ) {
            return array('error' => __('Missing required fields', 'vimeo-sync-memberships'));
        } else {
            $username = urldecode($_POST['username']);
            $password = urldecode($_POST['pass']);

            $wpvs_request_headers = getallheaders();
            if( ! empty($wpvs_request_headers) ) {
                if( isset($wpvs_request_headers['WPVS-RefreshToken']) ) {
                    $refresh_token = $wpvs_request_headers['WPVS-RefreshToken'];
                }

                if( empty($refresh_token) && isset($wpvs_request_headers['Wpvs-Refreshtoken']) ) {
                    $refresh_token = $wpvs_request_headers['Wpvs-Refreshtoken'];
                }
            }

            if( ! empty($refresh_token) && ! empty($username) && ! empty($password) ) {
                $user = wp_authenticate( $username, $password );
                if( ! is_wp_error($user) ) {
                    $wpvs_rest_controller = new WPVS_REST_Controller();
                    $wpvs_api_access = $wpvs_rest_controller->wpvs_refresh_api_access_token($user->ID, $refresh_token);
                    if( ! empty($wpvs_api_access) ) {
                        if( isset($wpvs_api_access['token']) ) {
                            $return_access['token'] = $wpvs_api_access['token'];
                        }
                        if( isset($wpvs_api_access['refresh_token']) ) {
                            $return_access['refresh_token'] = $wpvs_api_access['refresh_token'];
                        }
                        if( isset($wpvs_api_access['expires']) ) {
                            $return_access['expires'] = $wpvs_api_access['expires'];
                        }
                    } else {
                        return array('error' => __('Refresh access token failed', 'vimeo-sync-memberships'));
                    }
                } else {
                    return array('error' => __('User authentication failed', 'vimeo-sync-memberships'));
                }
            } else {
                return array('error' => __('Missing required fields', 'vimeo-sync-memberships'));
            }
            return $return_access;
        }
    }
}
add_action( 'rest_api_init', 'wpvs_memberships_register_api_routes' );
}

if( ! function_exists('wpvs_verify_api_access_token') ) {
    function wpvs_verify_api_access_token() {
        $is_valid_token = array('valid' => false, 'error' => __('Invalid token', 'vimeo-sync-memberships'));
        if( isset($_GET['wpvstoken']) && ! empty($_GET['wpvstoken'])) {
            $access_token = $_GET['wpvstoken'];
            $wpvs_rest_controller = new WPVS_REST_Controller();
            $is_valid_token = $wpvs_rest_controller->wpvs_validate_rest_api_access_token($access_token);
        }
        return $is_valid_token;
    }
}

if( ! function_exists('wpvs_rest_api_user_has_video_access') ) {
    function wpvs_rest_api_user_has_video_access($api_user_id, $video_id) {
        global $rvs_live_mode;
        if($rvs_live_mode == "off") {
            $get_memberships = 'rvs_user_memberships_test';
        } else {
            $get_memberships = 'rvs_user_memberships';
        }
        $wpvs_video_has_restriction = false;
        $show_video_content = false;
        $has_membership = false;
        $no_access_reason = "nosubscription";
        $no_access_message = __('Sorry, you do not have access to this video.', 'vimeo-sync-memberships');
        $rvs_overdue_access = get_option('rvs_overdue_access', 0);
        $wpvs_free_for_users = get_post_meta( $video_id, '_rvs_membership_users_free', true );
        $video_onetime_price = get_post_meta( $video_id, '_rvs_onetime_price', true );
        $video_rental_price = get_post_meta( $video_id, 'rvs_rental_price', true );
        // Gather required memberships
        $membership_array = get_post_meta( $video_id, '_rvs_memberships', true );
        $wpvs_video_terms = wp_get_post_terms( $video_id, 'rvs_video_category', array('fields' => 'id=>parent') );
        $wpvs_additional_payment_options = wpvs_get_additional_payment_options($wpvs_video_terms);
        $wpvs_additional_memberships = $wpvs_additional_payment_options['memberships'];
        $wpvs_additional_purchase_options = $wpvs_additional_payment_options['purchases'];
        $membership_array = array_merge($membership_array, $wpvs_additional_memberships);

        if( ! empty($membership_array) || ! empty($video_onetime_price) || ! empty($video_rental_price) || $wpvs_free_for_users || ! empty($wpvs_additional_purchase_options) ) {
            $wpvs_video_has_restriction = true;
        }

        if( $wpvs_video_has_restriction ) {
            if ( ! empty($api_user_id) ) {
                $wpvs_api_user = get_user_by('id', $api_user_id);
                $wpvs_customer = new WPVS_Customer($wpvs_api_user);
                if($wpvs_free_for_users || user_can( $api_user_id, 'manage_options' )) {
                    $show_video_content = true;
                }
                if( ! $show_video_content ) {
                    $has_membership = $wpvs_customer->has_access($membership_array, $video_id);
                    if($has_membership['has_access']) {
                        $show_video_content = true;
                    }
                }

                if( ! $show_video_content) {
                    $user_term_purchases = get_user_meta($api_user_id, 'rvs_user_term_purchases', true);
                    if( ! empty($user_term_purchases) && ! empty($wpvs_additional_purchase_options) ) {
                        foreach($user_term_purchases as $purchased_term) {
                            if( in_array($purchased_term, $wpvs_additional_purchase_options) ) {
                                $show_video_content = true;
                                break;
                            }
                        }
                    }
                }
            }
        } else {
            $show_video_content = true;
        }
        return $show_video_content;
    }
}
