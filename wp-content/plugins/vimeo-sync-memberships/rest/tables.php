<?php

function wpvs_create_rest_api_tables() {
    global $wpdb;
    if( ! wpvs_rest_api_access_tables_exist() ) {
        $wpvs_rest_api_table_name = $wpdb->prefix . 'wpvs_api_access';
        $charset_collate = $wpdb->get_charset_collate();

        $create_access_tables = "CREATE TABLE $wpvs_rest_api_table_name (
          token varchar(100) NOT NULL,
          user_id int NOT NULL,
          refresh_token varchar(100) NOT NULL,
          expires int NOT NULL,
          PRIMARY KEY (token),
          UNIQUE (token),
          UNIQUE (user_id),
          UNIQUE (refresh_token)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $create_access_tables );
    }
}

function wpvs_rest_api_access_tables_exist() {
    global $wpdb;
    $wpvs_rest_api_tables_exist = true;
    $wpvs_rest_api_table_name = $wpdb->prefix . 'wpvs_api_access';
    if($wpdb->get_var("SHOW TABLES LIKE '$wpvs_rest_api_table_name'") != $wpvs_rest_api_table_name) {
         $wpvs_rest_api_tables_exist = false;
    }
    return $wpvs_rest_api_tables_exist;
}


function wpvs_rest_api_add_new_access($user_id, $token, $refresh_token, $expires) {
    global $wpdb;
    $wpdb->show_errors = false;
    $wpvs_rest_api_table_name = $wpdb->prefix . 'wpvs_api_access';
    $new_user_access = array( 
        'token' => $token, 
        'user_id' => intval($user_id),
        'refresh_token' => $refresh_token,
        'expires' => intval($expires),
    );
	$access_inserted = $wpdb->insert( 
		$wpvs_rest_api_table_name, 
		$new_user_access
	);
    if( ! empty($access_inserted) ) {
        update_user_meta($user_id, 'wpvs_api_access_token', $new_user_access);
        return $new_user_access;
    } else {
        return false;
    }
}

function wpvs_rest_api_get_access_token_user_id($token) {
    global $wpdb;
    $wpvs_rest_api_table_name = $wpdb->prefix . 'wpvs_api_access';
    $user_id = null;
    $access_token = $wpdb->get_results("SELECT * FROM $wpvs_rest_api_table_name WHERE token = '$token'");
    if( ! empty($access_token) && isset($access_token[0]->user_id) ) {
        $user_id = $access_token[0]->user_id;
    } 
    return $user_id;
}

function wpvs_rest_api_update_access_token($user_id, $new_token, $refresh_token, $expires) {
    global $wpdb;
    $wpvs_rest_api_table_name = $wpdb->prefix . 'wpvs_api_access';
    
    $update_token = array(
        'token' => $new_token,
        'expires' => $expires
    );
    $update_api_access = array(
        'user_id' => $user_id,
        'refresh_token' => $refresh_token
    );
    $new_token_details = $wpdb->update($wpvs_rest_api_table_name, $update_token, $update_api_access);
    
    if( ! empty($new_token_details) ) {
        $new_user_access = array( 
            'token' => $new_token, 
            'user_id' => intval($user_id),
            'refresh_token' => $refresh_token,
            'expires' => intval($expires),
        );
        update_user_meta($user_id, 'wpvs_api_access_token', $new_user_access);
        return $new_user_access;
    } else {
        return false;
    }
}


