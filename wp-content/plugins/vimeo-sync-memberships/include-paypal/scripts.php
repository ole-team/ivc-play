<?php

function wpvs_memberships_load_paypal_scripts() {
    global $rvs_current_version;
    global $wpvs_include_checkout_scripts;
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
		$environment = 'sandbox';
	} else {
		$environment = 'production';
	}
    wp_register_script( 'paypal-checkout-object', 'https://www.paypalobjects.com/api/checkout.js', '', '', true);
    if( $wpvs_include_checkout_scripts ) {
        wp_enqueue_script('paypal-checkout-object');
        wp_enqueue_script( 'rvs-paypal-checkout', RVS_MEMBERS_PAYPAL_URL . 'js/paypal-checkout.js', array('jquery'), $rvs_current_version, true);
        wp_localize_script('rvs-paypal-checkout', 'paypalajax', array(
            'url' => admin_url( 'admin-ajax.php' ), 'rvsmessage' => array('error' => __('Oops, look like something went wrong','vimeo-sync-memberships'), 'processing' => __('Processing','vimeo-sync-memberships')), 'env' => $environment
            )
        );
    }
}
add_action('wp_enqueue_scripts', 'wpvs_memberships_load_paypal_scripts');