<?php 

function rvs_get_paypal_customer($payer_id) {
    global $rvs_live_mode;
    $found_paypal_customer = null;
    if($rvs_live_mode == "off") {
        $get_payer_id = 'rvs_paypal_test_payer_id';
    } else {
        $get_payer_id = 'rvs_paypal_payer_id';
    }
    $user_args = array(
        'blog_id'      => $GLOBALS['blog_id'],
        'meta_key'     => $get_payer_id,
        'meta_value'   => $payer_id,
        'meta_compare' => '=',
        'fields'       => array('ID'),
    );
    $paypal_customer = get_users( $user_args );
    if( ! empty($paypal_customer) && isset($paypal_customer[0]) ) {
        $found_paypal_customer = $paypal_customer[0];
    }
    return $found_paypal_customer;
}

function rvs_create_paypal_config() {
    global $rvs_live_mode;
    global $rvs_paypal_settings;
    if($rvs_live_mode == "on") {
        $client_id = $rvs_paypal_settings['live_client_id'];
        $client_secret = $rvs_paypal_settings['live_client_secret'];
        $api_mode = array('mode' => 'live');
    } else {
        $client_id = $rvs_paypal_settings['sandbox_client_id'];
        $client_secret = $rvs_paypal_settings['sandbox_client_secret'];
        $api_mode = array('mode' => 'sandbox');
    }
    return array('client' => $client_id, 'secret' => $client_secret, 'api' => $api_mode);
}

// UPDATE PAYPAL USER SUBSCRIPTION STATUS

function wpvs_update_paypal_user_subscription_status($user_id, $subscription_id, $status, $renewal, $test) {
    if($test) {
        $get_memberships = 'rvs_user_memberships_test';
    } else {
        $get_memberships = 'rvs_user_memberships';
    }
    $user_memberships = get_user_meta($user_id, $get_memberships, true);
    if(!empty($user_memberships)) {
        foreach($user_memberships as $key => &$membership) {
            if($membership["id"] == $subscription_id) {
                $membership["status"] = $status;
                $new_renewal = strtotime($renewal);
                $current_end_time = strtotime($membership["ends"]);
                if( ! empty($new_renewal) && $new_renewal > $current_end_time) {
                    $membership["ends"] = $renewal;
                    $membership["reminder_sent"] = 0;
                }
                break;
            }
        }
        update_user_meta($user_id, $get_memberships, $user_memberships);
    }
}

function wpvs_cancel_users_paypal_plan($user_id, $subscription_id, $test_mode) {
    global $rvs_paypal_settings;
    require_once(RVS_MEMBERS_BASE_DIR . '/paypal/autoload.php');
    if($test_mode) {
        $client_id = $rvs_paypal_settings['sandbox_client_id'];
        $client_secret = $rvs_paypal_settings['sandbox_client_secret'];
        $api_mode = array('mode' => 'sandbox');
    } else {
        $client_id = $rvs_paypal_settings['live_client_id'];
        $client_secret = $rvs_paypal_settings['live_client_secret'];
        $api_mode = array('mode' => 'live');
    }

    $apiContext = new PayPal\Rest\ApiContext(
        new PayPal\Auth\OAuthTokenCredential(
            $client_id,
            $client_secret
        )
    );
    $apiContext->setConfig($api_mode);
    $agreementStateDescriptor = new PayPal\Api\AgreementStateDescriptor();
    try {
        $paypal_agreement = PayPal\Api\Agreement::get($subscription_id, $apiContext);
        $agreementStateDescriptor->setNote("Cancelling the agreement");
        if($paypal_agreement->state != "Cancelled") {
            $paypal_agreement->cancel($agreementStateDescriptor, $apiContext);
        }
    } catch (Exception $ex) {

    }
}

function wpvs_handle_paypal_error($error_object) {
    $error_return = array(
        'name' => null,
        'message' => null
    );
    if( method_exists($error_object, 'getName') ) {
        $error_return['name'] = $error_object->getName();
    }
    if( method_exists($error_object, 'getMessage') ) {
        $error_return['message'] = $error_object->getMessage();
    }
    if( method_exists($error_object, 'getData') ) {
        $error_data = json_decode($error_object->getData());
        if( isset($error_data->name) ) {
            $error_return['name'] = $error_data->name;
        }
        if( isset($error_data->message) ) {
            $error_return['message'] = $error_data->message;
        }
        if( isset($error_data->details) && isset($error_data->details[0]->issue) ) {
            if( isset($error_data->details[0]->issue) ) {
                $error_return['message'] .= ' issue: ' . $error_data->details[0]->issue; 
            }
            
            if( isset($error_data->details[0]->field) ) {
                $error_return['message'] .= ' field: ' . $error_data->details[0]->field; 
            }
        }
    }
    
    return $error_return;
}