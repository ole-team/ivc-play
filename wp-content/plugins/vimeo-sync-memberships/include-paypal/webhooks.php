<?php

 function rvs_paypal_event_listener() {
    if(isset($_GET['rvs-paypal-listener']) && $_GET['rvs-paypal-listener'] == 'paypal') {
        require_once(RVS_MEMBERS_BASE_DIR . '/paypal/autoload.php');
        global $rvs_live_mode;
        $test_mode = false;
        if($rvs_live_mode == "off") {
            $test_mode = true;
        }
        $paypal_config = rvs_create_paypal_config();

        $apiContext = new PayPal\Rest\ApiContext(
            new PayPal\Auth\OAuthTokenCredential(
                $paypal_config['client'],  // ClientID
                $paypal_config['secret']  // ClientSecret
            )
        );
        $apiContext->setConfig($paypal_config['api']);

        $body = @file_get_contents('php://input');
        $event = json_decode($body);
        $wpvs_current_time = current_time('timestamp', 1);
        if(isset($event->event_type)) {

            if( isset($event->links[0]->href) ) {
                if( strpos($event->links[0]->href, 'sandbox.paypal') !== false) {
                    $test_mode = true;
                } else {
                    $test_mode = false;
                }
            }

            if($event->event_type == "PAYMENT.SALE.COMPLETED") {
                $get_amount = floatval($event->resource->amount->total);
                $charge_amount = intval($get_amount*100);
                $charge_type = null;
                $charge_product = null;
                $payment_id = $event->resource->id;
                $payment_time = $event->resource->create_time;
                $payment_time = strtotime($payment_time);
                $coupon_code = null;
                // IF IS SINGLE PAYMENT
                if( isset($event->resource->parent_payment) ) {
                    $parent_id = $event->resource->parent_payment;
                    try {
                        $payment_details = PayPal\Api\Payment::get($parent_id, $apiContext);
                        if( isset($payment_details->transactions) && isset($payment_details->transactions[0]->custom) ) {
                            $custom_meta = explode('-', $payment_details->transactions[0]->custom);
                            $charge_type = $custom_meta[0];
                            $charge_product = $custom_meta[1];
                            if( isset($custom_meta[2]) && ! empty($custom_meta[2]) ) {
                                $coupon_code = $custom_meta[2];
                            }
                        }
                    } catch (Exception $ex) {
                    }
                }

                // IF IS BILLING AGREEMENT PAYMENT
                if( isset($event->resource->billing_agreement_id) ) {
                    $subscription_id = $event->resource->billing_agreement_id;
                    $charge_type = "subscription";
                    $charge_product = 0;
                    try {
                        $payment_details = PayPal\Api\Agreement::get($subscription_id, $apiContext);
                    } catch (Exception $ex) {
                    }
                }

                if( ! empty($payment_details) ) {
                    $payer_id = $payment_details->payer->payer_info->payer_id;
                    $paypal_user = rvs_get_paypal_customer($payer_id);
                    if(!empty($paypal_user)) {
                        $user_id = intval($paypal_user->ID);
                    }
                }

                if(!empty($user_id)) {
                    if( ! empty($subscription_id) ) {
                        $wp_user = new WP_User($user_id);
                        $wpvs_customer = new WPVS_Customer($wp_user);
                        $paypal_agreement = $wpvs_customer->get_paypal_agreement($subscription_id, $test_mode);
                        if( ! empty($paypal_agreement) && isset($paypal_agreement['discount']) ) {
                            $coupon_code = $paypal_agreement['discount']['id'];
                        }
                    }
                    $wpvs_payment_manager = new WPVS_Payment_Manager($payment_id, $test_mode);
                    $wpvs_payment_manager->add_new_payment($payment_time, 'paypal', $user_id, $payer_id, $charge_amount, $charge_type, $charge_product, false, $coupon_code);
                    if( ! empty($subscription_id) ) {
                        $subscription_state = $payment_details->state;
                        $new_renewal = null;
                        if( isset($payment_details->agreement_details->next_billing_date) ) {
                            $new_renewal = $payment_details->agreement_details->next_billing_date;
                        } else {
                            if( $payment_details->state == "Pending" && isset($payment_details->start_date) ) {
                                $agreement_starts = strtotime($payment_details->start_date);
                                if( $agreement_starts > $wpvs_current_time ) {
                                    $new_renewal = $payment_details->start_date;
                                    $subscription_state = 'Active';
                                }
                            }
                        }
                        wpvs_update_paypal_user_subscription_status($user_id, $subscription_id, $subscription_state, $new_renewal, $test_mode);
                    }
                }

            }

            if($event->event_type == "BILLING.SUBSCRIPTION.CANCELLED" || $event->event_type == "BILLING.SUBSCRIPTION.SUSPENDED" || $event->event_type == "BILLING.SUBSCRIPTION.RE-ACTIVATED" || $event->event_type == "BILLING.SUBSCRIPTION.UPDATED") {
                $agreement = $event->resource;
                $payer_id = $agreement->payer->payer_info->payer_id;
                $subscription_id = $agreement->id;
                $subscription_state = $agreement->state;
                $new_renewal = null;
                if( isset($agreement->agreement_details->next_billing_date)) {
                    $new_renewal = $agreement->agreement_details->next_billing_date;
                } else {
                    if( $agreement->state == "Pending" && isset($agreement->start_date) ) {
                        $agreement_starts = strtotime($agreement->start_date);
                        if( $agreement_starts > $wpvs_current_time ) {
                            $new_renewal = $agreement->start_date;
                            $subscription_state = 'Active';
                        }
                    }
                }
                if(!empty($payer_id)) {
                    $paypal_user = rvs_get_paypal_customer($payer_id);
                    if(!empty($paypal_user)) {
                        $user_id = intval($paypal_user->ID);
                    }

                    if(!empty($user_id)) {
                        wpvs_update_paypal_user_subscription_status($user_id, $subscription_id, $subscription_state, $new_renewal, $test_mode);
                    }
                }
            }
        }
    }
}
add_action('init', 'rvs_paypal_event_listener');
