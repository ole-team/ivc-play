<?php

require_once(RVS_MEMBERS_BASE_DIR . '/paypal/autoload.php');

use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\PayerInfo;
use PayPal\Api\Plan;
use PayPal\Api\AgreementStateDescriptor;

// SINGLE PAYMENTS
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

// WEBHOOKS
use PayPal\Api\Webhook;

//REFUNDS

use PayPal\Api\Refund;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;


add_action( 'wp_ajax_rvs_remove_billing_agreement_admin', 'rvs_remove_billing_agreement_admin' );

function rvs_remove_billing_agreement_admin() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_live_mode;
        global $rvs_paypal_settings;

        if((isset($_REQUEST['planid']) && $_REQUEST['planid'] != "") && (isset($_REQUEST['paypalid'])
         && $_REQUEST['paypalid'] != "") && isset($_REQUEST['userid'])
         && $_REQUEST['userid'] != "") {

            $plan_id = $_REQUEST['planid'];
            $paypal_plan = $_REQUEST['paypalid'];
            $user_id = $_REQUEST['userid'];
            $new_status = $_REQUEST['new_status'];
            $run_wpvs_delete_functions = false;
            $remove_membership = null;

            if($rvs_live_mode == "on") {
                $client_id = $rvs_paypal_settings['live_client_id'];
                $client_secret = $rvs_paypal_settings['live_client_secret'];
                $api_mode = array('mode' => 'live');
                $paypal_key = "paypal";
                $get_memberships = 'rvs_user_memberships';
            } else {
                $client_id = $rvs_paypal_settings['sandbox_client_id'];
                $client_secret = $rvs_paypal_settings['sandbox_client_secret'];
                $api_mode = array('mode' => 'sandbox');
                $paypal_key = "paypal_test";
                $get_memberships = 'rvs_user_memberships_test';
            }

            // MAKE SURE PLAN WAS FOUND

            if(empty($plan_id) || empty($paypal_plan)) {
                status_header("400");
                _e('Could not find that plan id', 'vimeo-sync-memberships');
                exit;
            }

            // CHECK THAT CLIENT AND SECRET ARE VALID

            if($client_id == "" || $client_secret == "") {
                status_header("400");
                _e('You have PayPal enabled. However, you have not entered your Client ID or Client Secret key.', 'vimeo-sync-memberships');
                exit;
            } else {

                $apiContext = new \PayPal\Rest\ApiContext(
                    new \PayPal\Auth\OAuthTokenCredential(
                        $client_id,  // ClientID
                        $client_secret  // ClientSecret
                    )
                );
                $apiContext->setConfig($api_mode);
                //Create an Agreement State Descriptor, explaining the reason to suspend.
                $agreementStateDescriptor = new AgreementStateDescriptor();

                $user_memberships = get_user_meta($user_id, $get_memberships, true);

                try {
                    $agreement = Agreement::get($paypal_plan, $apiContext);
                    if($new_status == 'cancel' || $new_status == 'delete' ) {
                        $agreementStateDescriptor->setNote("Cancelling the agreement");

                        if($agreement->state != "Cancelled") {
                            if($agreement->state == "Active" || $agreement->state == "Suspended") {
                                $agreement->cancel($agreementStateDescriptor, $apiContext);
                            } else {
                                $agreementStateDescriptor->setNote("Suspending the agreement");
                                $agreement->suspend($agreementStateDescriptor, $apiContext);
                                $agreementStateDescriptor->setNote("Cancelling the agreement");
                                $agreement->cancel($agreementStateDescriptor, $apiContext);
                            }
                        }

                        if(!empty($user_memberships)) {
                            foreach($user_memberships as $key => &$access) {
                                if ($access["id"] == $paypal_plan) {
                                    if($new_status == 'delete' ) {
                                        unset($user_memberships[$key]);
                                        $run_wpvs_delete_functions = true;
                                        $remove_membership = $access;
                                    } else {
                                        $access['status'] = 'Cancelled';
                                    }
                                    break;
                                }
                            }
                        }

                    }

                    if($new_status == 'suspend') {
                        $agreementStateDescriptor->setNote("Suspending the agreement");
                        if($agreement->state != "Suspended") {
                            $agreement->suspend($agreementStateDescriptor, $apiContext);
                        }

                        if(!empty($user_memberships)) {
                            foreach($user_memberships as $key => &$access) {
                                if ($access["id"] == $paypal_plan) {
                                    $access['status'] = 'Suspended';
                                    break;
                                }
                            }
                        }
                    }

                    if($new_status == 'reactivate') {
                        $agreementStateDescriptor->setNote("Re-activating the agreement");
                        if($agreement->state != "Active") {
                            $agreement->reActivate($agreementStateDescriptor, $apiContext);
                        }

                        if(!empty($user_memberships)) {
                            foreach($user_memberships as $key => &$access) {
                                if ($access["id"] == $paypal_plan) {
                                    $access['status'] = 'Active';
                                    break;
                                }
                            }
                        }

                    }

                } catch (Exception $ex) {
                    $paypal_error = wpvs_handle_paypal_error($ex);
                    if( ! empty($paypal_error['name']) ) {
                        if( $paypal_error['name'] == "INVALID_PROFILE_ID" ) {
                            if( ! empty($user_memberships) ) {
                                foreach($user_memberships as $key => &$access) {
                                    if ($access["id"] == $paypal_plan) {
                                        unset($user_memberships[$key]);
                                        $run_wpvs_delete_functions = true;
                                        $remove_membership = $access;
                                        break;
                                    }
                                }
                            }
                        }
                    } else {
                        $paypal_error = wpvs_handle_paypal_error($ex);
                        rvs_exit_ajax($paypal_error['message'], 400);
                    }
                }
                update_user_meta($user_id, $get_memberships, $user_memberships);
                if( $run_wpvs_delete_functions && ! empty($remove_membership) ) {
                    wpvs_do_after_delete_membership_action($user_id, $remove_membership);
                }

            }
        } else {
            $wpvs_error_message = __('Missing Plan ID.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_rvs_paypal_create_webhooks', 'rvs_paypal_create_webhooks' );

function rvs_paypal_create_webhooks() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_live_mode;
        global $rvs_paypal_settings;
        $live_webhooks_created = false;
        $test_webhooks_created = false;
        $repeat_test_live = 2;
        while($repeat_test_live > 0) {
            if($repeat_test_live == 2) {
                $client_id = $rvs_paypal_settings['live_client_id'];
                $client_secret = $rvs_paypal_settings['live_client_secret'];
                $api_mode = array('mode' => 'live');
                $webhook_id_setup = 'rvs-paypal-webhook-id';
            }

            if($repeat_test_live == 1) {
                $client_id = $rvs_paypal_settings['sandbox_client_id'];
                $client_secret = $rvs_paypal_settings['sandbox_client_secret'];
                $api_mode = array('mode' => 'sandbox');
                $webhook_id_setup = 'rvs-paypal-webhook-id-test';
            }

            // CHECK THAT CLIENT AND SECRET ARE VALID

            if($client_id == "" || $client_secret == "") {
                status_header("400");
                _e('You have PayPal enabled. However, you have not entered your Client ID or Client Secret key.', 'vimeo-sync-memberships');
                exit;
            } else {
                $apiContext = new \PayPal\Rest\ApiContext(
                    new \PayPal\Auth\OAuthTokenCredential(
                        $client_id,  // ClientID
                        $client_secret  // ClientSecret
                    )
                );
                $apiContext->setConfig($api_mode);
                $paypal_webhook_id = get_option($webhook_id_setup);

                $website_url = get_bloginfo('url');
                $webhook_url = $website_url.'/?rvs-paypal-listener=paypal';

                if(stripos($website_url, 'https') === false) {
                    rvs_exit_ajax("PayPal requires an SSL (HTTPS) URL for Webhooks.", 400);
                }

                if(empty($paypal_webhook_id)) {
                    $paypal_webhook = new \PayPal\Api\Webhook();
                    $paypal_webhook->setUrl($webhook_url);
                    $webhookEventTypes = array();
                    $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
                      '{
                        "name":"PAYMENT.SALE.COMPLETED"
                      }'
                    );

                    $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
                      '{
                        "name":"BILLING.SUBSCRIPTION.CANCELLED"
                      }'
                    );

                    $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
                      '{
                        "name":"BILLING.SUBSCRIPTION.SUSPENDED"
                      }'
                    );

                    $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
                      '{
                        "name":"BILLING.SUBSCRIPTION.RE-ACTIVATED"
                      }'
                    );

                    $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
                      '{
                        "name":"BILLING.SUBSCRIPTION.UPDATED"
                      }'
                    );

                    $paypal_webhook->setEventTypes($webhookEventTypes);

                    try {
                        $updated_webhook = $paypal_webhook->create($apiContext);
                        update_option($webhook_id_setup, $updated_webhook->id);
                    } catch (PayPal\Exception\PayPalConnectionException $ex) {
                        $paypal_error = wpvs_handle_paypal_error($ex);
                        if($paypal_error['message'] == "Webhook URL already exists") {
                            try {
                                $current_webhooks = \PayPal\Api\Webhook::getAll($apiContext);
                                if(!empty($current_webhooks)) {
                                $paypal_webhooks = $current_webhooks->webhooks;
                                    foreach($paypal_webhooks as $webhook) {
                                        if(strpos($webhook->url, "rvs-paypal-listener=paypal")) {
                                            if(empty($paypal_webhook_id)) {
                                                update_option($webhook_id_setup, $webhook->id);
                                            }
                                            break;
                                        }
                                    }
                                }
                            } catch (Exception $ex) {
                                $paypal_error = wpvs_handle_paypal_error($ex);
                                rvs_exit_ajax($paypal_error['message'], 400);
                            }
                        } else {
                            rvs_exit_ajax($paypal_error['message'], 400);
                        }
                    }
                } else {
                    try {
                        $paypal_webhook = \PayPal\Api\Webhook::get($paypal_webhook_id, $apiContext);
                        if(!empty($paypal_webhook)) {
                            $update_events_string = '[{"name":"PAYMENT.SALE.COMPLETED"},{"name":"BILLING.SUBSCRIPTION.CANCELLED"},{"name":"BILLING.SUBSCRIPTION.SUSPENDED"},{"name":"BILLING.SUBSCRIPTION.RE-ACTIVATED"},{"name":"BILLING.SUBSCRIPTION.UPDATED"}]';

                            $webhook_patch_request = new \PayPal\Api\PatchRequest();
                            // MAKE SURE URL IS CORRECT
                            if($paypal_webhook->url != $webhook_url) {
                                $url_webhook_patch = new \PayPal\Api\Patch();
                                $url_webhook_patch->setOp("replace")
                                    ->setPath("/url")
                                    ->setValue($webhook_url);
                                $webhook_patch_request->addPatch($url_webhook_patch);
                            }

                            // UPDATE TYPES
                            if(!empty($update_events_string)) {
                                $events_patch = new \PayPal\Api\Patch();
                                $events_patch->setOp("replace")
                                ->setPath("/event_types")
                                ->setValue(json_decode($update_events_string));
                                $webhook_patch_request->addPatch($events_patch);
                            }

                            try {
                                $updated_webhook = $paypal_webhook->update($webhook_patch_request, $apiContext);
                                update_option($webhook_id_setup, $updated_webhook->id);
                            } catch (Exception $ex) {
                                $paypal_error = wpvs_handle_paypal_error($ex);
                                rvs_exit_ajax($paypal_error['message'], 400);
                            }

                        } else {
                            update_option($webhook_id_setup, null);
                            rvs_exit_ajax("Webhooks Missing. Please try again.", 400);
                        }
                    } catch (Exception $ex) {
                        $paypal_error = wpvs_handle_paypal_error($ex);
                        if($paypal_error['message'] == "Resource id is invalid") {
                            update_option($webhook_id_setup, null);
                        }
                        rvs_exit_ajax($paypal_error['message'], 400);
                    }

                }
                if($repeat_test_live == 2) {
                    $live_webhooks_created = true;
                }
                if($repeat_test_live == 1) {
                    $test_webhooks_created = true;
                }
            }
            $repeat_test_live--;
        }
        $live_test_errors = "";
        if( ! $live_webhooks_created ) {
            $live_test_errors .= "Live webhooks were not created.<br>";
        }

        if( ! $test_webhooks_created ) {
            $live_test_errors .= "Test webhooks were not created.";
        }

        if( ! empty($live_test_errors) ) {
            rvs_exit_ajax($live_test_errors, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_reset_paypal_webhooks', 'wpvs_reset_paypal_webhooks' );

if( ! function_exists('wpvs_reset_paypal_webhooks') ) {
function wpvs_reset_paypal_webhooks() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_live_mode;
        if( $rvs_live_mode == "on" ) {
            $webhook_id_setup = 'rvs-paypal-webhook-id';
        } else {
            $webhook_id_setup = 'rvs-paypal-webhook-id-test';
        }
        update_option($webhook_id_setup, null);
    }
    wp_die();
}
add_action( 'wp_ajax_rvs_paypal_refund_ajax_request', 'rvs_paypal_refund_ajax_request' );
}

function rvs_paypal_refund_ajax_request() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_currency;
        global $rvs_live_mode;
        $test_payment = false;
        if($rvs_live_mode == "off") {
            $test_payment = true;
        }
       $paypal_config = rvs_create_paypal_config();
        // CHECK THAT CLIENT AND SECRET ARE VALID

        if( empty($paypal_config['client']) || empty($paypal_config['secret']) ) {
            $wpvs_error_message = __('You have PayPal enabled. However, you have not entered your Client ID or Client Secret key.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        } else {

            $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    $paypal_config['client'],  // ClientID
                    $paypal_config['secret']  // ClientSecret
                )
            );
            $apiContext->setConfig($paypal_config['api']);
            if(isset($_POST["payment_id"]) && $_POST["payment_id"] != null) {
                $payment_id = $_POST["payment_id"];
                $wpvs_payment_manager = new WPVS_Payment_Manager($payment_id, $test_payment);
                if( $wpvs_payment_manager->payment_exists() ) {
                    $amount = intval($wpvs_payment_manager->amount);
                    $amount = number_format($amount/100, 2);
                    $paypal_amount = new Amount();
                    $paypal_amount->setTotal($amount)
                      ->setCurrency($rvs_currency);

                    $refund = new RefundRequest();
                    $refund->setAmount($paypal_amount);

                    // GET SALE ID
                    $sale = new Sale();
                    $sale->setId($payment_id);

                    try {
                        $refunded_sale = $sale->refund($refund, $apiContext);
                        if($refunded_sale->state == "completed") {
                            $refunded = wpvs_do_db_refund_payment($payment_id);
                        }
                    } catch (Exception $ex) {
                        $paypal_error = wpvs_handle_paypal_error($ex);
                        rvs_exit_ajax($paypal_error['message'], 400);
                    }
                    $refunded = wpvs_do_db_refund_payment($payment_id);
                    wpvs_reverse_product_purchase($payment_id, 'paypal');
                } else {
                    $error_message = __('Payment not found.', 'wp-video-subscriptions');
                    rvs_exit_ajax($message, 400);
                }

            } else {
                $error_message = __('No sale id provided', 'wp-video-subscriptions');
                rvs_exit_ajax($message, 400);
            }
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}


add_action( 'wp_ajax_wpvs_get_paypal_customer_payments_admin', 'wpvs_get_paypal_customer_payments_admin' );

function wpvs_get_paypal_customer_payments_admin() {
    if( wpvs_secure_admin_ajax() && wp_verify_nonce($_GET['wpvs_admin_nonce'], 'wpvs-admin-nonce') && isset($_GET['user_id']) && ! empty($_GET['user_id']) ) {
        $user_id = intval($_GET['user_id']);
        $payments_offset = $_GET['offset'];
        if( empty($payments_offset) ) {
            $payments_offset = 0;
        }
        global $rvs_live_mode;
        $test_payments = true;
        if($rvs_live_mode == "on") {
            $test_payments = false;
        }
        $found_payments = array();
        $customer_paypal_payments = wpvs_get_customer_db_paypal_payments($user_id, $test_payments, $payments_offset);
        if( ! empty($customer_paypal_payments) ) {
            foreach($customer_paypal_payments as $payment) {
                $payment_date = date( 'M d, Y', $payment->time );
                if( empty($payment->type) ) {
                    $payment_type = __('unknown', 'vimeo-sync-memberships');
                } else {
                    if( $payment->type == 'subscription' ) {
                        $payment_type = __('Renewal', 'vimeo-sync-memberships');
                    }
                    if( $payment->type == 'rental' ) {
                        if( ! empty($payment->productid) ) {
                            $admin_url_str = 'post.php?post='.$payment->productid.'&action=edit';
                            $edit_product_link = admin_url($admin_url_str);
                            $product_title = get_the_title($payment->productid);
                        }
                        $payment_type = '<small>'.__('(Rental)', 'vimeo-sync-memberships').'</small> <a href="'.$edit_product_link.'">'.$product_title.'</a>';
                    }
                    if( $payment->type == 'purchase' ) {
                        if( ! empty($payment->productid) ) {
                            $admin_url_str = 'post.php?post='.$payment->productid.'&action=edit';
                            $edit_product_link = admin_url($admin_url_str);
                            $product_title = get_the_title($payment->productid);
                        }
                        $payment_type = '<small>'.__('(Purchase)', 'vimeo-sync-memberships').'</small> <a href="'.$edit_product_link.'">'.$product_title.'</a>';
                    }
                    if( $payment->type == 'termpurchase' ) {
                        if( ! empty($payment->productid) ) {
                            $admin_url_str = 'term.php?taxonomy=rvs_video_category&tag_ID='.$payment->productid.'&post_type=rvs_video';
                            $edit_product_link = admin_url($admin_url_str);
                            $product_term = get_term_by('id', intval($payment->productid), 'rvs_video_category');
                            $product_title = $product_term->name;
                        }
                        $payment_type = '<small>'.__('(Access to)', 'vimeo-sync-memberships').'</small> <a href="'.$edit_product_link.'">'.$product_title.'</a>';
                    }
                }


                $add_payment = array(
                    'amount' => $payment->amount,
                    'paymentid' => $payment->paymentid,
                    'time' => $payment_date,
                    'type' => $payment_type,
                    'refunded' => intval($payment->refunded)
                );
                $found_payments[] =  $add_payment;
            }

        }
        echo json_encode($found_payments);
    } else {
        rvs_exit_ajax(__('You are not allowed to perform this request', 'vimeo-sync-memberships'), 400);
    }
    wp_die();
}
