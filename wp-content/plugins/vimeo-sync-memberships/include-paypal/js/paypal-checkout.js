jQuery(document).ready( function() {
    if(jQuery('.wpvs-paypal-subscription').length > 0) {
        wpvs_create_paypal_agreement_token();
    }

    if(jQuery('.wpvs-paypal-payment').length > 0) {
        if(jQuery('#wpvs-paypal-purchase').length > 0) {
            wpvs_create_paypal_payment_token();
        }

        if(jQuery('#wpvs-paypal-rental').length > 0) {
            wpvs_create_paypal_payment_token();
        }
    }

    jQuery('body').delegate('.wpvs-paypal-free', 'click', function() {
        if( wpvscheckout.coupon_code && wpvscheckout.coupon_code != "" ) {
            var video_id = jQuery(this).data('videoid');
            wpvs_execute_free_paypal_payment(video_id);
        }
    });
});

function wpvs_render_paypal_agreement_button(token, plan_id, coupon_code) {
    if( jQuery('#wpvs-paypal-button .paypal-button').length > 0 ) {
        jQuery('#wpvs-paypal-button .paypal-button').remove();
    }
    jQuery('#rvs-paypal-loading').hide();
    paypal.Button.render({
        env: paypalajax.env,
        style: {
            size: 'medium'
        },
        payment: function(resolve, reject) {
            resolve(token);
        },
        onAuthorize: function(data) {
            wpvs_execute_paypal_agreement(data.paymentToken, plan_id, wpvscheckout.redirect_success, coupon_code);
        },
        onCancel: function(data) {}
    }, '#wpvs-paypal-button');
}

function wpvs_create_paypal_agreement_token() {
    if( jQuery('#wpvs-paypal-button .paypal-button').length > 0 ) {
        jQuery('#wpvs-paypal-button .paypal-button').remove();
    }
    jQuery('#rvs-paypal-loading').show();
    var plan_id = wpvscheckout.product_id;
    var current_url = window.location.href;
    jQuery.ajax({
        url: paypalajax.url,
        type: "POST",
        data: {
            'action': 'rvs_create_billing_agreement',
            'paypal_plan_id': plan_id,
            'current_url': current_url,
            'coupon_code': wpvscheckout.coupon_code
        },
        success:function(response) {
            var ec_agreement_token = jQuery.parseJSON(response);
            ec_agreement_token = ec_agreement_token.link.split('token=');
            var agreement_token = ec_agreement_token[1];
            wpvs_render_paypal_agreement_button(agreement_token, plan_id, wpvscheckout.coupon_code);
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_execute_paypal_agreement(token, plan_id, redirect_url, coupon_code) {
    show_rvs_updating(paypalajax.rvsmessage.processing+"...");
    jQuery.ajax({
        url: paypalajax.url,
        type: "POST",
        data: {
            'action': 'rvs_execute_billing_agreement',
            'paypal_token': token,
            'plan_id': plan_id,
            'coupon_code': coupon_code
        },
        success:function(response) {
            if(redirect_url != null && redirect_url != '') {
                window.location.href = redirect_url
            } else {
                window.location.href = response;
            }

        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_render_paypal_payment_button(payment_id) {
    jQuery('#rvs-paypal-loading').hide();
    var purchase_button_location = '#wpvs-paypal-purchase';
    if( wpvscheckout.purchase_type == 'rental') {
        purchase_button_location = '#wpvs-paypal-rental';
    }
    paypal.Button.render({
        env: paypalajax.env,
        style: {
            size: 'medium'
        },
        payment: function(resolve, reject) {
            resolve(payment_id);
        },
        onAuthorize: function(data) {
            wpvs_execute_paypal_payment(data.paymentToken, data.paymentID, data.payerID);
        },
        onCancel: function(data) {}

    }, purchase_button_location);
}

function wpvs_create_paypal_payment_token() {
    jQuery('#wpvs-paypal-payment').find('.paypal-button').remove();
    jQuery('#rvs-paypal-loading').show();
    var purchase_type = wpvscheckout.purchase_type;
    var product_id = wpvscheckout.product_id;
    jQuery.ajax({
        url: paypalajax.url,
        type: "POST",
        data: {
            'action': 'wpvs_create_single_paypal_payment',
            'paypal_post_id':  wpvscheckout.product_id,
            'type': wpvscheckout.purchase_type,
            'coupon_code': wpvscheckout.coupon_code
        },
        success:function(paymentid) {
            if(paymentid == 'free') {
                jQuery('#rvs-paypal-loading').hide();
                if( purchase_type == 'purchase' || purchase_type == 'termpurchase') {
                    jQuery('#wpvs-paypal-purchase').html('<label class="rvs-button rvs-primary-button wpvs-paypal-free" data-purchasetype="'+purchase_type+'" data-videoid="'+product_id+'">Free Purchase</label>');
                }
                if( purchase_type == 'rental') {
                    jQuery('#wpvs-paypal-rental').html('<label class="rvs-button rvs-primary-button wpvs-paypal-free" data-purchasetype="'+purchase_type+'" data-videoid="'+product_id+'">Free Purchase</label>');
                }

            } else {
                wpvs_render_paypal_payment_button(paymentid);
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_execute_paypal_payment(token, payment_id, payer_id) {
    show_rvs_updating(paypalajax.rvsmessage.processing+"...");
    jQuery.ajax({
        url: paypalajax.url,
        type: "POST",
        data: {
            'action': 'wpvs_execute_paypal_payment',
            'paypal_token': token,
            'payment_id': payment_id,
            'payer_id': payer_id,
            'video_id': wpvscheckout.product_id,
            'purchase_type': wpvscheckout.purchase_type,
            'coupon_code': wpvscheckout.coupon_code
        },
        success:function(response) {
            window.location.href = window.location.href + '?rvsppsuccess=true';
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_execute_free_paypal_payment(video_id) {
    show_rvs_updating(paypalajax.rvsmessage.processing+"...");
    jQuery.ajax({
        url: paypalajax.url,
        type: "POST",
        data: {
            'action': 'wpvs_execute_free_paypal_purchase',
            'video_id': video_id,
            'purchase_type': wpvscheckout.purchase_type
        },
        success:function(response) {
            window.location.href = response;
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}
