<?php
/**
* WP Video Memberships PayPal API Config
*/

require_once(RVS_MEMBERS_BASE_DIR . '/paypal/autoload.php');

class WPVS_PayPal_API_Config {

    protected $api_mode;
    protected $client_id;
    protected $client_secret;

    public function __construct($api_mode) {
        global $rvs_paypal_settings;
        $this->api_mode = $api_mode;
        if( $api_mode == 'live' ) {
            $this->client_id = $rvs_paypal_settings['live_client_id'];
            $this->client_secret = $rvs_paypal_settings['live_client_secret'];
        } else {
            $this->client_id = $rvs_paypal_settings['sandbox_client_id'];
            $this->client_secret = $rvs_paypal_settings['sandbox_client_secret'];
        }
    }

    // SET TO live OR sandbox
    public function set_api_mode($api_mode) {
        global $rvs_paypal_settings;
        $this->api_mode = $api_mode;
        if( $this->api_mode == 'live' ) {
            $this->client_id = $rvs_paypal_settings['live_client_id'];
            $this->client_secret = $rvs_paypal_settings['live_client_secret'];
        } else {
            $this->client_id = $rvs_paypal_settings['sandbox_client_id'];
            $this->client_secret = $rvs_paypal_settings['sandbox_client_secret'];
        }
    }

    public function get_paypal_config() {
        return array('client' => $this->client_id, 'secret' => $this->client_secret, 'api' => $this->api_mode);
    }
}

/**
* WP Video Memberships PayPal Plan Creator
*/

class WPVS_PayPal_Plan_Creator {

    public $plan_id;
    public $name;
    public $amount;
    public $description;
    public $short_description;
    public $frequency;
    public $frequency_interval;
    public $cycles;
    public $trial_frequency;
    public $trial_frequency_interval;
    public $trial_amount;
    public $paypal_plan;
    public $type;
    public $parent_plan_id;

    public function __construct($plan_id, $name, $amount, $description, $short_description, $frequency, $frequency_interval, $cycles = "0", $trial_frequency, $trial_frequency_interval, $trial_amount = 0, $paypal_plan, $type = "infinite", $parent_plan_id = null) {
        $this->plan_id = $plan_id;
        $this->name = $name;
        $this->amount = $amount;
        $this->description = $description;
        $this->short_description = $short_description;
        $this->frequency = $frequency;
        $this->frequency_interval = $frequency_interval;
        $this->cycles = $cycles;
        $this->trial_frequency = $trial_frequency;
        $this->trial_frequency_interval = $trial_frequency_interval;
        $this->trial_amount = $trial_amount;
        $this->paypal_plan = $paypal_plan;
        $this->type = $type;
        $this->parent_plan_id = $parent_plan_id;
    }

    public function create_paypal_billing_plan() {
        global $rvs_paypal_settings;
        global $rvs_currency;
        $max_fail_attempts = get_option('rvs_paypal_max_attempts', "0");
        if( ! empty($this->parent_plan_id) ) {
            $redirect_plan_id = $this->parent_plan_id;
        } else {
            $redirect_plan_id = $this->plan_id;
        }

        if( ! empty($this->short_description) ) {
            $checkout_description = $this->short_description;
        } else {
            $checkout_description = $this->description;
        }

        if( $rvs_currency == "JPY" ) {
            $plan_amount = number_format($this->amount/100, 0);
        } else {
            $plan_amount = number_format($this->amount/100, 2, ".", "");
        }

        $trial_amount = $this->trial_amount;

        if( ! empty($trial_amount) && $trial_amount > 0) {
            $trial_amount = number_format($this->trial_amount/100, 2, ".", "");
        }

        // CREATE RETURN AND REDIRECT LINKS
        $rvs_paypal_return = get_option('rvs_payment_page');
        $rvs_paypal_link = get_permalink($rvs_paypal_return);

        // CHECK PERMALINKS
        if(strpos($rvs_paypal_link, '?page_id')) {
            $rvs_paypal_link .= '&';
        } else {
            $rvs_paypal_link .= '?';
        }

        $rvs_payment_page = esc_attr( get_option('rvs_payment_page'));
        $rvs_payment_return = get_permalink($rvs_payment_page);

        // CHECK PERMALINKS
        if(strpos($rvs_payment_return, '?page_id')) {
            $rvs_payment_return .= '&';
        } else {
            $rvs_payment_return .= '?';
        }

        $created_success = false;
        $error_message = "";
        $paypal_error = null;

        $create_both_live_and_test = 2;

        while($create_both_live_and_test > 0) {

            if($create_both_live_and_test == 2) {
                $client_id = $rvs_paypal_settings['live_client_id'];
                $client_secret = $rvs_paypal_settings['live_client_secret'];
                $api_mode = array('mode' => 'live');
                $paypal_key = "paypal";
            }

            if($create_both_live_and_test == 1) {
                $client_id = $rvs_paypal_settings['sandbox_client_id'];
                $client_secret = $rvs_paypal_settings['sandbox_client_secret'];
                $api_mode = array('mode' => 'sandbox');
                $paypal_key = "paypal_test";
            }

            if( empty($client_id) || empty($client_secret) ) {
                if($create_both_live_and_test == 2) {
                    $error_message = __('You have PayPal enabled. However, you have not entered your live Client ID or Client Secret key.', 'vimeo-sync-memberships');
                } else if($create_both_live_and_test == 1) {
                    $error_message = __('You have PayPal enabled. However, you have not entered your sandbox Client ID or Client Secret key.', 'vimeo-sync-memberships');
                } else {
                    $error_message = __('You have PayPal enabled. However, you have not entered your Client ID or Client Secret key.', 'vimeo-sync-memberships');
                }
            } else {

                $apiContext = new \PayPal\Rest\ApiContext(
                    new \PayPal\Auth\OAuthTokenCredential(
                        $client_id,  // ClientID
                        $client_secret  // ClientSecret
                    )
                );
                $apiContext->setConfig($api_mode);
                // CREATE NEW PLAN OBJECT

                $new_paypal_plan = new PayPal\Api\Plan();
                $new_paypal_plan->setName($this->name)->setDescription($checkout_description)->setType($this->type);

                // CREATE PAYMENT DEFINITION
                $payment_definition = new PayPal\Api\PaymentDefinition();
                $payment_currency = new PayPal\Api\Currency(array(
                    'value' => $plan_amount,
                    'currency' => $rvs_currency
                ));

                $payment_definition->setName('Regular Payments')->setType('REGULAR')->setAmount($payment_currency)->setFrequency($this->frequency)->setFrequencyInterval($this->frequency_interval)->setCycles($this->cycles);

                // Preset TAX definition
                $paypal_tax_amount = new PayPal\Api\Currency(array('value' => '0.00', 'currency' => $rvs_currency));
                $paypal_charge_model = new PayPal\Api\ChargeModel();
                $paypal_charge_model->setId('wpvspaypalagreementtax')->setType('TAX')->setAmount($paypal_tax_amount);
                try{
                    $payment_definition->setChargeModels(array($paypal_charge_model));
                } catch (Exception $ex) {
                    $created_success = false;
                    $paypal_error = wpvs_handle_paypal_error($ex);
                }

                $set_payment_definitions = array($payment_definition);

                /*
                * CREATE TRIAL PERIOD IF SET
                * Update version 4.7.1: Do not create trial period for primary plans as they are automatically added via set start time
                */

                if( ! empty($this->parent_plan_id) && ! empty($this->trial_frequency_interval) && $this->trial_frequency != "none") {
                    $trial_definition = new PayPal\Api\PaymentDefinition();
                    $trial_currency = new PayPal\Api\Currency(array('value' => $trial_amount, 'currency' => $rvs_currency));
                    $trial_definition->setName('Trial')->setType('TRIAL')->setFrequency($this->trial_frequency)->setFrequencyInterval("1")->setCycles($this->trial_frequency_interval)->setAmount($trial_currency);
                    try {
                        $trial_definition->setChargeModels(array($paypal_charge_model));
                    } catch (Exception $ex) {
                        $created_success = false;
                        $paypal_error = wpvs_handle_paypal_error($ex);
                    }
                    $set_payment_definitions[] = $trial_definition;
                }

                //CREATE MERCHANT PREFERENCES

                $merchantPreferences = new PayPal\Api\MerchantPreferences();
                $merchant_currency = new PayPal\Api\Currency(array('value' => 0, 'currency' => $rvs_currency));
                $merchantPreferences->setReturnUrl($rvs_paypal_link.'success=true&planid='.$redirect_plan_id)->setCancelUrl($rvs_payment_return.'id='.$redirect_plan_id)->setAutoBillAmount("yes")->setInitialFailAmountAction("CONTINUE")->setMaxFailAttempts($max_fail_attempts)->setSetupFee($merchant_currency);

                $new_paypal_plan->setPaymentDefinitions($set_payment_definitions);
                $new_paypal_plan->setMerchantPreferences($merchantPreferences);


                // Create Plan
                try {
                    $created_plan = $new_paypal_plan->create($apiContext);
                    $this->paypal_plan[$paypal_key] = array('plan_id' => $created_plan->getId());
                    $created_success = true;
                    try {
                        $patch = new PayPal\Api\Patch();
                        $value = new PayPal\Common\PayPalModel('{"state":"ACTIVE"}');
                        $patch->setOp('replace')->setPath('/')->setValue($value);
                        $patchRequest = new PayPal\Api\PatchRequest();
                        $patchRequest->addPatch($patch);
                        $created_plan->update($patchRequest, $apiContext);
                        $plan = PayPal\Api\Plan::get($created_plan->getId(), $apiContext);
                        $created_success = true;
                    } catch (PayPal\Exception\PayPalConnectionException $ex) {
                        $paypal_error = wpvs_handle_paypal_error($ex);
                        if( isset( $paypal_error['name']) ) {
                            $error_message .= $paypal_error['name'].': ';
                        }
                        if( isset( $paypal_error['message']) ) {
                            $error_message .= $paypal_error['message'];
                        }
                        die($ex);
                    } catch (Exception $ex) {
                        $created_success = false;
                        $paypal_error = wpvs_handle_paypal_error($ex);
                    }
                } catch (PayPal\Exception\PayPalConnectionException $ex) {
                    $paypal_error = wpvs_handle_paypal_error($ex);
                } catch (Exception $ex) {
                    $paypal_error = wpvs_handle_paypal_error($ex);
                }

                if( ! empty($paypal_error) ) {
                    if( isset( $paypal_error['name']) ) {
                        $error_message .= $paypal_error['name'].': ';
                    }
                    if( isset( $paypal_error['message']) ) {
                        $error_message .= $paypal_error['message'];
                    }
                    $create_both_live_and_test = 0;
                }
            }
            $create_both_live_and_test--;
        }
        return array('created' => $created_success, 'error' => $error_message, 'updated_plan' => $this->paypal_plan);
    }
}

/**
* WP Video Memberships PayPal Plan Manager
*/

class WPVS_PayPal_Plan_Manager extends WPVS_PayPal_API_Config {

    public $plan_id;

    public function __construct($api_mode, $plan_id) {
        parent::__construct($api_mode);
        $this->plan_id = $plan_id;
    }

    public function delete_paypal_billing_plan() {
        $error_message = "";
        $plan_deleted = false;
        $apiContext = new PayPal\Rest\ApiContext(
            new PayPal\Auth\OAuthTokenCredential(
                $this->client_id,
                $this->client_secret
            )
        );
        $apiContext->setConfig(array('mode' => $this->api_mode) );
        try {
            $paypal_plan = PayPal\Api\Plan::get($this->plan_id, $apiContext);
            $result = $paypal_plan->delete($apiContext);
            $plan_deleted = true;
        } catch (Exception $ex) {
            $paypal_error = wpvs_handle_paypal_error($ex);
        }
        if( ! empty($paypal_error) ) {
            if( isset( $paypal_error['name']) ) {
                $error_message .= $paypal_error['name'].': ';
            }
            if( isset( $paypal_error['message']) ) {
                $error_message .= $paypal_error['message'];
            }
            $create_both_live_and_test = 0;
        }
        return array( 'deleted' => $plan_deleted, 'error' => $error_message);
    }
}
