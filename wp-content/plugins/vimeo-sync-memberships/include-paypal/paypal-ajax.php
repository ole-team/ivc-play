<?php
require_once(RVS_MEMBERS_BASE_DIR . '/paypal/autoload.php');

use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\PayerInfo;
use PayPal\Api\Plan;
use PayPal\Api\AgreementStateDescriptor;

// SINGLE PAYMENTS
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

add_action( 'wp_ajax_rvs_create_billing_agreement', 'rvs_create_billing_agreement' );

function rvs_create_billing_agreement() {
    global $rvs_live_mode;
    global $rvs_current_user;
    global $wpvs_membership_plans_list;
    global $rvs_currency;
    if(isset($_POST['paypal_plan_id']) && ! empty($_POST['paypal_plan_id']) ) {
        $wpvs_require_billing_info = get_option('wpvs_require_billing_information', 0);
        $wpvs_customer = new WPVS_Customer($rvs_current_user);
        $plan_id = $_POST['paypal_plan_id'];
        $membership_plan = null;
        $wpvs_coupon = null;
        $coupon_plan_args = null;
        $trial_period = array();
        $applied_taxes = array();
        $set_start_time = null;
        $reduced_setup_fee = false;
        if($rvs_live_mode == "on") {
            $paypal_key = "paypal";
        } else {
            $paypal_key = "paypal_test";
        }

        $wpvs_membership_manager = new WPVS_Membership_Plan($plan_id);

        if( $wpvs_membership_manager->membership_plan_exists() ) {
            $membership_plan = $wpvs_membership_manager->get_plan();
            if( $wpvs_membership_manager->has_trial_period() ) {
                $trial_period['frequency'] = $wpvs_membership_manager->trial_frequency;
                $trial_period['period'] = $wpvs_membership_manager->trial_frequency_interval;
            }
        } else {
            $wpvs_error_message = __('You must create a plan first.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }

        if(isset($_POST['coupon_code']) && ! empty($_POST['coupon_code']) ) {
            $coupon_id = $_POST['coupon_code'];
            $wpvs_coupon = new WPVS_Coupon_Code($coupon_id);
            $create_new_paypal_coupon_code_plan = false;
            $parent_membership_has_trial = $wpvs_membership_manager->has_trial_period();

            if($wpvs_coupon->duration == 'once') {
                if( $parent_membership_has_trial ) {
                    // create new Billing Plan with first payment discount after trial period
                    $create_new_paypal_coupon_code_plan = true;
                }
                $reduced_setup_fee = true;
            }

            if( $wpvs_coupon->duration == 'repeating' ) {
                if( $parent_membership_has_trial ) {
                    // create new Billing Plan with repeating coupon discount payments for duration of coupon
                    $create_new_paypal_coupon_code_plan = true;
                } else {
                    // membership does not have trial period
                    $reduced_setup_fee = true;
                    if( $wpvs_coupon->month_duration == 1 ) {
                        // reduce first payment to coupon amount and start membership regular pricing 1 month from now
                        $set_start_time = '+1 months';
                    } else {
                        $create_new_paypal_coupon_code_plan = true;
                    }
                }
            }

            if( $wpvs_coupon->duration == 'forever' ) {
                // create new Billing Plan with discount payments forever
                $create_new_paypal_coupon_code_plan = true;
            }

            if( $create_new_paypal_coupon_code_plan ) {
                $membership_plan = $wpvs_membership_manager->get_paypal_coupon_plan($coupon_id);
                if( empty( $membership_plan ) ) {
                    $membership_plan = $wpvs_membership_manager->create_paypal_coupon_plan($wpvs_coupon, $wpvs_membership_manager->has_trial_period());
                } else {
                    if( ! isset($membership_plan['paypal']) ) {
                        $membership_plan = $wpvs_membership_manager->create_paypal_coupon_plan($wpvs_coupon, $wpvs_membership_manager->has_trial_period());
                    }
                }
            }
        }

        if( ! empty($membership_plan) ) {

            $plan_name = $membership_plan['name'];
            $plan_description = $membership_plan['description'];
            if( isset($membership_plan['short_description']) && ! empty($membership_plan['short_description']) ) {
                $checkout_description = $membership_plan['short_description'];
            } else {
                $checkout_description = $plan_description;
            }
            $plan_price = intval($membership_plan['amount']);
            $plan_amount = number_format($plan_price/100, 2);
            $plan_interval = $membership_plan['interval'];
            $plan_interval_count = $membership_plan['interval_count'];
            $paypal_plan_id = $membership_plan[$paypal_key]['plan_id'];
            if($plan_interval_count > 1) {
                $plan_interval = $plan_interval_count . ' ' .$plan_interval.'s';
            }
            if( empty($set_start_time) ) {
                $set_start_time = '+'.$plan_interval_count . ' ' .$plan_interval.'s';
            }
        }

        // MAKE SURE PLAN WAS FOUND
        if(empty($plan_id) || empty($paypal_plan_id)) {
            status_header("400");
            _e('Could not find that plan id', 'vimeo-sync-memberships');
            exit;
        }

        $paypal_config = rvs_create_paypal_config();

        $apiContext = new PayPal\Rest\ApiContext(
            new PayPal\Auth\OAuthTokenCredential(
                $paypal_config['client'],  // ClientID
                $paypal_config['secret']  // ClientSecret
            )
        );
        $apiContext->setConfig($paypal_config['api']);

        // SET INITIAL PAYMENT
        if($reduced_setup_fee && ! empty($wpvs_coupon) ) {
            $setup_fee_amount = wpvs_calculate_coupon_amount($plan_price, $wpvs_coupon->coupon_id);
        } else {
            $setup_fee_amount = $plan_price;
        }

        // SET TRIAL PERIOD IF THERE IS ONE AND CURRENT USER HAS NOT ALREADY HAD ONE
        if( ! empty($trial_period) && ! wpvs_user_has_completed_trial($rvs_current_user->ID, $plan_id) ) {

            $setup_fee_amount = '0.00';

            if($trial_period['frequency'] == "day") {
                $set_start_time = '+'.$trial_period['period'].' days';
            }

            if($trial_period['frequency'] == "week") {
                $set_start_time = '+'.$trial_period['period'].' weeks';
            }

            if($trial_period['frequency'] == "month") {
                $set_start_time = '+'.$trial_period['period'].' months';
            }
        }
        // APPLY TAXES
        if( $wpvs_require_billing_info && ! empty($wpvs_customer->billing_state) && ! empty($wpvs_customer->billing_country) ) {
            $wpvs_tax_rate = new WPVS_Tax_Rate();
            $jurisdiction_rates = $wpvs_tax_rate->get_tax_rates_by_jurisdiction($wpvs_customer->billing_state, $wpvs_customer->billing_country);
            if( ! empty($jurisdiction_rates) ) {
                foreach($jurisdiction_rates as $tax_rate) {
                    $get_tax_rate_amount = $wpvs_tax_rate->calculate_tax_rate_amount($plan_price, $tax_rate);
                    if( ! empty($get_tax_rate_amount) ) {
                        $add_to_tax_list = (object) array(
                            'tax_name' => $tax_rate->display_name,
                            'amount'   => $get_tax_rate_amount->amount,
                            'decimal_amount'   => $get_tax_rate_amount->decimal_amount,
                            'inclusive' => $tax_rate->inclusive
                        );
                        if( ! empty($setup_fee_amount) ) {
                            $setup_tax_rate_amount = $wpvs_tax_rate->calculate_tax_rate_amount($setup_fee_amount, $tax_rate);
                            $add_to_tax_list->setup_tax_amount = $setup_tax_rate_amount->amount;
                        }
                        $applied_taxes[] = $add_to_tax_list;
                    }
                }
            }
        }

        if( ! empty($applied_taxes) ) {
            $add_tax_amounts_due = 0;
            foreach($applied_taxes as $apply_tax) {
                if( ! $apply_tax->inclusive ) {
                    if( isset($apply_tax->setup_tax_amount) ) {
                        $setup_fee_amount += $apply_tax->setup_tax_amount;
                    }
                }
            }
        }

        if( $setup_fee_amount <= 0 ) {
            $setup_fee_amount = '0.00';
        } else {
            $setup_fee_amount = number_format(($setup_fee_amount/100), 2, '.', '');
        }

        $start_time = date('Y-m-d\TH:i:s\Z', strtotime($set_start_time, current_time('timestamp', 1)));
        $agreement = new Agreement();
        $agreement->setName($plan_name)->setDescription($checkout_description)->setStartDate($start_time);

        $paypal_plan = PayPal\Api\Plan::get($paypal_plan_id, $apiContext);

        $plan = new Plan();
        $plan->setId($paypal_plan_id);
        $agreement->setPlan($plan);

        // Add Payer
        $payer = new Payer();
        $payer->setPaymentMethod('paypal')->setPayerInfo(new PayerInfo(array('email' => $rvs_current_user->user_email)));
        $agreement->setPayer($payer);

        // SEND TO ACCOUNT PAGE
        $rvs_account_page = get_option('rvs_account_page');
        $paypal_success_url = get_permalink($rvs_account_page);
        if(isset($_POST['current_url']) && ! empty($_POST['current_url']) ) {
            $paypal_return_url = $_POST['current_url'];
        } else {
            $rvs_payment_page = esc_attr( get_option('rvs_payment_page'));
            $paypal_return_url = get_permalink($rvs_payment_page);
        }

        // CHECK PERMALINKS
        if(strpos($paypal_return_url, '?page_id') || strpos($paypal_return_url, '?')) {
            $paypal_return_url .= '&';
            $paypal_success_url .= '&';
        } else {
            $paypal_return_url .= '?';
            $paypal_success_url .= '?';
        }

        //OVERIDE MERCHANT PREFERENCES
        try {
            $merchantPreferences = new PayPal\Api\MerchantPreferences();
            $setup_fee = new PayPal\Api\Currency(array('value' => $setup_fee_amount, 'currency' => $rvs_currency));
            $merchantPreferences->setReturnUrl($paypal_success_url.'success=true&planid='.$plan_id)->setCancelUrl($paypal_return_url)->setSetupFee($setup_fee);
            $agreement->setOverrideMerchantPreferences($merchantPreferences);
        } catch (Exception $ex) {
            $paypal_error = wpvs_handle_paypal_error($ex);
            rvs_exit_ajax($paypal_error['message'], 400);
        }

        // Set taxes
        $plan_payment_definitions = $paypal_plan->getPaymentDefinitions();

        $tax_charge_models = array();
        if( ! empty($plan_payment_definitions) ) {
            foreach($plan_payment_definitions as $check_definition_tax) {
                $add_tax_amounts_due = 0;
                // apply taxes to discount payments
                if( $check_definition_tax->type == 'TRIAL' ) {
                    if( ! empty($jurisdiction_rates) ) {
                        $trial_definition_amount = number_format($check_definition_tax->amount->value*100, 0);
                        foreach($jurisdiction_rates as $tax_rate) {
                            $discount_tax_rate_amount = $wpvs_tax_rate->calculate_tax_rate_amount($trial_definition_amount, $tax_rate);
                            if( ! empty($discount_tax_rate_amount) ) {
                                if( ! $tax_rate->inclusive ) {
                                    $add_tax_amounts_due += $discount_tax_rate_amount->decimal_amount;
                                }
                            }
                        }
                    }
                } else {
                    if( ! empty($applied_taxes) ) {
                        foreach($applied_taxes as $apply_tax) {
                            if( ! $apply_tax->inclusive ) {
                                $add_tax_amounts_due += $apply_tax->decimal_amount;
                            }
                        }
                    }
                }
                if( isset($check_definition_tax->charge_models) ) {
                    foreach($check_definition_tax->charge_models as $plan_charge_model) {
                        if($plan_charge_model->type == 'TAX') {
                            $tax_charge_models[] = (object) array(
                                'id' => $plan_charge_model->id,
                                'amount' => $add_tax_amounts_due
                            );
                        }
                    }
                }
            }
        }
        if( ! empty($tax_charge_models) ) {
            foreach($tax_charge_models as $add_tax_model) {
                try {
                    $paypal_tax_amount = new PayPal\Api\Currency(array('value' => number_format($add_tax_model->amount, 2), 'currency' => $rvs_currency));
                    $paypal_taxes_charge_model = new PayPal\Api\OverrideChargeModel();
                    $paypal_taxes_charge_model->setChargeId($add_tax_model->id)->setAmount($paypal_tax_amount);
                    $agreement->addOverrideChargeModel($paypal_taxes_charge_model);
                } catch (Exception $ex) {
                    $paypal_error = wpvs_handle_paypal_error($ex);
                    rvs_exit_ajax($paypal_error['message'], 400);
                }
            }
        }

        // CREATE AGREEMENT
        try {
            $agreement = $agreement->create($apiContext);
            $approval_link = $agreement->getApprovalLink();
            $return_array = json_encode(array('link' => $approval_link));
            echo $return_array;
        } catch (Exception $ex) {
            $paypal_error = wpvs_handle_paypal_error($ex);
            rvs_exit_ajax($paypal_error['message'], 400);
        }

    }
    wp_die();
}

add_action( 'wp_ajax_rvs_execute_billing_agreement', 'rvs_execute_billing_agreement' );

function rvs_execute_billing_agreement() {
    global $rvs_paypal_settings;
    global $rvs_live_mode;
    global $rvs_current_user;
    global $wpvs_membership_plans_list;
    if(is_user_logged_in()) {
        if (isset($_POST['paypal_token']) && ! empty($_POST['paypal_token']) && isset($_POST['plan_id']) && ! empty($_POST['plan_id'])) {

            $plan_id = $_POST['plan_id'];
            $token = $_POST['paypal_token'];
            $wpvs_coupon = null;
            $trial_period_ends = null;
            $new_agreement_ends = null;
            $wpvs_customer = new WPVS_Customer($rvs_current_user);
            $wpvs_membership_manager = new WPVS_Membership_Plan($plan_id);
            $wpvs_current_time = current_time('timestamp', 1);
            if( $wpvs_membership_manager->membership_plan_exists() ) {
                $membership_plan = $wpvs_membership_manager->get_plan();
            } else {
                $wpvs_error_message = __('That plan does not exist.', 'vimeo-sync-memberships');
                rvs_exit_ajax($wpvs_error_message, 400);
            }

            if(isset($_POST['coupon_code']) && ! empty( $_POST['coupon_code']) ) {
                $coupon_id = $_POST['coupon_code'];
                $wpvs_coupon = new WPVS_Coupon_Code($coupon_id);
                if( ! empty($wpvs_coupon) ) {
                    $coupon_added = false;
                    if( ! $wpvs_membership_manager->has_trial_period() && ( $wpvs_coupon->duration == 'repeating' && $wpvs_coupon->month_duration == 1 ) ) {
                        $coupon_added = true;
                    }

                    if( ! $coupon_added && ( $wpvs_coupon->duration == 'forever' || $wpvs_coupon->duration == 'repeating' ) ) {
                        $membership_plan = $wpvs_membership_manager->get_paypal_coupon_plan($coupon_id);
                    }

                    if( $wpvs_membership_manager->has_trial_period() && $wpvs_coupon->duration == 'once' ) {
                        $membership_plan = $wpvs_membership_manager->get_paypal_coupon_plan($coupon_id);
                    }
                }
            }

            if( $wpvs_membership_manager->has_trial_period() && ! wpvs_user_has_completed_trial($rvs_current_user->ID, $plan_id)) {
                $trial_frequency = $wpvs_membership_manager->trial_frequency;
                $plan_trial = $wpvs_membership_manager->trial_frequency_interval;

                if($trial_frequency == "day") {
                    $future_start_time = '+'.$plan_trial.' days';
                }

                if($trial_frequency == "week") {
                    $future_start_time = '+'.$plan_trial.' weeks';
                }

                if($trial_frequency == "month") {
                    $future_start_time = '+'.$plan_trial.' months';
                }

                $trial_period_ends = strtotime($future_start_time, $wpvs_current_time);
            }

            $trial_complete = false;
            $price          = $membership_plan['amount'];
            $name           = $membership_plan['name'];
            $interval       = $membership_plan['interval'];
            $interval_count = $membership_plan['interval_count'];
            if( $wpvs_membership_manager->has_trial_period() ) {
                $trial_complete = true;
            }

            // CHECK IF TESTING OR LIVE
            if($rvs_live_mode == "on") {
                $paypal_meta_key = "paypal";
                $get_memberships = 'rvs_user_memberships';
                $get_payer_id = 'rvs_paypal_payer_id';
                $paypal_email = 'rvs_paypal_email';
                $test_member = false;
            } else {
                $paypal_meta_key = "paypal_test";
                $get_memberships = 'rvs_user_memberships_test';
                $get_payer_id = 'rvs_paypal_test_payer_id';
                $paypal_email = 'rvs_paypal_test_email';
                $test_member = true;
            }

            // GET USER MEMBERSHIPS

            $rvs_current_user_agreements = get_user_meta($rvs_current_user->ID, $get_memberships, true);

            if(!empty($rvs_current_user_agreements)) {
                foreach($rvs_current_user_agreements as $user_agreement) {
                    if($user_agreement['plan'] == $plan_id) {
                        rvs_exit_ajax(__('You are already subscribed to this plan.', 'vimeo-sync-memberships'), 400);
                    }
                }
            } else {
                $rvs_current_user_agreements = array();
            }

            $paypal_config = rvs_create_paypal_config();

            $apiContext = new PayPal\Rest\ApiContext(
                new PayPal\Auth\OAuthTokenCredential(
                    $paypal_config['client'],  // ClientID
                    $paypal_config['secret']  // ClientSecret
                )
            );
            $apiContext->setConfig($paypal_config['api']);

            $agreement = new \PayPal\Api\Agreement();
            try {
                $agreement->execute($token, $apiContext);
            } catch (Exception $ex) {
                $paypal_error = wpvs_handle_paypal_error($ex);
                rvs_exit_ajax($paypal_error['message'], 400);
            }

            try {
                $agreement = \PayPal\Api\Agreement::get($agreement->getId(), $apiContext);
                $agreement_id = $agreement->getId();

                $new_agreement_state = $agreement->state;
                if( isset($agreement->agreement_details->next_billing_date) ) {
                    $new_agreement_ends = $agreement->agreement_details->next_billing_date;
                } else {
                    if( $agreement->state == "Pending" && isset($agreement->start_date) ) {
                        $agreement_starts = strtotime($agreement->start_date);
                        if( $agreement_starts > $wpvs_current_time ) {
                            $new_agreement_ends = $agreement->start_date;
                            $new_agreement_state = 'Active';
                        }
                    }
                }

                if( empty($new_agreement_ends) ) {
                    $new_agreement_ends = current_time('mysql');
                }

                if( ! empty($trial_period_ends) ) {
                    $new_agreement_state = 'Active';
                    $trial_end_date = date('c', $trial_period_ends);
                    $new_agreement_ends = $trial_end_date;
                }

                $add_new_membership = array(
                    'plan' => $plan_id,
                    'id' => $agreement_id,
                    'amount' => $price,
                    'name' => $name,
                    'interval' => $interval,
                    'interval_count' => $interval_count,
                    'ends' => $new_agreement_ends,
                    'status' => $new_agreement_state,
                    'type' => $paypal_meta_key
                );

                if( ! empty($wpvs_coupon) ) {
                    $add_coupon_details = array(
                        'type' => 'paypal',
                        'id' => $wpvs_coupon->coupon_id,
                        'duration' => $wpvs_coupon->duration,
                        'duration_in_months' => $wpvs_coupon->month_duration,
                    );
                    if( $wpvs_coupon->type == 'amount' ) {
                        $add_coupon_details['amount_off'] = $wpvs_coupon->amount;
                    } else {
                        $add_coupon_details['percent_off'] = $wpvs_coupon->amount;
                    }
                    $add_new_membership['discount'] = $add_coupon_details;
                    wpvs_add_coupon_code_use($wpvs_coupon->coupon_id, $rvs_current_user->ID);
                }

                if( ! empty($trial_period_ends) ) {
                    $add_new_membership['in_trial'] = 1;
                }

                $wpvs_customer->add_membership($add_new_membership);

                // UPDATE USER PAYER ID
                $new_payer = $agreement->payer;
                $new_payer_id = $new_payer->payer_info->payer_id;
                update_user_meta($rvs_current_user->ID, $get_payer_id, $new_payer_id);

                // UPDATE USER PAYPAL EMAIL
                $new_paypal_email = $new_payer->payer_info->email;
                update_user_meta($rvs_current_user->ID, $paypal_email, $new_paypal_email);

                if( $trial_complete ) {
                    wpvs_user_completed_trial($rvs_current_user->ID, $plan_id);
                }

                // SEND TO ACCOUNT PAGE
                $rvs_account_page = get_option('rvs_account_page');
                $rvs_account_link = get_permalink($rvs_account_page);

                echo $rvs_account_link;
                wpvs_add_new_member($rvs_current_user->ID, $test_member);
            } catch (Exception $ex) {
                $paypal_error = wpvs_handle_paypal_error($ex);
                rvs_exit_ajax($paypal_error['message'], 400);
            }
        } else {
            status_header('400');
            rvs_exit_ajax(__("Oops! Looks like you're missing something", 'vimeo-sync-memberships'), 400);
        }
    } else {
        rvs_exit_ajax(__('You must be logged in.', 'vimeo-sync-memberships'), 400);
    }
    wp_die();
}

add_action( 'wp_ajax_rvs_remove_billing_agreement', 'rvs_remove_billing_agreement' );

function rvs_remove_billing_agreement() {
    global $rvs_live_mode;
    global $rvs_paypal_settings;
    global $rvs_current_user;

    if((isset($_REQUEST['planid']) && $_REQUEST['planid'] != "") && (isset($_REQUEST['paypalid'])
     && $_REQUEST['paypalid'] != "")) {

        $plan_id = $_REQUEST['planid'];
        $paypal_plan = $_REQUEST['paypalid'];
        $new_status = $_REQUEST['new_status'];
        $rvs_mail_text = "Cancelled";
        $run_wpvs_delete_functions = false;
        $remove_membership = null;

        if($rvs_live_mode == "on") {
            $client_id = $rvs_paypal_settings['live_client_id'];
            $client_secret = $rvs_paypal_settings['live_client_secret'];
            $api_mode = array('mode' => 'live');
            $paypal_key = "paypal";
            $get_memberships = 'rvs_user_memberships';
        } else {
            $client_id = $rvs_paypal_settings['sandbox_client_id'];
            $client_secret = $rvs_paypal_settings['sandbox_client_secret'];
            $api_mode = array('mode' => 'sandbox');
            $paypal_key = "paypal_test";
            $get_memberships = 'rvs_user_memberships_test';
        }

        // MAKE SURE PLAN WAS FOUND

        if(empty($plan_id) || empty($paypal_plan)) {
            status_header("400");
            _e('Could not find that plan id', 'vimeo-sync-memberships');
            exit;
        }

        // CHECK THAT CLIENT AND SECRET ARE VALID

        if($client_id == "" || $client_secret == "") {
            status_header("400");
            _e('You have PayPal enabled. However, you have not entered your Client ID or Client Secret key.', 'vimeo-sync-memberships');
            exit;
        } else {

            $apiContext = new \PayPal\Rest\ApiContext(
                new \PayPal\Auth\OAuthTokenCredential(
                    $client_id,  // ClientID
                    $client_secret  // ClientSecret
                )
            );
            $apiContext->setConfig($api_mode);
            //Create an Agreement State Descriptor, explaining the reason to suspend.
            $agreementStateDescriptor = new AgreementStateDescriptor();

            // GET USER MEMBERSHIPS

            $user_memberships = get_user_meta($rvs_current_user->ID, $get_memberships, true);
            try {
                $agreement = Agreement::get($paypal_plan, $apiContext);

                if($new_status == 'cancel' || $new_status == 'delete' ) {
                    $agreementStateDescriptor->setNote("Cancelling the agreement");

                    if($agreement->state != "Cancelled") {
                        if($agreement->state == "Active" || $agreement->state == "Suspended") {
                            $agreement->cancel($agreementStateDescriptor, $apiContext);
                        } else {
                            $agreementStateDescriptor->setNote("Suspending the agreement");
                            $agreement->suspend($agreementStateDescriptor, $apiContext);
                            $agreementStateDescriptor->setNote("Cancelling the agreement");
                            $agreement->cancel($agreementStateDescriptor, $apiContext);
                        }
                    }

                    if(!empty($user_memberships)) {
                        foreach($user_memberships as $key => &$access) {
                            if ($access["id"] == $paypal_plan) {
                                if($new_status == 'delete' ) {
                                    unset($user_memberships[$key]);
                                    $rvs_mail_text = "Deleted";
                                    $run_wpvs_delete_functions = true;
                                    $remove_membership = $access;
                                } else {
                                    $access['status'] = 'Cancelled';
                                }
                                break;
                            }
                        }
                    }

                }

                if($new_status == 'suspend') {
                    $agreementStateDescriptor->setNote("Suspending the agreement");
                    if($agreement->state != "Suspended") {
                        $agreement->suspend($agreementStateDescriptor, $apiContext);
                    }

                    if(!empty($user_memberships)) {
                        foreach($user_memberships as $key => &$access) {
                            if ($access["id"] == $paypal_plan) {
                                $access['status'] = 'Suspended';
                                break;
                            }
                        }
                    }
                }

                if($new_status == 'reactivate') {
                    $agreementStateDescriptor->setNote("Re-activating the agreement");
                    if($agreement->state != "Active") {
                        $agreement->reActivate($agreementStateDescriptor, $apiContext);
                    }

                    if(!empty($user_memberships)) {
                        foreach($user_memberships as $key => &$access) {
                            if ($access["id"] == $paypal_plan) {
                                $access['status'] = 'Active';
                                break;
                            }
                        }
                    }
                    $rvs_mail_text = "Reactivated";
                }

            } catch (Exception $ex) {
                $paypal_error = wpvs_handle_paypal_error($ex);
                if( ! empty($paypal_error['name']) && $paypal_error['name'] == "INVALID_PROFILE_ID") {
                    if( ! empty($user_memberships) ) {
                        foreach($user_memberships as $key => &$access) {
                            if ($access["id"] == $paypal_plan) {
                                unset($user_memberships[$key]);
                                $run_wpvs_delete_functions = true;
                                $remove_membership = $access;
                                break;
                            }
                        }
                    }

                } else {
                    if( ! empty($paypal_error['message']) ) {
                        $error_message = $paypal_error['message'];
                    }
                    rvs_exit_ajax($error_message, 400);
                }
            }
            update_user_meta($rvs_current_user->ID, $get_memberships, $user_memberships);
            if( $run_wpvs_delete_functions && ! empty($remove_membership) ) {
                wpvs_do_after_delete_membership_action($rvs_current_user->ID, $remove_membership);
            }
            if($new_status == 'delete' || $new_status == 'cancel' || $new_status == 'reactivate') {
                wpvs_send_email($rvs_mail_text, $rvs_current_user->ID, $plan_name);
            }
        }
    } else {
        status_header("400");
        _e('Missing plan id', 'vimeo-sync-memberships');
        exit;
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_create_single_paypal_payment', 'wpvs_create_single_paypal_payment' );

function wpvs_create_single_paypal_payment() {
    global $rvs_live_mode;
    global $rvs_paypal_settings;
    global $rvs_current_user;
    global $rvs_currency;
    if(isset($_POST['paypal_post_id']) && ! empty($_POST['paypal_post_id']) ) {
        $wpvs_require_billing_info = get_option('wpvs_require_billing_information', 0);
        $wpvs_customer = new WPVS_Customer($rvs_current_user);
        $purchase_type = $_POST['type'];
        $coupon_code = null;
        $applied_taxes = array();
        $jurisdiction_rates = array();
        if( $purchase_type == "termpurchase" ) {
            $product_id = intval($_POST['paypal_post_id']);
            $wpvs_term = get_term($product_id, 'rvs_video_category' );
            if( ! empty($wpvs_term) && ! is_wp_error($wpvs_term) ) {
                $video_price = get_term_meta($product_id, 'wpvs_category_purchase_price', true);
                $purchase_title = $wpvs_term->name;
                if( ! empty($wpvs_term->parent) ) {
                    $wpvs_parent_term = get_term(intval($wpvs_term->parent), 'rvs_video_category' );
                    if( ! empty($wpvs_parent_term) && ! is_wp_error($wpvs_parent_term) ) {
                        $purchase_title .= ' ('.$wpvs_parent_term->name.')';
                    }
                }
                $return_url = get_term_link($product_id, 'rvs_video_category');
            } else {
                rvs_exit_ajax(__('Could not find that video category.', 'vimeo-sync-memberships'), 400);
            }
        } else {
            $product_id = $_POST['paypal_post_id'];
            $purchase_title = get_the_title($product_id);
            $return_url = get_permalink($product_id);
            if($purchase_type == "purchase") {
                $video_price = get_post_meta( $product_id, '_rvs_onetime_price', true );
            }
            if($purchase_type == "rental") {
                $video_price = get_post_meta( $product_id, 'rvs_rental_price', true );
            }
        }

        // CHECK FOR COUPON CODE
        if( isset($_POST['coupon_code']) && ! empty($_POST['coupon_code']) ) {
            $coupon_code = $_POST['coupon_code'];
            $video_price = wpvs_calculate_coupon_amount($video_price, $coupon_code);
        }

        if($video_price <= 0) {
            echo 'free';
            exit;
        }

        // CHECK PERMALINKS
        if(strpos($return_url, '?page_id')) {
            $return_url .= '&';
        } else {
            $return_url .= '?';
        }

        // APPLY TAXES
        if( $wpvs_require_billing_info && ! empty($wpvs_customer->billing_state) && ! empty($wpvs_customer->billing_country) ) {
            $wpvs_tax_rate = new WPVS_Tax_Rate();
            $jurisdiction_rates = $wpvs_tax_rate->get_tax_rates_by_jurisdiction($wpvs_customer->billing_state, $wpvs_customer->billing_country);
            if( ! empty($jurisdiction_rates) ) {
                foreach($jurisdiction_rates as $tax_rate) {
                    $get_tax_rate_amount = $wpvs_tax_rate->calculate_tax_rate_amount($video_price, $tax_rate);
                    if( ! empty($get_tax_rate_amount) ) {
                        $applied_taxes[] = (object) array(
                            'tax_name' => $tax_rate->display_name,
                            'amount'   => $get_tax_rate_amount->amount,
                            'decimal_amount'   => $get_tax_rate_amount->decimal_amount,
                            'inclusive' => $tax_rate->inclusive
                        );
                    }
                }
            }
        }
        $rvs_payment_id = uniqid();
        if($rvs_live_mode == "on") {
            $paypal_key = "paypal";
        } else {
            $paypal_key = "paypal_test";
        }
        $paypal_config = rvs_create_paypal_config();
        if( empty($paypal_config['client']) || empty($paypal_config['secret']) ) {
            rvs_exit_ajax(__('You have PayPal enabled. However, you have not entered your Client ID or Client Secret key.', 'vimeo-sync-memberships'), 400);
        } else {

            $set_paypal_price = $video_price/100;
            $payment_amount = number_format($set_paypal_price,2,'.','');

            $apiContext = new PayPal\Rest\ApiContext(
                new PayPal\Auth\OAuthTokenCredential(
                    $paypal_config['client'],  // ClientID
                    $paypal_config['secret']  // ClientSecret
                )
            );
            $apiContext->setConfig($paypal_config['api']);
            $details = new Details();
            $details->setSubtotal($payment_amount);

            if( ! empty($applied_taxes)  ) {
                $payment_tax_amount = 0;
                foreach($applied_taxes as $apply_tax) {
                    if( ! $apply_tax->inclusive ) {
                        $payment_tax_amount += $apply_tax->decimal_amount;
                    }
                }
                if( ! empty($payment_tax_amount) ) {
                    $details->setTax($payment_tax_amount);
                    $payment_amount += $payment_tax_amount;
                }
            }

            $amount = new Amount();
            $amount->setCurrency($rvs_currency)->setTotal($payment_amount)->setDetails($details);

            if($purchase_type == "rental") {
                $expires = get_post_meta( $product_id, 'rvs_rental_expires', true );
                $interval = get_post_meta( $product_id, 'rvs_rental_type', true );
                $time_string = "+".$expires." ".$interval;
                $checkout_description = __('Access to', 'vimeo-sync-memberships') . ' ' . $purchase_title . ' ' .__('for', 'vimeo-sync-memberships'). ' ' . $rvs_currency . ' ' . $payment_amount . ' ('.$expires . ' ' . $interval.')';
            } else {
                $checkout_description = __('Access to', 'vimeo-sync-memberships') . ' ' . $purchase_title . ' ' .__('for', 'vimeo-sync-memberships'). ' ' . $rvs_currency . ' ' . $payment_amount;
            }

            $custom_meta = $purchase_type.'-'.$product_id;

            if( ! empty($coupon_code) ) {
                $custom_meta .= '-'.$coupon_code;
            }
            $transaction = new Transaction();
            $transaction->setAmount($amount)->setDescription($checkout_description)->setInvoiceNumber($rvs_payment_id)->setCustom($custom_meta);

            $redirectUrls = new RedirectUrls();
            $redirectUrls->setReturnUrl($return_url.'rvsppsuccess=true')->setCancelUrl($return_url.'rvsppsuccess=cancelled');

            $payer = new Payer();
            $payer->setPaymentMethod('paypal')->setPayerInfo(new PayerInfo(array('email' => $rvs_current_user->user_email)));

            $payment = new Payment();
            $payment->setIntent("sale")->setPayer($payer)->setRedirectUrls($redirectUrls)->setTransactions(array($transaction));

            // CREATE PAYMENT
            try {
                $payment->create($apiContext);
                echo $payment->id;
            } catch (Exception $ex) {
                $paypal_error = wpvs_handle_paypal_error($ex);
                rvs_exit_ajax($paypal_error['message'], 400);
            }
        }
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_execute_free_paypal_purchase', 'wpvs_execute_free_paypal_purchase' );

function wpvs_execute_free_paypal_purchase() {
    global $rvs_current_user;
    global $rvs_currency;
    if($rvs_current_user && isset($_POST['video_id']) && ! empty($_POST['video_id']) ) {
        $purchase_type = $_POST['purchase_type'];
        $product_id = intval($_POST['video_id']);
        if( $purchase_type == "termpurchase" ) {
            $wpvs_term = get_term($product_id, 'rvs_video_category' );
            if( ! empty($wpvs_term) && ! is_wp_error($wpvs_term) ) {
                $return_url = get_term_link($product_id, 'rvs_video_category');
            } else {
                rvs_exit_ajax(__('Could not find that video category.', 'vimeo-sync-memberships'), 400);
            }
        } else {
            $return_url = get_permalink($product_id);
        }

        if( ! empty($product_id) && ! empty($purchase_type) ) {
            $wpvs_customer = new WPVS_Customer($rvs_current_user);
            $wpvs_customer->add_product($product_id, $purchase_type);
            echo $return_url;
        }
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_execute_paypal_payment', 'wpvs_execute_paypal_payment' );

function wpvs_execute_paypal_payment() {
    global $rvs_live_mode;
    global $rvs_currency;
    $test_payment = false;
    if(is_user_logged_in()) {
        global $rvs_current_user;
        if (isset($_POST['paypal_token']) && ! empty($_POST['paypal_token']) && isset($_POST['video_id']) && ! empty($_POST['video_id'])) {
            $wpvs_require_billing_info = get_option('wpvs_require_billing_information', 0);
            $wpvs_customer = new WPVS_Customer($rvs_current_user);
            $applied_taxes = array();
            if($rvs_live_mode == "on") {
                $paypal_meta_key = "paypal";
                $get_payer_id = 'rvs_paypal_payer_id';
                $paypal_email = 'rvs_paypal_email';
                $test_member  = false;
            } else {
                $paypal_meta_key = "paypal_test";
                $get_payer_id = 'rvs_paypal_test_payer_id';
                $paypal_email = 'rvs_paypal_test_email';
                $test_payment = true;
                $test_member  = false;
            }

            $paypal_config = rvs_create_paypal_config();

            if( empty($paypal_config['client']) || empty($paypal_config['secret']) ) {
                status_header("400");
                _e('You have PayPal enabled. However, you have not entered your Client ID or Client Secret key.', 'vimeo-sync-memberships');
                exit;
            } else {

                $apiContext = new PayPal\Rest\ApiContext(
                    new PayPal\Auth\OAuthTokenCredential(
                        $paypal_config['client'],  // ClientID
                        $paypal_config['secret']  // ClientSecret
                    )
                );
                $apiContext->setConfig($paypal_config['api']);

                $token = $_POST['paypal_token'];
                $paymentId = $_POST['payment_id'];
                $payer_id = $_POST['payer_id'];
                $purchase_type = $_POST['purchase_type'];
                $product_id = null;
                if( $purchase_type == "termpurchase" ) {
                    $product_id = intval($_POST['video_id']);
                    $wpvs_term = get_term($product_id, 'rvs_video_category' );
                    if( ! empty($wpvs_term) && ! is_wp_error($wpvs_term) ) {
                        $video_price = get_term_meta($product_id, 'wpvs_category_purchase_price', true);
                        $purchase_title = $wpvs_term->name;
                        $return_url = get_term_link($product_id, 'rvs_video_category');
                    } else {
                        rvs_exit_ajax(__('Could not find that video category.', 'vimeo-sync-memberships'), 400);
                    }
                } else {
                    $product_id = intval($_POST['video_id']);
                    if($purchase_type == "purchase") {
                        $video_price = get_post_meta( $product_id, '_rvs_onetime_price', true );
                    }
                    if($purchase_type == "rental") {
                        $video_price = get_post_meta( $product_id, 'rvs_rental_price', true );
                    }
                }

                // CHECK FOR COUPON CODE
                if( isset($_POST['coupon_code']) && ! empty($_POST['coupon_code']) ) {
                    $coupon_code = $_POST['coupon_code'];
                    $video_price = wpvs_calculate_coupon_amount($video_price, $coupon_code);
                }

                $set_paypal_price = $video_price/100;
                $payment_amount = number_format($set_paypal_price,2,'.','');

                // Check customer location and taxes
                if( $wpvs_require_billing_info && ! empty($wpvs_customer->billing_state) && ! empty($wpvs_customer->billing_country)  ) {
                    $wpvs_tax_rate = new WPVS_Tax_Rate();
                    $jurisdiction_rates = $wpvs_tax_rate->get_tax_rates_by_jurisdiction($wpvs_customer->billing_state, $wpvs_customer->billing_country);
                    if( ! empty($jurisdiction_rates) ) {
                        foreach($jurisdiction_rates as $tax_rate) {
                            $get_tax_rate_amount = $wpvs_tax_rate->calculate_tax_rate_amount($payment_amount, $tax_rate);
                            if( ! empty($get_tax_rate_amount) ) {
                                $applied_taxes[] = (object) array(
                                    'tax_name' => $tax_rate->display_name,
                                    'amount'   => $get_tax_rate_amount->amount,
                                    'decimal_amount'   => $get_tax_rate_amount->decimal_amount,
                                    'inclusive' => $tax_rate->inclusive
                                );
                            }
                        }
                    }
                }

                $payment = PayPal\Api\Payment::get($paymentId, $apiContext);

                $execution = new PayPal\Api\PaymentExecution();
                $execution->setPayerId($payer_id);

                $transaction = new PayPal\Api\Transaction();
                $amount = new PayPal\Api\Amount();
                $details = new PayPal\Api\Details();

                $details->setSubtotal($payment_amount);

                // APPLY TAXES
                if( ! empty($applied_taxes)  ) {
                    $payment_tax_amount = 0;
                    foreach($applied_taxes as $apply_tax) {
                        if( ! $apply_tax->inclusive ) {
                            $payment_tax_amount += $apply_tax->decimal_amount;
                        }
                    }
                    if( ! empty($payment_tax_amount) ) {
                        $details->setTax($payment_tax_amount);
                        $payment_amount += $payment_tax_amount;
                    }
                }

                $amount->setCurrency($rvs_currency);
                $amount->setTotal($payment_amount);
                $amount->setDetails($details);
                $transaction->setAmount($amount);
                $execution->addTransaction($transaction);

                try {
                    $result = $payment->execute($execution, $apiContext);
                    try {
                        $payment = Payment::get($paymentId, $apiContext);
                        wpvs_add_product_to_customer($rvs_current_user->ID, $product_id, $purchase_type);
                        update_user_meta($rvs_current_user->ID, $get_payer_id, $payer_id);
                        $new_payer = $payment->payer;
                        $new_paypal_email = $new_payer->payer_info->email;
                        update_user_meta($rvs_current_user->ID, $paypal_email, $new_paypal_email);
                        wpvs_add_new_member($rvs_current_user->ID, $test_member);
                    } catch (Exception $ex) {
                        $paypal_error = wpvs_handle_paypal_error($ex);
                        rvs_exit_ajax($paypal_error['message'], 400);
                    }
                } catch (Exception $ex) {
                    $paypal_error = wpvs_handle_paypal_error($ex);
                    rvs_exit_ajax($paypal_error['message'], 400);
                }
            }


        } else {
            status_header('400');
            rvs_exit_ajax(__("Oops! Looks like you're missing something", 'vimeo-sync-memberships'), 400);
        }
    } else {
        rvs_exit_ajax(__('You must be logged in.', 'vimeo-sync-memberships'), 400);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_get_paypal_customer_payments', 'wpvs_get_paypal_customer_payments' );

function wpvs_get_paypal_customer_payments() {
    if( is_user_logged_in() && wp_verify_nonce($_POST['wpvs_paypal_nonce'], 'wpvs-paypal-nonce') ) {
        global $rvs_current_user;
        global $rvs_live_mode;
        $test_payments = true;
        if($rvs_live_mode == "on") {
            $test_payments = false;
        }
        $customer_paypal_payments = wpvs_get_customer_db_paypal_payments($rvs_current_user->ID, $test_payments);
        echo json_encode($customer_paypal_payments);
    } else {
        rvs_exit_ajax(__('You must be logged in', 'vimeo-sync-memberships'), 400);
    }
    wp_die();
}
