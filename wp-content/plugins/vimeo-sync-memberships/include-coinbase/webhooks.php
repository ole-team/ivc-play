<?php
use CoinbaseCommerce\ApiClient;
use CoinbaseCommerce\Resources\Charge;
use CoinbaseCommerce\Webhook;

function wpvs_coinbase_callback_listener() {
    if(isset($_GET['wpvs-coinbase-callback-listener']) && $_GET['wpvs-coinbase-callback-listener'] == 'cb') {
        global $wpvs_coinbase_manager;
        if( $wpvs_coinbase_manager->is_enabled() && ! empty($wpvs_coinbase_manager->get_webhook_secret()) ) {
            $wpvs_cb_secret = $wpvs_coinbase_manager->get_webhook_secret();
            $header_x_name = 'X-Cc-Webhook-Signature';
            $headers = getallheaders();
            $sig_header = isset($headers[$header_x_name]) ? $headers[$header_x_name] : null;
            $cb_payload = trim(file_get_contents('php://input'));
            try {
                $event = Webhook::buildEvent($cb_payload, $sig_header, $wpvs_cb_secret);
                //$event = trim(file_get_contents( ABSPATH.'coinbase-json/webhook.json'));
            
                $event = json_decode($cb_payload)->event;
                status_header(200);
                $wpvs_coinbase_payment = new \WPVS\Coinbase\WPVSCoinbasePayment();

                $charge_data     = $event->data;
                $charge_code     = $charge_data->code;
                $charge_timeline = $charge_data->timeline;
                $charge_payments = $charge_data->payments;
                $charge_meta     = $charge_data->metadata;
                $charge_user_id  = $charge_meta->user_id;
                $charge_id       = null;
                $charge_status   = null;
                $payment         = null;
                $local_currency  = null;
                $local_amount    = null;
                $coin_currency   = null;
                $coin_amount     = null;
                $latest_charge_status = array();
                
                if( ! empty($charge_payments) ) {
                    $payment = $charge_payments[0];
                    $local_currency = $payment->value->local->currency;
                    $local_amount = $payment->value->local->amount;
                    $coin_currency = $payment->value->crypto->currency;
                    $coin_amount = $payment->value->crypto->amount;
                }
                
                if( ! empty($charge_timeline) ) {
                    foreach($charge_timeline as $timeline) {
                        $latest_charge_status[] = array(
                            'time' => $timeline->time,
                            'status' => $timeline->status
                        );
                    }
                }
                
                $status_details = $wpvs_coinbase_payment->get_latest_charge_status($latest_charge_status);

                if( isset($status_details['status']) ) {
                    $charge_status = $status_details['status'];
                }
                
                if( empty($charge_status) ) {
                    // PAYMENT CONFIRMED OR RECEIVED AFTER EXPIRED
                    if( $event->type == 'charge:confirmed' || $event->type == 'charge:delayed' || $event->type == 'charge:resolved' ) {
                        $charge_status = 'CONFIRMED';
                    }
                    // PAYMENT FAILED
                    if( $event->type == 'charge:failed' ) {
                        $charge_status = 'UNRESOLVED';
                    }
                    if( $event->type == 'charge:pending' ) {
                        $charge_status = 'PENDING';
                    } 
                }
                
                // GET CHARGE ID
                ApiClient::init($wpvs_coinbase_manager->get_api_key());
                try {
                    $get_cb_charge = Charge::retrieve($charge_code);
                    if($get_cb_charge) {
                        $charge_id = $get_cb_charge->id;
                    } 
                } catch (Exception $e) {}
                
                $cb_payment = $wpvs_coinbase_payment->update_charge(
                    $charge_user_id, 
                    $charge_id,
                    $charge_code, 
                    $charge_status, 
                    $local_currency, 
                    $local_amount, 
                    $coin_currency, 
                    $coin_amount
                );
                
            } catch (\Exception $exception) {
                status_header(400);
                echo wp_json_encode(array('error' => $exception->getMessage()));
            }
            if( ! empty($cg_payment) ) {
                echo wp_json_encode(array('coinbasepaymentsuccess' => true, 'payment' => $cb_payment->id));
            } else {
                echo wp_json_encode(array('coinbasepaymentsuccess' => false, 'error' => __('Payment not found', 'vimeo-sync-memberships')));
            }
        }
        exit;
    }
}
add_action('template_redirect', 'wpvs_coinbase_callback_listener');