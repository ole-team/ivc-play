jQuery(document).ready( function() {
    var pending_payments_checked = false;
    if(jQuery('#wpvs-coinbase-subscription').length > 0) {
        jQuery('#wpvs-coinbase-subscription').click( function() {
            wpvs_create_coinbase_payment_token();
        });
    }

    if( jQuery('.wpvs-coinbase-paynow').length > 0 ) {
        jQuery('.wpvs-coinbase-paynow').click( function() {
            var pay_now_button = jQuery(this);
            var charge_id = pay_now_button.data('charge');
            var plan_id = pay_now_button.data('plan');
            wpvs_create_coinbase_renewal_token(charge_id, plan_id);
        });
    }

});

function wpvs_create_coinbase_payment_token() {
    show_rvs_updating(wpvscoinbase.wpvsmessage.processing+'...');
    var transaction_type = wpvscheckout.transaction_type;
    var purchase_type = wpvscheckout.purchase_type;

    var current_url = window.location.href;
    jQuery.ajax({
        url: wpvscoinbase.url,
        type: "POST",
        data: {
            'action': 'wpvs_create_coinbase_charge',
            'plan_id': wpvscheckout.product_id,
            'redirect_success': wpvscheckout.redirect_success,
            'redirect': current_url,
            'transaction_type': transaction_type,
            'purchase_type': purchase_type,
            'coupon_code': wpvscheckout.coupon_code
        },
        success:function(response) {
            var cb_payment_details = jQuery.parseJSON(response);
            if(cb_payment_details.redirect) {
                window.location.href = cb_payment_details.redirect;
                if(jQuery('#wpvs-coinbase-checkout').length > 0 ) {
                    jQuery('#wpvs-coinbase-checkout').attr('href', cb_payment_details.redirect);
                }
            }
            jQuery('#wpvs-updating-box').fadeOut('fast');
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_create_coinbase_renewal_token(charge_id, plan_id) {
    show_rvs_updating(wpvscoinbase.wpvsmessage.processing+'...');
    var current_url = window.location.href;
    jQuery.ajax({
        url: wpvscoinbase.url,
        type: "POST",
        data: {
            'action': 'wpvs_create_coinbase_charge',
            'plan_id': plan_id,
            'redirect': current_url,
            'transaction_type': 'subscription',
            'purchase_type': 'subscription',
            'renewal': true
        },
        success:function(response) {
            var cb_payment_details = jQuery.parseJSON(response);
            if(cb_payment_details.redirect) {
                window.location.href = cb_payment_details.redirect;
            } else {
                jQuery('#wpvs-updating-box').fadeOut('fast');
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}
