<?php

namespace WPVS\Coinbase;

class WPVSCoinbasePayment {

    public function __construct() {
    }

    public static function create($order_params, $test) {
        global $wpdb;
        if($test) {
            $table_name = $wpdb->prefix . 'wpvs_test_coinbase_payments';
        } else {
            $table_name = $wpdb->prefix . 'wpvs_coinbase_payments';
        }
        $check = $wpdb->insert(
            $table_name,
            $order_params
        );
    }

    public static function wpvs_delete_coinbase_payment($cb_id) {
        global $wpdb;
        global $rvs_live_mode;
        if($rvs_live_mode == "off") {
            $table_name = $wpdb->prefix . 'wpvs_test_coinbase_payments';
        } else {
            $table_name = $wpdb->prefix . 'wpvs_coinbase_payments';
        }
        $coinbase_payment_deleted = $wpdb->get_results("DELETE FROM $table_name WHERE id = '$cb_id'");
        return $coinbase_payment_deleted;
    }

    public static function wpvs_get_user_coinbase_payments($user_id, $type, $type_id, $purchase_type) {
        global $wpdb;
        global $rvs_live_mode;
        if($rvs_live_mode == "off") {
            $table_name = $wpdb->prefix . 'wpvs_test_coinbase_payments';
        } else {
            $table_name = $wpdb->prefix . 'wpvs_coinbase_payments';
        }

        if( $type == "subscription" ) {
            $coin_query = "SELECT * FROM $table_name WHERE userid = '$user_id' AND planid = '$type_id'";
        }
        if( $type == "purchase" ) {
            $coin_query = "SELECT * FROM $table_name WHERE userid = '$user_id' AND videoid = '$type_id'";
        }
        if( $type == "termpurchase" ) {
            $coin_query = "SELECT * FROM $table_name WHERE userid = '$user_id' AND termid = '$type_id'";
        }

        $coin_query .= " AND status != 'COMPLETED' AND purchase = '$purchase_type'";

        $coinbase_payments = $wpdb->get_results($coin_query);

        return $coinbase_payments;
    }

    public static function wpvs_get_user_coinbase_payments_by_id($user_id, $charge_id) {
        global $wpdb;
        global $rvs_live_mode;
        if($rvs_live_mode == "off") {
            $table_name = $wpdb->prefix . 'wpvs_test_coinbase_payments';
        } else {
            $table_name = $wpdb->prefix . 'wpvs_coinbase_payments';
        }

        $coin_query = "SELECT FROM $table_name WHERE userid = '$user_id' AND chargeid = '$charge_id' AND status != 'COMPLETED' AND status != 'CONFIRMED'";
        $wpdb->query($coin_query);
    }

    public function get_latest_charge_status($order_timeline = array()) {
        $last_updated = 0;
        $latest_status = null;
        if( ! empty($order_timeline) ) {
            foreach($order_timeline as $timeline) {
                $timeline_time = strtotime($timeline['time']);
                if( $timeline_time > $last_updated ) {
                    $last_updated = $timeline_time;
                    $latest_status = $timeline['status'];
                }
            }
        }
        return array('status' => $latest_status, 'time' => $last_updated);
    }

    public function update_charge($user_id, $charge_id, $code, $status, $local_currency, $local_amount, $coin_currency, $coin_amount) {
        global $wpdb;
        global $rvs_live_mode;
        $coinbase_payment = null;
        if($rvs_live_mode == "off") {
            $get_memberships = 'rvs_user_memberships_test';
            $coinbase_key = 'coinbase_test';
            $table_name = $wpdb->prefix . 'wpvs_test_coinbase_payments';
            $test_member = true;
        } else {
            $get_memberships = 'rvs_user_memberships';
            $coinbase_key = 'coinbase';
            $table_name = $wpdb->prefix . 'wpvs_coinbase_payments';
            $test_member = false;
        }
        $coinbase_payments = $wpdb->get_results("SELECT * FROM $table_name WHERE code = '$code' AND userid = '$user_id' AND chargeid = '$charge_id'");
        if( ! empty($coinbase_payments) ) {
            $coinbase_payment = $coinbase_payments[0];
            $cb_payment_type = $coinbase_payment->type;
            $purchase_type = $coinbase_payment->purchase;
            $wpvs_current_time = current_time('timestamp', 1);
            $remove_membership = null;
            if( $cb_payment_type == 'subscription' ) {
                $membership_id = $coinbase_payment->planid;
                $user_memberships = get_user_meta($user_id, $get_memberships, true);
                if( ! empty($user_memberships) ) {
                    foreach($user_memberships as $key => &$membership) {
                        if($membership["plan"] == $membership_id && $membership["type"] == $coinbase_key && $membership["id"] == $charge_id) {
                            if( isset($membership["trial_ends"]) && $membership["trial_ends"] > $wpvs_current_time ) {
                                exit;
                            } else {
                                if( $status == 'CANCELED' && $membership["ends"] < $wpvs_current_time ) {
                                    unset($user_memberships[$key]);
                                    $remove_membership = $membership;
                                } else {
                                    $membership["status"] = $status;
                                }
                                if( ! empty($coin_currency) ) {
                                    $membership["coin"] = $coin_currency;
                                }
                            }
                            break;
                        }
                    }
                }
                update_user_meta( $user_id, $get_memberships, $user_memberships);
                if( ! empty($remove_membership) ) {
                    wpvs_do_after_delete_membership_action($user_id, $remove_membership);
                }
            }

            if( $cb_payment_type == 'purchase' ) {
                $product_id = $coinbase_payment->videoid;
                if( $status == 'CONFIRMED' || $status == 'COMPLETED' ) {
                    wpvs_add_product_to_customer($user_id, $product_id, $purchase_type);
                    wpvs_send_user_has_video_access($user_id, $product_id, 'video', $coin_amount, $coin_currency);
                }
            }

            if( $cb_payment_type == 'termpurchase' ) {
                $product_id = $coinbase_payment->termid;
                if( $status == 'CONFIRMED' || $status == 'COMPLETED' ) {
                    wpvs_add_product_to_customer($user_id, $product_id, "termpurchase");
                    wpvs_send_user_has_video_access($user_id, $product_id, 'term', $coin_amount, $coin_currency);
                }
            }

            $refunded = 0;
            if( $status == 'refunded') {
                $refunded = 1;
            }
            $wpvs_cb_order_updates = array(
                'status' => $status,
                'refunded' => $refunded,
                'local_currency' => $local_currency,
                'local_amount' => $local_amount,
                'coin_amount' => $coin_amount,
                'coin_currency' => $coin_currency,
                'updated' => $wpvs_current_time
            );

            $wpvs_update_order = array(
                'chargeid' => $charge_id,
                'code' => $code,
                'userid' => $user_id
            );

            $wpvs_value_format = array('%s', '%d', '%s', '%s', '%s', '%s', '%s');

            $wpvs_update_format = array('%s', '%s', '%d');

            $wpdb->update($table_name, $wpvs_cb_order_updates, $wpvs_update_order, $wpvs_value_format, $wpvs_update_format);
        }
        return $coinbase_payment;
    }
}


class WPVSCoinbaseManager {

    private $coinbase_settings;

    public function __construct() {
        $this->init();
    }

    private function init() {
        $this->coinbase_settings = get_option('wpvs_coinbase_settings', array());
    }

    public function get_api_key() {
        return $this->api_key();
    }

    private function api_key() {
        if( isset($this->coinbase_settings['api_key']) ) {
            return $this->coinbase_settings['api_key'];
        } else {
            return null;
        }
    }

    public function get_webhook_secret() {
        return $this->webhook_secret();
    }

    private function webhook_secret() {
        if( isset($this->coinbase_settings['webhook_secret']) ) {
            return $this->coinbase_settings['webhook_secret'];
        } else {
            return null;
        }
    }

    public function is_enabled() {
        if( isset($this->coinbase_settings['enabled']) && $this->coinbase_settings['enabled'] != 0 ) {
            return true;
        } else {
            return false;
        }
    }

    public function get_accepted_currencies() {
        if( isset($this->coinbase_settings['accepted_currencies']) ) {
            return $this->coinbase_settings['accepted_currencies'];
        } else {
            return null;
        }
    }
}
