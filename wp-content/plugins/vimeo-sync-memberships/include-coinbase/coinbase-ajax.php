<?php

use CoinbaseCommerce\ApiClient;
use CoinbaseCommerce\Resources\Charge;

add_action( 'wp_ajax_wpvs_create_coinbase_charge', 'wpvs_create_coinbase_charge' );

function wpvs_create_coinbase_charge() {
    if( isset($_POST['transaction_type']) ) {
        global $rvs_live_mode;
        global $rvs_current_user;
        global $rvs_currency;
        global $wpvs_coinbase_manager;
        $wpvs_require_billing_info = get_option('wpvs_require_billing_information', 0);
        $wpvs_customer = new WPVS_Customer($rvs_current_user);
        $applied_taxes = array();
        $discount_price = null;
        if( empty($wpvs_coinbase_manager->get_api_key() ) ) {
            rvs_exit_ajax(__('Missing Coinbase API Key', 'vimeo-sync-memberships'), 400);
        }

        $selected_coin = null;
        $wpvs_return_order = null;
        $wpvs_coupon = null;
        $is_renewal = false;
        $redirect_plan_trial = false;
        $wpvs_current_time = current_time('timestamp', 1);

        if($rvs_live_mode == "on") {
            $coinbase_key = "coinbase";
            $test_payment = false;
		} else {
            $coinbase_key = "coinbase_test";
			$test_payment = true;
		}
        if( isset($_POST['renewal']) ) {
            $is_renewal = true;
        }
        $transaction_type = $_POST['transaction_type'];
        $purchase_type = $_POST["purchase_type"];
        if( $transaction_type == 'subscription' ) {
            $wpvs_membership_list = get_option('rvs_membership_list');
            $membership_id = $_POST['plan_id'];
            $item_id = $membership_id;
            if( ! empty($wpvs_membership_list) && ! empty($membership_id) ) {
                foreach($wpvs_membership_list as $plan) {
                    if($plan['id'] == $membership_id) {
                        $price = $plan['amount'];
                        $name = $plan['name'];
                        $interval = $plan['interval'];
                        $interval_count = $plan['interval_count'];
                        $plan_description = $plan['description'];
                        if( isset($plan['short_description']) && ! empty($plan['short_description']) ) {
                            $checkout_description = $plan['short_description'];
                        } else {
                            $checkout_description = $plan_description;
                        }
                        if(isset($plan['trial_frequency'])) {
                            $trial_frequency = $plan['trial_frequency'];
                            $plan_trial = $plan['trial'];
                        }
                        break;
                    }
                }
            }
        }

        if( $transaction_type == 'purchase' ) {
            $video_id = $_POST['plan_id'];
            $item_id = $video_id;
            $name = get_the_title($video_id);
            if( $purchase_type == "purchase" ) {
                $price = get_post_meta($video_id, '_rvs_onetime_price', true );
                $checkout_description = 'Access to ' . $name;
            }

            if( $purchase_type == "rental" ) {
                $price = get_post_meta($video_id, 'rvs_rental_price', true );
                $expires = get_post_meta( $video_id, 'rvs_rental_expires', true );
                $interval = get_post_meta( $video_id, 'rvs_rental_type', true );
                $time_string = "+".$expires." ".$interval;
                $checkout_description = 'Access to ' . $name . ' ('.$expires . ' ' . $interval.')';
            }

            if( $purchase_type == 'termpurchase' ) {
                $wpvs_term_id = intval($_POST['plan_id']);
                $item_id = $wpvs_term_id;
                $wpvs_term = get_term($wpvs_term_id, 'rvs_video_category' );
                if( ! empty($wpvs_term) && ! is_wp_error($wpvs_term) ) {
                    $price = get_term_meta($wpvs_term_id, 'wpvs_category_purchase_price', true);
                    $name = $wpvs_term->name;
                    $checkout_description = 'Access to ' . $name;
                } else {
                    rvs_exit_ajax(__('Could not find that video category.', 'vimeo-sync-memberships'), 400);
                }
            }
        }

        if(isset($_POST['coupon_code']) && ! empty( $_POST['coupon_code']) ) {
            $coupon_code = $_POST['coupon_code'];
            $wpvs_coupon = new WPVS_Coupon_Code($coupon_code);
            if( ! empty($wpvs_coupon) ) {
                $discount_price = wpvs_calculate_coupon_amount($price, $coupon_code);
            }
        }

        if( $is_renewal ) {
            $current_membership = $wpvs_customer->get_membership_by_id($membership_id);
            if( ! empty($current_membership) && isset($current_membership->discount) && $current_membership->discount['duration'] == 'forever' ) {
                $wpvs_coupon = new WPVS_Coupon_Code($current_membership->discount['id']);
                if( ! empty($wpvs_coupon) ) {
                    $discount_price = wpvs_calculate_coupon_amount($price, $wpvs_coupon->coupon_id);
                }
            }
        }

        if( $wpvs_require_billing_info && ! empty($wpvs_customer->billing_state) && ! empty($wpvs_customer->billing_country) ) {
            $wpvs_tax_rate = new WPVS_Tax_Rate();
            if( ! empty($discount_price) ) {
                $tax_rate_price = intval($discount_price);
            } else {
                $tax_rate_price = intval($price);
            }
            $jurisdiction_rates = $wpvs_tax_rate->get_tax_rates_by_jurisdiction($wpvs_customer->billing_state, $wpvs_customer->billing_country);
            if( ! empty($jurisdiction_rates) ) {
                foreach($jurisdiction_rates as $tax_rate) {
                    $get_tax_rate_amount = $wpvs_tax_rate->calculate_tax_rate_amount($tax_rate_price, $tax_rate);
                    if( ! empty($get_tax_rate_amount) ) {
                        $applied_taxes[] = (object) array(
                            'tax_name' => $tax_rate->display_name,
                            'amount'   => $get_tax_rate_amount->amount,
                            'decimal_amount'   => $get_tax_rate_amount->decimal_amount,
                            'inclusive' => $tax_rate->inclusive
                        );
                    }
                }
            }
        }

        if( ! empty($applied_taxes) ) {
            foreach($applied_taxes as $apply_tax) {
                if( ! $apply_tax->inclusive ) {
                    if( ! empty($discount_price) ) {
                        $discount_price += $apply_tax->amount;
                    } else {
                        $price += $apply_tax->amount;
                    }
                }
            }
        }

        if( ! empty($discount_price) ) {
            $nice_price = number_format($discount_price/100,2);
        } else {
            $nice_price = number_format($price/100,2);
        }

        if(isset($_POST['redirect_success']) && ! empty($_POST['redirect_success'])) {
            $cb_success_url = $_POST['redirect_success'];
        } else {
            $rvs_account_page = get_option('rvs_account_page');
            $cb_success_url = get_permalink($rvs_account_page);
        }

        if( ! empty($plan_trial) && ! $is_renewal && ! wpvs_user_has_completed_trial($rvs_current_user->ID, $item_id) ) {
            $redirect_plan_trial = true;
        }

        $cb_cancel_url = $_POST['redirect'];

        $wpvs_coinbase_payment = new \WPVS\Coinbase\WPVSCoinbasePayment();

        $user_pending_payments = $wpvs_coinbase_payment->wpvs_get_user_coinbase_payments($rvs_current_user->ID, $transaction_type, $item_id, $purchase_type);

        if( ! empty($user_pending_payments) ) {
            $order_found = false;
            $pending_order = null;
            foreach($user_pending_payments as $order) {
                if( empty($order->coin_currency) && ($order->status != "COMPLETED" ) ) {
                    $order_found = true;
                    $pending_order = $order;
                    break;
                }
            }
            if($order_found && ! empty($pending_order)) {
                $confirm_cb_order_price = number_format($pending_order->local_amount,2,'.','');
                if( $confirm_cb_order_price != $nice_price ) {
                    $wpvs_coinbase_payment->wpvs_delete_coinbase_payment($pending_order->id);
                } else {
                    ApiClient::init($wpvs_coinbase_manager->get_api_key());
                    try {
                        $pending_cb_order = Charge::retrieve($pending_order->chargeid);
                        if ($pending_cb_order) {
                            $order_timeline = $pending_cb_order->timeline;
                            if( ! empty($order_timeline) ) {
                                $order_status = $wpvs_coinbase_payment->get_latest_charge_status($order_timeline);

                                if( $order_status['status'] == "EXPIRED" || $order_status['status'] == "CANCELED" ) {
                                    $wpvs_coinbase_payment->wpvs_delete_coinbase_payment($pending_order->id);
                                } else {
                                    if( $order_status['status'] == "PENDING" ) {
                                        if( $transaction_type == 'subscription' ) {
                                            wpvs_add_coinbase_membership_to_user($rvs_current_user->ID, $item_id, $pending_order->chargeid, $pending_order->code, $price, $name, $interval, $interval_count, $order_status['status'], $plan_trial, $trial_frequency, $selected_coin, $is_renewal, $wpvs_coupon);
                                        }
                                        rvs_exit_ajax(__('You already have a pending order for this item', 'vimeo-sync-memberships'), 400);
                                    }
                                    if( $order_status['status'] == "UNRESOLVED" ) {
                                        $wpvs_return_order = array(
                                            'redirect' => $pending_cb_order->hosted_url
                                        );
                                        if( $transaction_type == 'subscription' ) {
                                            wpvs_add_coinbase_membership_to_user($rvs_current_user->ID, $item_id, $pending_order->chargeid, $pending_order->code, $price, $name, $interval, $interval_count, $order_status['status'], $plan_trial, $trial_frequency, $selected_coin, $is_renewal, $wpvs_coupon);
                                        }
                                    }
                                    if( $order_status['status'] == "NEW") {
                                        $wpvs_return_order = array('redirect' => $pending_cb_order->hosted_url );
                                        if( $transaction_type == 'subscription' ) {
                                            wpvs_add_coinbase_membership_to_user($rvs_current_user->ID, $item_id, $pending_order->chargeid, $pending_order->code, $price, $name, $interval, $interval_count, $order_status['status'], $plan_trial, $trial_frequency, $selected_coin, $is_renewal, $wpvs_coupon);
                                        }
                                    }
                                }
                            }
                        } else {
                            $wpvs_coinbase_payment->wpvs_delete_coinbase_payment($pending_order->id);
                        }
                    } catch (Exception $e) {
                        rvs_exit_ajax($e->getMessage(), 400);
                    }
                }
            }
        }

        if( empty($wpvs_return_order) ) {

            $coinbase_charge_params = [
                "name"         => $name,
                "description"  => $checkout_description,
                "pricing_type" => "fixed_price",
                "local_price"  => array(
                    "amount"   => $nice_price,
                    "currency" => $rvs_currency
                ),
                "metadata"     => array(
                    "user_id"  => $rvs_current_user->ID,
                    "email"    => $rvs_current_user->user_email
                ),
                "redirect_url" => $cb_success_url,
                "cancel_url"   => $cb_cancel_url
            ];


            ApiClient::init($wpvs_coinbase_manager->get_api_key());

            $coinbase_charge = new Charge($coinbase_charge_params);
            try {
               $coinbase_charge->save();

                if ($coinbase_charge->id) {

                    $wpvs_cb_order_params = array(
                        'chargeid' => $coinbase_charge->id,
                        'code' => $coinbase_charge->code,
                        'status' => "NEW",
                        'local_currency'  => $coinbase_charge['pricing']['local']['currency'],
                        'local_amount' => $coinbase_charge['pricing']['local']['amount'],
                        'coin_amount' => "",
                        'coin_currency' => "",
                        'refunded' => 0,
                        'userid' => $rvs_current_user->ID,
                        'planid' => null,
                        'videoid' => null,
                        'termid' => null,
                        'type' => $transaction_type,
                        'purchase' => $purchase_type,
                        'updated' => $wpvs_current_time
                    );
                    if( $transaction_type == 'subscription' ) {
                        $wpvs_cb_order_params['planid'] = $item_id;
                        wpvs_add_coinbase_membership_to_user($rvs_current_user->ID, $item_id, $coinbase_charge->id, $coinbase_charge->code, $price, $name, $interval, $interval_count, "NEW", $plan_trial, $trial_frequency, $is_renewal, $wpvs_coupon);
                    }

                    if( $transaction_type == 'purchase' ) {
                        if( $purchase_type == 'purchase' || $purchase_type == 'rental' ) {
                            $wpvs_cb_order_params['videoid'] = $item_id;
                        }
                        if( $purchase_type == 'termpurchase' ) {
                            $wpvs_cb_order_params['termid'] = $item_id;
                        }
                    }

                    $new_wpvs_cb_order = \WPVS\Coinbase\WPVSCoinbasePayment::create($wpvs_cb_order_params, $test_payment);

                    if( $redirect_plan_trial ) {
                        $wpvs_return_order = array('redirect' => $cb_success_url);
                    } else {
                        $wpvs_return_order = array('redirect' => $coinbase_charge->hosted_url );
                    }
                }

            } catch (\Exception $exception) {
                rvs_exit_ajax($exception->getMessage(), 400);
            }

        }

        if( ! empty($wpvs_return_order) ) {
            echo json_encode($wpvs_return_order);
        } else {
            rvs_exit_ajax(__('Error creating Coinbase order.', 'vimeo-sync-memberships'), 400);
        }

    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_remove_coinbase_access_ajax_request', 'wpvs_remove_coinbase_access_ajax_request' );

function wpvs_remove_coinbase_access_ajax_request() {
    global $rvs_live_mode;
    global $rvs_current_user;

    if($rvs_live_mode == "off") {
        $get_memberships = 'rvs_user_memberships_test';
    } else {
        $get_memberships = 'rvs_user_memberships';
    }

    if( isset($_POST["charge_id"]) && isset($_POST["new_status"]) ) {
        $charge_id = $_POST["charge_id"];
        $new_status = $_POST["new_status"];
        $rvs_mail_text = "Cancelled";
        $run_wpvs_delete_functions = false;
        $remove_membership = null;

        if( isset($_POST["user_id"]) && current_user_can('manage_options') ) {
            $user_id = $_POST["user_id"];
        } else {
            $user_id = $rvs_current_user->ID;
        }

        if($new_status == "reactivate") {
            $rvs_mail_text = "Reactivated";
        }
        $return_status = $new_status;
        $wpvs_current_time = current_time('timestamp', 1);
        $user_memberships = get_user_meta($user_id, $get_memberships, true);
        if(!empty($user_memberships)) {
            foreach($user_memberships as $key => &$access) {
                if ($access["id"] == $charge_id) {
                    if($new_status == "delete") {
                        unset($user_memberships[$key]);
                        wpvs_remove_coinbase_order($user_id, $access["id"]);
                        $run_wpvs_delete_functions = true;
                        $remove_membership = $access;
                    }
                    if($new_status == "cancel") {
                        $access["status"] = "CANCELED";
                        $access["ends_next_date"] = 1;
                    }
                    if($new_status == "reactivate") {
                        $membership_expires = $access["ends"];
                        if( intval($membership_expires) > $wpvs_current_time) {
                            $access["status"] = "COMPLETED";
                        } else {
                            $access["status"] = "new";
                            $return_status = "NEW";
                        }
                        if(isset($access["ends_next_date"]) && ! empty($access["ends_next_date"])) {
                            $access["ends_next_date"] = 0;
                            unset($access["ends_next_date"]);
                        }
                    }
                    $plan_name = $access["name"];
                    break;
                }
            }
        }
        update_user_meta($user_id, $get_memberships, $user_memberships);
        if( $run_wpvs_delete_functions && ! empty($remove_membership) ) {
            wpvs_do_after_delete_membership_action($user_id, $remove_membership);
        }
        wpvs_send_email($rvs_mail_text, $user_id, $plan_name);
        echo $return_status;
    } else {
        $wpvs_error_message = __('Missing Plan ID.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 400);
    }
    wp_die();
}
