<?php

function wpvs_create_coinbase_payment_tables() {
    global $wpdb;
    if( ! wpvs_coinbase_payments_table_exists() ) {
        $table_name = $wpdb->prefix . 'wpvs_coinbase_payments';
        $test_table_name = $wpdb->prefix . 'wpvs_test_coinbase_payments';
        $charset_collate = $wpdb->get_charset_collate();

        $live_payment_tables = "CREATE TABLE $table_name (
          id mediumint(20) NOT NULL UNIQUE AUTO_INCREMENT,
          chargeid varchar(100) NOT NULL UNIQUE,
          code varchar(100) NOT NULL UNIQUE,
          status tinytext NOT NULL,
          local_currency tinytext NOT NULL,
          local_amount tinytext NOT NULL,
          coin_amount tinytext,
          coin_currency tinytext,
          refunded boolean,
          userid int NOT NULL,
          planid tinytext,
          videoid mediumint(9),
          termid mediumint(9),
          type tinytext NOT NULL,
          purchase tinytext,
          updated text NOT NULL,
          PRIMARY KEY (id)
        ) $charset_collate;";

        $test_payment_tables = "CREATE TABLE $test_table_name (
          id mediumint(20) NOT NULL UNIQUE AUTO_INCREMENT,
          chargeid varchar(100) NOT NULL UNIQUE,
          code varchar(100) NOT NULL UNIQUE,
          status tinytext NOT NULL,
          local_currency tinytext NOT NULL,
          local_amount tinytext NOT NULL,
          coin_amount tinytext,
          coin_currency tinytext,
          refunded boolean,
          userid int NOT NULL,
          planid tinytext,
          videoid mediumint(9),
          termid mediumint(9),
          type tinytext NOT NULL,
          purchase tinytext,
          updated text NOT NULL,
          PRIMARY KEY (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $live_payment_tables );
        dbDelta( $test_payment_tables );
    }
}

function wpvs_coinbase_payments_table_exists() {
    global $wpdb;
    $payments_table_exists = true;
    $wpvs_payments_table_name = $wpdb->prefix . 'wpvs_coinbase_payments';
    if($wpdb->get_var("SHOW TABLES LIKE '$wpvs_payments_table_name'") != $wpvs_payments_table_name) {
         $payments_table_exists = false;
    }
    return $payments_table_exists;
}


function wpvs_get_admin_coinbase_payments($offset) {
    global $wpdb;
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $table_name = $wpdb->prefix . 'wpvs_test_coinbase_payments';
    } else {
        $table_name = $wpdb->prefix . 'wpvs_coinbase_payments';
    }
    $payments = $wpdb->get_results("SELECT * FROM $table_name ORDER BY time DESC LIMIT 50 OFFSET $offset");
    return $payments;
}

function wpvs_refund_coinbase_table_payment($order_id) {
    global $wpdb;
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $table_name = $wpdb->prefix . 'wpvs_test_coinbase_payments';
    } else {
        $table_name = $wpdb->prefix . 'wpvs_coinbase_payments';
    }
    $update_payment = array('refunded' => '1');
    $find_payment = array('id' => $order_id);
    $refund = $wpdb->update($table_name, $update_payment, $find_payment);
    return $refund;
}

function wpvs_get_coinbase_payment_by_id($order_id) {
    global $wpdb;
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $table_name = $wpdb->prefix . 'wpvs_test_coinbase_payments';
    } else {
        $table_name = $wpdb->prefix . 'wpvs_coinbase_payments';
    }
    $payment = $wpdb->get_results("SELECT * FROM $table_name WHERE id = '$order_id'");
    return $payment;
}