<?php

if( ! function_exists('wpvs_add_coinbase_membership_to_user') ) {
function wpvs_add_coinbase_membership_to_user($user_id, $membership_id, $charge_id, $code, $price, $name, $interval, $interval_count, $status, $plan_trial, $trial_frequency, $is_renewal, $wpvs_coupon) {
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $get_memberships = 'rvs_user_memberships_test';
        $get_trial_periods = 'wpvs_user_completed_trials_test';
        $coinbase_key = 'coinbase_test';
        $test_member = true;
    } else {
        $get_memberships = 'rvs_user_memberships';
        $get_trial_periods = 'wpvs_user_completed_trials';
        $coinbase_key = 'coinbase';
        $test_member = false;
    }
    $add_trial = false;
    $plan_interval = $interval;
    if($interval_count > 1) {
        $plan_interval = $interval_count . ' ' .$interval.'s';
    }
    $set_start_time = $interval_count . ' ' .$plan_interval.'s';
    $user_has_completed_trial = wpvs_user_has_completed_trial($user_id, $membership_id);
    if(!empty($plan_trial) && $trial_frequency != "none" && ! $is_renewal && ! $user_has_completed_trial ) {
        if($trial_frequency == "day") {
            $set_start_time = '+'.$plan_trial.' days';
        }

        if($trial_frequency == "week") {
            $set_start_time = '+'.$plan_trial.' weeks';
        }

        if($trial_frequency == "month") {
            $set_start_time = '+'.$plan_trial.' months';
        }
        $status = "COMPLETED";
        wpvs_user_completed_trial($user_id, $membership_id);
        $add_trial = true;
    } else {
        $set_start_time = '+'.$set_start_time;
    }

    $subscription_ends = strtotime($set_start_time, current_time('timestamp', 1));

    $has_membership = false;
    $user_memberships = get_user_meta($user_id, $get_memberships, true);
    if( ! empty($user_memberships) ) {
        foreach($user_memberships as $key => &$membership) {
            if($membership["plan"] == $membership_id && $membership["type"] == $coinbase_key) {
                $has_membership = true;
                $membership["id"] = $charge_id;
                if( $status != "refunded" ) {
                    $membership["status"] = $status;
                }
                if( $is_renewal ) {
                    $membership["ends"] = $subscription_ends;
                    $membership["reminder_sent"] = 0;
                }
                break;
            }
        }
    } else {
        $user_memberships = array();
    }

    if( ! $has_membership ) {
        $new_coin_membership = array(
            'plan' => $membership_id,
            'id' => $charge_id,
            'amount' => $price,
            'name' => $name,
            'interval' => $interval,
            'interval_count' => $interval_count,
            'ends' => $subscription_ends,
            'status' => $status,
            'type' => $coinbase_key,
            'code' => $code
        );
        if( $add_trial ) {
            $new_coin_membership['trial_ends'] = $subscription_ends;
        }
        if( ! empty($wpvs_coupon) ) {
            $add_coupon_details = array(
                'type' => 'coingate',
                'id' => $wpvs_coupon->coupon_id,
                'duration' => $wpvs_coupon->duration,
                'duration_in_months' => $wpvs_coupon->month_duration,
            );
            if( $wpvs_coupon->type == 'amount' ) {
                $add_coupon_details['amount_off'] = $wpvs_coupon->amount;
            } else {
                $add_coupon_details['percent_off'] = $wpvs_coupon->amount;
            }
            $new_coin_membership['discount'] = $add_coupon_details;
            wpvs_add_coupon_code_use($wpvs_coupon->coupon_id, $user_id);
        }
        $user_memberships[] = $new_coin_membership;
    }
    update_user_meta( $user_id, $get_memberships, $user_memberships);

    if( ! $has_membership ) {
        wpvs_send_email("New", $user_id, $name);
        wpvs_add_new_member($user_id, $test_member);
        wpvs_do_after_new_membership_action($user_id, $new_coin_membership);
    }
}
}

if( ! function_exists('wpvs_remove_coinbase_order') ) {
function wpvs_remove_coinbase_order($user_id, $charge_id) {
    $wpvs_user_coinbase_payment = new \WPVS\Coinbase\WPVSCoinbasePayment;
    $user_pending_payments = $wpvs_user_coinbase_payment->wpvs_get_user_coinbase_payments_by_id($user_id, $charge_id);
    if( ! empty($user_pending_payments) ) {
        $order_found = false;
        $pending_order = null;
        foreach($user_pending_payments as $order) {
            if( $order->status != "CONFIRMED" && $order->status != "COMPLETED" ) {
                $wpvs_coinbase_payment->wpvs_delete_coinbase_payment($charge_id);
            }
        }
    }
}
}
