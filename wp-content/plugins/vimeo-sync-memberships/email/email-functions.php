<?php
$wpvs_smtp_settings = get_option('wpvs_smtp_settings');

if( isset($wpvs_smtp_settings['enabled']) && !empty($wpvs_smtp_settings['enabled'])) {
    add_action( 'phpmailer_init', 'wpvs_smtp_mailer_setup' );
}

function wpvs_smtp_mailer_setup( $phpmailer ) {
    $wpvs_smtp_settings = get_option('wpvs_smtp_settings');
    $phpmailer->isSMTP();
    $phpmailer->Host = $wpvs_smtp_settings['host'];
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = $wpvs_smtp_settings['port'];
    $phpmailer->Username = $wpvs_smtp_settings['username'];
    $phpmailer->Password = $wpvs_smtp_settings['password'];
    $phpmailer->SMTPSecure = $wpvs_smtp_settings['securemode'];
    $phpmailer->From = $wpvs_smtp_settings['from_email'];
    $phpmailer->FromName = $wpvs_smtp_settings['from_name'];
}

function rvs_set_email_content_type( $content_type ) {
    return 'text/html';
}

function wpvs_create_browser_email($email_html) {
    $email_id = uniqid();
    $wpvs_html_email_dir = ABSPATH .'member/emails';
    if (!file_exists($wpvs_html_email_dir)) {
        mkdir($wpvs_html_email_dir, 0755, true);
    }
    $save_html_email = $wpvs_html_email_dir.'/email-'.$email_id.'.html';
    $email_url = home_url('/member/emails/email-'.$email_id.'.html');
    file_put_contents($save_html_email, $email_html);
    return $email_url;
}

function wpvs_send_email($mail_for, $member, $plan_name) {
    add_filter( 'wp_mail_content_type', 'rvs_set_email_content_type');
    $from_name = get_bloginfo('name');
    $admin_email = get_option('admin_email');
    $email_from = $admin_email;
    $account_page = get_option('rvs_account_page');
    $admin_user_url = admin_url('admin.php?page=rvs-edit-member&id=') . $member;
    $account_url = get_permalink($account_page);

    $new_user = get_user_by('id', $member);
    $member_name = $new_user->user_login;
    $member_email = $new_user->user_email;
    $mail_error = false;

    $site_name = get_bloginfo('name');
    $wpvs_accent_colour = get_option('rvs_primary_color', '#27ae60');
    $wpvs_email_logo = get_option('rvs_email_logo', get_theme_mod('rogue_company_logo'));
    if(empty($wpvs_email_logo)) {
        $wpvs_email_logo = RVS_MEMBERS_BASE_URL .'image/vimeo-sync-square.png';
    }
    $wpvs_show_powered_by_email = get_option('wpvs_show_powered_by_email', '1');

    if($wpvs_show_powered_by_email) {
        $footer_powered_by_text = 'Powered by ';
        $footer_link_text = 'WP Video Subscriptions';
        $footer_powered_by_link = 'https://wpvideosubscriptions.com';
    } else {
        $footer_powered_by_text = '';
        $footer_link_text = $site_name;
        $footer_powered_by_link = home_url();
    }

    switch($mail_for) {
        case "New":
            $rvs_new_membership_email = get_option('rvs_new_membership_email');

            $email_subject = 'Thanks for subscribing to our '. $plan_name .' membership';

            // MEMBER EMAIL
            $member_email_message = 'You can view your memberships <a href="'. $account_url .'">here</a><br/>From here you can view and manage your current memberships, credit cards and payments.<br/>If you have any questions, you can email us at <a href="mailto:' . $admin_email . '">'.$admin_email.'.</a>';

            if( ! empty($rvs_new_membership_email) ) {
                if(isset($rvs_new_membership_email['from_name'])) {
                    $from_name = $rvs_new_membership_email['from_name'];
                }
                if(isset($rvs_new_membership_email['from_email'])) {
                    $email_from = $rvs_new_membership_email['from_email'];
                }

                if( isset($rvs_new_membership_email['notify_email']) ) {
                    $admin_email = $rvs_new_membership_email['notify_email'];
                }

                if(isset($rvs_new_membership_email['subject'])) {
                    $email_subject = $rvs_new_membership_email['subject'];
                    // SET PLAN NAME
                    $email_subject = str_ireplace("{planname}", $plan_name, $email_subject);
                }

                if(isset($rvs_new_membership_email['content'])) {
                    $member_email_message = $rvs_new_membership_email['content'];
                    // SET PLAN NAME
                    $member_email_message = str_ireplace("{planname}", $plan_name, $member_email_message);
                }
            }

            // ADMIN EMAIL
            $admin_email_subject = 'New ' . $plan_name . ' Subscription';
            $admin_email_message = '<h3>Congratulations!</h3>';
            $admin_email_message .= '<a href="' . $admin_user_url . '">' . $member_name . '</a> has subscribed to the ' . $plan_name . ' plan.';
            break;
        case "Cancelled":
            $rvs_cancel_membership_email = get_option('rvs_cancel_membership_email');

            $email_subject = 'Subscription Canceled';

            // MEMBER EMAIL
            $member_email_message = '<h3>Your subscription has been cancelled.</h3>Your ' . $plan_name . ' subscription on ' . $site_name . ' has been cancelled. If you believe this is an error, you can email us at <a href="mailto:' . $admin_email . '">'.$admin_email.'.</a>';

            if(!empty($rvs_cancel_membership_email)) {
                if(isset($rvs_cancel_membership_email['from_name'])) {
                    $from_name = $rvs_cancel_membership_email['from_name'];
                }
                if(isset($rvs_cancel_membership_email['from_email'])) {
                    $email_from = $rvs_cancel_membership_email['from_email'];
                }
                if(isset($rvs_cancel_membership_email['subject'])) {
                    $email_subject = $rvs_cancel_membership_email['subject'];
                }
                if( isset($rvs_cancel_membership_email['notify_email']) ) {
                    $admin_email = $rvs_cancel_membership_email['notify_email'];
                }

                if(isset($rvs_cancel_membership_email['content'])) {
                    $member_email_message = $rvs_cancel_membership_email['content'];
                    // SET PLAN NAME
                    $member_email_message = str_ireplace("{planname}", $plan_name, $member_email_message);
                }
            }

            $admin_email_subject = $member_name .' has cancelled their subscription';
            $admin_email_message = '<a href="' . $admin_user_url . '">' . $member_name . '</a> has cancelled their '.$plan_name. ' subscription.';

            break;
        case "Reactivated":
            $wpvs_reactivate_membership_email = get_option('rvs_reactivate_membership_email');

            $email_subject = 'Subscription Re-activated';

            // MEMBER EMAIL
            $member_email_message = '<h3>Your subscription has been re-activated!</h3>Your ' . $plan_name . ' subscription on ' . $site_name . ' has been re-activated. If you believe this is an error, you can email us at <a href="mailto:' . $admin_email . '">'.$admin_email.'.</a>';

            if(!empty($wpvs_reactivate_membership_email)) {
                if(isset($wpvs_reactivate_membership_email['from_name'])) {
                    $from_name = $wpvs_reactivate_membership_email['from_name'];
                }
                if(isset($wpvs_reactivate_membership_email['from_email'])) {
                    $email_from = $wpvs_reactivate_membership_email['from_email'];
                }
                if(isset($wpvs_reactivate_membership_email['subject'])) {
                    $email_subject = $wpvs_reactivate_membership_email['subject'];
                }
                if( isset($wpvs_reactivate_membership_email['notify_email']) ) {
                    $admin_email = $wpvs_reactivate_membership_email['notify_email'];
                }

                if(isset($wpvs_reactivate_membership_email['content'])) {
                    $member_email_message = $wpvs_reactivate_membership_email['content'];
                    // SET PLAN NAME
                    $member_email_message = str_ireplace("{planname}", $plan_name, $member_email_message);
                }
            }

            $admin_email_subject = $member_name .' has re-activated their subscription';
            $admin_email_message = '<a href="' . $admin_user_url . '">' . $member_name . '</a> has re-activated their '.$plan_name. ' subscription.';

            break;
        case "Deleted":
            $rvs_delete_membership_email = get_option('rvs_delete_membership_email');

            $email_subject = 'Subscription Deleted';

            // MEMBER EMAIL
            $member_email_message = '<h3>Your subscription has been deleted.</h3>Your ' . $plan_name . ' subscription on ' . $site_name . ' has been deleted. If you believe this is an error, you can email us at <a href="mailto:' . $admin_email . '">'.$admin_email.'.</a>';

            if(!empty($rvs_delete_membership_email)) {
                if(isset($rvs_delete_membership_email['from_name'])) {
                    $from_name = $rvs_delete_membership_email['from_name'];
                }
                if(isset($rvs_delete_membership_email['from_email'])) {
                    $email_from = $rvs_delete_membership_email['from_email'];
                }
                if(isset($rvs_delete_membership_email['subject'])) {
                    $email_subject = $rvs_delete_membership_email['subject'];
                }
                if( isset($rvs_delete_membership_email['notify_email']) ) {
                    $admin_email = $rvs_delete_membership_email['notify_email'];
                }

                if(isset($rvs_delete_membership_email['content'])) {
                    $member_email_message = $rvs_delete_membership_email['content'];
                    // SET PLAN NAME
                    $member_email_message = str_ireplace("{planname}", $plan_name, $member_email_message);
                }
            }

            // ADMIN EMAIL
            $admin_email_subject = $member_name .' has deleted their subscription';
            $admin_email_message = '<a href="' . $admin_user_url . '">' . $member_name . '</a> has deleted their '.$plan_name. ' subscription.';

            break;
        case "Account":
            $rvs_new_account_email = get_option('rvs_new_account_email');
            $pass_reset_link = '<a href="'.wp_lostpassword_url( home_url() ).'">'.__('Reset Password', 'vimeo-sync-memberships').'</a>';
            $email_subject = 'New Account for ' . get_bloginfo('name');

            // MEMBER EMAIL
            $member_email_message = '<h3>Thanks for signing up!</h3>You can sign into your account <a href="'. $rvs_account_link .'">here</a><h3>Username</h3>Your username is: {username}<h5>Forgot Your Password?</h5>'.$pass_reset_link;

            if(!empty($rvs_new_account_email)) {
                if(isset($rvs_new_account_email['from_name'])) {
                    $from_name = $rvs_new_account_email['from_name'];
                }
                if(isset($rvs_new_account_email['from_email'])) {
                    $email_from = $rvs_new_account_email['from_email'];
                }
                if(isset($rvs_new_account_email['subject'])) {
                    $email_subject = $rvs_new_account_email['subject'];
                }
                if( isset($rvs_new_account_email['notify_email']) ) {
                    $admin_email = $rvs_new_account_email['notify_email'];
                }

                if(isset($rvs_new_account_email['content'])) {
                    $member_email_message = $rvs_new_account_email['content'];
                    // SET USER PASSWORD IN EMAIL
                    $member_email_message = str_ireplace("{password}", $pass_reset_link, $member_email_message);
                    // SET USERNAME IN EMAIL
                }
            }
            $member_email_message = str_ireplace("{username}", $member_name, $member_email_message);

            // ADMIN EMAIL

            $admin_email_subject = 'New Account for ' . get_bloginfo('name');
            $admin_email_message = '<h3>New Account Created</h3>';
            $admin_email_message .= '<a href="' . $admin_user_url . '">' . $member_name . '</a> has created a new account on ' . get_bloginfo('name') . '<br/>';
            $admin_email_message .= 'User email: <a href="mailto:' . $member_email . '">' . $member_email . '</a>';
            break;
        default:
            $mail_error = true;
            $admin_email_subject = "Oops - Emailing Error with WP Video Memberships Add-On";
            $admin_email_message = "Something went wrong when trying to send a WP Video membership email.";
    }


    // CREATE EMAIL
    $email_template = file_get_contents(RVS_MEMBERS_BASE_DIR .'/email/emailheader.html');
    $email_template .= file_get_contents(RVS_MEMBERS_BASE_DIR .'/email/notification.html');
    $email_template .= file_get_contents(RVS_MEMBERS_BASE_DIR .'/email/emailfooter.html');

    $setup_email_message = str_replace('{{wpvs_accent_colour}}', $wpvs_accent_colour, $email_template);
    $setup_email_message = str_replace('{{email_logo_image}}',$wpvs_email_logo, $setup_email_message);
    $setup_email_message = str_replace('{{powered_by}}', $footer_powered_by_text, $setup_email_message);
    $setup_email_message = str_replace('{{footer_link}}', $footer_powered_by_link, $setup_email_message);
    $setup_email_message = str_replace('{{footer_link_text}}', $footer_link_text, $setup_email_message);

    // SET HEADERS
    $from_header = $from_name . ' <' . $email_from . '>';
    $headers = 'From: '. $from_header ."\r\n".'Reply-To: '.$email_from."\r\n" ;

    if(!$mail_error) {
        $html_member_email = str_replace('{{title}}', $email_subject, $setup_email_message);
        $html_member_email = str_replace('{{notification}}', $member_email_message, $html_member_email);
        $html_member_email = str_replace('{{noticelink}}', $account_url, $html_member_email);
        $html_member_email = str_replace('{{noticebutton}}', __('View Account', 'vimeo-sync-memberships'), $html_member_email);

        // Create Browser Email
        $browser_link = wpvs_create_browser_email($html_member_email);
        $html_member_email = str_replace('{{browserlink}}', $browser_link, $html_member_email);
        wp_mail($member_email, $email_subject, $html_member_email, $headers);
    }

    $html_admin_email = str_replace('{{title}}', $admin_email_subject, $setup_email_message);
    $html_admin_email = str_replace('{{notification}}', $admin_email_message, $html_admin_email);
    $html_admin_email = str_replace('{{noticelink}}', $admin_user_url, $html_admin_email);
    $html_admin_email = str_replace('{{noticebutton}}', __('View Member', 'vimeo-sync-memberships'), $html_admin_email);

    // SEND ADMIN EMAIL
    wp_mail($admin_email, $admin_email_subject, $html_admin_email, $headers);
}

function wpvs_send_membership_reminder($user_id, $plan_name, $renewal_date, $is_coin_payment) {
    add_filter( 'wp_mail_content_type', 'rvs_set_email_content_type');
    $email_settings = wpvs_create_header_email_settings($user_id);
    $account_page = get_option('rvs_account_page');
    $account_url = get_permalink($account_page);

    $from_name = $email_settings['from_name'];
    $admin_email = $email_settings['admin_email'];
    $email_from = $admin_email;
    $member_name = $email_settings['member_name'];
    $member_email = $email_settings['member_email'];

    $mail_error = false;

    if($email_settings['show_footer']) {
        $footer_powered_by_text = 'Powered by ';
        $footer_link_text = 'WP Video Subscriptions';
        $footer_powered_by_link = 'https://wpvideosubscriptions.com';
    } else {
        $footer_powered_by_text = '';
        $footer_link_text = $member_email;
        $footer_powered_by_link = home_url();
    }

    $wpvs_membership_reminder_email = get_option('wpvs_membership_reminder_email');

    $email_title = $plan_name . ' ' . __('Subscription', 'vimeo-sync-memberships');

    $renewal_formatted_date = wp_date( 'M d, Y', $renewal_date);

    if( $is_coin_payment ) {
        $email_subject = 'Renewal Reminder - ' . $plan_name . ' Subscription';
        $member_email_message = '<p>This is a friendly reminder that your ' . $plan_name . ' membership expired on <strong>' .$renewal_formatted_date.'</strong>.</p><p>You have paid with crypto currency, which means you will need to renew your membership <a href="'. $account_url .'">here</a></p><p>If you have any questions, send us an email at <a href="mailto:' . $admin_email . '">'.$admin_email.'.</a></p>';
    } else {
        $email_subject = 'Automatic Renewal Reminder - ' . $plan_name . ' Subscription';
        $member_email_message = '<p>This is a friendly reminder that your ' . $plan_name . ' membership will automatically renew on <strong>' .$renewal_formatted_date.'</strong>.</p><p>You can manage your memberships <a href="'. $account_url .'">here</a></p><p>If you have any questions, send us an email at <a href="mailto:' . $admin_email . '">'.$admin_email.'.</a></p>';
    }


    if(!empty($wpvs_membership_reminder_email)) {
        if(isset($wpvs_membership_reminder_email['from_name'])) {
            $from_name = $wpvs_membership_reminder_email['from_name'];
        }
        if(isset($wpvs_membership_reminder_email['from_email'])) {
            $email_from = $wpvs_membership_reminder_email['from_email'];
        }

        if( $is_coin_payment ) {
            if(isset($wpvs_membership_reminder_email['coin_content'])) {
                $member_email_message = $wpvs_membership_reminder_email['coin_content'];
                $member_email_message = str_ireplace("{planname}", $plan_name, $member_email_message);
                $member_email_message = str_ireplace("{expireddate}", $renewal_formatted_date, $member_email_message);
            }
            if(isset($wpvs_membership_reminder_email['coin_subject'])) {
                $email_subject = $wpvs_membership_reminder_email['coin_subject'];
                $email_subject = str_ireplace("{planname}", $plan_name, $email_subject);
            }
        } else {
            if(isset($wpvs_membership_reminder_email['subject'])) {
                $email_subject = $wpvs_membership_reminder_email['subject'];
                $email_subject = str_ireplace("{planname}", $plan_name, $email_subject);
            }
            if(isset($wpvs_membership_reminder_email['content'])) {
                $member_email_message = $wpvs_membership_reminder_email['content'];
                $member_email_message = str_ireplace("{planname}", $plan_name, $member_email_message);
                $member_email_message = str_ireplace("{renewaldate}", $renewal_formatted_date, $member_email_message);
            }
        }
    }

    // CREATE EMAIL
    $email_template = file_get_contents(RVS_MEMBERS_BASE_DIR .'/email/emailheader.html');
    $email_template .= file_get_contents(RVS_MEMBERS_BASE_DIR .'/email/notification.html');
    $email_template .= file_get_contents(RVS_MEMBERS_BASE_DIR .'/email/emailfooter.html');

    $setup_email_message = str_replace('{{wpvs_accent_colour}}', $email_settings['accent_colour'], $email_template);
    $setup_email_message = str_replace('{{email_logo_image}}', $email_settings['logo'], $setup_email_message);
    $setup_email_message = str_replace('{{powered_by}}', $footer_powered_by_text, $setup_email_message);
    $setup_email_message = str_replace('{{footer_link}}', $footer_powered_by_link, $setup_email_message);
    $setup_email_message = str_replace('{{footer_link_text}}', $footer_link_text, $setup_email_message);

    // SET HEADERS
    $from_header = $from_name . ' <' . $email_from . '>';
    $headers = 'From: '. $from_header ."\r\n".'Reply-To: '.$email_from."\r\n" ;

    $html_member_email = str_replace('{{title}}', $email_title, $setup_email_message);
    $html_member_email = str_replace('{{notification}}', $member_email_message, $html_member_email);
    $html_member_email = str_replace('{{noticelink}}', $account_url, $html_member_email);
    if( $is_coin_payment ) {
        $html_member_email = str_replace('{{noticebutton}}', __('Renew Now', 'vimeo-sync-memberships'), $html_member_email);
    } else {
        $html_member_email = str_replace('{{noticebutton}}', __('View Account', 'vimeo-sync-memberships'), $html_member_email);
    }


    // Create Browser Email
    $browser_link = wpvs_create_browser_email($html_member_email);
    $html_member_email = str_replace('{{browserlink}}', $browser_link, $html_member_email);
    wp_mail($member_email, $email_subject, $html_member_email, $headers);
}

function wpvs_send_user_has_video_access($user_id, $item_id, $type, $amount, $currency) {
    add_filter( 'wp_mail_content_type', 'rvs_set_email_content_type');
    $email_settings = wpvs_create_header_email_settings($user_id);
    $from_name = $email_settings['from_name'];
    $admin_email = $email_settings['admin_email'];
    $email_from = $admin_email;
    $member_name = $email_settings['member_name'];
    $member_email = $email_settings['member_email'];

    $mail_error = false;

    if($email_settings['show_footer']) {
        $footer_powered_by_text = 'Powered by ';
        $footer_link_text = 'WP Video Subscriptions';
        $footer_powered_by_link = 'https://wpvideosubscriptions.com';
    } else {
        $footer_powered_by_text = '';
        $footer_link_text = $member_email;
        $footer_powered_by_link = home_url();
    }

    if($type == 'video') {
        $title = get_the_title($item_id);
        $item_link = get_permalink($item_id);
        $watch_now_text = __('Watch Now', 'vimeo-sync-memberships');
    }

    if($type == 'term') {
        $item_id = intval($item_id);
        $term = get_term($item_id, 'rvs_video_category');
        $title = $term->name;
        $item_link = get_term_link($item_id, 'rvs_video_category');
        $watch_now_text = __('Access Videos', 'vimeo-sync-memberships');
    }

    $email_subject = __('Payment Received - Access to', 'vimeo-sync-memberships').' '.$title;
    $email_title = __('Access to', 'vimeo-sync-memberships').' '.$title;
    $member_email_message = '<p>We have received your <strong>'.$amount.' ('.$currency.')</strong> payment for access to <a href="' . $item_link . '">'.$title.'</a>.';

    // CREATE EMAIL
    $email_template = file_get_contents(RVS_MEMBERS_BASE_DIR .'/email/emailheader.html');
    $email_template .= file_get_contents(RVS_MEMBERS_BASE_DIR .'/email/notification.html');
    $email_template .= file_get_contents(RVS_MEMBERS_BASE_DIR .'/email/emailfooter.html');

    $setup_email_message = str_replace('{{wpvs_accent_colour}}', $email_settings['accent_colour'], $email_template);
    $setup_email_message = str_replace('{{email_logo_image}}',$email_settings['logo'], $setup_email_message);
    $setup_email_message = str_replace('{{powered_by}}', $footer_powered_by_text, $setup_email_message);
    $setup_email_message = str_replace('{{footer_link}}', $footer_powered_by_link, $setup_email_message);
    $setup_email_message = str_replace('{{footer_link_text}}', $footer_link_text, $setup_email_message);

    // SET HEADERS
    $from_header = $from_name . ' <' . $email_from . '>';
    $headers = 'From: '. $from_header ."\r\n".'Reply-To: '.$email_from."\r\n" ;

    $html_member_email = str_replace('{{title}}', $email_title, $setup_email_message);
    $html_member_email = str_replace('{{notification}}', $member_email_message, $html_member_email);
    $html_member_email = str_replace('{{noticelink}}', $item_link, $html_member_email);
    $html_member_email = str_replace('{{noticebutton}}', $watch_now_text, $html_member_email);

    // Create Browser Email
    $browser_link = wpvs_create_browser_email($html_member_email);
    $html_member_email = str_replace('{{browserlink}}', $browser_link, $html_member_email);
    wp_mail($member_email, $email_subject, $html_member_email, $headers);
}

function wpvs_create_header_email_settings($user_id) {
    $member = get_user_by('id', $user_id);
    $wpvs_accent_colour = get_option('rvs_primary_color', '#27ae60');
    $wpvs_email_logo = get_option('rvs_email_logo', get_theme_mod('rogue_company_logo'));
    if(empty($wpvs_email_logo)) {
        $wpvs_email_logo = RVS_MEMBERS_BASE_URL .'image/vimeo-sync-square.png';
    }
    $wpvs_show_powered_by_email = get_option('wpvs_show_powered_by_email', '1');

    $email_settings = array(
        'from_name' => get_bloginfo('name'),
        'admin_email' => get_option('admin_email'),
        'member_name' => $member->user_login,
        'member_email' => $member->user_email,
        'site_name' => get_bloginfo('name'),
        'accent_colour' => $wpvs_accent_colour,
        'logo' => $wpvs_email_logo,
        'show_footer' => $wpvs_show_powered_by_email
    );
    return $email_settings;
}

function wpvs_send_test_smtp_settings($test_email) {
    add_filter( 'wp_mail_content_type', 'rvs_set_email_content_type');
    $email_settings = wpvs_create_header_email_settings(get_current_user_id());
    $from_name = $email_settings['from_name'];
    $admin_email = $email_settings['admin_email'];
    $email_from = $admin_email;
    $member_name = $email_settings['member_name'];
    $member_email = $test_email;

    $mail_error = false;

    if($email_settings['show_footer']) {
        $footer_powered_by_text = 'Powered by ';
        $footer_link_text = 'WP Video Subscriptions';
        $footer_powered_by_link = 'https://wpvideosubscriptions.com';
    } else {
        $footer_powered_by_text = '';
        $footer_link_text = $member_email;
        $footer_powered_by_link = home_url();
    }

    $email_subject = __('Your SMTP Email Is Working!', 'vimeo-sync-memberships');
    $email_title = $email_subject;
    $member_email_message = '<p>'.__('This email confirms your SMTP Email settings are working', 'vimeo-sync-memberships').'</p>';

    // CREATE EMAIL
    $email_template = file_get_contents(RVS_MEMBERS_BASE_DIR .'/email/emailheader.html');
    $email_template .= file_get_contents(RVS_MEMBERS_BASE_DIR .'/email/notification.html');
    $email_template .= file_get_contents(RVS_MEMBERS_BASE_DIR .'/email/emailfooter.html');

    $setup_email_message = str_replace('{{wpvs_accent_colour}}', $email_settings['accent_colour'], $email_template);
    $setup_email_message = str_replace('{{email_logo_image}}',$email_settings['logo'], $setup_email_message);
    $setup_email_message = str_replace('{{powered_by}}', $footer_powered_by_text, $setup_email_message);
    $setup_email_message = str_replace('{{footer_link}}', $footer_powered_by_link, $setup_email_message);
    $setup_email_message = str_replace('{{footer_link_text}}', $footer_link_text, $setup_email_message);

    // SET HEADERS
    $from_header = $from_name . ' <' . $email_from . '>';
    $headers = 'From: '. $from_header ."\r\n".'Reply-To: '.$email_from."\r\n" ;

    $html_member_email = str_replace('{{title}}', $email_title, $setup_email_message);
    $html_member_email = str_replace('{{notification}}', $member_email_message, $html_member_email);
    $html_member_email = str_replace('{{noticelink}}', admin_url('admin.php?page=rvs-email-settings'), $html_member_email);
    $html_member_email = str_replace('{{noticebutton}}', __('Ok!', 'vimeo-sync-memberships'), $html_member_email);

    $html_member_email = str_replace('{{browserlink}}', admin_url('admin.php?page=rvs-email-settings'), $html_member_email);
    $sent_success = wp_mail($member_email, $email_subject, $html_member_email, $headers);
    return $sent_success;
}
