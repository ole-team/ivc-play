<?php
/*
Plugin Name: WP Video Memberships
Plugin URI:  https://www.wpvideosubscriptions.com
Description: Restrict access to your WP Videos to members only.
Author:      Rogue Web Design
Author URI:  http://www.roguewebdesign.ca
Version:     4.7.8
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: /languages
Text Domain: vimeo-sync-memberships
*/

if(!defined('RVS_MEMBERS_BASE_URL')) {
	define('RVS_MEMBERS_BASE_URL', plugin_dir_url(__FILE__));
}
if(!defined('RVS_MEMBERS_BASE_DIR')) {
	define('RVS_MEMBERS_BASE_DIR', dirname(__FILE__));
}

if(!defined('RVS_MEMBERS_STRIPE_URL')) {
	define('RVS_MEMBERS_STRIPE_URL', plugin_dir_url(__FILE__).'include-stripe/');
}
if(!defined('RVS_MEMBERS_PAYPAL_URL')) {
	define('RVS_MEMBERS_PAYPAL_URL', plugin_dir_url(__FILE__).'include-paypal/');
}

// SET CURRENT VERSION

global $rvs_current_version;
$rvs_current_version = get_option('rvs_memberships_version');

if( ! defined('WPVS_VIDEO_MEMBERSHIPS_VERSION') ) {
    define('WPVS_VIDEO_MEMBERSHIPS_VERSION', '4.7.8');
}
global $wpvs_membership_update_required;
global $wpvs_stripe_options;
global $wpvs_stripe_api_version;
global $rvs_paypal_settings;
global $wpvs_coingate_settings;
global $rvs_live_mode;
global $wpvs_stripe_gateway_enabled;
global $rvs_paypal_enabled;
global $wpvs_coingate_enabled;
global $wpvs_coinbase_enabled;
global $wpvs_google_apple_enabled;
global $wpvs_stripe_checkout_enabled;
global $rvs_currency;
global $wpvs_currency_label;
global $rvs_current_user;
global $wpvs_stripe_customer;
global $wpvs_membership_plans_list;
global $wpvs_include_checkout_scripts;
$wpvs_include_checkout_scripts = false;
$wpvs_membership_update_required = false;
$wpvs_stripe_checkout_enabled = false;
$wpvs_stripe_options = get_option('wpvs_stripe_settings');
$rvs_paypal_settings = get_option('rvs_paypal_settings');
$wpvs_coingate_settings = get_option('wpvs_coingate_settings');
$rvs_live_mode = get_option('rvs_live_mode', "off");
$rvs_currency = get_option('rvs_currency', "USD");
$wpvs_membership_plans_list = get_option('rvs_membership_list');
$wpvs_stripe_api_version = get_option('wpvs_stripe_api_version', '2020-03-02');

global $rvs_message_array;
$rvs_message_array = array(
    'card' => __('Adding card','vimeo-sync-memberships'),
    'cardu' => __('Updating card','vimeo-sync-memberships'),
    'carddc' => __('Are you sure you want to delete this card?','vimeo-sync-memberships'),
    'cardd' => __('Deleting card','vimeo-sync-memberships'),
    'cardedit' => __('Editing card','vimeo-sync-memberships'),
    'cardudefault' => __('Updating default card','vimeo-sync-memberships'), 'checkcoupon' => __('Checking','vimeo-sync-memberships'),
    'discount' => __('Discount','vimeo-sync-memberships'),
    'valid' => __('Code is valid!','vimeo-sync-memberships'),
    'durationo' => __('first payment only','vimeo-sync-memberships'),
    'durationfor' => __('for','vimeo-sync-memberships'),
    'durationm' => __('months','vimeo-sync-memberships'),
    'durationf' => __('forever','vimeo-sync-memberships'),
    'invalid' => __('Invalid Code - Check Again','vimeo-sync-memberships'),
    'membershipdc' => __('Are you sure you want to cancel this membership?','vimeo-sync-memberships'),
    'membershipc' => __('Cancelling Membership','vimeo-sync-memberships'),
    'membershipd' => __('Deleting Membership','vimeo-sync-memberships'),
    'reactivate' => __('Re-activating Membership','vimeo-sync-memberships'),
    'updating' => __('Updating','vimeo-sync-memberships'),
    'cancelled' => __('cancelled','vimeo-sync-memberships'),
    'purchasetext' => __('Purchase','vimeo-sync-memberships'),
    'rentaltext' => __('Rent','vimeo-sync-memberships'),
    'gettingpurchase' => __('Getting purchase details','vimeo-sync-memberships'),
    'gettingrental' => __('Getting rental details','vimeo-sync-memberships'),
    'pausedtext' => __('Paused','vimeo-sync-memberships'),
    'pausetext' => __('Pause','vimeo-sync-memberships'),
    'cancelledtext' => __('Canceled','vimeo-sync-memberships'),
    'activetext' => __('Active','vimeo-sync-memberships'),
    'trialingtext' => __('Trialing','vimeo-sync-memberships'),
    'overduetext' => __('Overdue','vimeo-sync-memberships'),
    'canceltext' => __('Cancel','vimeo-sync-memberships'),
    'reactivatetext' => __('Re-activate','vimeo-sync-memberships'),
    'discountoff' => __('off','vimeo-sync-memberships'),
    'savingbilling' => __('Saving billing information','vimeo-sync-memberships'),
	'calculatingtotal' => __('Calculating total','vimeo-sync-memberships'),
	'total' => __('Total','vimeo-sync-memberships'),
	'subtotal' => __('Subtotal','vimeo-sync-memberships'),
	'stripe' => array(
		'couponnotsupportedtext' => __('Coupon Codes are currently not supported by Stripe Checkout for subscriptions', 'vimeo-sync-memberships'),
		'completepurchase' => __('Complete Purchase', 'vimeo-sync-memberships')
	),
);

if(!function_exists('rvs_load_language_support')) {
    function rvs_load_language_support() {
        load_plugin_textdomain( 'vimeo-sync-memberships', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
    }
    add_action( 'init', 'rvs_load_language_support' );
}


// CHECK IF STRIPE IS ENABLED
if(isset($wpvs_stripe_options['enabled']) && ! empty($wpvs_stripe_options['enabled']) ) {
    $wpvs_stripe_gateway_enabled = true;
	if(isset($wpvs_stripe_options['stripe_checkout_enabled']) && ! empty($wpvs_stripe_options['stripe_checkout_enabled']) ) {
	    $wpvs_stripe_checkout_enabled = true;
		require_once('include-stripe/checkout/checkout-ajax.php');
	}
} else {
    $wpvs_stripe_gateway_enabled = false;
}

// CHECK IF PAYPAL IS ENABLED
if(isset($rvs_paypal_settings['enabled']) && $rvs_paypal_settings['enabled'] != 0) {
    $rvs_paypal_enabled = true;
} else {
    $rvs_paypal_enabled = false;
}

// CHECK IF COINGATE IS ENABLED
if(isset($wpvs_coingate_settings['enabled']) && $wpvs_coingate_settings['enabled'] != 0) {
    $wpvs_coingate_enabled = true;
} else {
    $wpvs_coingate_enabled = false;
}

// CHECK IF GOOGLE / APPLE PAY IS ENABLED
if(isset($wpvs_stripe_options['google_apple_pay']) && ! empty($wpvs_stripe_options['google_apple_pay']) ) {
    $wpvs_google_apple_enabled = true;
} else {
    $wpvs_google_apple_enabled = false;
}

if( ! function_exists('wpvs_memberships_set_globals') ) {
    function wpvs_memberships_set_globals() {
        global $rvs_current_user;
        global $rvs_currency;
        global $wpvs_currency_label;
        if(is_user_logged_in()) {
            $rvs_current_user = wp_get_current_user();
			$wpvs_stripe_user_ajax_manager = new WPVS_STRIPE_USER_AJAX_MANAGER($rvs_current_user->ID);
			$wpvs_stripe_user_ajax_manager->setup_ajax_functions();
        } else {
            $rvs_current_user = null;
        }
        $wpvs_currency_label = get_option('rvs_currency_custom_label', "");
        if( empty($wpvs_currency_label) ) {
            if($rvs_currency == "USD" || $rvs_currency == "CAD")  {
                $wpvs_currency_label = '$'.strtoupper($rvs_currency);
            } else {
                $wpvs_currency_label = strtoupper($rvs_currency);
            }
        }
    }
    add_action('init', 'wpvs_memberships_set_globals');
}

require_once('includes/polyfills.php');
// LOAD CHECKOUT
require_once('includes/wpvs-load-checkout.php');
// CREATE TABLES
require_once('includes/admin/create-tables.php');
// CREATE CLASSES
require_once('includes/wpvs-classes.php');
// CREATE CUSTOMER CLASS
require_once('includes/wpvs-customer-class.php');

// INCLUDE COINGATE TABLES
require_once('include-coingate/admin/create-tables.php');
// INCLUDE COINGATE CLASSES
require_once('include-coingate/coingate-classes.php');

// INCLUDE COINBASE TABLES
require_once('include-coinbase/admin/create-tables.php');
// INCLUDE COINBASE CLASSES
require_once('include-coinbase/coinbase-classes.php');

// INCLUDE DB FUNCTIONS
require_once('includes/wpvs-wpdb-functions.php');

// INCLUDE EMAIL FUNCTIONS
require_once('email/email-functions.php');

// INCLUDE GLOBAL FUNCTIONS
require_once('includes/rvsfunctions.php');

// INCLUDE PRODUCT FUNCTIONS
require_once('includes/wpvs-product-functions.php');

// INCLUDE PRICING FUNCTIONS
require_once('includes/wpvs-pricing-functions.php');

// INCLUDE ACCESS FUNCTIONS
require_once('includes/access-functions.php');

// INCLUDED DEPRECATED THEME FUNCTIONS
require_once('includes/theme-functions.php');

// INCLUDE DEVICE FUNCTIONS
require_once('includes/device-functions.php');

// CREATE STRIPE TABLES
require_once('include-stripe/checkout/create-tables.php');

// INCLUDE STRIPE CLASSES
require_once('include-stripe/wpvs-stripe-classes.php');

// INCLUDE GLOBAL AJAS FUNCTIONS
require_once('includes/rvs-ajax.php');

// INCLUDE USER AJAX FUNCTIONS
require_once('includes/wpvs-user-ajax.php');

// INCLUDE STRIPE FUNCTIONS
require_once('include-stripe/stripe-functions.php');
include('include-stripe/process-payment.php');

// INCLUDE PAYPAL CLASSES
require_once('include-paypal/wpvs-paypal-classes.php');

// INCLUDE PAYPAL FUNCTIONS
require_once('include-paypal/paypal-functions.php');

// INCLUDE PAYPAL AJAX
require_once('include-paypal/paypal-ajax.php');

// INCLUDED CONTENT FILTER
require_once('includes/content-filter.php');

// INCLUDED EVENTS
require_once('includes/wpvs-events.php');

//INCLUDE ACTIONS
require_once('includes/wpvs-actions.php');

//INCLUDED FILTERS
require_once('includes/wpvs-filters.php');

// INCLUDE REST API
require_once('rest/tables.php');
require_once('rest/wpvs-rest-class.php');
require_once('rest/wpvs-members-rest-api-functions.php');

// INCLUDE CUSTOMER AJAX FUNCTIONS
require_once('include-stripe/customer-ajax-functions.php');
global $wpvs_coinbase_manager;

$wpvs_coinbase_manager = new WPVS\Coinbase\WPVSCoinbaseManager();

// CHECK IF COINBASE IS ENABLED
if( $wpvs_coinbase_manager->is_enabled() ) {
    $wpvs_coinbase_enabled = true;
} else {
    $wpvs_coinbase_enabled = false;
}

if( is_admin() ) {
    require_once('includes/admin/wpvs-admin-classes.php');
    require_once('includes/admin/rvs-admin-ajax.php');
    require_once('includes/admin/wpvs-admin-updates-ajax.php');
    require_once('includes/admin/rvs-admin-functions.php');

	if( $wpvs_stripe_gateway_enabled ) {
		require_once('include-stripe/admin/admin-ajax.php');
	}

    require_once('include-paypal/paypal-admin-ajax.php');
    require_once('updates/automatic-updates.php');
    require_once('includes/admin/payments/wpvs-admin-ajax.php');
    require_once('includes/admin/wpvs-role-manager.php');

    if( ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
        require_once('includes/admin/pages.php');
        require_once('includes/admin/updates.php');
    }
    require_once('includes/admin/admin-scripts.php');
    require_once('includes/admin/meta-boxes.php');
    require_once('includes/admin/genre-meta-boxes.php');

    require_once(RVS_MEMBERS_BASE_DIR.'/updates/wpvs-plugin-filters.php');
    require_once(RVS_MEMBERS_BASE_DIR.'/updates/plugin-update-checker.php');
    $wp_video_membership_updates = Puc_v4_Factory::buildUpdateChecker(
        'https://www.wpvideosubscriptions.com/updates/?action=get_metadata&slug=vimeo-sync-memberships',
        __FILE__,
        'vimeo-sync-memberships'
    );
    $wp_video_membership_updates->addQueryArgFilter('wpvs_membership_update_checks');
    if(strpos($_SERVER['REQUEST_URI'], 'update-core.php') || strpos($_SERVER['REQUEST_URI'], 'plugins.php')) {
        $wp_video_membership_updates->checkForUpdates();
    }
} else {
    include('includes/scripts.php');
    include('includes/shortcodes.php');
    if($wpvs_stripe_gateway_enabled || $wpvs_google_apple_enabled) {
        include('include-stripe/scripts.php');
    }
    if($rvs_paypal_enabled) {
        include('include-paypal/scripts.php');
    }
}

if($wpvs_stripe_gateway_enabled) {
    require_once('include-stripe/webhooks.php');
}
if($rvs_paypal_enabled) {
    require_once('include-paypal/webhooks.php');
}

if($wpvs_coinbase_enabled) {
    require_once(RVS_MEMBERS_BASE_DIR . '/include-coinbase/vendor/autoload.php');
    require_once('include-coinbase/scripts.php');
    require_once('include-coinbase/coinbase-functions.php');
    require_once('include-coinbase/coinbase-ajax.php');
    require_once('include-coinbase/webhooks.php');
}

if($wpvs_coingate_enabled) {
    require_once('include-coingate/scripts.php');
    require_once('include-coingate/coingate-functions.php');
    require_once('include-coingate/coingate-ajax.php');
    require_once('include-coingate/webhooks.php');
}
require_once('includes/wpvs-sessions.php');

function wpvs_memberships_query_vars_filter( $vars ) {
  $vars[] = "id";
  $vars[] = "wpvsview";
  return $vars;
}
add_filter( 'query_vars', 'wpvs_memberships_query_vars_filter' );

function wpvs_memberships_activation() {
	$wpvs_theme_is_active = get_option('wpvs_theme_active');
    global $rvs_current_version;
    // CHECK FOR WP VIDEOS
    if ( current_user_can( 'activate_plugins' ) ) {
		if ( ! is_plugin_active('video-sync-for-vimeo/vimeo-sync.php') && ! $wpvs_theme_is_active ) {
        	$missing_wp_videos_message = 'WP Video Memberships requires the either the <a href="https://en-ca.wordpress.org/plugins/video-sync-for-vimeo/" target="_blank">WP Videos</a> plugin or a WPVS theme to be installed and active. <br><a href="'.admin_url('plugins.php').'">&laquo; Return to Plugins</a>';
        	wp_die($missing_wp_videos_message);
		}
    }
    // CHECK FOR PHP VERSION
    if (version_compare(PHP_VERSION, '5.5') < 0) {
        $upgrade_message = 'You need to upgrade to at least PHP version 5.5 to use the WP Video Memberships plugin. <br><a href="'.admin_url('plugins.php').'">&laquo; Return to Plugins</a>';
        wp_die($upgrade_message, 'PHP Version Update Required');
    }
    if( ! get_option('rvs_membership_pages_created')) {
        $rvs_sign_up_page_details = array(
          'post_type' => 'page',
          'post_title'    => 'Register',
          'post_content' => '[rvs_memberships]',
          'post_status'   => 'publish'
        );

        $rvs_payment_page_details = array(
          'post_type' => 'page',
          'post_title'    => 'Checkout',
          'post_content' => '[rvs_payment_form]',
          'post_status'   => 'publish'
        );

        $rvs_account_page_details = array(
          'post_type' => 'page',
          'post_title'    => 'Account',
          'post_content' => '[rvs_account]',
          'post_status'   => 'publish'
        );

        $rvs_create_account_page_details = array(
          'post_type' => 'page',
          'post_title'    => 'Create Account',
          'post_content' => '[rvs_create_account]',
          'post_status'   => 'publish'
        );

        $rvs_user_rentals_page = array(
          'post_type' => 'page',
          'post_title'    => 'Rentals',
          'post_content' => '[rvs_user_rentals]',
          'post_status'   => 'publish'
        );

        $rvs_user_purchases_page = array(
          'post_type' => 'page',
          'post_title'    => 'Purchases',
          'post_content' => '[rvs_user_purchases]',
          'post_status'   => 'publish'
        );
        if(get_option('wpvs_theme_active')) {
            $rvs_sign_up_page_details['page_template'] = 'page_account.php';
            $rvs_payment_page_details['page_template'] = 'page_account.php';
            $rvs_account_page_details['page_template'] = 'page_account.php';
            $rvs_create_account_page_details['page_template'] = 'page_account.php';
            $rvs_user_rentals_page['page_template'] = 'page_account.php';
            $rvs_user_purchases_page['page_template'] = 'page_account.php';
        }
        // Insert the post into the database
        $sign_up_id = wp_insert_post( $rvs_sign_up_page_details );
        $payment_id = wp_insert_post( $rvs_payment_page_details );
        $account_id = wp_insert_post( $rvs_account_page_details );
        $create_account_id = wp_insert_post( $rvs_create_account_page_details );
        $user_rentals = wp_insert_post( $rvs_user_rentals_page );
        $user_purchases = wp_insert_post( $rvs_user_purchases_page);
        add_option('rvs_sign_up_page', $sign_up_id);
        add_option('rvs_payment_page', $payment_id);
        add_option('rvs_account_page', $account_id);
        add_option('rvs_create_account_page', $create_account_id);
        add_option('rvs_user_rental_page', $user_rentals);
        add_option('rvs_user_purchase_page', $user_purchases);
    }
    update_option('rvs_is_activated', true);
    update_option('rvs_membership_pages_created', true);
    update_option('rvs_memberships_version', '4.7.8');
    update_option('wpvs-memberships-updated', '4.7.8');
	update_option('wpvs_stripe_api_version', '2020-03-02');
    wpvs_create_payment_tables();
    wpvs_create_members_tables();
    wpvs_create_coin_payment_tables();
    wpvs_create_coinbase_payment_tables();
    wpvs_create_rest_api_tables();
	wpvs_create_stripe_checkout_tables();
	$wpvs_tax_rate_manager = new WPVS_Tax_Rate_Manager();
	$wpvs_tax_rate_manager->create_tax_rate_table();
    add_role( 'wpvs_member', 'Member', array( 'read' => true ) );
    add_role( 'wpvs_test_member', 'TEST Member', array( 'read' => true ) );
    if( ! wp_next_scheduled( 'wpvs_member_renewal_reminders_event', array(0) ) ) {
        wp_schedule_event( time() + 10, 'hourly', 'wpvs_member_renewal_reminders_event', array(0));
    }
    if( ! wp_next_scheduled( 'wpvs_clear_member_html_emails_event' ) ) {
        wp_schedule_event( time() + 10, 'daily', 'wpvs_clear_member_html_emails_event');
    }
    if( ! wp_next_scheduled( 'wpvs_clear_old_coin_gate_orders_event' ) ) {
        wp_schedule_event( time() + 10, 'daily', 'wpvs_clear_old_coin_gate_orders_event');
    }
	if( ! wp_next_scheduled( 'wpvs_clear_old_stripe_checkout_sessions_event' ) ) {
        wp_schedule_event( time() + 10, 'daily', 'wpvs_clear_old_stripe_checkout_sessions_event');
    }
}
register_activation_hook( __FILE__, 'wpvs_memberships_activation' );
function rvs_membership_admin_items_create() {
    global $wpvs_membership_update_required; ?>
    <a href="<?php echo admin_url('admin.php?page=rvs-settings'); ?>" title="Membership Settings" class="rvs-tab"><span class="dashicons dashicons-admin-generic"></span> Membership Settings</a>
    <a href="<?php echo admin_url('admin.php?page=wpvs-checkout-settings'); ?>" title="Checkout Settings" class="rvs-tab"><span class="dashicons dashicons-migrate"></span> Checkout Settings</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-memberships'); ?>" title="Memberships" class="rvs-tab"><span class="dashicons dashicons-lock"></span> Memberships</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-members'); ?>" title="Members" class="rvs-tab"><span class="dashicons dashicons-groups"></span> Members</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-payment-settings&tab=stripe'); ?>" title="Payment Gateway" class="rvs-tab"><span class="dashicons dashicons-store"></span> Payment Gateway</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-payments'); ?>" title="Payments" class="rvs-tab"><span class="dashicons dashicons-clipboard"></span> Payments</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-email-settings&tab=setup'); ?>" title="Email" class="rvs-tab"><span class="dashicons dashicons-email-alt"></span> Email</a>
    <a href="<?php echo admin_url('admin.php?page=wpvs-reminder-settings&tab=renewal'); ?>" title="Reminders" class="rvs-tab"><span class="dashicons dashicons-clock"></span> Reminders</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-coupon-codes'); ?>" title="Coupons" class="rvs-tab"><span class="dashicons dashicons-tickets-alt"></span> Coupons</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-webhooks'); ?>" title="Webhooks" class="rvs-tab"><span class="dashicons dashicons-info"></span> Webhooks</a>
    <a href="<?php echo admin_url('admin.php?page=wpvs-updates'); ?>" title="Run Updates" class="rvs-tab"><span class="dashicons dashicons-update"></span> Run Updates<?=($wpvs_membership_update_required) ? '<span class="dashicons dashicons-warning wpvs-update-needed rvs-error"></span>' : ''?></a>
<?php }
add_action('rvs_membership_admin_items', 'rvs_membership_admin_items_create');

function wpv_memberships_deactivation() {
    update_option('rvs_is_activated', false);
    wp_clear_scheduled_hook('wpvs_member_renewal_reminders_event', array(0));
    wp_clear_scheduled_hook('wpvs_clear_member_html_emails_event');
}
register_deactivation_hook( __FILE__, 'wpv_memberships_deactivation' );
