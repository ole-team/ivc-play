<?php

if( ! function_exists('wpvs_calculate_coupon_amount') ) {
    function wpvs_calculate_coupon_amount($charge_amount, $coupon_code) {
        $rvs_coupon_codes = get_option('rvs_coupon_codes');
        $new_amount = $charge_amount;
        $use_coupon = null;
        if(!empty($rvs_coupon_codes)) {
            foreach($rvs_coupon_codes as $key => &$coupon) {
                if($coupon['id'] == $coupon_code) {
                    if( isset($coupon['max_uses']) && ! empty($coupon['max_uses']) ) {
                        if( isset($coupon['used_count']) && ! empty($coupon['used_count']) ) {
                            if( intval($coupon['used_count']) >= intval($coupon['max_uses']) ) {
                                break;
                            }
                        }
                    }
                    $use_coupon = $coupon;
                    break;
                }
            }
        }

        if( ! empty($use_coupon) ) {
            $coupon_type = $coupon['type'];
            $coupon_amount = $coupon['amount'];
            if($coupon_type == "amount") {
                $new_amount = intval($charge_amount - $coupon_amount);
            } else {
                $percentage_off = floatval($coupon_amount)/100;
                $amount_off = $charge_amount*$percentage_off;
                $new_amount = floatval($charge_amount - $amount_off);
            }
            if( $new_amount < 0 ) {
                $new_amount = 0;
            }
        }
        return number_format($new_amount, 0, '', '');
    }
}

if( ! function_exists('wpvs_add_coupon_code_use') ) {
    function wpvs_add_coupon_code_use($coupon_code, $user_id) {
        $rvs_coupon_codes = get_option('rvs_coupon_codes');
        if( ! empty($rvs_coupon_codes) ) {
            foreach($rvs_coupon_codes as $key => &$coupon) {
                if($coupon['id'] == $coupon_code) {
                    $current_coupon_uses = 0;
                    if( isset($coupon['used_count']) && ! empty($coupon['used_count']) ) {
                        $current_coupon_uses = intval($coupon['used_count']);
                    }
                    $current_coupon_uses++;
                    $coupon['used_count'] = $current_coupon_uses;
                    break;
                }
            }
            update_option('rvs_coupon_codes', $rvs_coupon_codes);
        }
        $user_coupon_code_usage = get_user_meta($user_id, 'wpvs-coupon-code-usage', true);
        if( ! empty($user_coupon_code_usage) ) {
            foreach($user_coupon_code_usage as $key => &$coupon_used) {
                if( $coupon_used['id'] == $coupon_code) {
                    $current_used_count = intval($coupon_used['used_count']);
                    $current_used_count++;
                    $coupon_used['used_count'] = $current_used_count;
                }
            }
        } else {
            $user_coupon_code_usage = array();
            $new_coupon_used = array(
                'id' => $coupon_code,
                'used_count' => 1
            );
            $user_coupon_code_usage[] = $new_coupon_used;
        }
        update_user_meta($user_id, 'wpvs-coupon-code-usage', $user_coupon_code_usage);
    }
}

if( ! function_exists('wpvs_check_customer_coupon_usage') ) {
    function wpvs_check_customer_coupon_usage($user_id, $check_coupon_id, $coupon_max_uses_customer) {
        $usage_allowed = true;
        $user_coupon_code_usage = get_user_meta($user_id, 'wpvs-coupon-code-usage', true);
        if( ! empty($user_coupon_code_usage) ) {
            foreach($user_coupon_code_usage as $coupon_used) {
                if( $coupon_used['id'] == $check_coupon_id) {
                    if( intval($coupon_used['used_count']) >= $coupon_max_uses_customer ) {
                        $usage_allowed = false;
                    }
                }
            }
        }
        return $usage_allowed;
    }
}
