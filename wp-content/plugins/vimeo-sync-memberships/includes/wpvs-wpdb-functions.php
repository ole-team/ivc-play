<?php

function wpvs_add_new_member($user_id, $test) {
    global $wpdb;
	
    if($test) {
        $wpvs_members_table_name = $wpdb->prefix . 'wpvs_test_members';
    } else {
        $wpvs_members_table_name = $wpdb->prefix . 'wpvs_members';
    }
    
    $wpvs_member_check = wpvs_get_member_by_id($user_id, $test);

    if( empty($wpvs_member_check) ) {
        $wpdb->insert( 
            $wpvs_members_table_name, 
            array( 
                'user_id' => $user_id
            )
        );
    }
}

function wpvs_get_member_by_id($user_id, $test) {
    global $wpdb;
	
    if($test) {
        $wpvs_members_table_name = $wpdb->prefix . 'wpvs_test_members';
    } else {
        $wpvs_members_table_name = $wpdb->prefix . 'wpvs_members';
    }
    
    $wpvs_member = $wpdb->get_results("SELECT * FROM $wpvs_members_table_name WHERE user_id='$user_id'");
    return $wpvs_member;
}

function wpvs_get_stripe_payment($charge_id, $test) {
    global $wpdb;
	
    if($test) {
        $wpvs_payments_table_name = $wpdb->prefix . 'rvs_test_payments';
    } else {
        $wpvs_payments_table_name = $wpdb->prefix . 'rvs_payments';
    }
    
    $wpvs_payment = $wpdb->get_results("SELECT * FROM $wpvs_payments_table_name WHERE paymentid='$charge_id' AND gateway='stripe'");
    return $wpvs_payment;
}

function wpvs_get_paypal_payment($payment_id, $test) {
    global $wpdb;
	
    if($test) {
        $wpvs_payments_table_name = $wpdb->prefix . 'rvs_test_payments';
    } else {
        $wpvs_payments_table_name = $wpdb->prefix . 'rvs_payments';
    }
    
    $wpvs_payment = $wpdb->get_results("SELECT * FROM $wpvs_payments_table_name WHERE paymentid='$payment_id' AND gateway='paypal'");
    return $wpvs_payment;
}

function wpvs_get_customer_db_paypal_payments($user_id, $test, $offset) {
    global $wpdb;
	
    if($test) {
        $wpvs_payments_table_name = $wpdb->prefix . 'rvs_test_payments';
    } else {
        $wpvs_payments_table_name = $wpdb->prefix . 'rvs_payments';
    }
    
    $wpvs_customer_payments = $wpdb->get_results("SELECT * FROM $wpvs_payments_table_name WHERE userid='$user_id' AND gateway='paypal' ORDER BY time DESC LIMIT 100 OFFSET $offset");
    return $wpvs_customer_payments;
}

function wpvs_get_customer_db_stripe_payments($user_id, $test, $offset) {
    global $wpdb;
	
    if($test) {
        $wpvs_payments_table_name = $wpdb->prefix . 'rvs_test_payments';
    } else {
        $wpvs_payments_table_name = $wpdb->prefix . 'rvs_payments';
    }
    
    $wpvs_customer_payments = $wpdb->get_results("SELECT * FROM $wpvs_payments_table_name WHERE userid='$user_id' AND gateway='stripe' ORDER BY time DESC LIMIT 100 OFFSET $offset");
    return $wpvs_customer_payments;
}