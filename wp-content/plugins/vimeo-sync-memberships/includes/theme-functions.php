<?php

/* === DEPRECATED USED BY WPVS THEMES === */

function wpvs_user_has_membership($user_id, $membership_id) {
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $get_memberships = 'rvs_user_memberships_test';
    } else {
        $get_memberships = 'rvs_user_memberships';
    }
    $has_membership = false;
    $user_memberships = get_user_meta($user_id, $get_memberships, true);
    if(!empty($user_memberships)) {
        foreach($user_memberships as $key => $membership) {
            if($membership["plan"] == $membership_id) {
                $has_membership = true;
                break;
            }
        }
    }
    return $has_membership;
}

function rvs_has_membership($user, $membership_array, $post_id) {
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $get_memberships = 'rvs_user_memberships_test';
    } else {
        $get_memberships = 'rvs_user_memberships';
    }
    $has_membership = false;
    $no_access_reason = "nosubscription";
    $no_access_message = __('Sorry, you do not have access to this video.', 'vimeo-sync-memberships');
    $rvs_overdue_access = get_option('rvs_overdue_access', 0);
    
    $user_memberships = get_user_meta($user->ID, $get_memberships, true);
    if(!empty($user_memberships) && !empty($membership_array)) {
        foreach($user_memberships as $key => $membership) {
            if(in_array($membership["plan"], $membership_array)) {
                $membership_type = 'stripe';
                if( isset($membership['type']) ) {
                    if($membership['type'] == "stripe" || $membership['type'] == "stripe_test") {
                        $membership_type = 'stripe';
                    }
                    if($membership['type'] == "paypal" || $membership['type'] == "paypal_test") {
                        $membership_type = 'paypal';
                    }
                    if($membership['type'] == "coingate" || $membership['type'] == "coingate_test") {
                        $membership_type = 'coingate';
                    }
                    
                    if($membership['type'] == "coinbase" || $membership['type'] == "coinbase_test") {
                        $membership_type = 'coinbase';
                    }
                    
                    if($membership['type'] == "manual") {
                        $membership_type = 'manual';
                    }
                }
                
                if( $rvs_overdue_access && ($membership["status"] == "past_due" || $membership["status"] == "unpaid" || $membership["status"] == "incomplete" || $membership["status"] == "incomplete_expired") ) {
                    $no_access_reason = "overdue";
                    $no_access_message = __('Your subscription payment is overdue', 'vimeo-sync-memberships');
                } else {
                    $wpvs_current_time = current_time('timestamp', 1);
                    $membership_ends = $membership["ends"];
                    if($membership_ends != "never") {
                        $membership_expires = $membership_ends;
                        if( $membership_type == 'paypal' ) {
                            $membership_expires = strtotime($membership_ends);
                        }

                        if( intval($membership_expires) > $wpvs_current_time) {
                            if( $membership_type == 'coingate' || $membership_type == 'coinbase' ) {
                                if( $membership_type == 'coingate' ) {
                                    if($membership["status"] == "new" || $membership["status"] == "pending" || $membership["status"] == "invalid" || $membership["status"] == "expired") {
                                        $no_access_reason = "overdue";
                                        $no_access_message = __('Your subscription is awaiting payment', 'vimeo-sync-memberships');
                                    }
                                    if($membership["status"] == "confirming") {
                                        $no_access_reason = "overdue";
                                        $no_access_message = __('Subscription is waiting for payment confirmation', 'vimeo-sync-memberships');
                                    }
                                    if($membership["status"] == "paid") {
                                        $has_membership = true;
                                        $no_access_reason = null;
                                        $no_access_message = null;
                                    }
                                }

                                if( $membership_type == 'coinbase' ) {
                                    if($membership["status"] == "NEW" || $membership["status"] == "UNRESOLVED" || $membership["status"] == "EXPIRED") {
                                        $no_access_reason = "overdue";
                                        $no_access_message = __('Your subscription is awaiting payment', 'vimeo-sync-memberships');
                                    }
                                    if($membership["status"] == "PENDING") {
                                        $no_access_reason = "overdue";
                                        $no_access_message = __('Your subscription is waiting for payment confirmation', 'vimeo-sync-memberships');
                                    }
                                    if($membership["status"] == "COMPLETED" || $membership["status"] == "CONFIRMED") {
                                        $no_access_reason = null;
                                        $no_access_message = null;
                                    }
                                }
                            } else {
                                if( $membership_type == "manual" ) {
                                    unset($user_memberships[$key]);
                                    update_user_meta($user->ID, $get_memberships, $user_memberships);
                                    wpvs_do_after_delete_membership_action($user->ID, $membership);
                                }
                            }
                        }
                    } else {
                        $has_membership = true;
                        $no_access_reason = null;
                        $no_access_message = null;
                    }
                }
                if( $has_membership ) {
                    break;
                }
            }
        }
    }
    
    $wpvs_custom_access_filters = wpvs_custom_has_access_check_action($user, $membership_array, $user_memberships, $post_id);
    
    if( ! empty($wpvs_custom_access_filters) ) {
        if( isset($wpvs_custom_access_filters['has_access']) ) {
            $has_membership = $wpvs_custom_access_filters['has_access'];
        }
        if( isset($wpvs_custom_access_filters['no_access_reason']) && ! empty($wpvs_custom_access_filters['no_access_reason']) ) {
            $no_access_reason = $wpvs_custom_access_filters['no_access_reason'];
        }
        if( isset($wpvs_custom_access_filters['message']) && !empty($wpvs_custom_access_filters['message']) ) {
            $no_access_message = $wpvs_custom_access_filters['message'];
        }
    }
    
    // CHECK ONETIME PAYMENTS
    if(!$has_membership) {
        $user_video_access = get_user_meta($user->ID, 'rvs_user_video_access', true);
        if(!empty($user_video_access) && in_array($post_id, $user_video_access)) {
            $has_membership = true;
            $no_access_reason = null;
        }
    }
    return array('has_access' => $has_membership, 'reason' => $no_access_reason, 'message' => $no_access_message);
}