<?php

/*
* Class for creating WPVS filters
*
*/

class WPVS_Custom_Content_Filters {
    
    public function __construct() {}

    // Check custom registration fields filter
    public function check_custom_registration_form_fields($form_data) {
        $form_fields = array( 'error' => false, 'message' => null );
        if( has_filter('wpvs_check_custom_registration_form_fields') ) {
            $form_fields = apply_filters( 'wpvs_check_custom_registration_form_fields', $form_data);
        }
        return $form_fields;
    }
    
    public function apply_custom_registration_form_fields($form_data, $user_id) {
        if( has_filter('wpvs_apply_custom_registration_form_fields') ) {
            apply_filters( 'wpvs_apply_custom_registration_form_fields', $form_data, $user_id);
        }
    }
}
