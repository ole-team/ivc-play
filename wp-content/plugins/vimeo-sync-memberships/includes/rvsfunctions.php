<?php

class WPVS_Memberships_User_Redirect_Manager {

    public function __construct() {
        if( get_option('rvs_admin_access') ) {
            add_action('init', array($this, 'wpvs_restrict_admin_access'));
            add_action('init', array($this, 'wpvs_hide_user_toolbar'));
            add_action('wp_logout', array($this, 'wpvs_logout_home_redirect'));
            add_filter('template_redirect', array($this, 'wpvs_redirect_create_account_page_users'));
            add_action( 'wp_login_failed', [$this, 'wpvs_user_login_fail_filters'], 10, 2);
            add_filter('login_redirect', [$this, 'wpvs_user_account_login_redirect_filters'], 10, 3 );
            add_action('wp_authenticate', [$this, 'wpvs_login_wp_authenticate'], 10, 2 );
        }

    }

    public function wpvs_restrict_admin_access() {
        if ( is_user_logged_in() && is_admin() && ! current_user_can( 'administrator' ) &&
           ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
            $rvs_account_page = get_option('rvs_account_page');
            $rvs_account_link = get_permalink($rvs_account_page);
            wp_redirect( $rvs_account_link );
            exit;
        }
    }

    public function wpvs_hide_user_toolbar() {
        if( ! current_user_can( 'manage_options' ) ) {
            add_filter('show_admin_bar', '__return_false');
        }
    }

    public function wpvs_user_account_login_redirect_filters( $redirect_to, $request, $user ) {
        if ( isset( $user->roles ) && is_array( $user->roles ) ) {
            $redirect_url = $request;
    		if ( in_array( 'administrator', $user->roles ) ) {
                if( empty($redirect_url) ) {
                    return admin_url();
                } else {
                    return $redirect_url;
                }
    		} else {
                $rvs_redirect_login = get_option('rvs_redirect_login', 'same_page');
                if( ! empty($rvs_redirect_login) && $rvs_redirect_login == "home") {
                    $redirect_url = home_url();
                }
                if( ! empty($rvs_redirect_login) && $rvs_redirect_login == "custom") {
                    $redirect_url = get_option('rvs_redirect_login_link');
                }
                if( ! empty($rvs_redirect_login) && $rvs_redirect_login != "home" && $rvs_redirect_login != "same_page" && $rvs_redirect_login != "custom") {
                    $redirect_url = get_permalink($rvs_redirect_login);
                }
    			return $redirect_url;
    		}
    	} else {
    		return $redirect_to;
    	}
    }

    public function wpvs_redirect_create_account_page_users() {
        if( is_user_logged_in() && ! current_user_can('manage_options') ) {
            global $post;
            if( $post && ! empty($post->ID) && $post->ID == get_option('rvs_create_account_page') ) {
                $rvs_redirect_login = get_option('rvs_redirect_login', 'same_page');
                if( ! empty($rvs_redirect_login) && $rvs_redirect_login == "home") {
                    $wpvs_redirect_url = home_url();
                }

                if( ! empty($rvs_redirect_login) && $rvs_redirect_login == "custom") {
                    $wpvs_redirect_url = get_option('rvs_redirect_login_link');
                }

                if( ! empty($rvs_redirect_login) && $rvs_redirect_login != "home" && $rvs_redirect_login != "same_page" && $rvs_redirect_login != "custom") {
                    $wpvs_redirect_url = get_permalink($rvs_redirect_login);
                }

                if( empty($wpvs_redirect_url) ) {
                    $wpvs_account_page = get_option('rvs_account_page');
                    $wpvs_redirect_url = get_permalink($wpvs_account_page);
                }
                wp_redirect($wpvs_redirect_url);
            }
        }
    }

    public function wpvs_user_login_fail_filters( $username, $wp_error ) {
        if(isset($_POST['wp-submit'])) {
            if(isset($_SERVER['HTTP_REFERER'])) {
               $referrer = $_SERVER['HTTP_REFERER'];
               if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
                   if(strpos($referrer, "?failed")) {
                        $new_referrer = explode("?",$referrer);
                        $referrer = $new_referrer[0];
                    }
                    if(strpos($referrer, "?")) {
                        $qst = "&";
                    }    else {
                        $qst = "?";
                    }
                    if( ! empty($wp_error) && isset($wp_error->errors['wpvs_login_limit_reached']) ) {
                        $failed_text = $qst.'failed=loginlimit';
                    } else {
                        $failed_text = $qst.'failed=incorrect';
                    }
                    $redirect = $referrer.$failed_text;
                    wp_redirect( $redirect );
                    exit;
                }
            }
        }
    }

    public function wpvs_login_wp_authenticate($username, $password) {
        if(isset($_POST['wp-submit']) && isset($_SERVER['HTTP_REFERER'])) {
            $referrer = $_SERVER['HTTP_REFERER'];
            if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
                if(strpos($referrer, "?failed")) {
                    $new_referrer = explode("?",$referrer);
                    $referrer = $new_referrer[0];
                }

                if(strpos($referrer, "?")) {
                    $qst = "&";
                }    else {
                    $qst = "?";
                }
                $failed_text = $qst.'failed=empty';
                $redirect = $referrer.$failed_text;
                if ( empty($username) ) {
                    wp_redirect($redirect);
                    exit;
                }

                if ( empty($password) ) {
                    wp_redirect($redirect);
                    exit;
                }
            }
        }
    }

    public function wpvs_logout_home_redirect() {
      wp_redirect( home_url() );
      exit;
    }
}
$wpvs_memberships_user_redirect_manager = new WPVS_Memberships_User_Redirect_Manager();


function wpvs_get_stripe_customer($member_stripe_id) {
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $customer_key = 'rvs_stripe_customer_test';
    } else {
        $customer_key = 'rvs_stripe_customer';
    }
    $stripe_user_args = array(
        'meta_query' => array(
            array(
                'key' => $customer_key,
                'value' => $member_stripe_id,
                'compare' => '='
            )
        ),
        'orderby'      => 'registered',
        'order'        => 'ASC',
        'count_total'  => false,
        'fields'       => array('ID', 'user_login')
     );
     $stripe_users = get_users( $stripe_user_args );

     if(! empty($stripe_users) ) {
        foreach($stripe_users as $stripe_customer) {
            $stripe_customer_id = get_user_meta($stripe_customer->ID, $customer_key, true);
            if(!empty($stripe_customer_id) && ($stripe_customer_id == $member_stripe_id)) {
                return $stripe_customer;
                exit;
            }
        }
    } else {
        return null;
    }
}

function rvs_not_logged_in() {
    $wpvs_users_can_register = get_option( 'users_can_register' );
    if($wpvs_users_can_register) {
        $not_logged_in = '<div class="wpvs-text-align-center no-access-message">'. apply_filters( 'wpvs_custom_sign_in_create_account_text', __('Please Sign In or Create an account', 'vimeo-sync-memberships') ) . '</div>';
    } else {
        $not_logged_in = '<div class="wpvs-text-align-center no-access-message">'. apply_filters( 'wpvs_custom_sign_in_only_text', __('Please Sign In', 'vimeo-sync-memberships') ) . '</div>';
    }
    ob_start();
    include(RVS_MEMBERS_BASE_DIR .'/template/wpvs-login-form.php');
    $not_logged_in .= ob_get_contents();
    ob_end_clean();
    return $not_logged_in;
}

function rvs_check_login_errors() {
    $login_error = "";
    if(isset($_GET['failed']) && !empty($_GET['failed'])) {
        if($_GET['failed'] == "incorrect") {
            $login_error = '<p class="rvs-error">'.__('Username or password is incorrect.', 'vimeo-sync-memberships').'</p>';
        }
        if($_GET['failed'] == "empty") {
            $login_error = '<p class="rvs-error">'.__('Username or password cannot be empty.', 'vimeo-sync-memberships').'</p>';
        }
        if($_GET['failed'] == "loginlimit") {
            $login_error = '<p class="rvs-error">'.__('You have hit your login limit. Please logout from other devices to continue.', 'vimeo-sync-memberships').'</p>';
        }
    }
    if(!empty($login_error)) {
        return $login_error;
    }
}

function wpvs_translate_intervals($interval_count, $interval) {
    $interval_text = "";
    $is_plural = false;
    if( $interval_count > 1 ) {
        $is_plural = true;
    }
    if($interval == "day") {
        if( $is_plural ) {
            $interval_text = __('days', 'vimeo-sync-memberships');
        } else {
            $interval_text = __('day', 'vimeo-sync-memberships');
        }
    }
    if($interval == "week") {
        if( $is_plural ) {
            $interval_text = __('weeks', 'vimeo-sync-memberships');
        } else {
            $interval_text = __('week', 'vimeo-sync-memberships');
        }
    }
    if($interval == "month") {
        if( $is_plural ) {
            $interval_text = __('months', 'vimeo-sync-memberships');
        } else {
            $interval_text = __('month', 'vimeo-sync-memberships');
        }
    }
    if($interval == "year") {
        if( $is_plural ) {
            $interval_text = __('years', 'vimeo-sync-memberships');
        } else {
            $interval_text = __('year', 'vimeo-sync-memberships');
        }
    }
    return $interval_text;
}

function wpvs_get_additional_payment_options($wpvs_video_terms) {
    $wpvs_video_term_memberships = array();
    $wpvs_video_term_purchase_options = array();
    if( ! empty($wpvs_video_terms) && ! is_wp_error($wpvs_video_terms) ) {
        foreach($wpvs_video_terms as $term_id => $parent_term) {
            $wpvs_video_cat_memberships = get_term_meta($term_id, 'wpvs_category_memberships', true);
            $wpvs_term_purchase_price = get_term_meta($term_id, 'wpvs_category_purchase_price', true);
            if( ! empty($parent_term) ) {
                $wpvs_parent_cat_memberships = get_term_meta($parent_term, 'wpvs_category_memberships', true);
                $wpvs_parent_purchase_price = get_term_meta($parent_term, 'wpvs_category_purchase_price', true);
                if( ! empty($wpvs_parent_cat_memberships) ) {
                    foreach($wpvs_parent_cat_memberships as $term_membership) {
                        $wpvs_video_term_memberships[] = $term_membership;
                    }
                }

                if( ! empty($wpvs_parent_purchase_price) ) {
                    $wpvs_video_term_purchase_options[] = intval($parent_term);
                }

            }
            if( ! empty($wpvs_video_cat_memberships) ) {
                foreach($wpvs_video_cat_memberships as $term_membership) {
                    $wpvs_video_term_memberships[] = $term_membership;
                }
            }

            if( ! empty($wpvs_term_purchase_price) ) {
                $wpvs_video_term_purchase_options[] = intval($term_id);
            }
        }
        $wpvs_video_term_memberships = array_unique($wpvs_video_term_memberships);
        $wpvs_video_term_purchase_options = array_unique($wpvs_video_term_purchase_options);
    }
    return array( 'memberships' => $wpvs_video_term_memberships, 'purchases' => $wpvs_video_term_purchase_options);
}

function rvs_exit_ajax($message, $error_code) {
    if( empty($error_code) ) {
        $error_code = 400;
    }
    if( empty($message) ) {
        $message = __('Something went wrong processing your request', 'vimeo-sync-memberships');
    }
    status_header($error_code);
    echo $message;
    exit;
}

function wpvs_generate_user_memberships_output($user_memberships, $is_admin) {
    global $rvs_currency;
    $wpvs_current_time = current_time('timestamp', 1);
    $wpvs_users_can_delete_memberships = get_option('wpvs_users_can_delete_memberships', 1);
    $gateway = "Stripe";
    $wpvs_user_memberships_html = "";
    if( $is_admin ) {
        $wpvs_user_memberships_html .= '<table id="memberships" class="rvs_memberships">';
    } else {
        $wpvs_user_memberships_html .= '<table class="rvs_memberships">';
    }
    $wpvs_user_memberships_html .= '<tbody><tr>';
    $wpvs_user_memberships_html .= '<th>'.__('Membership', 'vimeo-sync-memberships').'</th>';
    $wpvs_user_memberships_html .= '<th>'.__('Price', 'vimeo-sync-memberships').'</th>';
    $wpvs_user_memberships_html .= '<th>'.__('Next Payment', 'vimeo-sync-memberships').'</th>';
    $wpvs_user_memberships_html .= '<th>'.__('Status', 'vimeo-sync-memberships').'</th>';
    if( $is_admin ) {
        $wpvs_user_memberships_html .= '<th>'.__('Gateway', 'vimeo-sync-memberships').'</th>';
        $wpvs_user_memberships_html .= '<th>'.__('Update', 'vimeo-sync-memberships').'</th>';
    }
    if( $wpvs_users_can_delete_memberships || $is_admin ) {
        $wpvs_user_memberships_html .= '<th class="rvs-remove-edit">'.__('Manage', 'vimeo-sync-memberships').'</th>';
    }
    foreach($user_memberships as $membership) {
        $is_manual_membership = false;
        $membership_interval = $membership["interval"];
        $membership_discount = null;
        $membership_ends_next_date = false;
        if(isset($membership['discount']) && !empty($membership['discount'])) {
            $membership_discount = $membership['discount'];
            if(!empty($membership_discount['amount_off'])) {
                if( isset($membership_discount['type']) && $membership_discount['type'] == 'paypal' ) {
                    $discount_amount = number_format(($membership_discount['amount_off'])/100, 2);
                } else {
                    $discount_amount = number_format(($membership["amount"] - $membership_discount['amount_off'])/100, 2);
                }

                $discount_amount =  $rvs_currency .' '. $discount_amount;
            } else {
                $discount_amount = $membership_discount['percent_off'].'% off ';
            }

            if($membership_discount['duration'] == "repeating") {
                $discount_left = 'for '.$membership_discount['duration_in_months'] . ' months';
            } elseif($membership_discount['duration'] == "once") {
                $discount_left = 'first payment only';
            } else {
                $discount_left = 'forever';
            }
        }
        $remove_text = __('Delete', 'vimeo-sync-memberships');
        $membership_status = $membership["status"];
        $status_update_class = "";
        $wpvs_user_memberships_html .= '<tr class="rvs-membership-editor">';
        $membership_type = 'stripe';
        if( isset($membership['type']) ) {
            # IF STRIPE WAS USED FOR PAYMENT
            if($membership['type'] == "stripe" || $membership['type'] == "stripe_test") {
                $membership_type = 'stripe';
            }

            # IF PAYPAL WAS USED FOR PAYMENT
            if($membership['type'] == "paypal" || $membership['type'] == "paypal_test") {
                $membership_type = 'paypal';
            }

            # IF IS MANUAL MEMBERSHIP
            if($membership['type'] == "manual") {
                $membership_type = 'manual';
            }

            # IF COINGATE WAS USED FOR PAYMENT
            if($membership['type'] == "coingate" || $membership['type'] == "coingate_test") {
                $membership_type = 'coingate';
            }

            # IF COINBASE WAS USED FOR PAYMENT
            if($membership['type'] == "coinbase" || $membership['type'] == "coinbase_test") {
                $membership_type = 'coinbase';
            }
        }

        if(isset($membership['ends_next_date']) && ! empty($membership['ends_next_date'])) {
            $membership_ends_next_date = true;
            $membership_status = 'canceled';
        }

        if( $membership_type == 'stripe' ) {
            $gateway = "Stripe";
            if($membership["ends"] < $wpvs_current_time) {
                $next_payment_date = __('expired', 'vimeo-sync-memberships');
            } else {
                $next_payment_date = wp_date( 'M d, Y', $membership["ends"]);
            }
            if( $is_admin ) {
                $sync_button = '<label class="rvs-sync-membership sync-stripe-plan" data-sync="'.$membership["id"].'">Sync</label>';
            }
            $remove_button = '<label class="removeMembership deleteNow">'.$remove_text.'</label>';
            $membership_id_values = '<input type="hidden" class="membership-sub-id" value="' . $membership["id"] . '"/>';
            $status_update_class = "wpvs-stripe-update-membership";
        }

        if( $membership_type == 'paypal' ) {
            $gateway = "PayPal";
            if(strtotime($membership["ends"]) < $wpvs_current_time) {
                $next_payment_date = __('expired', 'vimeo-sync-memberships');
            } else {
                $next_payment_date = wp_date( 'M d, Y', strtotime($membership["ends"]));
            }
            if( $is_admin ) {
                $sync_button = '<label class="rvs-sync-membership sync-paypal-plan" data-sync="'.$membership["id"].'">Sync</label>';
            }
            $remove_button = '<label class="removePayPalPlan">'.$remove_text.'</label>';
            $membership_id_values = '<input type="hidden" class="membership-sub-id" value="' . $membership["plan"] . '"/><input type="hidden" class="paypal-id" value="' . $membership["id"] . '"/>';
            $status_update_class = "wpvs-paypal-update-membership";
        }

        if($membership_type == "manual") {
            $gateway = "None";
            $next_payment_date = $membership["ends"];
            $remove_button = '<label class="rvs-remove removeManMembership">'.$remove_text.'</label>';
            $membership_id_values = '<input type="hidden" class="membership-sub-id" value="' . $membership["plan"] . '"/>';
            $is_manual_membership = true;
            if( $is_admin ) {
                $sync_button = 'N/A';
                if($next_payment_date == "never") {
                    $select_new_date_label = "Select date (optional)";
                } else {
                    $select_new_date_label = wp_date('F d, Y', $membership["ends"]);
                }
            }
        }

        if($membership_type == "coingate") {
            $gateway = __('CoinGate', 'vimeo-sync-memberships');
            if($membership["ends"] < $wpvs_current_time) {
                $next_payment_date = __('expired', 'vimeo-sync-memberships');
            } else {
                $next_payment_date = wp_date( 'M d, Y',$membership["ends"] );
            }
            $remove_button = '<label class="removeCoinMembership deleteNow">'.$remove_text.'</label>';
            $membership_id_values = '<input type="hidden" class="membership-sub-id" value="' . $membership["id"] . '"/>';
            $status_update_class = "wpvs-coingate-update-membership";
            if( $is_admin ) {
                $sync_button = '<label class="rvs-sync-membership sync-coingate-plan" data-sync="'.$membership["id"].'">Sync</label>';
            }
        }

        if($membership_type == "coinbase") {
            $gateway = __('Coinbase', 'vimeo-sync-memberships');
            if($membership["ends"] < $wpvs_current_time) {
                $next_payment_date = __('expired', 'vimeo-sync-memberships');
            } else {
                $next_payment_date = wp_date( 'M d, Y',$membership["ends"] );
            }
            $remove_button = '<label class="removeCoinBaseMembership deleteNow">'.$remove_text.'</label>';
            $membership_id_values = '<input type="hidden" class="membership-sub-id" value="' . $membership["id"] . '"/>';
            $status_update_class = "wpvs-coinbase-update-membership";
            if( $is_admin ) {
                $sync_button = '<label class="rvs-sync-membership sync-coinbase-plan" data-sync="'.$membership["id"].'">Sync</label>';
            }
        }

        if( isset($membership["interval_count"]) ) {
            $wpvs_membership_price_label = wpvs_currency_label($membership['amount'], $membership["interval"], $membership["interval_count"], true);
        } else {
            $wpvs_membership_price_label = wpvs_currency_label($membership['amount'], $membership["interval"], 1, false);
        }

        $wpvs_user_memberships_html .= '<td class="rvs-m-name"><span class="rvs-hidden-title">'.__('Name', 'vimeo-sync-memberships').': </span>'.$membership["name"].'</td>';
        $wpvs_user_memberships_html .= '<td class="rvs-m-amount"><span class="rvs-hidden-title">'.__('Price', 'vimeo-sync-memberships').': </span>'.$wpvs_membership_price_label;
        if(!empty($membership_discount)) {
            $wpvs_user_memberships_html .= '<span class="rvs-discount">('.__('Discount', 'vimeo-sync-memberships').') '.$discount_amount.' '.$discount_left.'</span>';
        }
        $wpvs_user_memberships_html .= '</td>';
        $wpvs_user_memberships_html .= '<td class="rvs-m-date"><span class="rvs-hidden-title">'.__('Next Payment', 'vimeo-sync-memberships').': </span>';
        if(($is_manual_membership && $membership["ends"] != "never") || $membership_status == 'Cancelled' || $membership_status == 'Canceled' || $membership_status == 'canceled') {
            $wpvs_user_memberships_html .= __('ends', 'vimeo-sync-memberships').' ';
        }
        if($is_manual_membership) {
            if( $is_admin ) {
                $wpvs_user_memberships_html .= '<input type="text" class="wpvs-select-date-admin" readonly placeholder="'.$select_new_date_label.'"><label class="wpvs-label-button wpvs-set-man-never">'.__('Never Ends', 'vimeo-sync-memberships').'</label>';
            } else {
                if( $membership["ends"] != "never" ) {
                    $wpvs_user_memberships_html .= wp_date('F d, Y', $membership["ends"]);
                }
            }
        } else {
            $wpvs_user_memberships_html .= $next_payment_date;
        }
        $wpvs_user_memberships_html .= '</td>';
        $wpvs_user_memberships_html .= '<td class="membership-status"><span class="rvs-hidden-title">'.__('Status', 'vimeo-sync-memberships').': </span>';
        if($is_manual_membership) {
            $wpvs_user_memberships_html .= __('Active', 'vimeo-sync-memberships');
        } else {
            if( $membership_type == 'stripe' ) {
                 if($membership_status == 'past_due' || $membership_status == 'unpaid' || $membership_status == 'incomplete' || $membership_status == 'incomplete_expired') {
                    $wpvs_user_memberships_html .= __('Overdue', 'vimeo-sync-memberships');
                    if( ! $is_admin ) {
                        $wpvs_user_memberships_html .= '<label class="wpvs-stripe-paynow wpvs-pay-now-button" data-subid="'.$membership['id'].'" data-plan="'.$membership['plan'].'">'.__('Pay Now', 'vimeo-sync-memberships').'</label>';
                    }
                } else {
                    $wpvs_user_memberships_html .= '<select class="wpvs-update-user-plan-status '.$status_update_class.'">';
                    if($membership_status == 'active' || $membership_status == 'trialing') {
                        if( $membership_status == 'active' ) {
                            $wpvs_user_memberships_html .= '<option value="">'.__('Active', 'vimeo-sync-memberships').'</option>';
                        }
                        if( $membership_status == 'trialing' ) {
                            $wpvs_user_memberships_html .= '<option value="">'.__('Trialing', 'vimeo-sync-memberships').'</option>';
                        }
                        $wpvs_user_memberships_html .= '<option value="cancel">'.__('Cancel', 'vimeo-sync-memberships').'</option>';
                    }
                    if($membership_status == 'canceled') {
                        $wpvs_user_memberships_html .= '<option value="">'.__('Canceled', 'vimeo-sync-memberships').'</option>';
                    }
                    if($membership_ends_next_date) {
                        $wpvs_user_memberships_html .= '<option value="reactivate">'.__('Re-activate', 'vimeo-sync-memberships').'</option>';
                    }
                    $wpvs_user_memberships_html .= '</select>';
                }
            }
            if( $membership_type == 'paypal' ) {
                $wpvs_user_memberships_html .= '<select class="wpvs-update-user-plan-status '.$status_update_class.'">';
                if($membership_status == 'Active') {
                    $wpvs_user_memberships_html .= '<option value="">'.__('Active', 'vimeo-sync-memberships').'</option>';
                    $wpvs_user_memberships_html .= '<option value="suspend">'.__('Pause', 'vimeo-sync-memberships').'</option>';
                    $wpvs_user_memberships_html .= '<option value="cancel">'.__('Cancel', 'vimeo-sync-memberships').'</option>';
                }
                if($membership_status == 'Suspended') {
                    $wpvs_user_memberships_html .= '<option value="">'.__('Paused', 'vimeo-sync-memberships').'</option>';
                    $wpvs_user_memberships_html .= '<option value="reactivate">'.__('Re-activate', 'vimeo-sync-memberships').'</option>';
                    $wpvs_user_memberships_html .= '<option value="cancel">'.__('Cancel', 'vimeo-sync-memberships').'</option>';
                }
                if($membership_status == 'Pending') {
                    $wpvs_user_memberships_html .= '<option value="">'.__('Pending', 'vimeo-sync-memberships').'</option>';
                }
                if($membership_status == 'Cancelled' || $membership_status == 'Canceled') {
                    $wpvs_user_memberships_html .= '<option value="">'.__('Canceled', 'vimeo-sync-memberships').'</option>';
                }
                if($membership_ends_next_date) {
                    $wpvs_user_memberships_html .= '<option value="reactivate">'.__('Re-activate', 'vimeo-sync-memberships').'</option>';
                }
                $wpvs_user_memberships_html .= '</select>';
            }
            if( $membership_type == 'coingate' ) {
                $used_coin = "";
                if( isset($membership['coin']) && ! empty($membership['coin']) ) {
                    $used_coin = $membership['coin'];
                }
                if($membership_status == 'new' || $membership_status == 'pending' || $membership_status == 'expired') {
                    if( $is_admin ) {
                        $wpvs_user_memberships_html .= '<span class="wpvs-coingate-status">'.__('Awaiting payment', 'vimeo-sync-memberships').'</span>';
                    } else {
                        $wpvs_user_memberships_html .= '<span class="wpvs-coingate-status">'.__('Awaiting payment', 'vimeo-sync-memberships').'</span><label class="wpvs-coingate-paynow wpvs-pay-now-button" data-order="'.$membership['id'].'" data-plan="'.$membership['plan'].'" data-coin="'.$used_coin.'">'.__('Pay Now', 'vimeo-sync-memberships').'</label>';
                    }

                }

                if($membership_status == 'confirming') {
                    $wpvs_user_memberships_html .= '<span class="wpvs-coingate-status">'.__('Confirming payment', 'vimeo-sync-memberships').'</span>';
                }

                if($membership_status == 'invalid') {
                    if( $is_admin ) {
                        $wpvs_user_memberships_html .= '<span class="wpvs-coingate-status">'.__('Payment rejected', 'vimeo-sync-memberships').'</span>';
                    } else {
                        $wpvs_user_memberships_html .= '<span class="wpvs-coingate-status">'.__('Payment rejected', 'vimeo-sync-memberships').'</span><label class="wpvs-coingate-paynow wpvs-pay-now-button" data-order="'.$membership['id'].'" data-plan="'.$membership['plan'].'" data-coin="'.$used_coin.'">'.__('Try Again', 'vimeo-sync-memberships').'</label>';
                    }
                }

                if($membership_status == 'paid') {
                    $wpvs_user_memberships_html .= '<select class="wpvs-update-user-plan-status '.$status_update_class.'">';
                    $wpvs_user_memberships_html .= '<option>'.__('Active', 'vimeo-sync-memberships').'</option>';
                    $wpvs_user_memberships_html .= '<option value="cancel">'.__('Cancel', 'vimeo-sync-memberships').'</option>';
                    $wpvs_user_memberships_html .= '</select>';
                }

                if($membership_status == 'canceled') {
                    $wpvs_user_memberships_html .= '<select class="wpvs-update-user-plan-status '.$status_update_class.'">';
                    $wpvs_user_memberships_html .= '<option value="">'.__('Canceled', 'vimeo-sync-memberships').'</option>';
                    $wpvs_user_memberships_html .= '<option value="reactivate">'.__('Re-activate', 'vimeo-sync-memberships').'</option>';
                    $wpvs_user_memberships_html .= '</select>';
                }
            }
            if( $membership_type == 'coinbase' ) {
                $membership_status = strtoupper($membership_status);
                if($membership_status == 'NEW' || $membership_status == 'EXPIRED') {
                    if( $is_admin ) {
                        $wpvs_user_memberships_html .= '<span class="wpvs-coinbase-status">'.__('Awaiting payment', 'vimeo-sync-memberships').'</span>';
                    } else {
                        $wpvs_user_memberships_html .= '<span class="wpvs-coinbase-status">'.__('Awaiting payment', 'vimeo-sync-memberships').'</span><label class="wpvs-coinbase-paynow wpvs-pay-now-button" data-charge="'.$membership['id'].'" data-plan="'.$membership['plan'].'">'.__('Pay Now', 'vimeo-sync-memberships').'</label>';
                    }

                }

                if($membership_status == 'PENDING') {
                    $wpvs_user_memberships_html .= '<span class="wpvs-coinbase-status">'.__('Confirming payment', 'vimeo-sync-memberships').'</span>';
                }

                if($membership_status == 'UNRESOLVED') {
                    if( $is_admin ) {
                        $wpvs_user_memberships_html .= '<span class="wpvs-coinbase-status">'.__('Payment failed', 'vimeo-sync-memberships').'</span>';
                    } else {
                        $wpvs_user_memberships_html .= '<span class="wpvs-coinbase-status">'.__('Payment failed', 'vimeo-sync-memberships').'</span><label class="wpvs-coinbase-paynow wpvs-pay-now-button" data-order="'.$membership['id'].'" data-plan="'.$membership['plan'].'">'.__('Try Again', 'vimeo-sync-memberships').'</label>';
                    }
                }

                if($membership_status == 'CONFIRMED' || $membership_status == 'COMPLETED') {
                    $wpvs_user_memberships_html .= '<select class="wpvs-update-user-plan-status '.$status_update_class.'">';
                    $wpvs_user_memberships_html .= '<option>'.__('Active', 'vimeo-sync-memberships').'</option>';
                    $wpvs_user_memberships_html .= '<option value="cancel">'.__('Cancel', 'vimeo-sync-memberships').'</option>';
                    $wpvs_user_memberships_html .= '</select>';
                }

                if($membership_status == 'CANCELED') {
                    $wpvs_user_memberships_html .= '<select class="wpvs-update-user-plan-status '.$status_update_class.'">';
                    $wpvs_user_memberships_html .= '<option value="">'.__('Canceled', 'vimeo-sync-memberships').'</option>';
                    $wpvs_user_memberships_html .= '<option value="reactivate">'.__('Re-activate', 'vimeo-sync-memberships').'</option>';
                    $wpvs_user_memberships_html .= '</select>';
                }

            }
        }
        $wpvs_user_memberships_html .= '</td>';
        if( $is_admin ) {
            $wpvs_user_memberships_html .= '<td>'.$gateway.'</td>';
            $wpvs_user_memberships_html .= '<td>'.$sync_button.'</td>';
        }
        if( $wpvs_users_can_delete_memberships || $is_admin ) {
            $wpvs_user_memberships_html .= '<td class="rvs-remove-edit">'.$remove_button.'</td>';
        }
        $wpvs_user_memberships_html .= $membership_id_values;
        $wpvs_user_memberships_html .= '</tr>';
     }
    $wpvs_user_memberships_html .= '</tbody></table>';
    return $wpvs_user_memberships_html;
}

function wpvs_user_has_completed_trial($user_id, $membership_id) {
    if( current_user_can('manage_options' ) ) {
        return false;
    }
    global $rvs_live_mode;
    $trial_complete = false;
    if($rvs_live_mode == "off") {
        $get_trial_periods = 'wpvs_user_completed_trials_test';
    } else {
        $get_trial_periods = 'wpvs_user_completed_trials';
    }

    $user_completed_trials = get_user_meta($user_id, $get_trial_periods, true);
    if( ! empty($user_completed_trials) && in_array($membership_id, $user_completed_trials) ) {
        $trial_complete = true;
    }
    return $trial_complete;
}

function wpvs_user_completed_trial($user_id, $membership_id) {
    global $rvs_live_mode;
    $trial_complete = false;
    if($rvs_live_mode == "off") {
        $get_trial_periods = 'wpvs_user_completed_trials_test';
    } else {
        $get_trial_periods = 'wpvs_user_completed_trials';
    }

    $user_completed_trials = get_user_meta($user_id, $get_trial_periods, true);
    if( empty($user_completed_trials) ) {
        $user_completed_trials = array();
    }
    $user_completed_trials[] = $membership_id;
    $user_completed_trials = array_unique($user_completed_trials);
    update_user_meta($user_id, $get_trial_periods, $user_completed_trials);
}

function wpvs_clear_user_membership_trial($user_id, $membership_id) {
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $get_trial_periods = 'wpvs_user_completed_trials_test';
    } else {
        $get_trial_periods = 'wpvs_user_completed_trials';
    }
    $user_completed_trials = get_user_meta($user_id, $get_trial_periods, true);
    if( ! empty($user_completed_trials) ) {
        foreach($user_completed_trials as $key => &$trial) {
            if($trial == $membership_id) {
                unset($user_completed_trials[$key]);
                update_user_meta($user_id, 'wpvs_user_completed_trials', $user_completed_trials);
            }
        }
    }
}

if( ! function_exists('wpvs_generate_customer_menu') ) {
    function wpvs_generate_customer_menu($current_view) {
        global $wpvs_stripe_gateway_enabled;
        $wpvs_account_page = esc_attr( get_option('rvs_account_page') );
        if( ! empty($wpvs_account_page) ) {
            $wpvs_account_link = get_permalink($wpvs_account_page);
        }
        $rvs_user_rentals_page = esc_attr( get_option('rvs_user_rental_page') );
        $rvs_user_purchases_page = esc_attr( get_option('rvs_user_purchase_page') );
        $is_account_page = true;
        if($current_view == "purchases" || $current_view == "rentals" || $current_view == "mylist") {
            $is_account_page = false;
        }
        $wpvs_custom_menu = '<label id="wpvs-open-account-menu"><span class="dashicons dashicons-admin-users"></span> '.__('Menu', 'vimeo-sync-memberships').'</label>';
        $wpvs_custom_menu .= '<div id="wpvs-account-menu">';
        if( ! empty($rvs_user_purchases_page) ) {
            $wpvs_user_rentals_link = get_permalink($rvs_user_purchases_page);
            $wpvs_custom_menu .= '<a class="wpvs-menu-item';
            if($current_view == "purchases") {
                $wpvs_custom_menu .= ' active';
            }
            $wpvs_custom_menu .= '" href="'.$wpvs_user_rentals_link.'">'.__('Purchases', 'vimeo-sync-memberships').'</a>';
        }
        if( ! empty($rvs_user_rentals_page) ) {
            $wpvs_user_rentals_link = get_permalink($rvs_user_rentals_page);
            $wpvs_custom_menu .= '<a class="wpvs-menu-item';
            if($current_view == "rentals") {
                $wpvs_custom_menu .= ' active';
            }
            $wpvs_custom_menu .= '" href="'.$wpvs_user_rentals_link.'">'.__('Rentals', 'vimeo-sync-memberships').'</a>';
        }
        if( get_option('wpvs_theme_active') && get_theme_mod('wpvs_my_list_enabled', 0) && ! empty(get_theme_mod('wpvs_my_list_page')) ) {
            $wpvs_user_my_list_link = get_permalink(get_theme_mod('wpvs_my_list_page'));
            $wpvs_custom_menu .= '<a class="wpvs-menu-item';
            if($current_view == "mylist") {
                $wpvs_custom_menu .= ' active';
            }
            $wpvs_custom_menu .= '" href="'.$wpvs_user_my_list_link.'">'.get_theme_mod('wpvs_user_menu_list_link', 'My List').'</a>';
        }
        $wpvs_custom_menu .= '<a id="wpvs-open-memberships" class="wpvs-menu-item';
        if( $current_view == "memberships") {
            $wpvs_custom_menu .= ' active';
        }
        if( $is_account_page ) {
            $wpvs_custom_menu .= ' disable-link';
        }
        $wpvs_custom_menu .= '" href="'.$wpvs_account_link.'?wpvsview=memberships" data-section="wpvs-account-memberships">'.__('Memberships', 'vimeo-sync-memberships').'</a>';

        $wpvs_custom_menu .= '<a id="wpvs-open-cards" class="wpvs-menu-item';
        if($current_view == "cards") {
            $wpvs_custom_menu .= ' active';
        }
        if( $is_account_page ) {
            $wpvs_custom_menu .= ' disable-link';
        }
        $wpvs_custom_menu .= '" href="'.$wpvs_account_link.'?wpvsview=cards" data-section="wpvs-account-cards">'.__('Billing', 'vimeo-sync-memberships').'</a>';
        if($wpvs_stripe_gateway_enabled) {
            $wpvs_custom_menu .= '<a id="wpvs-open-payments" class="wpvs-menu-item';
            if($current_view == "payments") {
                $wpvs_custom_menu .= ' active';
            }
            if( $is_account_page ) {
                $wpvs_custom_menu .= ' disable-link';
            }
            $wpvs_custom_menu .= '" href="'.$wpvs_account_link.'?wpvsview=payments" data-section="wpvs-account-payments">'.__('Payments', 'vimeo-sync-memberships').'</a>';
        }
        $wpvs_custom_menu .= '<a id="wpvs-open-information" class="wpvs-menu-item';
        if($current_view == "info") {
            $wpvs_custom_menu .= ' active';
        }
        if( $is_account_page ) {
            $wpvs_custom_menu .= ' disable-link';
        }
        $wpvs_custom_menu .= '" href="'.$wpvs_account_link.'?wpvsview=info" data-section="wpvs-account-information">'.__('My Account', 'vimeo-sync-memberships').'</a>';
        $wpvs_custom_menu .= '<a class="wpvs-menu-item" href="'.wp_logout_url(home_url()).'">'.__('Logout', 'vimeo-sync-memberships').' <span class="dashicons dashicons-migrate"></span></a></div>';
        return $wpvs_custom_menu;
    }
}
