<?php

if( ! function_exists('wpvs_do_after_new_membership_action') ) {
    function wpvs_do_after_new_membership_action($user_id, $membership_array) {
        if( has_action('wpvs_do_after_new_membership') ) {
            do_action( 'wpvs_do_after_new_membership', $user_id, $membership_array );
        }
    }
}

if( ! function_exists('wpvs_do_after_new_product_purchase_action') ) {
    function wpvs_do_after_new_product_purchase_action($user_id, $product_id, $purchase_type) {
        if( has_action('wpvs_do_after_new_product_purchase') ) {
            do_action( 'wpvs_do_after_new_product_purchase', $user_id, $product_id, $purchase_type );
        }
    }
}

if( ! function_exists('wpvs_custom_has_access_check_action') ) {
    function wpvs_custom_has_access_check_action($user_id, $membership_array, $user_memberships, $post_id) {
        if( has_filter('wpvs_custom_has_access_check') ) {
            return apply_filters( 'wpvs_custom_has_access_check', $user_id, $membership_array, $user_memberships, $post_id);
        } else {
            return null;
        }
    }
}

if( ! function_exists('wpvs_do_after_user_role_is_set') ) {
    function wpvs_do_after_user_role_is_set($user_id, $role, $old_roles) {
        if( $role == 'wpvs_test_member' ) {
            wpvs_add_new_member($user_id, true);
        }
        if( $role == 'wpvs_member' ) {
            wpvs_add_new_member($user_id, false);
        }
    }
    add_action('set_user_role', 'wpvs_do_after_user_role_is_set', 10, 3);
}

if( ! function_exists('wpvs_do_after_delete_membership_action') ) {
    function wpvs_do_after_delete_membership_action($user_id, $membership) {
        if( has_action('wpvs_do_after_delete_membership') ) {
            do_action( 'wpvs_do_after_delete_membership', $user_id, $membership );
        }
    }
}

if( ! function_exists('wpvs_add_fields_after_registration_form_action') ) {
    function wpvs_add_fields_after_registration_form_action() {
        if( has_action('wpvs_add_fields_after_registration_form') ) {
            do_action( 'wpvs_add_fields_after_registration_form' );
        }
    }
}

if( ! function_exists('wpvs_add_fields_after_account_form_action') ) {
    function wpvs_add_fields_after_account_form_action($user_id) {
        if( has_action('wpvs_add_fields_after_account_form') ) {
            do_action( 'wpvs_add_fields_after_account_form', $user_id);
        }
    }
}
