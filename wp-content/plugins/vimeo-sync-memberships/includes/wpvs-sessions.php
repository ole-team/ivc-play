<?php

class WPVS_User_Sessions_Manager {
    private $login_restrictions;
    public function __construct() {
        $this->login_restrictions = get_option('wpvs_login_restrictions');
        add_action('wp_login', array($this, 'create_wpvs_session'));
        add_action('wp_logout', array($this, 'end_wpvs_session'));
        add_filter( 'wp_authenticate_user', array($this, 'validate_user' ) );
    }

    public function create_wpvs_session() {
        if( $this->is_session_started() === FALSE ) {
            session_start();
        }
    }

    public function end_wpvs_session() {
        if( $this->is_session_started() === TRUE ) {
            session_destroy();
        }
    }

    private function is_session_started() {
        $wpvs_session_started = FALSE;
        if ( php_sapi_name() !== 'cli' ) {
            if ( version_compare(phpversion(), '5.4.0', '>=') ) {
                if( session_status() === PHP_SESSION_ACTIVE ) {
                    $wpvs_session_started = TRUE;
                }
            } else {
                if( session_id() === '' ) {
                    $wpvs_session_started = FALSE;
                }
            }
        }
        return $wpvs_session_started;
    }

    public function validate_user( $user ) {
        if ( is_wp_error( $user ) ) {
			return $user;
		}
        if( isset($this->login_restrictions['enabled']) && ! empty($this->login_restrictions['enabled']) ) {
    		if ( $this->login_limit_reached( $user->ID ) ) {
    			if ( $this->login_restrictions['login_logic'] == 'block' ) {
    				return new WP_Error( 'wpvs_login_limit_reached', __('You have hit your login limit. Please logout from other devices to continue.', 'vimeo-sync-memberships') );
    			} else {
                    // destroy other user sessions
    				$session_manager = WP_Session_Tokens::get_instance( $user->ID );
    				$session_manager->destroy_all();
    			}
    		}
        }
		return $user;
    }

    private function login_limit_reached( $user_id ) {
		$wpvs_login_limit = intval( $this->login_restrictions['login_limit'] );
		$session_manager = WP_Session_Tokens::get_instance( $user_id );
		$session_count = count( $session_manager->get_all() );
		return $session_count >= $wpvs_login_limit;
	}
}
$wpvs_user_sessions_manager = new WPVS_User_Sessions_Manager();
