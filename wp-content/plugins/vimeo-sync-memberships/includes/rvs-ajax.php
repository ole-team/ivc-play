<?php


// CHECK COUPON CODE

add_action( 'wp_ajax_wpvs_check_coupon_code', 'wpvs_check_coupon_code' );

function wpvs_check_coupon_code() {
    global $wpvs_stripe_options;
    global $rvs_paypal_enabled;
    global $rvs_live_mode;
    global $rvs_current_user;
    $rvs_coupon_codes = get_option('rvs_coupon_codes');
    $coupon_found = false;
    $use_coupon = null;
    $coupon_paypal_plan_id = null;
    $error_message = "";
    if(isset($_POST['coupon_id']))  {
        $check_coupon_id = $_POST['coupon_id'];

        if(!empty($rvs_coupon_codes)) {
            foreach($rvs_coupon_codes as $key => &$coupon) {
                if($coupon['id'] == $check_coupon_id) {
                    if( isset($coupon['max_uses']) && ! empty($coupon['max_uses']) ) {
                        if( isset($coupon['used_count']) && ! empty($coupon['used_count']) ) {
                            if( intval($coupon['used_count']) >= intval($coupon['max_uses']) ) {
                                $error_message = __('Coupon code is no longer valid', 'vimeo-sync-memberships');
                            }
                        }
                    }
                    if( isset($coupon['max_uses_customer']) && ! empty($coupon['max_uses_customer']) ) {
                        $customer_can_use_coupon = wpvs_check_customer_coupon_usage($rvs_current_user->ID, $check_coupon_id, $coupon['max_uses_customer']);
                        if( ! $customer_can_use_coupon ) {
                            $error_message = __('Coupon code usage limit reached', 'vimeo-sync-memberships');
                        }
                    }
                    if( empty($error_message) ) {
                        $coupon_found = true;
                        $use_coupon = $coupon;
                    }
                    break;
                }
            }
        }

        if( ! empty($check_coupon_id) && $coupon_found) {

            // CHECK IF STRIPE IS ACTIVE
            if(isset($wpvs_stripe_options['enabled']) && $wpvs_stripe_options['enabled']) {
                $stripe_payments_manager = new WPVS_Stripe_Payments_Manager($rvs_current_user->ID);
                $stripe_coupon_code = $stripe_payments_manager->verify_coupon_code($check_coupon_id);
                if( ! $stripe_coupon_code->is_valid ) {
                    $error_message .= $stripe_coupon_code->error;
                }
            }

            if( empty($error_message) ) {
                echo json_encode(array('coupon' => $use_coupon));
            } else {
                rvs_exit_ajax($error_message, 400);
            }
        } else {
            if( empty($error_message) ) {
                $error_message = __('Invalid coupon code', 'vimeo-sync-memberships');
            }
        }

        if( ! empty($error_message) ) {
            rvs_exit_ajax($error_message, 400);
        }
    } else {
        $rvs_error_message = __('Missing parameters', 'vimeo-sync-memberships');
        rvs_exit_ajax($rvs_error_message, 400);
    }
    wp_die();
}

add_action( 'wp_ajax_rvs_remove_man_plan', 'rvs_remove_man_plan' );

function rvs_remove_man_plan() {
    global $rvs_live_mode;
    global $rvs_current_user;
    if($rvs_live_mode == "off") {
        $get_memberships = 'rvs_user_memberships_test';
    } else {
        $get_memberships = 'rvs_user_memberships';
    }

    if(isset($_POST["plan_id"]) && !empty($_POST["plan_id"])) {

        $plan_id = $_POST["plan_id"];
        $user_id = $rvs_current_user->ID;
        $user_memberships = get_user_meta($user_id, $get_memberships, true);
        $remove_membership = null;
        if(!empty($user_memberships)) {
            foreach($user_memberships as $key => &$membership) {
                if ($membership["plan"] == $plan_id) {
                    unset($user_memberships[$key]);
                    $remove_membership = $membership;
                    break;
                }
            }
            update_user_meta($user_id, $get_memberships, $user_memberships);
            if( ! empty($remove_membership) ) {
                wpvs_do_after_delete_membership_action($user_id, $remove_membership);
            }
        }

    } else {
        $rvs_error_message = __('Missing either User ID or Membership ID', 'vimeo-sync-memberships');
        rvs_exit_ajax($rvs_error_message, 400);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_product_change_purchase_type', 'wpvs_product_change_purchase_type' );

function wpvs_product_change_purchase_type() {
    $return_price = array();
    if(isset($_POST['type']) && !empty($_POST['type']) && isset($_POST['video']) && !empty($_POST['video'])) {
        $purchase_type = $_POST['type'];
        $video_id = $_POST['video'];
        $video_title = get_the_title($video_id);
        if($purchase_type == "purchase") {
            $video_price = get_post_meta( $video_id, '_rvs_onetime_price', true );
            $expires = "never";
            $interval = null;
            $description = sprintf(__('Purchase %s', 'vimeo-sync-memberships'), $video_title);
        }
        if($purchase_type == "rental") {
            $video_price = get_post_meta( $video_id, 'rvs_rental_price', true );
            $expires = get_post_meta( $video_id, 'rvs_rental_expires', true );
            $interval = get_post_meta( $video_id, 'rvs_rental_type', true );
            $description = sprintf(__('Rent %s for %s %s', 'vimeo-sync-memberships'), $video_title, $expires, $interval);
        }
        if( isset($_POST['coupon_code']) && ! empty($_POST['coupon_code']) ) {
            $coupon_code = $_POST['coupon_code'];
            $video_price = wpvs_calculate_coupon_amount($video_price, $coupon_code);
        }
        $return_price = array(
            'price' => intval($video_price),
            'expires' => intval($expires),
            'interval' => $interval,
            'type' => $purchase_type,
            'description' => $description
        );

        if(!empty($return_price)) {
            echo json_encode($return_price);
        } else {
            rvs_exit_ajax("Could not find video purchase details.", 400);
        }
    } else {
        rvs_exit_ajax(__('Missing parameters', 'vimeo-sync-memberships'), 400);
    }
    wp_die();
}
