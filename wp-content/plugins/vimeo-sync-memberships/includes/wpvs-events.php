<?php

function wpvs_get_members_for_reminder($test_mode, $offset) {
    global $wpdb;
    if($test_mode) {
        $wpvs_members_table_name = $wpdb->prefix . 'wpvs_test_members';
    } else {
        $wpvs_members_table_name = $wpdb->prefix . 'wpvs_members';
    }
    $wpvs_members = $wpdb->get_results("SELECT * FROM $wpvs_members_table_name LIMIT 100 OFFSET $offset");
    return $wpvs_members;
}

function wpvs_member_renewal_reminders($member_offset) {
    $wpvs_membership_reminder_email = get_option('wpvs_membership_reminder_email');
    $send_renewal_reminders = false;

    if( !empty($wpvs_membership_reminder_email) && isset($wpvs_membership_reminder_email['enabled']) ) {
        $send_renewal_reminders = true;
    }

    if( $send_renewal_reminders ) {
        global $rvs_live_mode;
        if($rvs_live_mode == "on") {
            $test_mode = false;
            $get_memberships = 'rvs_user_memberships';
        } else {
            $test_mode = true;
            $get_memberships = 'rvs_user_memberships_test';
        }
        if(!isset($wpvs_membership_reminder_email['before_count'])) {
            $wpvs_membership_reminder_email['before_count'] = 3;
        }

        if(!isset($wpvs_membership_reminder_email['reminder_type'])) {
            $wpvs_membership_reminder_email['reminder_type'] = 'days';
        }

        $reminder_count = $wpvs_membership_reminder_email['before_count'];
        $reminder_interval = $wpvs_membership_reminder_email['reminder_type'];
        $reminder_time_string = '+'.$reminder_count.' '.$reminder_interval;
        $current_time = current_time('timestamp', 1);
        $reminder_time_cap = strtotime($reminder_time_string, $current_time);
        $wpvs_members = wpvs_get_members_for_reminder($test_mode, $member_offset);

        if( ! empty($wpvs_members)) {
            foreach($wpvs_members as $member_data) {
                $user_id = $member_data->user_id;
                $user_memberships = get_user_meta($user_id, $get_memberships, true);
                if( ! empty($user_memberships) ) {
                    foreach($user_memberships as $key => &$membership) {
                        if( empty($membership['reminder_sent']) && isset($membership['ends']) ) {
                            $plan_name = $membership["name"];
                            $membership_type = 'stripe';
                            if( isset($membership['type']) ) {
                                $membership_type = $membership['type'];
                                if($membership['type'] == "coingate" || $membership['type'] == "coingate_test") {
                                    $membership_type = 'coingate';
                                }
                                if($membership['type'] == "coinbase" || $membership['type'] == "coinbase_test") {
                                    $membership_type = 'coinbase';
                                }
                                if($membership['type'] == "paypal" || $membership['type'] == "paypal_test") {
                                    $membership_type = 'paypal';
                                }
                            }

                            if($membership_type == 'stripe') {
                                $renewal_time = $membership['ends'];
                            }

                            if($membership_type == 'paypal') {
                                $renewal_time = strtotime($membership['ends']);
                            }

                            if($membership_type == 'stripe' || $membership_type == 'paypal') {
                                if( ! empty($renewal_time) && ( $renewal_time > $current_time ) && ( $renewal_time < $reminder_time_cap ) ) {
                                    $membership['reminder_sent'] = 1;
                                    wpvs_send_membership_reminder($user_id, $plan_name, $renewal_time, false);
                                }
                            }

                            if($membership_type == 'coingate') {
                                $renewal_time = $membership['ends'];
                                if( ! empty($renewal_time) && ( $renewal_time < $current_time ) ) {
                                    $membership['reminder_sent'] = 1;
                                    if( isset($membership['coin']) && ! empty($membership['coin']) ) {
                                        $membership['status'] = 'pending';
                                    } else {
                                        $membership['status'] = 'new';
                                    }
                                    wpvs_send_membership_reminder($user_id, $plan_name, $renewal_time, true);
                                }
                            }

                            if($membership_type == 'coinbase') {
                                $renewal_time = $membership['ends'];
                                if( ! empty($renewal_time) && ( $renewal_time < $current_time ) ) {
                                    $membership['reminder_sent'] = 1;
                                    $membership['status'] = 'NEW';
                                    wpvs_send_membership_reminder($user_id, $plan_name, $renewal_time, true);
                                }
                            }
                        }
                    }
                    update_user_meta($user_id, $get_memberships, $user_memberships);
                }
            }
            if(count($wpvs_members) == 100) {
                $new_offset = intval($member_offset) + 100;
                wpvs_member_renewal_reminders($new_offset);
            }
        }
    }
}

add_action('wpvs_member_renewal_reminders_event', 'wpvs_member_renewal_reminders', 10, 1);

function wpvs_clear_member_html_emails() {
    $wpvs_html_email_dir = ABSPATH .'member/emails/';
    $html_email_files = glob($wpvs_html_email_dir."*");
    $current_time = time();
    $remove_days_ago = strtotime('-14 days', $current_time);
    if( ! empty($html_email_files) ) {
        foreach ($html_email_files as $html_file) {
            if (is_file($html_file) ) {
                if (filemtime($html_file) <= $remove_days_ago) {
                    unlink($html_file);
                }
            }
        }
    }
}

add_action('wpvs_clear_member_html_emails_event', 'wpvs_clear_member_html_emails', 10);

function wpvs_clear_old_coin_gate_orders() {
    global $wpdb;
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $table_name = $wpdb->prefix . 'wpvs_test_coin_payments';
    } else {
        $table_name = $wpdb->prefix . 'wpvs_coin_payments';
    }
    $wpvs_current_time = current_time('timestamp', 1);
    $three_days_ago = strtotime('-3 days', $wpvs_current_time);
    $coin_query = "SELECT * FROM $table_name WHERE updated <= '$three_days_ago' AND status != 'paid'";
    $coin_payments = $wpdb->get_results($coin_query);
    if( ! empty($coin_payments) ) {
        $wpvs_coingate_controller = new \WPVS\CoinGate\WPVSCoinGatePayment;
        foreach($coin_payments as $old_payment) {
            $wpvs_coingate_controller->wpvs_delete_coin_payment($old_payment->id);
        }
    }
}

add_action('wpvs_clear_old_coin_gate_orders_event', 'wpvs_clear_old_coin_gate_orders', 10);

function wpvs_clear_old_stripe_checkout_sessions() {
    global $wpdb;
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $table_name = $wpdb->prefix . 'wpvs_test_stripe_checkout_sessions';
    } else {
        $table_name = $wpdb->prefix . 'wpvs_stripe_checkout_sessions';
    }
    $wpvs_current_time = current_time('timestamp', 1);
    $wpvs_stripe_checkout_sessions_db_query = "SELECT * FROM $table_name WHERE expires <= '$wpvs_current_time'";
    $wpvs_stripe_checkout_db_sessions = $wpdb->get_results($wpvs_stripe_checkout_sessions_db_query);
    if( ! empty($wpvs_stripe_checkout_db_sessions) ) {
        foreach($wpvs_stripe_checkout_db_sessions as $old_stripe_checkout_db_session) {
            $wpdb->get_results("DELETE FROM $table_name WHERE id = '$old_stripe_checkout_db_session->id'");
        }
    }
}

add_action('wpvs_clear_old_stripe_checkout_sessions_event', 'wpvs_clear_old_stripe_checkout_sessions', 10);
