<?php

function rvs_filter_the_content_members( $content ) {
    global $post;
    global $rvs_current_user;
    $custom_content = "";
    $no_access_content = "";
    $wpvs_video_membership_list = get_post_meta( $post->ID, '_rvs_memberships', true );
    // GET VIDEO PURCHASE PRICE
    $video_onetime_price = get_post_meta( $post->ID, '_rvs_onetime_price', true );
    $wpvs_free_for_users = get_post_meta( $post->ID, '_rvs_membership_users_free', true );
    // GET VIDEO RENTAL DETAILS
    $video_rental_price = get_post_meta( $post->ID, 'rvs_rental_price', true );
    $show_video_content = false;
    $wpvs_video_has_restriction = false;
    $wpvs_video_placemen = get_option('rvs_video_position', 'above');
    $wpvs_video_terms = wp_get_post_terms( $post->ID, 'rvs_video_category', array('fields' => 'id=>parent') );
    $wpvs_additional_payment_options = wpvs_get_additional_payment_options($wpvs_video_terms);
    $wpvs_additional_memberships = $wpvs_additional_payment_options['memberships'];
    $wpvs_additional_purchase_options = $wpvs_additional_payment_options['purchases'];
    $wpvs_video_membership_list = array_merge($wpvs_video_membership_list, $wpvs_additional_memberships);
    $video_restricted_content = get_post_meta($post->ID, 'wpvs_restricted_video_content', true);
    if( empty($video_restricted_content) ) {
        $video_restricted_content = 'video';
    }

    if( ! empty($wpvs_video_membership_list) || ! empty($video_onetime_price) || ! empty($video_rental_price) || $wpvs_free_for_users || ! empty($wpvs_additional_purchase_options) ) {
        $wpvs_video_has_restriction = true;
    }

    if ( is_user_logged_in() ) {
        $wpvs_customer = new WPVS_Customer($rvs_current_user);
        if($wpvs_free_for_users || current_user_can( 'manage_options' )) {
            $show_video_content = true;
        }
        $has_membership = $wpvs_customer->has_access($wpvs_video_membership_list, $post->ID);
        if($has_membership['has_access']) {
            $show_video_content = true;
        }

        if( ! $wpvs_video_has_restriction ) {
            $show_video_content = true;
        }

        if( ! $show_video_content) {
            $user_term_purchases = get_user_meta($rvs_current_user->ID, 'rvs_user_term_purchases', true);
            if( ! empty($user_term_purchases) && ! empty($wpvs_additional_purchase_options) ) {
                foreach($user_term_purchases as $purchased_term) {
                    if( in_array($purchased_term, $wpvs_additional_purchase_options) ) {
                        $show_video_content = true;
                        break;
                    }
                }
            }
        }

        if( ! $show_video_content ) {
            $no_access_content .= rvs_memberships_content_payments($has_membership, $wpvs_video_membership_list, $video_onetime_price, $video_rental_price, $wpvs_additional_purchase_options);
        }

    } else {
        if( ! $wpvs_video_has_restriction ) {
            $show_video_content = true;
        } else {
            $no_access_content .= rvs_not_logged_in();
        }
    }

    if( $wpvs_video_placemen == "below") {
        if( $video_restricted_content == 'video' ) {
            $custom_content .= $content;
        }
        if( $video_restricted_content == 'videocontent' && $show_video_content ) {
            $custom_content .= $content;
        }
    }

    if( $show_video_content ) {
        $custom_content .= addVideoToContent($post);
    } else {
        $custom_content .= $no_access_content;
    }

    if( $wpvs_video_placemen == "above") {
        if( $video_restricted_content == 'video' ) {
            $custom_content .= $content;
        }
        if( $video_restricted_content == 'videocontent' && $show_video_content ) {
            $custom_content .= $content;
        }
    }
    return $custom_content;
}

// Use custom template for videos

function rvs_video_post_type_template_members($single_template) {
     global $post;
     if ($post->post_type == 'rvs_video') {
        wp_enqueue_style( 'dashicons' );
        add_filter( 'the_content', 'rvs_filter_the_content_members' );
     }
    return $single_template;
}
if(!get_option('wpvs_theme_active')) {
    add_filter( 'single_template', 'rvs_video_post_type_template_members' );
}

// LOCK FULL SITE

function wpvs_lock_full_site() {
    global $post;
    $allowed_to_view = false;
    if( !empty($post) && get_post_meta($post->ID, 'wpvs_unlock_page_override', true) ) {
        $allowed_to_view = true;
    }
    if( is_user_logged_in() && current_user_can('manage_options') ) {
        $allowed_to_view = true;
    }
    if( ! $allowed_to_view ) {
        $allowed_pages = array();
        $frontpage_id = get_option( 'page_on_front' );
        $wpvs_account_page = esc_attr( get_option('rvs_account_page'));
        $rvs_sign_up_page = esc_attr( get_option('rvs_sign_up_page'));
        $rvs_payment_page = esc_attr( get_option('rvs_payment_page'));
        $rvs_create_account_page = esc_attr( get_option('rvs_create_account_page'));
        $wpvs_require_login_page = get_option('wpvs_require_login_page', $rvs_create_account_page);
        $wpvs_landing_page = get_option('wpvs_landing_page', $frontpage_id);
        $wpvs_site_lock_redirect_logged_in_users_page = get_option('wpvs_site_lock_redirect_logged_in_users_page', $rvs_sign_up_page);
        if( is_user_logged_in() ) {
            global $rvs_current_user;
            $wpvs_customer = new WPVS_Customer($rvs_current_user);
            $wpvs_site_lock_required_memberships = get_option('wpvs_site_lock_required_memberships', array());
            if( ! empty($wpvs_site_lock_required_memberships) ) {
                foreach($wpvs_site_lock_required_memberships as $required_membership_id) {
                    $current_user_has_access = $wpvs_customer->has_membership_by_id($required_membership_id);
                    if($current_user_has_access['has_access']) {
                        $allowed_to_view = true;
                        break;
                    }
                }
            } else {
                $allowed_to_view = true;
            }

            if( ! empty($wpvs_site_lock_redirect_logged_in_users_page) ) {
                $allowed_pages[] = $wpvs_site_lock_redirect_logged_in_users_page;
            }
        }

        if( ! empty($wpvs_account_page) ) {
            $allowed_pages[] = $wpvs_account_page;
        }

        if( ! empty($rvs_sign_up_page) ) {
            $allowed_pages[] = $rvs_sign_up_page;
        }

        if( ! empty($rvs_payment_page) ) {
            $allowed_pages[] = $rvs_payment_page;
        }

        if( ! empty($rvs_create_account_page) ) {
            $allowed_pages[] = $rvs_create_account_page;
        }

        if( ! empty($wpvs_landing_page) ) {
            $allowed_pages[] = $wpvs_landing_page;
        }

        if( ! empty($wpvs_require_login_page) && $wpvs_require_login_page != 'login') {
            $allowed_pages[] = $wpvs_require_login_page;
        }

        if( is_page($allowed_pages) ) {
             $allowed_to_view = true;
        }

    }

    if( ! $allowed_to_view ) {
        if( is_user_logged_in() ) {
            $login_redirect_link = get_permalink($wpvs_site_lock_redirect_logged_in_users_page);
        } else {
            if($wpvs_require_login_page == 'login') {
                $login_redirect_link = wp_login_url();
            } else {
                $login_redirect_link = get_permalink($wpvs_require_login_page);
            }
        }
        wp_redirect($login_redirect_link);
    }
}
$wpvs_require_user_login = get_option('wpvs_require_user_login', 0);
if($wpvs_require_user_login) {
    add_action('template_redirect', 'wpvs_lock_full_site');
}
