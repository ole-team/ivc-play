<?php

function wpvs_check_membership_plans($user) {
    global $rvs_paypal_enabled;
    global $wpvs_stripe_gateway_enabled;
    global $wpvs_coingate_enabled;
    global $wpvs_coinbase_enabled;
    global $rvs_live_mode;
    if($rvs_live_mode == "on") {
        $get_memberships = 'rvs_user_memberships';
        $stripe_key = "stripe";
        $coingate_key = "coingate";
        $coinbase_key = "coinbase";
    } else {
        $get_memberships = 'rvs_user_memberships_test';
        $stripe_key = "stripe_test";
        $coingate_key = "coingate_test";
        $coinbase_key = "coinbase_test";
    }
    $user_id = $user->ID;
    $user_memberships = get_user_meta($user_id, $get_memberships, true);
    $wpvs_current_time = current_time('timestamp', 1);
    if($wpvs_stripe_gateway_enabled) {
        $stripe_payments_manager = new WPVS_Stripe_Payments_Manager($user_id);
        $wpvs_stripe_customer = $stripe_payments_manager->get_stripe_customer_details();
        if( ! empty($wpvs_stripe_customer) ) {
            if(!empty($user_memberships)) {
                foreach($user_memberships as $key => &$membership) {
                    if($membership['type'] && ($membership['type'] == $stripe_key )) {
                        if(isset($membership["ends"]) && ! empty($secret_key) ) {
                            $subscription = $stripe_payments_manager->get_subscription($membership["id"]);
                            if($subscription->current_period_end > $membership["ends"]) {
                                $membership["ends"] = $subscription->current_period_end;
                                if( $subscription->current_period_end > current_time('timestamp', 1) ) {
                                    $membership["reminder_sent"] = 0;
                                }
                            }

                            if($stripe_subscription->cancel_at_period_end) {
                                $membership["status"] = 'canceled';
                            } else {
                                $membership["status"] = $subscription->status;
                            }

                            if(isset($membership['discount']) && !empty($membership['discount']) && $subscription->discount != null) {
                                $membership['discount'] = $subscription->discount->coupon;
                            } else {
                                $membership['discount'] = null;
                            }
                        }
                    }
                }
                update_user_meta( $user_id, $get_memberships, $user_memberships);
            }
        }
    }


    if( ! empty($user_memberships) ) {
        foreach($user_memberships as $key => &$membership) {
            if($membership['type'] && ($membership['type'] == $coingate_key )) {
                $renewal_time = $membership['ends'];
                if( ! empty($renewal_time) && ( $renewal_time <= $wpvs_current_time ) && $membership['status'] == 'paid' ) {
                    if( isset($membership['coin']) && ! empty($membership['coin']) ) {
                        $membership['status'] = 'pending';
                    } else {
                        $membership['status'] = 'new';
                    }
                }
            }

            if($membership['type'] && ($membership['type'] == $coinbase_key )) {
                $renewal_time = $membership['ends'];
                if( ! empty($renewal_time) && ( $renewal_time <= $wpvs_current_time ) ) {
                    if( $membership['status'] == 'COMPLETED' || $membership['status'] == 'CONFIRMED' ) {
                        $membership['status'] = 'NEW';
                    }
                }
            }
        }
        update_user_meta( $user_id, $get_memberships, $user_memberships);
    }


    // CHECK PAYPAL MEMBERSHIPS
    if($rvs_paypal_enabled) {
        global $rvs_paypal_settings;
        $remove_membership = null;
        require_once(RVS_MEMBERS_BASE_DIR . '/paypal/autoload.php');
        if($rvs_live_mode == "on") {
            $client_id = $rvs_paypal_settings['live_client_id'];
            $client_secret = $rvs_paypal_settings['live_client_secret'];
            $api_mode = array('mode' => 'live');
            $paypal_key = "paypal";
        } else {
            $client_id = $rvs_paypal_settings['sandbox_client_id'];
            $client_secret = $rvs_paypal_settings['sandbox_client_secret'];
            $api_mode = array('mode' => 'sandbox');
            $paypal_key = "paypal_test";
        }

        // CHECK THAT CLIENT AND SECRET ARE VALID

        if($client_id == "" || $client_secret == "") {
            exit;
        }

        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                $client_id,  // ClientID
                $client_secret  // ClientSecret
            )
        );
        $apiContext->setConfig($api_mode);

        if(!empty($user_memberships)) {
            foreach($user_memberships as $key => &$membership) {
                if(isset($membership['type']) && $membership['type'] == $paypal_key) {
                    try {
                        $agreement = PayPal\Api\Agreement::get($membership['id'], $apiContext);
                        $membership['status'] = $agreement->state;
                        if( isset($agreement->agreement_details->next_billing_date) && ! empty($agreement->agreement_details->next_billing_date) ) {
                            $agreement_ends = strtotime($agreement->agreement_details->next_billing_date);
                            if( $agreement_ends > strtotime($membership["ends"]) ) {
                                $membership["ends"] = $agreement->agreement_details->next_billing_date;
                                $membership["reminder_sent"] = 0;
                            }
                        } else {
                            if( $agreement->state == "Pending" && isset($agreement->start_date) ) {
                                $agreement_starts = strtotime($agreement->start_date);
                                if( $agreement_starts > strtotime($membership["ends"]) ) {
                                    $membership["ends"] = $agreement->start_date;
                                    $membership['status'] = 'Active';
                                    $membership["reminder_sent"] = 0;
                                }
                            }
                        }
                    } catch (Exception $ex) {
                        $paypal_error = wpvs_handle_paypal_error($ex);
                        if( ! empty($paypal_error['name']) && $paypal_error['name'] == "INVALID_PROFILE_ID") {
                            unset($user_memberships[$key]);
                            $remove_membership = $membership;
                        }
                    }
                }
            }
            update_user_meta( $user_id, $get_memberships, $user_memberships);
            if( ! empty($remove_membership) ) {
                wpvs_do_after_delete_membership_action($user_id, $remove_membership);
            }
        }
    }

    // CHECK USER RENTALS
    rvs_get_user_rentals($user_id);
}
add_action('wpvs_check_plans_event', 'wpvs_check_membership_plans', 10, 1);

function wpvs_set_user_plan_check($user_login, $user) {
    wp_schedule_single_event( current_time('timestamp', 1) + 1, 'wpvs_check_plans_event', array($user));
    rvs_check_rentals_purchases($user->ID);
}
add_action('wp_login', 'wpvs_set_user_plan_check', 10, 2);

// UPDATE USER MEMBERSHIP STATUS

function rvs_update_user_membership_status($user_id, $plan_id, $status, $renewal, $live, $cancel_at_period_end) {
    $current_time = current_time('timestamp', 1);
    if($live) {
        $get_memberships = 'rvs_user_memberships';
    } else {
        $get_memberships = 'rvs_user_memberships_test';
    }
    $user_memberships = get_user_meta($user_id, $get_memberships, true);
    if(!empty($user_memberships)) {
        foreach($user_memberships as $key => &$membership) {
            if($membership["plan"] == $plan_id) {
                $membership["status"] = $status;
                if($cancel_at_period_end) {
                    $membership["ends_next_date"] = 1;
                    $membership["status"] = 'canceled';
                } else {
                    if(isset($membership["ends_next_date"]) && ! empty($membership["ends_next_date"])) {
                        $membership["ends_next_date"] = 0;
                        unset($membership["ends_next_date"]);
                    }
                }
                if($renewal > $current_time) {
                    $membership["ends"] = $renewal;
                    $membership["reminder_sent"] = 0;
                }
                break;
            }
        }
        update_user_meta($user_id, $get_memberships, $user_memberships);
    }
}

function rvs_check_rentals_purchases($user_id) {
    $user_video_access = get_user_meta($user_id, 'rvs_user_video_access', true);
    $user_video_rentals = get_user_meta($user_id, 'rvs_user_video_rentals', true);
	$user_video_purchases = get_user_meta($user_id, 'rvs_user_video_purchases', true);
    if( empty($user_video_rentals) ) {
        $user_video_rentals = array();
    }
    if( empty($user_video_purchases) ) {
        $user_video_purchases = array();
    }
    $rentals_and_purchases = array_merge($user_video_rentals, $user_video_purchases);
    if( empty( $rentals_and_purchases ) ) {
        update_user_meta($user_id, 'rvs_user_video_access', array());
	} else {
        $check_all_videos = array_column($rentals_and_purchases, 'video' );
        if( ! empty($user_video_access) && is_array($user_video_access) ) {
            foreach($user_video_access as $key => $video) {
                if(empty($video) || ! in_array($video, $check_all_videos) ) {
                    unset($user_video_access[$key]);
                }
            }

        } else {
            $user_video_access = array();
        }
        update_user_meta($user_id, 'rvs_user_video_access', $user_video_access);
    }
}

// GET USER RENTALS

function rvs_get_user_rentals($user_id) {
    $user_video_rentals = get_user_meta($user_id, 'rvs_user_video_rentals', true);
    $found_rentals = array();
    if(!empty($user_video_rentals)) {
        $user_video_access = get_user_meta($user_id, 'rvs_user_video_access', true);
        foreach($user_video_rentals as $key => &$rental) {
            $video_id = intval($rental['video']);
            $video_expires = $rental['expires'];
            $current_time = time();
            if($video_expires < $current_time) {
                unset($user_video_rentals[$key]);
                if(!empty($user_video_access)) {
                    $remove_video = array_search($video_id, $user_video_access);
                    if($remove_video) {
                        unset($user_video_access[$remove_video]);
                        update_user_meta($user_id, 'rvs_user_video_access', $user_video_access);
                    }
                }
            } else {
                $video = get_post($video_id);
                if(!empty($video) && $video->post_status == 'publish') {
                    $found_rentals[] = array('id' => $video_id, 'video' => $video, 'expires' => $video_expires);
                }
            }
        }
        update_user_meta($user_id, 'rvs_user_video_rentals', $user_video_rentals);
    }
    return $found_rentals;
}

function rvs_get_user_purchases($user_id) {
    $user_video_purchases = get_user_meta($user_id, 'rvs_user_video_purchases', true);
    $found_purchases = array();
    if(!empty($user_video_purchases)) {
        foreach($user_video_purchases as $purchase) {
            $video_id = intval($purchase['video']);
            $video_expires = $purchase['expires'];
            $video = get_post($video_id);
            if(!empty($video) && $video->post_status == 'publish') {
                $found_purchases[] = array('id' => $video_id, 'video' => $video, 'expires' => $video_expires);
            }
        }
    }
    return $found_purchases;
}

function wpvs_get_user_term_purchases($user_id) {
    $user_term_purchases = get_user_meta($user_id, 'rvs_user_term_purchases', true);
    $found_purchases = array();
    $found_term_error = false;
    if(!empty($user_term_purchases)) {
        foreach($user_term_purchases as $key => &$term_id) {
            $wpvs_term = get_term(intval($term_id), 'rvs_video_category' );
            if( ! empty($wpvs_term) && ! is_wp_error($wpvs_term) ) {
                $found_purchases[] = array('id' => $term_id, 'term' => $wpvs_term);
            } else {
                unset($user_term_purchases[$key]);
                $found_term_error = true;
            }
        }
        if($found_term_error) {
            update_user_meta($user_id, 'rvs_user_term_purchases', $user_term_purchases);
        }
    }
    return $found_purchases;
}

function rvs_memberships_content_payments($has_membership, $membership_list, $onetime_price, $rental_price, $term_purchase_options) {
    $return_content = "";
    $membership_content = "";
    $purchase_content = "";
    $get_access_content = "";
    $purchase_available = false;
    $get_access_content .= '<div id="rvs-access-options" class="rvs-border-box">';
    $access_tab_list = "";
    $access_tab_count = 0;
    $load_option = "membership";
    if( ! empty($membership_list) ) {
        global $post;
        $rvs_payment_page = get_option('rvs_payment_page');
        $payment_link = get_permalink($rvs_payment_page);
        $current_video_link = urlencode(get_permalink($post->ID));
        $ql = "?";
        if(strpos($payment_link , '?page_id')) {
            $ql = "&";
        }
        $all_rvs_memberships = get_option('rvs_membership_list');
        $no_access_message = $has_membership['message'];
        $show_required_memberships = true;
        if(isset($has_membership['reason']) && ( $has_membership['reason'] == "overdue" || $has_membership['reason'] == "noaccess" ) ) {
            $show_required_memberships = false;
        }
        $return_content .= '<p class="rvs-area rvs-error">' . $no_access_message . '</p>';
        if($show_required_memberships) {
            $access_tab_count++;
            $access_tab_list .= '<label id="rvs-memberships-access-tab" class="rvs-access-tab active">'.__('Subscribe', 'vimeo-sync-memberships').'</label>';
            $membership_content = '<div id="rvs-membership-access" class="rvs-access-section active"><h4>'.__('Subscribe', 'vimeo-sync-memberships').'</h4>';
            $video_has_memberships = true;
            $wpvs_displayed_memberships = array();
            foreach($membership_list as $membership_id) {
                foreach($all_rvs_memberships as $key => $membership) {
                    if( $membership_id == $membership['id'] && ! $membership['hidden'] && ! in_array($membership_id, $wpvs_displayed_memberships) ) {
                        $subscribe_label = wpvs_currency_label($membership['amount'], $membership['interval'], $membership['interval_count'], true);
                        $membership_content .= '<div class="rvs-membership-item"><h4>'.$membership['name'].'</h4><p>'.$membership['description'].'</p><label class="wpvs-price">'.$subscribe_label.'</label><a class="rvs-button rvs-primary-button" href="'.$payment_link.$ql.'id=' . $membership['id'] .'&redirect='.$current_video_link.'">'.__('Subscribe', 'vimeo-sync-memberships').'</a></div>';
                        $wpvs_displayed_memberships[] = $membership_id;
                    }
                }
            }
            $membership_content .= '</div>';
        }

    }

    if( ! empty($term_purchase_options) && empty($onetime_price) && empty($rental_price) ) {
        $access_tab_count++;
        if( empty($membership_content) ) {
            $access_tab_list .= '<label id="rvs-purchase-access-tab" class="rvs-access-tab active">'.__('Buy', 'vimeo-sync-memberships').'</label>';
            $purchase_content .= '<div id="rvs-purchase-access" class="rvs-access-section active">';
            $load_option = "purchase";
        } else {
            $access_tab_list .= '<label id="rvs-purchase-access-tab" class="rvs-access-tab">'.__('Buy', 'vimeo-sync-memberships').'</label>';
            $purchase_content .= '<div id="rvs-purchase-access" class="rvs-access-section">';
        }
        $purchase_content .= '<div class="wpvs-term-purchases"><h3>'.__('Purchase Options', 'vimeo-sync-memberships').'</h3>';
        foreach($term_purchase_options as $term_id) {
            $wpvs_term = get_term(intval($term_id), 'rvs_video_category');
            if( $wpvs_term && ( ! is_wp_error($wpvs_term) ) ) {
                $wpvs_term_link = get_term_link($term_id, 'rvs_video_category');
                $wpvs_term_purchase_price = get_term_meta($term_id, 'wpvs_category_purchase_price', true);
                $purchase_label = wpvs_currency_label($wpvs_term_purchase_price, false, false, false).__('Buy Access to', 'vimeo-sync-memberships').' '.$wpvs_term->name.'<span class="dashicons dashicons-arrow-right-alt2"></span>';
                $purchase_content .= '<a href="'.$wpvs_term_link.'?view=purchase" class="wpvs-purchase-term-link">'.$purchase_label.'</a>';
            }
        }
        $purchase_content .= '</div></div>';
    }

    if(!empty($onetime_price) || !empty($rental_price)) {
        if(empty($membership_content)) {
            $purchase_content .= '<div id="rvs-purchase-access" class="rvs-access-section active">';
        } else {
            $purchase_content .= '<div id="rvs-purchase-access" class="rvs-access-section">';
        }
        if( ! empty($term_purchase_options) ) {
            $purchase_content .= '<div class="wpvs-term-purchases"><h5>'.__('Other Purchase Options', 'vimeo-sync-memberships').'</h5>';
            foreach($term_purchase_options as $term_id) {
                $wpvs_term = get_term(intval($term_id), 'rvs_video_category');
                if( $wpvs_term && ( ! is_wp_error($wpvs_term) ) ) {
                    $wpvs_term_link = get_term_link($term_id, 'rvs_video_category');
                    $wpvs_term_purchase_price = get_term_meta($term_id, 'wpvs_category_purchase_price', true);
                    $purchase_label = wpvs_currency_label($wpvs_term_purchase_price, false, false, false).__('Buy Access to', 'vimeo-sync-memberships').' '.$wpvs_term->name.'<span class="dashicons dashicons-arrow-right-alt2"></span>';
                    $purchase_content .= '<a href="'.$wpvs_term_link.'?view=purchase" class="wpvs-purchase-term-link">'.$purchase_label.'</a>';
                }
            }
            $purchase_content .= '</div>';
        }
        ob_start();
        include(RVS_MEMBERS_BASE_DIR .'/includes/single-payment-form.php');
        $purchase_content .= ob_get_contents();
        ob_end_clean();
        $purchase_content .= '</div>';
    }

    if(!empty($onetime_price)) {
        $access_tab_count++;
        $purchase_label = wpvs_currency_label($onetime_price, false, false, false).__('Buy', 'vimeo-sync-memberships');
        if(empty($membership_content)) {
            $access_tab_list .= '<label id="rvs-purchase-access-tab" class="rvs-access-tab wpvs-single-purchase active">'.$purchase_label.'</label>';
            $load_option = "purchase";
        } else {
            $access_tab_list .= '<label id="rvs-purchase-access-tab" class="rvs-access-tab wpvs-single-purchase">'.$purchase_label.'</label>';
        }
        $purchase_available = true;
    }

    if(!empty($rental_price)) {
        $access_tab_count++;
        $rental_label = wpvs_currency_label($rental_price, false, false, false).__('Rent', 'vimeo-sync-memberships');
        if(empty($membership_content) && !$purchase_available) {
            $access_tab_list .= '<label id="rvs-rental-access-tab" class="rvs-access-tab active">'.$rental_label.'</label>';
            $load_option = "rental";
        } else {
            $access_tab_list .= '<label id="rvs-rental-access-tab" class="rvs-access-tab">'.$rental_label.'</label>';
        }
    }

    if( ! empty($access_tab_list) && $access_tab_count > 1) {
        $get_access_content .= '<div id="rvs-access-tabs" class="rvs-border-box">';
        $get_access_content .= $access_tab_list;
        $get_access_content .='</div>';
    }

    $get_access_content .= '<input type="hidden" id="wpvs-load-purchase-type" value="'.$load_option.'" />';

    if(!empty($membership_content)) {
        $get_access_content .= $membership_content;
    }

    if(!empty($purchase_content)) {
        $get_access_content .= $purchase_content;
    }

    $get_access_content .= '</div>';
    $return_content .= $get_access_content;
    return $return_content;
}

function wpvs_currency_label($price, $interval, $interval_count, $has_interval) {
    global $wpvs_currency_label;
    global $rvs_currency;
    if( $rvs_currency == "JPY" ) {
        // remove decimals for Japanese Yen
        $price_format = number_format($price/100,0);
    } else {
        $price_format = number_format($price/100,2);
    }

    $price_format = apply_filters('wpvs_filter_currency_label_price_amount', $price_format); // filter price amount before currency label

    $wpvs_currency_position = get_option('wpvs_currency_position', "after");
    if( $wpvs_currency_position == 'after') {
        $currency_label = $price_format.$wpvs_currency_label.' ';
    } else {
        $currency_label = $wpvs_currency_label.$price_format.' ';
    }
    if($has_interval) {
        $interval_text = wpvs_translate_intervals($interval_count, $interval);
        if($interval_count > 1) {
            $interval_text = $interval_count . ' ' . $interval_text;
        }
        $currency_label .= '/ '.$interval_text;
    }
    $currency_label = apply_filters('wpvs_filter_currency_label', $currency_label); // filter currency label
    return $currency_label;
}

function wpvs_is_current_term_purchase() {
    global $wp_query;
    $wpvs_term_purchase_price = null;
    $wpvs_user_has_purchased = false;
    $wpvs_is_term_purchase_content = "";
    if( wpvs_check_for_membership_add_on() && isset($_GET['view'])) {
        if( is_user_logged_in() ) {
            global $rvs_current_user;
            $user_term_purchases = get_user_meta($rvs_current_user->ID, 'rvs_user_term_purchases', true);
            $wpvs_current_term = get_term($wp_query->get_queried_object_id(), 'rvs_video_category' );
            $wpvs_view = $_GET['view'];
            if( ! empty($wpvs_current_term) && ! is_wp_error($wpvs_current_term) && ! empty($wpvs_view) && $wpvs_view == "purchase" ) {
                if( ! empty($user_term_purchases) && in_array($wpvs_current_term->term_id, $user_term_purchases) ) {
                    $wpvs_user_has_purchased = true;
                } else {
                    $wpvs_term_purchase_price = get_term_meta($wpvs_current_term->term_id, 'wpvs_category_purchase_price', true);
                }
            }
            if( ! empty($wpvs_term_purchase_price) && ! $wpvs_user_has_purchased ) {
                ob_start();
                include(RVS_MEMBERS_BASE_DIR .'/includes/single-payment-form.php');
                $wpvs_is_term_purchase_content .= ob_get_contents();
                ob_end_clean();
            }
        } else {
            $wpvs_is_term_purchase_content .= '<div id="rvs-area" class="rvs-area">';
            ob_start();
            include(RVS_MEMBERS_BASE_DIR .'/template/wpvs-login-form.php');
            $wpvs_is_term_purchase_content .= ob_get_contents();
            ob_end_clean();
            $wpvs_is_term_purchase_content .= '</div>';
        }
    }
    return $wpvs_is_term_purchase_content;
}
