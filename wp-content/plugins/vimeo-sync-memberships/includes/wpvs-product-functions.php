<?php

if( ! function_exists('wpvs_add_product_to_customer') ) {
    function wpvs_add_product_to_customer($user_id, $product_id, $purchase_type) {
        $user_video_access = get_user_meta($user_id, 'rvs_user_video_access', true);
        
        if(empty($user_video_access) || ! is_array($user_video_access)) {
            $user_video_access = array();
        }

        if($purchase_type == "rental") {
            $user_video_rentals = get_user_meta($user_id, 'rvs_user_video_rentals', true);
            if(empty($user_video_rentals)) {
                $user_video_rentals = array();
            }
            $expires = get_post_meta( $product_id, 'rvs_rental_expires', true );
            $interval = get_post_meta( $product_id, 'rvs_rental_type', true );
            $time_string = "+".$expires." ".$interval;
            $expires_date = strtotime($time_string);
            if(!empty($expires_date) && !in_array($product_id, $user_video_access)) {
                $new_rental = array('video' => $product_id, 'expires' => $expires_date);
                $user_video_rentals[] = $new_rental;
                update_user_meta($user_id, 'rvs_user_video_rentals', $user_video_rentals);
            }
        }

        if($purchase_type == "purchase") {
            $user_video_purchases = get_user_meta($user_id, 'rvs_user_video_purchases', true);
            if(empty($user_video_purchases)) {
                $user_video_purchases = array();
            }
            $new_purchase = array('video' => $product_id, 'expires' => "never");
            $user_video_purchases[] = $new_purchase;
            update_user_meta($user_id, 'rvs_user_video_purchases', $user_video_purchases);
        }

        if($purchase_type == "termpurchase") {
            $user_term_purchases = get_user_meta($user_id, 'rvs_user_term_purchases', true);
            if(empty($user_term_purchases)) {
                $user_term_purchases = array();
            }
            $user_term_purchases[] = intval($product_id);
            $user_term_purchases = array_unique($user_term_purchases);
            update_user_meta($user_id, 'rvs_user_term_purchases', $user_term_purchases);
        }

        if( ! empty($product_id) ) {
            $user_video_access[] = $product_id;
            $user_video_access = array_unique($user_video_access);
            update_user_meta($user_id, 'rvs_user_video_access', $user_video_access);
        }
        wpvs_do_after_new_product_purchase_action($user_id, $product_id, $purchase_type);
    }
}

if( ! function_exists('wpvs_add_membership_to_customer') ) {
    function wpvs_add_membership_to_customer($user_id, $membership) {
        global $rvs_live_mode;
        $wpvs_membership_manager = new WPVS_Membership_Plan($membership['plan']);
        if($rvs_live_mode == "on") {
            $get_memberships = 'rvs_user_memberships';
            $membership_role = 'wpvs_'.$wpvs_membership_manager->plan_id.'_role';
		} else {
            $get_memberships = 'rvs_user_memberships_test';
            $membership_role = 'wpvs_'.$wpvs_membership_manager->plan_id.'_role_test';
		}
        $user_memberships = get_user_meta($user_id, $get_memberships, true);
        if(empty($user_memberships)) {
            $user_memberships = array();
        }
        $current_time = current_time('timestamp', 1);
        $membership['start_time'] = $current_time;
        $user_memberships[] = $membership;
        update_user_meta( $user_id, $get_memberships, $user_memberships);

        if( $wpvs_membership_manager->has_role && get_role($membership_role) ) {
            $user = get_user_by('id', $user_id);
            if ( ! in_array( $membership_role, $user->roles ) ) {
                $user->add_role($membership_role);
            }
        }
        
        wpvs_send_email("New", $user_id, $membership['name']);
        wpvs_do_after_new_membership_action($user_id, $membership);
    }
}

if( ! function_exists('wpvs_reverse_product_purchase') ) {
    function wpvs_reverse_product_purchase($charge_id, $gateway) {
        global $rvs_live_mode;
        if($rvs_live_mode == "off") {
            $test_mode = true;
        } else {
            $test_mode = false;
        }
        if($gateway == "stripe") {
            $found_payment = wpvs_get_stripe_payment($charge_id, $test_mode);
        }
        if($gateway == "paypal") {
            $found_payment = wpvs_get_paypal_payment($charge_id, $test_mode);
        }
        
        if( isset($found_payment[0]) && ! empty($found_payment[0]) ) {
            $payment = $found_payment[0];
            if( isset($payment->userid) && isset($payment->type) && isset($payment->productid) ) {
                $user_id = $payment->userid;
                $purchase_type = $payment->type;
                $product_id = $payment->productid;
                $user_video_access = get_user_meta($user_id, 'rvs_user_video_access', true);
        
                if(empty($user_video_access) || ! is_array($user_video_access)) {
                    $user_video_access = array();
                }

                if($purchase_type == "rental") {
                    $user_video_rentals = get_user_meta($user_id, 'rvs_user_video_rentals', true);
                    if( ! empty($user_video_rentals)) {
                        foreach($user_video_rentals as $key => &$rental) {
                            if($rental['video'] == $product_id) {
                                unset($user_video_rentals[$key]);
                                break;
                            }
                        }
                    }
                    update_user_meta($user_id, 'rvs_user_video_rentals', $user_video_rentals);
                }

                if($purchase_type == "purchase") {
                    $user_video_purchases = get_user_meta($user_id, 'rvs_user_video_purchases', true);
                    if( ! empty($user_video_purchases)) {
                        foreach($user_video_purchases as $key => &$purchase) {
                            if($purchase['video'] == $product_id) {
                                unset($user_video_purchases[$key]);
                                break;
                            }
                        }
                    }
                    update_user_meta($user_id, 'rvs_user_video_purchases', $user_video_purchases);
                }

                if($purchase_type == "termpurchase") {
                    $user_term_purchases = get_user_meta($user_id, 'rvs_user_term_purchases', true);
                    if( ! empty($user_term_purchases)) {
                        foreach($user_term_purchases as $key => &$termpurchase) {
                            if( intval($termpurchase) == intval($product_id) ) {
                                unset($user_term_purchases[$key]);
                                break;
                            }
                        }
                    }
                    update_user_meta($user_id, 'rvs_user_term_purchases', $user_term_purchases);
                }
            
                if( ! empty($user_video_access)) {
                    foreach($user_video_access as $key => &$access) {
                        if( intval($access) == intval($product_id) ) {
                            unset($user_video_access[$key]);
                            break;
                        }
                    }
                    update_user_meta($user_id, 'rvs_user_video_access', $user_video_access);
                } 
            }
        }
    }
}