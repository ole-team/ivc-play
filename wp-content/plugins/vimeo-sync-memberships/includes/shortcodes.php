<?php

function rvs_shortcode_payment_form() {
    global $wpvs_stripe_options;
    global $rvs_paypal_settings;
    global $rvs_live_mode;
    global $rvs_currency;
    global $wpvs_currency_label;
    global $rvs_paypal_enabled;
    global $wpvs_stripe_gateway_enabled;
    global $wpvs_google_apple_enabled;
    global $wpvs_stripe_checkout_enabled;
    global $wpvs_coingate_enabled;
    global $wpvs_coinbase_enabled;
    global $wpvs_add_stripe_forms;
    global $wpvs_add_paypal_forms;
    global $wpvs_requires_save_card;
    $wpvs_requires_save_card = true;
    $wpvs_add_stripe_forms = false;
    $wpvs_add_paypal_forms = false;
    $membership_active = false;
    $wpvs_checkout_form_content = "";
    if ( is_user_logged_in() ) {
        global $rvs_current_user;
        $wpvs_customer = new WPVS_Customer($rvs_current_user);
        $wpvs_checkout = new WPVS_Checkout($wpvs_customer);
        $wpvs_checkout->set_checkout_type('subscription');
        $wpvs_checkout->set_transaction_type('subscription');
        $wpvs_checkout->set_purchase_type('subscription');
        $membership_id = get_query_var( 'id' );
        if( ! empty($membership_id) ) {
            $wpvs_membership_plan = new WPVS_Membership_Plan($membership_id);
            if( ! $wpvs_membership_plan->hidden ) {
                $has_membership = $wpvs_customer->has_membership_by_id($membership_id);
                if( ! $has_membership['has_access'] ) {
                    $wpvs_checkout->set_membership($membership_id);
                    $wpvs_checkout_form_content .= $wpvs_checkout->checkout_content();
                } else {
                    if(isset($has_membership['reason']) && isset($has_membership['message'])) {
                        $wpvs_checkout_form_content .= sprintf('%s'. __('Oops', 'vimeo-sync-memberships') .'!%s%s'. __('Looks like you are already subscribed to the', 'vimeo-sync-memberships') . ' ' . $wpvs_membership_plan->name . ' ' . __('plan', 'vimeo-sync-memberships') . '%s', '<h3>','</h3>', '<p>', '</p>');
                        $wpvs_checkout_form_content .= '<p>'.$has_membership['message'].'</p>';
                    } else {
                        $wpvs_checkout_form_content .= sprintf('%s'. __('Oops', 'vimeo-sync-memberships') .'!%s%s'. __('Looks like you are already subscribed to the', 'vimeo-sync-memberships') . ' ' . $wpvs_membership_plan->name . ' ' . __('plan', 'vimeo-sync-memberships') . '%s', '<h3>','</h3>', '<p>', '</p>');
                    }
                    $wpvs_checkout_form_content .= '<a class="rvs-button rvs-primary-button" href="'.$wpvs_checkout->account_link.'">'.__('Manage Subscriptions', 'vimeo-sync-memberships').'</a>';
                }
            } else {
                $register_page = esc_attr( get_option('rvs_sign_up_page'));
                $registration_link = get_permalink($register_page);
                $wpvs_checkout_form_content .= sprintf('%s'. __('Sorry', 'vimeo-sync-memberships') .'!%s%s'. __('This membership is not available', 'vimeo-sync-memberships') . '%s', '<h3>','</h3>', '<p>', '</p>');
                $wpvs_checkout_form_content .= '<a class="rvs-button rvs-primary-button" href="'.$registration_link.'">'.__('View Memberships', 'vimeo-sync-members').'</a>';
            }
        } else { # END MEMBERSHIP ID CHECK
            $register_page = esc_attr( get_option('rvs_sign_up_page'));
            $registration_link = get_permalink($register_page);
            $wpvs_checkout_form_content .= sprintf('%s'. __('Please select a membership', 'vimeo-sync-memberships') .'%s', '<h3>','</h3>');
            $wpvs_checkout_form_content .= '<a class="rvs-button rvs-primary-button" href="'.$registration_link.'">'.__('View Memberships', 'vimeo-sync-members').'</a>';
        }
    } else {
        $wpvs_checkout_form_content .= '<div id="rvs-area" class="rvs-area">';
            ob_start();
            include(RVS_MEMBERS_BASE_DIR .'/template/wpvs-login-form.php');
            $wpvs_checkout_form_content .= ob_get_contents();
            ob_end_clean();
        $wpvs_checkout_form_content .= '</div>';
    }
    return $wpvs_checkout_form_content;
}
add_shortcode('rvs_payment_form', 'rvs_shortcode_payment_form');

function rvs_membership_list() {
    $wpvs_membership_list_content = wpvs_generate_membership_options_html(null, null);
    return $wpvs_membership_list_content;
}
add_shortcode('rvs_memberships', 'rvs_membership_list');

function rvs_account_page() {
    $wpvs_account_page_content = "";
    if(is_user_logged_in()) {
        global $rvs_current_user;
        global $wpvs_stripe_customer;
        global $rvs_paypal_enabled;
        global $wpvs_stripe_gateway_enabled;
        global $rvs_currency;
        global $wpvs_currency_label;
        global $wpvs_stripe_options;
        global $rvs_live_mode;

        $wpvs_customer = new WPVS_Customer($rvs_current_user);
        $wpvs_checkout = new WPVS_Checkout($wpvs_customer);
        $wpvs_stripe_payments_manager = new WPVS_Stripe_Payments_Manager($rvs_current_user->ID);
        $rvs_account_sub_menu = get_option('rvs_account_sub_menu', 1);
        $wpvs_viewing = get_option('wpvs_default_account_menu_item', 'memberships');
        $wpvs_require_billing_information = get_option('wpvs_require_billing_information', 0);
        if( ! empty($_GET['wpvsview']) ) {
            $wpvs_viewing = $_GET['wpvsview'];
        }
        // ========= GET USER INFORMATION ==========
        if($wpvs_stripe_customer != null) {
            $stripe_customer_invoices = $wpvs_stripe_payments_manager->get_customer_invoices(null);
        }
        $wpvs_account_page_content .= '<div id="rvs-account-info" class="rvs-area">';
        if( $rvs_account_sub_menu ) {
            $wpvs_account_page_content .= wpvs_generate_customer_menu($wpvs_viewing);
        }
        $wpvs_account_page_content .= '<div id="wpvs-account-memberships" class="wpvs-account-section';
        if($wpvs_viewing == "memberships") {
            $wpvs_account_page_content .= ' active';
        }
        $wpvs_account_page_content .= '">';
        $wpvs_account_page_content .= '<h4>'.__('Your Memberships', 'vimeo-sync-memberships').'</h4>';
        # GET USER MEMBERSHPS

        $user_memberships = $wpvs_customer->get_memberships();
        if(!empty($user_memberships)) {
            $wpvs_account_page_content .= wpvs_generate_user_memberships_output($user_memberships, false);
        } else {
            $membership_list = get_option('rvs_membership_list');
            $rvs_sign_up_page = get_option('rvs_sign_up_page');
            $rvs_sign_up_page_link = get_permalink($rvs_sign_up_page);
            $wpvs_account_page_content .= sprintf('<p>' . __('You have not signed up for any memberships.', 'vimeo-sync-memberships') . '</p>');
            if( !empty($membership_list) ) {
                $wpvs_account_page_content .= sprintf('<a class="rvs-button rvs-primary-button" href="%s">' . __('View Memberships', 'vimeo-sync-memberships') . '</a>', $rvs_sign_up_page_link);
            }
        }

        $wpvs_account_page_content .= '</div>'; # END MEMBERSHIPS


        # CUSTOMER CARDS AND BILLING ADDRESS
        $wpvs_account_page_content .= '<div id="wpvs-account-cards" class="wpvs-account-section';
        if($wpvs_viewing == "cards") {
            $wpvs_account_page_content .= ' active';
        }
        $wpvs_account_page_content .= '">';

        if( $wpvs_require_billing_information ) {
            $wpvs_account_page_content .= $wpvs_checkout->generate_billing_fields(__('Save Changes', 'vimeo-sync-memberships'), true);
        }

        if($wpvs_stripe_gateway_enabled) {
            $wpvs_stripe_invoices_enabled = false;
            $payment_methods = $wpvs_stripe_payments_manager->get_payment_methods();
            if( isset($wpvs_stripe_options['invoices_enabled']) && ! empty($wpvs_stripe_options['invoices_enabled']) ) {
                $wpvs_stripe_invoices_enabled = true;
            }

            if( ($wpvs_stripe_customer == null) ||  empty($payment_methods) ) {
                $wpvs_account_page_content .= '<p>'.__('No cards saved', 'vimeo-sync-memberships').'</p>';
            } else {
                $default_card = $wpvs_stripe_customer["invoice_settings"]["default_payment_method"];

                $wpvs_account_page_content .= '<h4>'.__('Saved Cards', 'vimeo-sync-memberships').'</h4>';
                $wpvs_account_page_content .= '<div class="wpvs-checkout-section rvs-border-box">';
                $wpvs_account_page_content .= '<table class="rvs_memberships"><tbody><tr>';
                $wpvs_account_page_content .= '<th>'.__('Card Number', 'vimeo-sync-memberships').'</th>';
                $wpvs_account_page_content .= '<th>'.__('Default', 'vimeo-sync-memberships').'</th>';
                $wpvs_account_page_content .= '<th class="rvs-remove-edit">'.__('Edit', 'vimeo-sync-memberships').'</th>';
                $wpvs_account_page_content .= '</tr>';
                foreach($payment_methods as $card) {
                    $wpvs_account_page_content .= '<tr>';
                    $wpvs_account_page_content .= '<td>****' . $card->card->last4 . '</td>';
                    if($card->id == $default_card) {
                        $wpvs_account_page_content .= '<td><input type="radio" class="rvs-radio rvs-update-default" id="'. $card->id .'" name="card_default" value="'. $card->id .'" checked></td>';
                    } else {
                        $wpvs_account_page_content .= '<td><input type="radio" class="rvs-radio rvs-update-default" id="'. $card->id .'" name="card_default" value="'. $card->id .'"></td>';
                    }
                    $wpvs_account_page_content .= sprintf('<td class="rvs-remove-edit"><label class="rvs-edit-card">' . __('Edit', 'vimeo-sync-memberships') . '</label> | <label class="rvs-delete-card rvs-remove"> ' . __('Delete', 'vimeo-sync-memberships') . '</label></td></tr>');
                }
                $wpvs_account_page_content .= '</tbody></table></div>';
                # EDIT CARD FORM
                $wpvs_account_page_content .= '<div id="edit-card-form">';
                $wpvs_account_page_content .= '<h4 id="rvs-editing-card-title">'.__('Editing Card', 'vimeo-sync-memberships').' ****</h4>';
                $wpvs_account_page_content .= '<div class="rvs-form-row">';
                $wpvs_account_page_content .= '<label>'.__('Expiration (MM/YYYY)', 'vimeo-sync-memberships').'</label>';
                $wpvs_account_page_content .= '<input type="text" size="2" class="edit-card-expiry-month"/><span> / </span><input type="text" size="4" class="edit-card-expiry-year"/>';
                $wpvs_account_page_content .= '</div>';
                $wpvs_account_page_content .= '<input type="hidden" id="rvs-card-edit-input" name="rvs-card-edit-input" />';
                $wpvs_account_page_content .= '<label class="rvs-button rvs-primary-button" id="rvs-save-card-changes">'.__('Save Changes', 'vimeo-sync-memberships').'</label><label class="rvs-button rvs-red-button" id="rvs-cancel-card-changes">'.__('Cancel', 'vimeo-sync-memberships').'</label>';
                $wpvs_account_page_content .= '</div>';
            }
            $wpvs_account_page_content .= '<label class="rvs-button rvs-primary-button" id="wpvs-show-card-form">'.__('Add New Card', 'vimeo-sync-memberships').'</label>';
            $wpvs_account_page_content .= wpvs_new_credit_card_form();
            $wpvs_account_page_content .= '</div>'; # END CUSTOMER CARDS
            $wpvs_account_page_content .= '<div id="wpvs-account-payments" class="wpvs-account-section';
            if($wpvs_viewing == "payments") {
                $wpvs_account_page_content .= ' active';
            }
            $wpvs_account_page_content .= '">';
            $wpvs_account_page_content .= '<h4>'.__('Payments', 'vimeo-sync-memberships').'</h4>';
            if( $wpvs_stripe_customer != null && ! empty($stripe_customer_invoices) ) {
                $wpvs_account_page_content .= '<table id="payments" class="rvs_memberships"><tbody><tr>';
                $wpvs_account_page_content .= '<th>'.__('Amount', 'vimeo-sync-memberships').'</th>';
                $wpvs_account_page_content .= '<th>'.__('Date', 'vimeo-sync-memberships').'</th>';
                $wpvs_account_page_content .= '<th>'.__('Status', 'vimeo-sync-memberships').'</th>';
                if( $wpvs_stripe_invoices_enabled ) {
                    $wpvs_account_page_content .= '<th>'.__('Invoice', 'vimeo-sync-memberships').'</th></tr>';
                }

                foreach($stripe_customer_invoices as $charge) {
                    if($charge->paid) {
                        $paymentDate = date( 'M d, Y',$charge->created );
                        if($charge->refunded) {
                            $paid = __('Refunded', 'vimeo-sync-memberships');
                        } else {
                            if($charge->status == "succeeded") {
                                $paid = __('Paid', 'vimeo-sync-memberships');
                            } else {
                                $paid = $charge->status;
                            }
                        }
                        $wpvs_account_page_content .= '<tr><td>' .$rvs_currency. ' ' . number_format(($charge->amount)/100, 2) . '</td>';
                        $wpvs_account_page_content .= '<td>' . $paymentDate . '</td>';
                        $wpvs_account_page_content .= '<td id="' . $charge->id . '" class="wpvs-stripe-charge">' . $paid . '</td>';
                        if( $wpvs_stripe_invoices_enabled ) {
                            $wpvs_account_page_content .= '<td id="' . $charge->id . '">';
                            if( ! empty($charge->invoice) ) {
                                $wpvs_account_page_content .= '<label class="wpvs-download-invoice" data-invoice="'.$charge->invoice.'"><span class="dashicons dashicons-media-text"></span></label></td></tr>';
                            }
                        }

                    }
                }
                $wpvs_account_page_content .= '</tbody></table>';
                $wpvs_account_page_content .= '<nav id="rvs-payment-nav"><label id="rvs-next" class="rvs-button rvs-primary-button">'.__('More Payments', 'vimeo-sync-memberships').'</label></nav>';
            } else {
                $wpvs_account_page_content .= '<p>'.__('No payments yet', 'vimeo-sync-memberships').'</p>';
            }
        }
        $wpvs_account_page_content .= '</div>'; // END CARDS AND BILLING
        $wpvs_account_page_content .= '<div id="wpvs-account-information" class="wpvs-account-section';
        if($wpvs_viewing == "info") {
            $wpvs_account_page_content .= ' active';
        }
        $wpvs_account_page_content .= '">';
        $wpvs_account_page_content .= '<h4>'.__('My Account', 'vimeo-sync-memberships').'</h4>';
        $wpvs_account_page_content .= do_shortcode('[wpvs_customer_details_form]');
        $wpvs_account_page_content .= '</div>'; # END ACCOUNT INFORMATION
        $wpvs_account_page_content .= '</div>'; # END ACCOUNT AREA
    } else {
        $wpvs_account_page_content .= '<div id="rvs-area" class="rvs-area">';
        ob_start();
        include(RVS_MEMBERS_BASE_DIR .'/template/wpvs-login-form.php');
        $wpvs_account_page_content .= ob_get_contents();
        ob_end_clean();
        $wpvs_account_page_content .= '</div>';
    }
    return $wpvs_account_page_content;
}
add_shortcode('rvs_account', 'rvs_account_page');

function wpvs_new_credit_card_form() {
    global $rvs_current_user;
    global $wpvs_requires_save_card;
    $wpvs_requires_save_card = true;
    $stripe_payments_manager = new WPVS_Stripe_Payments_Manager($rvs_current_user->ID);
    $setup_intent = $stripe_payments_manager->new_setup_intent();
    $wpvs_new_card_form_content = '<div id="wpvs-add-credit-card-form"><form action="" method="POST" id="wpvs-new-stripe-card-form" class="wpvs-stripe-card-form"><div class="card-container">';
    ob_start();
    include(RVS_MEMBERS_BASE_DIR.'/template/card-form.php');
    $wpvs_new_card_form_content .= ob_get_contents();
    ob_end_clean();
    $wpvs_new_card_form_content .= '<button class="rvs-button rvs-primary-button rvs-pay-button" type="submit" id="wpvs-new-stripe-card" data-secret="'.$setup_intent['payment_intent_client_secret'].'">';
    $wpvs_new_card_form_content .= __('Add Card', 'vimeo-sync-memberships');
    $wpvs_new_card_form_content .= '</button>';
    $wpvs_new_card_form_content .= '</div></form></div>';
    return $wpvs_new_card_form_content;
}

if( ! function_exists('rvs_create_account_form') ) {
function rvs_create_account_form() {
    global $rvs_current_version;
    $wpvs_new_account_form_content = "";
    $wpvs_users_can_register = get_option( 'users_can_register' );
    $rvs_usernames_allowed = get_option('rvs_usernames_allowed', 1);
    $wpvs_require_first_last_name = get_option('wpvs_signup_require_first_last_name', 0);
    $wpvs_signup_create_password = get_option('wpvs_signup_create_password', 1);
    $wpvs_recaptcha_enabled = false;
    $wpvs_agreement_checkbox_enabled = false;

    // Google ReCaptcha
    $wpvs_google_recaptcha_settings = get_option('wpvs_google_recaptcha_settings');
    if( $wpvs_google_recaptcha_settings && isset($wpvs_google_recaptcha_settings['enabled']) ) {
        wp_register_script( 'wpvs-google-captcha', 'https://www.google.com/recaptcha/api.js', array('jquery'), '', true );
        wp_enqueue_script('wpvs-google-captcha');
        $wpvs_recaptcha_enabled = true;
    }

    // Terms & Agreement
    $wpvs_registration_agreement_settings = get_option('wpvs_registration_agreement_settings');

    if( $wpvs_registration_agreement_settings && isset($wpvs_registration_agreement_settings['enabled']) && isset($wpvs_registration_agreement_settings['agreement_content']) ) {
        $wpvs_agreement_checkbox_enabled = true;
    }

    wp_enqueue_script( 'rvs-create-account', RVS_MEMBERS_BASE_URL .'js/rvs-create-account.js', array('rvs-memberships-js'), $rvs_current_version, true);
    wp_localize_script('rvs-create-account', 'wpvsna', array(
        'ajax'            => admin_url('admin-ajax.php'),
        'usernames'       => $rvs_usernames_allowed,
        'firstlast'       => $wpvs_require_first_last_name,
        'passcreation'    => $wpvs_signup_create_password,
        'creatingaccount' => __('Creating account', 'vimeo-sync-memberships'),
        'recaptcha'       => $wpvs_recaptcha_enabled,
        'agreement_box'   => $wpvs_agreement_checkbox_enabled
    ));
    if($wpvs_users_can_register) {
        $wpvs_new_account_form_content .= '<p id="rvs-new-account-error" class="text-align-center rvs-form-error rvs-error"></p>';
        $wpvs_new_account_form_content .= '<form id="rvs-account-details" method="POST" action="" class="wpvs-login-form">';
        if($wpvs_require_first_last_name) {
            $wpvs_new_account_form_content .= '<div class="rvs-form-row">';
            $wpvs_new_account_form_content .= '<label>'.__('First Name', 'vimeo-sync-memberships').'</label>';
            $wpvs_new_account_form_content .= '<input id="new_user_first_name" name="new_user_first_name" type="text" required /></div>';
            $wpvs_new_account_form_content .= '<div class="rvs-form-row">';
            $wpvs_new_account_form_content .= '<label>'.__('Last Name', 'vimeo-sync-memberships').'</label>';
            $wpvs_new_account_form_content .= '<input id="new_user_last_name" name="new_user_last_name" type="text" required /></div>';
        }
        if($rvs_usernames_allowed) {
            $wpvs_new_account_form_content .= '<div class="rvs-form-row">';
            $wpvs_new_account_form_content .= '<label>'.__('Username', 'vimeo-sync-memberships').'</label>';
            $wpvs_new_account_form_content .= '<input id="new_user_name" name="new_user_name" type="text" required /></div>';
        }
        $wpvs_new_account_form_content .= '<div class="rvs-form-row">';
        $wpvs_new_account_form_content .= '<label>'.__('Email', 'vimeo-sync-memberships').'</label>';
        $wpvs_new_account_form_content .= '<input id="new_user_email" name="new_user_email" type="email" required /></div>';
        if($wpvs_signup_create_password) {
            $wpvs_new_account_form_content .= '<div class="rvs-form-row">';
            $wpvs_new_account_form_content .= '<label>'.__('Password', 'vimeo-sync-memberships').'</label>';
            $wpvs_new_account_form_content .= '<input id="new_user_password" name="new_user_password" type="password" required autocomplete="off"/></div>';

            $wpvs_new_account_form_content .= '<div class="rvs-form-row">';
            $wpvs_new_account_form_content .= '<label>'.__('Confirm Password', 'vimeo-sync-memberships').'</label>';
            $wpvs_new_account_form_content .= '<input id="confirm_user_password" name="confirm_user_password" type="password" required autocomplete="off"/></div>';
        }

        if( has_action('wpvs_add_fields_after_registration_form') ) {
            ob_start();
            wpvs_add_fields_after_registration_form_action();
            $wpvs_new_account_form_content .= ob_get_contents();
            ob_end_clean();
        }

        if( $wpvs_agreement_checkbox_enabled ) {
            $wpvs_new_account_form_content .= '<div class="rvs-form-row"><div class="wpvs-agreement-checkbox-container">';
            $wpvs_new_account_form_content .= '<input id="wpvs_agreement_checkbox" name="wpvs_agreement_checkbox" type="checkbox" required value="1" />';
            $wpvs_new_account_form_content .= '<label>'.$wpvs_registration_agreement_settings['agreement_content'].'</label>';
            $wpvs_new_account_form_content .= '</div></div>';
        }

        if( $wpvs_recaptcha_enabled && isset($wpvs_google_recaptcha_settings['site_key']) ) {
            $wpvs_new_account_form_content .= '<div class="rvs-form-row">';
            $wpvs_new_account_form_content .= '<div class="g-recaptcha" data-sitekey="'.$wpvs_google_recaptcha_settings['site_key'].'" data-theme="'.$wpvs_google_recaptcha_settings['theme'].'" data-size="'.$wpvs_google_recaptcha_settings['size'].'" data-callback="wpvs_verify_google_recaptcha"></div>';
            $wpvs_new_account_form_content .= '</div>';
            $wpvs_new_account_form_content .= '<div class="rvs-form-row">';
            $wpvs_new_account_form_content .= '<input type="submit" id="rvs-create-new-account" class="rvs-button rvs-primary-button wpvs-ca-disabled-recaptcha" value="'.__('Create Account', 'vimeo-sync-memberships').'" disabled="true"/></div>';
        } else {
            $wpvs_new_account_form_content .= '<div class="rvs-form-row">';
            $wpvs_new_account_form_content .= '<input type="submit" id="rvs-create-new-account" class="rvs-button rvs-primary-button" value="'.__('Create Account', 'vimeo-sync-memberships').'" /></div>';
        }
        $wpvs_new_account_form_content .= '</form>';
    } else {
        $wpvs_new_account_form_content .= '<p>'.__('Registration is disabled.', 'vimeo-sync-memberships').'</p>';
    }
    return $wpvs_new_account_form_content;
}
add_shortcode('rvs_create_account', 'rvs_create_account_form');
}

if( ! function_exists('wpvs_generate_customer_details_form') ) {
function wpvs_generate_customer_details_form() {
    if( is_user_logged_in() ) {
        global $rvs_current_user;
        global $rvs_current_version;
        $wpvs_update_account_form_content = "";
        $wpvs_users_can_register = get_option( 'users_can_register' );
        $rvs_usernames_allowed = get_option('rvs_usernames_allowed', 1);
        wp_enqueue_script( 'wpvs-customer-account', RVS_MEMBERS_BASE_URL .'js/wpvs-customer-account.js', array('jquery'), $rvs_current_version, true);
        wp_localize_script('wpvs-customer-account', 'wpvsacc', array(
            'ajax' => admin_url('admin-ajax.php'),
            'updatingaccount' => __('Updating', 'vimeo-sync-memberships')
        ));

        $wpvs_update_account_form_content .= '<p id="rvs-new-account-error" class="rvs-form-error rvs-error wpvs-text-align-center"></p>';
        $wpvs_update_account_form_content .= '<form id="rvs-account-details" method="POST" action="" class="wpvs-login-form">';
        if($rvs_usernames_allowed) {
            $wpvs_update_account_form_content .= '<div class="rvs-form-row">';
            $wpvs_update_account_form_content .= '<label>'.__('Username', 'vimeo-sync-memberships').' <small><em>'.__('Usernames cannot be changed', 'vimeo-sync-memberships').'</em></small></label>';
            $wpvs_update_account_form_content .= '<input id="new_user_name" name="new_user_name" type="text" readonly required value="'.$rvs_current_user->user_login.'" /></div>';
        }
        $wpvs_update_account_form_content .= '<div class="rvs-form-row">';
        $wpvs_update_account_form_content .= '<label>'.__('First Name', 'vimeo-sync-memberships').'</label>';
        $wpvs_update_account_form_content .= '<input id="new_user_first_name" name="new_user_first_name" type="text" value="'.$rvs_current_user->first_name.'" /></div>';
        $wpvs_update_account_form_content .= '<div class="rvs-form-row">';
        $wpvs_update_account_form_content .= '<label>'.__('Last Name', 'vimeo-sync-memberships').'</label>';
        $wpvs_update_account_form_content .= '<input id="new_user_last_name" name="new_user_last_name" type="text" value="'.$rvs_current_user->last_name.'" /></div>';

        $wpvs_update_account_form_content .= '<div class="rvs-form-row">';
        $wpvs_update_account_form_content .= '<label>'.__('Email', 'vimeo-sync-memberships').'</label>';
        $wpvs_update_account_form_content .= '<input id="new_user_email" name="new_user_email" type="email" required value="'.$rvs_current_user->user_email.'" /></div>';
        $wpvs_update_account_form_content .= '<div class="rvs-form-row">';
        $wpvs_update_account_form_content .= '<label>'.__('New Password', 'vimeo-sync-memberships').'</label>';
        $wpvs_update_account_form_content .= '<input id="new_user_password" name="new_user_password" type="password" autocomplete="off"/></div>';
        $wpvs_update_account_form_content .= '<div class="rvs-form-row">';
        $wpvs_update_account_form_content .= '<label>'.__('Confirm New Password', 'vimeo-sync-memberships').'</label>';
        $wpvs_update_account_form_content .= '<input id="confirm_user_password" name="confirm_user_password" type="password" autocomplete="off"/></div>';

        if( has_action('wpvs_add_fields_after_account_form') ) {
            ob_start();
            wpvs_add_fields_after_account_form_action($rvs_current_user->ID);
            $wpvs_update_account_form_content .= ob_get_contents();
            ob_end_clean();
        }

        $wpvs_update_account_form_content .= '<div class="rvs-form-row"><input id="wpvs_account_nonce" name="wpvs_account_nonce" type="hidden" required value="'.wp_create_nonce('wpvs-update-account').'"/></div>';

        $wpvs_update_account_form_content .= '<div class="rvs-form-row">';
        $wpvs_update_account_form_content .= '<input type="submit" id="rvs-create-new-account" class="rvs-button rvs-primary-button" value="'.__('Update Account', 'vimeo-sync-memberships').'" /></div></form>';

        return $wpvs_update_account_form_content;
    }
}
add_shortcode('wpvs_customer_details_form', 'wpvs_generate_customer_details_form');
}

if( ! function_exists('rvs_display_user_rentals') ) {
    function rvs_display_user_rentals( $atts ) {
        $wpvs_user_rentals_content = "";
        if(is_user_logged_in()) {
            $wpvs_atts = shortcode_atts( array(
                'show_customer_menu' => '0'
            ), $atts );
            global $rvs_current_user;
            $rvs_account_sub_menu = get_option('rvs_account_sub_menu', 1);
            if( $rvs_account_sub_menu && $wpvs_atts['show_customer_menu'] ) {
                $wpvs_user_rentals_content .= wpvs_generate_customer_menu("rentals");
            }
            rvs_check_rentals_purchases($rvs_current_user->ID);
            $user_rentals = rvs_get_user_rentals($rvs_current_user->ID);
            if(!empty($user_rentals)) {
                $wpvs_user_rentals_content .= '<div class="video-listings"';
                // Setting from original WP Videos plugin
                $wp_videos_gallery_styling = get_option('rvs_vimeo_styling');
                if( ! empty($wp_videos_gallery_styling) && isset($wp_videos_gallery_styling['background']) && ! empty($wp_videos_gallery_styling['background']) ) {
                    $vimeosync_background = $wp_videos_gallery_styling['background'];
                    $wpvs_user_rentals_content .= ' style="background: '.$vimeosync_background.';"';
                }
                $wpvs_user_rentals_content .= '>';
                foreach($user_rentals as $rental) {
                    $video_id = $rental['id'];
                    $video = $rental['video'];
                    $rental_expires = $rental['expires'];
                    $video_title = $video->post_title;
                    $video_link = get_permalink($video_id);
                    $wpvs_user_rentals_content .= '<div class="video-item border-box"><div class="video-thumbnail">';
                    if( has_post_thumbnail($video_id) ) {
                        $wpvs_user_rentals_content .= get_the_post_thumbnail($video_id, 'rvs-video-size');
                    } else {
                        $thumbnail_image = get_post_meta($video_id, 'rvs_thumbnail_image', true);
                        $wpvs_user_rentals_content .= '<img src="'.$thumbnail_image.'" alt="'.$video_title.'"/>';
                    }
                    $wpvs_user_rentals_content .= '</div>';
                    $wpvs_user_rentals_content .= '<a href="'.$video_link.'"><div class="video-item-details">';
                    $wpvs_user_rentals_content .= '<h4 class="video-item-title">'.$video_title.'</h4>';
                    $wpvs_user_rentals_content .= '</div></a></div>';
                }
                $wpvs_user_rentals_content .= '</div>';
            } else {
                global $wpvs_video_slug_settings;
                $video_url = '/videos';
                if( ! empty($wpvs_video_slug_settings) && isset($wpvs_video_slug_settings['slug']) ) {
                    $video_url = '/'.$wpvs_video_slug_settings['slug'];
                }
                $videos_link = home_url($video_url);
                $wpvs_user_rentals_content .= '<h4>'.__('You have not rented any videos', 'vimeo-sync-memberships').'</h4>';
                $wpvs_user_rentals_content .= '<a class="rvs-button rvs-primary-button" href="'.$videos_link.'">'.__('Browse videos', 'vimeo-sync-memberships').'</a>';
            }
        }
        return $wpvs_user_rentals_content;
    }
}
if(!function_exists('rvs_display_user_purchases')) {
    function rvs_display_user_purchases( $atts ) {
        $wpvs_user_purchases_content = "";
        if(is_user_logged_in()) {
            $wpvs_atts = shortcode_atts( array(
                'show_customer_menu' => '0'
            ), $atts );
            global $rvs_current_user;
            $rvs_account_sub_menu = get_option('rvs_account_sub_menu', 1);
            if( $rvs_account_sub_menu && $wpvs_atts['show_customer_menu'] ) {
                $wpvs_user_purchases_content .= wpvs_generate_customer_menu("purchases");
            }
            rvs_check_rentals_purchases($rvs_current_user->ID);
            $user_purchases = rvs_get_user_purchases($rvs_current_user->ID);
            if( ! empty($user_purchases) ) {
                $wpvs_user_purchases_content .= '<div class="video-listings"';
                // Setting from original WP Videos plugin
                $wp_videos_gallery_styling = get_option('rvs_vimeo_styling');
                if( ! empty($wp_videos_gallery_styling) && isset($wp_videos_gallery_styling['background']) && ! empty($wp_videos_gallery_styling['background']) ) {
                    $vimeosync_background = $wp_videos_gallery_styling['background'];
                    $wpvs_user_purchases_content .= ' style="background: '.$vimeosync_background.';"';
                }
                $wpvs_user_purchases_content .= '><h3>'.__('Videos', 'vimeo-sync-memberships').'</h3>';
                foreach($user_purchases as $purchase) {
                    $video_id = $purchase['id'];
                    $video = $purchase['video'];
                    $video_download_link = get_post_meta( $video_id, 'rvs_video_download_link', true );
                    $video_title = $video->post_title;
                    $video_link = get_permalink($video_id);
                    $wpvs_user_purchases_content .= '<div class="video-item border-box">';
                    $wpvs_user_purchases_content .= '<a href="'.$video_link.'" class="video-thumbnail">';
                    if(has_post_thumbnail($video_id)) {
                        $wpvs_user_purchases_content .= get_the_post_thumbnail($video_id, 'rvs-video-size');
                    } else {
                        $thumbnail_image = get_post_meta($video_id, 'rvs_thumbnail_image', true);
                        $wpvs_user_purchases_content .= '<img src="'.$thumbnail_image.'" alt="'.get_the_title($video_id).'"/>';
                    }
                    $wpvs_user_purchases_content .= '</a>';
                    if( ! empty($video_download_link) ) {
                        $wpvs_user_purchases_content .= '<a class="wpvs-video-download-link" href="'.$video_download_link.'" download><span class="dashicons dashicons-download"></span></a>';
                    }
                    $wpvs_user_purchases_content .= '<a href="'.$video_link.'"><h4 class="video-item-title">'.$video_title.'</h4></a>';
                     $wpvs_user_purchases_content .= '</div>';
                }
                 $wpvs_user_purchases_content .= '</div>';
            }
            $user_term_purchases = wpvs_get_user_term_purchases($rvs_current_user->ID);
            if( ! empty($user_term_purchases) ) {
                global $wpvs_genre_slug_settings;
                $wpvs_user_purchases_content .= '<div class="video-listings"';
                if( ! empty($vimeosync_background) ) {
                    $wpvs_user_purchases_content .= 'style="background: '.$vimeosync_background.';"';
                }
                $wpvs_user_purchases_content .= '>';
                $wpvs_user_purchases_content .= '<div class="wpvs-term-purchases"><h3>'.$wpvs_genre_slug_settings['name-plural'].'</h3>';
                foreach($user_term_purchases as $purchase) {
                    $term_id = intval($purchase['id']);
                    $wpvs_term = $purchase['term'];
                    $wpvs_term_link = get_term_link($term_id, 'rvs_video_category');
                    $wpvs_term_title = $wpvs_term->name;
                    if( ! empty($wpvs_term->parent) ) {
                        $wpvs_parent_term = get_term(intval($wpvs_term->parent), 'rvs_video_category' );
                        if( ! empty($wpvs_parent_term) && ! is_wp_error($wpvs_parent_term) ) {
                            $wpvs_term_title .= ' ('.$wpvs_parent_term->name.')';
                        }
                    }
                    $wpvs_user_purchases_content .= '<a href="'.$wpvs_term_link.'" class="wpvs-purchase-term-link">'.$wpvs_term_title.'<span class="dashicons dashicons-arrow-right-alt2"></span></a>';
                }
                $wpvs_user_purchases_content .= '</div></div>';
            }

            if( empty($user_purchases) && empty($user_term_purchases) ) {
                global $wpvs_video_slug_settings;
                $video_url = '/videos';
                if( ! empty($wpvs_video_slug_settings) && isset($wpvs_video_slug_settings['slug']) ) {
                    $video_url = '/'.$wpvs_video_slug_settings['slug'];
                }
                $videos_link = home_url($video_url);
                $wpvs_user_purchases_content .= '<h4>'.__('You have not purchased any videos', 'vimeo-sync-memberships').'</h4>';
                $wpvs_user_purchases_content .= '<a class="rvs-button rvs-primary-button" href="'.$videos_link.'">'.__('Browse videos', 'vimeo-sync-memberships').'</a>';
            }
        }
        return $wpvs_user_purchases_content;
    }
}
if( ! get_option('wpvs_theme_active') ) {
add_shortcode('rvs_user_rentals', 'rvs_display_user_rentals');
add_shortcode('rvs_user_purchases', 'rvs_display_user_purchases');
}

function wpvs_restricted_content_shortcode( $atts, $content ) {
    global $post;
    $restricted_content = "";
    $required_memberships = array();
    $wpvs_register_page = esc_attr( get_option('rvs_sign_up_page'));
    $wpvs_registration_link = get_permalink($wpvs_register_page);
    $wpvs_redirect_url = null;
    if( $post && isset($post->ID) ) {
        $wpvs_redirect_url = get_permalink($post->ID);
    }
    if( empty($wpvs_registration_link || is_wp_error($wpvs_registration_link)) ) {
        $wpvs_registration_link = wp_login_url();
    }
    $wpvs_shortcode_atts = shortcode_atts( array(
        'memberships'    => array(),
        'registered_users_free' => false,
        'no_access_text'   => __('This content is for members only', 'vimeo-sync-memberships'),
        'no_access_link'   => $wpvs_registration_link,
        'no_access_button_text'   => __('Subscribe', 'vimeo-sync-memberships'),
    ), $atts );

    if( isset($wpvs_shortcode_atts['memberships']) && ! empty($wpvs_shortcode_atts['memberships']) ) {
        $required_memberships = $wpvs_shortcode_atts['memberships'];
        $required_memberships = explode(",", $required_memberships);
    }

    if ( is_user_logged_in() ) {
        if( $wpvs_shortcode_atts['registered_users_free'] ) {
            $restricted_content .= $content;
        } else {
            global $rvs_current_user;
            $wpvs_customer = new WPVS_Customer($rvs_current_user);
            if( ! empty($required_memberships) ) {
                $user_has_access = $wpvs_customer->has_access($required_memberships, null);
                if( $user_has_access['has_access'] ) {
                    $restricted_content .= $content;
                } else {
                    $restricted_content .= '<div class="wpvs-restricted-content">';
                    $restricted_content .= '<div class="wpvs-restricted-content-text"><p>'.$wpvs_shortcode_atts['no_access_text'].'</p></div>';
                    // if customer has membership but has an overdue payment or other
                    if( isset($user_has_access['reason']) && $user_has_access['reason'] != 'nosubscription' && isset($user_has_access['message']) && ! empty($user_has_access['message']) ) {
                        $wpvs_shortcode_atts['no_access_text'] = $user_has_access['message'];
                        $wpvs_account_page = get_option('rvs_account_page');
                        $wpvs_account_link = get_permalink($wpvs_account_page);
                        if( ! empty($wpvs_account_link && ! is_wp_error($wpvs_account_link) ) ) {
                            $wpvs_shortcode_atts['no_access_link'] = $wpvs_account_link;
                            $wpvs_shortcode_atts['no_access_button_text'] = __('Manage Account', 'vimeo-sync-memberships');
                        }
                        $restricted_content .= '<a class="rvs-button rvs-primary-button" href="'.$wpvs_shortcode_atts['no_access_link'].'">'.$wpvs_shortcode_atts['no_access_button_text'].'</a>';
                        $restricted_content .= '</div>';
                    } else {
                        $restricted_content .= '</div>'; // end restricted content text
                        $restricted_content .= wpvs_generate_membership_options_html($required_memberships, $wpvs_redirect_url);
                    }
                }
            }
        }
    } else {
        $restricted_content .= '<div class="wpvs-restricted-content">';
        $restricted_content .= '<div class="wpvs-restricted-content-text"><p>'.$wpvs_shortcode_atts['no_access_text'].'</p></div>';
        $wpvs_membership_options_html = wpvs_generate_membership_options_html($required_memberships, $wpvs_redirect_url);
        if( empty($wpvs_membership_options_html) ) {
            $restricted_content .= '<a class="rvs-button rvs-primary-button" href="'.$wpvs_shortcode_atts['no_access_link'].'">'.$wpvs_shortcode_atts['no_access_button_text'].'</a>';
            $restricted_content .= '</div>';
        } else {
            $restricted_content .= '</div>';
            $restricted_content .= $wpvs_membership_options_html;
        }
    }
    return $restricted_content;
}

add_shortcode( 'wpvs-restricted-content', 'wpvs_restricted_content_shortcode' );

function wpvs_generate_membership_options_html($membership_ids = array(), $redirect = null) {
    $wpvs_membership_options_html = "";
    $wpvs_memberships_list = get_option('rvs_membership_list');
    if( ! empty($wpvs_memberships_list) ) {
        $wpvs_membership_options_html .= '<div class="rvs-area wpvs-membership-options-list">';
        $wpvs_payment_page = get_option('rvs_payment_page');
        $wpvs_payment_link = get_permalink($wpvs_payment_page);
        $ql = "?";
        if(strpos($wpvs_payment_link , '?page_id')) {
            $ql = "&";
        }
        foreach($wpvs_memberships_list as $wpvs_plan) {
            if( ! $wpvs_plan['hidden'] ) {
                $wpvs_plan_interval = $wpvs_plan['interval'];
                $wpvs_plan_interval_count = $wpvs_plan['interval_count'];
                $wpvs_plan_payment_link = $wpvs_payment_link.$ql.'id=' . $wpvs_plan['id'];
                if( ! empty($redirect) ) {
                    $wpvs_plan_payment_link .= '&redirect='.urlencode($redirect);
                }
                $subscribe_label = wpvs_currency_label($wpvs_plan['amount'], $wpvs_plan_interval, $wpvs_plan_interval_count, true);
                if( ! empty($membership_ids) ) {
                    if( in_array($wpvs_plan['id'], $membership_ids) ) {
                        $wpvs_membership_options_html .= '<div class="rvs-membership-item">';
                        $wpvs_membership_options_html .= '<h4>'.$wpvs_plan['name'].'</h4>';
                        $wpvs_membership_options_html .= '<p>'.stripslashes($wpvs_plan['description']).'</p>';
                        $wpvs_membership_options_html .= '<label class="wpvs-price">'.$subscribe_label.'</label>';
                        $wpvs_membership_options_html .= '<a class="rvs-button rvs-primary-button" href="'.$wpvs_plan_payment_link.'">' . __('Subscribe', 'vimeo-sync-memberships') . '</a>';
                        $wpvs_membership_options_html .= '</div>';
                    }
                } else {
                    $wpvs_membership_options_html .= '<div class="rvs-membership-item">';
                    $wpvs_membership_options_html .= '<h4>'.$wpvs_plan['name'].'</h4>';
                    $wpvs_membership_options_html .= '<p>'.stripslashes($wpvs_plan['description']).'</p>';
                    $wpvs_membership_options_html .= '<label class="wpvs-price">'.$subscribe_label.'</label>';
                    $wpvs_membership_options_html .= '<a class="rvs-button rvs-primary-button" href="'.$wpvs_plan_payment_link.'">' . __('Subscribe', 'vimeo-sync-memberships') . '</a>';
                    $wpvs_membership_options_html .= '</div>';
                }
            }
        }
        $wpvs_membership_options_html .= '</div>';
    }
    return $wpvs_membership_options_html;
}
