<?php

global $wp_query;
global $post;
global $wpvs_stripe_options;
global $rvs_paypal_settings;
global $rvs_live_mode;
global $rvs_currency;
global $rvs_paypal_enabled;
global $wpvs_stripe_gateway_enabled;
global $wpvs_google_apple_enabled;
global $wpvs_stripe_checkout_enabled;
global $wpvs_coingate_enabled;
global $wpvs_coinbase_enabled;
global $rvs_current_user;
global $wpvs_stripe_customer;
global $wpvs_add_stripe_forms;
global $wpvs_add_paypal_forms;
global $wpvs_requires_save_card;
$wpvs_requires_save_card = false;
$wpvs_add_stripe_forms = false;
$wpvs_add_paypal_forms = false;
$wpvs_customer = new WPVS_Customer($rvs_current_user);
$wpvs_checkout = new WPVS_Checkout($wpvs_customer);
$wpvs_checkout->set_checkout_type('purchase');
$wpvs_checkout->set_transaction_type('purchase');
if( is_tax( 'rvs_video_category' ) ) {
    $wpvs_checkout->set_purchase_type('termpurchase');
    $wpvs_checkout->set_product($wp_query->get_queried_object_id());
    $wpvs_current_term = get_term($wp_query->get_queried_object_id(), 'rvs_video_category' );
} else {
    $wpvs_checkout->set_purchase_type('purchase');
    $wpvs_checkout->set_product($post->ID);
}
echo $wpvs_checkout->checkout_content();
