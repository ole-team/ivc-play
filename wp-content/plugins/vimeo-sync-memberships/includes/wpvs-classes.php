<?php

/**
* WP Video Memberships PayPal Membership Plan
*/

class WPVS_Membership_Plan {

    public $plan_id;
    public $name;
    public $amount;
    public $description;
    public $short_description;
    public $interval;
    public $interval_count;
    public $trial_frequency;
    public $trial_frequency_interval;
    public $hidden;
    public $has_role;

    protected $plan_key;

    public function __construct($plan_id) {
        $this->plan_id = $plan_id;
        if( $this->membership_plan_exists() && ! empty($this->get_plan()) ) {
            $membership_plan = $this->get_plan();
            $this->name = $membership_plan['name'];
            $this->amount = $membership_plan['amount'];
            $this->description = $membership_plan['description'];
            if( isset($membership_plan['short_description']) ) {
                $this->short_description = $membership_plan['short_description'];
            } else {
                $this->short_description = null;
            }
            $this->interval = $membership_plan['interval'];
            $this->interval_count = $membership_plan['interval_count'];
            if( isset($membership_plan['trial_frequency']) ) {
                $this->trial_frequency = $membership_plan['trial_frequency'];
                $this->trial_frequency_interval = $membership_plan['trial'];
            } else {
                $this->trial_frequency = "none";
                $this->trial_frequency_interval = null;
            }
            $this->hidden = $membership_plan['hidden'];
            if( isset($membership_plan['has_role']) && ! empty($membership_plan['has_role']) ) {
                $this->has_role = true;
            } else {
                $this->has_role = false;
            }

            $this->plan_key = $this->get_key();
        }
    }

    public function new_init() {
        if( $this->membership_plan_exists() && ! empty($this->get_plan()) ) {
            $membership_plan = $this->get_plan();
            $this->name = $membership_plan['name'];
            $this->amount = $membership_plan['amount'];
            $this->description = $membership_plan['description'];
            if( isset($membership_plan['short_description']) ) {
                $this->short_description = $membership_plan['short_description'];
            } else {
                $this->short_description = null;
            }
            $this->interval = $membership_plan['interval'];
            $this->interval_count = $membership_plan['interval_count'];
            if( isset($membership_plan['trial_frequency']) ) {
                $this->trial_frequency = $membership_plan['trial_frequency'];
                $this->trial_frequency_interval = $membership_plan['trial'];
            } else {
                $this->trial_frequency = "none";
                $this->trial_frequency_interval = null;
            }
            $this->hidden = $membership_plan['hidden'];
            if( isset($membership_plan['has_role']) && ! empty($membership_plan['has_role']) ) {
                $this->has_role = true;
            } else {
                $this->has_role = false;
            }
            $this->plan_key = $this->get_key();
        }
    }

    // CHECK IF A PLAN WITH THIS ID ALREADY EXISTS

    public function membership_plan_exists() {
        global $wpvs_membership_plans_list;
        $membership_plan_exists = false;
        if( ! empty($wpvs_membership_plans_list) ) {
            foreach($wpvs_membership_plans_list as $plan) {
                if($plan['id'] == $this->plan_id) {
                    $membership_plan_exists = true;
                    break;
                }
            }
        }
        return $membership_plan_exists;
    }

    // GET THE PLAN ARRAY

    public function get_plan() {
        global $wpvs_membership_plans_list;
        $membership_plan = array();
        if( ! empty($wpvs_membership_plans_list) ) {
            foreach($wpvs_membership_plans_list as $plan) {
                if($plan['id'] == $this->plan_id) {
                    $membership_plan = $plan;
                    break;
                }
            }
        }
        return $membership_plan;
    }

    // GET THE PLAN KEY (PRIVATE)

    protected function get_key() {
        global $wpvs_membership_plans_list;
        $plan_key = null;
        if( ! empty($wpvs_membership_plans_list) ) {
            foreach($wpvs_membership_plans_list as $key => $plan) {
                if($plan['id'] == $this->plan_id) {
                    $plan_key = $key;
                    break;
                }
            }
        }
        return $plan_key;
    }

    // GET THE PLAN ARRAY WITH KEY

    public function get_plan_with_key() {
        global $wpvs_membership_plans_list;
        $membership_plan = array();
        $plan_key = null;
        if( ! empty($wpvs_membership_plans_list) ) {
            foreach($wpvs_membership_plans_list as $key => $plan) {
                if($plan['id'] == $this->plan_id) {
                    $membership_plan = $plan;
                    $plan_key = $key;
                    break;
                }
            }
        }
        return array ('plan' => $membership_plan, 'key' => $plan_key);
    }


    // UPDATE MEMBERSHIP PLAN ARRAY

    public function update_membership($new_membership_details) {
        global $wpvs_membership_plans_list;
        if( ! empty($wpvs_membership_plans_list) ) {
            $wpvs_membership_plans_list[$this->plan_key] = $new_membership_details;
            return update_option('rvs_membership_list', $wpvs_membership_plans_list);
        }
    }

    // CHECK FOR TRIAL PERIOD

    public function has_trial_period() {
        $has_trial = false;
        if( ! empty($this->trial_frequency_interval) && $this->trial_frequency != "none" ) {
            $has_trial = true;
        }
        return $has_trial;
    }

    // CHECK IF COUPON PLAN ALREADY EXISTS

    public function get_paypal_coupon_plan($coupon_id) {
        global $wpvs_membership_plans_list;
        $paypal_coupon_plan = null;
        if( ! empty($wpvs_membership_plans_list) && isset($wpvs_membership_plans_list[$this->plan_key]['paypal_coupon_plans']) && ! empty($wpvs_membership_plans_list[$this->plan_key]['paypal_coupon_plans']) ) {
            $paypal_coupon_plans = $wpvs_membership_plans_list[$this->plan_key]['paypal_coupon_plans'];
            foreach($paypal_coupon_plans as $coupon_plan) {
                if( $coupon_plan['id'] == $coupon_id ) {
                    $paypal_coupon_plan = $coupon_plan;
                    break;
                }
            }

        }
        return $paypal_coupon_plan;
    }

    // ADD PAYPAL COUPON PLAN TO MEMBERSHIP PLAN

    public function create_paypal_coupon_plan($coupon, $primary_plan_has_trial) {
        $new_paypal_coupon_plan = null;
        $wpvs_error_message     = "";
        $plan_type              = 'infinite';
        $plan_id                = $this->plan_id;
        $coupon_plan_id         = $plan_id.'-'.$coupon->coupon_id;
        $membership_plan        = $this->get_plan();

        if( isset($membership_plan['paypal_coupon_plans']) ) {
            unset($membership_plan['paypal_coupon_plans']);
        }

        if( isset($membership_plan['stripe']) ) {
            unset($membership_plan['stripe']);
        }

        if( isset($membership_plan['paypal']) ) {
            unset($membership_plan['paypal']);
        }

        if( isset($membership_plan['paypal_test']) ) {
            unset($membership_plan['paypal_test']);
        }

        $new_paypal_coupon_plan           = $membership_plan;
        $new_paypal_coupon_plan['id']     = $coupon->coupon_id;
        if( isset($new_paypal_coupon_plan['short_description']) && ! empty($new_paypal_coupon_plan['short_description']) ) {
            $short_description = $new_paypal_coupon_plan['short_description'];
        } else {
            $short_description = null;
        }

        $trial_frequency = $new_paypal_coupon_plan['trial_frequency'];
        $plan_trial = $new_paypal_coupon_plan['trial'];
        $trial_amount = 0;

        $new_amount = wpvs_calculate_coupon_amount($new_paypal_coupon_plan['amount'], $coupon->coupon_id);

        // discount on first payment only
        if( $coupon->duration == "once" ) {
            $trial_frequency = $this->interval;
            $new_paypal_coupon_plan['trial_frequency'] = $this->interval;
            $new_paypal_coupon_plan['trial'] = 1;
            $plan_trial = 1;
            $trial_amount = $new_amount;
            $new_paypal_coupon_plan['trial_amount'] = $new_amount;
        }

        // discount on repeating payments
        if( $coupon->duration == "repeating" ) {
            $trial_frequency = 'month';
            $new_paypal_coupon_plan['trial_frequency'] = 'month';
            if( $primary_plan_has_trial ) {
                $new_paypal_coupon_plan['trial'] = $coupon->month_duration;
                $plan_trial = $coupon->month_duration;
            } else {
                $new_paypal_coupon_plan['trial'] = intval($coupon->month_duration) - 1;
                $plan_trial = intval($coupon->month_duration) - 1;
            }
            $trial_amount = $new_amount;
            $new_paypal_coupon_plan['trial_amount'] = $new_amount;
        }

        // new plan with discount forever
        if( $coupon->duration == "forever" ) {
            $new_paypal_coupon_plan['amount'] = $new_amount;
            $new_paypal_coupon_plan['trial_frequency'] = "none";
            $trial_frequency = "none";
            $new_paypal_coupon_plan['trial'] = 0;
            $plan_trial = 0;
        }

        $paypal_plan_manager = new WPVS_PayPal_Plan_Creator(
            $coupon_plan_id,
            $new_paypal_coupon_plan['name'],
            $new_paypal_coupon_plan['amount'],
            $new_paypal_coupon_plan['description'],
            $short_description,
            $new_paypal_coupon_plan['interval'],
            $new_paypal_coupon_plan['interval_count'],
            "0",
            $trial_frequency,
            $plan_trial,
            $trial_amount,
            $new_paypal_coupon_plan,
            $plan_type,
            $plan_id
        );

        $coupon_plan_created = $paypal_plan_manager->create_paypal_billing_plan();

        if( ! empty($coupon_plan_created) && isset($coupon_plan_created['created']) && $coupon_plan_created['created'] ) {
            $new_coupon_plan = $coupon_plan_created['updated_plan'];
            $new_paypal_coupon_plan['paypal'] = $new_coupon_plan['paypal'];
            $new_paypal_coupon_plan['paypal_test'] = $new_coupon_plan['paypal_test'];
            $this->add_paypal_coupon_plan($new_paypal_coupon_plan);
        }
        return $new_paypal_coupon_plan;
    }

    public function add_paypal_coupon_plan($coupon_plan) {
        global $wpvs_membership_plans_list;
        if( empty( $this->get_paypal_coupon_plan( $coupon_plan['id']) ) && ! empty($wpvs_membership_plans_list) ) {
            $wpvs_membership_plans_list[$this->plan_key]['paypal_coupon_plans'][] = $coupon_plan;
            update_option('rvs_membership_list', $wpvs_membership_plans_list);
        }
    }
}

/**
* WP Video Memberships Coupon Code
*/

class WPVS_Coupon_Code {

    public $coupon_id;
    public $name;
    public $type;
    public $amount;
    public $duration;
    public $month_duration;
    public $max_uses;
    public $max_uses_customer;

    protected $coupon_key;

    public function __construct($coupon_id) {
        $this->coupon_id = $coupon_id;
        if( $this->coupon_exists() && ! empty($this->get_coupon()) ) {
            $coupon = $this->get_coupon();
            $this->name = $coupon['name'];
            $this->type = $coupon['type'];
            $this->amount = $coupon['amount'];
            $this->duration = $coupon['duration'];
            $this->month_duration = $coupon['month_duration'];
            if( isset( $coupon['max_uses']) ) {
                $this->max_uses = $coupon['max_uses'];
            } else {
                $this->max_uses = null;
            }
            if( isset( $coupon['max_uses_customer']) ) {
                $this->max_uses_customer = $coupon['max_uses_customer'];
            } else {
                $this->max_uses_customer = null;
            }
            $this->coupon_key = $this->get_key();
        }
    }

    // CHECK IF A PLAN WITH THIS ID ALREADY EXISTS

    public function coupon_exists() {
        $wpvs_coupon_codes = get_option('rvs_coupon_codes', array());
        $coupon_exists = false;
        if( ! empty($wpvs_coupon_codes) ) {
            foreach($wpvs_coupon_codes as $coupon) {
                if($coupon['id'] == $this->coupon_id) {
                    $coupon_exists = true;
                    break;
                }
            }
        }
        return $coupon_exists;
    }

    // GET THE COUPON ARRAY

    public function get_coupon() {
        $wpvs_coupon_codes = get_option('rvs_coupon_codes', array());
        $found_coupon = array();
        if( ! empty($wpvs_coupon_codes) ) {
            foreach($wpvs_coupon_codes as $coupon) {
                if($coupon['id'] == $this->coupon_id) {
                    $found_coupon = $coupon;
                    break;
                }
            }
        }
        return $found_coupon;
    }

    // GET THE COUPON KEY (PRIVATE)

    protected function get_key() {
        $wpvs_coupon_codes = get_option('rvs_coupon_codes', array());
        $coupon_key = array();
        if( ! empty($wpvs_coupon_codes) ) {
            foreach($wpvs_coupon_codes as $key => $coupon) {
                if($coupon['id'] == $this->coupon_id) {
                    $coupon_key = $key;
                    break;
                }
            }
        }
        return $coupon_key;
    }

}

/**
* WP Video Memberships Payment Manager
*/

class WPVS_Payment_Manager {

    public $payment_time;
    public $gateway;
    public $user_id;
    public $customer_id;
    public $amount;
    public $payment_id;
    public $payment_type;
    public $coupon_code;
    public $product_id;
    public $refunded;
    public $is_test_payment;

    protected $db_table;

    public function __construct($payment_id, $is_test_payment) {
        $this->payment_id = $payment_id;
        $this->is_test_payment = $is_test_payment;
        $this->set_db_table();
        if( $this->payment_exists() ) {
            $payment_data = $this->get_payment();
            $this->payment_time = $payment_data->time;
            $this->gateway = $payment_data->gateway;
            $this->user_id = $payment_data->userid;
            $this->customer_id = $payment_data->customerid;
            $this->amount = $payment_data->amount;
            $this->payment_type = $payment_data->type;
            $this->product_id = $payment_data->productid;
            $this->refunded = $payment_data->refunded;
            $this->coupon_code = $payment_data->coupon_code;
        }
    }

    // SETS THE DATABASE TABLE TO USE

    private function set_db_table() {
        global $wpdb;
        if( $this->is_test_payment ) {
            $this->db_table = $wpdb->prefix . 'rvs_test_payments';
        } else {
            $this->db_table = $wpdb->prefix . 'rvs_payments';
        }
    }

    // CHECK IF A PAYMENT EXISTS

    public function payment_exists() {
        global $wpdb;
        $payment_exists = false;
        $payment = $wpdb->get_results("SELECT * FROM $this->db_table WHERE paymentid = '$this->payment_id'");
        if( ! empty($payment) ) {
            $payment_exists = true;
        }
        return $payment_exists;
    }

    private function get_payment() {
        global $wpdb;
        $payments = $wpdb->get_results("SELECT * FROM $this->db_table WHERE paymentid = '$this->payment_id'");
        return $payments[0];
    }

    public function add_new_payment($time, $gateway, $user_id, $customer_id, $amount, $payment_type, $product_id, $refunded, $coupon_code) {
        global $wpdb;

        if( ! $this->payment_exists() ) {
            $check = $wpdb->insert(
                $this->db_table,
                array(
                    'time' => $time,
                    'gateway' => $gateway,
                    'userid' => $user_id,
                    'customerid'  => $customer_id,
                    'amount' => $amount,
                    'paymentid' => $this->payment_id,
                    'coupon_code' => $coupon_code,
                    'type' => $payment_type,
                    'productid' => $product_id,
                    'refunded' => $refunded
                )
            );
        }
    }
}

/**
* WP Video Memberships Members Manager
*/

class WPVS_Members_Manager {

    public $test_mode;
    protected $db_table;

    public function __construct() {
        global $rvs_live_mode;
        if( $rvs_live_mode == "on" ) {
            $this->test_mode = false;
        } else {
            $this->test_mode = true;
        }
        $this->set_db_table();
    }

    // SETS THE DATABASE TABLE TO USE

    private function set_db_table() {
        global $wpdb;
        if( $this->test_mode ) {
            $this->db_table = $wpdb->prefix . 'wpvs_test_members';
        } else {
            $this->db_table = $wpdb->prefix . 'wpvs_members';
        }
    }

    public function get_all_members() {
        global $wpdb;
        $wpvs_members = $wpdb->get_results("SELECT * FROM $this->db_table");
        return $wpvs_members;
    }

    public function get_all_members_with_membership($plan_id) {
        global $wpdb;
        $live_members_with_membership = array();
        $test_members_with_membership = array();
        $wpvs_members = $this->get_all_members();
        if( ! empty($wpvs_members) ) {
            foreach($wpvs_members as $member) {
                $user_id = $member->user_id;
                $user_memberships = get_user_meta($user_id, 'rvs_user_memberships', true);
                if( ! empty($user_memberships) ) {
                    foreach($user_memberships as $membership) {
                        if( $membership['plan'] == $plan_id ) {
                            $live_members_with_membership[] = $user_id;
                            break;
                        }
                    }
                }
                $test_user_memberships = get_user_meta($user_id, 'rvs_user_memberships_test', true);
                if( ! empty($test_user_memberships) ) {
                    foreach($test_user_memberships as $test_membership) {
                        if( $test_membership['plan'] == $plan_id ) {
                            $test_members_with_membership[] = $user_id;
                            break;
                        }
                    }
                }
            }
        }
        return array( 'live_members' => $live_members_with_membership, 'test_members' => $test_members_with_membership);
    }
}


/**
* WP Video Memberships Checkout
*/

class WPVS_Checkout {

    private $customer;
    public $checkout_type;
    public $purchase_type;
    public $transaction_type;
    public $subtotal;
    public $total;
    public $product_price;
    public $coupon_code;
    public $applied_taxes;
    public $membership;
    public $membership_plan;
    public $product_name;
    public $currency;
    public $currency_label;
    public $formatted_subtotal;
    public $formatted_total;
    public $formatted_product_price;
    public $require_billing_info;
    public $redirect_url = null;
    public $cancel_redirect = null;
    public $add_trial_period = false;
    public $account_link;
    public $rental_price = null;
    public $purchase_price = null;

    public function __construct($wpvs_customer) {
        global $rvs_currency;
        global $wpvs_currency_label;
        if( empty($wpvs_customer) ) {
            return;
        }
        $this->customer = $wpvs_customer;
        $this->currency = $rvs_currency;
        $this->currency_label = $wpvs_currency_label;
        $this->require_billing_info = get_option('wpvs_require_billing_information', 0);
        $wpvs_account_page = get_option('rvs_account_page');
        $this->account_link = get_permalink($wpvs_account_page);
        if(isset($_GET['redirect']) && ! empty($_GET['redirect']) ) {
            $this->redirect_url = urldecode($_GET['redirect']);
        } else {
            $this->redirect_url = $this->account_link;
        }
        $this->cancel_redirect = get_permalink();

    }

    /*
    * Sets the purchase type to 'subscription' or 'payment'
    */
    public function set_checkout_type($type) {
        $this->checkout_type = $type;
    }
    public function set_transaction_type($type) {
        $this->transaction_type = $type;
    }
    public function set_purchase_type($type) {
        $this->purchase_type = $type;
    }
    public function set_coupon_code($coupon_code) {
        $this->coupon_code = $coupon_code;
    }

    protected function set_checkout_amounts() {
        $applied_taxes = array();
        if( ! empty($this->coupon_code) && ! empty($this->subtotal) ) {
            $this->subtotal = wpvs_calculate_coupon_amount($this->subtotal, $this->coupon_code);
        }
        $this->total = $this->subtotal;

        if( ! empty($this->subtotal) && $this->require_billing_info ) {
            $wpvs_tax_rate = new WPVS_Tax_Rate();
            if( ! empty($this->customer->billing_state) && ! empty($this->customer->billing_country) ) {
                $jurisdiction_rates = $wpvs_tax_rate->get_tax_rates_by_jurisdiction($this->customer->billing_state, $this->customer->billing_country);
                if( ! empty($jurisdiction_rates) ) {
                    foreach($jurisdiction_rates as $tax_rate) {
                        $get_tax_rate_amount = $wpvs_tax_rate->calculate_tax_rate_amount($this->subtotal, $tax_rate);
                        if( ! empty($get_tax_rate_amount) ) {
                            $applied_taxes[] = (object) array(
                                'tax_name' => $tax_rate->display_name,
                                'amount'   => $get_tax_rate_amount->amount,
                                'decimal_amount'   => $get_tax_rate_amount->decimal_amount,
                                'inclusive' => $tax_rate->inclusive
                            );
                        }
                    }
                }
            }
            if( ! empty($applied_taxes) ) {
                foreach($applied_taxes as $apply_tax) {
                    if( $apply_tax->inclusive ) {
                        $this->subtotal -= $apply_tax->amount;
                    } else {
                        $this->total += $apply_tax->amount;
                    }
                }
                $this->applied_taxes = $applied_taxes;
            }
        } else {
            $this->applied_taxes = array();
        }

        if( $this->currency == "JPY" ) {
            $this->formatted_subtotal = number_format($this->subtotal/100,0);
            $this->formatted_total = number_format($this->total/100,0);
            $this->formatted_product_price =  number_format($this->product_price/100,0);
        } else {
            $this->formatted_subtotal = number_format($this->subtotal/100,2);
            $this->formatted_total = number_format($this->total/100,2);
            $this->formatted_product_price =  number_format($this->product_price/100,2);
        }
    }

    protected function apply_trial() {
        $this->subtotal = 0;
        $this->add_trial_period = true;
        $this->set_checkout_amounts();
    }

    /*
    * Sets the membership information
    */
    public function set_membership($membership_id) {
        $this->membership = new WPVS_Membership_Plan($membership_id);
        $this->subtotal = $this->membership->amount;
        $this->product_price = $this->membership->amount;
        $this->product_name = $this->membership->name;
        $this->product_id = $this->membership->plan_id;
        $this->membership_plan = $this->membership->get_plan();
        $this->set_checkout_amounts();
    }

    public function set_product($product_id) {
        $this->product_id = intval($product_id);
        if( $this->purchase_type == 'termpurchase' ) {
            $wpvs_current_term = get_term($this->product_id, 'rvs_video_category' );
            $this->product_price = get_term_meta($this->product_id, 'wpvs_category_purchase_price', true);
            $this->purchase_price = $this->product_price;
            $this->redirect_url = get_term_link($this->product_id, 'rvs_video_category');
            $this->product_name = $wpvs_current_term->name;
            if( ! empty($wpvs_current_term->parent) ) {
                $wpvs_parent_term = get_term(intval($wpvs_current_term->parent), 'rvs_video_category' );
                if( ! empty($wpvs_parent_term) && ! is_wp_error($wpvs_parent_term) ) {
                    $this->product_name .= ' ('.$wpvs_parent_term->name.')';
                }
            }
        } else {
            $wpvs_product_buy_price = get_post_meta($this->product_id, '_rvs_onetime_price', true );
            $wpvs_product_rental_price = get_post_meta($this->product_id, 'rvs_rental_price', true );
            if( ! empty($wpvs_product_buy_price) ) {
                $this->purchase_price = $wpvs_product_buy_price;
            }
            if( ! empty($wpvs_product_rental_price) ) {
                $this->rental_price = $wpvs_product_rental_price;
            }
            if( $this->purchase_type == 'purchase' ) {
                $this->product_price = $this->purchase_price;
            }
            if( $this->purchase_type == 'rental' ) {
                $this->product_price = $this->rental_price;
            }
            $this->product_name = get_the_title($this->product_id);
        }
        $this->subtotal = $this->product_price;
        $this->set_checkout_amounts();
    }

    public function checkout_content() {
        $wpvs_checkout_form_content = "";
        $wpvs_payment_errors = $this->check_errors();
        if( ! empty($wpvs_payment_errors) ) {
            $wpvs_checkout_form_content .= $wpvs_payment_errors;
        }

        if( $this->checkout_type == 'subscription' ) {
            if($this->membership->has_trial_period() && ! wpvs_user_has_completed_trial($this->customer->user_id, $this->membership->plan_id) ) {
                $this->apply_trial();
            }
            $wpvs_checkout_form_content .= '<div id="wpvs-checkout-wrapper" class="rvs-area">';
            $wpvs_checkout_form_content .= '<h4>'.__('Checkout Details', 'vimeo-sync-memberships').'</h4>';
            $wpvs_checkout_form_content .= '<div class="wpvs-checkout-section rvs-border-box"><table id="rvs-checkout-table" class="rvs_memberships"><tbody><tr>';
            $wpvs_checkout_form_content .= '<th>'.__('Membership', 'vimeo-sync-memberships').'</th>';
            $wpvs_checkout_form_content .= '<th>'.__('Description', 'vimeo-sync-memberships').'</th>';
            $wpvs_checkout_form_content .= '<th>'.__('Price', 'vimeo-sync-memberships').'</th>';
            $wpvs_checkout_form_content .= '</tr><tr>';
            $wpvs_checkout_form_content .= '<td>'.$this->membership->name.'</td>';
            $wpvs_checkout_form_content .= '<td>'.$this->membership->description.'</td>';
            $wpvs_checkout_form_content .= '<td>'.wpvs_currency_label($this->product_price, $this->membership->interval, $this->membership->interval_count, true).'</td>';
            $wpvs_checkout_form_content .= '</tr>';
            if($this->add_trial_period) {
                $wpvs_checkout_form_content .= '<tr><td>'.__('Trial Period', 'vimeo-sync-memberships').'</td>';
                $wpvs_checkout_form_content .= '<td>'.$this->membership->trial_frequency_interval. ' ' . wpvs_translate_intervals($this->membership->trial_frequency_interval, $this->membership->trial_frequency).'</td>';
                $wpvs_checkout_form_content .= '<td>'.$this->currency_label.$this->formatted_subtotal . ' ' . __('for', 'vimeo-sync-memberships') . ' ' . $this->membership->trial_frequency_interval . ' ' . wpvs_translate_intervals($this->membership->trial_frequency_interval, $this->membership->trial_frequency).'</td></tr>';
            }
            $wpvs_checkout_form_content .= '</tbody></table></div>';
        }

        if( $this->checkout_type == 'purchase' ) {
            $wpvs_product_description = __('Access to', 'vimeo-sync-memberships') . ' ' . $this->product_name . ' ' . __('on', 'vimeo-sync-memberships') . ' ' . get_bloginfo('name');

            $wpvs_checkout_form_content .= '<div id="vs-single-payment-box">';
            $wpvs_checkout_form_content .= '<h3 id="rvs-purchase-title">'.__('Purchase', 'vimeo-sync-memberships').'</h3><div class="wpvs-checkout-section rvs-border-box"><table id="rvs-checkout-table" class="rvs_memberships"><tbody>';
            $wpvs_checkout_form_content .= '<tr><td>'.$this->product_name.'</td><td></td><td id="wpvs-purchase-details">'.$wpvs_product_description.'</td></tr>';
            $wpvs_checkout_form_content .= '<tr><td>'.__('Price', 'vimeo-sync-memberships').'</td><td></td><td>'.$this->currency_label.'<span class="wpvs-update-product-price">'. $this->formatted_product_price .'</span></td></tr></tbody></table></div>';
        }

        $wpvs_checkout_form_content .= '<div id="wpvs-coupon-code-field"><h4>'.__('Coupon Code', 'vimeo-sync-memberships').'</h4>';
        $wpvs_checkout_form_content .= '<div class="wpvs-checkout-section rvs-border-box">';
        $wpvs_checkout_form_content .= '<div id="rvs-enter-coupon">';
        $wpvs_checkout_form_content .= '<input type="text" name="rvs_coupon_code" id="rvs_coupon_code" placeholder="'.__('Enter Code', 'vimeo-sync-memberships').'"/>';
        $wpvs_checkout_form_content .= '<label id="rvs-apply-coupon" class="rvs-button rvs-primary-button">'.__('Apply Coupon', 'vimeo-sync-memberships').'</label>';
        $wpvs_checkout_form_content .= '</div></div></div>';

        if( $this->require_billing_info ) {
            $wpvs_checkout_form_content .= $this->generate_billing_fields(__('Proceed To Payment', 'vimeo-sync-memberships'), false);
        }
        $wpvs_checkout_form_content .= '<div class="wpvs-proceed-button border-box"><button id="wpvs-proceed-button" class="button rvs-button rvs-primary-button">'.__('Proceed To Payment', 'vimeo-sync-memberships').'</button></div>';


        $wpvs_checkout_form_content .= '<div id="wpvs-payment-total-section" class="wpvs-payment-section rvs-border-box wpvs-hide-payment-section">';
        $wpvs_checkout_form_content .= $this->generate_totals_table();
        $wpvs_checkout_form_content .= $this->generate_payment_options();
        $wpvs_checkout_form_content .= '</div>'; // end payments and total section
        $wpvs_checkout_form_content .= '</div>'; // end checkout wrapper

        $this->set_checkout_json_data();
        return $wpvs_checkout_form_content;
    }

    public function generate_totals_table() {
        $wpvs_checkout_totals_table = '<div class="wpvs-checkout-section rvs-border-box">';
        $wpvs_checkout_totals_table .= '<table id="wpvs-checkout-total-amounts" class="rvs_total_due"><tbody><tr>';
        $wpvs_checkout_totals_table .= '<td>'.__('Total', 'vimeo-sync-memberships').':</td>';
        $wpvs_checkout_totals_table .= '<td>'.$this->currency_label.' <span class="rvs-update-total">'. $this->formatted_subtotal .'</span></td>';
        $wpvs_checkout_totals_table .= '</tr></tbody></table></div>';
        return $wpvs_checkout_totals_table;
    }

    public function generate_payment_options() {
        global $wpvs_stripe_gateway_enabled;
        global $rvs_paypal_enabled;
        global $wpvs_stripe_options;
        global $wpvs_google_apple_enabled;
        global $wpvs_stripe_checkout_enabled;
        global $wpvs_coingate_enabled;
        global $wpvs_coinbase_enabled;
        global $wpvs_add_stripe_forms;
        global $wpvs_add_paypal_forms;
        $wpvs_add_stripe_forms = false;
        $wpvs_add_paypal_forms = false;
        global $wpvs_requires_save_card;
        if( $this->checkout_type == 'subscription' ) {
            $wpvs_requires_save_card = true;
        } else {
            $wpvs_requires_save_card = false;
        }

        if($wpvs_stripe_gateway_enabled) {
            if( $this->checkout_type == 'purchase' ) {
                $wpvs_add_stripe_forms = true;
            } else {
                if( isset($this->membership_plan['stripe']) || isset($this->membership_plan['stripe_test']) ) {
                    $wpvs_add_stripe_forms = true;
                }
            }
        }
        if( $rvs_paypal_enabled ) {
            if( $this->checkout_type == 'purchase' ) {
                $wpvs_add_paypal_forms = true;
            } else {
                if( isset($this->membership_plan['paypal']) || isset($this->membership_plan['paypal_test']) ) {
                    $wpvs_add_paypal_forms = true;
                }
            }
        }

        $wpvs_payment_options_content = '<h4>'.__('Payment Method', 'vimeo-sync-memberships').'</h4>';
        $wpvs_payment_options_content .= '<div id="wpvs-checkout-options" class="rvs-border-box">';
        ob_start();
        include(RVS_MEMBERS_BASE_DIR . '/template/payment-types.php');
        $wpvs_payment_options_content .= ob_get_contents();
        ob_end_clean();
        $wpvs_payment_options_content .= '</div>';
        if($wpvs_add_stripe_forms) {
            $stripe_payments_manager = new WPVS_Stripe_Payments_Manager($this->customer->user_id);
            $wpvs_stripe_customer = $stripe_payments_manager->get_stripe_customer_details();
            $customer_payment_methods = $stripe_payments_manager->get_payment_methods();
            $wpvs_payment_options_content .= '<div id="wpvs-stripe-box" class="wpvs-payment-box active">';

            if( ($wpvs_stripe_customer != null && empty($customer_payment_methods) ) || $wpvs_stripe_customer == null) {
                // CREATE STRIPE CHECKOUT OPTION
                if( $wpvs_stripe_checkout_enabled ) {
                    $stripe_checkout_button_text = __('Proceed To Checkout', 'vimeo-sync-memberships');
                    if( isset($wpvs_stripe_options['stripe_checkout_button_text']) && ! empty($wpvs_stripe_options['stripe_checkout_button_text']) ) {
                        $stripe_checkout_button_text = $wpvs_stripe_options['stripe_checkout_button_text'];
                    }

                    $wpvs_payment_options_content .= '<div class="wpvs-checkout-padding wpvs-text-align-center"><div class="wpvs-stripe-checkout-box">';
                    // display loading content
                    $wpvs_payment_options_content .= '<div id="wpvs-stripe-checkout-loading"><div class="wpvs-updating-box"><span class="wpvs-loading-text">'.__('Creating Stripe Checkout', 'vimeo-sync-memberships').'...</span><span class="loadingCircle"></span><span class="loadingCircle"></span><span class="loadingCircle"></span><span class="loadingCircle"></span></div></div>';

                    // display checkout button after session id
                    $wpvs_payment_options_content .= '<div id="wpvs-stripe-checkout-loaded"><label id="wpvs-stripe-checkout-link" class="rvs-button rvs-primary-button rvs-pay-button" data-session="">'.$stripe_checkout_button_text.' <span class="dashicons dashicons-migrate"></span></label>';
                    $wpvs_payment_options_content .= '<div class="wpvs-stripe-checkout-page-notice"><small><span class="dashicons dashicons-lock"></span> '.__('You will be redirected to a secure payment page powered by', 'vimeo-sync-memberships').' <a href="https://stripe.com/en-ca/payments/checkout" target="_blank">Stripe</a>.</small></div></div>';
                    $wpvs_payment_options_content .= '</div></div>'; // end checkout box
                } else {
                    # CREATE STRIPE FORM
                    $setup_intent = $stripe_payments_manager->new_setup_intent();
                    $wpvs_payment_options_content .= '<div id="rvs-checkout-box"><form action="" method="POST" id="wpvs-new-stripe-card-form" class="wpvs-stripe-card-form"><div class="card-container">';
                    ob_start();
                    include(RVS_MEMBERS_BASE_DIR.'/template/card-form.php');
                    $wpvs_payment_options_content .= ob_get_contents();
                    ob_end_clean();
                    $wpvs_payment_options_content .= '<button class="rvs-button rvs-primary-button rvs-pay-button" type="submit" id="wpvs-new-stripe-card" data-secret="'.$setup_intent['payment_intent_client_secret'].'">';
                    $wpvs_payment_options_content .= __('Pay ' .$this->currency_label.' <span class="rvs-update-total">'.$this->formatted_subtotal.'</span>', 'vimeo-sync-memberships');
                    $wpvs_payment_options_content .= '</button></div>';
                    $wpvs_payment_options_content .= '<input type="hidden" name="stripe_nonce" value="'.wp_create_nonce('stripe-nonce').'"/>';
                    $wpvs_payment_options_content .= '</form></div>';
                }
            } else { #IF STRIPE CUSTOMER HAS CARDS
                $default_card = $wpvs_stripe_customer["invoice_settings"]["default_payment_method"];
                $wpvs_payment_options_content .= '<div class="wpvs-checkout-padding"><form action="" method="POST" id="stripe-payment-form" class="wpvs-stripe-card-form"><div id="rvs-cards">';
                $wpvs_payment_options_content .= '<h4>'.__('You have', 'vimeo-sync-memberships').' '.count($customer_payment_methods).' '.__('saved card(s)', 'vimeo-sync-memberships').'</h4>';
                $wpvs_payment_options_content .= '<div class="wpvs-user-saved-cards">';
                foreach($customer_payment_methods as $card) {
                    if($card->id == $default_card) {
                        $wpvs_payment_options_content .= sprintf(__('Using card ending in', 'vimeo-sync-memberships') . ' **** %s ',$card->card->last4);
                    }
                }
                $wpvs_payment_options_content .= '<a href="'.$this->account_link.'?wpvsview=cards">'.__('Edit Cards', 'vimeo-sync-memberships').'</a></div>';
                $wpvs_payment_options_content .= '</div>'; # END CARDS
                $wpvs_payment_options_content .= '<button class="rvs-button rvs-primary-button rvs-pay-button" type="submit" id="stripe-submit" data-method="'.$default_card.'">'.__('Pay Now ', 'vimeo-sync-memberships');
                $wpvs_payment_options_content .= $this->currency_label;
                $wpvs_payment_options_content .= ' <span class="rvs-update-total">'.$this->formatted_subtotal.'</span></button>';
                $wpvs_payment_options_content .= '<input type="hidden" name="stripe_nonce" value="'.wp_create_nonce('stripe-nonce').'"/>';
                $wpvs_payment_options_content .= '</form></div>'; # END USER HAS CARDS
            }
            $wpvs_payment_options_content .= '</div>';
        } # END STRIPE FORMS

        #IF GOOGLE / APPLE PAY ENABLED
        if($wpvs_google_apple_enabled) {
            $wpvs_payment_options_content .= '<div id="wpvs-google-payment" class="wpvs-payment-box wpvs-checkout-padding wpvs-google-pay-payment"><form action="" method="post" id="google-payment-form"><div class="form-container"><div id="payment-request-button"></div></div>';
            $wpvs_payment_options_content .= '<input type="hidden" name="stripe_nonce" value="'.wp_create_nonce('stripe-nonce').'"/>';
            $wpvs_payment_options_content .= '</form></div>';
        }

        # IF PAYPAL IS ENABLED
        if($wpvs_add_paypal_forms) {
            $wpvs_payment_options_content .= '<div id="wpvs-paypal-payment" class="wpvs-checkout-padding wpvs-payment-box';
            if( ! $wpvs_add_stripe_forms ) {
                $wpvs_payment_options_content .= ' active';
            }
            if( $this->transaction_type == 'subscription' ) {
                $wpvs_payment_options_content .= ' wpvs-paypal-subscription';
            } else {
                $wpvs_payment_options_content .= ' wpvs-paypal-payment';
            }
            $wpvs_payment_options_content .= '">';
            $wpvs_payment_options_content .= '<div id="wpvs-paypal-button"><div id="rvs-paypal-loading">';
            $wpvs_payment_options_content .= '<div class="wpvs-updating-box"><span class="wpvs-loading-text">'.__('PayPal is working', 'vimeo-sync-memberships').'...</span><span class="loadingCircle"></span><span class="loadingCircle"></span><span class="loadingCircle"></span><span class="loadingCircle"></span></div></div>';

            if( ! empty($this->purchase_price) ) {
                $wpvs_payment_options_content .= '<div id="wpvs-paypal-purchase"></div>';
            }
            if( ! empty($this->rental_price) ) {
                $wpvs_payment_options_content .= '<div id="wpvs-paypal-rental" class="';
                if( ! empty($this->purchase_price) ) {
                    $wpvs_payment_options_content .= 'wpvs-hide-paypal-button';
                }
                $wpvs_payment_options_content .= '"></div>';
            }
            $wpvs_payment_options_content .= '</div></div>';
        }

        #IF COINGATE IS ENABLED
        if($wpvs_coingate_enabled) {
            $wpvs_payment_options_content .= '<div id="wpvs-coingate-payment" class="wpvs-payment-box wpvs-checkout-padding">';
            $wpvs_payment_options_content .= '<div id="wpvs-coingate-options" class="wpvs-text-align-center"><div id="wpvs-coingate-fields"><label id="wpvs-converted-coin-amount"></label><div id="wpvs-select-coin"><label class="wpvs-choose-coin rvs-border-box active" data-coin="BTC"><img class="wpvs-coin-option" src="'.RVS_MEMBERS_BASE_URL .'image/bitcoin.png" alt="Checkout with Bitcoin" /> Bitcoin (BTC)</label><label class="wpvs-choose-coin rvs-border-box" data-coin="LTC"><img class="wpvs-coin-option" src="'.RVS_MEMBERS_BASE_URL .'image/litecoin.png" alt="Checkout with Litecoin" /> Litecoin (LTC)</label><label id="wpvs-coingate-checkout" class="rvs-button rvs-primary-button" data-coin="BTC">'.__('Checkout using Bitcoin', 'vimeo-sync-memberships').'<span class="dashicons dashicons-arrow-right-alt"></span></label><label id="wpvs-other-coin-options" class="rvs-border-box"><img src="'.RVS_MEMBERS_BASE_URL .'image/altcoins.png" alt="Checkout with Altcoins" /> '.__('More Coin Options', 'vimeo-sync-memberships').'</label></div></div></div>';
            $wpvs_payment_options_content .= '</div>';
        }

        #IF COINBASE IS ENABLED
        if($wpvs_coinbase_enabled) {
            $wpvs_payment_options_content .= '<div id="wpvs-coinbase-payment" class="wpvs-payment-box wpvs-checkout-padding">';
            $wpvs_payment_options_content .= '<div class="wpvs-text-align-center"><p>';
            $wpvs_payment_options_content .= __('If you are not automatically redirected', 'vimeo-sync-memberships');
            $wpvs_payment_options_content .= ' <a href="" id="wpvs-coinbase-checkout">';
            $wpvs_payment_options_content .= __('click here', 'vimeo-sync-memberships');
            $wpvs_payment_options_content .= '</a></p></div>';
            $wpvs_payment_options_content .= '</div>';
        }
        return $wpvs_payment_options_content;
    }

    public function generate_billing_fields($button_text, $show_button) {
        $wpvs_checkout_form_content = "";
        $wpvs_billing_country = $this->customer->billing_country;
        $wpvs_checkout_form_content .= '<h4>'.__('Billing Information', 'vimeo-sync-memberships').'</h4>';
        $wpvs_checkout_form_content .= '<div class="wpvs-checkout-section rvs-border-box">';
        $wpvs_checkout_form_content .= '<div class="wpvs-checkout-billing-information rvs-area">';
        $wpvs_checkout_form_content .= '<form id="wpvs-billing-info-form" action="" method="POST" class="wpvs-checkout-billing-fields rvs-border-box">';
        $wpvs_checkout_form_content .= '<div class="wpvs-checkout-field rvs-border-box">';
        $wpvs_checkout_form_content .= '<label for="wpvs_billing_address">'.__('Address', 'vimeo-sync-memberships').'<span class="wpvs-required-field">*</span></label>';
        $wpvs_checkout_form_content .= '<input type="text" id="wpvs_billing_address" name="wpvs_billing_address" value="'.$this->customer->billing_address.'" required /></div>';

        $wpvs_checkout_form_content .= '<div class="wpvs-checkout-field rvs-border-box">';
        $wpvs_checkout_form_content .= '<label for="wpvs_billing_address_line_2">'.__('Address Line 2 (optional)', 'vimeo-sync-memberships').'</label>';
        $wpvs_checkout_form_content .= '<input type="text" id="wpvs_billing_address_line_2" name="wpvs_billing_address_line_2" value="'.$this->customer->billing_address_line_2.'" /></div>';

        $wpvs_checkout_form_content .= '<div class="wpvs-checkout-field rvs-border-box">';
        $wpvs_checkout_form_content .= '<label for="wpvs_billing_city">'.__('City', 'vimeo-sync-memberships').'<span class="wpvs-required-field">*</span></label>';
        $wpvs_checkout_form_content .= '<input type="text" id="wpvs_billing_city" name="wpvs_billing_city" value="'.$this->customer->billing_city.'" required /></div>';

        $wpvs_checkout_form_content .= '<div class="wpvs-checkout-field rvs-border-box">';
        $wpvs_checkout_form_content .= '<label for="wpvs_billing_state">'.__('State', 'vimeo-sync-memberships').'/'.__('Province', 'vimeo-sync-memberships').'<span class="wpvs-required-field">*</span></label>';
        $wpvs_checkout_form_content .= '<select id="wpvs_billing_state" name="wpvs_billing_state" required ><option value="">'.__('Select a Country', 'vimeo-sync-memberships').'</option></select></div>';

        $wpvs_checkout_form_content .= '<div class="wpvs-checkout-field rvs-border-box">';
        $wpvs_checkout_form_content .= '<label for="wpvs_billing_zip_code">'.__('Zip/Postal Code', 'vimeo-sync-memberships').'<span class="wpvs-required-field">*</span></label>';
        $wpvs_checkout_form_content .= '<input type="text" id="wpvs_billing_zip_code" name="wpvs_billing_zip_code" value="'.$this->customer->billing_zip_code.'" required /></div>';

        $wpvs_checkout_form_content .= '<div class="wpvs-checkout-field rvs-border-box">';
        $wpvs_checkout_form_content .= '<label for="wpvs_billing_country">'.__('Country', 'vimeo-sync-memberships').'<span class="wpvs-required-field">*</span></label>';
        $wpvs_checkout_form_content .= '<select id="wpvs_billing_country" name="wpvs_billing_country" required >';
        ob_start();
        include(RVS_MEMBERS_BASE_DIR.'/template/country-list.php');
        $wpvs_checkout_form_content .= ob_get_contents();
        ob_end_clean();
        $wpvs_checkout_form_content .= '</select></div>';
        $wpvs_checkout_form_content .= '<input type="hidden" id="wpvs_billing_save" name="wpvs_billing_save" value="wpvs_save_billing"/>';
        $wpvs_checkout_form_content .= '<input type="hidden" id="wpvs_billing_nonce" name="wpvs_billing_nonce" value="'.wp_create_nonce('wpvs-billing-nonce').'"/>';
        if( $show_button ) {
            $wpvs_checkout_form_content .= '<input type="submit" class="button rvs-button rvs-primary-button" value="'.$button_text.'">';
        }
        $wpvs_checkout_form_content .= '</form></div></div>';
        return $wpvs_checkout_form_content;
    }

    public function set_checkout_json_data() {
        $this->add_trial_period = $this->add_trial_period === true ? 'true': 'false';
        $wpvs_checkout_json_data = array(
            'product_id' => $this->product_id,
            'product_name' => $this->product_name,
            'product_price' => $this->product_price,
            'subtotal' => $this->subtotal,
            'total'    => $this->total,
            'taxes'    => $this->applied_taxes,
            'redirect_success' => $this->redirect_url,
            'redirect_cancel'  => $this->cancel_redirect,
            'currency'  => $this->currency,
            'currency_label' => $this->currency_label,
            'trial' => $this->add_trial_period,
            'checkout_type' => $this->checkout_type,
            'transaction_type' => $this->transaction_type,
            'purchase_type' => $this->purchase_type,
            'formatted_pricing' => array(
                'subtotal' => $this->formatted_subtotal,
                'total' => $this->formatted_total,
                'product_price' => $this->formatted_product_price,
            ),
        );
        wp_localize_script('wpvs-checkout-js', 'wpvscheckout', $wpvs_checkout_json_data);
    }

    public function update_checkout_json_data($wpvs_checkout) {
        $wpvs_checkout = (object) $wpvs_checkout;
        $this->set_purchase_type($wpvs_checkout->purchase_type);
        if( ! empty($wpvs_checkout->coupon_code) ) {
            $this->set_coupon_code($wpvs_checkout->coupon_code);
        }
        if( $wpvs_checkout->checkout_type == 'subscription' ) {
            $this->set_membership($wpvs_checkout->product_id);
            $wpvs_checkout->trial = $wpvs_checkout->trial === 'true' ? true: false;
            if( ! empty($wpvs_checkout->trial) ) {
                $this->apply_trial();
            }

        }
        if( $wpvs_checkout->checkout_type == 'purchase' ) {
            $this->set_product($wpvs_checkout->product_id);
        }


        $wpvs_checkout_json_data = array(
            'product_id' => $this->product_id,
            'product_name' => $this->product_name,
            'product_price' => $this->product_price,
            'subtotal' => $this->subtotal,
            'total'    => $this->total,
            'taxes'    => $this->applied_taxes,
            'redirect_success' => $wpvs_checkout->redirect_success,
            'redirect_cancel'  => $wpvs_checkout->redirect_cancel,
            'currency'  => $this->currency,
            'currency_label' => $this->currency_label,
            'trial' => $this->add_trial_period,
            'checkout_type' => $wpvs_checkout->checkout_type,
            'transaction_type' => $wpvs_checkout->transaction_type,
            'purchase_type' => $wpvs_checkout->purchase_type,
            'formatted_pricing' => array(
                'subtotal' => $this->formatted_subtotal,
                'total' => $this->formatted_total,
                'product_price' => $this->formatted_product_price,
            ),
        );
        if( ! empty($this->coupon_code) ) {
            $wpvs_checkout_json_data['coupon_code'] = $this->coupon_code;
        }
        return $wpvs_checkout_json_data;
    }

    public function check_errors() {
        $wpvs_error_message = "";
        if( isset($_GET['errmsg']) && ! empty($_GET['errmsg']) ) {
            $wpvs_error_text = "";
            if(isset($_GET['subscription']) && $_GET['subscription'] == 'failed') {
                $wpvs_error_text = __('Something went wrong with your payment', 'vimeo-sync-memberships');
                $wpvs_error_text .= ': '.explode(':',$_GET['errmsg'])[0];
            }
            if(isset($_GET['charge']) && $_GET['charge'] == 'failed') {
                $wpvs_error_text = __('Something went wrong with your payment', 'vimeo-sync-memberships');
                $wpvs_error_text .= ': '.$_GET['errmsg'];
            }
            if(isset($_GET['account']) && $_GET['account'] == 'billing') {
               $wpvs_error_text = __('Please fill in required billing information', 'vimeo-sync-memberships');
            }
            if( empty($wpvs_error_text) ) {
                $wpvs_error_text = __('Something went wrong with your payment', 'vimeo-sync-memberships');
            }
            $wpvs_error_message .= '<p class="error rvs-error">' . $wpvs_error_text . '</p>';
        }
        return $wpvs_error_message;
    }
}

/**
* WP Video Memberships Tax Rate Class
*/

class WPVS_Tax_Rate {

    public $id;
    public $active;
    public $display_name;
    public $description;
    public $jurisdiction;
    public $percentage;
    public $metadata;
    protected $tax_rate_table_name;

    public function __construct() {
        global $wpdb;
        $this->tax_rate_table_name = $wpdb->prefix . 'wpvs_tax_rates';
    }
    protected function tax_rate_exists($name, $jurisdiction, $country) {
        $tax_rate_exists = false;
        $wpvs_tax_rates = $this->get_tax_rates();
        if( ! empty($wpvs_tax_rates) ) {
            foreach($wpvs_tax_rates as $tax_rate) {
                if( $tax_rate->display_name == $name && $tax_rate->jurisdiction == $jurisdiction && $tax_rate->country == $country ) {
                    $tax_rate_exists = true;
                    break;
                }
            }
        }
        return $tax_rate_exists;
    }

    public function get_tax_rates() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM $this->tax_rate_table_name");
    }

    public function get_tax_rate_by_id($tax_rate_id) {
        global $wpdb;
        $found_tax_rate = $wpdb->get_results("SELECT * FROM $this->tax_rate_table_name WHERE id = '$tax_rate_id'");
        if( ! empty($found_tax_rate) ) {
            $found_tax_rate = $found_tax_rate[0];
        }
        return $found_tax_rate;
    }

    public function get_tax_rates_by_jurisdiction($jurisdiction, $country) {
        global $wpdb;
        $tax_rate_query = $wpdb->prepare("SELECT * FROM $this->tax_rate_table_name WHERE jurisdiction = %s AND country = %s", array($jurisdiction, $country));
        $found_tax_rates = $wpdb->get_results($tax_rate_query);
        return $found_tax_rates;
    }

    public function calculate_tax_rate_amount($total, $tax_rate) {
        if( $tax_rate->inclusive ) {
            $tax_percentage_calc = 1 + ($tax_rate->percentage/100);
            $subtotal = $total / $tax_percentage_calc;
            $tax_amount = $total - $subtotal;
        } else {
            $tax_amount = ($total*$tax_rate->percentage) / 100;
        }
        return (object) array( 'amount' => number_format($tax_amount, 0, '', ''), 'decimal_amount' => number_format($tax_amount/100, 2) );
    }
}
