<?php

add_action( 'wp_ajax_wpvs_create_new_account_req', 'wpvs_create_new_account_req' );
add_action( 'wp_ajax_nopriv_wpvs_create_new_account_req', 'wpvs_create_new_account_req' );

function wpvs_create_new_account_req() {
    global $rvs_live_mode;
    $user_logged_in = false;
    $rvs_create_account_link = null;
    $error_message = null;
    $custom_fields = null;
    $wpvs_signup_create_password = get_option('wpvs_signup_create_password', 1);
    if( isset($_POST["new_user_email"]) ) {
        if( $wpvs_signup_create_password && ! isset($_POST["new_user_password"]) ) {
            rvs_exit_ajax(__('Error creating your account', 'vimeo-sync-memberships'), 400);
        }
        $rvs_usernames_allowed = get_option('rvs_usernames_allowed', 1);
        $wpvs_signup_create_password = get_option('wpvs_signup_create_password', 1);
        if($rvs_usernames_allowed && isset($_POST["new_user_login"])) {
            $new_username = sanitize_text_field($_POST["new_user_login"]);
            $new_username = sanitize_user($new_username);
        } else {
            $new_username = sanitize_user($_POST["new_user_email"]);
        }

        if(strpos($new_username, ' ')) {
            $error_message = __('Username must be numbers and letters only (no spaces or special characters)', 'vimeo-sync-memberships');
        } else {
            $new_email = sanitize_email($_POST["new_user_email"]);

            if( isset($_POST["new_user_password"]) && isset($_POST["confirm_user_password"]) ) {
                $new_password = $_POST["new_user_password"];
                $confirm_password = $_POST["confirm_user_password"];
                if($new_password != $confirm_password) {
                    $error_message = __('Passwords do not match.', 'vimeo-sync-memberships');
                }

                if(strlen($new_password) < 6) {
                    $error_message = __('Password is too short.', 'vimeo-sync-memberships');
                }
            } else {
                $new_password = wp_generate_password();
            }
        }

        if( ! empty($error_message) ) {
            rvs_exit_ajax($error_message, 400);
            exit;
        } else {

            $new_user_data = array(
                'user_login' => $new_username,
                'user_email' => $new_email,
                'user_pass' => $new_password
            );

            if( isset($_POST["wpvs_first_name"]) && ! empty($_POST["wpvs_first_name"]) ) {
                $user_first_name = sanitize_text_field($_POST["wpvs_first_name"]);
                $new_user_data['first_name'] = $user_first_name;
                $new_user_data['display_name'] = $user_first_name;
            }

            if( isset($_POST["wpvs_last_name"]) && ! empty($_POST["wpvs_last_name"]) ) {
                $new_user_data['last_name'] = sanitize_text_field($_POST["wpvs_last_name"]);
            }

            if( isset($_POST["wpvs_custom_registration_fields"]) && ! empty($_POST["wpvs_custom_registration_fields"]) ) {
                $custom_fields = $_POST["wpvs_custom_registration_fields"];
                $wpvs_custom_content_filters = new WPVS_Custom_Content_Filters();
                $custom_form_fields = $wpvs_custom_content_filters->check_custom_registration_form_fields($custom_fields);
                if( isset($custom_form_fields['error']) && $custom_form_fields['error'] ) {
                    $error_message = __('Something went wrong creating your account.', 'vimeo-sync-memberships');
                    if( isset($custom_form_fields['message']) && ! empty( $custom_form_fields['message'] ) ) {
                        $error_message = $custom_form_fields['message'];
                        rvs_exit_ajax($error_message, 400);
                        exit;
                    }
                }
            }

            $new_user = wp_insert_user( $new_user_data );
        }

        if( ! empty($new_user) && is_wp_error($new_user)) {
            $error_message = $new_user->get_error_message();
            rvs_exit_ajax($error_message, 400);
        } else {
            if( ! $wpvs_signup_create_password ) {
                wp_new_user_notification( $new_user, null, 'user' );
            }
            wpvs_send_email("Account", $new_user, null);
            $creds = array(
                'user_login'    => $new_username,
                'user_password' => $new_password,
                'remember'      => true
            );
            $user_login = wp_signon( $creds, false );

            if( ! empty($custom_fields) && $wpvs_custom_content_filters && is_a($wpvs_custom_content_filters, 'WPVS_Custom_Content_Filters') ) {
                $wpvs_custom_content_filters->apply_custom_registration_form_fields($custom_fields, $new_user);
            }
        }

        if( ! empty($user_login) && ! is_wp_error($user_login) ) {
            $rvs_register_page = get_option('rvs_sign_up_page');
            $rvs_redirect_page = get_option('rvs_redirect_page', $rvs_register_page);
            if($rvs_redirect_page != "noredirect") {
                if($rvs_redirect_page == "custom") {
                    $rvs_create_account_link = get_option('rvs_redirect_signup_link');
                } else {
                    $rvs_create_account_link = get_permalink($rvs_redirect_page);
                }
            }
            $user_logged_in = true;
        } else {
            $error_message = $user_login->get_error_message();
            rvs_exit_ajax($error_message, 400);
        }
        echo json_encode(array('loggedin' => $user_logged_in, 'redirect' => $rvs_create_account_link));

    } else {
        rvs_exit_ajax(__('Error creating your account', 'vimeo-sync-memberships'), 400);
    }
    wp_die();
}

if( ! function_exists('wpvs_verify_google_recaptcha_areq') ) {
    add_action( 'wp_ajax_wpvs_verify_google_recaptcha_areq', 'wpvs_verify_google_recaptcha_areq' );
    add_action( 'wp_ajax_nopriv_wpvs_verify_google_recaptcha_areq', 'wpvs_verify_google_recaptcha_areq' );
    function wpvs_verify_google_recaptcha_areq() {
        if(isset($_POST["recaptcha_code"]) && ! empty($_POST["recaptcha_code"]) ) {
            $wpvs_google_recaptcha_settings = get_option('wpvs_google_recaptcha_settings');
            if( ! isset($wpvs_google_recaptcha_settings['secret_key']) || empty($wpvs_google_recaptcha_settings['secret_key']) ) {
                rvs_exit_ajax(__('Missing reCAPTCHA credentials', 'vimeo-sync-memberships'), 400);
            } else {
                $recaptcha_code = $_POST["recaptcha_code"];
                $secret_key = $wpvs_google_recaptcha_settings['secret_key'];
                $recaptcha_post = array(
                    'secret'   => $secret_key,
                    'response' => $recaptcha_code
                );
                $recaptcha_post_json = json_encode($recaptcha_post);
                $wpvs_curl = curl_init('https://www.google.com/recaptcha/api/siteverify');
                curl_setopt($wpvs_curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($wpvs_curl, CURLOPT_POST, true);
                curl_setopt($wpvs_curl, CURLOPT_POSTFIELDS, $recaptcha_post);
                $verify_recaptcha = curl_exec($wpvs_curl);
                $recaptcha_verified = json_decode($verify_recaptcha, true);
                curl_close($wpvs_curl);
                if( isset($recaptcha_verified['success']) && $recaptcha_verified['success'] == true ) {
                    echo $verify_recaptcha;
                } else {
                    rvs_exit_ajax(__('Unable to verify you are human.', 'vimeo-sync-memberships'), 400);
                }
            }
        }
        wp_die();
    }
}

if( ! function_exists('wpvs_update_customer_account_info')) {
function wpvs_update_customer_account_info() {
    $error_message = null;
    $custom_fields = null;
    if( is_user_logged_in() ) {
        global $rvs_current_user;
        if( ! empty($rvs_current_user) ) {
            if(wp_verify_nonce($_POST['wpvs_account_nonce'], 'wpvs-update-account')) {
                $new_password = null;
                $update_user_data = array('ID' => $rvs_current_user->ID);

                if( isset($_POST["new_user_email"]) ) {
                    $new_email = sanitize_email($_POST["new_user_email"]);
                    if($new_email != $rvs_current_user->user_email) {
                        $update_user_data['user_email'] = sanitize_email($_POST["new_user_email"]);
                    }
                } else {
                    $error_message = __('Please provide an email address.', 'vimeo-sync-memberships');
                }

                if( isset($_POST["wpvs_first_name"]) ) {
                    $new_first_name = sanitize_text_field($_POST["wpvs_first_name"]);
                    if($new_first_name != $rvs_current_user->first_name) {
                        $update_user_data['first_name'] = $new_first_name;
                        $update_user_data['display_name'] = $new_first_name;
                    }
                }

                if( isset($_POST["wpvs_last_name"]) ) {
                    $new_last_name = sanitize_text_field($_POST["wpvs_last_name"]);
                    if($new_last_name != $rvs_current_user->last_name) {
                        $update_user_data['last_name'] = $new_last_name;
                    }
                }

                if( isset($_POST["new_user_password"]) && ! empty($_POST["new_user_password"]) ) {
                    $new_password = $_POST["new_user_password"];
                }

                if( isset($_POST["confirm_user_password"]) && ! empty($_POST["confirm_user_password"]) ) {
                    $confirm_password = $_POST["confirm_user_password"];
                }

                if( ! empty($new_password) ) {
                    if($new_password != $confirm_password) {
                        $error_message = __('Passwords do not match.', 'vimeo-sync-memberships');
                    }

                    if(strlen($new_password) < 6) {
                        $error_message = __('Password is too short.', 'vimeo-sync-memberships');
                    }
                }

                if( isset($_POST["wpvs_custom_account_fields"]) && ! empty($_POST["wpvs_custom_account_fields"]) ) {
                    $custom_fields = $_POST["wpvs_custom_account_fields"];
                    $wpvs_custom_content_filters = new WPVS_Custom_Content_Filters();
                    $custom_form_fields = $wpvs_custom_content_filters->check_custom_registration_form_fields($custom_fields);
                    if( isset($custom_form_fields['error']) && $custom_form_fields['error'] ) {
                        $error_message = __('Something went wrong creating your account.', 'vimeo-sync-memberships');
                        if( isset($custom_form_fields['message']) && ! empty( $custom_form_fields['message'] ) ) {
                            $error_message = $custom_form_fields['message'];
                            rvs_exit_ajax($error_message, 400);
                            exit;
                        }
                    }
                }

                if( ! empty($custom_fields) && $wpvs_custom_content_filters && is_a($wpvs_custom_content_filters, 'WPVS_Custom_Content_Filters') ) {
                    $wpvs_custom_content_filters->apply_custom_registration_form_fields($custom_fields, $rvs_current_user->ID);
                }

                if( ! empty($error_message) ) {
                    rvs_exit_ajax($error_message, 400);
                    exit;
                } else {
                    if( ! empty($new_password) ) {
                        $update_user_data['user_pass'] = $new_password;
                    }
                    $user_updated = wp_update_user( $update_user_data );
                }

                if(!empty($user_updated) && is_wp_error($user_updated)) {
                    $error_message = $new_user->get_error_message();
                    rvs_exit_ajax($error_message, 400);
                } else {
                    _e('Account updated.', 'vimeo-sync-memberships');
                }
            }
        }
    } else {
        rvs_exit_ajax(__('User not logged in', 'vimeo-sync-memberships'), 400);
    }
    wp_die();
}
add_action( 'wp_ajax_wpvs_update_customer_account_info', 'wpvs_update_customer_account_info' );
}

add_action( 'wp_ajax_wpvs_save_billing_on_checkout', 'wpvs_save_billing_on_checkout' );

function wpvs_save_billing_on_checkout() {
    if( is_user_logged_in() ) {
        global $rvs_current_user;
        if( ! empty($rvs_current_user) && isset($_POST['wpvs_billing_save']) && $_POST['wpvs_billing_save'] == 'wpvs_save_billing' && wp_verify_nonce($_POST['wpvs_billing_nonce'], 'wpvs-billing-nonce') ) {

            if( isset($_POST['wpvs_billing_address']) ) {
                update_user_meta( $rvs_current_user->ID, 'wpvs_billing_address', sanitize_text_field($_POST['wpvs_billing_address']));
            }
            if( isset($_POST['wpvs_billing_address_line_2']) ) {
                update_user_meta( $rvs_current_user->ID, 'wpvs_billing_address_line_2', sanitize_text_field($_POST['wpvs_billing_address_line_2']));
            }
            if( isset($_POST['wpvs_billing_city']) ) {
                update_user_meta( $rvs_current_user->ID, 'wpvs_billing_city', sanitize_text_field($_POST['wpvs_billing_city']));
            }
            if( isset($_POST['wpvs_billing_state']) ) {
                update_user_meta( $rvs_current_user->ID, 'wpvs_billing_state', sanitize_text_field($_POST['wpvs_billing_state']));
            }
            if( isset($_POST['wpvs_billing_state']) ) {
                update_user_meta( $rvs_current_user->ID, 'wpvs_billing_zip_code', sanitize_text_field($_POST['wpvs_billing_zip_code']));
            }
            if( isset($_POST['wpvs_billing_country']) ) {
                update_user_meta( $rvs_current_user->ID, 'wpvs_billing_country', sanitize_text_field($_POST['wpvs_billing_country']));
            }
        }
    } else {
        rvs_exit_ajax(__('User not logged in', 'vimeo-sync-memberships'), 400);
    }
    wp_die();
}

add_action('wp_ajax_wpvs_get_checkout_total_amounts', 'wpvs_get_checkout_total_amounts');

function wpvs_get_checkout_total_amounts() {
    global $rvs_current_user;
    $wpvs_customer = new WPVS_Customer($rvs_current_user);
    $subtotal = 0;
    $applied_taxes = array();
    if( isset($_GET['wpvs_checkout']) ) {
        $wpvs_checkout = new WPVS_Checkout($wpvs_customer);
        $current_wpvs_checkout = $_GET['wpvs_checkout'];
        $updated_wpvs_checkout = $wpvs_checkout->update_checkout_json_data($current_wpvs_checkout);
        echo json_encode($updated_wpvs_checkout);
    } else {
        rvs_exit_ajax(__('Missing total checkout amount for tax calculation.', 'vimeo-sync-memberships'), 400);
    }
    wp_die();
}
