<?php

function wpvs_ajax_url_variables() {
    global $rvs_live_mode;
    global $rvs_message_array;
    global $rvs_current_user;
    global $rvs_currency;
    $wpvs_requests_user_id = null;
    $stripe_customer_id = null;
    if($rvs_live_mode == "off") {
        $stripe_key = 'rvs_stripe_customer_test';
    } else {
        $stripe_key = 'rvs_stripe_customer';
    }
    if($rvs_current_user) {
        $wpvs_requests_user_id = $rvs_current_user->ID;
        $stripe_customer_id = get_user_meta($rvs_current_user->ID, $stripe_key, true);
    } ?>
    <script type="text/javascript">
        var rvsrequests = <?php echo json_encode( array(
            'ajax' => admin_url( "admin-ajax.php" ),
            'rvsmessage' => $rvs_message_array,
            'currency' => $rvs_currency,
            'user' => array(
                'id' => $wpvs_requests_user_id,
                'customer' => $stripe_customer_id
        ) ) ); ?>;
    </script>
<?php }
add_action ( 'wp_head', 'wpvs_ajax_url_variables' );

function rvs_load_membership_scripts() {
    global $rvs_live_mode;
    global $rvs_currency;
    global $rvs_current_version;
    global $rvs_current_user;
    global $wpvs_stripe_gateway_enabled;
    global $wpvs_stripe_options;
    global $wpvs_include_checkout_scripts;
    $wpvs_stripe_invoices_enabled = false;
    $wpvs_user_billing_state = false;
    if( isset($wpvs_stripe_options['invoices_enabled']) && ! empty($wpvs_stripe_options['invoices_enabled']) ) {
        $wpvs_stripe_invoices_enabled = true;
    }
    if($rvs_current_user) {
        $wpvs_user_billing_state = get_user_meta( $rvs_current_user->ID, 'wpvs_billing_state', true);
    }

    wp_enqueue_style('rvs-styling', RVS_MEMBERS_BASE_URL . 'css/rvs-membership-styles.css', '', $rvs_current_version);
    wp_enqueue_style('rvs-user-styling', RVS_MEMBERS_BASE_URL . 'css/rvs-user-styles.css', '', $rvs_current_version);
    wp_enqueue_style('rvs-user-forms', RVS_MEMBERS_BASE_URL . 'css/forms.css', '', $rvs_current_version);
    wp_enqueue_style('wpvs-members', RVS_MEMBERS_BASE_URL . 'css/wpvs-user.css', '', $rvs_current_version);
    wp_enqueue_style('dashicons');
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'rvs-memberships-js', RVS_MEMBERS_BASE_URL .'js/rvs-membership.js', array('jquery'), $rvs_current_version, true);
    wp_enqueue_script( 'rvs-account', RVS_MEMBERS_BASE_URL .'js/rvs-account.js', array('rvs-memberships-js'), $rvs_current_version, true);
    wp_localize_script('rvs-account', 'rvssettings', array(
        'ajax' => admin_url('admin-ajax.php'),
        'currency' => $rvs_currency,
        'rvsmessage' => array(
            'payments' => __('Getting Payments','vimeo-sync-memberships'),
            'refunded' => __('Refunded','vimeo-sync-memberships'),
            'paid' => __('Paid','vimeo-sync-memberships'),
            'nomorepayments' => __('No More Payments','vimeo-sync-memberships'),
            'cancelpaypal' => __('Are you sure you want to cancel this membership?','vimeo-sync-memberships'),
            'deletepaypal' => __('Are you sure you want to delete this membership?','vimeo-sync-memberships'),
            'cancellingpp' => __('Cancelling Membership','vimeo-sync-memberships'),
            'reactivate' => __('Re-activating Membership','vimeo-sync-memberships'),
            'suspend' => __('Pausing Membership','vimeo-sync-memberships'),
            'delete' => __('Deleting Membership','vimeo-sync-memberships'),
            'gettinginvoice' => __('Getting Invoice','vimeo-sync-memberships'),
            'gettingpaymenturl' => __('Redirecting to payment page','vimeo-sync-memberships')
        ),
        'stripeinvoices' => $wpvs_stripe_invoices_enabled
    ));
    wp_register_script( 'wpvs-country-dropdown-js', RVS_MEMBERS_BASE_URL .'js/wpvs-country-dropdown.js', array('jquery'), $rvs_current_version, true);
    wp_localize_script('wpvs-country-dropdown-js', 'wpvsdir', array(
        'list' => RVS_MEMBERS_BASE_URL . 'countries/',
        'state' => $wpvs_user_billing_state
    ));
    wp_register_script( 'wpvs-checkout-js', RVS_MEMBERS_BASE_URL .'js/wpvs-checkout.js', array('jquery'), $rvs_current_version, true);
    if( $wpvs_include_checkout_scripts ) {
         wp_enqueue_script( 'wpvs-checkout-js' );
         wp_enqueue_script( 'wpvs-country-dropdown-js' );
    }
    if($wpvs_stripe_gateway_enabled) {
        wp_register_style('wpvs-stripe-forms', RVS_MEMBERS_BASE_URL . 'include-stripe/css/stripe-forms.css', '', $rvs_current_version);
        wp_register_script('wpvs-google-apple-payments', RVS_MEMBERS_STRIPE_URL . 'js/stripe-google.js', array('wpvs-stripe'), $rvs_current_version, true);
    }
}

// ENQUEUE STANDARD MEMBERSHIP SCRIPTS
add_action('wp_enqueue_scripts', 'rvs_load_membership_scripts');

// LOAD CUSTOM USER STYLES
function rvs_customize_css() {
    global $rvs_message_array;
    $rvs_primary_color = get_option('rvs_primary_color', '#27ae60');
    $rvs_delete_color = get_option('rvs_delete_color', '#c0392b');
    $rvs_card_form_background = get_option('rvs_card_form_background', '#ffffff');
    $rvs_card_form_color = get_option('rvs_card_form_color', '#444444');
    $rvs_card_button_background = get_option('rvs_card_button_background', '#27ae60');
    ?>
     <style type="text/css">

         .rvs-primary-button, a.rvs-primary-button, .loadingCircle, .wpvs-login-form input[type="submit"], #wpvs-login-form input[type="submit"] {
             background: <?php echo $rvs_primary_color; ?>;
             color: #fff;
         }
         .rvs-edit-card, .wpvs-loading-text, label.wpvs-login-label:hover {
             color: <?php echo $rvs_primary_color; ?>;
         }
         .rvs-red-button {
             background: <?php echo $rvs_delete_color; ?>;
         }
         .removeMembership, .rvs-remove, .removePayPalPlan, .remove-coupon-code, .removeCoinMembership, .removeCoinBaseMembership {
             color: <?php echo $rvs_delete_color; ?>;
         }

         .card-container {
            background: <?php echo $rvs_card_form_background; ?>;
        }

        .card-container input, .card-container select, .wpvs-stripe-card-form .StripeElement {
            color: <?php echo $rvs_card_form_color; ?>;
        }

        .rvs-button.rvs-pay-button {
            background: <?php echo $rvs_card_button_background; ?>;
        }
     </style>

<?php
}
add_action( 'wp_head', 'rvs_customize_css');
