<?php

/**
* WP Video Memberships Customer
*/

class WPVS_Customer {

    public $user;
    public $user_id;
    public $first_name;
    public $last_name;
    public $billing_address;
    public $billing_address_line_2;
    public $billing_city;
    public $billing_state;
    public $billing_zip_code;
    public $billing_country;
    public $full_name;
    public $test_mode;

    protected $plan_key;

    public function __construct($wp_user) {
        if( empty($wp_user) ) {
            return;
        }
        global $rvs_live_mode;
        if( $rvs_live_mode == "on" ) {
            $this->test_mode = false;
        } else {
            $this->test_mode = true;
        }
        $this->user = $wp_user;
        $this->user_id = $wp_user->ID;
        $this->first_name = $wp_user->first_name;
        $this->last_name = $wp_user->last_name;
        $this->full_name = $this->first_name.' '.$this->last_name;
        $billing_information = $this->set_billing_address();
        $this->billing_address = $billing_information['address'];
        $this->billing_address_line_2 = $billing_information['address_line_2'];
        $this->billing_city = $billing_information['city'];
        $this->billing_state = $billing_information['state'];
        $this->billing_zip_code = $billing_information['zip'];
        $this->billing_country = $billing_information['country'];
    }

    public function get_memberships() {
        $user_memberships = array();
        if( $this->test_mode ) {
            $user_memberships = get_user_meta($this->user_id, 'rvs_user_memberships_test', true);
        } else {
            $user_memberships = get_user_meta($this->user_id, 'rvs_user_memberships', true);
        }
        return $user_memberships;
    }

    public function get_memberships_by_mode( $get_test_memberships = false ) {
        $user_memberships = array();
        if( $get_test_memberships ) {
            $user_memberships = get_user_meta($this->user_id, 'rvs_user_memberships_test', true);
        } else {
            $user_memberships = get_user_meta($this->user_id, 'rvs_user_memberships', true);
        }
        return $user_memberships;
    }

    public function get_membership_by_id($membership_id) {
        $found_membership = null;
        $user_memberships = $this->get_memberships_by_mode($this->test_mode);
        if(!empty($user_memberships)) {
            foreach($user_memberships as $key => &$membership) {
                if($membership["plan"] == $membership_id) {
                    $found_membership = $membership;
                    break;
                }
            }
        }
        return (object) $found_membership;
    }

    public function get_stripe_subscription($subscription_id, $test_memberships = false) {
        $stripe_subscription = null;
        $user_memberships = $this->get_memberships_by_mode($test_memberships);
        if(!empty($user_memberships)) {
            foreach($user_memberships as $key => &$membership) {
                if($membership["id"] == $subscription_id) {
                    $stripe_subscription = $membership;
                    break;
                }
            }
        }
        return $stripe_subscription;
    }

    public function get_paypal_agreement($agreement_id, $test_memberships = false) {
        $paypal_agreement = null;
        $user_memberships = $this->get_memberships_by_mode($test_memberships);
        if(!empty($user_memberships)) {
            foreach($user_memberships as $key => &$membership) {
                if($membership["id"] == $agreement_id) {
                    $paypal_agreement = $membership;
                    break;
                }
            }
        }
        return $paypal_agreement;
    }

    public function has_membership($membership_id) {
        $has_membership = false;
        $user_memberships = $this->get_memberships();
        if( ! empty($user_memberships) ) {
            foreach($user_memberships as $key => $membership) {
                if($membership["plan"] == $membership_id) {
                    $has_membership = true;
                    break;
                }
            }
        }
        return $has_membership;
    }

    /*private function get_membership_expiry_date($membership) {
        $membership_expires = null;
        if( isset($membership['ends']) ) {
            if($membership['type'] == "paypal" || $membership['type'] == "paypal_test") {
                $membership_expires = strtotime($membership['ends']);
            } else {
                $membership_expires = $membership['ends'];
            }
        }
        return $membership_expires;
    }

    public function has_active_membership($membership_id) {
        $current_time = current_time('timestamp', 1);
        $has_membership = false;
        $user_memberships = $this->get_memberships();
        if( ! empty($user_memberships) ) {
            foreach($user_memberships as $key => $membership) {
                if($membership["plan"] == $membership_id) {
                    if( $membership['ends'] == 'never' ) {
                        $has_membership = true;
                    } else {
                        $membership_expires = $this->get_membership_expiry_date($membership);
                        if( $membership_expires && $membership_expires >= $current_time) {
                            $has_membership = true;
                        }
                    }
                    break;
                }
            }
        }
        return $has_membership;
    }*/

    public function has_membership_by_id($membership_id) {
        $has_membership = false;
        $no_access_reason = null;
        $no_access_message = null;
        $user_memberships = $this->get_memberships();
        if( ! empty($user_memberships) ) {
            foreach($user_memberships as $key => $membership) {
                if($membership["plan"] == $membership_id) {
                    $has_membership = true;
                    $membership_type = 'stripe';
                    if( isset($membership['type']) ) {
                        if($membership['type'] == "stripe" || $membership['type'] == "stripe_test") {
                            $membership_type = 'stripe';
                        }
                        if($membership['type'] == "paypal" || $membership['type'] == "paypal_test") {
                            $membership_type = 'paypal';
                        }
                        if($membership['type'] == "coingate" || $membership['type'] == "coingate_test") {
                            $membership_type = 'coingate';
                        }

                        if($membership['type'] == "coinbase" || $membership['type'] == "coinbase_test") {
                            $membership_type = 'coinbase';
                        }

                        if($membership['type'] == "manual") {
                            $membership_type = 'manual';
                        }
                    }

                    if($membership["status"] == "past_due" || $membership["status"] == "unpaid" || $membership["status"] == "incomplete" || $membership["status"] == "incomplete_expired") {
                        $no_access_reason = "overdue";
                        $no_access_message = __('Your subscription payment is overdue', 'vimeo-sync-memberships');
                    } else {
                        if( $membership_type == 'coingate' ) {
                            if($membership["status"] == "new" || $membership["status"] == "pending" || $membership["status"] == "invalid" || $membership["status"] == "expired") {
                                $no_access_reason = "overdue";
                                $no_access_message = __('Your subscription is awaiting payment', 'vimeo-sync-memberships');
                            }
                            if($membership["status"] == "confirming") {
                                $no_access_reason = "overdue";
                                $no_access_message = __('Subscription is waiting for payment confirmation', 'vimeo-sync-memberships');
                            }
                            if($membership["status"] == "paid" || $membership["status"] == "canceled") {
                                $no_access_reason = null;
                                $no_access_message = null;
                            }
                        }

                        if( $membership_type == 'coinbase' ) {
                            if($membership["status"] == "NEW" || $membership["status"] == "UNRESOLVED" || $membership["status"] == "EXPIRED") {
                                $no_access_reason = "overdue";
                                $no_access_message = __('Your subscription is awaiting payment', 'vimeo-sync-memberships');
                            }
                            if($membership["status"] == "PENDING") {
                                $no_access_reason = "overdue";
                                $no_access_message = __('Your subscription is waiting for payment confirmation', 'vimeo-sync-memberships');
                            }
                            if($membership["status"] == "COMPLETED" || $membership["status"] == "CONFIRMED") {
                                $no_access_reason = null;
                                $no_access_message = null;
                            }
                        }
                    }
                    break;
                }
            }
        }
        return array('has_access' => $has_membership, 'reason' => $no_access_reason, 'message' => $no_access_message);
    }

    public function has_access($membership_array, $post_id) {
        $has_membership = false;
        $no_access_reason = "nosubscription";
        $no_access_message = __('Sorry, you do not have access to this video.', 'vimeo-sync-memberships');
        $rvs_overdue_access = get_option('rvs_overdue_access', 0);
        $user_memberships = $this->get_memberships();
        if( ! empty($user_memberships) && ! empty($membership_array) ) {
            foreach($user_memberships as $key => $membership) {
                if(in_array($membership["plan"], $membership_array)) {
                    $membership_type = 'stripe';
                    if( isset($membership['type']) ) {
                        if($membership['type'] == "stripe" || $membership['type'] == "stripe_test") {
                            $membership_type = 'stripe';
                        }
                        if($membership['type'] == "paypal" || $membership['type'] == "paypal_test") {
                            $membership_type = 'paypal';
                        }
                        if($membership['type'] == "coingate" || $membership['type'] == "coingate_test") {
                            $membership_type = 'coingate';
                        }

                        if($membership['type'] == "coinbase" || $membership['type'] == "coinbase_test") {
                            $membership_type = 'coinbase';
                        }

                        if($membership['type'] == "manual") {
                            $membership_type = 'manual';
                        }
                    }

                    if( $rvs_overdue_access && ($membership["status"] == "past_due" || $membership["status"] == "unpaid" || $membership["status"] == "incomplete" || $membership["status"] == "incomplete_expired") ) {
                        $no_access_reason = "overdue";
                        $no_access_message = __('Your subscription payment is overdue', 'vimeo-sync-memberships');
                    } else {
                        $wpvs_current_time = current_time('timestamp', 1);
                        $membership_ends = $membership["ends"];
                        if($membership_ends != "never") {
                            $membership_expires = $membership_ends;
                            if( $membership_type == 'paypal' ) {
                                $membership_expires = strtotime($membership_ends);
                            }

                            if( intval($membership_expires) > $wpvs_current_time) {
                                if( $membership_type == 'coingate' || $membership_type == 'coinbase' ) {
                                    if( $membership_type == 'coingate' ) {
                                        if($membership["status"] == "new" || $membership["status"] == "pending" || $membership["status"] == "invalid" || $membership["status"] == "expired") {
                                            $no_access_reason = "overdue";
                                            $no_access_message = __('Your subscription is awaiting payment', 'vimeo-sync-memberships');
                                        }
                                        if($membership["status"] == "confirming") {
                                            $no_access_reason = "overdue";
                                            $no_access_message = __('Subscription is waiting for payment confirmation', 'vimeo-sync-memberships');
                                        }
                                        if($membership["status"] == "paid") {
                                            $has_membership = true;
                                            $no_access_reason = null;
                                            $no_access_message = null;
                                        }
                                    }

                                    if( $membership_type == 'coinbase' ) {
                                        if($membership["status"] == "NEW" || $membership["status"] == "UNRESOLVED" || $membership["status"] == "EXPIRED") {
                                            $no_access_reason = "overdue";
                                            $no_access_message = __('Your subscription is awaiting payment', 'vimeo-sync-memberships');
                                        }
                                        if($membership["status"] == "PENDING") {
                                            $no_access_reason = "overdue";
                                            $no_access_message = __('Subscription is waiting for payment confirmation', 'vimeo-sync-memberships');
                                        }
                                        if($membership["status"] == "COMPLETED" || $membership["status"] == "CONFIRMED") {
                                            $no_access_reason = null;
                                            $no_access_message = null;
                                        }
                                    }
                                } else {
                                    $has_membership = true;
                                    $no_access_reason = null;
                                    $no_access_message = null;
                                }
                            } else {
                                if( $membership_type == "manual" ) {
                                    unset($user_memberships[$key]);
                                    update_user_meta($this->user_id, $get_memberships, $user_memberships);
                                    wpvs_do_after_delete_membership_action($this->user_id, $membership);
                                }
                            }
                        } else {
                            $has_membership = true;
                            $no_access_reason = null;
                            $no_access_message = null;
                        }
                    }
                    if( $has_membership ) {
                        break;
                    }
                }
            }
        }

        if( ! empty($post_id) ) {
            $wpvs_custom_access_filters = wpvs_custom_has_access_check_action($this->user, $membership_array, $user_memberships, $post_id);
        }

        if( ! empty($wpvs_custom_access_filters) ) {
            if( isset($wpvs_custom_access_filters['has_access']) ) {
                $has_membership = $wpvs_custom_access_filters['has_access'];
            }
            if( isset($wpvs_custom_access_filters['no_access_reason']) && ! empty($wpvs_custom_access_filters['no_access_reason']) ) {
                $no_access_reason = $wpvs_custom_access_filters['no_access_reason'];
            }
            if( isset($wpvs_custom_access_filters['message']) && !empty($wpvs_custom_access_filters['message']) ) {
                $no_access_message = $wpvs_custom_access_filters['message'];
            }
        }

        // CHECK ONETIME PAYMENTS
        if( ! $has_membership && ! empty($post_id) ) {
            $user_video_access = get_user_meta($this->user_id, 'rvs_user_video_access', true);
            if(!empty($user_video_access) && in_array($post_id, $user_video_access)) {
                $has_membership = true;
                $no_access_reason = null;
            }
        }
        return array('has_access' => $has_membership, 'reason' => $no_access_reason, 'message' => $no_access_message);
    }

    /**
    * Gets the current Stripe customers ID
    **/

    public function get_stripe_customer_id() {
        if( $this->test_mode ) {
            $customer_key = 'rvs_stripe_customer_test';
        } else {
            $customer_key = 'rvs_stripe_customer';
        }
        return get_user_meta($this->user_id, $customer_key, true);
    }

    /**
    * ADD MEMBERSHIPS TO CUSTOMER
    **/

    public function add_membership($membership) {
        $wpvs_membership_manager = new WPVS_Membership_Plan($membership['plan']);
        if( $this->test_mode ) {
            $update_memberships = 'rvs_user_memberships_test';
            $membership_role = 'wpvs_'.$wpvs_membership_manager->plan_id.'_role_test';
		} else {
            $update_memberships = 'rvs_user_memberships';
            $membership_role = 'wpvs_'.$wpvs_membership_manager->plan_id.'_role';

		}
        $user_memberships = $this->get_memberships();
        if( empty($user_memberships) ) {
            $user_memberships = array();
        }
        $current_time = current_time('timestamp', 1);
        $membership['start_time'] = $current_time;
        $user_memberships[] = $membership;
        update_user_meta( $this->user_id, $update_memberships, $user_memberships);

        if( $wpvs_membership_manager->has_role && get_role($membership_role) ) {
            if ( ! in_array( $membership_role, $this->user->roles ) ) {
                $this->user->add_role($membership_role);
            }
        }

        wpvs_send_email("New", $this->user_id, $membership['name']);
        wpvs_do_after_new_membership_action($this->user_id, $membership);
    }

    /**
    * ADD PRODUCTS TO CUSTOMER
    **/

    public function add_product($product_id, $purchase_type) {
        $user_video_access = get_user_meta($this->user_id, 'rvs_user_video_access', true);
        if(empty($user_video_access) || ! is_array($user_video_access)) {
            $user_video_access = array();
        }

        if($purchase_type == "rental") {
            $user_video_rentals = get_user_meta($this->user_id, 'rvs_user_video_rentals', true);
            if(empty($user_video_rentals)) {
                $user_video_rentals = array();
            }
            $expires = get_post_meta( $product_id, 'rvs_rental_expires', true );
            $interval = get_post_meta( $product_id, 'rvs_rental_type', true );
            $time_string = "+".$expires." ".$interval;
            $expires_date = strtotime($time_string);
            if(!empty($expires_date) && !in_array($product_id, $user_video_access)) {
                $new_rental = array('video' => $product_id, 'expires' => $expires_date);
                $user_video_rentals[] = $new_rental;
                update_user_meta($this->user_id, 'rvs_user_video_rentals', $user_video_rentals);
            }
        }

        if($purchase_type == "purchase") {
            $user_video_purchases = get_user_meta($this->user_id, 'rvs_user_video_purchases', true);
            if(empty($user_video_purchases)) {
                $user_video_purchases = array();
            }
            $new_purchase = array('video' => $product_id, 'expires' => "never");
            $user_video_purchases[] = $new_purchase;
            update_user_meta($this->user_id, 'rvs_user_video_purchases', $user_video_purchases);
        }

        if($purchase_type == "termpurchase") {
            $user_term_purchases = get_user_meta($this->user_id, 'rvs_user_term_purchases', true);
            if(empty($user_term_purchases)) {
                $user_term_purchases = array();
            }
            $user_term_purchases[] = intval($product_id);
            $user_term_purchases = array_unique($user_term_purchases);
            update_user_meta($this->user_id, 'rvs_user_term_purchases', $user_term_purchases);
        }

        if( ! empty($product_id) ) {
            $user_video_access[] = $product_id;
            $user_video_access = array_unique($user_video_access);
            update_user_meta($this->user_id, 'rvs_user_video_access', $user_video_access);
        }
        wpvs_do_after_new_product_purchase_action($this->user_id, $product_id, $purchase_type);
    }

    private function set_billing_address() {
        $wpvs_billing_address = get_user_meta( $this->user_id, 'wpvs_billing_address', true);
        $wpvs_billing_address_line_2 = get_user_meta( $this->user_id, 'wpvs_billing_address_line_2', true);
        $wpvs_billing_city = get_user_meta( $this->user_id, 'wpvs_billing_city', true);
        $wpvs_billing_state = get_user_meta( $this->user_id, 'wpvs_billing_state', true);
        $wpvs_billing_country = get_user_meta( $this->user_id, 'wpvs_billing_country', true);
        $wpvs_billing_zip_code = get_user_meta( $this->user_id, 'wpvs_billing_zip_code', true);
        $billing_details = array(
            'address' => $wpvs_billing_address,
            'address_line_2' => $wpvs_billing_address_line_2,
            'city' => $wpvs_billing_city,
            'state' => $wpvs_billing_state,
            'zip' => $wpvs_billing_zip_code,
            'country' => $wpvs_billing_country
        );
        return $billing_details;
    }

    public function missing_billing_info() {
        $has_missing_fields = false;
        $wpvs_billing_address = get_user_meta( $this->user_id, 'wpvs_billing_address', true);
        $wpvs_billing_address_line_2 = get_user_meta( $this->user_id, 'wpvs_billing_address_line_2', true);
        $wpvs_billing_city = get_user_meta( $this->user_id, 'wpvs_billing_city', true);
        $wpvs_billing_state = get_user_meta( $this->user_id, 'wpvs_billing_state', true);
        $wpvs_billing_country = get_user_meta( $this->user_id, 'wpvs_billing_country', true);
        $wpvs_billing_zip_code = get_user_meta( $this->user_id, 'wpvs_billing_zip_code', true);
        if( empty($wpvs_billing_address) || empty($wpvs_billing_city) || empty($wpvs_billing_state) || empty($wpvs_billing_country) || empty($wpvs_billing_zip_code) ) {
            $has_missing_fields = true;
        }
        return $has_missing_fields;
    }
}
