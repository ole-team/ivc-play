<?php

function wpv_key_compare_plans($membership1, $membership2) {
    $add_memberships = false;
    if(!empty($membership2)) {
        foreach($membership2 as $key => $member_plan) {
            $current_plans[] = $member_plan['plan'];
        }
    } else {
        if(!empty($membership1)) {
            // USER HAS NO MEMBERSHIPS
            foreach($membership1 as $key=> $plan) {
                $add_memberships[] = $plan;
            }
        }
    }
    if(!empty($membership1) && !empty($current_plans)) {
        foreach($membership1 as $key=> $plan) {
            if(!in_array($plan['id'], $current_plans)) {
                $add_memberships[] = $plan;
            }
        }
    }
    return $add_memberships;
}

function wpvs_get_all_members($test_mode) {
    global $wpdb;
    if($test_mode) {
        $wpvs_members_table_name = $wpdb->prefix . 'wpvs_test_members';
    } else {
        $wpvs_members_table_name = $wpdb->prefix . 'wpvs_members';
    }
    $wpvs_members = $wpdb->get_results("SELECT * FROM $wpvs_members_table_name");
    return $wpvs_members;
}

function wpvs_get_members_paged($test_mode, $offset) {
    global $wpdb;
    if($test_mode) {
        $wpvs_members_table_name = $wpdb->prefix . 'wpvs_test_members';
    } else {
        $wpvs_members_table_name = $wpdb->prefix . 'wpvs_members';
    }
    $wpvs_members = $wpdb->get_results("SELECT * FROM $wpvs_members_table_name LIMIT 50 OFFSET $offset");
    return $wpvs_members;
}

function wpvs_remove_member($test_mode, $user_id) {
    global $wpdb;
    if($test_mode) {
        $wpvs_members_table_name = $wpdb->prefix . 'wpvs_test_members';
    } else {
        $wpvs_members_table_name = $wpdb->prefix . 'wpvs_members';
    }
    $wpvs_member = $wpdb->delete($wpvs_members_table_name, array('user_id' => $user_id) );
    return $wpvs_member;
}

function wpvs_clear_membership_data($plan_id, $remove_user_plans, $remove_user_role) {

    if( $remove_user_plans ) {
        global $vs_current_user;
        $stripe_payments_manager = new WPVS_Stripe_Payments_Manager($rvs_current_user->ID);
        $wpvs_members = wpvs_get_all_members(false); // GET LIVE MEMBERS

        if( ! empty($wpvs_members) ) {
            foreach($wpvs_members as $member) {
                $user_memberships = get_user_meta($member->user_id, 'rvs_user_memberships', true);
                if( ! empty($user_memberships) ) {
                    foreach($user_memberships as $key => &$membership) {
                        if($membership["plan"] == $plan_id) {
                            if( isset($membership['type']) && $membership['type'] == "stripe" ) {
                                $subscription_id = $membership["id"];
                                $stripe_payments_manager->cancel_subscription($member->user_id, $subscription_id, 'live');
                            }

                            if( isset($membership['type']) && $membership['type'] == "paypal" ) {
                                $agreement_id = $membership["id"];
                                wpvs_cancel_users_paypal_plan($member->user_id, $agreement_id, false);
                            }
                            unset($user_memberships[$key]);
                            update_user_meta($member->user_id, 'rvs_user_memberships', $user_memberships);
                            wpvs_do_after_delete_membership_action($member->user_id, $membership);
                            if( $remove_user_role ) {
                                wpvs_remove_user_role_after_membership_delete($member->user_id, $membership);
                            }
                        }
                    }
                }
                wpvs_clear_user_membership_trial($member->user_id, $plan_id);
            }
        }

        $wpvs_test_members = wpvs_get_all_members(true); // GET TEST MEMBERS

        if( ! empty($wpvs_test_members) ) {
            foreach($wpvs_test_members as $member) {
                $user_test_memberships = get_user_meta($member->user_id, 'rvs_user_memberships_test', true);
                if( ! empty($user_test_memberships) ) {
                    foreach($user_test_memberships as $key => &$membership) {
                        if($membership["plan"] == $plan_id) {
                            if( isset($membership['type']) && $membership['type'] == "stripe_test" ) {
                                $subscription_id = $membership["id"];
                                $stripe_payments_manager->cancel_subscription($member->user_id, $subscription_id, 'test');
                            }

                            if( isset($membership['type']) && $membership['type'] == "paypal_test" ) {
                                $agreement_id = $membership["id"];
                                wpvs_cancel_users_paypal_plan($member->user_id, $agreement_id, true);
                            }
                            unset($user_test_memberships[$key]);
                            update_user_meta($member->user_id, 'rvs_user_memberships_test', $user_test_memberships);
                            wpvs_do_after_delete_membership_action($member->user_id, $membership);
                            if( $remove_user_role ) {
                                wpvs_remove_user_role_after_membership_delete($member->user_id, $membership);
                            }
                        }
                    }
                }
            }
        }
    }

    $video_post_args = array(
        'posts_per_page' => -1,
        'nopaging' => true,
        'post_type' => 'rvs_video',
        'post_status' => 'any',
        'fields' => 'ids'
    );
    $wpvs_videos = get_posts( $video_post_args ); // GET VIDEOS WITH MEMBERSHIP
    if( ! empty($wpvs_videos) ) {
        foreach($wpvs_videos as $rvs_video_id) {
            $rvs_video_membership_list = get_post_meta($rvs_video_id, '_rvs_memberships', true );
            if( ! empty($rvs_video_membership_list) ) {
                foreach($rvs_video_membership_list as $key => &$rvs_video_membership) {
                    if($rvs_video_membership == $plan_id) {
                        unset($rvs_video_membership_list[$key]);
                        break;
                    }
                }
                update_post_meta( $rvs_video_id, '_rvs_memberships', $rvs_video_membership_list );
            }
        }
    }
}

if( ! function_exists('wpvs_remove_user_role_after_membership_delete') ) {
function wpvs_remove_user_role_after_membership_delete($user_id, $membership) {
    if( isset($membership['plan']) && ! empty($membership['plan']) ) {
        $membership_role = 'wpvs_'.$membership['plan'].'_role';
        $membership_test_role = $membership_role.'_test';

        $user = get_user_by('id', $user_id);
        if ( in_array( $membership_role, $user->roles ) ) {
            $user->remove_role($membership_role);
            if( empty( $user->roles ) ) {
                $user->add_role("wpvs_member");
            }
        }
        if ( in_array( $membership_test_role, $user->roles ) ) {
            $user->remove_role($membership_test_role);
            if( empty( $user->roles ) ) {
                $user->add_role("wpvs_test_member");
            }
        }
    }
}
}
