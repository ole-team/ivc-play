<?php
$wpvs_plan_id = $_GET['id'];
global $wpvs_membership_plans_list;
global $rvs_live_mode;
if( ! empty($wpvs_membership_plans_list) ) {
    foreach($wpvs_membership_plans_list as $plan) {
        if ($plan['id'] == $wpvs_plan_id) {
            $edit_plan = $plan;
        }
    }
}
if(isset($edit_plan['trial_frequency'])) {
    $trial_frequency = $edit_plan['trial_frequency'];
} else {
    $trial_frequency = "none";
}

if(isset($edit_plan['trial'])) {
    $trial_period = $edit_plan['trial'];
} else {
    $trial_period = null;
}

if(isset($edit_plan['hidden'])) {
    $membership_hidden = $edit_plan['hidden'];
} else {
    $membership_hidden = 0;
}

if( !isset($edit_plan['short_description'])) {
    $edit_plan['short_description'] = "";
}

if( isset($edit_plan['has_role']) ) {
    $has_custom_role = $edit_plan['has_role'];
} else {
    $has_custom_role = 0;
}

if($rvs_live_mode == "off") {
    $stripe_url = 'https://dashboard.stripe.com/test/';
} else {
    $stripe_url = 'https://dashboard.stripe.com/';
}
?>
<div class="wrap">
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title">Editing <?php echo $edit_plan['name']; ?></h1>
            <a class="rvs-button rvs-create-button" href="<?php echo admin_url('admin.php?page=rvs-memberships'); ?>">Back to memberships</a>
            <div class="rvs-container">
                <div class="rvs-col-6">
                    <form class="rvs-edit-form" method="post" action="">
                        <input type="hidden" name="id_of_plan" id="id_of_plan" value="<?php echo $wpvs_plan_id; ?>" />
                        <label>Name of Plan</label>
                        <input type="text" name="name_of_plan" id="name_of_plan" value="<?php echo $edit_plan['name']; ?>" required><br />

                        <label>Description</label>
                        <textarea rows="5" cols="50" maxlength="500" name="description_of_plan" id="description_of_plan"><?php echo stripslashes($edit_plan['description']); ?></textarea><br />

                        <label>Short Description</label>
                        <textarea rows="3" cols="50" maxlength="127" name="short_description_of_plan" id="short_description_of_plan"><?php echo stripslashes($edit_plan['short_description']); ?></textarea><br />

                        <label>Trial Period</label>
                        <select id="trial_period" name="trial_period" class="rvs-admin-select">
                            <option value="none" <?php selected("none", $trial_frequency); ?>>None</option>
                            <option value="day" <?php selected("day", $trial_frequency); ?>>Days</option>
                            <option value="week" <?php selected("week", $trial_frequency); ?>>Weeks</option>
                            <option value="month" <?php selected("month", $trial_frequency); ?>>Months</option>
                        </select>
                        <input type="number" min="0" id="trial_period_days" name="trial_period_days" placeholder="Number of days"  class="<?=($trial_frequency != "none") ? 'rvs-show-trial-days' : ''?>" value="<?php echo $trial_period; ?>"/>
                        <div class="rvs-field-section">
                            <label class="rvs-label-inline">
                            <input type="checkbox" name="create_membership_role" id="create_membership_role" value="1" <?php checked($has_custom_role, 1); ?>/>
                            <?php _e('Create User Role', 'vimeo-sync-memberships') ; ?></label>
                            <p class="description"><?php _e('Creates a new user role that gets assigned to customers subscribed to this membership', 'vimeo-sync-memberships'); ?>.</p>
                        </div>
                        <div class="rvs-form-section">
                        <label class="rvs-label-inline">
                        <input type="checkbox" name="hide_membership" id="hide_membership" value="1" <?php checked($membership_hidden, 1); ?> />Hide this Membership</label>
                        </div>
                        <hr>

                        <label class="rvs-button rvs-primary-button" id="save-membership">Save</label>
                        <label class="rvs-button rvs-red-button" id="delete-membership">Delete</label>
                        <div id="wpvs-deletion-options">
                            <div class="wpvs-deletion-option">
                                <label><input type="checkbox" id="wpvs_remove_user_plans" value="1" /> Delete active customer subscriptions?</label><br>
                                <p class="description">Checking this will cancel and remove this membership from all customers currently assigned to it.</p>
                            </div>
                            <div class="wpvs-deletion-option">
                                <label><input type="checkbox" id="wpvs_remove_role" value="1" /> Delete the User Role associated with this membership?</label><br>
                                <p class="description">Checking this will delete the associated User Role with this membership.</p>
                            </div>
                            <div class="wpvs-deletion-option">
                                <label><input type="checkbox" id="wpvs_delete_stripe_plan" value="1" /> Delete Stripe Plan?</label><br>
                                <p class="description">Checking this will delete the <a href="<?php echo $stripe_url . 'plans/' . $wpvs_plan_id; ?>" target="_blank">Stripe Plan</a> associated with this membership.</p>
                            </div>
                            <label class="rvs-button" id="confirm-delete-membership">Confirm Delete</label>
                        </div>
                    </form>
                </div>
                <div class="rvs-col-6">
                    <h4><span class="dashicons dashicons-warning rvs-error"></span> Things to Note:</h4>
                    <p><strong>Deleting a plan</strong> will also remove it from Stripe and / or PayPal, and all videos currently requiring this plan.</p>
                    <p>Changing the <strong>Trial Period</strong> will require you to recreate the <strong>PayPal Billing Plan</strong> on the Memberships page. This will <strong>NOT</strong> effect users currently subscribed to this Plan. The new <strong>Trial Period</strong> will only apply to <strong>new users</strong>.</p>

                </div>
            </div>
        </div>
    </div>
</div>
