<?php 
if(isset($_GET['tab'])) {
    $current_tab = $_GET['tab'];
} else {
     $current_tab = 'renewal';
}
$cron_is_enabled =  true;
if ( defined('DISABLE_WP_CRON') && ! DISABLE_WP_CRON ) {
    $cron_is_enabled =  false;
}

?>
<div class="wrap">
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title">Reminder Settings</h1>
            <div class="rvs-container">
                <ul class="subsubsub">
                    <li><a class="<?= ($current_tab == "renewal") ? 'current': '' ?>" href="<?php echo admin_url('admin.php?page=wpvs-reminder-settings&tab=setup'); ?>"><?php _e('Renewal Reminder', 'vimeo-sync-memberships'); ?></a></li>
                </ul>
            </div>
            <div class="rvs-container rvs-box">
                <form method="post" action="options.php">
                    <?php settings_fields('wpvs-memberships-reminders'); 
                           
                    $site_name = get_bloginfo('name');
                    $admin_email = get_option('admin_email');
                    $wpvs_membership_reminder_email = get_option('wpvs_membership_reminder_email');
                    
                    if( ! isset($wpvs_membership_reminder_email['enabled']) ) {
                        $wpvs_membership_reminder_email['enabled'] = 0;
                    }
                    
                    if(!isset($wpvs_membership_reminder_email['before_count'])) {
                        $wpvs_membership_reminder_email['before_count'] = 3;
                    }
                    
                    if(!isset($wpvs_membership_reminder_email['reminder_type'])) {
                        $wpvs_membership_reminder_email['reminder_type'] = 'days';
                    }
                    
                    if(!isset($wpvs_membership_reminder_email['from_name'])) {
                        $wpvs_membership_reminder_email['from_name'] = $site_name;
                    }
                    
                    if(!isset($wpvs_membership_reminder_email['from_email'])) {
                        $wpvs_membership_reminder_email['from_email'] = $admin_email;
                    }
                    
                    if(!isset($wpvs_membership_reminder_email['notify_email'])) {
                        $wpvs_membership_reminder_email['notify_email'] = $admin_email;
                    }
                    
                    if(!isset($wpvs_membership_reminder_email['subject'])) {
                        $wpvs_membership_reminder_email['subject'] = 'Automatic Renewal Reminder - {planname} Subscription';
                    }
                    
                    if(!isset($wpvs_membership_reminder_email['coin_subject'])) {
                        $wpvs_membership_reminder_email['coin_subject'] = 'Renewal Reminder - {planname} Subscription';
                    }
                    
                    $rvs_account_page = esc_attr( get_option('rvs_account_page'));
                    $rvs_account_link = get_permalink($rvs_account_page);
                    
                    if(!isset($wpvs_membership_reminder_email['content'])) {
                        $wpvs_membership_reminder_email['content'] = '<p>This is a friendly reminder that your {planname} membership will automatically renew on <strong>{renewaldate}</strong>.</p><p>You can manage your memberships <a href="'. $rvs_account_link .'">here</a></p><p>If you have any questions, send us an email at <a href="mailto:' . $admin_email . '">'.$admin_email.'.</a></p>';
                    }
                    
                    if(!isset($wpvs_membership_reminder_email['coin_content'])) {
                        $wpvs_membership_reminder_email['coin_content'] = '<p>This is a friendly reminder that your {planname} membership expired on <strong>{expireddate}</strong>.</p><p>You have paid with crypto currency, which means you will need to renew your membership <a href="'. $rvs_account_link .'">here</a></p><p>If you have any questions, send us an email at <a href="mailto:' . $admin_email . '">'.$admin_email.'.</a></p>';
                    }
                    
                    ?>

                    <div class="<?= ($current_tab == "renewal") ? '': 'rvs-hidden-email' ?>">
                        <h3 class="title"><?php _e('Renewal Reminder Email', 'vimeo-sync-memberships'); ?></h3>
                        <table class="form-table"> 
                            <tbody>
                                <?php if( ! $cron_is_enabled) { ?>
                                <tr valign="top">	
                                    <th scope="row" valign="top">
                                        <span class="wpvs-error-text"><?php _e('Possible Error', 'vimeo-sync-memberships'); ?></span>
                                    </th>
                                    <td>
                                        
                                        <label class="description" for="wpvs_membership_reminder_email[from_name]"><em><?php _e('Make sure that CRON is enabled on your website and server', 'vimeo-sync-memberships'); ?></em>. <em><?php _e('You may need to contact your hosting company and ask if cron is enabled.', 'vimeo-sync-memberships'); ?></em><br><a href="https://99robots.com/docs/how-to-enable-wp-cron/" target="_blank">How Do I Enable Cron?</a></label>
                                    </td>
                                </tr>

                                <?php } ?>
                                <tr valign="top">	
                                    <th scope="row" valign="top">
                                        <?php _e('Send Reminders', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input type="checkbox" name="wpvs_membership_reminder_email[enabled]" value="1" <?php checked(1, $wpvs_membership_reminder_email['enabled']); ?> />
                                        <label class="description"><?php _e('Send renewal reminders', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                
                                <tr valign="top">	
                                    <th scope="row" valign="top">
                                        <?php _e('Notice Time', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <label class="description"><?php _e('Send reminders', 'vimeo-sync-memberships'); ?></label>
                                        <input type="number" min="1" max="30" name="wpvs_membership_reminder_email[before_count]" value="<?php echo $wpvs_membership_reminder_email['before_count']; ?>" />
                                        <select name="wpvs_membership_reminder_email[reminder_type]">
                                            <option value="days" <?php selected('days', $wpvs_membership_reminder_email['reminder_type']); ?>>Day(s)</option>
                                            <option value="weeks" <?php selected('weeks', $wpvs_membership_reminder_email['reminder_type']); ?>>Week(s)</option>
                                        </select>
                                        <label class="description"><?php _e('before renewal date', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                
                                <tr valign="top">	
                                    <th scope="row" valign="top">
                                        <?php _e('From Name', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="wpvs_membership_reminder_email[from_name]" name="wpvs_membership_reminder_email[from_name]" type="text" value="<?php echo $wpvs_membership_reminder_email['from_name']; ?>" />
                                        <p class="description"><?php _e('Name of sender', 'vimeo-sync-memberships'); ?></p>
                                    </td>
                                </tr>
                                <tr valign="top">	
                                    <th scope="row" valign="top">
                                        <?php _e('From Email', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="wpvs_membership_reminder_email[from_email]" name="wpvs_membership_reminder_email[from_email]" type="email" value="<?php echo $wpvs_membership_reminder_email['from_email']; ?>" />
                                        <p class="description"><?php _e('Email sent from', 'vimeo-sync-memberships'); ?></p>
                                    </td>
                                </tr>
                                <tr valign="top">	
                                    <th scope="row" valign="top">
                                        <?php _e('Notification Email', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="wpvs_membership_reminder_email[notify_email]" name="wpvs_membership_reminder_email[notify_email]" type="text" value="<?php echo $wpvs_membership_reminder_email['notify_email']; ?>" />
                                        <p class="description"><?php _e('Admin email to receive notifications', 'vimeo-sync-memberships'); ?></p>
                                    </td>
                                </tr>
                                <tr valign="top">	
                                    <th scope="row" valign="top">
                                        <?php _e('Email Subject', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="wpvs_membership_reminder_email[subject]" name="wpvs_membership_reminder_email[subject]" type="text" value="<?php echo $wpvs_membership_reminder_email['subject']; ?>" />
                                        <p class="description"><?php _e('Email Subject', 'vimeo-sync-memberships'); ?></p>
                                    </td>
                                </tr>
  
                                <tr valign="top">	
                                    <th scope="row" valign="top">
                                        <?php _e('Email Content', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <?php wp_editor( $wpvs_membership_reminder_email['content'], 'wpvs_membership_reminder_email_content', array('textarea_name' => 'wpvs_membership_reminder_email[content]', 'wpautop' => false) );
                                        ?>
                                        <h4><?php _e('Variables: These will be replaced with the reminder Membership Name and Renewal Date', 'vimeo-sync-memberships'); ?></h4>
                                        <ul>
                                            <li>{planname}</li>
                                            <li>{renewaldate}</li>
                                        </ul>

                                    </td>
                                </tr>
                                
                                <tr valign="top">	
                                    <th scope="row" valign="top">
                                        <?php _e('Crypto Reminders Email Subject', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="wpvs_membership_reminder_email[subject]" name="wpvs_membership_reminder_email[coin_subject]" type="text" value="<?php echo $wpvs_membership_reminder_email['coin_subject']; ?>" />
                                        <p class="description"><?php _e('Crypto memberships do not renew automatically.', 'vimeo-sync-memberships'); ?></p>
                                    </td>
                                </tr>
                                
                                <tr valign="top">	
                                    <th scope="row" valign="top">
                                        <?php _e('Crypto Reminders Email Content', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <?php wp_editor( $wpvs_membership_reminder_email['coin_content'], 'wpvs_membership_reminder_email_coin_content', array('textarea_name' => 'wpvs_membership_reminder_email[coin_content]', 'wpautop' => false) );
                                        ?>
                                        <h4><?php _e('Variables: These will be replaced with the reminder Membership Name and Expiration Date', 'vimeo-sync-memberships'); ?></h4>
                                        <ul>
                                            <li>{planname}</li>
                                            <li>{expireddate}</li>
                                        </ul>

                                    </td>
                                </tr>
                                
                                
                            </tbody>
                        </table>
                    </div>
                    <?php submit_button(); ?>
                </form>
        </div>
    </div>
</div>