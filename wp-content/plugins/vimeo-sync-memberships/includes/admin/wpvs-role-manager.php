<?php
/*
* WPVS ADMIN ROLE MANAGER
*/

class WPVS_Role_Manager {
    public $test_mode;

    public function __construct() {
        global $rvs_live_mode;
        if( $rvs_live_mode == "on" ) {
            $this->test_mode = false;
        } else {
            $this->test_mode = true;
        }
        add_action( 'admin_head' , array($this, 'hide_adminstrator_roles_css' ) );
        add_action( 'show_user_profile', array($this, 'admin_user_editor_roles' ) );
        add_action( 'edit_user_profile', array($this, 'admin_user_editor_roles' ) );
        add_action( 'profile_update', array($this, 'admin_user_editor_save_roles' ), 2, 10 );
    }


    public function hide_adminstrator_roles_css() {
        $current_screen = get_current_screen();
        $admin_roles_css = '<style type="text/css">';
        if( isset($current_screen->base) && $current_screen->base == 'users' ) {
            $wpvs_roles = get_option('wpvs_membership_roles', array());
            if( $this->test_mode ) {
                $admin_roles_css .= 'li.wpvs_member {display:none;}';
                if( ! empty($wpvs_roles) ) {
                    foreach($wpvs_roles as $wpvs_role) {
                        $admin_roles_css .= 'li.'.$wpvs_role.' {display:none;}';
                    }
                }
            } else {
                $admin_roles_css .= 'li.wpvs_test_member {display:none;}';
                if( ! empty($wpvs_roles) ) {
                    foreach($wpvs_roles as $wpvs_role) {
                        $test_role_id = $wpvs_role.'_test';
                        $admin_roles_css .= 'li.'.$test_role_id.' {display:none;}';
                    }
                }
            }
        }
        $admin_roles_css .= '</style>';
        echo $admin_roles_css;
    }

    public function admin_user_editor_roles( $user ) {
        wp_enqueue_script('wpvs-country-dropdown-js');
        $all_roles = get_editable_roles();
        $wpvs_billing_address = get_user_meta( $user->ID, 'wpvs_billing_address', true);
        $wpvs_billing_address_line_2 = get_user_meta( $user->ID, 'wpvs_billing_address_line_2', true);
        $wpvs_billing_city = get_user_meta( $user->ID, 'wpvs_billing_city', true);
        $wpvs_billing_state = get_user_meta( $user->ID, 'wpvs_billing_state', true);
        $wpvs_billing_country = get_user_meta( $user->ID, 'wpvs_billing_country', true);
        $wpvs_billing_zip_code = get_user_meta( $user->ID, 'wpvs_billing_zip_code', true);
    ?>
        <h3><?php _e("Billing Information", "vimeo-sync-memberships"); ?></h3>
        <table class="form-table">
            <tr>
                <th><label><?php _e("Address", "vimeo-sync-memberships"); ?></label></th>
                <td>
                    <input type="text" name="wpvs_billing_address" class="regular-text" value="<?php echo $wpvs_billing_address; ?>" />
                </td>
            </tr>
            <tr>
                <th><label><?php _e("Address Line 2", "vimeo-sync-memberships"); ?></label></th>
                <td>
                    <input type="text" name="wpvs_billing_address_line_2" class="regular-text" value="<?php echo $wpvs_billing_address_line_2; ?>" />
                </td>
            </tr>
            <tr>
                <th><label><?php _e("City", "vimeo-sync-memberships"); ?></label></th>
                <td>
                    <input type="text" name="wpvs_billing_city" class="regular-text" value="<?php echo $wpvs_billing_city; ?>" />
                </td>
            </tr>
            <tr>
                <th><label><?php _e("State", "vimeo-sync-memberships"); ?>/<?php _e("Province", "vimeo-sync-memberships"); ?></label></th>
                <td>
                    <select id="wpvs_billing_state" name="wpvs_billing_state">
                        <option value="">Select a Country</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th><label><?php _e("Zip/Postal Code", "vimeo-sync-memberships"); ?></label></th>
                <td>
                    <input type="text" name="wpvs_billing_zip_code" class="regular-text" value="<?php echo $wpvs_billing_zip_code; ?>" />
                </td>
            </tr>
            <tr>
                <th><label><?php _e("Country", "vimeo-sync-memberships"); ?></label></th>
                <td>
                    <select id="wpvs_billing_country" name="wpvs_billing_country">
                        <?php include(RVS_MEMBERS_BASE_DIR.'/template/country-list.php'); ?>
                    </select>
                </td>
            </tr>
        </table>

        <h3><?php _e("Other Roles", "vimeo-sync-memberships"); ?></h3>
        <table class="form-table">
        <tr>
            <th><label><?php _e("Select", "vimeo-sync-memberships"); ?></label></th>
            <td>
                <?php if( ! empty($all_roles) ) {
                    foreach($all_roles as $key => $role) { ?>
                        <input type="checkbox" name="wpvs_other_user_roles[]" value="<?php echo $key; ?>" <?php checked(true, in_array($key, $user->roles)); ?> />
                        <span><?php echo $role['name']; ?></span>
                        <br><br>
                  <?php } }  ?>
            </td>
        </tr>
        </table>
    <?php }

    public function admin_user_editor_save_roles( $user_id, $old_user_data ) {
        if( current_user_can( 'edit_user', $user_id ) ) {
            if( isset($_POST['wpvs_other_user_roles']) ) {
                $other_user_roles = $_POST['wpvs_other_user_roles'];
                $user = get_user_by('id', $user_id);
                foreach($other_user_roles as $other_role) {
                    if ( ! in_array( $other_role, $user->roles ) ) {
                        $user->add_role($other_role);
                    }
                }
            }

            if( isset($_POST['wpvs_billing_address']) ) {
                update_user_meta( $user_id, 'wpvs_billing_address', sanitize_text_field($_POST['wpvs_billing_address']));
            }
            if( isset($_POST['wpvs_billing_address_line_2']) ) {
                update_user_meta( $user_id, 'wpvs_billing_address_line_2', sanitize_text_field($_POST['wpvs_billing_address_line_2']));
            }
            if( isset($_POST['wpvs_billing_city']) ) {
                update_user_meta( $user_id, 'wpvs_billing_city', sanitize_text_field($_POST['wpvs_billing_city']));
            }
            if( isset($_POST['wpvs_billing_state']) ) {
                update_user_meta( $user_id, 'wpvs_billing_state', sanitize_text_field($_POST['wpvs_billing_state']));
            }
            if( isset($_POST['wpvs_billing_state']) ) {
                update_user_meta( $user_id, 'wpvs_billing_zip_code', sanitize_text_field($_POST['wpvs_billing_zip_code']));
            }
            if( isset($_POST['wpvs_billing_country']) ) {
                update_user_meta( $user_id, 'wpvs_billing_country', sanitize_text_field($_POST['wpvs_billing_country']));
            }
        }
    }
}

$wpvs_role_manager = new WPVS_Role_Manager();
