<?php
global $rvs_current_version;
$wpvs_memberships_updates = get_option('wpvs-memberships-updated');
$update_needed = false;
if(!empty($wpvs_memberships_updates)) {
    $wpvs_updates_version = intval(str_replace(".","",$wpvs_memberships_updates));
    if($wpvs_updates_version < 305) {
        $update_needed = true;
    }
    if($wpvs_updates_version < 307 || ( ! wpvs_members_table_exists() ) ) {
        $update_needed = true;
    }
    if($wpvs_updates_version < 348 ) {
        $update_needed = true;
    }

    if($wpvs_updates_version < 362 ) {
        $update_needed = true;
    }

    if($wpvs_updates_version < 363 ) {
        $update_needed = true;
    }
    if($wpvs_updates_version < 367 ) {
        $update_needed = true;
    }
    if($wpvs_updates_version < 412 ) {
        $update_needed = true;
    }
    if($wpvs_updates_version < 419 ) {
        $update_needed = true;
    }
    if($wpvs_updates_version < 428 ) {
        $update_needed = true;
    }
}

?>
<div class="wrap">
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title">Database Updates</h1>
            <p>This page will display any updates that are required by the WP Video Memberships plugin.</p>
            <div class="wpvs-database-updates">
                <h3>Updates:</h3>
                <?php if($update_needed) :
                     if($wpvs_updates_version < 100) : ?>
                        <div class="wpvs-database-update">
                            <h4>Payments Update</h4>
                            <p>This update will add a UNIQUE key to payments in your database to prevent duplicate payments. It will also remove any duplicate payments if they exist.</p>
                            <p class="rvs-error"><strong>We recommend you backup your database before doing any of these updates.</strong></p>
                            <label class="button button-primary wpvs-run-update">Run Update</label>
                            <label class="rvs-success wpvs-updating-icon"><span class="dashicons dashicons-update wpvs-update-spin"></span> Updating</label>
                            <div class="wpvs-update-errors"><p class="rvs-error"></p></div>
                        </div>
                <?php endif; if($wpvs_updates_version < 305) : ?>
                    <div class="wpvs-database-update">
                        <h4>Stripe API Update (2018-02-06)</h4>
                        <p>Please update your Stripe API Version to the latest version within your Stripe Dashboard.</p>
                        <a href="https://dashboard.stripe.com/developers" class="button button-primary" target="_blank">Update Your API Here</a>
                    </div>
                <?php endif; if($wpvs_updates_version < 307 || ! wpvs_members_table_exists()) :  ?>
                    <div class="wpvs-database-update">
                        <h4>Members Database Update (IMPORTANT)</h4>
                        <p>This update will create new Member database tables resulting in a faster and improved Members management system.</p>
                        <p class="rvs-error"><strong>We recommend you backup your database before running this update.</strong></p>
                        <label class="button button-primary wpvs-run-update">Run Update</label>
                        <label class="rvs-success wpvs-updating-icon"><span class="dashicons dashicons-update wpvs-update-spin"></span> Updating</label>
                        <div class="wpvs-update-errors"><p class="rvs-error"></p></div>
                    </div>
                <?php endif; if($wpvs_updates_version < 348 ) :  ?>
                    <div class="wpvs-database-update">
                        <h4>Stripe API Update (2018-07-27)</h4>
                        <p>Please update your Stripe API Version to the latest version within your Stripe Dashboard.</p>
                        <a href="https://dashboard.stripe.com/developers" class="button button-primary" target="_blank">Update Your API Here</a>
                    </div>
                <?php update_option('wpvs-memberships-updated', $rvs_current_version); endif;
                    if($wpvs_updates_version < 362 ) :  ?>
                    <div class="wpvs-database-update">
                        <h4>Stripe Country Code</h4>
                        <p>Please set your Stripe Country Code on the <a href="<?php echo admin_url('admin.php?page=rvs-payment-settings'); ?>">Payment Gateway</a> page.</p>
                        <a href="<?php echo admin_url('admin.php?page=rvs-payment-settings'); ?>" class="button button-primary">Set Country Code</a>
                    </div>
                <?php update_option('wpvs-memberships-updated', $rvs_current_version); endif;
                    if($wpvs_updates_version < 363 ) :  ?>
                    <div class="wpvs-database-update">
                        <h4>Payments Database Update (IMPORTANT)</h4>
                        <p>This update will add additional columns to the Payments table in your database.</p>
                        <p class="rvs-error"><strong>We recommend you backup your database before running this update.</strong></p>
                        <label class="button button-primary wpvs-run-update">Run Update</label>
                        <label class="rvs-success wpvs-updating-icon"><span class="dashicons dashicons-update wpvs-update-spin"></span> Updating</label>
                        <div class="wpvs-update-errors"><p class="rvs-error"></p></div>
                    </div>
                <?php endif; if($wpvs_updates_version < 367) : ?>
                        <div class="wpvs-database-update">
                            <h4>Members Role Update</h4>
                            <p>This update will create a new role for Members and TEST Members in your database.</p>
                            <p class="rvs-error"><strong>We recommend you backup your database before doing any of these updates.</strong></p>
                            <label class="button button-primary wpvs-run-update">Run Update</label>
                            <label class="rvs-success wpvs-updating-icon"><span class="dashicons dashicons-update wpvs-update-spin"></span> Updating</label>
                            <div class="wpvs-update-errors"><p class="rvs-error"></p></div>
                        </div>
                <?php endif; if($wpvs_updates_version < 403) : ?>
                    <div class="wpvs-database-update">
                        <h4>Stripe API Update (2018-11-08)</h4>
                        <p>Please update your Stripe API Version to the latest version within your Stripe Dashboard.</p>
                        <a href="https://dashboard.stripe.com/developers" class="button button-primary" target="_blank">Update Your API Here</a>
                    </div>
                <?php endif; if($wpvs_updates_version < 412) : ?>
                    <div class="wpvs-database-update">
                        <h4>Payments Database Update</h4>
                        <p>This update will update the payments database tables to allow for improved coupon code tracking.</p>
                        <p class="rvs-error"><strong>We recommend you backup your database before doing any of these updates.</strong></p>
                        <label class="button button-primary wpvs-run-update">Run Update</label>
                        <label class="rvs-success wpvs-updating-icon"><span class="dashicons dashicons-update wpvs-update-spin"></span> Updating</label>
                        <div class="wpvs-update-errors"><p class="rvs-error"></p></div>
                    </div>
                <?php endif; if($wpvs_updates_version < 428) : ?>
                    <div class="wpvs-database-update">
                        <h4>Stripe API Update (2019-05-16)</h4>
                        <p>Please update your Stripe API Version to the latest version within your Stripe Dashboard.</p>
                        <a href="https://dashboard.stripe.com/developers" class="button button-primary" target="_blank">Update Your API Here</a>
                    </div>
                <?php update_option('wpvs-memberships-updated', $rvs_current_version); endif;
                else : ?>
                <h2 class="rvs-success">You're all up to date!</h2>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
