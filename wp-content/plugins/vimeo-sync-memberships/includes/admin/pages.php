<?php

function wpvs_ajax_url_variables() {
    global $rvs_message_array;?>
    <script type="text/javascript">
        var rvsrequests = <?php echo json_encode( array('ajax' => admin_url( "admin-ajax.php" ), 'rvsmessage' => $rvs_message_array, 'adminurl' => admin_url() )); ?>;
    </script>
<?php }
add_action ( 'admin_head', 'wpvs_ajax_url_variables' );

if( !function_exists('register_vimeo_sync_membership_settings')) {
    function register_vimeo_sync_membership_settings() {
        global $rvs_message_array;
        global $rvs_current_version;
        global $rvs_currency;
        wp_register_style('wpvs-date-picker-admin', RVS_MEMBERS_BASE_URL . 'css/admin/wpvs-admin-date-picker.css', '', $rvs_current_version);
        wp_register_style('wpvs-admin-members', RVS_MEMBERS_BASE_URL . 'css/admin/admin-members.css', '', $rvs_current_version);
        wp_register_style('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css');
        wp_register_script( 'wpvs-admin-updates', RVS_MEMBERS_BASE_URL .'js/admin/updates.js', array('jquery'), $rvs_current_version, true);
        wp_enqueue_style( 'vimeosync-members-admin', RVS_MEMBERS_BASE_URL . 'css/admin.css', '', $rvs_current_version);
        wp_enqueue_style('rvs-members-styling', RVS_MEMBERS_BASE_URL . 'css/rvs-membership-styles.css', '', $rvs_current_version);
        wp_enqueue_script('jquery');
        wp_enqueue_script( 'rvs-memberships-js', RVS_MEMBERS_BASE_URL .'js/rvs-membership.js', array('jquery'), $rvs_current_version, true);
        wp_enqueue_script( 'rvs-admin-memberships', RVS_MEMBERS_BASE_URL .'js/rvs-memberships-admin.js', array('rvs-memberships-js'), $rvs_current_version, true);
        wp_localize_script('rvs-admin-memberships', 'wpvsadmindata', array(
            'ajax' => admin_url('admin-ajax.php'),
            'currency' => $rvs_currency,
            'rvsmessage' => array(
                'payments' => __('Getting Payments','vimeo-sync-memberships'),
                'refunded' => __('Refunded','vimeo-sync-memberships'),
                'paid' => __('Paid','vimeo-sync-memberships'),
                'nomorepayments' => __('No More Payments','vimeo-sync-memberships'),
                'cancelpaypal' => __('Are you sure you want to cancel this membership?','vimeo-sync-memberships'),
                'deletepaypal' => __('Are you sure you want to delete this membership?','vimeo-sync-memberships'),
                'cancellingpp' => __('Cancelling Membership','vimeo-sync-memberships'),
                'reactivate' => __('Re-activating Membership','vimeo-sync-memberships'),
                'suspend' => __('Pausing Membership','vimeo-sync-memberships'),
                'delete' => __('Deleting Membership','vimeo-sync-memberships')
            ),
            'adminurl' => admin_url()
        ));
        wp_enqueue_script( 'wpvs-stripe-js', RVS_MEMBERS_BASE_URL .'js/wpvs-stripe.js', array('rvs-memberships-js'), $rvs_current_version, true);
        register_setting( 'vimeo-sync-membership-auth', 'rvs_auth_key', 'rvs_sanitize_license' );
        register_setting( 'wpvs-membership-settings', 'rvs_sign_up_page' );
        register_setting( 'wpvs-membership-settings', 'rvs_payment_page' );
        register_setting( 'wpvs-membership-settings', 'rvs_account_page' );
        register_setting( 'wpvs-membership-settings', 'rvs_create_account_page' );
        register_setting( 'wpvs-membership-settings', 'rvs_user_rental_page' );
        register_setting( 'wpvs-membership-settings', 'rvs_user_purchase_page' );
        register_setting( 'wpvs-membership-settings', 'rvs_redirect_page' );
        register_setting( 'wpvs-membership-settings', 'rvs_redirect_login' );
        register_setting( 'wpvs-membership-settings', 'rvs_redirect_signup_link' );
        register_setting( 'wpvs-membership-settings', 'rvs_redirect_login_link' );
        register_setting( 'wpvs-membership-settings', 'rvs_primary_color' );
        register_setting( 'wpvs-membership-settings', 'rvs_delete_color' );
        register_setting( 'wpvs-membership-settings', 'rvs_card_form_background' );
        register_setting( 'wpvs-membership-settings', 'rvs_card_form_color' );
        register_setting( 'wpvs-membership-settings', 'rvs_admin_access' );

        register_setting( 'wpvs-membership-settings', 'wpvs_login_restrictions', array(
            'type' => 'array',
            'default' => array(
                'enabled' => 0,
                'login_limit' => 1,
                'login_logic' => 'allow'
            )
        ));

        register_setting( 'wpvs-membership-settings', 'rvs_usernames_allowed' );
        register_setting( 'wpvs-membership-settings', 'wpvs_signup_require_first_last_name' );
        register_setting( 'wpvs-membership-settings', 'wpvs_signup_create_password' );
        register_setting( 'wpvs-membership-settings', 'rvs_overdue_access' );
        register_setting( 'wpvs-membership-settings', 'rvs_account_sub_menu' );
        register_setting( 'wpvs-membership-settings', 'wpvs_require_user_login' );
        register_setting( 'wpvs-membership-settings', 'wpvs_require_login_page' );
        register_setting( 'wpvs-membership-settings', 'wpvs_site_lock_redirect_logged_in_users_page' );
        register_setting( 'wpvs-membership-settings', 'wpvs_landing_page' );
        register_setting( 'wpvs-membership-settings', 'wpvs_site_lock_required_memberships' );
        register_setting( 'wpvs-membership-settings', 'wpvs_default_account_menu_item' );
        register_setting( 'wpvs-membership-settings', 'wpvs_users_can_delete_memberships' );
        register_setting( 'wpvs-membership-stripe-settings', 'wpvs_stripe_settings');
        register_setting( 'wpvs-membership-stripe-settings', 'rvs_card_button_image' );
        register_setting( 'wpvs-membership-stripe-settings', 'rvs_card_button_background' );
        register_setting( 'wpvs-membership-stripe-settings', 'wpvs_stripe_api_version' );
        register_setting( 'wpvs-membership-paypal-settings', 'rvs_paypal_settings');
        register_setting( 'wpvs-membership-paypal-settings', 'rvs_paypal_max_attempts');
        register_setting( 'wpvs-memberships-coingate', 'wpvs_coingate_settings');
        register_setting( 'wpvs-membership-email-settings', 'rvs_new_account_email');
        register_setting( 'wpvs-membership-email-settings', 'rvs_new_membership_email');
        register_setting( 'wpvs-membership-email-settings', 'rvs_cancel_membership_email');
        register_setting( 'wpvs-membership-email-settings', 'rvs_delete_membership_email');
        register_setting( 'wpvs-membership-email-settings', 'rvs_reactivate_membership_email');
        register_setting( 'wpvs-membership-email-settings', 'rvs_email_logo');
        register_setting( 'wpvs-membership-email-settings', 'wpvs_smtp_settings');
        register_setting( 'wpvs-membership-email-settings', 'wpvs_show_powered_by_email');
        register_setting( 'wpvs-memberships-reminders', 'wpvs_membership_reminder_email');
        register_setting( 'wpvs-checkout-settings', 'wpvs_require_billing_information');
        register_setting( 'wpvs-checkout-settings', 'wpvs_google_recaptcha_settings');
        register_setting( 'wpvs-checkout-settings', 'wpvs_registration_agreement_settings');
        register_setting( 'wpvs-memberships-coinbase', 'wpvs_coinbase_settings');
    }
}
add_action( 'admin_init', 'register_vimeo_sync_membership_settings' );

function rvs_membership_menu_items() {
    $wpvs_parent_admin_page = 'vimeo-sync';
    if( get_option('wpvs_theme_active') ) {
        $wpvs_parent_admin_page = 'wpvs-theme-video-settings';
    }
    add_submenu_page( $wpvs_parent_admin_page, 'Membership Settings', 'Membership Settings', 'manage_options', 'rvs-settings', 'wpvs_admin_membership_settings' );
    add_submenu_page( $wpvs_parent_admin_page, 'Checkout Settings', 'Checkout Settings', 'manage_options', 'wpvs-checkout-settings', 'wpvs_admin_checkout_settings' );
    add_submenu_page( $wpvs_parent_admin_page, 'Memberships', 'Memberships', 'manage_options', 'rvs-memberships', 'wpvs_membership_settings' );
    add_submenu_page( $wpvs_parent_admin_page, 'Members', 'Members', 'manage_options', 'rvs-members', 'wpvs_admin_members_overview' );
    add_submenu_page( $wpvs_parent_admin_page, 'Payment Gateway', 'Payment Gateway', 'manage_options', 'rvs-payment-settings', 'wpvs_admin_payment_setup' );
    add_submenu_page( $wpvs_parent_admin_page, 'Payments', 'Payments', 'manage_options', 'rvs-payments', 'wpvs_admin_payments' );
    add_submenu_page( $wpvs_parent_admin_page, 'Tax Rates', 'Tax Rates', 'manage_options', 'wpvs-tax-rate-settings', 'wpvs_admin_tax_rate_settings' );
    add_submenu_page( $wpvs_parent_admin_page, 'Email', 'Email', 'manage_options', 'rvs-email-settings', 'wpvs_admin_email_settings' );
    add_submenu_page( $wpvs_parent_admin_page, 'Reminders', 'Reminders', 'manage_options', 'wpvs-reminder-settings', 'wpvs_reminder_settings' );
    add_submenu_page( $wpvs_parent_admin_page, 'Coupons', 'Coupons', 'manage_options', 'rvs-coupon-codes', 'wpvs_admin_coupon_codes' );
    add_submenu_page( null, 'New Plan', 'New Plan', 'manage_options', 'rvs-new-membership', 'wpvs_admin_new_membership' );
    add_submenu_page( null, 'Edit Plan', 'Edit Plan', 'manage_options', 'rvs-edit-membership', 'wpvs_admin_edit_membership' );
    add_submenu_page( null, 'Edit Member', 'Edit Member', 'manage_options', 'rvs-edit-member', 'wpvs_admin_edit_member' );
    add_submenu_page( null, 'New Coupon Code', 'New Coupon Code', 'manage_options', 'rvs-new-coupon', 'wpvs_admin_new_coupon' );
    add_submenu_page( null, 'New Tax Rate', 'New Tax Rate', 'manage_options', 'wpvs-add-new-tax-rate', 'wpvs_admin_add_new_tax_rate' );
    add_submenu_page( $wpvs_parent_admin_page, 'Webhooks', 'Webhooks', 'manage_options', 'rvs-webhooks', 'wpvs_admin_webhook_setup' );
    add_submenu_page( $wpvs_parent_admin_page, 'Run Updates', 'Run Updates', 'manage_options', 'wpvs-updates', 'wpvs_run_updates' );
    add_submenu_page( null, 'Bulk Members', 'Bulk Members', 'manage_options', 'wpvs-bulk-members-editor', 'wpvs_bulk_members_admin_editor' );
}
add_action( 'rvs_add_membership_menu_items', 'rvs_membership_menu_items' );

if( !function_exists('wpvs_admin_payment_setup')) {
    function wpvs_admin_payment_setup() {
        global $rvs_current_version;
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script('wp-color-picker' );
        wp_enqueue_media();
        wp_enqueue_script( 'rvs-image-upload', RVS_MEMBERS_BASE_URL . 'js/rvs-image-upload.js', array('rvs-memberships-js'), $rvs_current_version, true);
        wp_localize_script('rvs-image-upload', 'wpvsmedia', array(
                'defaultimage' => RVS_MEMBERS_BASE_URL . 'image/stripe.png'
            )
        );
        wp_enqueue_script( 'rvs-admin-gateways', RVS_MEMBERS_BASE_URL . 'js/rvs-gateways-admin.js', array('rvs-memberships-js'), $rvs_current_version, true);
        require_once('payment-settings.php');
    }
}

if( !function_exists('wpvs_membership_settings')) {
    function wpvs_membership_settings() {
        require_once('rvs-memberships.php');
    }
}

if( !function_exists('wpvs_admin_checkout_settings')) {
    function wpvs_admin_checkout_settings() {
        require_once('wpvs-checkout-settings.php');
    }
}
if( ! function_exists('wpvs_admin_tax_rate_settings')) {
    function wpvs_admin_tax_rate_settings() {
        wp_enqueue_script('wpvs-admin-tax-rates-js');
        require_once('tax-rates.php');
    }
}

if( ! function_exists('wpvs_admin_add_new_tax_rate')) {
    function wpvs_admin_add_new_tax_rate() {
        wp_enqueue_script('wpvs-admin-tax-rates-js');
        require_once('new-tax-rate.php');
    }
}

if( !function_exists('wpvs_admin_email_settings')) {
    function wpvs_admin_email_settings() {
        global $rvs_current_version;
        require_once('email-settings.php');
        wp_enqueue_media();
        wp_enqueue_script( 'rvs-image-upload', RVS_MEMBERS_BASE_URL . 'js/rvs-image-upload.js', array('rvs-memberships-js'), $rvs_current_version, true);
        wp_localize_script('rvs-image-upload', 'wpvsmedia', array(
                'defaultimage' => RVS_MEMBERS_BASE_URL . 'image/stripe.png'
            )
        );
        wp_enqueue_script( 'wpvs-admin-email-settings');
    }
}

if( !function_exists('wpvs_reminder_settings')) {
    function wpvs_reminder_settings() {
        global $rvs_current_version;
        require_once('reminder-settings.php');
    }
}

if( !function_exists('wpvs_admin_coupon_codes')) {
    function wpvs_admin_coupon_codes() {
        global $rvs_current_version;
        wp_enqueue_script( 'rvs-admin-coupons', RVS_MEMBERS_BASE_URL . 'js/rvs-admin-coupons.js', array('rvs-memberships-js'), $rvs_current_version, true);
        require_once('coupon-codes.php');
    }
}

if( !function_exists('wpvs_admin_new_coupon')) {
    function wpvs_admin_new_coupon() {
        global $rvs_current_version;
        wp_enqueue_script( 'rvs-admin-coupons', RVS_MEMBERS_BASE_URL . 'js/rvs-admin-coupons.js', array('rvs-memberships-js'), $rvs_current_version, true);
        require_once('new-coupon-code.php');
    }
}

if( !function_exists('wpvs_admin_members_overview')) {
    function wpvs_admin_members_overview() {
        global $rvs_current_version;
        wp_enqueue_style( 'wpvs-admin-members' );
        wp_enqueue_script('jquery');
        wp_enqueue_script('stripe', 'https://js.stripe.com/v3/');
        wp_enqueue_script( 'rvs-admin-edit-members' );
        require_once('rvs-members.php');
    }
}
if( !function_exists('wpvs_admin_payments')) {
    function wpvs_admin_payments() {
        global $rvs_current_version;
        wp_enqueue_style('wpvs-admin-payments');
        wp_enqueue_script( 'wpvs-admin-payment-functions' );
        wp_enqueue_script( 'rvs-payments-admin', RVS_MEMBERS_BASE_URL .'js/rvs-payments-admin.js', array('rvs-memberships-js', 'wpvs-admin-payment-functions'), $rvs_current_version, true);
        require_once('rvs-payments.php');
    }
}

if( !function_exists('wpvs_admin_new_membership')) {
    function wpvs_admin_new_membership() {
        require_once('rvs-new-membership.php');
    }
}
if( !function_exists('wpvs_admin_edit_membership')) {
    function wpvs_admin_edit_membership() {
        global $rvs_current_version;
        require_once('rvs-edit-membership.php');
    }
}

if( !function_exists('wpvs_admin_edit_member')) {
    function wpvs_admin_edit_member() {
        global $rvs_current_version;
        wp_enqueue_style('jquery-ui');
        wp_enqueue_style('wpvs-date-picker-admin');
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script( 'wpvs-admin-payment-functions' );
        wp_enqueue_script( 'rvs-admin-member');
        require_once('rvs-edit-member.php');
    }
}

if( !function_exists('wpvs_admin_membership_settings')) {
    function wpvs_admin_membership_settings() {
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script('wp-color-picker' );
        require_once('rvs-settings.php');
    }
}

if( !function_exists('wpvs_admin_webhook_setup')) {
    function wpvs_admin_webhook_setup() {
        global $rvs_current_version;
        wp_enqueue_script( 'wpvs-webhooks-admin' );
        wp_localize_script('wpvs-webhooks-admin', 'wpvswebh', array(
            'ajax' => admin_url('admin-ajax.php')
        )
        );
        require_once('webhook-setup.php');
    }
}

if( !function_exists('wpvs_run_updates')) {
    function wpvs_run_updates() {
        global $rvs_current_version;
        wp_enqueue_script( 'wpvs-admin-updates');
        require_once('run-updates.php');
    }
}

if( !function_exists('wpvs_bulk_members_admin_editor')) {
    function wpvs_bulk_members_admin_editor() {
        global $rvs_current_version;
        wp_enqueue_style( 'wpvs-admin-members' );
        wp_enqueue_style('wpvs-date-picker-admin');
        wp_enqueue_script('jquery');
        wp_enqueue_style('jquery-ui');
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_script('stripe', 'https://js.stripe.com/v3/');
        wp_enqueue_script( 'wpvs-admin-bulk-edit-members' );
        require_once('wpvs-bulk-members.php');
    }
}
