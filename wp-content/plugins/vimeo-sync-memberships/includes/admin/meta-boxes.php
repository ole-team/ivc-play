<?php
function rvs_membership_for_video() {
    $wpvs_require_user_login = get_option('wpvs_require_user_login', 0);

    add_meta_box(
        'rvs_membership_section',
        __( 'Access Options', 'vimeo-sync-memberships' ),
        'rvs_select_membership_callback',
        array( 'rvs_video' ),
        'normal','high'
    );

    if($wpvs_require_user_login) {
        add_meta_box(
            'wpvs_page_site_lock_options',
            __( 'Site Lock Options', 'vimeo-sync-memberships' ),
            'wpvs_site_lock_options_callback',
            array('rvs_video', 'page', 'post'),
            'side','high'
        );
    }
}
add_action( 'add_meta_boxes', 'rvs_membership_for_video' );
function rvs_select_membership_callback( $post ) {
	wp_nonce_field( 'rvs_video_membership_save', 'rvs_video_membership_save_nonce' );
	$video_memberships = get_post_meta( $post->ID, '_rvs_memberships', true );
    $wpvs_free_for_users = get_post_meta( $post->ID, '_rvs_membership_users_free', true );
    $video_onetime_price = get_post_meta( $post->ID, '_rvs_onetime_price', true );
    $video_rental_price = get_post_meta( $post->ID, 'rvs_rental_price', true );
    $video_rental_expires = get_post_meta( $post->ID, 'rvs_rental_expires', true );
    $video_rental_type = get_post_meta( $post->ID, 'rvs_rental_type', true );
    $video_download_link = get_post_meta( $post->ID, 'rvs_video_download_link', true );
    $members_can_download = get_post_meta( $post->ID, 'wpvs_members_can_download', true );
    $download_link_text = get_post_meta( $post->ID, 'wpvs_download_link_text', true );
    $main_video_type = get_post_meta( $post->ID, '_rvs_video_type', true );
    $vimeo_id = get_post_meta($post->ID, 'rvs_video_post_vimeo_id', true);
    $video_required_download_memberships = get_post_meta($post->ID, 'wpvs_required_download_memberships', true);
    $video_restricted_content = get_post_meta($post->ID, 'wpvs_restricted_video_content', true);

    if(empty($video_memberships)) {
        $video_memberships = array();
    }
    if(empty($video_onetime_price)) {
        $video_onetime_price = "";
    } else {
        $video_onetime_price = number_format($video_onetime_price/100,2);
    }
    if(empty($video_rental_price)) {
        $$video_rental_price = "";
    } else {
        $video_rental_price = number_format($video_rental_price/100,2);
    }
    if( empty($video_download_link) ) {
        $video_download_link = "";
    }
    if( empty($members_can_download) ) {
        $members_can_download = 0;
    }
    if( empty($download_link_text) ) {
        $download_link_text = __('Download', 'vimeo-sync-memberships');
    }
    if( empty($video_required_download_memberships) ) {
        $video_required_download_memberships = array();
    }

    if( empty($video_restricted_content) ) {
        $video_restricted_content = 'video';
    }


    $membershipList = get_option('rvs_membership_list');
    ?>
    <div class="rvs-container">
        <div class="col-12 wpvs-meta-title">
            <h2><?php _e( 'Pricing', 'vimeo-sync-memberships' ); ?></h2>
        </div>
    </div>
    <div class="rvs-container">
        <div class="col-4">
            <h4><?php _e( 'Membership Access', 'vimeo-sync-memberships' ); ?></h4>
            <?php if(!empty($membershipList)) {
                foreach($membershipList as $plan) { ?>
                    <label class="selectit"><input type="checkbox" value="<?php echo $plan['id']; ?>" <?php checked(in_array($plan['id'], $video_memberships)); ?>  name="wpvs_selected_memberships[]"><?php echo $plan['name']; ?></label><br/>
                <?php
                }
            } ?>
            <label for="rvs_membership_users_free" class="selectit"><input type="checkbox" id="rvs_membership_users_free" name="rvs_membership_users_free" value="1" <?php checked("1", $wpvs_free_for_users); ?>>Free for Registered Users</label>
            <p><?php _e('Users subscribed to checked memberships will have access to the video.', 'vimeo-sync-memberships'); ?></p>
        </div>
        <div class="col-4">
            <h4><label for="rvs_membership_onetime"><?php _e( 'Purchase Price', 'vimeo-sync-memberships' ); ?>:</label></h4>
            <input type="text" id="rvs_membership_onetime" name="rvs_membership_onetime" placeholder="14.95" value="<?php echo $video_onetime_price; ?>">
            <p><?php _e('(Optional) Users will get access to this video if they purchase it.', 'vimeo-sync-memberships'); ?></p>
            <p><?php _e('Leave Purchase Price blank to disable.', 'vimeo-sync-memberships'); ?></p>
        </div>
        <div class="col-4">
            <h4><label for="rvs_rental_price"><?php _e( 'Rental Price', 'vimeo-sync-memberships' ); ?>:</label></h4>
            <input type="text" id="rvs_rental_price" name="rvs_rental_price" placeholder="4.95" value="<?php echo $video_rental_price; ?>">
            <h4><label for="rvs_rental_expires"><?php _e( 'Rental Expires In', 'vimeo-sync-memberships' ); ?>:</label></h4>
            <input type="number" id="rvs_rental_expires" name="rvs_rental_expires" placeholder="48" value="<?php echo $video_rental_expires; ?>">
            <select name="rvs_rental_type">
                <option value="hours" <?php selected('hours', $video_rental_type); ?>>Hours</option>
                <option value="days" <?php selected('days', $video_rental_type); ?>>Days</option>
                <option value="weeks" <?php selected('weeks', $video_rental_type); ?>>Weeks</option>
                <option value="months" <?php selected('months', $video_rental_type); ?>>Months</option>
            </select>
            <p><?php _e('(Optional) Users will get access to the video for the rental time set.', 'vimeo-sync-memberships'); ?></p>
            <p><?php _e('Leave Rental Price blank to disable.', 'vimeo-sync-memberships'); ?></p>
        </div>
    </div>

    <div class="rvs-container">
        <div class="col-12 wpvs-meta-title">
            <h2><?php _e( 'Content', 'vimeo-sync-memberships' ); ?></h2>
        </div>
    </div>
    <div class="rvs-container">
        <div class="col-4">
            <h4><?php _e( 'Restricted Content', 'vimeo-sync-memberships' ); ?></h4>
            <select name="wpvs_restricted_video_content">
                <option value="video" <?php selected('video', $video_restricted_content); ?>><?php _e('Video Only', 'vimeo-sync-memberships'); ?></option>
                <option value="videocontent" <?php selected('videocontent', $video_restricted_content); ?>><?php _e('Video and Content', 'vimeo-sync-memberships'); ?></option>
            </select>
            <p><?php _e('Which part of the video page should be restricted', 'vimeo-sync-memberships'); ?>.</p>
        </div>
    </div>

    <div class="rvs-container">
        <div class="col-12 wpvs-meta-title">
            <h2><?php _e( 'Download Options', 'vimeo-sync-memberships' ); ?></h2>
        </div>
    </div>
    <div class="rvs-container">
        <div class="col-4">
            <h4><?php _e( 'Members Can Download Video', 'vimeo-sync-memberships' ); ?>:</h4>
            <label class="selectit"><input type="checkbox" value="1" <?php checked(1, $members_can_download); ?>  name="wpvs_members_can_download"><?php _e( 'Enabled', 'vimeo-sync-memberships' ); ?></label><br/>
            <p><?php _e( 'Customers with membership access can download the video file', 'vimeo-sync-memberships' ); ?>.</p>
            <p><em>Requires a <strong>Download File Link</strong></em>.</p>
            <h4><?php _e( 'Specific Memberships Only', 'vimeo-sync-memberships' ); ?></h4>
            <p><?php _e( 'Restrict download to specific membership(s)', 'vimeo-sync-memberships' ); ?>.</p>
            <div id="wpvs-required-download-memberships">
                <?php if( ! empty($membershipList) && ! empty($video_memberships) ) {
                    foreach($membershipList as $plan) {
                        if( in_array($plan['id'], $video_memberships) ) { ?>
                            <div class="wpvs-required-download-membership">
                            <label class="selectit"><input type="checkbox" value="<?php echo $plan['id']; ?>" <?php checked(in_array($plan['id'], $video_required_download_memberships)); ?>  name="wpvs_required_download_memberships[]"><?php echo $plan['name']; ?></label>
                            </div>
                        <?php
                        }
                    }
                } else { ?>
                    <p><em><?php _e( 'No Membership Access is set for this video', 'vimeo-sync-memberships' ); ?></em>.</p>
                <?php } ?>
            </div>
        </div>

        <div class="col-4">
            <h4><?php _e( 'Download File Link (optional)', 'vimeo-sync-memberships' ); ?>:</h4>
            <p><?php _e( 'Customers who have purchased the video can download the video file', 'vimeo-sync-memberships' ); ?>.</p>
            <input class="regular-text" type="url" id="rvs_video_download_link" name="rvs_video_download_link" placeholder="http://yourdownloadlink.com/videofile.mp4" value="<?php echo $video_download_link; ?>"/>
            <?php
            global $wpvs_vimeo_config;
            if( $wpvs_vimeo_config ) { ?>
                <label id="rvs-get-vimeo-download" class="rvs-button <?=($main_video_type == "vimeo" && ! empty($vimeo_id)) ? 'show-vimeo-downloads-button' : ''?>" data-vimeoid="<?php echo $vimeo_id; ?>"><?php _e('Get Vimeo Downloads', 'vimeo-sync-memberships'); ?></label>
                <div id="wpvs-vimeo-downloads" class="rvs-border-box">
                </div>
            <?php } ?>
        </div>

        <div class="col-4">
            <h4><?php _e( 'Download Text', 'vimeo-sync-memberships' ); ?>:</h4>
            <input type="text" name="wpvs_download_link_text" placeholder="Download" value="<?php echo $download_link_text; ?>">
        </div>

    </div>
<?php }

function wpvs_site_lock_options_callback( $post ) {
	wp_nonce_field( 'wpvs_site_lock_settings_save', 'wpvs_site_lock_settings_save_nonce' );
	$wpvs_unlock_page_override = get_post_meta( $post->ID, 'wpvs_unlock_page_override', true );
    $post_type_name = 'page';
    if( $post->post_type == "rvs_video") {
        $post_type_name = 'video';
    }
    if( $post->post_type == "post") {
        $post_type_name = 'post';
    }
    ?>
    <div class="inside">
        <br>
        <label class="selectit" for="wpvs_unlock_page_override">
        <input type="checkbox" name="wpvs_unlock_page_override" value="1" <?php checked('1', $wpvs_unlock_page_override); ?>><?php _e('Unlocked', 'vimeo-sync-memberships'); ?></label>
        <br>
        <br>
        <p class="description"><?php echo sprintf(__('With Site Lock enabled, you can optionally unlock this %s to allow visitors to access it without logging in.', 'vimeo-sync-memberships'), $post_type_name); ?></p>
    </div>
<?php }

function wpvs_video_memberships_save_meta( $post_id ) {
	if(rvs_memberships_save_data($post_id, 'rvs_video_membership_save_nonce', 'rvs_video_membership_save')) {
        if(isset($_POST['wpvs_selected_memberships']) ) {
            $new_membership_array = $_POST['wpvs_selected_memberships'];
            update_post_meta( $post_id, '_rvs_memberships', $new_membership_array );
        } else {
            update_post_meta( $post_id, '_rvs_memberships', array() );
        }

        if(isset($_POST['rvs_membership_users_free']) ) {
            update_post_meta( $post_id, '_rvs_membership_users_free', 1 );
        } else {
            update_post_meta( $post_id, '_rvs_membership_users_free', 0 );
        }

        // UPDATE PURCHASE PRICE
        if(isset($_POST['rvs_membership_onetime']) && ! empty( $_POST['rvs_membership_onetime']) ) {
            $new_onetime_price = $_POST['rvs_membership_onetime'];
            if(strpos($new_onetime_price, '.')) {
                $new_onetime_price = str_replace(".", "", $new_onetime_price);
            } else {
                $new_onetime_price = $new_onetime_price * 100;
            }
            update_post_meta( $post_id, '_rvs_onetime_price', $new_onetime_price );
        } else {
            update_post_meta( $post_id, '_rvs_onetime_price', null );
        }

        // UPDATE RENTAL PRICE
        if(isset($_POST['rvs_rental_price']) && ! empty($_POST['rvs_rental_price'])) {
            $new_rental_price = $_POST['rvs_rental_price'];
            if(strpos($new_rental_price, '.')) {
                $new_rental_price = str_replace(".", "", $new_rental_price);
            } else {
                $new_rental_price = $new_rental_price * 100;
            }
            update_post_meta( $post_id, 'rvs_rental_price', $new_rental_price );
        } else {
            update_post_meta( $post_id, 'rvs_rental_price', null );
        }

        // UPDATE RENTAL EXPIRES
        if(isset($_POST['rvs_rental_expires']) && ! empty($_POST['rvs_rental_expires'])) {
            $new_rental_expires = $_POST['rvs_rental_expires'];
            update_post_meta( $post_id, 'rvs_rental_expires', $new_rental_expires );
        } else {
            update_post_meta( $post_id, 'rvs_rental_expires', null );
        }

        // UPDATE RENTAL EXPIRES
        if(isset($_POST['rvs_rental_type']) && ! empty($_POST['rvs_rental_type'])) {
            $new_rental_expires = $_POST['rvs_rental_type'];
            update_post_meta( $post_id, 'rvs_rental_type', $new_rental_expires );
        }

        // UPDATE DOWNLOAD LINK
        if(isset($_POST['rvs_video_download_link']) && ! empty($_POST['rvs_video_download_link'])) {
            $new_video_download_link = $_POST['rvs_video_download_link'];
            update_post_meta( $post_id, 'rvs_video_download_link', $new_video_download_link );
        } else {
            update_post_meta( $post_id, 'rvs_video_download_link', "" );
        }

        if(isset($_POST['wpvs_members_can_download']) && ! empty($_POST['wpvs_members_can_download'])) {
            update_post_meta( $post_id, 'wpvs_members_can_download', 1 );
        } else {
            update_post_meta( $post_id, 'wpvs_members_can_download', 0 );
        }

        if( isset($_POST['wpvs_download_link_text']) ) {
            $new_download_text = sanitize_text_field($_POST['wpvs_download_link_text']);
            update_post_meta( $post_id, 'wpvs_download_link_text', $new_download_text );
        }

        if( isset($_POST['wpvs_required_download_memberships']) ) {
            $new_download_membership_array = $_POST['wpvs_required_download_memberships'];
            update_post_meta( $post_id, 'wpvs_required_download_memberships', $new_download_membership_array );
        } else {
            update_post_meta( $post_id, 'wpvs_required_download_memberships', array() );
        }

        if( isset($_POST['wpvs_restricted_video_content']) ) {
            update_post_meta( $post_id, 'wpvs_restricted_video_content', $_POST['wpvs_restricted_video_content'] );
        }
    }

    if(rvs_memberships_save_data($post_id, 'wpvs_site_lock_settings_save_nonce', 'wpvs_site_lock_settings_save')) {
        if(isset($_POST['wpvs_unlock_page_override']) ) {
            update_post_meta( $post_id, 'wpvs_unlock_page_override', 1 );
        } else {
            update_post_meta( $post_id, 'wpvs_unlock_page_override', 0 );
        }

    }
}
add_action( 'save_post', 'wpvs_video_memberships_save_meta' );

// SAVE FUNCTION
function rvs_memberships_save_data( $post_id, $save_nonce, $save_nonce_name ) {
    if ( ! isset( $_POST[$save_nonce] ) ) {
        return false;
    }
    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST[$save_nonce], $save_nonce_name ) ) {
        return false;
    }
    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return false;
    }
    if ( wp_is_post_revision( $post_id ) )
        return false;
    // Check the user's permissions.

    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return false;
    }
    return true;
}

// ADD QUICK EDIT MEMBERSHIPS
function add_wpvs_membership_edit_column($columns) {
    return array_merge( $columns, array('wpvs_memberships_column' => __('Memberships', 'vimeo-sync-memberships')) );
}
add_filter('manage_rvs_video_posts_columns', 'add_wpvs_membership_edit_column');

function wpvs_memberships_column_content($column_name, $post_id) {
    if ($column_name == 'wpvs_memberships_column') {
        $wpvs_video_memberships = get_post_meta( $post_id, '_rvs_memberships', true );
        $wpvs_is_user_free = get_post_meta( $post_id, '_rvs_membership_users_free', true );
        if ( ! empty($wpvs_video_memberships) ) {
            echo '<div class="wpvs-column-membership-list">';
            foreach($wpvs_video_memberships as $wpvs_membership) {
                if($wpvs_membership == end($wpvs_video_memberships)) {
                    echo $wpvs_membership;
                } else {
                    echo $wpvs_membership . ', ';
                }
            }
            echo '</div>';
        }
        if($wpvs_is_user_free) {
            _e('Free for Registered Users', 'vimeo-sync-memberships');
        } ?>
        <input type="hidden" class="wpvs-membership-users-free" value="<?php echo $wpvs_is_user_free; ?>"  />
    <?php }
}

add_action('manage_rvs_video_posts_custom_column', 'wpvs_memberships_column_content', 10, 2);

// QUICK EDIT MEMBERSHIPS

function wpvs_memberships_quickedit( $column_name, $post_type ) {
    if ($column_name == 'wpvs_memberships_column') {
        $wpvs_memberships_list = get_option('rvs_membership_list');
        ?>
        <fieldset class="wpvs-bulk-edit-column inline-edit-wpvs-memberships">
            <?php wp_nonce_field( 'wpvs_memberships_quick_save', 'wpvs_memberships_quick_save_nonce' ); ?>
          <div class="inline-edit-col column-<?php echo $column_name; ?>">
            <div class="inline-edit-group">
                <h4><?php _e('Memberships', 'vimeo-sync-memberships'); ?></h4>
                <ul class="cat-checklist wpvs-membership-checklist">
                <?php if(!empty($wpvs_memberships_list)) {
                    foreach($wpvs_memberships_list as $wpvs_plan) { ?>
                        <li><label class="selectit"><input type="checkbox" value="<?php echo $wpvs_plan['id']; ?>" name="wpvs_selected_memberships[]" class="wpvs-quick-edit-membership-checkbox"> <?php echo $wpvs_plan['name']; ?></label></li>
                    <?php
                    }
                } ?>
                    <li><label for="wpvs_membership_users_free" class="selectit"><input type="checkbox" name="wpvs_membership_users_free" class="wpv-membership-users-free-edit" value="1"> Free for Registered Users</label></li>
                </ul>
            </div>
          </div>
        </fieldset>
<?php }
}

add_action( 'quick_edit_custom_box', 'wpvs_memberships_quickedit', 10, 2 );

function save_wpvs_memberships_quick( $post_id ) {
    $wpvs_video_type = 'rvs_video';
    if ( isset($_POST['post_type']) && $wpvs_video_type !== $_POST['post_type'] ) {
        return;
    }
    if ( ! current_user_can( 'edit_post', $post_id ) ) {
        return;
    }
    if( isset($_POST['wpvs_memberships_quick_save_nonce']) ) {
        if ( ! wp_verify_nonce( $_POST['wpvs_memberships_quick_save_nonce'], 'wpvs_memberships_quick_save' ) ) {
            return false;
        }

        if(isset($_POST['wpvs_selected_memberships']) ) {
            $new_membership_array = $_POST['wpvs_selected_memberships'];
            update_post_meta( $post_id, '_rvs_memberships', $new_membership_array );
        } else {
            update_post_meta( $post_id, '_rvs_memberships', array() );
        }

        if(isset($_POST['wpvs_membership_users_free']) ) {
            update_post_meta( $post_id, '_rvs_membership_users_free', 1 );
        } else {
            update_post_meta( $post_id, '_rvs_membership_users_free', 0 );
        }
    }
}

add_action( 'save_post', 'save_wpvs_memberships_quick' );


// BULK EDIT MEMBERSHIPS

function wpvs_memberships_bulk_edit( $column_name, $post_type ) {
    global $post;
    if($column_name == "wpvs_memberships_column") { ?>
        <fieldset class="wpvs-bulk-edit-column inline-edit-wpvs-memberships">
            <?php
                wp_nonce_field( 'wpvs_memberships_bulk_save', 'wpvs_memberships_bulk_save_nonce' );
                $wpvs_memberships_list = get_option('rvs_membership_list');
                $wpvs_membership_count = count($wpvs_memberships_list);
          ?>
          <div class="inline-edit-col column-<?php echo $column_name; ?>">
            <div class="inline-edit-group">
                <h4><?php _e('Memberships', 'vimeo-sync-memberships'); ?></h4>
                <div><strong>Leave all unchecked to keep current video membership settings.</strong></div>
                <em>Checking a <strong>Membership</strong> or <strong>None</strong>, will overwite all selected videos memberships.</em>
                <input type="hidden" value="<?php echo $wpvs_membership_count; ?>" name="membership_count">
                <ul class="cat-checklist wpvs-membership-checklist">
                <?php if(!empty($wpvs_memberships_list)) {
                    foreach($wpvs_memberships_list as $wpvs_plan) { ?>
                        <li><label for="membership_select_<?php echo $wpvs_membership_count; ?>" class="selectit"><input type="checkbox" value="<?php echo $wpvs_plan['id']; ?>" id="membership_select_<?php echo $wpvs_membership_count; ?>" name="membership_select_<?php echo $wpvs_membership_count; ?>" class="wpvs-quick-edit-membership-checkbox"> <?php echo $wpvs_plan['name']; ?></label></li>
                    <?php
                        $wpvs_membership_count--;
                    } } ?>
                    <li><label class="selectit"><input type="checkbox" name="wpvs_memberships_set_none" id="wpvs_memberships_set_none" value="1"> None <strong>(Remove all memberships from selected videos)</strong></label></li>
                    <li class="wpvs-label-section"><strong>Free for Registered Users</strong></li>
                    <li><label class="selectit"><input type="radio" name="wpvs_membership_users_bulk_free" value="nochange" checked> No Change</label></li>
                    <li><label class="selectit"><input type="radio" name="wpvs_membership_users_bulk_free" value="1"> Yes</label></li>
                    <li><label class="selectit"><input type="radio" name="wpvs_membership_users_bulk_free" value="0"> No</label></li>
                </ul>
            </div>
          </div>
        </fieldset>
    <?php }
}

add_action( 'bulk_edit_custom_box', 'wpvs_memberships_bulk_edit', 10, 2 );

add_action( 'wp_ajax_save_bulk_edit_rvs_video_memberships', 'save_bulk_edit_rvs_video_memberships' );
function save_bulk_edit_rvs_video_memberships() {
	$wpvs_video_ids           = ( ! empty( $_POST[ 'video_ids' ] ) ) ? $_POST[ 'video_ids' ] : array();
	$wpvs_update_memberships  = ( ! empty( $_POST[ 'video_memberships' ] ) ) ? $_POST[ 'video_memberships' ] : array();
    $wpvs_free_for_users  = ( ! empty( $_POST[ 'wpvs_free_for_users' ] ) ) ? $_POST[ 'wpvs_free_for_users' ] : 0;
    $wpvs_remove_all = 0;

    if( isset($_POST[ 'wpvs_remove_all' ]) && $_POST[ 'wpvs_remove_all' ] ) {
         $wpvs_remove_all = 1;
    }

	// if everything is in order
	if ( ! empty( $wpvs_video_ids ) && is_array( $wpvs_video_ids ) ) {
		foreach( $wpvs_video_ids as $video_id ) {
            if( ! empty($wpvs_update_memberships) && is_array($wpvs_update_memberships) ) {
                $wpvs_update_memberships = array_unique($wpvs_update_memberships);
                update_post_meta( $video_id, '_rvs_memberships', $wpvs_update_memberships);
            }

            if( empty($wpvs_free_for_users) ) {
                update_post_meta( $video_id, '_rvs_membership_users_free', 0);
            } else {
                if($wpvs_free_for_users != "nochange") {
                    update_post_meta( $video_id, '_rvs_membership_users_free', 1);
                }
            }

            if($wpvs_remove_all) {
                update_post_meta( $video_id, '_rvs_memberships', array());
            }
		}
	}
	wp_die();
}


function wpvs_add_quick_edit_script( $hook ) {
	if ( 'edit.php' === $hook && isset( $_GET['post_type'] ) && 'rvs_video' === $_GET['post_type'] ) {
		wp_enqueue_script( 'wpvs-edit-wpvs-memberships', RVS_MEMBERS_BASE_URL . '/js/admin/wpvs-quick-edit-memberships.js',
			array('jquery'), '', true );
	}
}
add_action( 'admin_enqueue_scripts', 'wpvs_add_quick_edit_script' );
