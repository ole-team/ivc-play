<div class="wrap">
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title"><?php _e('New Coupon Code', 'vimeo-sync-memberships'); ?></h1>
            <div class="rvs-edit-form">
                <label><?php _e('Coupon ID (Example: DISCOUNT20)', 'vimeo-sync-memberships'); ?></label>
                <input type="text" name="coupon_id" id="coupon_id" required><br />

                <label><?php _e('Coupon Title', 'vimeo-sync-memberships'); ?></label>
                <input type="text" name="coupon_name" id="coupon_name" required><br />

                <label><?php _e('Discount type', 'vimeo-sync-memberships'); ?>:</label>
                <select id="discount_type" name="discount_type">
                    <option value="amount"><?php _e('Amount', 'vimeo-sync-memberships'); ?></option>
                    <option value="percentage"><?php _e('Percentage', 'vimeo-sync-memberships'); ?></option>
                </select><br/>

                <label id="discount-type-label"><?php _e('Amount Off', 'vimeo-sync-memberships'); ?></label>
                <input placeholder="10.00" type="number" step="any" name="amount_off" id="amount_off" required><br />

                <label><?php _e('Duration', 'vimeo-sync-memberships'); ?></label>
                <select id="coupon_duration" name="coupon_duration">
                    <option value="once"><?php _e('Once', 'vimeo-sync-memberships'); ?></option>
                    <option value="repeating"><?php _e('Repeating', 'vimeo-sync-memberships'); ?></option>
                    <option value="forever"><?php _e('Forever', 'vimeo-sync-memberships'); ?></option>
                </select><br/>

                <label class="duration-months"><?php _e('Duration in Months (How many months?)', 'vimeo-sync-memberships'); ?></label>
                <input class="duration-months" placeholder="6" type="number" step="any" name="coupon_month_duration" id="coupon_month_duration" required><br />

                <label><?php _e('Max uses (How many times can this coupon be used) Leave blank for unlimited', 'vimeo-sync-memberships'); ?></label>
                <input placeholder="(unlimited)" type="number" step="any" name="coupon_max_uses" id="coupon_max_uses"><br />

                <label><?php _e('Max uses per customer (How many times each customer use this coupon)', 'vimeo-sync-memberships'); ?> <em><?php _e('Max uses must be unlimited', 'vimeo-sync-memberships'); ?></em></label>
                <input placeholder="(unlimited)" type="number" step="any" name="coupon_max_uses_customer" id="coupon_max_uses_customer"><br />

                <hr>

                <label id="new-coupon-code" type="submit" class="rvs-button rvs-primary-button"><?php _e('Create', 'vimeo-sync-memberships'); ?></label>
            </div>
        </div>
    </div>
</div>
