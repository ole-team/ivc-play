<?php
global $wpvs_stripe_options;
global $rvs_paypal_settings;
global $rvs_paypal_enabled;
global $wpvs_stripe_gateway_enabled;
global $rvs_currency;
global $wpvs_currency_label;
$membershipList = get_option('rvs_membership_list');
?>
<div class="wrap">
    <h2 class="wp-admin-notice-placement"></h2>
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title">Memberships</h1>
            <a class="rvs-create-button rvs-button" href="admin.php?page=rvs-new-membership">Add New Plan</a>
            <div id="rvs-memberships-list">
            <?php
                if($membershipList) {
                    foreach($membershipList as $plan) {
                        $trial_frequency = "none";
                        $trial_period = 0;
                        $trial_text = "No Trial Set";
                        $plan_interval = $plan['interval'];
                        if(isset($plan['trial_frequency']) && $plan['trial_frequency'] != "none" && isset($plan['trial']) && $plan['trial'] != 0) {
                            $trial_frequency = $plan['trial_frequency'];
                            $trial_period = $plan['trial'];
                            $trial_text = $trial_period.' '.$trial_frequency.'(s)';
                        }

                        if(isset($plan['interval_count']) && ! empty($plan['interval_count']) ) {
                            $plan_interval_count = $plan['interval_count'];
                        } else {
                            $plan_interval_count = 1;
                        }

                        if($plan_interval_count > 1) {
                            $plan_interval = $plan_interval_count . ' ' .$plan_interval.'s';
                        }

                ?>
                    <div class="rvs-membership-tab">
                        <div class="rvs-membership-header">
                            <a class="rvs-edit rvs-border-box" href="admin.php?page=rvs-edit-membership&id=<?php echo $plan['id']; ?>"><span class="dashicons dashicons-edit"></span> Edit</a>
                            <label class="rvs-name rvs-border-box"><?php echo $plan['name']; ?></label>
                            <label class="rvs-price rvs-border-box"><?php echo $wpvs_currency_label.' '.number_format($plan['amount']/100, 2) . '/ ' . $plan_interval; ?></label>

                            <label class="rvs-trial rvs-border-box"><?php _e('Free Trial', 'vimeo-sync-memberships'); ?>: <?php echo $trial_text; ?></label>
                        </div>

                        <!-- IF STRIPE IS ENABLED -->
                        <?php if($wpvs_stripe_gateway_enabled) {
                            $stripe_active = isset($plan['stripe']); ?>
                        <div class="rvs-membership-gateway rvs-border-box <?=($stripe_active) ? 'rvs-gateway-active' : 'rvs-gateway-inactive' ?>">
                            <label class="rvs-gateway-title"><?=($stripe_active) ? '<span class="dashicons dashicons-yes"></span> ' : '<span class="dashicons dashicons-warning rvs-error"></span> ' ?>Stripe <?=($stripe_active) ? '<span class="rvs-active-plan">Active</span>' : '<span class="rvs-gateway-missing">You have not created a Billing Plan for Stripe</span> '?></label>
                            <div class="rvs-right-buttons">
                            <?php
                                if($stripe_active) {
                                    echo '<label class="remove-active-stripe-plan rvs-button" data-plan="'.$plan['id'].'"><span class="dashicons dashicons-trash"></span></label>';
                                } else {
                                    echo '<label class="rvs-create-stripe-plan rvs-button" data-plan="'.$plan['id'].'"><span class="dashicons dashicons-update"></span> Create Billing Plan</label>';
                                }
                            ?>
                            </div>

                        </div>
                        <?php } ?>

                        <!-- IF PAYPAL IS ENABLED -->
                        <?php if($rvs_paypal_enabled) {
                            $paypal_active = isset($plan['paypal']); ?>
                        <div class="rvs-membership-gateway rvs-border-box <?=(!$paypal_active) ? 'rvs-gateway-inactive' : 'rvs-gateway-active' ?>">
                            <label class="rvs-gateway-title"><?=($paypal_active) ? '<span class="dashicons dashicons-yes"></span> ' : '<span class="dashicons dashicons-warning rvs-error"></span> '?>PayPal <?=($paypal_active) ? '<span class="rvs-active-plan">Active</span>' : '<span class="rvs-gateway-missing">You have not created a Billing Plan for PayPal</span>'?></label>
                            <div class="rvs-right-buttons">
                            <?php
                                if($paypal_active) {
                                    echo '<label class="remove-active-paypal-plan rvs-button" data-plan="'.$plan['id'].'"><span class="dashicons dashicons-trash"></span></label>';
                                } else { ?>
                                <div class="rvs-paypal-permalinks">
                                    <p><strong>Important:</strong> Make sure you have set your <a href="<?php echo admin_url('options-permalink.php'); ?>">Permalinks</a>.<span class="rvs-perma-help dashicons dashicons-editor-help"></span></p>
                                    <p class="rvs-hidden-text">Changing your Permalinks <em>after</em> a PayPal Billing Plan has been created, will effect PayPal's return URL. Therefore, we suggest setting your permalink stucture <em>before</em> creating PayPal Billing Plans. <strong>You only need to do this once</strong>. We recommend anything <strong>other than</strong> the Plain Permalink structure.</p>
                                </div>
                               <?php echo '<label class="rvs-create-paypal-plan rvs-button" data-plan="'.$plan['id'].'"><span class="dashicons dashicons-update"></span> Create Billing Plan</label>';
                                } ?>
                            </div>

                        </div>
                        <?php } ?>
                    </div>
                <?php } } ?>
            </div>
        </div>
    </div>
</div>
