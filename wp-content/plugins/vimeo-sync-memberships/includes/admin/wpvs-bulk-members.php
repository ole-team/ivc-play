<?php
global $wpvs_stripe_gateway_enabled;
global $rvs_live_mode;

$all_roles = get_editable_roles();
$wpvs_roles = get_option('wpvs_membership_roles', array());
$wpvs_test_roles = array();
if( ! empty($wpvs_roles) ) {
    foreach($wpvs_roles as $add_role) {
        $wpvs_test_roles[] = $add_role.'_test';
    }
}
$list_of_roles = array();

if($rvs_live_mode == "on") {
    $import_stripe_payments_text = __('Import Stripe Customers', 'vimeo-sync-memberships');
    $default_member_filter = "wpvs_member";
    $default_member_filter_text = __('Member', 'vimeo-sync-memberships');
    if( ! empty($all_roles) && ! empty($wpvs_roles) ) {
        foreach($all_roles as $key => $role) {
            if( in_array($key, $wpvs_roles) ) {
                $list_of_roles[$key] = $role['name'];
            }
        }
    }
} else {
    $import_stripe_payments_text = __('Import Stripe TEST Customers', 'vimeo-sync-memberships');
    $default_member_filter = "wpvs_test_member";
    $default_member_filter_text = __('TEST Member', 'vimeo-sync-memberships');
    if( ! empty($all_roles) && ! empty($wpvs_test_roles) ) {
        foreach($all_roles as $key => $role) {
            if( in_array($key, $wpvs_test_roles) ) {
                $list_of_roles[$key] = $role['name'];
            }
        }
    }
}
$wpvs_memberships_list = get_option('rvs_membership_list', array());

?>
<div class="wrap">
    <h2 class="wp-admin-notice-placement"></h2>
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title">Bulk Edit Users</h1>
            <p>This admin page allows you to add / remove manual <strong>(free)</strong> memberships to / from multiple users.</p>
            <?php if( $wpvs_stripe_gateway_enabled ) { ?>
            <h3 class="wpvs-admin-section-title"><?php _e('Import Customers', 'vimeo-sync-memberships'); ?></h3>
            <div class="wpvs-admin-inline-button">
                <label id="import-stripe-customers" class="wpvs-admin-inline-button"><span class="dashicons dashicons-plus-alt"></span> <?php echo $import_stripe_payments_text; ?></label>
            </div>
             <?php } ?>

            <h3 class="wpvs-admin-section-title"><?php _e('Add / Remove Memberships', 'vimeo-sync-memberships'); ?></h3>
            <div class="wpvs-filter-options rvs-border-box">
                <?php if( ! empty($list_of_roles) ) { ?>
                    <label class="wpvs-filter-label"><?php _e('Filter Role', 'vimeo-sync-memberships'); ?></label>
                    <div class="wpvs-filter-setting">
                        <select id="wpvs_filter_members_by_role" name="wpvs_filter_members_by_role">
                            <option value="0"><?php _e('All', 'vimeo-sync-memberships'); ?></option>
                            <option value="nomembership"><?php _e('Without Membership', 'vimeo-sync-memberships'); ?></option>
                            <option value="<?php echo $default_member_filter; ?>"><?php echo $default_member_filter_text; ?></option>
                            <?php foreach($list_of_roles as $key => $member_role) { ?>
                                <option value="<?php echo $key; ?>"><?php echo $member_role; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
                <div class="wpvs-filter-setting">
                    <select id="wpvs_bulk_members_action" name="wpvs_bulk_members_action">
                        <option value="add"><?php _e('Add', 'vimeo-sync-memberships'); ?></option>
                        <option value="remove"><?php _e('Remove', 'vimeo-sync-memberships'); ?></option>
                    </select>
                </div>
                <div class="wpvs-filter-setting">
                    <select id="wpvs_bulk_members_membership" name="wpvs_bulk_members_membership">
                        <?php if( empty($wpvs_memberships_list) ) { ?>
                            <option value=""><?php _e('No memberships created', 'vimeo-sync-memberships'); ?></option>
                        <?php } else {
                            foreach($wpvs_memberships_list as $wpvs_membership) { ?>
                                <option value="<?php echo $wpvs_membership['id']; ?>"><?php echo $wpvs_membership['name']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="wpvs-filter-setting">
                    <input type="text" class="wpvs-select-date-admin" readonly placeholder="End Date (optional)">
                    <input type="text" id="wpvs-set-membership-ends" value="" readonly hidden>
                    <label id="wpvs-clear-membership-ends">Clear</label>
                </div>

            </div>
            <div class="wpvs-filter-options rvs-border-box">
                <div class="wpvs-filter-setting">
                    <label id="wpvs-submit-bulk-members-edit" class="rvs-button">Submit</label>
                </div>
            </div>

            <div class="rvs-navigation">
                <div class="border-box rvs-previous-button rvs-nav-buttons">
                    <label class="rvs-button rvs-button-blue rvs-back-members">&#60; Previous Page</label>
                </div>
                <div class="border-box text-align-right rvs-next-button rvs-nav-buttons">
                    <label class="rvs-button rvs-button-blue rvs-more-members">Next Page &#62;</label>
                </div>
            </div>
            <div id="wpvs-loading-members" class="wpvs-update-content"><span id="wpvs-update-text" class="wpvs-loading-text">Getting Users...</span><span class="loadingCircle"></span><span class="loadingCircle"></span><span class="loadingCircle"></span><span class="loadingCircle"></span></div>
            <div id="wpvs-members-loaded"></div>
        </div>
    </div>
</div>
