<?php
if(isset($_GET['tab'])) {
    $current_tab = $_GET['tab'];
} else {
    $current_tab = "stripe";
}
global $rvs_live_mode;
global $rvs_currency;
?>
<div class="wrap">
    <h2 class="wp-admin-notice-placement"></h2>
    <div class="rvs-container">
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
                <div class="rvs-container">
                    <div class="col-8">
                        <h1 class="rvs-title">Payment Gateways</h1>
                        <div class="rvs-container">
                            <ul class="subsubsub">
                                <li><a class="<?= ($current_tab == "stripe") ? 'current': '' ?>" href="<?php echo admin_url('admin.php?page=rvs-payment-settings&tab=stripe'); ?>"><?php _e('Stripe', 'vimeo-sync-memberships'); ?></a> | </li>
                                <li><a class="<?= ($current_tab == "paypal") ? 'current': '' ?>" href="<?php echo admin_url('admin.php?page=rvs-payment-settings&tab=paypal'); ?>"><?php _e('PayPal', 'vimeo-sync-memberships'); ?></a> | </li>
                                <li><a class="<?= ($current_tab == "coinbase") ? 'current': '' ?>" href="<?php echo admin_url('admin.php?page=rvs-payment-settings&tab=coinbase'); ?>"><?php _e('Coinbase', 'vimeo-sync-memberships'); ?></a> | </li>
                                <li><a class="<?= ($current_tab == "coingate") ? 'current': '' ?>" href="<?php echo admin_url('admin.php?page=rvs-payment-settings&tab=coingate'); ?>"><?php _e('CoinGate', 'vimeo-sync-memberships'); ?></a> | </li>
                            </ul>
                        </div>
                    <?php if($current_tab == "stripe") { ?>
                    <h3>Stripe <?php _e('Setup', 'vimeo-sync-memberships'); ?></h3>
                    <div class="text-align-right"><a href="https://docs.wpvideosubscriptions.com/wp-video-memberships/stripe-payments-setup/" target="_blank"><?php _e('Stripe Setup Guide', 'vimeo-sync-memberships'); ?></a></div>
                    <form method="post" action="options.php">
                        <?php settings_fields('wpvs-membership-stripe-settings');
                            global $wpvs_stripe_options;
                            global $wpvs_stripe_api_version;
                            if(!isset($wpvs_stripe_options['enabled'])) {
                                $wpvs_stripe_options['enabled'] = 0;
                            }

                            if(!isset($wpvs_stripe_options['billing_address'])) {
                                $wpvs_stripe_options['billing_address'] = 0;
                            }

                            if(!isset($wpvs_stripe_options['verify_zip'])) {
                                $wpvs_stripe_options['verify_zip'] = 0;
                            }

                            if(!isset($wpvs_stripe_options['google_apple_pay'])) {
                                $wpvs_stripe_options['google_apple_pay'] = 0;
                            }

                            if(!isset($wpvs_stripe_options['invoices_enabled'])) {
                                $wpvs_stripe_options['invoices_enabled'] = 0;
                            }

                            if(!isset($wpvs_stripe_options['country_code'])) {
                                $wpvs_stripe_options['country_code'] = 'US';
                            }

                            if( ! isset($wpvs_stripe_options['agreement_content']) ) {
                                $wpvs_stripe_options['agreement_content'] = '<strong>Agreement:</strong> I authorise '.get_bloginfo('name').' to send instructions to the financial institution that issued my card to take payments from my card account in accordance with the terms of my agreement with you.';
                            }

                            if( ! isset($wpvs_stripe_options['card_note_content']) ) {
                                $wpvs_stripe_options['card_note_content'] = '<strong>Note:</strong> If your card requires authentication on all transactions, you may need to login to your account to pay for subscription renewals.';
                            }

                            if( ! isset($wpvs_stripe_options['stripe_checkout_enabled']) ) {
                                $wpvs_stripe_options['stripe_checkout_enabled'] = 0;
                            }

                            if( ! isset($wpvs_stripe_options['stripe_checkout_button_text']) ) {
                                $wpvs_stripe_options['stripe_checkout_button_text'] = __('Proceed To Checkout', 'vimeo-sync-memberships');
                            }

                            if( ! isset($wpvs_stripe_options['test_webhook_id']) ) {
                                $wpvs_stripe_options['test_webhook_id'] = '';
                            }

                            if( ! isset($wpvs_stripe_options['test_webhook_secret']) ) {
                                $wpvs_stripe_options['test_webhook_secret'] = '';
                            }

                            if( ! isset($wpvs_stripe_options['webhook_id']) ) {
                                $wpvs_stripe_options['webhook_id'] = '';
                            }

                            if( ! isset($wpvs_stripe_options['webhook_secret']) ) {
                                $wpvs_stripe_options['webhook_secret'] = '';
                            }

                            $rvs_card_button_image = get_option('rvs_card_button_image', RVS_MEMBERS_BASE_URL . 'image/stripe.png');
                            $rvs_card_button_background = get_option('rvs_card_button_background', '#27ae60');

                        ?>
                        <div class="rvs-container rvs-box">
                            <div class="rvs-col-12">
                                <table class="form-table">
                                    <tbody>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Stripe API Version', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <label class="description"><strong><?php echo $wpvs_stripe_api_version; ?></strong></label>
                                                <p class="description">Make sure your Stripe account is using the same API version. <a href="https://dashboard.stripe.com/developers" target="_blank">Set Version</a></p>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Enable Stripe', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_stripe_settings[enabled]" name="wpvs_stripe_settings[enabled]" type="checkbox" value="1" <?php checked(1, $wpvs_stripe_options['enabled']); ?> />
                                                <label class="description" for="wpvs_stripe_settings[enabled]"><?php _e('Enable Credit Card Payments', 'vimeo-sync-memberships'); ?></label>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Google / Apply Pay', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_stripe_settings[google_apple_pay]" name="wpvs_stripe_settings[google_apple_pay]" type="checkbox" value="1" <?php checked(1, $wpvs_stripe_options['google_apple_pay']); ?> />
                                                <label class="description" for="wpvs_stripe_settings[google_apple_pay]"><?php _e('Enable Google and Apple Pay.', 'vimeo-sync-memberships'); ?> <a href="https://stripe.com/docs/stripe-js/elements/payment-request-button#verifying-your-domain-with-apple-pay" target="_blank">Setup Requirements</a></label>
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Stripe Checkout Page', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_stripe_settings[stripe_checkout_enabled]" name="wpvs_stripe_settings[stripe_checkout_enabled]" type="checkbox" value="1" <?php checked(1, $wpvs_stripe_options['stripe_checkout_enabled']); ?> />
                                                <label class="description" for="wpvs_stripe_settings[ideal_payments_enabled]"><?php _e('Enable Stripe Checkout', 'vimeo-sync-memberships'); ?></label>
                                                <p class="description">A Checkout page hosted by Stripe. <a href="https://stripe.com/docs/payments/checkout" target="_blank">See Example</a>.</p>
                                                <p class="description"><strong>Important Notes:</strong></p>
                                                <ul style="list-style: disc;">
                                                    <li><em>This will <strong>disable</strong> the default Credit Card form on Checkout pages.</em></li>
                                                    <li><em><?php _e('Coupon Codes are currently not supported by Stripe Checkout for subscriptions', 'vimeo-sync-memberships'); ?>.</em></li>
                                                </ul>
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Stripe Checkout Button Text', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_stripe_settings[stripe_checkout_button_text]" name="wpvs_stripe_settings[stripe_checkout_button_text]" type="text" value="<?php echo $wpvs_stripe_options['stripe_checkout_button_text']; ?>" class="regular-text" />
                                                <p class="description">Changes the text for the Stripe Checkout button. Only applicable if <strong>Stripe Checkout Page</strong> is enabled.</p>
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Country Code', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <select name="wpvs_stripe_settings[country_code]">
                                                    <option value="AT" <?php selected('AT', $wpvs_stripe_options['country_code']); ?>>AT</option>
                                                    <option value="AU" <?php selected('AU', $wpvs_stripe_options['country_code']); ?>>AU</option>
                                                    <option value="BE" <?php selected('BE', $wpvs_stripe_options['country_code']); ?>>BE</option>
                                                    <option value="BR" <?php selected('BR', $wpvs_stripe_options['country_code']); ?>>BR</option>
                                                    <option value="CA" <?php selected('CA', $wpvs_stripe_options['country_code']); ?>>CA</option>
                                                    <option value="CH" <?php selected('CH', $wpvs_stripe_options['country_code']); ?>>CH</option>
                                                    <option value="DE" <?php selected('DE', $wpvs_stripe_options['country_code']); ?>>DE</option>
                                                    <option value="DK" <?php selected('DK', $wpvs_stripe_options['country_code']); ?>>DK</option>
                                                    <option value="EE" <?php selected('EE', $wpvs_stripe_options['country_code']); ?>>EE</option>
                                                    <option value="ES" <?php selected('ES', $wpvs_stripe_options['country_code']); ?>>ES</option>
                                                    <option value="FI" <?php selected('FI', $wpvs_stripe_options['country_code']); ?>>FI</option>
                                                    <option value="FR" <?php selected('FR', $wpvs_stripe_options['country_code']); ?>>FR</option>
                                                    <option value="GB" <?php selected('GB', $wpvs_stripe_options['country_code']); ?>>GB</option>
                                                    <option value="HK" <?php selected('HK', $wpvs_stripe_options['country_code']); ?>>HK</option>
                                                    <option value="IE" <?php selected('IE', $wpvs_stripe_options['country_code']); ?>>IE</option>
                                                    <option value="IN" <?php selected('IN', $wpvs_stripe_options['country_code']); ?>>IN</option>
                                                    <option value="IT" <?php selected('IT', $wpvs_stripe_options['country_code']); ?>>IT</option>
                                                    <option value="JP" <?php selected('JP', $wpvs_stripe_options['country_code']); ?>>JP</option>
                                                    <option value="LT" <?php selected('LT', $wpvs_stripe_options['country_code']); ?>>LT</option>
                                                    <option value="LU" <?php selected('LU', $wpvs_stripe_options['country_code']); ?>>LU</option>
                                                    <option value="LV" <?php selected('LV', $wpvs_stripe_options['country_code']); ?>>LV</option>
                                                    <option value="MX" <?php selected('MX', $wpvs_stripe_options['country_code']); ?>>MX</option>
                                                    <option value="NL" <?php selected('NL', $wpvs_stripe_options['country_code']); ?>>NL</option>
                                                    <option value="NZ" <?php selected('NZ', $wpvs_stripe_options['country_code']); ?>>NZ</option>
                                                    <option value="NO" <?php selected('NO', $wpvs_stripe_options['country_code']); ?>>NO</option>
                                                    <option value="PH" <?php selected('PH', $wpvs_stripe_options['country_code']); ?>>PH</option>
                                                    <option value="PL" <?php selected('PL', $wpvs_stripe_options['country_code']); ?>>PL</option>
                                                    <option value="PT" <?php selected('PT', $wpvs_stripe_options['country_code']); ?>>PT</option>
                                                    <option value="RO" <?php selected('RO', $wpvs_stripe_options['country_code']); ?>>RO</option>
                                                    <option value="SE" <?php selected('SE', $wpvs_stripe_options['country_code']); ?>>SE</option>
                                                    <option value="SG" <?php selected('SG', $wpvs_stripe_options['country_code']); ?>>SG</option>
                                                    <option value="SK" <?php selected('SK', $wpvs_stripe_options['country_code']); ?>>SK</option>
                                                    <option value="US" <?php selected('US', $wpvs_stripe_options['country_code']); ?>>US</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Stripe Webhook', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <a href="<?php echo admin_url('admin.php?page=rvs-webhooks'); ?>">Webhook Setup</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h3 class="title"><?php _e('API Keys', 'vimeo-sync-memberships'); ?></h3>
                                <table class="form-table">
                                    <tbody>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Test Secret', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_stripe_settings[test_secret_key]" name="wpvs_stripe_settings[test_secret_key]" type="password" class="regular-text" value="<?php echo $wpvs_stripe_options['test_secret_key']; ?>"/><br>
                                                <label class="description" for="wpvs_stripe_settings[test_secret_key]"><?php _e('Paste your test secret key.', 'vimeo-sync-memberships'); ?></label>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Test Publishable', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_stripe_settings[test_publishable_key]" name="wpvs_stripe_settings[test_publishable_key]" class="regular-text" type="password" value="<?php echo $wpvs_stripe_options['test_publishable_key']; ?>"/><br>
                                                <label class="description" for="wpvs_stripe_settings[test_publishable_key]"><?php _e('Paste your test publishable key.', 'vimeo-sync-memberships'); ?></label>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Live Secret', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_stripe_settings[live_secret_key]" name="wpvs_stripe_settings[live_secret_key]" type="password" class="regular-text" value="<?php echo $wpvs_stripe_options['live_secret_key']; ?>"/><br>
                                                <label class="description" for="wpvs_stripe_settings[live_secret_key]"><?php _e('Paste your live secret key.', 'vimeo-sync-memberships'); ?></label>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Live Publishable', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_stripe_settings[live_publishable_key]" name="wpvs_stripe_settings[live_publishable_key]" type="password" class="regular-text" value="<?php echo $wpvs_stripe_options['live_publishable_key']; ?>"/><br>
                                                <label class="description" for="wpvs_stripe_settings[live_publishable_key]"><?php _e('Paste your live publishable key.', 'vimeo-sync-memberships'); ?></label>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Require Billing Address', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_stripe_settings[billing_address]" name="wpvs_stripe_settings[billing_address]" type="checkbox" value="1" <?php checked(1, $wpvs_stripe_options['billing_address']); ?> />
                                                <label class="description" for="wpvs_stripe_settings[billing_address]"><?php _e('Required billing address on payment.', 'vimeo-sync-memberships'); ?></label>
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Verify Zip', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_stripe_settings[verify_zip]" name="wpvs_stripe_settings[verify_zip]" type="checkbox" value="1" <?php checked(1, $wpvs_stripe_options['verify_zip']); ?> />
                                                <label class="description" for="wpvs_stripe_settings[verify_zip]"><?php _e('Verify zip code of the card. (<strong>This can help if payments are failing.</strong>)', 'vimeo-sync-memberships'); ?></label>
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Max Fail Attempts', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                            <label><?php _e('How many times should Stripe retry to charge customers for subscriptions before cancelling their membership.', 'vimeo-sync-memberships'); ?></label><br><br>
                                            <label><?php _e('Stripe Retry rules can be set in your', 'vimeo-sync-memberships'); ?> <a href="https://dashboard.stripe.com/account/recurring" target="_blank">Stripe Dashboard</a></label>
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Invoices', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_stripe_settings[invoices_enabled]" name="wpvs_stripe_settings[invoices_enabled]" type="checkbox" value="1" <?php checked(1, $wpvs_stripe_options['invoices_enabled']); ?> />
                                                <label class="description"><?php _e('Allow customers to download their invoices for past payments.', 'vimeo-sync-memberships'); ?></label><br><br>
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Card Agreement Notice', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <?php
                                                wp_editor(
                                                    $wpvs_stripe_options['agreement_content'],
                                                    'wpvs_stripe_card_agreement_notice',
                                                    $settings = array(
                                                        'textarea_name' => 'wpvs_stripe_settings[agreement_content]',
                                                        'media_buttons' => false,
                                                        'teeny' => true,
                                                        'wpautop' => false,
                                                        'textarea_rows' => 6
                                                    )
                                                );
                                                ?>
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Card Note', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <?php
                                                wp_editor(
                                                    $wpvs_stripe_options['card_note_content'],
                                                    'wpvs_stripe_card_note_notice',
                                                    $settings = array(
                                                        'textarea_name' => 'wpvs_stripe_settings[card_note_content]',
                                                        'media_buttons' => false,
                                                        'teeny' => true,
                                                        'wpautop' => false,
                                                        'textarea_rows' => 6
                                                    )
                                                );
                                                ?>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <input name="wpvs_stripe_settings[test_webhook_id]" type="hidden" value="<?php echo $wpvs_stripe_options['test_webhook_id']; ?>"/>
                            <input name="wpvs_stripe_settings[test_webhook_secret]" type="hidden" value="<?php echo $wpvs_stripe_options['test_webhook_secret']; ?>"/>
                            <input name="wpvs_stripe_settings[webhook_id]" type="hidden" value="<?php echo $wpvs_stripe_options['webhook_id']; ?>"/>
                            <input name="wpvs_stripe_settings[webhook_secret]" type="hidden" value="<?php echo $wpvs_stripe_options['webhook_secret']; ?>"/>
                            <div class="rvs-container">
                                <div class="rvs-col-12">
                                <input type="submit" class="button-primary" value="<?php _e('Save Stripe Options', 'vimeo-sync-memberships'); ?>" />
                                </div>
                            </div>
                        </div>
                        <h3><?php _e('Checkout Form', 'vimeo-sync-memberships') ; ?></h3>
                        <div class="rvs-container rvs-box">
                            <div class="rvs-col-12">
                                <p>Choose an image that displays as the Credit Card payment method or use the default one. <em>(260px x 44px)</em></p>
                                 <div class="imageContain">
                                    <img src="<?php echo $rvs_card_button_image; ?>" id="wpvs-card-image-update" class="change-image"/>
                                 </div>
                                <input class="upload-button button button-primary" type="button" value="Choose Image" />
                                <input class="upload-url" name="rvs_card_button_image" id="rvs_card_button_image" type="text" value="<?php echo $rvs_card_button_image; ?>" hidden/>
                                <label id="rvs-reset-card-image" class="button" type="button">Use Default</label>
                                <h4>Checkout Button Background</h4>
                                <input id="rvs_card_button_background" name="rvs_card_button_background" type="text" value="<?php echo $rvs_card_button_background; ?>" class="rvs-color-field" /><p class="description">Background color of the credit card checkout button. <strong>Note:</strong> This color will be overwritten by the VS Netflix Themes button colour automatically.</p>
                            </div>
                            <div class="rvs-container">
                                <div class="rvs-col-12">
                                <input type="submit" class="button-primary" value="<?php _e('Save Stripe Options', 'vimeo-sync-memberships'); ?>" />
                                </div>
                            </div>
                        </div>
                        </form>
                        <?php } if($current_tab == "paypal") { ?>
                        <h3>PayPal <?php _e('Setup', 'vimeo-sync-memberships'); ?></h3>
                        <div class="text-align-right"><a href="https://docs.wpvideosubscriptions.com/wp-video-memberships/paypal-payments-setup/" target="_blank"><?php _e('PayPal Setup Guide', 'vimeo-sync-memberships'); ?></a></div>
                        <form method="post" action="options.php">
                            <?php settings_fields('wpvs-membership-paypal-settings'); global $rvs_paypal_settings;
                                if(!isset($rvs_paypal_settings['enabled'])) {
                                    $rvs_paypal_settings['enabled'] = 0;
                                }

                                if(!isset($rvs_paypal_settings['sandbox_client_id'])) {
                                    $rvs_paypal_settings['sandbox_client_id'] = "";
                                }

                                if(!isset($rvs_paypal_settings['sandbox_client_secret'])) {
                                    $rvs_paypal_settings['sandbox_client_secret'] = "";
                                }

                                if(!isset($rvs_paypal_settings['live_client_id'])) {
                                    $rvs_paypal_settings['live_client_id'] = "";
                                }

                                if(!isset($rvs_paypal_settings['live_client_secret'])) {
                                    $rvs_paypal_settings['live_client_secret'] = "";
                                }

                                if(!isset($rvs_paypal_settings['email'])) {
                                    $rvs_paypal_settings['email'] = "";
                                }
                            ?>
                        <div class="rvs-container rvs-box">
                            <div class="rvs-col-12">
                                <table class="form-table">
                                    <tbody>

                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Enable PayPal', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="rvs_paypal_settings[enabled]" name="rvs_paypal_settings[enabled]" type="checkbox" value="1" <?php checked(1, $rvs_paypal_settings['enabled']); ?> />
                                                <label class="description" for="rvs_paypal_settings[enabled]"><?php _e('On / Off', 'vimeo-sync-memberships'); ?></label>
                                            </td>
                                        </tr>

                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('PayPal Webhooks', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <a href="<?php echo admin_url('admin.php?page=rvs-webhooks'); ?>">Webhook Setup</a>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                                <h3 class="title"><?php _e('API Keys', 'vimeo-sync-memberships'); ?></h3>
                                <table class="form-table">
                                    <tbody>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('SandBox Client ID', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="rvs_paypal_settings[sandbox_client_id]" name="rvs_paypal_settings[sandbox_client_id]" type="text" class="regular-text" value="<?php echo $rvs_paypal_settings['sandbox_client_id']; ?>"/>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('SandBox Client Secret', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="rvs_paypal_settings[sandbox_client_secret]" name="rvs_paypal_settings[sandbox_client_secret]" class="regular-text" type="text" value="<?php echo $rvs_paypal_settings['sandbox_client_secret']; ?>"/>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Live Client ID', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="rvs_paypal_settings[live_client_id]" name="rvs_paypal_settings[live_client_id]" type="text" class="regular-text" value="<?php echo $rvs_paypal_settings['live_client_id']; ?>"/>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Live Client Secret', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="rvs_paypal_settings[live_client_secret]" name="rvs_paypal_settings[live_client_secret]" class="regular-text" type="text" value="<?php echo $rvs_paypal_settings['live_client_secret']; ?>"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h3><?php _e('PayPal REST API', 'vimeo-sync-memberships'); ?></h3>
                                <p><?php _e('To use the PayPal REST API you need a PayPal Business or Premier Account.', 'vimeo-sync-memberships'); ?> <a href="https://www.paypal.com/UPGRADE" target="_blank"><?php _e('Upgrade to business account', 'vimeo-sync-memberships'); ?></a></p>
                                <table class="form-table">
                                    <tbody>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('PayPal Email', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input class="regular-text" id="rvs_paypal_settings[email]" name="rvs_paypal_settings[email]" type="email" value="<?php echo $rvs_paypal_settings['email']; ?>" />
                                                <label class="description" for="rvs_paypal_settings[email]"><?php _e('Your PayPal account email address.', 'vimeo-sync-memberships'); ?></label>
                                            </td>
                                        </tr>

                                        <?php $paypal_max_attempts = get_option('rvs_paypal_max_attempts', "0"); ?>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Max Fail Attempts', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                            <input type="number" min="0" max="10" name="rvs_paypal_max_attempts" value="<?php echo $paypal_max_attempts; ?>" /><br><br>
                                            <label><?php _e('How many times should PayPal re-attempt charges on customer subscriptions before suspending their membership.', 'vimeo-sync-memberships'); ?></label><br><br>
                                            <label><strong><?php _e('0 = unlimited attempts', 'vimeo-sync-memberships'); ?></strong> - <?php _e('Requiring you to manually cancel customers memberships if their payments continue to fail.', 'vimeo-sync-memberships'); ?></label><br><br>
                                            <label><em><?php _e('Example: If this is set to "2", PayPal will attempt to charge customers PayPal accounts 2 times before suspending their membership.', 'vimeo-sync-memberships'); ?></em></label>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="rvs-container">
                                <div class="rvs-col-12">
                                <input type="submit" class="button-primary" value="<?php _e('Save PayPal Options', 'vimeo-sync-memberships'); ?>" />
                                </div>
                            </div>
                        </div>
                        </form>
                <?php } if($current_tab == "coinbase") { ?>
                        <h3>Coinbase <?php _e('Setup', 'vimeo-sync-memberships'); ?></h3>
                        <form method="post" action="options.php">
                            <?php settings_fields('wpvs-memberships-coinbase');
                                $wpvs_coinbase_settings = get_option('wpvs_coinbase_settings');
                                global $rvs_currency;
                                if( ! isset($wpvs_coinbase_settings['enabled']) ) {
                                    $wpvs_coinbase_settings['enabled'] = 0;
                                }

                                if( ! isset($wpvs_coinbase_settings['api_key']) ) {
                                    $wpvs_coinbase_settings['api_key'] = "";
                                }

                                if( ! isset($wpvs_coinbase_settings['webhook_secret']) ) {
                                    $wpvs_coinbase_settings['webhook_secret'] = "";
                                }

                                if( ! isset($wpvs_coinbase_settings['accepted_currencies']) ) {
                                    $wpvs_coinbase_settings['accepted_currencies'] = array('BTC', 'BCH', 'ETH', 'LTC', 'USDC');
                                }
                            ?>
                        <div class="rvs-container rvs-box">
                            <div class="rvs-col-12">
                                <table class="form-table">
                                    <tbody>

                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Enable Coinbase', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_coinbase_settings[enabled]" name="wpvs_coinbase_settings[enabled]" type="checkbox" value="1" <?php checked(1, $wpvs_coinbase_settings['enabled']); ?> />
                                                <label class="description" for="wpvs_coinbase_settings[enabled]"><?php _e('On / Off', 'vimeo-sync-memberships'); ?></label>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                                <h3 class="title"><?php _e('API Keys', 'vimeo-sync-memberships'); ?></h3>
                                <table class="form-table">
                                    <tbody>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('API Key', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_coinbase_settings[api_key]" name="wpvs_coinbase_settings[api_key]" type="text" class="regular-text" value="<?php echo $wpvs_coinbase_settings['api_key']; ?>"/>
                                                <p><a href="https://commerce.coinbase.com/dashboard/settings#api-keys" target="_blank">Create API Key</a></p>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Webhook Secret', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input name="wpvs_coinbase_settings[webhook_secret]" type="password" class="regular-text" value="<?php echo $wpvs_coinbase_settings['webhook_secret']; ?>"/>
                                                <p><a href="https://commerce.coinbase.com/dashboard/settings#webhooks" target="_blank">Create Webhook subscription</a></p>
                                                <p class="description">Add this endpoint to your Webhook subscriptions:</p>
                                                <p><strong><?php echo home_url('?wpvs-coinbase-callback-listener=cb'); ?></strong></p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h3 class="title"><?php _e('Accepted Coins', 'vimeo-sync-memberships'); ?></h3>
                                <p>These should match your <a href="https://commerce.coinbase.com/dashboard/settings" target="_blank">Active cryptocurrencies</a> in your Coinbase Commerce Dashboard.</p>
                                <table class="form-table">
                                    <tbody>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Bitcoin', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input type="checkbox" name="wpvs_coinbase_settings[accepted_currencies][]" value="BTC" <?php checked( in_array('BTC', $wpvs_coinbase_settings['accepted_currencies'])); ?> />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Bitcoin Cash', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input type="checkbox" name="wpvs_coinbase_settings[accepted_currencies][]" value="BCH" <?php checked( in_array('BCH', $wpvs_coinbase_settings['accepted_currencies'])); ?> />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Ethereum', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input type="checkbox" name="wpvs_coinbase_settings[accepted_currencies][]" value="ETH" <?php checked( in_array('ETH', $wpvs_coinbase_settings['accepted_currencies'])); ?> />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Litecoin', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input type="checkbox" name="wpvs_coinbase_settings[accepted_currencies][]" value="LTC" <?php checked( in_array('LTC', $wpvs_coinbase_settings['accepted_currencies'])); ?> />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('USD Coin', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input type="checkbox" name="wpvs_coinbase_settings[accepted_currencies][]" value="USDC" <?php checked( in_array('USDC', $wpvs_coinbase_settings['accepted_currencies'])); ?> />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="rvs-container">
                                <div class="rvs-col-12">
                                <input type="submit" class="button-primary" value="<?php _e('Save Coinbase Settings', 'vimeo-sync-memberships'); ?>" />
                                </div>
                            </div>
                        </div>
                        </form>
                <?php } if($current_tab == "coingate") { ?>
                        <h3>CoinGate <?php _e('Setup', 'vimeo-sync-memberships'); ?></h3>
                        <form method="post" action="options.php">
                            <?php settings_fields('wpvs-memberships-coingate');
                                global $wpvs_coingate_settings;
                                global $rvs_currency;
                                if( ! isset($wpvs_coingate_settings['enabled']) ) {
                                    $wpvs_coingate_settings['enabled'] = 0;
                                }

                                if( ! isset($wpvs_coingate_settings['auth_token']) ) {
                                    $wpvs_coingate_settings['auth_token'] = "";
                                }

                                if( ! isset($wpvs_coingate_settings['test_auth_token']) ) {
                                    $wpvs_coingate_settings['test_auth_token'] = "";
                                }

                                if( ! isset($wpvs_coingate_settings['receiving_currency']) ) {
                                    $wpvs_coingate_settings['receiving_currency'] = "";
                                } ?>
                        <div class="rvs-container rvs-box">
                            <div class="rvs-col-12">
                                <table class="form-table">
                                    <tbody>

                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Enable CoinGate', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_coingate_settings[enabled]" name="wpvs_coingate_settings[enabled]" type="checkbox" value="1" <?php checked(1, $wpvs_coingate_settings['enabled']); ?> />
                                                <label class="description" for="wpvs_coingate_settings[enabled]"><?php _e('On / Off', 'vimeo-sync-memberships'); ?></label>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                                <h3 class="title"><?php _e('API Keys', 'vimeo-sync-memberships'); ?></h3>
                                <table class="form-table">
                                    <tbody>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Live Auth Token (API v2)', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_coingate_settings[auth_token]" name="wpvs_coingate_settings[auth_token]" type="text" class="regular-text" value="<?php echo $wpvs_coingate_settings['auth_token']; ?>"/>
                                                <p><a href="https://coingate.com/ref/WPVS" target="_blank">Create CoinGate Account</a></p>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Test Auth Token (API v2)', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <input id="wpvs_coingate_settings[test_auth_token]" name="wpvs_coingate_settings[test_auth_token]" type="text" class="regular-text" value="<?php echo $wpvs_coingate_settings['test_auth_token']; ?>"/>
                                                <p><a href="https://sandbox.coingate.com/sign_up" target="_blank">Create CoinGate Sandbox Account</a></p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h3 class="title"><?php _e('Currencies', 'vimeo-sync-memberships'); ?></h3>
                                <table class="form-table">
                                    <tbody>
                                        <tr valign="top">
                                            <th scope="row" valign="top">
                                                <?php _e('Receiving Currency', 'vimeo-sync-memberships'); ?>
                                            </th>
                                            <td>
                                                <select id="wpvs_coingate_settings[receiving_currency]" name="wpvs_coingate_settings[receiving_currency]">
                                                    <option value="BTC" <?php selected("BTC", $wpvs_coingate_settings['receiving_currency']); ?>>Bitcoin</option>
                                                    <option value="USD" <?php selected("USD", $wpvs_coingate_settings['receiving_currency']); ?>>United States Dollar</option>
                                                    <option value="EUR" <?php selected("EUR", $wpvs_coingate_settings['receiving_currency']); ?>>Euro</option>
                                                </select>
                                                <p class="description"><?php _e('The currency in which you wish to receive your payments.', 'vimeo-sync-memberships'); ?></p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="rvs-container">
                                <div class="rvs-col-12">
                                <input type="submit" class="button-primary" value="<?php _e('Save CoinGate Settings', 'vimeo-sync-memberships'); ?>" />
                                </div>
                            </div>
                        </div>
                        </form>
                <?php } ?>
                </div>
                <div class="col-4">
                    <h3><?php _e('Live', 'vimeo-sync-memberships'); ?> / <?php _e('Testing', 'vimeo-sync-memberships'); ?></h3>
                    <div class="onoffswitch">
                        <input type="checkbox" name="rvs_test_mode" class="onoffswitch-checkbox" id="rvs_test_mode" <?php checked("on", $rvs_live_mode); ?> value="<?php echo $rvs_live_mode; ?>">
                        <label id="rvs-change-test-mode" class="onoffswitch-label" for="rvs_test_mode">
                            <span class="onoffswitch-inner onoffswitch-mode"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                    <h3><?php _e('Currency', 'vimeo-sync-memberships'); ?></h3>
                    <select id="rvs_currency_setting" name="rvs_currency_setting">
                        <option value="USD" <?php selected("USD", $rvs_currency); ?>>United States (USD)</option>
                        <option value="CAD" <?php selected("CAD", $rvs_currency); ?>>Canadian (CAD)</option>
                        <option value="AUD" <?php selected("AUD", $rvs_currency); ?>>Australian (AUD)</option>
                        <option value="BRL" <?php selected("BRL", $rvs_currency); ?>>Brazilian (BRL)</option>
                        <option value="CZK" <?php selected("CZK", $rvs_currency); ?>>Czech Crown (CZK)</option>
                        <option value="DKK" <?php selected("DKK", $rvs_currency); ?>>Danish krone (DKK)</option>
                        <option value="EUR" <?php selected("EUR", $rvs_currency); ?>>Euro (EUR)</option>
                        <option value="HKD" <?php selected("HKD", $rvs_currency); ?>>Hong Kong (HKD)</option>
                        <option value="INR" <?php selected("INR", $rvs_currency); ?>>Indian Rupee (INR)</option>
                        <option value="ILS" <?php selected("ILS", $rvs_currency); ?>>Israeli (ILS)</option>
                        <option value="JPY" <?php selected("JPY", $rvs_currency); ?>>Japanese Yen (JPY)</option>
                        <option value="MXN" <?php selected("MXN", $rvs_currency); ?>>Mexican Peso (MXN)</option>
                        <option value="NZD" <?php selected("NZD", $rvs_currency); ?>>New Zealand (NZD)</option>
                        <option value="NOK" <?php selected("NOK", $rvs_currency); ?>>Norwegian (NOK)</option>
                        <option value="PHP" <?php selected("PHP", $rvs_currency); ?>>Philippine Peso (PHP)</option>
                        <option value="PLN" <?php selected("PLN", $rvs_currency); ?>>Polish (PLN)</option>
                        <option value="GBP" <?php selected("GBP", $rvs_currency); ?>>Pound (GBP)</option>
                        <option value="RUB" <?php selected("RUB", $rvs_currency); ?>>Russian (RUB)</option>
                        <option value="SGD" <?php selected("SGD", $rvs_currency); ?>>Singapore (SGD)</option>
                        <option value="SEK" <?php selected("SEK", $rvs_currency); ?>>Swedish Krona (SEK)</option>
                        <option value="CHF" <?php selected("CHF", $rvs_currency); ?>>Swiss Franc (CHF)</option>
                        <option value="THB" <?php selected("THB", $rvs_currency); ?>>Thai Baht (THB)</option>
                    </select>
                    <?php
                        $wpvs_custom_currency = get_option('rvs_currency_custom_label', $rvs_currency);
                        if( empty($wpvs_custom_currency)) {
                            $wpvs_custom_currency_option = strtoupper($rvs_currency);
                        } else {
                            $wpvs_custom_currency_option = $wpvs_custom_currency;
                        }
                        $wpvs_currency_position = get_option('wpvs_currency_position', "after");
                    ?>
                    <h3><?php _e('Custom Currency Label', 'vimeo-sync-memberships'); ?></h3>
                    <p>If you want to change the currency label. (optional)</p>
                    <input type="text" name="wpvs_custom_currency" id="wpvs_custom_currency" placeholder="Example: $" value="<?php echo $wpvs_custom_currency; ?>" />
                    <select id="wpvs_currency_position_setting" name="wpvs_currency_position_setting">
                        <option value="after" <?php selected("after", $wpvs_currency_position); ?>>After Amount (5.00 <?php echo $wpvs_custom_currency_option; ?>)</option>
                        <option value="before" <?php selected("before", $wpvs_currency_position); ?>>Before Amount (<?php echo $wpvs_custom_currency_option; ?> 5.00)</option>
                    </select>
                    <label class="button button-primary" id="wpvs-save-custom-currency">Save Currency Settings</label>
                </div>
            </div>
        </div>
    </div>
</div>
