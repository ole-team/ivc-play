<?php

function wpvs_secure_admin_ajax() {
    $wpvs_ajax_request_allowed = false;
    if(is_user_logged_in() && current_user_can('manage_options')) {
        $wpvs_ajax_request_allowed = true;
    }
    return $wpvs_ajax_request_allowed;
}

add_action( 'wp_ajax_rvs_update_test_mode', 'rvs_update_test_mode' );

function rvs_update_test_mode() {
    if( wpvs_secure_admin_ajax() && isset($_POST['test_mode'])) {
        update_option('rvs_live_mode', $_POST['test_mode']);
        echo $_POST['test_mode'];
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_rvs_update_currency', 'rvs_update_currency' );

function rvs_update_currency() {
    if( wpvs_secure_admin_ajax() && isset($_POST['currency']) ) {
        update_option('rvs_currency', $_POST['currency']);
    }
    wp_die();
}

add_action( 'wp_ajax_rvs_update_currency_text', 'rvs_update_currency_text' );

function rvs_update_currency_text() {
    if( wpvs_secure_admin_ajax() ) {
        if(isset($_POST['custom_label']) && ! empty($_POST['custom_label'])) {
            update_option('rvs_currency_custom_label', $_POST['custom_label']);
        } else {
            update_option('rvs_currency_custom_label', "");
        }
        update_option('wpvs_currency_position', $_POST['currency_position']);
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

// CREATE NEW MEMBERSHIP IN WORDPRESS
add_action( 'wp_ajax_rvs_create_new_membership', 'rvs_create_new_membership' );

function rvs_create_new_membership() {
    if( wpvs_secure_admin_ajax() ) {
        if( $_POST && isset($_POST['id_of_plan']) )  {
            $plan_id = $_POST['id_of_plan'];
            $wpvs_membership_manager = new WPVS_Membership_Manager($plan_id);

            if( ! $wpvs_membership_manager->membership_plan_exists() ) {

                $plan_name = $_POST['name_of_plan'];
                $plan_amount = $_POST['price_of_plan'];
                $plan_description = sanitize_textarea_field($_POST['description_of_plan']);
                $plan_short_description = sanitize_textarea_field($_POST['short_description']);
                $plan_interval = $_POST['interval_of_plan'];
                $plan_interval_count = $_POST['interval_count'];
                $plan_trial_frequency = $_POST['trial_frequency'];
                $plan_trial_frequency_interval = intval($_POST['trial_period']);
                $hide_membership = intval($_POST['hide_membership']);
                $create_role = false;

                if( isset($_POST['create_role']) && ! empty($_POST['create_role']) ) {
                    $create_role = true;
                }

                if(strpos($plan_amount, '.')) {
                    $plan_amount = str_replace(".", "", $plan_amount);
                } else {
                    $plan_amount = $plan_amount * 100;
                }

                $membership_created = $wpvs_membership_manager->create_new_membership_plan(
                    $plan_name,
                    $plan_amount,
                    $plan_description,
                    $plan_short_description,
                    $plan_interval,
                    $plan_interval_count,
                    $plan_trial_frequency,
                    $plan_trial_frequency_interval,
                    $hide_membership,
                    $create_role
                );

                if( $membership_created ) {
                    if( $create_role ) {
                        $wpvs_membership_manager->create_membership_role($plan_name);
                    }
                    echo admin_url('admin.php?page=rvs-memberships');
                } else {
                    $wpvs_error_message = __('Something went wrong creating your membership', 'vimeo-sync-memberships');
                    rvs_exit_ajax($wpvs_error_message, 400);
                }

            } else {
                $wpvs_error_message = __('Plan with this ID already exists.', 'vimeo-sync-memberships');
                rvs_exit_ajax($wpvs_error_message, 400);
            }

        } else {
            $wpvs_error_message = __('Missing fields for Membership.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

// CREATE NEW PAYPAL PLAN

add_action( 'wp_ajax_rvs_create_paypal_plan', 'rvs_create_paypal_plan' );

function rvs_create_paypal_plan() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_paypal_settings;
        global $rvs_paypal_enabled;
        global $rvs_currency;
        //GET MEMBERSHIPS
        $membershipList = get_option('rvs_membership_list');

        if(isset($_POST['id_of_plan']) && ! empty($_POST['id_of_plan']))  {

            $plan_id = $_POST['id_of_plan'];
            $wpvs_membership_manager = new WPVS_Membership_Manager($plan_id);

            if( $wpvs_membership_manager->membership_plan_exists() ) {
                $membership_plan = $wpvs_membership_manager->get_plan();
            } else {
                $wpvs_error_message = __('You must create a plan first.', 'vimeo-sync-memberships');
                rvs_exit_ajax($wpvs_error_message, 400);
            }

            if( ! empty($membership_plan) ) {
                if( isset($membership_plan['short_description']) && ! empty($membership_plan['short_description']) ) {
                    $short_description = $membership_plan['short_description'];
                } else {
                    $short_description = null;
                }

                $plan_manager = new WPVS_PayPal_Plan_Creator(
                    $membership_plan['id'],
                    $membership_plan['name'],
                    $membership_plan['amount'],
                    $membership_plan['description'],
                    $short_description,
                    $membership_plan['interval'],
                    $membership_plan['interval_count'],
                    "0",
                    $membership_plan['trial_frequency'],
                    $membership_plan['trial'],
                    0,
                    $membership_plan,
                    "infinite",
                    null
                );

                $plan_created = $plan_manager->create_paypal_billing_plan();

                if( ! empty($plan_created) && isset($plan_created['created']) && $plan_created['created'] ) {
                    $paypal_plan_added = $wpvs_membership_manager->update_membership($plan_created['updated_plan']);
                    if( $paypal_plan_added ) {
                        echo 'PayPal plan successfully created!';
                    } else {
                        $wpvs_error_message = __('Error adding PayPal Plan to Membership.', 'vimeo-sync-memberships');
                        rvs_exit_ajax($wpvs_error_message, 400);
                    }
                } else {
                    rvs_exit_ajax($plan_created['error'], 400);
                }
            }

        } else {
            $wpvs_error_message = __('Missing Plan ID parameter.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

// DELETE MEMBERSHIP

add_action( 'wp_ajax_rvs_delete_membership', 'rvs_delete_membership' );

function rvs_delete_membership() {
    if( wpvs_secure_admin_ajax() ) {
        $error_message = "";
        if ( $_POST && isset($_POST['plan_id']) ) {
            $plan_id = $_POST['plan_id'];
            $remove_user_plans = false;
            $remove_role = false;
            $remove_stripe_plan = false;

            if( isset($_POST['remove_user_plans']) && ! empty($_POST['remove_user_plans']) ) {
                $remove_user_plans = true;
            }

            if( isset($_POST['remove_role']) && ! empty($_POST['remove_role']) ) {
                $remove_role = true;
            }

            if( isset($_POST['remove_stripe_plan']) && ! empty($_POST['remove_stripe_plan']) ) {
                $remove_stripe_plan = true;
            }

            $wpvs_membership_manager = new WPVS_Membership_Manager($plan_id);

            if( $wpvs_membership_manager->membership_plan_exists() ) {
                $membership_deleted = $wpvs_membership_manager->delete_plan($remove_user_plans, $remove_role, $remove_stripe_plan);
            } else {
                $wpvs_error_message = __('Plan with this ID does not exist.', 'vimeo-sync-memberships');
                rvs_exit_ajax($wpvs_error_message, 400);
            }

            if( isset($membership_deleted['error']) ) {
                rvs_exit_ajax($membership_deleted['error'], 400);
            } else {
                echo admin_url('admin.php?page=rvs-memberships');
            }
        } else {
            $wpvs_error_message = __('Missing Plan ID.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

// DELETE STRIPE PLAN

add_action( 'wp_ajax_rvs_delete_stripe_plan', 'rvs_delete_stripe_plan' );

function rvs_delete_stripe_plan() {
    if( wpvs_secure_admin_ajax() ) {
        //GET MEMBERSHIPS
        $membershipList = get_option('rvs_membership_list');
        $error_message = "";
        if(  $_POST && isset($_REQUEST['id_of_plan']) )  {

            $plan_id = $_REQUEST['id_of_plan'];

            $wpvs_membership_manager = new WPVS_Membership_Manager($plan_id);

            if( $wpvs_membership_manager->membership_plan_exists() ) {
                $membership_deleted = $wpvs_membership_manager->delete_stripe_plan();
            } else {
                $wpvs_error_message = __('Plan with this ID does not exist.', 'vimeo-sync-memberships');
                rvs_exit_ajax($wpvs_error_message, 400);
            }

            if( isset( $stripe_plan_deleted['error']) ) {
                rvs_exit_ajax($stripe_plan_deleted['error'], 400);
            }

        } else {
            $wpvs_error_message = __('Missing Plan ID.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

// DELETE PAYPAL PLAN

add_action( 'wp_ajax_rvs_delete_paypal_plan', 'rvs_delete_paypal_plan' );

function rvs_delete_paypal_plan() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_paypal_settings;
        //GET MEMBERSHIPS
        $membershipList = get_option('rvs_membership_list');

        if(isset($_REQUEST['id_of_plan']))  {

            $plan_id = $_REQUEST['id_of_plan'];

            if(!empty($membershipList)) {
                foreach($membershipList as $key => &$plan) {
                    if ($plan['id'] == $plan_id) {
                        $delete_this_plan = $plan;
                        $remove_key = $key;
                        break;
                    }
                }

            } else {
                status_header("400");
                echo "No memberships created.";
                exit;
            }

            //DELETE PAYPAL PLAN LIVE

            if(isset($delete_this_plan['paypal']) && isset($delete_this_plan['paypal']['plan_id']) && $delete_this_plan['paypal']['plan_id'] != "") {

                require_once(RVS_MEMBERS_BASE_DIR . '/paypal/autoload.php');

                $delete_both_live_and_test = 2;

                while($delete_both_live_and_test > 0) {

                    if($delete_both_live_and_test == 2) {
                        $client_id = $rvs_paypal_settings['live_client_id'];
                        $client_secret = $rvs_paypal_settings['live_client_secret'];
                        $api_mode = array('mode' => 'live');
                        $paypal_key = "paypal";
                        $get_memberships = 'rvs_user_memberships';
                    }

                    if($delete_both_live_and_test == 1) {
                        $client_id = $rvs_paypal_settings['sandbox_client_id'];
                        $client_secret = $rvs_paypal_settings['sandbox_client_secret'];
                        $api_mode = array('mode' => 'sandbox');
                        $paypal_key = "paypal_test";
                        $get_memberships = 'rvs_user_memberships_test';
                    }

                    if($client_id == "" || $client_secret == "") {
                        status_header("400");
                        if($create_both_live_and_test == 2) {
                            _e('You have PayPal enabled. However, you have not entered your live Client ID or Client Secret key.', 'vimeo-sync-memberships');
                        } else if($create_both_live_and_test == 1) {
                            _e('You have PayPal enabled. However, you have not entered your sandbox Client ID or Client Secret key.', 'vimeo-sync-memberships');
                        } else {
                            _e('You have PayPal enabled. However, you have not entered your Client ID or Client Secret key.', 'vimeo-sync-memberships');
                        }
                        exit;
                    } else {

                        $apiContext = new \PayPal\Rest\ApiContext(
                            new \PayPal\Auth\OAuthTokenCredential(
                                $client_id,  // ClientID
                                $client_secret  // ClientSecret
                            )
                        );

                        $apiContext->setConfig($api_mode);

                        try {
                            $paypal_plan = PayPal\Api\Plan::get($delete_this_plan[$paypal_key]['plan_id'], $apiContext);
                            $result = $paypal_plan->delete($apiContext);
                        } catch (Exception $ex) {
                            if(empty($paypal_plan)) {
                                unset($delete_this_plan['paypal']);
                                unset($delete_this_plan['paypal_test']);
                                $membershipList[$remove_key] = $delete_this_plan;
                                update_option('rvs_membership_list', $membershipList);
                            }
                        }

                    }
                    $delete_both_live_and_test--;
                }
                unset($delete_this_plan['paypal']);
                unset($delete_this_plan['paypal_test']);
                $membershipList[$remove_key] = $delete_this_plan;
                update_option('rvs_membership_list', $membershipList);
            }
        } else {
            $wpvs_error_message = __('Missing Plan ID.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

// UPDATE MEMBERSHIP

add_action( 'wp_ajax_rvs_update_membership', 'rvs_update_membership' );

function rvs_update_membership() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_paypal_settings;
        global $rvs_currency;
        $membershipList = get_option('rvs_membership_list');
        $plan_id = $_POST['id_of_plan'];
        $wpvs_membership_manager = new WPVS_Membership_Manager($plan_id);

        if( $wpvs_membership_manager->membership_plan_exists() ) {
            $plan = $wpvs_membership_manager->get_plan();
            $plan_trial = 0;
            $planName = $_POST['name_of_plan'];
            $planDescription = sanitize_text_field($_POST['description_of_plan']);
            $plan_short_description = sanitize_text_field($_POST['short_description']);
            $trial_frequency = $_POST['trial_frequency'];
            $plan_trial = intval($_POST['trial_period']);
            $hide_membership = intval($_POST['hide_membership']);
            $create_role = false;
            if( isset($_POST['create_role']) && ! empty($_POST['create_role']) ) {
                $create_role = true;
            }
            $skip_trial_updates = false;

            if( $wpvs_membership_manager->trial_frequency == $trial_frequency && $wpvs_membership_manager->trial_frequency_interval == $plan_trial) {
                $skip_trial_updates = true;
            }

            // assign new values

            $plan['name'] = $planName;
            $plan['description'] = $planDescription;
            $plan['short_description'] = $plan_short_description;
            $plan['trial_frequency'] = $trial_frequency;
            $plan['trial'] = $plan_trial;
            $plan['hidden'] = $hide_membership;
            $plan['has_role'] = $create_role;
            $update_this_plan = $plan;

            if( $trial_frequency == "none") {
                $plan_trial = 0;
            }

            if(!$skip_trial_updates) {
                if(isset($update_this_plan['stripe']) && isset($update_this_plan['stripe']['plan_id']) && $update_this_plan['stripe']['plan_id'] != "") {
                    global $wpvs_stripe_admin_ajax_requests;
                    if( ! empty($plan_trial) ) {
                        if($trial_frequency == "week") {
                            $plan_trial = $plan_trial*7;
                        }
                        if($trial_frequency == "month") {
                            $plan_trial = $plan_trial*30;
                        }
                    }
                    $stripe_plan_data = array(
                        'trial_period_days' => $plan_trial,
                    );

                    $update_both_live_and_test = 2;

                    while($update_both_live_and_test > 0) {

                        if($update_both_live_and_test == 2) {
                            $wpvs_stripe_admin_ajax_requests->set_api_mode('live');
                        }

                        if($update_both_live_and_test == 1) {
                            $wpvs_stripe_admin_ajax_requests->set_api_mode('test');
                        }

                        $stripe_plan_updated = $wpvs_stripe_admin_ajax_requests->wpvs_update_stripe_plan($plan_id, $stripe_plan_data);
                        $update_both_live_and_test--;
                    }
                }

                //RESET PAYPAL REPEATING COUPON PLANS
                if( isset($update_this_plan['paypal_coupon_plans']) && ! empty($update_this_plan['paypal_coupon_plans']) ) {
                    $paypal_coupon_plans = $update_this_plan['paypal_coupon_plans'];
                    foreach($paypal_coupon_plans as $c_key => $coupon_plan) {
                        if( $coupon_plan['trial'] && $coupon_plan['trial_frequency'] != 'none' ) {
                            if( isset($coupon_plan['paypal']) && isset($coupon_plan['paypal']['plan_id']) && ! empty($coupon_plan['paypal']['plan_id']) ) {
                                $wpvs_paypal_manager =  new WPVS_PayPal_Plan_Manager('live', $coupon_plan['paypal']['plan_id']);
                                $wpvs_paypal_manager->delete_paypal_billing_plan();
                            }

                            if( isset($coupon_plan['paypal_test']) && isset($coupon_plan['paypal_test']['plan_id']) && ! empty($coupon_plan['paypal_test']['plan_id']) ) {
                                $wpvs_paypal_manager =  new WPVS_PayPal_Plan_Manager('sandbox', $coupon_plan['paypal_test']['plan_id']);
                                $wpvs_paypal_manager->delete_paypal_billing_plan();
                            }
                            unset($paypal_coupon_plans[$c_key]);
                        }
                    }
                    $update_this_plan['paypal_coupon_plans'] = $paypal_coupon_plans;
                }
            }
            if( $create_role ) {
                $wpvs_membership_manager->create_membership_role(null);
            } else {
                $wpvs_membership_manager->remove_membership_role();
            }

            $wpvs_membership_manager->update_membership($update_this_plan);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

// CREATE NEW COUPON CODE

add_action( 'wp_ajax_rvs_create_new_coupon', 'rvs_create_new_coupon' );

function rvs_create_new_coupon() {
    if( wpvs_secure_admin_ajax() ) {
        global $wpvs_stripe_options;
        global $rvs_currency;
        $rvs_coupon_codes = get_option('rvs_coupon_codes');
        if( $_POST && isset($_POST['coupon_id']) )  {
            $new_coupon_id = $_POST['coupon_id'];
            $new_coupon_name= $_POST['coupon_name'];
            $new_coupon_type = $_POST['discount_type'];
            $new_coupon_amount = $_POST['amount_off'];
            $new_coupon_duration = $_POST['coupon_duration'];

            $coupon_manager = new WPVS_Coupon_Code_Manager($new_coupon_id);

            if( $coupon_manager->coupon_exists() ) {
                $wpvs_error_message = __('Missing Plan ID.', 'vimeo-sync-memberships');
                rvs_exit_ajax($wpvs_error_message, 400);
            }

            // Check if max uses was set
            if(isset($_POST['coupon_max_uses']) && $_POST['coupon_max_uses'] != "") {
                $new_coupon_uses = $_POST['coupon_max_uses'];
            } else {
                $new_coupon_uses = null;
            }

            // Check if max uses per customer was set
            if(isset($_POST['coupon_max_uses_customer']) && ! empty($_POST['coupon_max_uses_customer']) ) {
                $new_coupon_uses_customer = $_POST['coupon_max_uses_customer'];
            } else {
                $new_coupon_uses_customer = null;
            }

            // Check if coupon has repeating duration
            if($new_coupon_duration == "repeating") {
                $new_coupon_month_duration = $_POST['coupon_month_duration'];
            } else {
                $new_coupon_month_duration = null;
            }

            // Check if discount is amount or percentage
            if($new_coupon_type == "amount") {
                if(strpos($new_coupon_amount, '.')) {
                    $new_coupon_amount = str_replace(".", "", $new_coupon_amount);
                } else {
                    $new_coupon_amount = $new_coupon_amount * 100;
                }
                $discount_key = 'amount_off';
            } else {
                if(strpos($new_coupon_amount, '%')) {
                    $new_coupon_amount = str_replace("%", "", $new_coupon_amount);
                }
                $discount_key = 'percent_off';
            }

            $new_wpvs_coupon = array(
                'id' => $new_coupon_id,
                'name' => $new_coupon_name,
                'type' => $new_coupon_type,
                'amount' => $new_coupon_amount,
                'duration' => $new_coupon_duration,
                'month_duration' => $new_coupon_month_duration,
                'max_uses' => $new_coupon_uses,
                'max_uses_customer' => $new_coupon_uses_customer
            );

            $created_success = true;

            // CHECK IF STRIPE IS ACTIVE
            if(isset($wpvs_stripe_options['enabled']) && $wpvs_stripe_options['enabled']) {
                global $wpvs_stripe_admin_ajax_requests;
                $new_stripe_coupon = array(
                    "id" => $new_coupon_id,
                    $discount_key => $new_coupon_amount,
                    "currency" => $rvs_currency,
                    "duration" => $new_coupon_duration,
                    "duration_in_months" => $new_coupon_month_duration,
                    "max_redemptions" => $new_coupon_uses
                );
                $create_both_live_and_test = 2;
                while($create_both_live_and_test > 0) {
                    if($create_both_live_and_test == 2) {
                        $wpvs_stripe_admin_ajax_requests->set_api_mode('live');
                    }
                    if($create_both_live_and_test == 1) {
                        $wpvs_stripe_admin_ajax_requests->set_api_mode('test');
                    }
                    $stripe_coupon_created = $wpvs_stripe_admin_ajax_requests->create_stripe_coupon_code($new_stripe_coupon);
                    $created_success = $stripe_coupon_created->created;
                    $create_both_live_and_test--;
                }
            }


            if($created_success) {
                $new_coupon_created = $coupon_manager->create_new_coupon($new_coupon_name, $new_coupon_type, $new_coupon_amount, $new_coupon_duration, $new_coupon_month_duration, $new_coupon_uses, $new_coupon_uses_customer);
                if( $new_coupon_created ) {
                    echo admin_url('admin.php?page=rvs-coupon-codes');
                } else {
                    $wpvs_error_message = __('Error creating Coupon Code.', 'vimeo-sync-memberships');
                    rvs_exit_ajax($wpvs_error_message, 400);
                }
            }
        } else {
            $wpvs_error_message = __('Missing coupon information.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

// DELETE COUPON CODE

add_action( 'wp_ajax_rvs_remove_coupon', 'rvs_remove_coupon' );

function rvs_remove_coupon() {
    if( wpvs_secure_admin_ajax() ) {
        $error_message = "";
        $rvs_coupon_codes = get_option('rvs_coupon_codes');
        if(isset($_POST['coupon_id']))  {
            $remove_coupon_id = $_POST['coupon_id'];
            $wpvs_coupon_code = new WPVS_Coupon_Code_Manager($remove_coupon_id);
            if( $wpvs_coupon_code->coupon_exists() ) {
                $wpvs_coupon_code->delete_coupon();
            } else {
                $wpvs_error_message = __('Couponn with this ID does not exist.', 'vimeo-sync-memberships');
                rvs_exit_ajax($wpvs_error_message, 400);
            }
        } else {
            $wpvs_error_message = __('Missing coupon ID.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_set_membership_man', 'wpvs_set_membership_man' );

function wpvs_set_membership_man() {
    if( wpvs_secure_admin_ajax() ) {
        if(isset($_POST["user_id"]) && isset($_POST["plan_id"])) {
            global $rvs_live_mode;
            if($rvs_live_mode == "on") {
                $test_member = false;
            } else {
                $test_member = true;
            }
            $user_id = $_POST["user_id"];
            $member = get_user_by('id', $user_id);
            $wpvs_customer = new WPVS_Customer($member);

            $plan_id = $_POST["plan_id"];
            $wpvs_membership_manager = new WPVS_Membership_Manager($plan_id);
            if( $wpvs_membership_manager->membership_plan_exists() ) {
                $add_membership = $wpvs_membership_manager->get_plan();
            } else {
                $wpvs_error_message = __('Membership does not exist', 'vimeo-sync-memberships');
                rvs_exit_ajax($wpvs_error_message, 400);
            }
            $user_memberships = $wpvs_customer->get_memberships();
            if( ! empty($user_memberships) ) {
                foreach($user_memberships as $member_plan) {
                    if($member_plan['plan'] == $plan_id) {
                        $wpvs_error_message = $member->user_login . ' ' . __('is already subscribed to the', 'vimeo-sync-memberships') . ' ' . $member_plan['name'] . ' ' . __('plan', 'vimeo-sync-memberships');
                        rvs_exit_ajax($wpvs_error_message, 400);
                    }
                }
            } else {
                $user_memberships = array();
            }

            if( ! empty($add_membership) ) {
                $new_membership = array(
                    'plan'      => $add_membership['id'],
                    'id'        => 'wpvm-'.$add_membership['id'],
                    'amount'    => '0.00',
                    'name'      => $add_membership['name'],
                    'interval'  => 'none',
                    'ends'      => 'never',
                    'status'    => 'active',
                    'type'      => 'manual'
                );
                $wpvs_customer->add_membership($new_membership);
                echo json_encode($new_membership);
                wpvs_add_new_member($user_id, $test_member);
            }

        } else {
            $wpvs_error_message = __('Missing User ID or Plan ID.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

function wpvs_get_stripe_payments_ajax_request() {
    if( wpvs_secure_admin_ajax() && wp_verify_nonce($_GET['wpvs_admin_nonce'], 'wpvs-admin-nonce') && isset($_GET['user_id']) && ! empty($_GET['user_id']) ) {
        $user_id = intval($_GET['user_id']);
        $payments_offset = $_GET['offset'];
        if( empty($payments_offset) ) {
            $payments_offset = 0;
        }
        global $rvs_live_mode;
        $test_payments = true;
        if($rvs_live_mode == "on") {
            $test_payments = false;
        }
        $found_payments = array();
        $customer_stripe_payments = wpvs_get_customer_db_stripe_payments($user_id, $test_payments, $payments_offset);
        if( ! empty($customer_stripe_payments) ) {
            foreach($customer_stripe_payments as $payment) {
                $payment_date = date( 'M d, Y', $payment->time );
                if( empty($payment->type) ) {
                    $payment_type = __('unknown', 'vimeo-sync-memberships');
                } else {
                    if( $payment->type == 'subscription' ) {
                        $payment_type = __('Renewal', 'vimeo-sync-memberships');
                    }
                    if( $payment->type == 'rental' ) {
                        if( ! empty($payment->productid) ) {
                            $admin_url_str = 'post.php?post='.$payment->productid.'&action=edit';
                            $edit_product_link = admin_url($admin_url_str);
                            $product_title = get_the_title($payment->productid);
                        }
                        $payment_type = '<small>'.__('(Rental)', 'vimeo-sync-memberships').'</small> <a href="'.$edit_product_link.'">'.$product_title.'</a>';
                    }
                    if( $payment->type == 'purchase' ) {
                        if( ! empty($payment->productid) ) {
                            $admin_url_str = 'post.php?post='.$payment->productid.'&action=edit';
                            $edit_product_link = admin_url($admin_url_str);
                            $product_title = get_the_title($payment->productid);
                        }
                        $payment_type = '<small>'.__('(Purchase)', 'vimeo-sync-memberships').'</small> <a href="'.$edit_product_link.'">'.$product_title.'</a>';
                    }
                    if( $payment->type == 'termpurchase' ) {
                        if( ! empty($payment->productid) ) {
                            $admin_url_str = 'term.php?taxonomy=rvs_video_category&tag_ID='.$payment->productid.'&post_type=rvs_video';
                            $edit_product_link = admin_url($admin_url_str);
                            $product_term = get_term_by('id', intval($payment->productid), 'rvs_video_category');
                            $product_title = $product_term->name;
                        }
                        $payment_type = '<small>'.__('(Access to)', 'vimeo-sync-memberships').'</small> <a href="'.$edit_product_link.'">'.$product_title.'</a>';
                    }
                }
                $add_payment = array(
                    'amount' => $payment->amount,
                    'paymentid' => $payment->paymentid,
                    'time' => $payment_date,
                    'type' => $payment_type,
                    'refunded' => intval($payment->refunded)
                );
                $found_payments[] =  $add_payment;
            }

        }
        echo json_encode($found_payments);
    } else {
        rvs_exit_ajax(__('You are not allowed to perform this request', 'vimeo-sync-memberships'), 400);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_get_stripe_payments_ajax_request', 'wpvs_get_stripe_payments_ajax_request' );


add_action( 'wp_ajax_wpvs_search_members_ajax', 'wpvs_search_members_ajax' );
function wpvs_search_members_ajax() {
    if( wpvs_secure_admin_ajax() ) {
        if( isset($_POST["search_term"]) && ! empty($_POST["search_term"]) ) {
            $user_search_term = $_POST["search_term"];
            global $rvs_live_mode;
            if($rvs_live_mode == "off") {
                $get_memberships = 'rvs_user_memberships_test';
            } else {
                $get_memberships = 'rvs_user_memberships';
            }
            $wpvs_members_list = array();
            $user_args = array(
                'blog_id'      => $GLOBALS['blog_id'],
                'fields'       => array('ID', 'user_login')
            );

            $user_args['meta_query'] = array(
                'relation' => 'OR',
                array(
                    'key'     => 'first_name',
                    'value'   => $user_search_term,
                    'compare' => 'LIKE'
                ),
                array(
                    'key'     => 'last_name',
                    'value'   => $user_search_term,
                    'compare' => 'LIKE'
                ),
                array(
                    'key'     => 'user_email',
                    'value'   => $user_search_term,
                    'compare' => 'LIKE'
                ),
            );

            $found_members = get_users( $user_args );

            if( empty($found_members) ) {
                unset($user_args['meta_query']);
                $user_args['search'] = $user_search_term.'*';
                $found_members = get_users( $user_args );
            }

            if( ! empty($found_members) ) {
                foreach($found_members as $member) {
                    $user_memberships = get_user_meta($member->ID, $get_memberships, true);
                    $user_memberships_list = '';
                    if( ! empty($user_memberships) ) {
                        foreach($user_memberships as $key => $membership) {
                            if($membership == end($user_memberships)) {
                                $user_memberships_list .= $membership["name"];
                            } else {
                                $user_memberships_list .= $membership["name"] . ', ';
                            }
                        }
                    }
                    $add_member = array('user_id' => $member->ID, 'user_name' => $member->user_login, 'user_memberships' => $user_memberships_list);
                    $wpvs_members_list[] = $add_member;
                }
            }

            if( empty($wpvs_members_list) ) {
                echo 'No members found';
            } else {
                echo json_encode($wpvs_members_list);
            }

        } else {
            $wpvs_error_message = __('No search data provided.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

// SYNC SUBSCRIPTIONS

add_action( 'wp_ajax_wpvs_sync_stripe_subscription', 'wpvs_sync_stripe_subscription' );
function wpvs_sync_stripe_subscription() {
    if( wpvs_secure_admin_ajax() ) {
        if( isset($_POST["subscription"]) && ! empty($_POST["subscription"]) && isset($_POST["user_id"]) && ! empty($_POST["user_id"]) ) {
            $subscription_id = $_POST["subscription"];
            $user_id = intval($_POST["user_id"]);
            $new_status = null;
            global $rvs_live_mode;
            if($rvs_live_mode == "off") {
                $get_memberships = 'rvs_user_memberships_test';
            } else {
                $get_memberships = 'rvs_user_memberships';
            }

            global $wpvs_stripe_admin_ajax_requests;
            $stripe_subscription = $wpvs_stripe_admin_ajax_requests->get_subscription($subscription_id);
            if( isset($stripe_subscription->error) && ! empty($stripe_subscription->error) ) {
                rvs_exit_ajax($stripe_subscription->error, 400);
            } else {
                $user_memberships = get_user_meta($user_id, $get_memberships, true);
                if( ! empty($user_memberships) ) {
                    foreach($user_memberships as $key => &$membership) {
                        if($membership["id"] == $subscription_id) {
                            if($stripe_subscription->cancel_at_period_end) {
                                $new_status = 'canceled';
                                $membership["ends_next_date"] = 1;
                            } else {
                                $membership["status"] = $stripe_subscription->status;
                                $new_status =  $stripe_subscription->status;
                                if(isset($membership["ends_next_date"]) && ! empty($membership["ends_next_date"])) {
                                    $membership["ends_next_date"] = 0;
                                    unset($membership["ends_next_date"]);
                                }
                            }
                            if( ! empty($stripe_subscription->current_period_end) ) {
                                $membership["ends"] = $stripe_subscription->current_period_end;
                                if( $stripe_subscription->current_period_end > current_time('timestamp', 1) ) {
                                    $membership["reminder_sent"] = 0;
                                }
                            }
                            break;
                        }
                    }
                    update_user_meta($user_id, $get_memberships, $user_memberships);
                }
                $wpvs_stripe_admin_ajax_requests->set_user_id($user_id);
                if( empty($wpvs_stripe_admin_ajax_requests->customer_id()) && isset($stripe_subscription->customer) ) {
                    $wpvs_stripe_admin_ajax_requests->update_customer_id($stripe_subscription->customer);
                }
                echo json_encode(array('status' => $new_status, 'ends' => date( 'M d, Y', $stripe_subscription->current_period_end)));
            }
        } else {
            $error_message = __('No subscription id provided.', 'wp-video-subscriptions');
            rvs_exit_ajax($error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_sync_paypal_subscription', 'wpvs_sync_paypal_subscription' );
function wpvs_sync_paypal_subscription() {
    if( wpvs_secure_admin_ajax() ) {
        if( isset($_POST["subscription"]) && ! empty($_POST["subscription"]) && isset($_POST["user_id"]) && ! empty($_POST["user_id"]) ) {
            $subscription_id = $_POST["subscription"];
            $user_id = $_POST["user_id"];
            require_once(RVS_MEMBERS_BASE_DIR . '/paypal/autoload.php');
            global $rvs_paypal_settings;
            global $rvs_live_mode;

            if($rvs_live_mode == "off") {
                $get_memberships = 'rvs_user_memberships_test';
            } else {
                $get_memberships = 'rvs_user_memberships';
            }
            $paypal_config = rvs_create_paypal_config();

            $apiContext = new PayPal\Rest\ApiContext(
                new PayPal\Auth\OAuthTokenCredential(
                    $paypal_config['client'],  // ClientID
                    $paypal_config['secret']  // ClientSecret
                )
            );
            $apiContext->setConfig($paypal_config['api']);

            try {
                $paypal_agreement = PayPal\Api\Agreement::get($subscription_id, $apiContext);
                $user_memberships = get_user_meta($user_id, $get_memberships, true);
                $paypal_agreement_state = null;
                if(!empty($user_memberships)) {
                    foreach($user_memberships as $key => &$membership) {
                        if($membership["id"] == $subscription_id) {
                            $membership["status"] = $paypal_agreement->state;
                            $paypal_agreement_state = $paypal_agreement->state;
                            $paypal_timestamp = strtotime($membership["ends"]);
                            if( ! empty($paypal_agreement->agreement_details->next_billing_date) ) {
                                if( strtotime($paypal_agreement->agreement_details->next_billing_date) > $paypal_timestamp ) {
                                    $membership["ends"] = $paypal_agreement->agreement_details->next_billing_date;
                                    $membership["reminder_sent"] = 0;
                                }
                            } else {
                                if( $paypal_agreement->state == "Pending" && isset($paypal_agreement->start_date) ) {
                                    $agreement_starts = strtotime($paypal_agreement->start_date);
                                    if( $agreement_starts > strtotime($membership["ends"]) ) {
                                        $membership["ends"] = $paypal_agreement->start_date;
                                        $membership['status'] = 'Active';
                                        $membership["reminder_sent"] = 0;
                                        $paypal_agreement_state = 'Active';
                                    }
                                }
                            }
                            break;
                        }
                    }
                    update_user_meta($user_id, $get_memberships, $user_memberships);
                    if( ! empty($paypal_agreement->agreement_details->next_billing_date) ) {
                        $paypal_timestamp = strtotime($paypal_agreement->agreement_details->next_billing_date);
                    } else {
                        if( $paypal_agreement->state == "Pending" && isset($paypal_agreement->start_date) ) {
                            $paypal_timestamp = strtotime($paypal_agreement->start_date);
                            $paypal_agreement_state = 'Active';
                        }
                    }
                }

                echo json_encode(array('status' => $paypal_agreement_state, 'ends' => date( 'M d, Y', $paypal_timestamp)));
            } catch (Exception $ex) {
                $error_message = $ex->getMessage();
                rvs_exit_ajax($error_message, 400);
            }

        } else {
            $error_message = __('No subscription id provided.', 'wp-video-subscriptions');
            rvs_exit_ajax($error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_sync_coingate_subscription', 'wpvs_sync_coingate_subscription' );
function wpvs_sync_coingate_subscription() {
    if( wpvs_secure_admin_ajax() ) {
        if( isset($_POST["order_id"]) && ! empty($_POST["order_id"]) && isset($_POST["user_id"]) && ! empty($_POST["user_id"]) ) {
            $order_id = $_POST["order_id"];
            $user_id = $_POST["user_id"];
            $wpvs_current_time = current_time('timestamp', 1);
            require_once(RVS_MEMBERS_BASE_DIR . '/include-coingate/init.php');
            global $rvs_live_mode;
            global $wpvs_coingate_settings;
            $return_status = "";
            if($rvs_live_mode == "off") {
                $get_memberships = 'rvs_user_memberships_test';
            } else {
                $get_memberships = 'rvs_user_memberships';
            }

            $coingate_config = wpvs_coingate_config_request();
            \CoinGate\CoinGate::config(array(
                'environment'               => $coingate_config['environment'],
                'auth_token'                => $coingate_config['token'],
                'curlopt_ssl_verifypeer'    => $coingate_config['ssl']
            ));

            try {
                $cg_order = \CoinGate\Merchant\Order::find($order_id);
                if ($cg_order) {
                    $user_memberships = get_user_meta($user_id, $get_memberships, true);
                    if(!empty($user_memberships)) {
                        foreach($user_memberships as $key => &$membership) {
                            if($membership["id"] == $order_id) {
                                $membership["status"] = $cg_order->status;
                                if($cg_order->status == "canceled") {
                                    $return_status = 'new';
                                } else {
                                    $return_status = $cg_order->status;
                                }
                                break;
                            }
                        }
                        update_user_meta($user_id, $get_memberships, $user_memberships);
                    }
                    echo json_encode(array('status' => $return_status));
                }
            } catch (Exception $e) {
                rvs_exit_ajax($e->getMessage(), 400);
            }

        } else {
            $error_message = __('No order id provided.', 'wp-video-subscriptions');
            rvs_exit_ajax($error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_sync_coinbase_subscription', 'wpvs_sync_coinbase_subscription' );
function wpvs_sync_coinbase_subscription() {
    if( wpvs_secure_admin_ajax() ) {
        if( isset($_POST["charge_id"]) && ! empty($_POST["charge_id"]) && isset($_POST["user_id"]) && ! empty($_POST["user_id"]) ) {
            $charge_id = $_POST["charge_id"];
            $user_id = $_POST["user_id"];
            $wpvs_current_time = current_time('timestamp', 1);
            global $rvs_live_mode;
            global $wpvs_coinbase_manager;
            $return_status = "";
            if($rvs_live_mode == "off") {
                $get_memberships = 'rvs_user_memberships_test';
            } else {
                $get_memberships = 'rvs_user_memberships';
            }

            $wpvs_coinbase_payment = new \WPVS\Coinbase\WPVSCoinbasePayment();

            CoinbaseCommerce\ApiClient::init($wpvs_coinbase_manager->get_api_key());
            try {
                $pending_cb_order = CoinbaseCommerce\Resources\Charge::retrieve($charge_id);
                if ($pending_cb_order) {
                    $order_timeline = $pending_cb_order->timeline;
                    if( ! empty($order_timeline) ) {
                        $order_status = $wpvs_coinbase_payment->get_latest_charge_status($order_timeline);
                        if( isset($order_status['status']) ) {
                            $set_new_status = $order_status['status'];
                            $user_memberships = get_user_meta($user_id, $get_memberships, true);
                            if( ! empty($user_memberships) ) {
                                foreach($user_memberships as $key => &$membership) {
                                    if( isset($membership["trial_ends"]) && $membership["trial_ends"] > current_time('timestamp', 1) ) {
                                        rvs_exit_ajax(__('This customer is still on their Trial Period', 'vimeo-sync-memberships'), 400);
                                    } else {
                                        if($membership["id"] == $charge_id) {
                                            $membership["status"] = $set_new_status;
                                            if($set_new_status == "CANCELED") {
                                                $return_status = 'CANCELED';
                                            } else {
                                                $return_status = $set_new_status;
                                            }
                                            break;
                                        }
                                    }
                                }
                                update_user_meta($user_id, $get_memberships, $user_memberships);
                            }
                            echo json_encode(array('status' => $return_status));
                        }
                    }
                }
            } catch (Exception $e) {
                rvs_exit_ajax($e->getMessage(), 400);
            }
        } else {
            $error_message = __('No charge id provided.', 'wp-video-subscriptions');
            rvs_exit_ajax($error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

function wpvs_update_man_membership_date() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_live_mode;
        global $rvs_current_user;
        if($rvs_live_mode == "off") {
            $get_memberships = 'rvs_user_memberships_test';
        } else {
            $get_memberships = 'rvs_user_memberships';
        }

        if(isset($_POST["plan_id"]) && !empty($_POST["plan_id"]) && isset($_POST["user_id"]) && !empty($_POST["user_id"]) && isset($_POST["new_end_date"]) && !empty($_POST["new_end_date"])) {

            $plan_id = $_POST["plan_id"];
            $user_id = $_POST["user_id"];
            $new_end_date = $_POST["new_end_date"];
            $user_memberships = get_user_meta($user_id, $get_memberships, true);
            if(!empty($user_memberships)) {
                foreach($user_memberships as $key => &$membership) {
                    if ($membership["plan"] == $plan_id) {
                        $membership["ends"] = $new_end_date;
                        break;
                    }
                }
                update_user_meta($user_id, $get_memberships, $user_memberships);
            }

        } else {
            $rvs_error_message = __('Missing either User ID or Membership ID', 'vimeo-sync-memberships');
            rvs_exit_ajax($rvs_error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_update_man_membership_date', 'wpvs_update_man_membership_date' );

function wpvs_remove_man_plan_admin() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_live_mode;
        global $rvs_current_user;
        if($rvs_live_mode == "off") {
            $get_memberships = 'rvs_user_memberships_test';
        } else {
            $get_memberships = 'rvs_user_memberships';
        }

        if(isset($_POST["plan_id"]) && !empty($_POST["plan_id"]) && isset($_POST["user_id"]) && !empty($_POST["user_id"])) {

            $plan_id = $_POST["plan_id"];
            $user_id = $_POST["user_id"];
            $user_memberships = get_user_meta($user_id, $get_memberships, true);
            $remove_membership = null;
            if(!empty($user_memberships)) {
                foreach($user_memberships as $key => &$membership) {
                    if ($membership["plan"] == $plan_id) {
                        unset($user_memberships[$key]);
                        $remove_membership = $membership;
                        break;
                    }
                }
                update_user_meta($user_id, $get_memberships, $user_memberships);
                if( ! empty($remove_membership) ) {
                    wpvs_do_after_delete_membership_action($user_id, $remove_membership);
                    $membership_role = 'wpvs_'.$plan_id.'_role';
                    $membership_test_role = $membership_role.'_test';
                    $member = get_user_by('id', $user_id);
                    if ( in_array( $membership_role, $member->roles ) ) {
                        $member->remove_role($membership_role);
                        if( empty( $member->roles ) ) {
                            $member->add_role("wpvs_member");
                        }
                    }
                    if ( in_array( $membership_test_role, $member->roles ) ) {
                        $member->remove_role($membership_test_role);
                        if( empty( $member->roles ) ) {
                            $member->add_role("wpvs_test_member");
                        }
                    }
                }
            }

        } else {
            $rvs_error_message = __('Missing either User ID or Membership ID', 'vimeo-sync-memberships');
            rvs_exit_ajax($rvs_error_message, 400);
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_remove_man_plan_admin', 'wpvs_remove_man_plan_admin' );

function wpvs_get_members_list() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_live_mode;
        if($rvs_live_mode == "on") {
            $test_mode = false;
            $get_memberships = 'rvs_user_memberships';
        } else {
            $test_mode = true;
            $get_memberships = 'rvs_user_memberships_test';
        }

        $members_offset = 0;
        $wpvs_members_list = array();

        if( isset($_POST["members_offset"]) && ! empty($_POST["members_offset"]) ) {
            $members_offset = intval($_POST["members_offset"]);
        }

        $role_filter = $_POST["role"];

        // GET ALL MEMBERS IF NO ROLE FILTER IS SET
        if( empty($role_filter) || $role_filter == "wpvs_member" || $role_filter == "wpvs_test_member") {
            if( wpvs_members_table_exists() ) {
                $wpvs_members = wpvs_get_members_paged($test_mode, $members_offset);
                if( ! empty($wpvs_members)) {
                    foreach($wpvs_members as $member_data) {
                        $member = get_userdata($member_data->user_id);
                        if( ! empty($member) ) {
                            $user_memberships = get_user_meta($member->ID, $get_memberships, true);
                            $user_memberships_list = '';
                            if( ! empty($user_memberships) ) {
                                foreach($user_memberships as $key => $membership) {
                                    if($membership == end($user_memberships)) {
                                        $user_memberships_list .= $membership["name"];
                                    } else {
                                        $user_memberships_list .= $membership["name"] . ', ';
                                    }
                                }
                            }
                            $add_member = array('user_id' => $member->ID, 'user_name' => $member->user_login, 'user_memberships' => $user_memberships_list);
                            $wpvs_members_list[] = $add_member;
                        } else {
                            wpvs_remove_member($test_mode, $member_data->user_id);
                        }
                    }
                }
            } else {
                $admin_updates_link = admin_url('admin.php?page=wpvs-updates');
                echo json_encode(array('found' => false, 'reason' => 'Members database tables missing. Make sure you have <a href="'.$admin_updates_link.'">run your updates</a>.'));
            }
        } else { // GET USERS BY ROLE
            $user_args = array(
                'blog_id'      => $GLOBALS['blog_id'],
                'role__in'     => array($role_filter),
                'fields'       => array('ID', 'user_login'),
                'offset'       => $members_offset
            );
            $found_members = get_users( $user_args );
            if( ! empty($found_members) ) {
                foreach($found_members as $member) {
                    $user_memberships = get_user_meta($member->ID, $get_memberships, true);
                    $user_memberships_list = '';
                    if( ! empty($user_memberships) ) {
                        foreach($user_memberships as $key => $membership) {
                            if($membership == end($user_memberships)) {
                                $user_memberships_list .= $membership["name"];
                            } else {
                                $user_memberships_list .= $membership["name"] . ', ';
                            }
                        }
                    }
                    $add_member = array('user_id' => $member->ID, 'user_name' => $member->user_login, 'user_memberships' => $user_memberships_list);
                    $wpvs_members_list[] = $add_member;
                }
            }
        }
        if( empty($wpvs_members_list) ) {
            echo json_encode(array('found' => false, 'reason' => 'No members found.'));
        } else {
            echo json_encode(array('found' => true, 'members' => $wpvs_members_list));
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_get_members_list', 'wpvs_get_members_list' );

function wpvs_admin_get_users_for_bulk_editing() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_live_mode;
        if($rvs_live_mode == "on") {
            $test_mode = false;
            $get_memberships = 'rvs_user_memberships';
        } else {
            $test_mode = true;
            $get_memberships = 'rvs_user_memberships_test';
        }

        $members_offset = 0;
        $wpvs_members_list = array();

        if( isset($_POST["members_offset"]) && ! empty($_POST["members_offset"]) ) {
            $members_offset = intval($_POST["members_offset"]);
        }

        $role_filter = $_POST["role"];

        $blog_id = $GLOBALS['blog_id'];
        if( is_multisite() ) {
            $blog_id = get_current_blog_id();
        }

        $user_args = array(
            'blog_id' => $blog_id,
            'fields'  => array('ID', 'user_login'),
            'offset'  => $members_offset,
            'number'  => 100
        );

        // GET ALL USERS IF NO ROLE FILTER IS SET
        if( ! empty($role_filter) && $role_filter != 'nomembership' ) {
            $user_args['role__in'] = array($role_filter);
        }

        // GET ALL USERS WITHOUT A MEMBERSHIP
        if( ! empty($role_filter) && $role_filter == 'nomembership' ) {
            $user_args['meta_query'] = array(
                'relation' => 'OR',
                array(
                    'key' => $get_memberships,
                    'compare' => 'NOT EXISTS'
                ),
                array(
                    array(
                        'key' => $get_memberships,
                        'compare' => 'EXISTS'
                    ),
                    array(
                        'key' => $get_memberships,
                        'value' => serialize(array()),
                        'compare' => '='
                    )
                )
            );
        }
        $found_members = get_users( $user_args );
        if( ! empty($found_members) ) {
            foreach($found_members as $member) {
                $user_memberships = get_user_meta($member->ID, $get_memberships, true);
                $user_memberships_list = '';
                if( ! empty($user_memberships) ) {
                    foreach($user_memberships as $key => $membership) {
                        if($membership == end($user_memberships)) {
                            $user_memberships_list .= $membership["name"];
                        } else {
                            $user_memberships_list .= $membership["name"] . ', ';
                        }
                    }
                }
                $add_member = array('user_id' => $member->ID, 'user_name' => $member->user_login, 'user_memberships' => $user_memberships_list);
                $wpvs_members_list[] = $add_member;
            }
        }

        if( empty($wpvs_members_list) ) {
            echo json_encode(array('found' => false, 'reason' => 'No members found.'));
        } else {
            echo json_encode(array('found' => true, 'members' => $wpvs_members_list));
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_admin_get_users_for_bulk_editing', 'wpvs_admin_get_users_for_bulk_editing' );

function wpvs_admin_bulk_add_membership_to_members() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_live_mode;
        if($rvs_live_mode == "on") {
            $test_member = false;
            $get_memberships = 'rvs_user_memberships';
        } else {
            $test_member = true;
            $get_memberships = 'rvs_user_memberships_test';
        }
        $users_updated_count = 0;
        if( ! isset($_POST["members"]) || empty($_POST["members"]) ) {
            $wpvs_error_message = __('Missing members list to update.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }

        if( ! isset($_POST["update_action"]) || empty($_POST["update_action"]) ) {
            $wpvs_error_message = __('Missing bulk edit update action', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }

        if( ! isset($_POST["membership_id"]) || empty($_POST["membership_id"]) ) {
            $wpvs_error_message = __('Missing bulk edit membership ID', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }

        $members_to_update = $_POST["members"];
        $update_action = $_POST["update_action"];
        $plan_id = $_POST["membership_id"];

        foreach($members_to_update as $user_id) {
            $member = get_user_by('id', $user_id);
            $wpvs_customer = new WPVS_Customer($member);
            $has_membership = false;
            $user_memberships = $wpvs_customer->get_memberships();
            if( $update_action == "add" ) {
                $wpvs_membership_manager = new WPVS_Membership_Manager($plan_id);
                if( $wpvs_membership_manager->membership_plan_exists() ) {
                    $add_membership = $wpvs_membership_manager->get_plan();
                } else {
                    $wpvs_error_message = __('Membership does not exist', 'vimeo-sync-memberships');
                    rvs_exit_ajax($wpvs_error_message, 400);
                }

                if( ! empty($user_memberships) ) {
                    foreach($user_memberships as $member_plan) {
                        if($member_plan['plan'] == $plan_id) {
                            $has_membership = true;
                            break;
                        }
                    }
                } else {
                    $user_memberships = array();
                }

                if( ! empty($add_membership) && ! $has_membership ) {
                    $new_membership = array(
                        'plan'      => $add_membership['id'],
                        'id'        => 'wpvm-'.$add_membership['id'],
                        'amount'    => '0.00',
                        'name'      => $add_membership['name'],
                        'interval'  => 'none',
                        'ends'      => 'never',
                        'status'    => 'active',
                        'type'      => 'manual'
                    );
                    if( isset($_POST["end_date"]) && ! empty($_POST["end_date"]) ) {
                        $new_membership['ends'] = $_POST["end_date"];
                    }
                    $wpvs_customer->add_membership($new_membership);
                    wpvs_add_new_member($user_id, $test_member);
                    $users_updated_count++;
                }
            }

            if( $update_action == "remove" ) {
                $remove_id = 'wpvm-'.$plan_id; // make sure we are removing manually added memberships only
                $membership_role = 'wpvs_'.$plan_id.'_role';
                $membership_test_role = $membership_role.'_test';
                if( ! empty($user_memberships) ) {
                    foreach($user_memberships as $key => $member_plan) {
                        if($member_plan['plan'] == $plan_id && $member_plan['id'] == $remove_id) {
                            $has_membership = true;
                            unset($user_memberships[$key]);
                            break;
                        }
                    }
                    if( $has_membership ) {
                        update_user_meta($user_id, $get_memberships, $user_memberships);
                        $users_updated_count++;
                    }
                }
                if( $has_membership ) {
                    if ( in_array( $membership_role, $member->roles ) ) {
                        $member->remove_role($membership_role);
                        if( empty( $member->roles ) ) {
                            $member->add_role("wpvs_member");
                        }
                    }
                    if ( in_array( $membership_test_role, $member->roles ) ) {
                        $member->remove_role($membership_test_role);
                        if( empty( $member->roles ) ) {
                            $member->add_role("wpvs_test_member");
                        }
                    }
                }
            }
        }
        echo json_encode( array(
            'updated_members_count' => $users_updated_count
        ));
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_admin_bulk_add_membership_to_members', 'wpvs_admin_bulk_add_membership_to_members' );

function wpvs_remove_member_ajax() {
    if( wpvs_secure_admin_ajax() ) {
        global $rvs_live_mode;
        global $rvs_current_user;
        if($rvs_live_mode == "on") {
            $test_mode = false;
            $get_memberships = 'rvs_user_memberships';
            $stripe_key = "stripe";
            $paypal_key = "paypal";
        } else {
            $test_mode = true;
            $get_memberships = 'rvs_user_memberships_test';
            $stripe_key = "stripe_test";
            $paypal_key = "paypal_test";
        }

        if( current_user_can('manage_options') &&  isset($_POST["user_id"]) && ! empty($_POST["user_id"]) ) {
            $stripe_payments_manager = new WPVS_Stripe_Payments_Manager($rvs_current_user->ID);
            $user_id = intval($_POST["user_id"]);
            $wpvs_member = wpvs_remove_member($test_mode, $user_id);
            $user_memberships = get_user_meta($user_id, $get_memberships, true);
            if( ! empty($user_memberships) ) {
                foreach($user_test_memberships as $key => &$membership) {
                    if( isset($membership['type']) && $membership['type'] == $stripe_key ) {
                        $subscription_id = $membership["id"];
                        $stripe_payments_manager->cancel_subscription($user_id, $subscription_id, null);
                    }

                    if( isset($membership['type']) && $membership['type'] == $paypal_key ) {
                        $agreement_id = $membership["id"];
                        wpvs_cancel_users_paypal_plan($user_id, $agreement_id, true);
                    }
                }
                update_user_meta($user_id, $get_memberships, array());
            }
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_remove_member_ajax', 'wpvs_remove_member_ajax' );

function wpvs_send_test_smtp_email() {
    if( wpvs_secure_admin_ajax() ) {
        if( current_user_can('manage_options') && isset($_POST['email']) && ! empty($_POST['email']) ) {
            $test_email = $_POST['email'];
            $email_sent = wpvs_send_test_smtp_settings($test_email);
            if($email_sent) {
                _e('Email sent!', 'vimeo-sync-memberships');
            } else {
                $wpvs_error_message = __('An issue occured when sending your test email.', 'vimeo-sync-memberships');
                rvs_exit_ajax($wpvs_error_message, 401);
            }
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}
add_action( 'wp_ajax_wpvs_send_test_smtp_email', 'wpvs_send_test_smtp_email' );

// VIMEO SYNC DOWNLOADS
function wpvs_get_vimeo_download() {
    if( wpvs_secure_admin_ajax() ) {
        global $wpvs_vimeo_config;
        global $wpvs_vimeo_api_key;
        if( !empty($wpvs_vimeo_api_key) && isset($_POST["vimeo_id"]) ) {
            require_once(WPVS_VIDEO_IMPORTER_DIR . '/vimeo/autoload.php');
            $vimeo_id = $_POST["vimeo_id"];
            $lib = new Vimeo\Vimeo($wpvs_vimeo_config['client_id'], $wpvs_vimeo_config['client_secret'], $wpvs_vimeo_api_key);
            $video_details = $lib->request('/me/videos/'.$vimeo_id, array('type'=>'GET'));
            if(!empty($video_details["body"]["error"])) {
                $error_message = $video_details["body"]["error"];
                rvs_exit_ajax($error_message, 400);
            } else {
                $download_files = $video_details["body"]["download"];
                echo json_encode($download_files);
            }
        }
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}
add_action( 'wp_ajax_wpvs_get_vimeo_download', 'wpvs_get_vimeo_download' );
