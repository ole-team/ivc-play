<?php

function rvs_admin_load_payments() {
    if( wpvs_secure_admin_ajax() ) {
        if(isset($_REQUEST["offset"])) {
            $offset = intval($_REQUEST["offset"]);
        }

        if( isset($_REQUEST["gateways"]) && ! empty($_REQUEST["gateways"]) ) {
            $gateways = $_REQUEST["gateways"];
        } else {
            $gateways = null;
        }

        if( isset($_REQUEST["coupon_id"]) && ! empty($_REQUEST["coupon_id"]) ) {
            $coupon_id = $_REQUEST["coupon_id"];
        } else {
            $coupon_id = null;
        }

        $payments = rvs_get_admin_payments($offset, $gateways, $coupon_id);
        $count_payments = 0;
        $found_payments = array();
        if(!empty($payments)) {
            $count_payments = count($payments);
            foreach($payments as $payment) {
                $user = get_user_by('id', $payment->userid);
                if( empty($user) ) {
                    $user_login = '<em>(user does not exist)</em>';
                } else {
                    $user_login = '<a href="'.admin_url('admin.php?page=rvs-edit-member&id='.$user->ID).'">'.$user->user_login.'</a>';
                }


                $amount = number_format($payment->amount/100,2);
                $date = date( 'M d, Y', $payment->time );
                $add_payment = array(
                    'user' => $user_login,
                    'amount' => $amount,
                    'date' => $date,
                    'gateway' => $payment->gateway,
                    'paymentid' => $payment->paymentid,
                    'coupon' => $payment->coupon_code,
                    'refunded' => intval($payment->refunded)
                );
                $found_payments[] = $add_payment;

            }
        }
        if(!empty($found_payments)) {
            $return_payments = array('count' => $count_payments, 'payments' => $found_payments);
        } else {
            $return_payments = array('count' => 0);
        }
        echo json_encode($return_payments);
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_rvs_admin_load_payments', 'rvs_admin_load_payments' );

function wpvs_admin_load_timeframe_payments() {
    if( wpvs_secure_admin_ajax() ) {
        if(isset($_REQUEST["offset"])) {
            $offset = intval($_REQUEST["offset"]);
        }
        $current_time = current_time('timestamp', 1);

        // GET CURRENT YEAR
        $current_year = date('Y-01-01', $current_time);
        $current_year_timestamp = strtotime($current_year);

        // GET CURRENT MONTH
        $current_month = date('m-Y', $current_time);
        $set_first_day = '01-'.$current_month;
        $first_day_of_month = date($set_first_day);
        $current_month_timestamp = strtotime($first_day_of_month);
        $seven_days_ago = strtotime('-7 days', $current_time);

        // GET LAST WEEK PAYMENTS
        $last_week_amount = 0;
        $last_week_payments = wpvs_get_timeframe_admin_payments($seven_days_ago, $current_time);

        if( ! empty($last_week_payments) ) {
            foreach($last_week_payments as $payment) {
                $last_week_amount += $payment->amount;
            }
        }

        if( ! empty($last_week_amount) ) {
            $last_week_amount = number_format($last_week_amount/100,2);
        }

        // GET THIS MONTH PAYMENTS

        $this_month_amount = 0;
        $this_month_payments = wpvs_get_timeframe_admin_payments($current_month_timestamp, $current_time);

        if( ! empty($this_month_payments) ) {
            foreach($this_month_payments as $payment) {
                $this_month_amount += $payment->amount;
            }
        }

        if( ! empty($this_month_amount) ) {
            $this_month_amount = number_format($this_month_amount/100,2);
        }

        // GET THIS YEAT PAYMENTS
        $this_year_amount = 0;
        $this_year_payments = wpvs_get_timeframe_admin_payments($current_year_timestamp, $current_time);

        if( ! empty($this_year_payments) ) {
            foreach($this_year_payments as $payment) {
                $this_year_amount += $payment->amount;
            }
        }

        if( ! empty($this_year_amount) ) {
            $this_year_amount = number_format($this_year_amount/100,0);
        }

        $return_payments = array(
            'last_week'   => $last_week_amount,
            'this_month'  => $this_month_amount,
            'this_year'   => $this_year_amount
        );

        echo json_encode($return_payments);
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_admin_load_timeframe_payments', 'wpvs_admin_load_timeframe_payments' );

function wpvs_admin_sync_all_stripe_payments() {
    if( wpvs_secure_admin_ajax() ) {
        $starting_after = null;
        $customer_id = null;
        if( isset($_POST['customer_id']) && ! empty($_POST['customer_id']) ) {
            $customer_id = $_POST['customer_id'];
        }

        if( isset($_POST['start_at']) && ! empty($_POST['start_at']) ) {
            $starting_after = $_POST['start_at'];
        }

        $more_charges = false;
        $add_new_payments = array();
        $wpvs_error = null;
        global $wpvs_stripe_admin_ajax_requests;
        $all_stripe_charges = $wpvs_stripe_admin_ajax_requests->wpvs_stripe_get_list_of_charges($starting_after, $customer_id);

        if( isset($all_stripe_charges['error']) && ! empty($all_stripe_charges['error']) ) {
            $wpvs_error = $all_stripe_charges['error'];
            $more_charges = false;
        } else {
            $more_charges = $all_stripe_charges['has_more'];

            if( isset($all_stripe_charges['charges']) && ! empty($all_stripe_charges['charges']) && is_array($all_stripe_charges['charges']) ) {
                $new_charges = $all_stripe_charges['charges'];
                $add_new_payments = $new_charges;
            }

            if( isset($all_stripe_charges['last_charge']) && ! empty($all_stripe_charges['last_charge']) ) {
                $starting_after = $all_stripe_charges['last_charge'];
            }

            if( ! empty($add_new_payments) ) {
                foreach($add_new_payments as $new_payment) {
                    if( isset($new_payment['status']) && $new_payment['status'] == 'succeeded' ) {
                        $wpvs_payment_manager = new WPVS_Payment_Manager($new_payment['charge_id'], $new_payment['test_mode']);
                        $wpvs_payment_manager->add_new_payment(
                            $new_payment['charge_time'],
                            'stripe',
                            $new_payment['charge_user_id'],
                            $new_payment['charge_customer'],
                            $new_payment['charge_amount'],
                            $new_payment['charge_type'],
                            $new_payment['charge_product'],
                            $new_payment['charge_refunded'],
                            $new_payment['coupon_code']
                        );
                    }

                }
            }
            echo json_encode(array( 'more' => $more_charges, 'start_at' => $starting_after));
        }
        if( ! empty($wpvs_error) ) {
            rvs_exit_ajax($wpvs_error, 401);
        }
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_admin_sync_all_stripe_payments', 'wpvs_admin_sync_all_stripe_payments' );

function wpvs_admin_import_all_stripe_customers() {
    if( wpvs_secure_admin_ajax() ) {
        $starting_after = null;
        $total_new_customers_added = 0;
        $more_customers = false;
        $wpvs_error = null;
        if( isset($_POST['start_at']) && ! empty($_POST['start_at']) ) {
            $starting_after = $_POST['start_at'];
        }

        if( isset($_POST['total_added']) && ! empty($_POST['total_added']) ) {
            $total_new_customers_added = intval($_POST['total_added']);
        }
        global $wpvs_stripe_admin_ajax_requests;
        $all_stripe_customers = $wpvs_stripe_admin_ajax_requests->wpvs_stripe_import_customers($starting_after);
        if( ! empty($all_stripe_customers->error) ) {
            $wpvs_error = $all_stripe_customers->error;
            $more_customers = false;
        } else {
            $more_customers = $all_stripe_customers->has_more_customers;

            if( ! empty($all_stripe_customers->customer_count) ) {
                $total_new_customers_added = $total_new_customers_added + intval($all_stripe_customers->customer_count);
            }

            if( ! empty($all_stripe_customers->last_customer) ) {
                $starting_after = $all_stripe_customers->last_customer;
            }

            echo json_encode(array( 'more' => $more_customers, 'start_at' => $starting_after, 'added' => $total_new_customers_added));
        }
        if( ! empty($wpvs_error) ) {
            rvs_exit_ajax($wpvs_error, 401);
        }
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_admin_import_all_stripe_customers', 'wpvs_admin_import_all_stripe_customers' );
