<?php

function wpvs_run_database_update() {
    global $rvs_current_version;
    if( wpvs_secure_admin_ajax() ) {
        $wpvs_memberships_updates = get_option('wpvs-memberships-updated');
        if(!empty($wpvs_memberships_updates)) {
            $wpvs_updates_version = intval(str_replace(".","",$wpvs_memberships_updates));
        } else {
            $wpvs_updates_version = 0;
        }
        update_option('wpvs-memberships-updated', $rvs_current_version);
    } else {
        $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 401);
    }
    wp_die();
}
add_action( 'wp_ajax_wpvs_run_database_update', 'wpvs_run_database_update' );
