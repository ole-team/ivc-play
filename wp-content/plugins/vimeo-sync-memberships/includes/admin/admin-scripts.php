<?php

function wpvs_load_admin_styles() {
    global $rvs_current_version;
    wp_enqueue_style('wpvs-admin-video', RVS_MEMBERS_BASE_URL . 'css/admin-video.css', '', $rvs_current_version);
    wp_register_style('wpvs-admin-payments', RVS_MEMBERS_BASE_URL . 'css/admin/payments.css', '', $rvs_current_version);
}

add_action('admin_enqueue_scripts', 'wpvs_load_admin_styles');

function wpvs_load_admin_scripts() {
    global $rvs_current_version;
    $editing_user_id = null;
    $wpvs_customer_state = null;
    $current_admin_screen = get_current_screen();
    if ( $current_admin_screen->base == 'user-edit' && isset($_GET['user_id']) ) {
        $editing_user_id = $_GET['user_id'];
        $wpvs_customer_state = get_user_meta( $editing_user_id, 'wpvs_billing_state', true);
    }
    wp_register_script( 'wpvs-admin-email-settings', RVS_MEMBERS_BASE_URL . 'js/admin/wpvs-email-settings.js', array('jquery'), $rvs_current_version, true);
    wp_register_script( 'wpvs-video-edit', RVS_MEMBERS_BASE_URL .'js/admin/wpvs-admin-video.js', array('jquery'), $rvs_current_version, true);
    wp_register_script( 'wpvs-webhooks-admin', RVS_MEMBERS_BASE_URL .'js/admin/wpvs-admin-webhooks.js', array('jquery'), $rvs_current_version, true);
    wp_register_script( 'rvs-image-upload', RVS_MEMBERS_BASE_URL . 'js/rvs-image-upload.js', array('jquery'), $rvs_current_version, true);
    wp_register_script( 'wpvs-admin-payment-functions', RVS_MEMBERS_BASE_URL . 'js/admin/wpvs-payment-functions.js', array('jquery'), $rvs_current_version, true);
    wp_register_script( 'rvs-admin-member', RVS_MEMBERS_BASE_URL .'js/rvs-member-admin.js', array('rvs-memberships-js', 'wpvs-admin-payment-functions'), $rvs_current_version, true);
    wp_register_script( 'wpvs-admin-bulk-edit-members', RVS_MEMBERS_BASE_URL .'js/admin/wpvs-bulk-edit-members-admin.js', array('rvs-memberships-js', 'wpvs-admin-payment-functions'), $rvs_current_version, true);
    wp_register_script( 'rvs-admin-edit-members', RVS_MEMBERS_BASE_URL .'js/admin/rvs-members-admin.js', array('rvs-memberships-js', 'wpvs-admin-payment-functions'), $rvs_current_version, true);
    wp_register_script( 'wpvs-country-dropdown-js', RVS_MEMBERS_BASE_URL .'js/wpvs-country-dropdown.js', array('jquery'), $rvs_current_version, true);
    wp_localize_script('wpvs-country-dropdown-js', 'wpvsdir', array(
        'list' => RVS_MEMBERS_BASE_URL . 'countries/',
        'state' => $wpvs_customer_state
    ));
    wp_register_script( 'wpvs-admin-tax-rates-js', RVS_MEMBERS_BASE_URL .'js/admin/wpvs-admin-tax-rates.js', array('jquery', 'wpvs-country-dropdown-js'), $rvs_current_version, true);
    wp_localize_script('wpvs-admin-tax-rates-js', 'wpvstaxmanager', array(
        'redirect' => admin_url('admin.php?page=wpvs-tax-rate-settings')
    ));
    wp_enqueue_script('wpvs-video-edit');
}

add_action('admin_enqueue_scripts', 'wpvs_load_admin_scripts');
