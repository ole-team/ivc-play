<?php
if(isset($_GET['tab'])) {
    $current_tab = $_GET['tab'];
} else {
     $current_tab = 'setup';
}
?>
<div class="wrap">
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title">Email Settings</h1>
            <div class="rvs-container">
                <ul class="subsubsub">
                    <li><a class="<?= ($current_tab == "setup") ? 'current': '' ?>" href="<?php echo admin_url('admin.php?page=rvs-email-settings&tab=setup'); ?>"><?php _e('Settings', 'vimeo-sync-memberships'); ?></a> | </li>
                    <li><a class="<?= ($current_tab == "new_account") ? 'current': '' ?>" href="<?php echo admin_url('admin.php?page=rvs-email-settings&tab=new_account'); ?>"><?php _e('New Account', 'vimeo-sync-memberships'); ?></a> | </li>
                    <li><a class="<?= ($current_tab == "new_membership") ? 'current': '' ?>" href="<?php echo admin_url('admin.php?page=rvs-email-settings&tab=new_membership'); ?>"><?php _e('New Membership', 'vimeo-sync-memberships'); ?></a> | </li>
                    <li><a class="<?= ($current_tab == "cancel_membership") ? 'current': '' ?>" href="<?php echo admin_url('admin.php?page=rvs-email-settings&tab=cancel_membership'); ?>"><?php _e('Cancelled Membership', 'vimeo-sync-memberships'); ?></a> | </li>
                    <li><a class="<?= ($current_tab == "reactivate_membership") ? 'current': '' ?>" href="<?php echo admin_url('admin.php?page=rvs-email-settings&tab=reactivate_membership'); ?>"><?php _e('Re-activated Membership', 'vimeo-sync-memberships'); ?></a> | </li>
                    <li><a class="<?= ($current_tab == "delete_membership") ? 'current': '' ?>" href="<?php echo admin_url('admin.php?page=rvs-email-settings&tab=delete_membership'); ?>"><?php _e('Deleted Membership', 'vimeo-sync-memberships'); ?></a></li>
                </ul>
            </div>
            <div class="rvs-container rvs-box">
                <form method="post" action="options.php">
                    <?php settings_fields('wpvs-membership-email-settings');

                    $site_name = get_bloginfo('name');
                    $admin_email = get_option('admin_email');
                    $pass_reset_link = '<a href="'.wp_lostpassword_url( home_url() ).'">'.__('Reset Password', 'vimeo-sync-memberships').'</a>';
                    $wpvs_show_powered_by_email = get_option('wpvs_show_powered_by_email', '1');

                    // EMAIL LOGO
                    $wpvs_email_logo = get_option('rvs_email_logo', get_theme_mod('rogue_company_logo'));
                    if(empty($wpvs_email_logo)) {
                        $wpvs_email_logo = RVS_MEMBERS_BASE_URL .'image/vimeo-sync-square.png';
                    }

                    // SMTP

                    $wpvs_smtp_settings = get_option('wpvs_smtp_settings');

                    if( ! isset($wpvs_smtp_settings['enabled'])) {
                        $wpvs_smtp_settings['enabled'] = 0;
                    }
                    if( ! isset($wpvs_smtp_settings['host'])) {
                        $wpvs_smtp_settings['host'] = 'mail.yourdomain.com';
                    }

                    if( ! isset($wpvs_smtp_settings['port'])) {
                        $wpvs_smtp_settings['port'] = 465;
                    }

                    if( ! isset($wpvs_smtp_settings['securemode'])) {
                        $wpvs_smtp_settings['securemode'] = 'ssl';
                    }

                    if( ! isset($wpvs_smtp_settings['username'])) {
                        $wpvs_smtp_settings['username'] = '';
                    }

                    if( ! isset($wpvs_smtp_settings['password'])) {
                        $wpvs_smtp_settings['password'] = '';
                    }

                    if( ! isset($wpvs_smtp_settings['from_name'])) {
                        $wpvs_smtp_settings['from_name'] = '';
                    }

                    if( ! isset($wpvs_smtp_settings['from_email'])) {
                        $wpvs_smtp_settings['from_email'] = '';
                    }

                    if( ! isset($wpvs_smtp_settings['enabled'])) {
                        $wpvs_smtp_settings['enabled'] = 0;
                    }

                    // NEW ACCOUNT EMAIL OPTIONS
                    $rvs_new_account_email = get_option('rvs_new_account_email');
                    if(!isset($rvs_new_account_email['from_name'])) {
                        $rvs_new_account_email['from_name'] = $site_name;
                    }

                    if(!isset($rvs_new_account_email['from_email'])) {
                        $rvs_new_account_email['from_email'] = $admin_email;
                    }

                    if(!isset($rvs_new_account_email['notify_email'])) {
                        $rvs_new_account_email['notify_email'] = $admin_email;
                    }

                    if(!isset($rvs_new_account_email['subject'])) {
                        $rvs_new_account_email['subject'] = 'New Account for ' . $site_name;
                    }

                    $rvs_account_page = esc_attr( get_option('rvs_account_page'));
                    $rvs_account_link = get_permalink($rvs_account_page);

                    if(!isset($rvs_new_account_email['content'])) {
                        $rvs_new_account_email['content'] = '<h3>Thanks for signing up!</h3>You can sign into your account <a href="'. $rvs_account_link .'">here</a><h3>Username</h3>Your username is: {username}<h5>Forgot Your Password?</h5>'.$pass_reset_link;
                    }

                    // NEW MEMBERSHIP EMAIL OPTIONS
                    $rvs_new_membership_email = get_option('rvs_new_membership_email');
                    if(!isset($rvs_new_membership_email['from_name'])) {
                        $rvs_new_membership_email['from_name'] = $site_name;
                    }

                    if(!isset($rvs_new_membership_email['from_email'])) {
                        $rvs_new_membership_email['from_email'] = $admin_email;
                    }

                    if(!isset($rvs_new_membership_email['notify_email'])) {
                        $rvs_new_membership_email['notify_email'] = $admin_email;
                    }

                    if(!isset($rvs_new_membership_email['subject'])) {
                        $rvs_new_membership_email['subject'] = "Thanks for subscribing to our {planname} membership";
                    }

                    if(!isset($rvs_new_membership_email['content'])) {
                        $rvs_new_membership_email['content'] = 'You can view your memberships <a href="'. $rvs_account_link .'">here</a><br/>From here you can view and manage your current memberships, credit cards and payments.<br/>If you have any questions, you can email us at <a href="mailto:' . get_option('admin_email') . '">'.get_option('admin_email').'.</a>';
                    }

                    // CANCEL MEMBERSHIP EMAIL OPTIONS
                    $rvs_cancel_membership_email = get_option('rvs_cancel_membership_email');
                    if(!isset($rvs_cancel_membership_email['from_name'])) {
                        $rvs_cancel_membership_email['from_name'] = $site_name;
                    }

                    if(!isset($rvs_cancel_membership_email['from_email'])) {
                        $rvs_cancel_membership_email['from_email'] = $admin_email;
                    }

                    if(!isset($rvs_cancel_membership_email['notify_email'])) {
                        $rvs_cancel_membership_email['notify_email'] = $admin_email;
                    }

                    if(!isset($rvs_cancel_membership_email['subject'])) {
                        $rvs_cancel_membership_email['subject'] = "Subscription Cancelled";
                    }

                    if(!isset($rvs_cancel_membership_email['content'])) {
                        $rvs_cancel_membership_email['content'] = '<h3>Your subscription has been cancelled.</h3>Your {planname} subscription on ' . $site_name . ' has been cancelled. If you believe this is an error, you can email us at <a href="mailto:' . $admin_email . '">'.$admin_email.'.</a>';
                    }

                    // DELETE MEMBERSHIP EMAIL OPTIONS
                    $rvs_delete_membership_email = get_option('rvs_delete_membership_email');
                    if(!isset($rvs_delete_membership_email['from_name'])) {
                        $rvs_delete_membership_email['from_name'] = $site_name;
                    }

                    if(!isset($rvs_delete_membership_email['from_email'])) {
                        $rvs_delete_membership_email['from_email'] = $admin_email;
                    }

                    if(!isset($rvs_delete_membership_email['notify_email'])) {
                        $rvs_delete_membership_email['notify_email'] = $admin_email;
                    }

                    if(!isset($rvs_delete_membership_email['subject'])) {
                        $rvs_delete_membership_email['subject'] = "Subscription Deleted";
                    }

                    if(!isset($rvs_delete_membership_email['content'])) {
                        $rvs_delete_membership_email['content'] = '<h3>Your subscription has been deleted.</h3>Your {planname} subscription on ' . $site_name . ' has been deleted. If you believe this is an error, you can email us at <a href="mailto:' . $admin_email . '">'.$admin_email.'.</a>';
                    }

                    // REACTIVATE MEMBERSHIP EMAIL OPTIONS
                    $rvs_reactivate_membership_email = get_option('rvs_reactivate_membership_email');
                    if(!isset($rvs_reactivate_membership_email['from_name'])) {
                        $rvs_reactivate_membership_email['from_name'] = $site_name;
                    }

                    if(!isset($rvs_reactivate_membership_email['from_email'])) {
                        $rvs_reactivate_membership_email['from_email'] = $admin_email;
                    }

                    if(!isset($rvs_reactivate_membership_email['notify_email'])) {
                        $rvs_reactivate_membership_email['notify_email'] = $admin_email;
                    }

                    if(!isset($rvs_reactivate_membership_email['subject'])) {
                        $rvs_reactivate_membership_email['subject'] = "Subscription Re-activated";
                    }

                    if(!isset($rvs_reactivate_membership_email['content'])) {
                        $rvs_reactivate_membership_email['content'] = '<h3>Your subscription has been re-activated.</h3>Your {planname} subscription on ' . $site_name . ' has been re-activated. If you believe this is an error, you can email us at <a href="mailto:' . $admin_email . '">'.$admin_email.'.</a>';
                    }

                    ?>
                    <div class="<?= ($current_tab == "setup") ? '': 'rvs-hidden-email' ?>">
                        <h3 class="title"><?php _e('Email Settings', 'vimeo-sync-memberships'); ?></h3>
                        <table class="form-table">
                            <tbody>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Email Logo', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <div class="imageContain">
                                            <img style="max-width: 250px;" class="change-image" src="<?php echo $wpvs_email_logo; ?>" />
                                        </div>
                                    <input class="upload-button button button-primary" type="button" value="Choose Image" /><input class="upload-url" name="rvs_email_logo" type="text" value="<?php echo $wpvs_email_logo; ?>" hidden/><br/><br/>
                                    <label class="description"><?php _e('Displays at the top of your emails. (Recommended size is no larger than 300px wide)', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row"><label><?php _e('SMTP Enabled (Optional)', 'vimeo-sync-memberships'); ?>:</label></th>
                                    <td><input name="wpvs_smtp_settings[enabled]" type="checkbox" value="1" <?php checked(1, $wpvs_smtp_settings['enabled']); ?>/><label class="description"><?php _e('Use your own mailing server to send emails. This can help if emails are not sending properly.', 'vimeo-sync-memberships'); ?></label></td>
                                </tr>
                                <tr>
                                    <th scope="row"><label><?php _e('Host', 'vimeo-sync-memberships'); ?>:</label></th>
                                    <td><input class="regular-text" name="wpvs_smtp_settings[host]" type="text" value="<?php echo esc_attr( $wpvs_smtp_settings['host'] ); ?>" /></td>
                                </tr>
                                <tr>
                                    <th scope="row"><label><?php _e('Port', 'vimeo-sync-memberships'); ?>:</label></th>
                                    <td><input class="regular-text" name="wpvs_smtp_settings[port]" type="text" value="<?php echo esc_attr( $wpvs_smtp_settings['port'] ); ?>" /></td>
                                </tr>
                                <tr>
                                    <th scope="row"><label><?php _e('SSL', 'vimeo-sync-memberships'); ?>:</label></th>
                                    <td><input name="wpvs_smtp_settings[securemode]" type="radio" value="ssl" <?php checked('ssl', $wpvs_smtp_settings['securemode']); ?>/></td>
                                </tr>
                                <tr>
                                    <th scope="row"><label><?php _e('TLS', 'vimeo-sync-memberships'); ?>:</label></th>
                                    <td><input name="wpvs_smtp_settings[securemode]" type="radio" value="tls" <?php checked('tls', $wpvs_smtp_settings['securemode']); ?>/></td>
                                </tr>
                                <tr>
                                    <th scope="row"><label><?php _e('Username', 'vimeo-sync-memberships'); ?>:</label></th>
                                    <td><input class="regular-text" name="wpvs_smtp_settings[username]" type="text" value="<?php echo esc_attr( $wpvs_smtp_settings['username'] ); ?>" /></td>
                                </tr>
                                <tr>
                                    <th scope="row"><label><?php _e('Password', 'vimeo-sync-memberships'); ?>:</label></th>
                                    <td><input class="regular-text" name="wpvs_smtp_settings[password]" type="password" value="<?php echo esc_attr( $wpvs_smtp_settings['password'] ); ?>" /></td>
                                </tr>
                                <tr>
                                    <th scope="row"><label><?php _e('From Name', 'vimeo-sync-memberships'); ?>:</label></th>
                                    <td><input class="regular-text" name="wpvs_smtp_settings[from_name]" type="text" value="<?php echo esc_attr( $wpvs_smtp_settings['from_name'] ); ?>" /></td>
                                </tr>
                                <tr>
                                    <th scope="row"><label><?php _e('From Email', 'vimeo-sync-memberships'); ?>:</label></th>
                                    <td><input class="regular-text" name="wpvs_smtp_settings[from_email]" type="email" value="<?php echo esc_attr( $wpvs_smtp_settings['from_email'] ); ?>" /></td>
                                </tr>
                                <tr>
                                    <th scope="row"><label><?php _e('Credit in email Footer', 'vimeo-sync-memberships'); ?>:</label></th>
                                    <td><input name="wpvs_show_powered_by_email" type="checkbox" value="1" <?php checked(1, $wpvs_show_powered_by_email); ?>/><label class="description"><?php _e('Show <strong>Powered by WP Video Subscriptions</strong> in email footer.', 'vimeo-sync-memberships'); ?></label></td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="form-table">
                            <tbody>
                                <tr>
                                    <th scope="row"><label><?php _e('Email Test', 'vimeo-sync-memberships'); ?>:</label></th>
                                    <td><input class="regular-text" id="wpvs-test-email" type="email" value="" /><br>
                                        <label id="wpvs-send-test-email"><?php _e('Send Test Now', 'vimeo-sync-memberships') ;?></label>
                                        <div class="wpvs-admin-notice"></div>
                                        <label class="rvs-success wpvs-updating-icon"><span class="dashicons dashicons-update wpvs-update-spin"></span> Sending Test...</label>
                                        <div class="wpvs-update-errors"><p class="rvs-error"></p></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <div class="<?= ($current_tab == "new_account") ? '': 'rvs-hidden-email' ?>">
                        <h3 class="title"><?php _e('New Account Email', 'vimeo-sync-memberships'); ?></h3>
                        <table class="form-table">
                            <tbody>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('From Name', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_new_account_email[from_name]" name="rvs_new_account_email[from_name]" type="text" value="<?php echo $rvs_new_account_email['from_name']; ?>" />
                                        <label class="description" for="rvs_new_account_email[from_name]"><?php _e('Name of sender', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('From Email', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_new_account_email[from_email]" name="rvs_new_account_email[from_email]" type="email" value="<?php echo $rvs_new_account_email['from_email']; ?>" />
                                        <label class="description" for="rvs_new_account_email[from_email]"><?php _e('Email sent from', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Notification Email', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_new_account_email[notify_email]" name="rvs_new_account_email[notify_email]" type="text" value="<?php echo $rvs_new_account_email['notify_email']; ?>" />
                                        <label class="description" for="rvs_new_account_email[notify_email]"><?php _e('Admin email to receive notifications', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Email Subject', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_new_account_email[subject]" name="rvs_new_account_email[subject]" type="text" value="<?php echo $rvs_new_account_email['subject']; ?>" />
                                        <label class="description" for="rvs_new_account_email[subject]"><?php _e('Email Subject', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>

                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Email Content', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <?php wp_editor( $rvs_new_account_email['content'], 'rvs_new_account_email_content', $settings = array('textarea_name' => 'rvs_new_account_email[content]') );
                                        ?>
                                        <h4><?php _e('Variables: These will be replaced with the new user details', 'vimeo-sync-memberships'); ?></h4>
                                        <ul>
                                            <li>{username}</li>
                                            <li>{password} - <em>Replaced with a Password Reset link</em></li>
                                        </ul>

                                    </td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                    <div class="<?= ($current_tab == "new_membership") ? '': 'rvs-hidden-email' ?>">
                        <h3 class="title"><?php _e('New Membership Email', 'vimeo-sync-memberships'); ?></h3>
                        <table class="form-table">
                            <tbody>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('From Name', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_new_membership_email[from_name]" name="rvs_new_membership_email[from_name]" type="text" value="<?php echo $rvs_new_membership_email['from_name']; ?>" />
                                        <label class="description" for="rvs_new_membership_email[from_name]"><?php _e('Name of sender', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('From Email', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_new_membership_email[from_email]" name="rvs_new_membership_email[from_email]" type="email" value="<?php echo $rvs_new_membership_email['from_email']; ?>" />
                                        <label class="description" for="rvs_new_membership_email[from_email]"><?php _e('Email sent from', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Notification Email', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_new_membership_email[notify_email]" name="rvs_new_membership_email[notify_email]" type="text" value="<?php echo $rvs_new_membership_email['notify_email']; ?>" />
                                        <label class="description" for="rvs_new_membership_email[notify_email]"><?php _e('Admin email to receive notifications', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Email Subject', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_new_membership_email[subject]" name="rvs_new_membership_email[subject]" type="text" value="<?php echo $rvs_new_membership_email['subject']; ?>" />
                                        <label class="description" for="rvs_new_membership_email[subject]"><?php _e('Email Subject', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>

                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Email Content', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <?php wp_editor( $rvs_new_membership_email['content'], 'rvs_new_membership_email_content', $settings = array('textarea_name' => 'rvs_new_membership_email[content]') );
                                        ?>
                                        <h4><?php _e('Variables: These will be replaced with the plan details', 'vimeo-sync-memberships'); ?></h4>
                                        <ul>
                                            <li>{planname}</li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="<?= ($current_tab == "cancel_membership") ? '': 'rvs-hidden-email' ?>">
                        <h3 class="title"><?php _e('Cancelled Membership Email', 'vimeo-sync-memberships'); ?></h3>
                        <table class="form-table">
                            <tbody>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('From Name', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_cancel_membership_email[from_name]" name="rvs_cancel_membership_email[from_name]" type="text" value="<?php echo $rvs_cancel_membership_email['from_name']; ?>" />
                                        <label class="description" for="rvs_cancel_membership_email[from_name]"><?php _e('Name of sender', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('From Email', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_cancel_membership_email[from_email]" name="rvs_cancel_membership_email[from_email]" type="email" value="<?php echo $rvs_cancel_membership_email['from_email']; ?>" />
                                        <label class="description" for="rvs_cancel_membership_email[from_email]"><?php _e('Email sent from', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Notification Email', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_cancel_membership_email[notify_email]" name="rvs_cancel_membership_email[notify_email]" type="text" value="<?php echo $rvs_cancel_membership_email['notify_email']; ?>" />
                                        <label class="description" for="rvs_cancel_membership_email[notify_email]"><?php _e('Admin email to receive notifications', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Email Subject', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_cancel_membership_email[subject]" name="rvs_cancel_membership_email[subject]" type="text" value="<?php echo $rvs_cancel_membership_email['subject']; ?>" />
                                        <label class="description" for="rvs_cancel_membership_email[subject]"><?php _e('Email Subject', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>

                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Email Content', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <?php wp_editor( $rvs_cancel_membership_email['content'], 'rvs_cancel_membership_email_content', $settings = array('textarea_name' => 'rvs_cancel_membership_email[content]') );
                                        ?>
                                        <h4><?php _e('Variables: These will be replaced with the plan details', 'vimeo-sync-memberships'); ?></h4>
                                        <ul>
                                            <li>{planname}</li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="<?= ($current_tab == "reactivate_membership") ? '': 'rvs-hidden-email' ?>">
                        <h3 class="title"><?php _e('Re-activated Membership Email', 'vimeo-sync-memberships'); ?></h3>
                        <table class="form-table">
                            <tbody>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('From Name', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_reactivate_membership_email[from_name]" name="rvs_reactivate_membership_email[from_name]" type="text" value="<?php echo $rvs_reactivate_membership_email['from_name']; ?>" />
                                        <label class="description" for="rvs_reactivate_membership_email[from_name]"><?php _e('Name of sender', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('From Email', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_reactivate_membership_email[from_email]" name="rvs_reactivate_membership_email[from_email]" type="email" value="<?php echo $rvs_reactivate_membership_email['from_email']; ?>" />
                                        <label class="description" for="rvs_reactivate_membership_email[from_email]"><?php _e('Email sent from', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Notification Email', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_reactivate_membership_email[notify_email]" name="rvs_reactivate_membership_email[notify_email]" type="text" value="<?php echo $rvs_reactivate_membership_email['notify_email']; ?>" />
                                        <label class="description" for="rvs_reactivate_membership_email[notify_email]"><?php _e('Admin email to receive notifications', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Email Subject', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_reactivate_membership_email[subject]" name="rvs_reactivate_membership_email[subject]" type="text" value="<?php echo $rvs_reactivate_membership_email['subject']; ?>" />
                                        <label class="description" for="rvs_reactivate_membership_email[subject]"><?php _e('Email Subject', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>

                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Email Content', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <?php wp_editor( $rvs_reactivate_membership_email['content'], 'rvs_reactivate_membership_email_content', $settings = array('textarea_name' => 'rvs_reactivate_membership_email[content]') );
                                        ?>
                                        <h4><?php _e('Variables: These will be replaced with the plan details', 'vimeo-sync-memberships'); ?></h4>
                                        <ul>
                                            <li>{planname}</li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="<?= ($current_tab == "delete_membership") ? '': 'rvs-hidden-email' ?>">
                        <h3 class="title"><?php _e('Deleted Membership Email', 'vimeo-sync-memberships'); ?></h3>
                        <table class="form-table">
                            <tbody>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('From Name', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_delete_membership_email[from_name]" name="rvs_delete_membership_email[from_name]" type="text" value="<?php echo $rvs_delete_membership_email['from_name']; ?>" />
                                        <label class="description" for="rvs_delete_membership_email[from_name]"><?php _e('Name of sender', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('From Email', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_delete_membership_email[from_email]" name="rvs_delete_membership_email[from_email]" type="email" value="<?php echo $rvs_delete_membership_email['from_email']; ?>" />
                                        <label class="description" for="rvs_delete_membership_email[from_email]"><?php _e('Email sent from', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Notification Email', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_delete_membership_email[notify_email]" name="rvs_delete_membership_email[notify_email]" type="text" value="<?php echo $rvs_delete_membership_email['notify_email']; ?>" />
                                        <label class="description" for="rvs_delete_membership_email[notify_email]"><?php _e('Admin email to receive notifications', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Email Subject', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <input class="regular-text code" id="rvs_delete_membership_email[subject]" name="rvs_delete_membership_email[subject]" type="text" value="<?php echo $rvs_delete_membership_email['subject']; ?>" />
                                        <label class="description" for="rvs_delete_membership_email[subject]"><?php _e('Email Subject', 'vimeo-sync-memberships'); ?></label>
                                    </td>
                                </tr>

                                <tr valign="top">
                                    <th scope="row" valign="top">
                                        <?php _e('Email Content', 'vimeo-sync-memberships'); ?>
                                    </th>
                                    <td>
                                        <?php wp_editor( $rvs_delete_membership_email['content'], 'rvs_delete_membership_email_content', $settings = array('textarea_name' => 'rvs_delete_membership_email[content]') );
                                        ?>
                                        <h4><?php _e('Variables: These will be replaced with the plan details', 'vimeo-sync-memberships'); ?></h4>
                                        <ul>
                                            <li>{planname}</li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php submit_button(); ?>
                </form>
        </div>
    </div>
</div>
