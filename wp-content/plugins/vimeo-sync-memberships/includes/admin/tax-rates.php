<?php
global $wpvs_tax_rate_manager;
$wpvs_tax_rates = $wpvs_tax_rate_manager->get_tax_rates();
?>
<div class="wrap">
    <h2 class="wp-admin-notice-placement"></h2>
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title"><?php _e('Tax Rates', 'vimeo-sync-memberships'); ?></h1>
            <a class="rvs-create-button rvs-button" href="<?php echo admin_url('admin.php?page=wpvs-add-new-tax-rate'); ?>"><?php _e('New Tax Rate', 'vimeo-sync-memberships'); ?></a>
            <table id="tax-rates" class="rvs_memberships">
                <tbody>
                    <tr>
                        <th><?php _e('Name', 'vimeo-sync-memberships'); ?></th>
                        <th><?php _e('Description', 'vimeo-sync-memberships'); ?></th>
                        <th><?php _e('Jurisdiction', 'vimeo-sync-memberships'); ?></th>
                        <th><?php _e('Rate', 'vimeo-sync-memberships'); ?></th>
                        <th><?php _e('Country', 'vimeo-sync-memberships'); ?></th>
                        <th><?php _e('Type', 'vimeo-sync-memberships'); ?></th>
                        <th><?php _e('Edit', 'vimeo-sync-memberships'); ?></th>
                    </tr>
            <?php
            if( ! empty($wpvs_tax_rates)) {
                foreach($wpvs_tax_rates as $tax_rate) { ?>
                    <tr>
                    <td><?php echo $tax_rate->display_name; ?></td>
                    <td><?php echo $tax_rate->description; ?></td>
                    <td><?php echo $tax_rate->jurisdiction; ?></td>
                    <td><?php echo $tax_rate->percentage; ?>%</td>
                    <td><?php echo $tax_rate->country; ?></td>
                    <td><?php
                    if( $tax_rate->inclusive ) {
                        _e('Inclusive', 'vimeo-sync-memberships');
                    } else {
                        _e('Exclusive', 'vimeo-sync-memberships');
                    }
                    ?>
                    </td>
                    <td><div class="wpvs-delete-icon wpvs-delete-tax-rate" data-tax-rate-id="<?php echo $tax_rate->id; ?>"><span class="dashicons dashicons-trash"></span></div></td>
                    </tr>
                <?php } ?>
                <?php } ?>
        </tbody>
    </table>
    </div>
</div>

</div>
