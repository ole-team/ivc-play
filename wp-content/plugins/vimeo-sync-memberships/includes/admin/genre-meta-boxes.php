<?php

if( ! function_exists('wpvs_video_category_membership_fields') ) {
function wpvs_video_category_membership_fields() {
    $wpvs_genre_naming = 'genre / category';
    global $wpvs_genre_slug_settings;
    if( ! empty($wpvs_genre_slug_settings) ) {
        if( isset($wpvs_genre_slug_settings['name']) ) {
            $wpvs_genre_naming = $wpvs_genre_slug_settings['name'];
        }
        if( isset($wpvs_genre_slug_settings['name-seasons']) ) {
            $wpvs_genre_naming .= ' / '.$wpvs_genre_slug_settings['name-seasons'];
        }
    }
    $wpvs_membership_list = get_option('rvs_membership_list');
	?>
    <h3><?php _e('Memberships &amp; Payments', 'vimeo-sync-memberships'); ?></h3>
    <div class="form-field term-slug-wrap">
        <label><strong><?php _e('Required Memberships', 'vimeo-sync-memberships'); ?></strong></label>
        <p><?php _e('Select required memberships to access videos within this '.$wpvs_genre_naming.' (optional)', 'vimeo-sync-memberships'); ?></p><br>
        <?php 
        if( ! empty($wpvs_membership_list) ) {
            foreach($wpvs_membership_list as $membership) { 
        ?>
            <label><input type="checkbox" value="<?php echo $membership['id']; ?>" name="wpvs_membership_select[]"><?php echo $membership['name']; ?></label><br>
        <?php
            } 
        } ?>
    </div>
    <div class="form-field term-slug-wrap">
        <label><strong><?php _e('Purchase Price', 'vimeo-sync-memberships'); ?></strong></label>
        <p><?php _e('Set a price customers can pay one time to get access to videos within this '.$wpvs_genre_naming.' (optional)', 'vimeo-sync-memberships'); ?></p>
        <input type="text" name="wpvs_category_purchase_price" value="" placeholder="29.95"/>
    </div>
<?php
}
add_action( 'rvs_video_category_add_form_fields', 'wpvs_video_category_membership_fields', 10, 2 );
}

if( ! function_exists('wpvs_video_category_edit_membership_fields') ) {
function wpvs_video_category_edit_membership_fields($term) {
    $wpvs_genre_naming = 'genre / category';
    global $wpvs_genre_slug_settings;
    if( ! empty($wpvs_genre_slug_settings) ) {
        if( isset($wpvs_genre_slug_settings['name']) ) {
            $wpvs_genre_naming = $wpvs_genre_slug_settings['name'];
        }
        if( isset($wpvs_genre_slug_settings['name-seasons']) ) {
            $wpvs_genre_naming .= ' / '.$wpvs_genre_slug_settings['name-seasons'];
        }
    }
    $wpvs_membership_list = get_option('rvs_membership_list');
	$wpvs_video_cat_memberships = get_term_meta($term->term_id, 'wpvs_category_memberships', true);
    if( empty($wpvs_video_cat_memberships) ) {
        $wpvs_video_cat_memberships = array();
    }
    $wpvs_video_cat_purchase_price = get_term_meta($term->term_id, 'wpvs_category_purchase_price', true);
    if( empty($wpvs_video_cat_purchase_price) ) {
        $wpvs_video_cat_purchase_price = "";
    } else {
        $wpvs_video_cat_purchase_price = number_format($wpvs_video_cat_purchase_price/100,2);
    }
    ?>
    <tr>
        <th scope="row" valign="top"><label><?php _e('Required Memberships', 'vimeo-sync-memberships'); ?></label></th>
            <td>
            <?php 
            if( ! empty($wpvs_membership_list) ) {
                foreach($wpvs_membership_list as $membership) { 
            ?>
                <label><input type="checkbox" value="<?php echo $membership['id']; ?>" <?php checked(in_array($membership['id'], $wpvs_video_cat_memberships)); ?> name="wpvs_membership_select[]"><?php echo $membership['name']; ?></label><br><br>
            <?php
                } 
            } ?>
            <p class="description"><?php _e('Select required memberships to access videos within this '.$wpvs_genre_naming.' (optional)', 'vimeo-sync-memberships'); ?></p>
            </td>
        </tr>
	<tr>
    <tr>
        <th scope="row" valign="top"><label><strong><?php _e('Purchase Price', 'vimeo-sync-memberships'); ?></strong></label></th>
            <td>
                <input type="text" name="wpvs_category_purchase_price" value="<?php echo $wpvs_video_cat_purchase_price; ?>" placeholder="29.95"/><br><br>
                <p class="description"><?php _e('Set a price customers can pay one time to get access to videos within this '.$wpvs_genre_naming.' (optional)', 'vimeo-sync-memberships'); ?></p>
            </td>
        </tr>
	<tr>   
	<?php
}
add_action( 'rvs_video_category_edit_form_fields', 'wpvs_video_category_edit_membership_fields', 10, 2 );
}

if( ! function_exists('wpvs_video_category_save_membership_fields') ) {
    function wpvs_video_category_save_membership_fields( $term_id ) {
        if ( isset( $_POST['wpvs_membership_select'] ) ) {
            $new_membership_list = $_POST['wpvs_membership_select'];
            update_term_meta($term_id, 'wpvs_category_memberships', $new_membership_list);
        } else {
            update_term_meta($term_id, 'wpvs_category_memberships', array());
        }
        
        if( isset($_POST['wpvs_category_purchase_price']) && ! empty($_POST['wpvs_category_purchase_price']) ) {
            $new_purchase_price = $_POST['wpvs_category_purchase_price'];
            if(strpos($new_purchase_price, '.')) {
                $new_purchase_price = str_replace(".", "", $new_purchase_price);
            } else {
                $new_purchase_price = intval($new_purchase_price) * 100;
            }
            update_term_meta($term_id, 'wpvs_category_purchase_price', $new_purchase_price);
        } else {
            update_term_meta($term_id, 'wpvs_category_purchase_price', null);
        }
        
    }
}
add_action( 'edited_rvs_video_category', 'wpvs_video_category_save_membership_fields', 10, 2 );  
add_action( 'create_rvs_video_category', 'wpvs_video_category_save_membership_fields', 10, 2 );