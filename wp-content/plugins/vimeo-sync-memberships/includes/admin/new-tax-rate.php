<div class="wrap">
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title"><?php _e('New Tax Rate', 'vimeo-sync-memberships'); ?></h1>
            <form id="wpvs-new-tax-rate" method="POST" action="" class="rvs-edit-form">
                <div class="rvs-field-section">
                    <label><?php _e('Tax Rate Name', 'vimeo-sync-memberships'); ?></label>
                    <input type="text" name="display_name" id="display_name" placeholder="GST" required />
                    <p class="description">This is the tax name that displays at checkout. Customers will see this name</p>
                </div>

                <div class="rvs-field-section">
                    <label>Description <em>(optional)</em></label>
                    <textarea rows="2" cols="50" maxlength="200" name="description" id="description"></textarea>
                    <p class="description">A brief description of the tax rate. Customers do not see this.</p>
                </div>

                <div class="rvs-field-section">
                    <label><?php _e('Tax Percentage', 'vimeo-sync-memberships'); ?></label>
                    <input placeholder="5.5" type="text" name="percentage" id="percentage" required />
                </div>

                <div class="rvs-field-section">
                    <label><?php _e('Tax Type', 'vimeo-sync-memberships'); ?></label>
                    <select id="inclusive" name="inclusive" required>
                        <option value=false><?php _e('Exclusive', 'vimeo-sync-memberships'); ?></option>
                        <option value=true><?php _e('Inclusive', 'vimeo-sync-memberships'); ?></option>
                    </select>
                </div>

                <div class="rvs-field-section">
                    <label><?php _e('Country', 'vimeo-sync-memberships'); ?></label>
                    <select id="wpvs_billing_country" name="country" required>
                        <?php include(RVS_MEMBERS_BASE_DIR.'/template/country-list.php'); ?>
                    </select>
                </div>
                <div class="rvs-field-section">
                    <label><?php _e('Jurisdiction', 'vimeo-sync-memberships'); ?> <em>(<?php _e('State', 'vimeo-sync-memberships'); ?> / <?php _e('Province', 'vimeo-sync-memberships'); ?>)</em></label>
                    <select id="wpvs_billing_state" name="jurisdiction" required>
                        <option value="">Select a Country</option>
                    </select>
                </div>

                <div class="rvs-field-section">
                    <input id="wpvs-new-tax-rate" type="submit" value="<?php _e('Add Tax Rate', 'vimeo-sync-memberships'); ?>" class="rvs-button rvs-primary-button" />
                </div>
            </form>
        </div>
    </div>
</div>
