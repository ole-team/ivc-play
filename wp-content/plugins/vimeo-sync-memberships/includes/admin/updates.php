<?php

function wpvs_database_updates_check() {
    $wpvs_customer_has_access = get_option('rvs-activated', false);
    global $wpvs_membership_update_required;
    global $wpvs_stripe_gateway_enabled;
    $wpvs_memberships_updates = get_option('wpvs-memberships-updated');

    if( ! $wpvs_customer_has_access) {
        function wpvs_account_not_activated_admin_message() {
            $wpvs_admin_message = '<div class="notice notice-warning is-dismissible"><h3>WP Video Subscriptions - Website Not Activated</h3><p>To receive updates for the WP Video Memberships plugin, please <a href="'.admin_url('admin.php?page=wpvs-activation').'">activate your website</a> using your login credentials from <a href="https://www.wpvideosubscriptions.com" target="_blank">www.wpvideosubscriptions.com</a>.</p> <p><a href="https://www.wpvideosubscriptions.com/wp-login.php?action=lostpassword" target="_blank">I forgot my password</a></p><p>If you need help activating your website, checkout our <a href="https://docs.wpvideosubscriptions.com/general/activating-your-website/" target="_blank">Website Activation Guide.</a></p></div>';
            echo $wpvs_admin_message;
        }
        add_action( 'admin_notices', 'wpvs_account_not_activated_admin_message' );
    }

    if( $wpvs_stripe_gateway_enabled ) {
        global $wpvs_stripe_options;
        if( ! isset($wpvs_stripe_options['webhook_id']) || ! isset($wpvs_stripe_options['test_webhook_id']) ) {
            function wpvs_stripe_webhook_endpoints_setup_required_admin_message() {
                $wpvs_stripe_setup_admin_message = '<div class="notice notice-warning is-dismissible"><h3>WP Video Subscriptions - Stripe Webhooks Setup Required</h3><p>You need to create new Stripe Webhook Endpoints. <a href="'.admin_url('admin.php?page=rvs-webhooks').'">Create Webhook Endpoints</a></p></div>';
                echo $wpvs_stripe_setup_admin_message;
            }
            add_action( 'admin_notices', 'wpvs_stripe_webhook_endpoints_setup_required_admin_message' );
        }
    }

    if( ! empty($wpvs_memberships_updates) ) {
        global $wpvs_admin_message;
        $wpvs_admin_message = sprintf(__('WP Video Memberships has an IMPORTANT update. You can see this update ', 'vimeo-sync-memberships') . '<a href="%s">' . __('here', 'vimeo-sync-memberships') .'</a>', admin_url('admin.php?page=wpvs-updates') );
        $wpvs_updates_version = intval(str_replace(".","",$wpvs_memberships_updates));

    }

    $vs_netflix_current_version = get_option('vs_netflix_current_version');
    if(!empty($vs_netflix_current_version) && get_option('vs_netflix_active')) {
        $vs_netflix_updates_version = intval(str_replace(".","",$vs_netflix_current_version));
        if($vs_netflix_updates_version < 500) {
            $wpvs_membership_update_required = true;
            add_action( 'admin_notices', 'vs_netflix_update_message_420' );
            function vs_netflix_update_message_420() {
                echo '<div class="update-nag">';
                $wpvs_netflix_admin_message = sprintf(__('The VS Netflix Theme needs an update. Please ', 'vimeo-sync-memberships') . '<a href="%s">' . __('upgrade to version 5.0.0', 'vimeo-sync-memberships') .'</a>', admin_url('themes.php') );
                echo $wpvs_netflix_admin_message;
                echo '</div>';
            }
        }
    }

    $wpvs_videos_current_version = get_option('wpv_vimeosync_current_version');
    if(!empty($wpvs_videos_current_version)) {
        $wpvs_videos_updates_version = intval(str_replace(".","",$wpvs_videos_current_version));

    }
}
add_action('admin_init', 'wpvs_database_updates_check');
