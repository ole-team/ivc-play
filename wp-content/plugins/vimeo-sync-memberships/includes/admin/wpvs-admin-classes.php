<?php
/**
* WP Video Memberships PayPal Membership Manager
*/

class WPVS_Membership_Manager extends WPVS_Membership_Plan {

    // CREATE NEW MEMBERSHIP (SHOULD CHECK CURRENT USER IS ADMIN BEFORE USING THIS FUNCTION)

    public function create_new_membership_plan($name, $amount, $description, $short_description, $interval, $interval_count, $trial_frequency, $trial_frequency_interval, $hidden = false, $create_role = false) {
        global $wpvs_membership_plans_list;
        $wpvs_membership_plans_updated = false;
        if( isset($this->plan_id) && ! empty($this->plan_id) ) {
            // CREATE NEW MEMBERSHIP PARAMETERS
            $new_membership_plan = array(
                'id'                => $this->plan_id,
                'name'              => $name,
                'amount'            => $amount,
                'description'       => $description,
                'short_description' => $short_description,
                'interval'          => $interval,
                'interval_count'    => $interval_count,
                'trial_frequency'   => $trial_frequency,
                'trial'             => $trial_frequency_interval,
                'hidden'            => $hidden,
                'has_role'          => $create_role
            );

            $wpvs_membership_plans_list[] = $new_membership_plan;
            $wpvs_membership_plans_updated = update_option('rvs_membership_list', $wpvs_membership_plans_list);
        }
        return $wpvs_membership_plans_updated;
    }

    public function add_stripe_plan_id($stripe_plan_id) {
        global $wpvs_membership_plans_list;
        if( ! empty($wpvs_membership_plans_list) ) {
            $wpvs_membership_plans_list[$this->plan_key]['stripe'] = array('plan_id' => $stripe_plan_id);
            return update_option('rvs_membership_list', $wpvs_membership_plans_list);
        }
    }

    public function delete_plan($remove_user_plans, $remove_role, $remove_stripe_plan) {
        global $wpvs_membership_plans_list;
        $membership_plan = $this->get_plan();
        $error_message = "";

        // DELETE STRIPE PLAN
        if( $remove_stripe_plan ) {
            global $wpvs_stripe_admin_ajax_requests;
            if( isset($membership_plan['stripe']) && isset($membership_plan['stripe']['plan_id']) ) {
                $wpvs_stripe_admin_ajax_requests->set_api_mode('live');
                $plan_deleted = $wpvs_stripe_admin_ajax_requests->wpvs_delete_stripe_plan($this->plan_id);
                if( ! empty($plan_deleted->error) ) {
                    $error_message .= $plan_deleted->error;
                }
                unset($membership_plan['stripe']);
            }

            if( isset($membership_plan['stripe_test']) && isset($membership_plan['stripe_test']['plan_id']) ) {
                $wpvs_stripe_admin_ajax_requests->set_api_mode('test');
                $plan_deleted = $wpvs_stripe_admin_ajax_requests->wpvs_delete_stripe_plan($this->plan_id);
                if( ! empty($plan_deleted->error) ) {
                    $error_message .= $plan_deleted->error;
                }
                unset($membership_plan['stripe_test']);
            }
        }

        //DELETE PAYPAL PLAN LIVE

        if(isset($membership_plan['paypal']) && isset($membership_plan['paypal']['plan_id']) && ! empty($membership_plan['paypal']['plan_id']) ) {
            $wpvs_paypal_manager =  new WPVS_PayPal_Plan_Manager('live', $membership_plan['paypal']['plan_id']);
            $wpvs_paypal_manager->delete_paypal_billing_plan();
            unset($membership_plan['paypal']);
        }

        if(isset($membership_plan['paypal_test']) && isset($membership_plan['paypal_test']['plan_id']) && ! empty($membership_plan['paypal_test']['plan_id']) ) {
            $wpvs_paypal_manager =  new WPVS_PayPal_Plan_Manager('sandbox', $membership_plan['paypal_test']['plan_id']);
            $wpvs_paypal_manager->delete_paypal_billing_plan();
            unset($membership_plan['paypal_test']);
        }

        $this->update_membership($membership_plan);
        if( empty($error_message) ) {
            if( ! empty($wpvs_membership_plans_list) ) {
                unset($wpvs_membership_plans_list[$this->plan_key]);
                wpvs_clear_membership_data($this->plan_id, $remove_user_plans, $remove_user_role);
                if( $remove_role ) {
                    $this->remove_membership_role();
                }
                return update_option('rvs_membership_list', $wpvs_membership_plans_list);
            }
        } else {
            return array('error' => $error_message);
        }
    }

    public function delete_stripe_plan() {
        $membership_plan = $this->get_plan();
        $error_message = "";
        global $wpvs_stripe_admin_ajax_requests;

        // DELETE STRIPE PLAN
        if( isset($membership_plan['stripe']) && isset($membership_plan['stripe']['plan_id']) ) {
            $wpvs_stripe_admin_ajax_requests->set_api_mode('live');
            $plan_deleted = $wpvs_stripe_admin_ajax_requests->wpvs_delete_stripe_plan($this->plan_id);
            if( ! empty($plan_deleted->error) ) {
                $error_message .= $plan_deleted->error;
            }
            unset($membership_plan['stripe']);
        }

        if( isset($membership_plan['stripe_test']) && isset($membership_plan['stripe_test']['plan_id']) ) {
            $wpvs_stripe_admin_ajax_requests->set_api_mode('test');
            $plan_deleted = $wpvs_stripe_admin_ajax_requests->wpvs_delete_stripe_plan($this->plan_id);
            if( ! empty($plan_deleted->error) ) {
                $error_message .= $plan_deleted->error;
            }
            unset($membership_plan['stripe_test']);
        }

        if( empty($error_message) ) {
            return $this->update_membership($membership_plan);
        } else {
            return array('error' => $error_message);
        }
    }

    public function create_membership_role($membership_name) {
        global $wp_roles;
        if( $wp_roles ) {
            $new_role_id = 'wpvs_'.$this->plan_id.'_role';
            $new_role_id_test = 'wpvs_'.$this->plan_id.'_role_test';
            if( empty($membership_name) ) {
                $membership_name = $this->name;
            }
            if( ! get_role($new_role_id) ) {
                $new_role_name = $membership_name.' '.__('Subscriber', 'vimeo-sync-memberships');
                $new_role_name_test = $membership_name.' '.__('(Test Mode) Subscriber', 'vimeo-sync-memberships');
                add_role( $new_role_id, $new_role_name, array( 'read' => true ) );
                add_role( $new_role_id_test, $new_role_name_test, array( 'read' => true ) );
                $this->add_to_wpvs_roles($new_role_id);
                $this->update_members_roles($new_role_id, "add");
            }
        }
    }

    public function remove_membership_role() {
        global $wp_roles;
        if( $wp_roles ) {
            $remove_role_id = 'wpvs_'.$this->plan_id.'_role';
            $remove_role_id_test = 'wpvs_'.$this->plan_id.'_role_test';
            $this->update_members_roles($remove_role_id, "remove");
            if( get_role($remove_role_id) ) {
                remove_role( $remove_role_id );
            }
            if( get_role($remove_role_id_test) ) {
                remove_role( $remove_role_id_test );
            }
            $this->remove_from_wpvs_roles($remove_role_id);
        }
    }

    private function update_members_roles($role_id, $action) {
        $wpvs_members_manager = new WPVS_Members_Manager();
        $members_with_plan = $wpvs_members_manager->get_all_members_with_membership($this->plan_id);
        if( isset($members_with_plan['live_members']) && ! empty($members_with_plan['live_members']) ) {
            foreach($members_with_plan['live_members'] as $user_id) {
                $user = get_user_by('id', $user_id);
                if( $action == "add" &&  ! in_array( $role_id, $user->roles ) ) {
                    $user->add_role($role_id);
                }
                if( $action == "remove" ) {
                    $user->remove_role($role_id);
                    if( empty( $user->roles ) ) {
                        $user->add_role("wpvs_member");
                    }
                }
            }
        }
        if( isset($members_with_plan['test_members']) && ! empty($members_with_plan['test_members']) ) {
            $test_role_id = $role_id.'_test';
            foreach($members_with_plan['test_members'] as $user_id) {
                $user = get_user_by('id', $user_id);
                if( $action == "add" && ! in_array( $test_role_id, $user->roles ) ) {
                    $user->add_role($test_role_id);
                }
                if( $action == "remove" ) {
                    $user->remove_role($test_role_id);
                    if( empty( $user->roles ) ) {
                        $user->add_role("wpvs_test_member");
                    }
                }
            }
        }

        // CHECK NO USERS WITH ROLE WERE MISSED
        if( $action == "remove" ) {
            $role_id_test = $role_id.'_test';
            $user_args = array(
                'blog_id'      => $GLOBALS['blog_id'],
                'role__in'     => array($role_id)
            );
            $found_members = get_users( $user_args );

            if( ! empty($found_members) ) {
                foreach($found_members as $member) {
                    $member->remove_role($role_id);
                    if( empty( $member->roles ) ) {
                       $member->add_role("wpvs_member");
                    }
                }
            }

            $user_test_args = array(
                'blog_id'      => $GLOBALS['blog_id'],
                'role__in'     => array($role_id_test)
            );
            $found_test_members = get_users( $user_test_args );

            if( ! empty($found_test_members) ) {
                foreach($found_test_members as $test_member) {
                    $test_member->remove_role($role_id_test);
                    if( empty( $test_member->roles ) ) {
                        $test_member->add_role("wpvs_test_member");
                    }
                }
            }
        }
    }

    private function add_to_wpvs_roles($role_id) {
        $wpvs_roles = get_option('wpvs_membership_roles', array());
        if( ! in_array($role_id, $wpvs_roles) ) {
             $wpvs_roles[] = $role_id;
        }
        $wpvs_roles = array_unique($wpvs_roles);
        update_option('wpvs_membership_roles', $wpvs_roles);
    }

    private function remove_from_wpvs_roles($role_id) {
        $wpvs_roles = get_option('wpvs_membership_roles', array());
        if( ! empty($wpvs_roles) && in_array($role_id, $wpvs_roles) ) {
            foreach($wpvs_roles as $key => &$role) {
                if( $role == $role_id) {
                    unset($wpvs_roles[$key]);
                    break;
                }
            }
            update_option('wpvs_membership_roles', $wpvs_roles);
        }
    }
}

/**
* WP Video Memberships Coupon Code Manager
*/

class WPVS_Coupon_Code_Manager extends WPVS_Coupon_Code {

    // CREATE NEW COUPON (SHOULD CHECK CURRENT USER IS ADMIN BEFORE USING THIS FUNCTION)

    public function create_new_coupon($name, $type, $amount, $duration, $month_duration = null, $use_count = null, $customer_use_count = null) {
        $wpvs_coupon_codes = get_option('rvs_coupon_codes', array());

        // CREATE NEW COUPON PARAMETERS
        $new_coupon = array(
            'id' => $this->coupon_id,
            'name' => $name,
            'type' => $type,
            'amount' => $amount,
            'duration' => $duration,
            'month_duration' => $month_duration,
            'max_uses' => $use_count,
            'max_uses_customer' => $customer_use_count
        );

        $wpvs_coupon_codes[] = $new_coupon;
        return update_option('rvs_coupon_codes', $wpvs_coupon_codes);
    }

    public function delete_coupon() {
        $wpvs_coupon_codes = get_option('rvs_coupon_codes', array());
        if( ! empty($wpvs_coupon_codes) ) {
            $this->remove_stripe_coupon();
            $this->remove_paypal_coupon_plans();
            unset($wpvs_coupon_codes[$this->coupon_key]);
            update_option('rvs_coupon_codes', $wpvs_coupon_codes);
        }
    }

    private function remove_paypal_coupon_plans() {
        global $wpvs_membership_plans_list;
        if( ! empty($wpvs_membership_plans_list) ) {
            foreach($wpvs_membership_plans_list as $key => &$plan) {
                if( isset($plan['paypal_coupon_plans']) && ! empty($plan['paypal_coupon_plans']) ) {
                    $paypal_coupon_plans = $plan['paypal_coupon_plans'];
                    foreach($paypal_coupon_plans as $c_key => $coupon_plan) {
                        if( $coupon_plan['id'] == $this->coupon_id ) {

                            if( isset($coupon_plan['paypal']) && isset($coupon_plan['paypal']['plan_id']) && ! empty($coupon_plan['paypal']['plan_id']) ) {
                                $wpvs_paypal_manager =  new WPVS_PayPal_Plan_Manager('live', $coupon_plan['paypal']['plan_id']);
                                $wpvs_paypal_manager->delete_paypal_billing_plan();
                            }

                            if( isset($coupon_plan['paypal_test']) && isset($coupon_plan['paypal_test']['plan_id']) && ! empty($coupon_plan['paypal_test']['plan_id']) ) {
                                $wpvs_paypal_manager =  new WPVS_PayPal_Plan_Manager('sandbox', $coupon_plan['paypal_test']['plan_id']);
                                $wpvs_paypal_manager->delete_paypal_billing_plan();
                            }
                            unset($paypal_coupon_plans[$c_key]);
                        }
                    }
                    $plan['paypal_coupon_plans'] = $paypal_coupon_plans;
                }
            }
            update_option('rvs_membership_list', $wpvs_membership_plans_list);
        }
    }

    private function remove_stripe_coupon() {
        global $wpvs_stripe_options;
        global $wpvs_stripe_api_version;
        if(isset($wpvs_stripe_options['enabled']) && $wpvs_stripe_options['enabled']) {
            global $wpvs_stripe_admin_ajax_requests;
            $delete_both_live_and_test = 2;
            while($delete_both_live_and_test > 0) {
                $stripe_delete =  false;
                if($delete_both_live_and_test == 2) {
                    $wpvs_stripe_admin_ajax_requests->set_api_mode('live');
                }
                if($delete_both_live_and_test == 1) {
                    $wpvs_stripe_admin_ajax_requests->set_api_mode('test');
                }
                $wpvs_stripe_admin_ajax_requests->delete_coupon_code($this->coupon_id);
                $delete_both_live_and_test--;
            }
        }
    }
}


class WPVS_Tax_Rate_Manager extends WPVS_Tax_Rate {

    public function __construct() {
        parent::__construct();
        add_action( 'wp_ajax_wpvs_admin_create_tax_rate', array( $this, 'wpvs_admin_create_tax_rate' ) );
        add_action( 'wp_ajax_wpvs_admin_delete_tax_rate', array( $this, 'wpvs_admin_delete_tax_rate' ) );

    }

    public function wpvs_admin_create_tax_rate() {
        if( wpvs_secure_admin_ajax() ) {
            if( isset($_POST['display_name']) && isset($_POST['percentage']) ) {
                global $wpvs_stripe_gateway_enabled;
                unset($_POST['action']);
                $new_tax_rate = stripslashes_deep($_POST);
                $new_tax_rate['percentage'] = number_format($new_tax_rate['percentage'], 2);
                $new_tax_rate['inclusive'] = filter_var($new_tax_rate['inclusive'], FILTER_VALIDATE_BOOLEAN);
                $wpvs_error_message = "";
                if( $this->tax_rate_exists($new_tax_rate['display_name'], $new_tax_rate['jurisdiction'], $new_tax_rate['country']) ) {
                    $wpvs_error_message = __('A tax rate for this jurisdiction already exists.', 'vimeo-sync-memberships');
                    rvs_exit_ajax($wpvs_error_message, 401);
                }
                // if stripe is enabled create stripe tax rate
                if( $wpvs_stripe_gateway_enabled ) {
                    global $wpvs_stripe_admin_ajax_requests;
                    $stripe_tax_rate_data = $new_tax_rate;
                    $create_both_live_and_test = 2;

                    while($create_both_live_and_test > 0) {
                        $new_tax_rate_created = null;
                        if($create_both_live_and_test == 2) {
                            $wpvs_stripe_admin_ajax_requests->set_api_mode('live');
                            $stripe_tax_rate_id_key = 'stripe_tax_rate_id';
                        }
                        if($create_both_live_and_test == 1) {
                            $wpvs_stripe_admin_ajax_requests->set_api_mode('test');
                            $stripe_tax_rate_id_key = 'test_stripe_tax_rate_id';
                        }
                        $new_tax_rate_created = $wpvs_stripe_admin_ajax_requests->create_tax_rate($stripe_tax_rate_data);
                        if( $new_tax_rate_created->created && ! empty($new_tax_rate_created->new_tax_rate) ) {
                            $new_tax_rate['meta'][$stripe_tax_rate_id_key] = $new_tax_rate_created->new_tax_rate->id;
                        } else {
                            $wpvs_error_message .= $new_tax_rate_created->error;
                        }
                        $create_both_live_and_test--;
                    }
                }
                if( ! empty($wpvs_error_message) ) {
                    rvs_exit_ajax($wpvs_error_message, 401);
                } else {
                    $tax_rate_created = $this->create_tax_rate($new_tax_rate);
                    if( empty($tax_rate_created) ) {
                        rvs_exit_ajax(__('Tax Rate may not have been created.', 'vimeo-sync-memberships'), 401);
                    }
                }
            }
        } else {
            $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 401);
        }
        wp_die();
    }

    public function wpvs_admin_delete_tax_rate($tax_rate_id) {
        if( wpvs_secure_admin_ajax() ) {
            if( isset($_POST['tax_rate_id']) ) {
                $tax_rate_deleted = $this->delete_tax_rate($_POST['tax_rate_id']);
                if( ! empty($tax_rate_deleted->error) ) {
                    rvs_exit_ajax($tax_rate_deleted->error, 401);
                }
            }
        } else {
            $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 401);
        }
        wp_die();
    }

    private function delete_tax_rate($tax_rate_id) {
        global $wpdb;
        global $wpvs_stripe_admin_ajax_requests;
        $tax_rate_deleted = array(
            'deleted' => false,
            'error'   => null,
        );
        $tax_rate = $this->get_tax_rate_by_id($tax_rate_id);
        $tax_rate_updates = array(
            'active' => false
        );
        $tax_rate_meta = json_decode($tax_rate->meta, false);
        if( ! empty($tax_rate_meta->stripe_tax_rate_id) ) {
            $wpvs_stripe_admin_ajax_requests->set_api_mode('live');
            $stripe_tax_rate_updated = $wpvs_stripe_admin_ajax_requests->update_tax_rate($tax_rate_meta->stripe_tax_rate_id, $tax_rate_updates);
            if( ! empty($stripe_tax_rate_updated->error) ) {
                $tax_rate_deleted['error'] .= $stripe_tax_rate_updated->error;
            }
        }
        if( ! empty($tax_rate_meta->test_stripe_tax_rate_id) ) {
            $wpvs_stripe_admin_ajax_requests->set_api_mode('test');
            $stripe_tax_rate_updated = $wpvs_stripe_admin_ajax_requests->update_tax_rate($tax_rate_meta->test_stripe_tax_rate_id, $tax_rate_updates);
            if( ! empty($stripe_tax_rate_updated->error) ) {
                $tax_rate_deleted['error'] .= $stripe_tax_rate_updated->error;
            }
        }
        $wpdb->get_results("DELETE FROM $this->tax_rate_table_name WHERE id = '$tax_rate_id'");
        if( empty($tax_rate_deleted['error']) ) {
            $tax_rate_deleted['deleted'] = true;
        }
        return (object) $tax_rate_deleted;
    }

    private function create_tax_rate($tax_rate_data) {
        global $wpdb;
        if( ! isset($tax_rate_data['meta']) ) {
            $tax_rate_data['meta'] = "";
        } else {
            $tax_rate_data['meta'] = json_encode($tax_rate_data['meta']);
        }
        $tax_rate_created = $wpdb->insert(
            $this->tax_rate_table_name,
            $tax_rate_data
        );
        return $tax_rate_created;
    }

    public function create_tax_rate_table() {
        global $wpdb;
        if( ! $this->tax_rate_table_exists() ) {
            $wpvs_tax_rate_tables = "CREATE TABLE $this->tax_rate_table_name (
              id mediumint(20) NOT NULL UNIQUE AUTO_INCREMENT,
              display_name tinytext NOT NULL,
              description TEXT NOT NULL,
              jurisdiction tinytext NOT NULL,
              percentage FLOAT NOT NULL,
              country tinytext NOT NULL,
              inclusive boolean,
              meta TEXT,
              PRIMARY KEY (id)
            ) $charset_collate;";
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $wpvs_tax_rate_tables );
        }
    }

    private function tax_rate_table_exists() {
        global $wpdb;
        $tax_rates_table_exists = true;
        if($wpdb->get_var("SHOW TABLES LIKE '$this->tax_rate_table_name'") != $this->tax_rate_table_name) {
             $tax_rates_table_exists = false;
        }
        return $tax_rates_table_exists;
    }
}
global $wpvs_tax_rate_manager;
$wpvs_tax_rate_manager = new WPVS_Tax_Rate_Manager();
