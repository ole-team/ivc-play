<?php
require_once(RVS_MEMBERS_BASE_DIR . '/stripe/init.php');
global $rvs_live_mode;
global $rvs_currency;
global $wpvs_stripe_gateway_enabled;
global $rvs_paypal_enabled;
$member = $_GET['id'];
if($rvs_live_mode == "off") {
    $stripe_url = 'https://dashboard.stripe.com/test/customers/';
    $customer_key = 'rvs_stripe_customer_test';
    $get_memberships = 'rvs_user_memberships_test';
    $get_payer_id = 'rvs_paypal_test_payer_id';
    $get_paypal_email = 'rvs_paypal_test_email';
} else {
    $stripe_url = 'https://dashboard.stripe.com/customers/';
    $customer_key = 'rvs_stripe_customer';
    $get_memberships = 'rvs_user_memberships';
    $get_payer_id = 'rvs_paypal_payer_id';
    $get_paypal_email = 'rvs_paypal_email';
}
$stripe_customer_id = get_user_meta($member, $customer_key, true);
$user_memberships = get_user_meta($member, $get_memberships, true);
$user = get_userdata( $member );
$paypal_payer = get_user_meta($member, $get_payer_id, true);
$paypal_email = get_user_meta($member, $get_paypal_email, true);

// GET All Memberships
$membership_list = get_option('rvs_membership_list');
if(!empty($membership_list) && !empty($user_memberships)) {
    $add_memberships = wpv_key_compare_plans($membership_list, $user_memberships);
}
if(empty($user_memberships) && !empty($membership_list)) {
    foreach($membership_list as $key=> $plan) {
        $add_memberships[] = $plan;
    }
}

// GET MEMBER RENTALS
//$user_rentals = rvs_get_user_rentals($member);
?>
<div class="wrap">
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h3 class="wpvs-admin-section-title">Managing <?php echo $user->user_login; ?></h3>
            <table class="rvs_memberships">
                <tbody>
                    <tr>
                        <th>Username:</th>
                        <th>Stripe Customer:</th>
                        <th>PayPal Payer ID:</th>
                        <th>PayPal Email:</th>
                    </tr>
                    <tr>
                        <td><?php echo $user->user_login; ?>: <a href="<?php echo admin_url('user-edit.php?user_id='.$user->ID); ?>">Edit User</a></td>
                        <td>
                    <?php if(!empty($stripe_customer_id)) : ?>
                        <?php echo $stripe_customer_id; ?> | <a href="<?php echo $stripe_url . $stripe_customer_id; ?>" target="_blank">View in Stripe</a>
                    <?php else : ?>
                        No Stripe Customer
                    <?php endif; ?>
                    </td>
                    <td>
                    <?php if( !empty($paypal_payer) ) :?>
                        <?php echo $paypal_payer; ?>
                    <?php else : ?>
                        No PayPal Customer ID
                    <?php endif; ?>
                    </td>
                    <td>
                        <?php if( !empty($paypal_email) ) :?>
                        <?php echo $paypal_email; ?>
                    <?php else : ?>
                        No PayPal Customer Email
                    <?php endif; ?>
                    </td>
                    </tr>
                </tbody>
            </table>
            <h3 class="wpvs-admin-section-title">Membership Access</h3>
            <div id="wpvs-user-memberships">
            <?php if(!empty($user_memberships)) {
                echo wpvs_generate_user_memberships_output($user_memberships, true);
            } ?>
            </div>
            <?php if(!empty($add_memberships)) { ?>
                <div class="border-box">
                <label id="rvs-add-memberships" class="rvs-button rvs-button-blue"><span class="icon-switch"><i class="dashicons-plus dashicons"></i></span> <?php _e('Add membership to', 'vimeo-sync-memberships'); ?> <?php echo $user->user_login; ?></label>
                </div>
                <div id="rvs-other-memberships" class="border-box rvs-box text-align-right">
                    <?php foreach($add_memberships as $new_plan) { ?>
                        <label id="<?php echo $new_plan['id']; ?>" class="rvs-add-membership"><i class="dashicons-plus dashicons"></i> <?php echo $new_plan['name']; ?></label>
                     <?php } ?>
                </div>
            <?php } ?>
            <?php if($wpvs_stripe_gateway_enabled) { ?>
            <h3 class="wpvs-admin-section-title"><?php _e('Import Customer Payments', 'vimeo-sync-memberships'); ?></h3>
            <div class="wpvs-admin-inline-button">
                <label id="import-stripe-customer-payments" class="wpvs-admin-inline-button"><span class="dashicons dashicons-update"></span> <?php _e('Import Stripe Payments', 'vimeo-sync-memberships'); ?></label>
            </div>
            <?php } ?>
            <h3 class="wpvs-admin-section-title">Payments</h3>
            <div class="wpvs-admin-split-section">
                <div class="wpvs-admin-split-labels">
                    <label class="wpvs-admin-split-label active" data-section="#stripe-payments"><?php _e('Credit Card', 'vimeo-sync-memberships'); ?></label>
                    <label class="wpvs-admin-split-label" data-section="#paypal-payments"><?php _e('PayPal', 'vimeo-sync-memberships'); ?></label>
                </div>
                <div id="stripe-payments" class="wpvs-admin-split active">
                    <table id="payments" class="rvs_memberships">
                        <tbody>
                            <tr>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Product</th>
                                <th>Refund</th>
                            </tr>
                        </tbody>
                    </table>
                    <nav id="rvs-payment-nav" class="wpvs-admin-payments-nav">
                        <label id="wpvs-next-stripe-payments" class="rvs-button rvs-primary-button">More Payments</label>
                    </nav>
                </div>
                 <div id="paypal-payments" class="wpvs-admin-split">
                    <table id="customer-paypal-payments" class="rvs_memberships">
                        <tbody>
                            <tr>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Product</th>
                                <th>Refund</th>
                            </tr>
                        </tbody>
                    </table>
                    <nav id="wpvs-paypal-payment-nav" class="wpvs-admin-payments-nav">
                        <label id="wpvs-next-paypal-payments" class="rvs-button rvs-primary-button">More Payments</label>
                    </nav>
                </div>
            </div>
        </div>
</div>
<script>
var stripe_id = '<?php echo $stripe_customer_id; ?>';
var user_id = '<?php echo $member; ?>';
var wpvs_admin_nonce = '<?php echo wp_create_nonce('wpvs-admin-nonce'); ?>';
jQuery(document).ready(function() {
    <?php if(!empty($stripe_customer_id)) : ?>
    wpvs_get_customer_stripe_payments(user_id);
     <?php else : ?>
    jQuery('#rvs-payment-nav').html('<p>No Credit Card Payments</p>');
    <?php endif; ?>
});
</script>
