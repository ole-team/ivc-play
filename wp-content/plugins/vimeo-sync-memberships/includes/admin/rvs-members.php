<?php
global $wpvs_stripe_gateway_enabled;
global $rvs_live_mode;

$all_roles = get_editable_roles();
$wpvs_roles = get_option('wpvs_membership_roles', array());
$wpvs_test_roles = array();
if( ! empty($wpvs_roles) ) {
    foreach($wpvs_roles as $add_role) {
        $wpvs_test_roles[] = $add_role.'_test';
    }
}
$list_of_roles = array();
if($rvs_live_mode == "on") {
    $default_member_filter = "wpvs_member";
    $default_member_filter_text = __('Member', 'vimeo-sync-memberships');
    if( ! empty($all_roles) && ! empty($wpvs_roles) ) {
        foreach($all_roles as $key => $role) {
            if( in_array($key, $wpvs_roles) ) {
                $list_of_roles[$key] = $role['name'];
            }
        }
    }

} else {
    $default_member_filter = "wpvs_test_member";
    $default_member_filter_text = __('TEST Member', 'vimeo-sync-memberships');
    if( ! empty($all_roles) && ! empty($wpvs_test_roles) ) {
        foreach($all_roles as $key => $role) {
            if( in_array($key, $wpvs_test_roles) ) {
                $list_of_roles[$key] = $role['name'];
            }
        }
    }
}

?>
<div class="wrap">
    <h2 class="wp-admin-notice-placement"></h2>
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title">Members</h1>
            <div id="rvs-search-for-members" class="border-box rvs-box">
                <input type="email" id="wpvs-search-user-input" placeholder="Name, Email or User ID" />
                <label id="rvs-search-member" class="rvs-button"><span class="dashicons dashicons-search"></span> Search</label>
                <label class="rvs-button" id="wpvs-refresh-members-page"><span class="dashicons dashicons-update"></span> Refresh</label>
                <a class="rvs-button" href="<?php echo admin_url('admin.php?page=wpvs-bulk-members-editor'); ?>"><span class="dashicons dashicons-groups"></span> Bulk Manage</a>
            </div>
            <?php if( ! empty($list_of_roles) ) { ?>
            <div class="wpvs-filter-options rvs-border-box">
                <label class="wpvs-filter-label"><?php _e('Filter', 'vimeo-sync-memberships'); ?><span class="dashicons dashicons-filter"></span></label>
                <div class="wpvs-filter-setting">
                    <select id="wpvs_filter_members_by_role" name="wpvs_filter_members_by_role">
                        <option value="<?php echo $default_member_filter; ?>"><?php echo $default_member_filter_text; ?></option>
                        <?php foreach($list_of_roles as $key => $member_role) { ?>
                            <option value="<?php echo $key; ?>"><?php echo $member_role; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php } ?>

            <div id="wpvs-loading-members" class="wpvs-update-content"><span id="wpvs-update-text" class="wpvs-loading-text">Getting Members...</span><span class="loadingCircle"></span><span class="loadingCircle"></span><span class="loadingCircle"></span><span class="loadingCircle"></span></div>


            <div id="wpvs-members-loaded"></div>

            <div class="rvs-navigation">
            <div class="border-box rvs-previous-button rvs-nav-buttons">
                <label class="rvs-button rvs-button-blue rvs-back-members">&#60; Previous Page</label>
            </div>
            <div class="border-box text-align-right rvs-next-button rvs-nav-buttons">
                <label class="rvs-button rvs-button-blue rvs-more-members">Next Page &#62;</label>
            </div>
            </div>
        </div>
    </div>
</div>
