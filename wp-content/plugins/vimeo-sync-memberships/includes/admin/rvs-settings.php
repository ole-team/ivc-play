<div class="wrap">
    <h2 class="wp-admin-notice-placement"></h2>
    <?php include('rvs-admin-menu.php'); ?>
    <div id="rvs-page-settings" class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title">Membership Settings</h1>
            <form method="post" action="options.php">
                <?php settings_fields( 'wpvs-membership-settings' );
                    $pages = get_pages();
                    $frontpage_id = get_option( 'page_on_front' );
                    $rvs_sign_up_page = esc_attr( get_option('rvs_sign_up_page'));
                    $rvs_payment_page = esc_attr( get_option('rvs_payment_page'));
                    $rvs_account_page = esc_attr( get_option('rvs_account_page'));
                    $rvs_create_account_page = esc_attr( get_option('rvs_create_account_page'));
                    $rvs_redirect_page = get_option('rvs_redirect_page', $rvs_create_account_page);
                    $rvs_redirect_login = get_option('rvs_redirect_login', 'same_page');
                    $rvs_redirect_signup_link = get_option('rvs_redirect_signup_link', '');
                    $rvs_redirect_login_link = get_option('rvs_redirect_login_link', '');

                    $wpvs_require_user_login = get_option('wpvs_require_user_login', 0);
                    $wpvs_require_login_page = get_option('wpvs_require_login_page', $rvs_create_account_page);
                    $wpvs_site_lock_redirect_logged_in_users_page = get_option('wpvs_site_lock_redirect_logged_in_users_page', $rvs_sign_up_page);
                    $wpvs_landing_page = get_option('wpvs_landing_page', $frontpage_id);
                    $register_page_title = get_the_title($rvs_sign_up_page);
                    $account_page_title = get_the_title($rvs_account_page);
                    $rvs_user_rentals_page = esc_attr( get_option('rvs_user_rental_page') );
                    $rvs_user_purchases_page = esc_attr( get_option('rvs_user_purchase_page'));
                    $rvs_account_sub_menu = get_option('rvs_account_sub_menu', 1);

                    $wpvs_site_lock_required_memberships = get_option('wpvs_site_lock_required_memberships', array());
                    if( empty($wpvs_site_lock_required_memberships) ) {
                        $wpvs_site_lock_required_memberships = array();
                    }
                    $wpvs_membership_list = get_option('rvs_membership_list', array());
                    $wpvs_signup_require_first_last_name = get_option('wpvs_signup_require_first_last_name', 0);
                    $wpvs_signup_create_password = get_option('wpvs_signup_create_password', 1);
                    $wpvs_default_account_menu_item = get_option('wpvs_default_account_menu_item', 'memberships');
                    $wpvs_users_can_delete_memberships = get_option('wpvs_users_can_delete_memberships', 1);

                    $wpvs_login_restrictions = get_option('wpvs_login_restrictions');
                ?>
                <div class="rvs-container">
                    <ul class="subsubsub">
                        <li><a href="#wpvs-memberships-page-settings"><?php _e('Page Settings', 'vimeo-sync-memberships'); ?></a> | </li>
                        <li><a href="#wpvs-memberships-account-settings"><?php _e('Account Settings', 'vimeo-sync-memberships'); ?></a> | </li>
                        <li><a href="#wpvs-memberships-sitelock-settings"><?php _e('Site Lock Settings', 'vimeo-sync-memberships'); ?></a> | </li>
                        <li><a href="#wpvs-memberships-color-settings"><?php _e('Colours', 'vimeo-sync-memberships'); ?></a> | </li>
                    </ul>
                </div>
                <h2>Membership Pages</h2>
                <div id="wpvs-membership-page-settings" class="rvs-container rvs-box">
                    <div class="rvs-col-6">
                        <div class="wpvs-settings-box">
                            <h3>Register Page</h3>
                            <select id="rvs_sign_up_page" name="rvs_sign_up_page">
                                <?php
                                    foreach($pages as $page) {
                                        echo '<option value="'.$page->ID.'"' . selected( $page->ID,  esc_attr( $rvs_sign_up_page )) . ' >'.$page->post_title.'</option>';
                                    }
                                ?>
                            </select><br>
                            <label>Include shortcode <strong>[rvs_memberships]</strong> on page.</label>
                        </div>
                        <div class="wpvs-settings-box">
                            <h3>Checkout Page</h3>
                            <select id="rvs_payment_page" name="rvs_payment_page">
                                <?php
                                    foreach($pages as $page) {
                                        echo '<option value="'.$page->ID.'"' . selected( $page->ID,  esc_attr( $rvs_payment_page )) . ' >'.$page->post_title.'</option>';
                                    }
                                ?>

                            </select><br>
                            <label>Include shortcode <strong>[rvs_payment_form]</strong> on page.</label>
                        </div>
                        <div class="wpvs-settings-box">
                            <h3>Account Page</h3>
                            <select id="rvs_account_page" name="rvs_account_page">
                                <?php
                                    foreach($pages as $page) {
                                        echo '<option value="'.$page->ID.'"' . selected( $page->ID,  esc_attr( $rvs_account_page )) . ' >'.$page->post_title.'</option>';
                                    }
                                ?>

                            </select><br>
                            <p><strong>Account (default)</strong></p>
                            <p>Include shortcode <strong>[rvs_account]</strong> on page.</p>
                            <h3>Default Account Menu Tab</h3>
                            <select name="wpvs_default_account_menu_item">
                                <option value="memberships" <?php selected('memberships', $wpvs_default_account_menu_item); ?>><?php _e('Memberships', 'vimeo-sync-memberships'); ?></option>
                                <option value="info" <?php selected('info', $wpvs_default_account_menu_item); ?>><?php _e('Account Info', 'vimeo-sync-memberships'); ?></option>
                            </select>
                            <p>Sets the default menu item when the Account page is loaded.</p>
                            <p><em>* only applicable if using the default <strong>Account</strong> page above.</em></p>
                            <p><em>If you would like to set the Account landing page to <strong>Purchases</strong>, <strong>Rentals</strong>, or <strong>My List</strong>, set the <strong>Account Page</strong> setting above to your Purchases, Rentals or My List page.</em></p>
                            <p><a href="https://docs.wpvideosubscriptions.com/wp-video-memberships/account-page-setup" target="_blank" rel="help">Account Page Setup Guide</a> | <a href="https://docs.wpvideosubscriptions.com/wp-video-memberships/shortcodes/" target="_blank" rel="help">Shortcode Parameters</a></p>
                        </div>
                    </div>
                    <div class="rvs-col-6">
                        <div class="wpvs-settings-box">
                            <h3>Create Account Page</h3>
                            <select id="rvs_create_account_page" name="rvs_create_account_page">
                                <?php
                                    foreach($pages as $page) {
                                        echo '<option value="'.$page->ID.'"' . selected( $page->ID,  esc_attr( $rvs_create_account_page )) . ' >'.$page->post_title.'</option>';
                                    }
                                ?>

                            </select><br>
                            <label>Include shortcode <strong>[rvs_create_account]</strong> on page.</label>
                        </div>
                        <div class="wpvs-settings-box">
                            <h3>User Rentals Page</h3>
                            <select id="rvs_user_rental_page" name="rvs_user_rental_page">
                                <option value="">--<?php _e('Disable', 'vimeo-sync-memberships'); ?>--</option>
                                <?php
                                    foreach($pages as $page) {
                                        echo '<option value="'.$page->ID.'"' . selected( $page->ID,  esc_attr( $rvs_user_rentals_page )) . ' >'.$page->post_title.'</option>';
                                    }
                                ?>

                            </select><br>
                            <label>Include shortcode <strong>[rvs_user_rentals]</strong> on page.</label>
                        </div>
                        <div class="wpvs-settings-box">
                            <h3>User Purchases Page</h3>
                            <select id="rvs_user_purchase_page" name="rvs_user_purchase_page">
                                <option value="">--<?php _e('Disable', 'vimeo-sync-memberships'); ?>--</option>
                                <?php
                                    foreach($pages as $page) {
                                        echo '<option value="'.$page->ID.'"' . selected( $page->ID,  esc_attr( $rvs_user_purchases_page )) . ' >'.$page->post_title.'</option>';
                                    }
                                ?>
                            </select><br>
                            <label>Include shortcode <strong>[rvs_user_purchases]</strong> on page.</label>
                        </div>
                    </div>
                    <div class="wpvs-save-settings-box">
                        <input type="submit" name="submit" class="button button-primary" value="Save Changes">
                    </div>
                </div>
                <h2><?php _e('Account Settings', 'vimeo-sync-memberships'); ?></h2>
                <div id="wpvs-memberships-account-settings" class="rvs-container rvs-box">
                    <div class="rvs-col-6">
                        <div class="wpvs-settings-box">
                            <h3>New Account Redirect</h3>
                            <p>Where should new users be taken after they create an account?</p>
                            <select id="rvs_redirect_page" name="rvs_redirect_page">
                                <option value="noredirect" <?php selected( $rvs_redirect_page,  "noredirect"); ?>>Same page (No Redirect)</option>
                                <option value="<?php echo $rvs_account_page; ?>" <?php selected( $rvs_account_page, $rvs_redirect_page ); ?>><?php echo $account_page_title; ?></option>
                                <option value="<?php echo $rvs_sign_up_page; ?>" <?php selected( $rvs_sign_up_page, $rvs_redirect_page ); ?>><?php echo $register_page_title; ?></option>
                                <option value="custom" <?php selected( "custom",  $rvs_redirect_page); ?>>Custom</option>
                            </select>
                            <h4>Enter a custom redirect link if using <strong>Custom</strong></h4>
                            <input type="url" class="regular-text" name="rvs_redirect_signup_link" value="<?php echo $rvs_redirect_signup_link; ?>" placeholder="Custom redirect link"/>
                        </div>
                        <div class="wpvs-settings-box">
                            <h3>Login Redirect</h3>
                            <p>Where should users be taken after they login?</p>
                            <select id="rvs_redirect_login" name="rvs_redirect_login">
                                <option value="same_page" <?php selected( $rvs_redirect_login,  "same_page"); ?>>Same page (No Redirect)</option>
                                <option value="<?php echo $rvs_account_page; ?>" <?php selected( $rvs_redirect_login,  esc_attr( $rvs_account_page )); ?>><?php echo $account_page_title; ?></option>
                                <option value="home" <?php selected( $rvs_redirect_login,  "home"); ?>>Home Page</option>
                                <option value="custom" <?php selected( "custom",  $rvs_redirect_login); ?>>Custom</option>
                            </select>
                            <h4>Enter a custom redirect link if using <strong>Custom</strong></h4>
                            <input type="url" class="regular-text" name="rvs_redirect_login_link" value="<?php echo $rvs_redirect_login_link; ?>" placeholder="Custom redirect link"/>
                        </div>
                        <div class="wpvs-settings-box">
                            <h3>Display Account Sub menu</h3>
                            <input type="checkbox" id="rvs_account_sub_menu" name="rvs_account_sub_menu" value="1" <?php checked(1, $rvs_account_sub_menu); ?> />
                            <p>Display the account page sub menu (Memberships, Credit Cards, Payments, Logout)</p>
                        </div>
                        <div class="wpvs-settings-box">
                            <h3>Membership Management</h3>
                            <label for="wpvs_users_can_delete_memberships"><input type="checkbox" name="wpvs_users_can_delete_memberships" value="1" <?php checked(1, $wpvs_users_can_delete_memberships); ?> /><?php _e('Allow customers to delete memberships from their account', 'vimeo-sync-memberships'); ?>.</label>
                        </div>
                        <div class="wpvs-settings-box">
                            <h3>Login Restrictions</h3>
                            <label for="wpvs_login_restrictions[enabled]"><input type="checkbox" name="wpvs_login_restrictions[enabled]" value="1" <?php checked(1, $wpvs_login_restrictions['enabled']); ?> />Enable account login restrictions.</label>
                            <p class="description"><?php _e('Prevents users from logging in multiple times', 'vimeo-sync-memberships'); ?>.</p>
                            <h4><?php _e('Login Limit', 'vimeo-sync-memberships'); ?></h4>
                            <label><input type="number" min="1" max="50" name="wpvs_login_restrictions[login_limit]" value="<?php echo $wpvs_login_restrictions['login_limit'] ?>"  /> <?php _e('Set the maximum number of logins a user can have', 'vimeo-sync-memberships'); ?>.</label>
                            <h4>Login Logic</h4>
                            <select name="wpvs_login_restrictions[login_logic]">
                                <option value="allow" <?php selected( $wpvs_login_restrictions['login_logic'],  'allow'); ?>><?php _e('Allow', 'vimeo-sync-memberships'); ?></option>
                                <option value="block" <?php selected( $wpvs_login_restrictions['login_logic'],  'block'); ?>><?php _e('Block', 'vimeo-sync-memberships'); ?></option>
                            </select>
                            <ul>
                                <li><strong><?php _e('Allow', 'vimeo-sync-memberships'); ?>:</strong> <?php _e('Allow new login by removing all old sessions when the login limit is reached', 'vimeo-sync-memberships'); ?>. <?php _e('This will automatically logout the user on other devices when their login limit is reached', 'vimeo-sync-memberships'); ?>.</li>
                                <li><strong><?php _e('Block', 'vimeo-sync-memberships'); ?>:</strong> <?php _e('Do not allow new logins until the users old sessions have expired', 'vimeo-sync-memberships'); ?>. <?php _e('Users will need to logout of other devices before logging into new devices', 'vimeo-sync-memberships'); ?>.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="rvs-col-6">
                        <div class="wpvs-settings-box">
                        <h3>Usernames Allowed</h3>
                            <?php $rvs_usernames_allowed = get_option('rvs_usernames_allowed', 1); ?>
                            <input type="checkbox" id="rvs_usernames_allowed" name="rvs_usernames_allowed" value="1" <?php checked(1, $rvs_usernames_allowed); ?> />
                            <p>Allow users to create usernames when creating an account. If left unchecked, users emails will be used for usernames.</p>
                        </div>
                        <div class="wpvs-settings-box">
                        <h3><?php _e('Require Name On Sign Up', 'vimeo-sync-memberships'); ?></h3>
                            <input type="checkbox" name="wpvs_signup_require_first_last_name" value="1" <?php checked(1, $wpvs_signup_require_first_last_name); ?> />
                            <p>Require First and Last Name when new customers sign up.</p>
                        </div>
                        <div class="wpvs-settings-box">
                        <h3><?php _e('Create Password On Sign Up', 'vimeo-sync-memberships'); ?></h3>
                            <input type="checkbox" name="wpvs_signup_create_password" value="1" <?php checked(1, $wpvs_signup_create_password); ?> />
                            <p>Customers create their password during sign up. <em>If disabled, new customers will be sent a password creation link after sign up.</em></p>
                        </div>
                        <div class="wpvs-settings-box">
                            <h3>Restrict WP Admin Access</h3>
                            <input id="rvs_admin_access" name="rvs_admin_access" type="checkbox" value="1" <?php checked(1, get_option('rvs_admin_access')); ?> />
                            <p>Remove admin access for members. This will redirect members to the <strong>account page</strong>  when/if they try to access the default WordPress admin area.</p>
                        </div>
                        <div class="wpvs-settings-box">
                            <h3>No access for Overdue subscriptions</h3>
                            <?php $rvs_overdue_access = get_option('rvs_overdue_access', 0); ?>
                            <input type="checkbox" id="rvs_overdue_access" name="rvs_overdue_access" value="1" <?php checked(1, $rvs_overdue_access); ?> />
                            <p>Enabling this means that users with Overdue subscription payments will <strong>not</strong> have access to their video content.</p>
                        </div>
                    </div>
                    <div class="wpvs-save-settings-box">
                        <input type="submit" name="submit" class="button button-primary" value="Save Changes">
                    </div>
                </div>
                <h2>Site Lock Settings</h2>
                <div id="wpvs-memberships-sitelock-settings" class="rvs-container rvs-box">
                    <div class="rvs-col-6">
                        <div class="wpvs-settings-box">
                            <h3>Lock Entire Site</h3>
                            <?php $wpvs_require_user_login = get_option('wpvs_require_user_login', 0); ?>
                            <label class="selectit" for="wpvs_require_user_login"><input type="checkbox" id="wpvs_require_user_login" name="wpvs_require_user_login" value="1" <?php checked(1, $wpvs_require_user_login); ?> /> <?php _e('Enabled', 'vimeo-sync-memberships'); ?></label>
                            <p>Forces users to login to browse videos and pages.</p>
                            <h4><?php _e('Required Membership(s)', 'vimeo-sync-memberships'); ?></h4>
                            <?php if(!empty($wpvs_membership_list)) {
                                foreach($wpvs_membership_list as $plan) { ?>
                                    <label class="selectit"><input type="checkbox" value="<?php echo $plan['id']; ?>" <?php checked(in_array($plan['id'], $wpvs_site_lock_required_memberships)); ?>  name="wpvs_site_lock_required_memberships[]"><?php echo $plan['name']; ?></label><br/>
                                <?php
                                }
                            } else { ?>
                            <p>No memberships have been created. <a href="<?php echo admin_url('?page=rvs-memberships'); ?>">Create one here</a></p>
                            <?php } ?>
                            <p>Requried Membership(s) is an <em>optional setting</em> if you would like to require customers to purchase a membership to access videos and pages.</p>
                            <p><strong>Note: </strong> <em>We recommend you also apply your required membership(s) to all the videos you would like users to have access to.</em></p>
                        </div>
                    </div>
                    <div class="rvs-col-6">
                        <div class="wpvs-settings-box">
                            <h3>Redirection</h3>
                            <h4>Landing Page</h4>
                            <p>A page that non-logged in users can view.</p>
                            <select id="wpvs_landing_page" name="wpvs_landing_page">
                                <option value="login">Default Login Page</option>
                                <?php
                                    foreach($pages as $page) {
                                        echo '<option value="'.$page->ID.'"' . selected( $page->ID,  esc_attr( $wpvs_landing_page )) . ' >'.$page->post_title.'</option>';
                                    }
                                ?>
                            </select>
                            <p><em>The <strong>Register</strong> and <strong>Checkout</strong> pages are viewable by default to allow users to flow through the sign up / subscription process.</em></p>
                            <p class="description">You can also mark individual pages, videos and posts as <strong>Unlocked</strong> in order to make them viewable without login.</p>
                            <h4>Logged Out Redirect</h4>
                            <p>Where non-logged in users are redirected to login, subscribe or sign-up.</p>
                            <select id="wpvs_require_login_page" name="wpvs_require_login_page">
                                <option value="login">Default Login Page</option>
                                <?php
                                    foreach($pages as $page) {
                                        echo '<option value="'.$page->ID.'"' . selected( $page->ID,  esc_attr( $wpvs_require_login_page )) . ' >'.$page->post_title.'</option>';
                                    }
                                ?>
                            </select>
                            <p><em><strong>Tip:</strong> Set this to the <strong>Register</strong> or <strong>Create Account</strong> page to redirect new customers to subscribe or create an account.</em></p>
                            <h4>Logged In Redirect </h4>
                            <p>Where logged in users are redirected to subscribe or sign-up.</p>
                            <p><em>Only applicable if <strong>Required Membership(s)</strong> are set.</em></p>
                            <select name="wpvs_site_lock_redirect_logged_in_users_page">
                                <option value="login">Default Login Page</option>
                                <?php
                                    foreach($pages as $page) {
                                        echo '<option value="'.$page->ID.'"' . selected( $page->ID,  esc_attr( $wpvs_site_lock_redirect_logged_in_users_page )) . ' >'.$page->post_title.'</option>';
                                    }
                                ?>
                            </select>
                            <p><em><strong>Tip:</strong> Set this to your <strong>Register Page</strong> to redirect customers to subscribe.</em></p>
                        </div>
                    </div>
                    <div class="wpvs-save-settings-box">
                        <input type="submit" name="submit" class="button button-primary" value="Save Changes">
                    </div>
                </div>
                <h2>Colours</h2>
                <div id="wpvs-memberships-color-settings" class="rvs-container rvs-box">
                    <div class="rvs-col-6">
                        <div class="wpvs-settings-box">
                            <h3>Primary Colour</h3>
                            <input id="rvs_primary_color" name="rvs_primary_color" type="text" value="<?php echo get_option('rvs_primary_color', '#27ae60'); ?>" class="rvs-color-field" /><p>This will be color of buttons on any WP Video Memberships page.</p>
                        </div>
                        <div class="wpvs-settings-box">
                            <h3>Secondary Colour</h3>
                            <input id="rvs_delte_color" name="rvs_delete_color" type="text" value="<?php echo get_option('rvs_delete_color', '#c0392b'); ?>" class="rvs-color-field" /><p>This will be color of "Delete" or "Cancel" buttons on any WP Video Memberships page.</p>
                        </div>
                    </div>
                    <div class="rvs-col-6">
                        <div class="wpvs-settings-box">
                            <h3>Card Form Background</h3>
                            <input id="rvs_card_form_background" name="rvs_card_form_background" type="text" value="<?php echo get_option('rvs_card_form_background', '#ffffff'); ?>" class="rvs-color-field" /><p>This will be background color of the credit card form.</p>
                        </div>
                        <div class="wpvs-settings-box">
                            <h3>Card Form Input Colour</h3>
                            <input id="rvs_card_form_color" name="rvs_card_form_color" type="text" value="<?php echo get_option('rvs_card_form_color', '#444444'); ?>" class="rvs-color-field" /><p>This will be color of the credit card form input fields.</p>
                        </div>
                    </div>
                    <div class="wpvs-save-settings-box">
                        <input type="submit" name="submit" class="button button-primary" value="Save Changes">
                    </div>
                </div>
        </form>
        </div>
    </div>
</div>
<script>
jQuery(document).ready( function() {
    jQuery('.rvs-color-field').wpColorPicker();
});
</script>
