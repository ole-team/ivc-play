<?php $rvs_screen = $_GET['page']; global $wpvs_membership_update_required;?>
<label id="rvs-dropdown-menu" for="rvs-menu-checkbox"><span class="dashicons dashicons-menu"></span> Menu</label>
<div id="rvs-admin-menu" class="border-box">

    <?php if( ! get_option('is-wp-videos-multi-site')) { ?>
        <a href="<?php echo admin_url('admin.php?page=wpvs-activation'); ?>" title="Activate Website" class="rvs-tab <?=($rvs_screen == "wpvs-activation") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-star-filled"></span> Activate Website</a>
    <?php } ?>
    <a href="<?php echo admin_url('admin.php?page=vimeo-sync'); ?>" title="API Keys" class="rvs-tab <?=($rvs_screen == "vimeo-sync") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-admin-network"></span> API Keys</a>
    <a href="<?php echo admin_url('admin.php?page=wpvs-theme-video-settings'); ?>" title="API Keys" class="rvs-tab <?=($rvs_screen == "wpvs-theme-video-settings") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-admin-generic"></span> Video Settings</a>
    <a href="<?php echo admin_url('admin.php?page=wpvs-custom-player-settings'); ?>" title="Custom Player Settings" class="rvs-tab <?=($rvs_screen == "wpvs-custom-player-settings") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-editor-code"></span> Custom Player</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-settings'); ?>" title="Membership Settings" class="rvs-tab <?=($rvs_screen == "rvs-settings") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-admin-generic"></span> Membership Settings</a>
    <a href="<?php echo admin_url('admin.php?page=wpvs-checkout-settings'); ?>" title="Checkout Settings" class="rvs-tab <?=($rvs_screen == "wpvs-checkout-settings") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-migrate"></span> Checkout Settings</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-memberships'); ?>" title="Memberships" class="rvs-tab <?=($rvs_screen == "rvs-memberships") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-lock"></span> Memberships</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-members'); ?>" title="Members" class="rvs-tab <?=($rvs_screen == "rvs-members") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-groups"></span> Members</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-payment-settings&tab=stripe'); ?>" title="Payment Gateway" class="rvs-tab <?=($rvs_screen == "rvs-payment-settings" || $rvs_screen == "rvs-paypal-settings") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-store"></span> Payment Gateway</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-payments'); ?>" title="Payments" class="rvs-tab <?=($rvs_screen == "rvs-payments") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-clipboard"></span> Payments</a>
    <a href="<?php echo admin_url('admin.php?page=wpvs-tax-rate-settings'); ?>" title="Tax Rates" class="rvs-tab <?=($rvs_screen == "wpvs-tax-rate-settings") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-admin-site-alt3"></span> <?php _e('Tax Rates', 'vimeo-sync-memberships'); ?></a>
    <a href="<?php echo admin_url('admin.php?page=rvs-email-settings&tab=setup'); ?>" title="Email" class="rvs-tab <?=($rvs_screen == "rvs-email-settings") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-email-alt"></span> Email</a>
    <a href="<?php echo admin_url('admin.php?page=wpvs-reminder-settings&tab=renewal'); ?>" title="Reminders" class="rvs-tab <?=($rvs_screen == "wpvs-reminder-settings") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-clock"></span> Reminders</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-coupon-codes'); ?>" title="Coupons" class="rvs-tab <?=($rvs_screen == "rvs-coupon-codes") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-tickets-alt"></span> Coupons</a>
    <a href="<?php echo admin_url('admin.php?page=rvs-webhooks'); ?>" title="Webhooks" class="rvs-tab <?=($rvs_screen == "rvs-webhooks") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-info"></span> Webhooks</a>
    <a href="<?php echo admin_url('admin.php?page=wpvs-updates'); ?>" title="Run Updates" class="rvs-tab <?=($rvs_screen == "wpvs-updates") ? 'rvs-tab-active' : ''?>"><span class="dashicons dashicons-update"></span> Run Updates<?=($wpvs_membership_update_required) ? '<span class="dashicons dashicons-warning wpvs-update-needed rvs-error"></span>' : ''?></a>
</div>
