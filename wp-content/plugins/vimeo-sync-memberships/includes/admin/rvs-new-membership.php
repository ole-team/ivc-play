<?php global $rvs_currency; ?>
<div class="wrap">
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title">New Membership Plan</h1>
            <div class="rvs-edit-form">
                <div class="rvs-field-section">
                <label>Plan Id (Example: "Gold" - No Spaces or Special Characters)</label>
                <input type="text" name="id_of_plan" id="id_of_plan" required>
                </div>

                <div class="rvs-field-section">
                <label>Name of Plan</label>
                <input type="text" name="name_of_plan" id="name_of_plan" required>
                </div>

                <div class="rvs-field-section">
                <label>Price</label>
                <input placeholder="<?php echo $rvs_currency; ?>" type="number" step="any" name="price_of_plan" id="price_of_plan" required><br />
                </div>
                <div class="rvs-field-section">
                    <label>Description</label>
                    <textarea rows="5" cols="50" maxlength="500" name="description_of_plan" id="description_of_plan"></textarea>
                    <label>Short Description (127 character max)</label>
                    <textarea rows="3" cols="50" maxlength="127" name="short_description_of_plan" id="short_description_of_plan"></textarea>
                </div>

                <div class="rvs-field-section rvs-membership-interval">
                    <div class="rvs-side-section">
                        <label>Payment Interval</label>
                        <select id="interval_of_plan" name="interval_of_plan" class="rvs-admin-select">
                            <option value="day">Day</option>
                            <option value="week">Week</option>
                            <option value="month">Month</option>
                            <option value="year">Year</option>
                        </select>
                    </div>
                    <div class="rvs-side-section">
                        <label>Interval Count</label>
                        <input type="number" min="1" id="plan_interval_count" name="plan_interval_count" />
                        <label class="rvs-field-description">The number of intervals (specified in the Payment Interval) between each subscription billing.</label>
                        <label class="rvs-field-description">For example, <strong>Payment Interval = month</strong> and <strong>Interval Count = 3</strong> bills every 3 months.</label>
                    </div>
                </div>

                <div class="rvs-field-section">
                <label>Trial Period</label>
                <select id="trial_period" name="trial_period" class="rvs-admin-select">
                    <option value="none">None</option>
                    <option value="day">Days</option>
                    <option value="week">Weeks</option>
                    <option value="month">Months</option>
                </select>
                <input type="number" min="0" id="trial_period_days" name="trial_period_days" placeholder="Number of days" />
                </div>
                <div class="rvs-field-section">
                    <label class="rvs-label-inline">
                    <input type="checkbox" name="create_membership_role" id="create_membership_role" value="1" />
                    <?php _e('Create User Role', 'vimeo-sync-memberships') ; ?></label>
                    <p class="description"><?php _e('Creates a new user role that gets assigned to customers subscribed to this membership', 'vimeo-sync-memberships'); ?>.</p>
                </div>
                <div class="rvs-field-section">
                <label class="rvs-label-inline">
                <input type="checkbox" name="hide_membership" id="hide_membership" value="1" />
                    Hide this Membership</label>
                </div>

                <label id="new-membership" type="submit" class="rvs-button">Create Membership Plan</label>
            </div>
        </div>
    </div>
</div>
