<div class="wrap">
    <h2 class="wp-admin-notice-placement"></h2>
    <?php include('rvs-admin-menu.php'); ?>
    <div id="rvs-page-settings" class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title">Registration &amp; Checkout Settings</h1>
            <form method="post" action="options.php">
            <?php settings_fields( 'wpvs-checkout-settings' );

                // Billing Information Settings
                $wpvs_require_billing_information = get_option('wpvs_require_billing_information', 0);

                // Google reCAPTCHA Settings
                $wpvs_default_recaptcha = array(
                    'enabled'    => 0,
                    'site_key'   => "",
                    'secret_key' => "",
                    'version'    => "v2",
                    'theme'      => "dark",
                    'size'       => "normal"
                );
                $wpvs_google_recaptcha_settings = get_option('wpvs_google_recaptcha_settings', $wpvs_default_recaptcha);
                if( ! isset($wpvs_google_recaptcha_settings['enabled']) ) {
                    $wpvs_google_recaptcha_settings['enabled'] = 0;
                }

                // Agreement Checkbox Settings
                $wpvs_registration_agreement_settings = get_option('wpvs_registration_agreement_settings');

                if( ! isset($wpvs_registration_agreement_settings['enabled']) ) {
                    $wpvs_registration_agreement_settings['enabled'] = 0;
                }

                if( ! isset($wpvs_registration_agreement_settings['agreement_content']) ) {
                    $site_privacy_link = home_url('/privacy-policy');
                    if( function_exists('get_privacy_policy_url') ) {
                        $site_privacy_link = get_privacy_policy_url();
                    }
                    $wpvs_registration_agreement_settings['agreement_content'] = 'By checking the box, I agree to the <a href="'.$site_privacy_link.'" target="_blank">Terms &amp; Agreement</a>';
                }

            ?>
            <div class="rvs-container">
                <ul class="subsubsub">
                    <li><a href="#wpvs-registration-settings"><?php _e('Registration', 'vimeo-sync-memberships'); ?></a> | </li>
                    <li><a href="#wpvs-billing-settings"><?php _e('Checkout', 'vimeo-sync-memberships'); ?></a> | </li>
                </ul>
            </div>
            <h2>Registration</h2>
            <div id="wpvs-registration-settings" class="rvs-container rvs-box">
                <div class="rvs-col-6">
                    <div class="wpvs-settings-box">
                        <h3>Terms &amp; Agreement</h3>
                        <label><input type="checkbox" name="wpvs_registration_agreement_settings[enabled]" value="1" <?php checked(1, $wpvs_registration_agreement_settings['enabled']); ?> />
                        Show Terms &amp; Agreement Checkbox.</label>
                        <p><em>Show / Hide the Terms &amp; Agreement checkbox.</em></p>
                        <h4>Terms &amp; Agreement Text</h4>
                        <?php
                        wp_editor(
                            $wpvs_registration_agreement_settings['agreement_content'],
                            'wpvs_registration_agreement_content',
                            $settings = array(
                                'textarea_name' => 'wpvs_registration_agreement_settings[agreement_content]',
                                'media_buttons' => false,
                                'teeny' => true,
                                'wpautop' => true,
                                'textarea_rows' => 3
                            )
                        );
                        ?>
                        <p><em>This should be a short sentence that links to your Terms &amp; Agreements and / or Privacy Policy page.</em></p>
                    </div>
                </div>
                <div class="rvs-col-6">
                    <div class="wpvs-settings-box">
                        <h3>Google reCAPTCHA</h3>
                        <label><input type="checkbox" name="wpvs_google_recaptcha_settings[enabled]" value="1" <?php checked(1, $wpvs_google_recaptcha_settings['enabled']); ?> />
                        Enabled.</label>
                        <p><em>Add Google reCAPTCHA to registration form.</em></p>
                        <h4>reCAPTCHA Site Key</h4>
                        <input type="text" name="wpvs_google_recaptcha_settings[site_key]" value="<?php echo $wpvs_google_recaptcha_settings['site_key']; ?>" autocomplete="off"/>
                        <p><em>Add Google reCAPTCHA <a href="https://www.google.com/recaptcha/admin" target="_blank">Site Key</a>.</em></p>
                        <h4>reCAPTCHA Secret Key</h4>
                        <input type="password" name="wpvs_google_recaptcha_settings[secret_key]" value="<?php echo $wpvs_google_recaptcha_settings['secret_key']; ?>" autocomplete="off" />
                        <p><em>Add Google reCAPTCHA <a href="https://www.google.com/recaptcha/admin" target="_blank">Secret Key</a>.</em></p>
                        <h4>reCAPTCHA Version</h4>
                        <select name="wpvs_google_recaptcha_settings[version]">
                            <option value="v2" <?php selected('v2', $wpvs_google_recaptcha_settings['version']); ?>>v2 Checkbox</option>
                        </select>
                        <p><em>Choose Google reCAPTCHA version.</em></p>
                        <h4>reCAPTCHA Theme</h4>
                        <select name="wpvs_google_recaptcha_settings[theme]">
                            <option value="light" <?php selected('light', $wpvs_google_recaptcha_settings['theme']); ?>>Light</option>
                            <option value="dark" <?php selected('dark', $wpvs_google_recaptcha_settings['theme']); ?>>Dark</option>
                        </select>
                        <p><em>Choose Google reCAPTCHA theme.</em></p>
                        <h4>reCAPTCHA Size</h4>
                        <select name="wpvs_google_recaptcha_settings[size]">
                            <option value="normal" <?php selected('normal', $wpvs_google_recaptcha_settings['size']); ?>>Normal</option>
                            <option value="compact" <?php selected('compact', $wpvs_google_recaptcha_settings['size']); ?>>Compact</option>
                        </select>
                        <p><em>Choose Google reCAPTCHA theme.</em></p>
                    </div>
                </div>
            </div>
            <h2>Checkout</h2>
            <div id="wpvs-billing-settings" class="rvs-container rvs-box">
                <div class="rvs-col-6">
                    <div class="wpvs-settings-box">
                    <h3>Require Billing Information</h3>
                        <label><input type="checkbox" name="wpvs_require_billing_information" value="1" <?php checked(1, $wpvs_require_billing_information); ?> />
                        Require Billing Information during checkout.</label>
                        <p><em>Displays Address fields during checkout. Also displays Address fields on the Credit Card's Account tab.</em></p>
                    </div>
                </div>
            </div>
            <div>
                <?php submit_button(); ?>
            </div>
        </form>
        </div>
    </div>
</div>
