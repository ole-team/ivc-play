<?php

function wpvs_create_payment_tables() {
    global $wpdb;
    if( ! wpvs_payments_table_exists() ) {
        $table_name = $wpdb->prefix . 'rvs_payments';
        $test_table_name = $wpdb->prefix . 'rvs_test_payments';
        $charset_collate = $wpdb->get_charset_collate();

        $live_payment_tables = "CREATE TABLE $table_name (
          id mediumint(9) NOT NULL AUTO_INCREMENT,
          time int NOT NULL,
          gateway tinytext NOT NULL,
          userid int NOT NULL,
          customerid varchar(100) NOT NULL,
          amount int NOT NULL,
          paymentid varchar(100) NOT NULL UNIQUE,
          coupon_code text,
          type tinytext NOT NULL,
          productid int NOT NULL,
          refunded boolean,
          PRIMARY KEY  (id)
        ) $charset_collate;";

        $test_payment_tables = "CREATE TABLE $test_table_name (
          id mediumint(9) NOT NULL AUTO_INCREMENT,
          time int NOT NULL,
          gateway tinytext NOT NULL,
          userid int NOT NULL,
          customerid varchar(100) NOT NULL,
          amount int NOT NULL,
          paymentid varchar(100) NOT NULL UNIQUE,
          coupon_code text,
          type tinytext NOT NULL,
          productid int NOT NULL,
          refunded boolean,
          PRIMARY KEY  (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $live_payment_tables );
        dbDelta( $test_payment_tables );
    }
}

function wpvs_payments_table_exists() {
    global $wpdb;
    $payments_table_exists = true;
    $wpvs_payments_table_name = $wpdb->prefix . 'rvs_payments';
    if($wpdb->get_var("SHOW TABLES LIKE '$wpvs_payments_table_name'") != $wpvs_payments_table_name) {
         $payments_table_exists = false;
    }
    return $payments_table_exists;
}

function wpvs_create_members_tables() {
    global $wpdb;
    if( ! wpvs_members_table_exists() ) {
        $members_table_name = $wpdb->prefix . 'wpvs_members';
        $test_members_table_name = $wpdb->prefix . 'wpvs_test_members';
        $charset_collate = $wpdb->get_charset_collate();

        $live_members_tables = "CREATE TABLE $members_table_name (
          id int NOT NULL AUTO_INCREMENT,
          user_id int NOT NULL,
          PRIMARY KEY  (id),
          UNIQUE (user_id)
        ) $charset_collate;";

        $test_members_tables = "CREATE TABLE $test_members_table_name (
          id int NOT NULL AUTO_INCREMENT,
          user_id int NOT NULL,
          PRIMARY KEY  (id),
          UNIQUE (user_id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $live_members_tables );
        dbDelta( $test_members_tables );
    }
}

function wpvs_members_table_exists() {
    global $wpdb;
    $members_table_exists = true;
    $wpvs_members_table_name = $wpdb->prefix . 'wpvs_members';
    if($wpdb->get_var("SHOW TABLES LIKE '$wpvs_members_table_name'") != $wpvs_members_table_name) {
         $members_table_exists = false;
    }
    return $members_table_exists;
}

function rvs_get_admin_payments($offset, $gateways, $coupon_id) {
    global $wpdb;
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $table_name = $wpdb->prefix . 'rvs_test_payments';
    } else {
        $table_name = $wpdb->prefix . 'rvs_payments';
    }
    $gateway_filter = false;

    $db_query = "SELECT * FROM $table_name";

    if( ! empty($gateways) ) {
        $db_query .= " WHERE gateway = '$gateways'";
        $gateway_filter = true;
    }

    if( ! empty($coupon_id) ) {
        if( $gateway_filter ) {
            $db_query .= " AND coupon_code = '$coupon_id'";
        } else {
            $db_query .= " WHERE coupon_code = '$coupon_id'";
        }
    }

    $db_query .= " ORDER BY time DESC LIMIT 50 OFFSET $offset";

    $payments = $wpdb->get_results($db_query);

    return $payments;
}

function wpvs_do_db_refund_payment($charge_id) {
    global $wpdb;
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $table_name = $wpdb->prefix . 'rvs_test_payments';
    } else {
        $table_name = $wpdb->prefix . 'rvs_payments';
    }
    $update_payment = array('refunded' => '1');
    $find_payment = array('paymentid' => $charge_id);
    $refund = $wpdb->update($table_name, $update_payment, $find_payment);
    return $refund;
}

function wpvs_get_timeframe_admin_payments($timeframe, $current_time) {
    global $wpdb;
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $table_name = $wpdb->prefix . 'rvs_test_payments';
    } else {
        $table_name = $wpdb->prefix . 'rvs_payments';
    }
    $payments = $wpdb->get_results("SELECT amount FROM $table_name WHERE time <= '$current_time' AND time >= '$timeframe'");
    return $payments;
}
