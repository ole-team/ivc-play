<?php
global $rvs_paypal_enabled;
global $wpvs_stripe_gateway_enabled;
if($rvs_paypal_enabled) {
    $paypal_error_message = "";
    $active_webhooks = array(
        'salecomplete' => false,
        'subscriptioncancelled' => false,
        'subscriptionsuspended' => false,
        'subscriptionreactivated' => false,
        'subscriptionupdated' => false
    );
    $missing_webhooks = 0;
    $website_url = get_bloginfo('url');

    if(stripos($website_url, 'https') === false) {
        $paypal_error_message .= '<li>PayPal requires an SSL (HTTPS) URL for Webhooks. <a href="'.admin_url('options-general.php').'">Update Site Address (URL)</a> - <em>Make sure you have an SSL for your website before updating this</em></li>';
    }
    require_once(RVS_MEMBERS_BASE_DIR . '/paypal/autoload.php');
    $paypal_config = rvs_create_paypal_config();

    if($paypal_config['api']['mode'] == 'live') {
        $get_webhook_id = 'rvs-paypal-webhook-id';
    } else {
        $get_webhook_id = 'rvs-paypal-webhook-id-test';
    }
    $paypal_webhook_id = get_option($get_webhook_id);
    if(empty($paypal_error_message)) {
        if($paypal_config['client'] == "" || $paypal_config['secret'] == "") {
            $paypal_error_message .= __('You have PayPal enabled. However, you have not entered your Client ID or Client Secret key.', 'vimeo-sync-memberships');
        } else {
            if(!empty($paypal_webhook_id)) {
                $apiContext = new \PayPal\Rest\ApiContext(
                    new \PayPal\Auth\OAuthTokenCredential(
                        $paypal_config['client'],  // ClientID
                        $paypal_config['secret']  // ClientSecret
                    )
                );
                $apiContext->setConfig($paypal_config['api']);
                try {
                    $paypal_webhook = \PayPal\Api\Webhook::get($paypal_webhook_id, $apiContext);
                    if(!empty($paypal_webhook)) {
                        $event_types = $paypal_webhook->event_types;
                        if(!empty($event_types)) {
                            foreach($event_types as $check_event) {
                                if($check_event->name == "PAYMENT.SALE.COMPLETED") {
                                    $active_webhooks['salecomplete'] = true;
                                    $missing_webhooks++;
                                }
                                if($check_event->name == "BILLING.SUBSCRIPTION.CANCELLED") {
                                    $active_webhooks['subscriptioncancelled'] = true;
                                    $missing_webhooks++;
                                }
                                if($check_event->name == "BILLING.SUBSCRIPTION.SUSPENDED") {
                                    $active_webhooks['subscriptionsuspended'] = true;
                                    $missing_webhooks++;
                                }
                                if($check_event->name == "BILLING.SUBSCRIPTION.RE-ACTIVATED") {
                                    $active_webhooks['subscriptionreactivated'] = true;
                                    $missing_webhooks++;
                                }

                                if($check_event->name == "BILLING.SUBSCRIPTION.UPDATED") {
                                    $active_webhooks['subscriptionupdated'] = true;
                                    $missing_webhooks++;
                                }
                            }
                        }
                    }

                } catch (Exception $ex) {
                    $error_message = $ex->getMessage();
                    $paypal_error_message .= $error_message;
                }
            }
        }
    }
} else {
    $paypal_error_message = __('PayPal is disabled', 'vimeo-sync-memberships');
}
?>
<div class="wrap">
    <h2 class="wp-admin-notice-placement"></h2>
    <?php include('rvs-admin-menu.php'); ?>
        <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title"><?php _e('Webhooks', 'vimeo-sync-memberships'); ?></h1>
            <?php if($wpvs_stripe_gateway_enabled) {
                global $wpvs_stripe_options;
            ?>
                <div class="wpvs-admin-webhook-overview rvs-border-box">
                    <h3><?php _e('Stripe Webhooks', 'vimeo-sync-memberships'); ?></h3>
                    <?php if( ! isset($wpvs_stripe_options['webhook_id']) || empty($wpvs_stripe_options['webhook_id']) ) { ?>
                        <div class="rvs-membership-gateway">
                            <label class="rvs-gateway-title">You have not created your Live Stripe Webhook Endpoints.</label>
                            <div class="rvs-right-buttons">
                                <label class="rvs-active-webhook"><span class="dashicons dashicons-warning rvs-error"></span></label>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="rvs-membership-gateway">
                            <div class="rvs-gateway-title">Live Stripe Webhook Endpoints created!</div>
                            <div class="rvs-right-buttons">
                                <label class="rvs-active-webhook"><span class="dashicons dashicons-yes"></span></label>
                            </div>
                        </div>
                    <?php } if( ! isset($wpvs_stripe_options['test_webhook_id']) || empty($wpvs_stripe_options['test_webhook_id']) ) { ?>
                        <div class="rvs-membership-gateway">
                            <label class="rvs-gateway-title">You have not created your Test Stripe Webhook Endpoints.</label>
                            <div class="rvs-right-buttons">
                                <label class="rvs-active-webhook"><span class="dashicons dashicons-warning rvs-error"></span></label>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="rvs-membership-gateway">
                            <div class="rvs-gateway-title">Test Stripe Webhook Endpoints created!</div>
                            <div class="rvs-right-buttons">
                                <label class="rvs-active-webhook"><span class="dashicons dashicons-yes"></span></label>
                            </div>
                        </div>
                    <?php } ?>
                    <p class="description">You should see the <strong><?php echo get_bloginfo('url').'/?wpvs-stripe-event-listener=stripe'; ?></strong> endpoint for both <strong>Live</strong> and <strong>Test</strong> mode in your <a href="https://dashboard.stripe.com/account/webhooks" target="_blank">Stripe dashboard</a>.</p>

                    <?php if( ( isset($wpvs_stripe_options['webhook_id']) && ! empty($wpvs_stripe_options['webhook_id']) ) && ( isset($wpvs_stripe_options['test_webhook_id']) && ! empty($wpvs_stripe_options['test_webhook_id']) ) ) { ?>
                        <label id="wpvs-admin-create-stripe-endpoints" class="wpvs-blue-text"><?php _e('Reset Stripe Webhook Endpoints', 'vimeo-sync-memberships'); ?></label>
                    <?php } else { ?>
                        <button id="wpvs-admin-create-stripe-endpoints" class="rvs-button"><?php _e('Create Stripe Webhook Endpoints', 'vimeo-sync-memberships'); ?></button>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="wpvs-admin-webhook-overview rvs-border-box">
                <h3><?php _e('PayPal Webhooks', 'vimeo-sync-memberships'); ?></h3>
                <?php if(!empty($paypal_error_message)) :
                    echo '<ul>'.$paypal_error_message.'</ul>';
                else : if($missing_webhooks < 4) : ?>
                        <div class="rvs-info-box"><p><span class="dashicons dashicons-warning rvs-error"></span> Indicates missing webhook in your <a href="https://developer.paypal.com/developer/applications" target="_blank">Developer Dashboard</a>.</p></div>
                    <?php endif; ?>

                    <!-- SALE COMPLETE WEBHOOK -->
                    <div class="rvs-membership-gateway">
                        <label class="rvs-gateway-title">Sale Complete</label>
                        <div class="rvs-right-buttons">
                        <?php
                            if($active_webhooks['salecomplete']) {
                                echo '<label class="rvs-active-webhook"><span class="dashicons dashicons-yes"></span></label>';
                            } else {
                                echo '<label class="rvs-active-webhook"><span class="dashicons dashicons-warning"></span></label>';
                            } ?>
                        </div>
                    </div>
                    <!-- SUBSCRIPTION CANCELLED WEBHOOK -->
                    <div class="rvs-membership-gateway">
                        <label class="rvs-gateway-title">Subscription Cancelled</label>
                        <div class="rvs-right-buttons">
                        <?php
                            if($active_webhooks['subscriptioncancelled']) {
                                echo '<label class="rvs-active-webhook"><span class="dashicons dashicons-yes"></span></label>';
                            } else {
                                echo '<label class="rvs-active-webhook"><span class="dashicons dashicons-warning"></span></label>';
                            } ?>
                        </div>
                    </div>
                    <!-- SUBSCRIPTION CANCELLED SUSPENDED -->
                    <div class="rvs-membership-gateway">
                        <label class="rvs-gateway-title">Subscription Suspended</label>
                        <div class="rvs-right-buttons">
                        <?php
                            if($active_webhooks['subscriptionsuspended']) {
                                echo '<label class="rvs-active-webhook"><span class="dashicons dashicons-yes"></span></label>';
                            } else {
                                echo '<label class="rvs-active-webhook"><span class="dashicons dashicons-warning"></span></label>';
                            } ?>
                        </div>
                    </div>

                    <!-- SUBSCRIPTION RE-ACTIVATED WEBHOOK -->
                    <div class="rvs-membership-gateway">
                        <label class="rvs-gateway-title">Subscription Re-Activated</label>
                        <div class="rvs-right-buttons">
                        <?php
                            if($active_webhooks['subscriptionreactivated']) {
                                echo '<label class="rvs-active-webhook"><span class="dashicons dashicons-yes"></span></label>';
                            } else {
                                echo '<label class="rvs-active-webhook"><span class="dashicons dashicons-warning"></span></label>';
                            } ?>
                        </div>
                    </div>

                    <!-- SUBSCRIPTION UPDATED WEBHOOK -->
                    <div class="rvs-membership-gateway">
                        <label class="rvs-gateway-title">Subscription Updated</label>
                        <div class="rvs-right-buttons">
                        <?php
                            if($active_webhooks['subscriptionupdated']) {
                                echo '<label class="rvs-active-webhook"><span class="dashicons dashicons-yes"></span></label>';
                            } else {
                                echo '<label class="rvs-active-webhook"><span class="dashicons dashicons-info"></span></label>';
                            } ?>
                        </div>
                    </div>

                <div id="rvs-paypal-webhooks">
                    <?php if($missing_webhooks < 5) : ?>
                        <div class="rvs-info-box"><p><span class="dashicons dashicons-warning rvs-error"></span> Indicates missing webhook in your <a href="https://developer.paypal.com/developer/applications" target="_blank">Developer Dashboard</a>.</p></div>
                    <?php endif; ?>
                    <label id="create-paypal-webhooks" class="rvs-button">Create PayPal Webhooks</label><br>
                    <p>You can also manually create webhooks for PayPal in your <a href="https://developer.paypal.com/developer/applications/" target="_blank">PayPal developer dashboard</a>.</p>
                    <p><a href="https://www.wpvideosubscriptions.com/how-to/wordpress-video-memberships/paypal-payments-setup/" target="_blank">How To Setup PayPal Webhooks</a></p>
                    <input class="regular-text" type="text" value="<?php echo get_bloginfo('url').'/?rvs-paypal-listener=paypal'; ?>" readonly onClick="this.select();">
                </div>
                <?php endif; ?>
                <label id="wpvs-reset-paypal-webhooks" class="wpvs-blue-text"><?php _e('Reset PayPal Webhooks', 'vimeo-sync-memberships'); ?></label>
            </div>
        </div>
    </div>
</div>
