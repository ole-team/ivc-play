<?php
global $rvs_live_mode;
global $wpvs_currency_label;
global $wpvs_stripe_gateway_enabled;
global $rvs_paypal_enabled;
global $wpvs_coingate_enabled;
global $wpvs_coinbase_enabled;
$coingate_payments_url = 'https://coingate.com/account/orders';
if( $wpvs_currency_label == "CAD" || $wpvs_currency_label == "USD") {
    $wpvs_currency_label = '$';
}
$found_payments_count = 0;
$rvs_payments = rvs_get_admin_payments(0, null, null);
$paged = false;
if(!empty($rvs_payments) && count($rvs_payments) >= 50) {
    $paged = true;
}
$payments_title = __('Payments', 'vimeo-sync-memberships');
$import_stripe_payments_text = __('Import Stripe Payments', 'vimeo-sync-memberships');
if($rvs_live_mode == "off") {
    $payments_title = __('TEST Payments', 'vimeo-sync-memberships');
    $import_stripe_payments_text = __('Import Stripe TEST Payments', 'vimeo-sync-memberships');
    $coingate_payments_url = 'https://sandbox.coingate.com/account/orders';
}
$coupon_codes = get_option('rvs_coupon_codes');
?>
<div class="wrap">
    <h2 class="wp-admin-notice-placement"></h2>
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title"><?php echo $payments_title; ?></h1>
            <h3 class="wpvs-admin-section-title"><?php _e('Overview', 'vimeo-sync-memberships'); ?></h3>
            <div class="wpvs-admin-overview">
                <div class="wpvs-admin-overview-block rvs-border-box">
                    <label class="wpvs-overiew-time"><?php _e('Last 7 Days', 'vimeo-sync-memberships'); ?></label>
                    <label class="wpvs-retrieving-payment-data"><span class="dashicons dashicons-update wpvs-update-spin"></span> <?php _e('Retrieving data...', 'vimeo-sync-memberships'); ?></label>
                    <label class="wpvs-admin-amount-made"><?php echo $wpvs_currency_label; ?> <span id="wpvs-last-week-payments"></span></label>
                </div>
                <div class="wpvs-admin-overview-block rvs-border-box">
                    <label class="wpvs-overiew-time"><?php _e('This Month', 'vimeo-sync-memberships'); ?></label>
                    <label class="wpvs-retrieving-payment-data"><span class="dashicons dashicons-update wpvs-update-spin"></span> <?php _e('Retrieving data...', 'vimeo-sync-memberships'); ?></label>
                    <label class="wpvs-admin-amount-made"><?php echo $wpvs_currency_label; ?> <span id="wpvs-this-month-payments"></span></label>
                </div>
                <div class="wpvs-admin-overview-block rvs-border-box">
                    <label class="wpvs-overiew-time"><?php _e('This Year', 'vimeo-sync-memberships'); ?></label>
                    <label class="wpvs-retrieving-payment-data"><span class="dashicons dashicons-update wpvs-update-spin"></span> <?php _e('Retrieving data...', 'vimeo-sync-memberships'); ?></label>
                    <label class="wpvs-admin-amount-made"><?php echo $wpvs_currency_label; ?> <span id="wpvs-this-year-payments"></span></label>
                </div>
            </div>

            <?php if($wpvs_stripe_gateway_enabled ) { ?>
            <h3 class="wpvs-admin-section-title"><?php _e('Import Payments', 'vimeo-sync-memberships'); ?></h3>
            <div class="wpvs-admin-inline-button">
                <label id="import-stripe-payments" class="wpvs-admin-inline-button"><span class="dashicons dashicons-update"></span> <?php echo $import_stripe_payments_text; ?></label>
            </div>
            <?php  } ?>
            <h3 class="wpvs-admin-section-title"><?php _e('Payments', 'vimeo-sync-memberships'); ?></h3>
            <?php if(!empty($rvs_payments)) : ?>
            <div class="wpvs-filter-options rvs-border-box">
                <label class="wpvs-filter-label"><?php _e('Filter', 'vimeo-sync-memberships'); ?><span class="dashicons dashicons-filter"></span></label>
                <div class="wpvs-filter-setting">
                    <label><input type="checkbox" class="wpvs-get-gateway-payments" name="wpvs-load-stripe-payments" value="stripe" checked><?php _e('Stripe Payments', 'vimeo-sync-memberships'); ?></label>
                </div>
                <div class="wpvs-filter-setting">
                    <label><input type="checkbox" class="wpvs-get-gateway-payments" name="wpvs-load-paypal-payments" value="paypal" checked><?php _e('PayPal Payments', 'vimeo-sync-memberships'); ?></label>
                </div>
                <div class="wpvs-filter-setting">
                    <select id="wpvs-filter-coupon-payments">
                        <option value="">--<?php _e('Select Coupon', 'vimeo-sync-memberships'); ?>--</option>
                        <?php if( ! empty($coupon_codes) ) {
                            foreach($coupon_codes as $coupon) {
                                echo '<option value="'.$coupon['id'].'">'.$coupon['name'].'</option>';
                            }
                        } ?>
                    </select>
                </div>
                <?php if($wpvs_coinbase_enabled) { ?>
                <div class="wpvs-filter-setting">
                    <a href="https://commerce.coinbase.com/dashboard/payments" target="_blank"><?php _e('Coinbase Payments', 'vimeo-sync-memberships'); ?></a>
                </div>
                <?php } if($wpvs_coingate_enabled) { ?>
                <div class="wpvs-filter-setting">
                    <a href="<?php echo $coingate_payments_url; ?>" target="_blank"><?php _e('Coingate Payments', 'vimeo-sync-memberships'); ?></a>
                </div>
                <?php } ?>
            </div>
            <?php if($paged) : ?>

            <div class="rvs-navigation">
            <div class="border-box rvs-previous-payments rvs-previous-button rvs-nav-buttons">
                <label class="rvs-button rvs-button-blue rvs-back-payments">&#60; Previous Page</label>
            </div>
            <div class="border-box text-align-right rvs-next-payments rvs-next-button rvs-nav-buttons">
                <label class="rvs-button rvs-button-blue rvs-more-payments">Next Page &#62;</label>
            </div>
            </div>
            <?php endif; ?>
            <table id="payments" class="rvs_memberships">
                <tbody>
                    <tr>
                        <th>User</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Coupon</th>
                        <th>Gateway</th>
                        <th>Refund</th>
                    </tr>
                    <?php foreach($rvs_payments as $payment) :
                        $user = get_user_by('id', $payment->userid);
                        if( empty($user) ) {
                            $user_edit = '<em>(user does not exist)</em>';
                        } else {
                            $user_edit = '<a href="'.admin_url('admin.php?page=rvs-edit-member&id='.$user->ID).'">'.$user->user_login.'</a>';
                        }
                        $amount = number_format($payment->amount/100,2);
                        $date = date( 'M d, Y', $payment->time );
                        $refund_label = 'stripe-refund';
                        if($payment->gateway == "paypal") {
                             $refund_label = 'paypal-refund';
                        }

                        $issue_refund = '<label class="issue-refund '.$refund_label.'" id="'.$payment->paymentid.'">Refund</label>';

                        if($payment->refunded) {
                            $issue_refund = '<label id="'.$payment->paymentid.'"><em>Refunded</em></label>';
                        }
                    ?>
                    <tr class="rvs-payment-data">
                        <td><?php echo $user_edit; ?></td>
                        <td><?php echo $date; ?></td>
                        <td><?php echo $amount; ?></td>
                        <td><?php echo $payment->coupon_code; ?></td>
                        <td><?php echo $payment->gateway; ?></td>
                        <td><?php echo $issue_refund; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php if($paged) : ?>
            <div class="rvs-navigation">
            <div class="border-box rvs-previous-payments rvs-previous-button rvs-nav-buttons">
                <label class="rvs-button rvs-button-blue rvs-back-payments">&#60; Previous Page</label>
            </div>
            <div class="border-box text-align-right rvs-next-payments rvs-next-button rvs-nav-buttons">
                <label class="rvs-button rvs-button-blue rvs-more-payments">Next Page &#62;</label>
            </div>
            </div>
        <?php endif; ?>
        <?php else : ?>
            <p>No payments yet. Make sure that you have added your Webhooks for the Payment gateway(s) you're using.</p>
            <p><a href="<?php echo admin_url('admin.php?page=rvs-webhooks'); ?>">Setup Webhooks</a></p>
            <p><em><strong>Note: If you were using WP Video Subscriptions prior to version 2.4.2, you can view previous Stripe payments on individual <a href="<?php echo admin_url('admin.php?page=rvs-members'); ?>">Member</a> pages.</strong></em></p>
        <?php endif; ?>
        </div>
    </div>
</div>
