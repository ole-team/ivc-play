<?php
global $rvs_currency;
$coupon_codes = get_option('rvs_coupon_codes');
?>
<div class="wrap">
    <h2 class="wp-admin-notice-placement"></h2>
    <?php include('rvs-admin-menu.php'); ?>
    <div class="vimeosync">
        <div class="rvsPadding">
            <h1 class="rvs-title"><?php _e('Coupon Codes', 'vimeo-sync-memberships'); ?></h1>
            <a class="rvs-create-button rvs-button" href="admin.php?page=rvs-new-coupon"><?php _e('Add Coupon Code', 'vimeo-sync-memberships'); ?></a>
            <table id="coupon-codes" class="rvs_memberships">
                <tbody>
                    <tr>
                        <th><?php _e('Code', 'vimeo-sync-memberships'); ?></th>
                        <th><?php _e('Name', 'vimeo-sync-memberships'); ?></th>
                        <th><?php _e('Discount', 'vimeo-sync-memberships'); ?></th>
                        <th><?php _e('Usage', 'vimeo-sync-memberships'); ?></th>
                        <th><?php _e('Max Uses', 'vimeo-sync-memberships'); ?></th>
                        <th><?php _e('Uses / customer', 'vimeo-sync-memberships'); ?></th>
                        <th><?php _e('Manage', 'vimeo-sync-memberships'); ?></th>
                    </tr>
            <?php
                if(!empty($coupon_codes)) {
                    foreach($coupon_codes as $coupon) {
                        if($coupon['type'] == "amount") {
                            $amount_off = $rvs_currency . ' '. number_format($coupon['amount']/100, 2);
                        } else {
                            $amount_off = $coupon['amount'] . '%';
                        }
                ?>
                    <tr>
                    <td><?php echo $coupon['id']; ?></td>
                    <td><?php echo $coupon['name']; ?></td>
                    <td><?php echo $amount_off ?></td>
                    <td><?php
                        if($coupon['duration'] == 'repeating') {
                            $coupon_duration_text = __('repeats for').' '.$coupon['month_duration'].' '.__('months');
                        } else {
                            $coupon_duration_text = $coupon['duration'];
                        }
                        echo $coupon_duration_text; ?>
                    </td>
                    <td><?php
                        if(isset($coupon['used_count']) && ! empty($coupon['used_count']) ) {
                            $coupon_max_uses_text = $coupon['used_count'];
                        } else {
                            $coupon_max_uses_text = 0;
                        }
                        $coupon_max_uses_text .= '/';
                        if(isset($coupon['max_uses']) && ! empty($coupon['max_uses']) ) {
                            $coupon_max_uses_text .= $coupon['max_uses'];
                        } else {
                            $coupon_max_uses_text .= '&#8734;';
                        }
                        echo $coupon_max_uses_text; ?>
                    </td>
                    <td><?php
                        if(isset($coupon['max_uses_customer']) && ! empty($coupon['max_uses_customer']) ) {
                            $coupon_max_uses_customer_text = $coupon['max_uses_customer'];
                        } else {
                            $coupon_max_uses_customer_text = '&#8734;';
                        }
                        echo $coupon_max_uses_customer_text; ?>
                    </td>
                    <td><label class="remove-coupon-code" data-id="<?php echo $coupon['id']; ?>"><?php _e('Delete', 'vimeo-sync-memberships'); ?></label></td>
                    </tr>
                <?php } ?>
                <?php } ?>
        </tbody>
    </table>
    </div>
</div>

</div>
