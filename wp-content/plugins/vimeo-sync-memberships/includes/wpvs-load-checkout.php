<?php

function wpvs_memberships_set_load_checkout_scripts() {
    global $wp_query;
    global $post;
    global $wpvs_include_checkout_scripts;
    $wpvs_checkout_page = esc_attr( get_option('rvs_payment_page'));
    $wpvs_account_page = esc_attr( get_option('rvs_account_page'));
    if( $post && get_post_type() == "rvs_video") {
        $check_for_single_payment = get_post_meta( $post->ID, '_rvs_onetime_price', true );
        $rental_price = get_post_meta( $post->ID, 'rvs_rental_price', true );
        if( ! empty($check_for_single_payment) || ! empty($rental_price) ) {
            $wpvs_include_checkout_scripts = true;
        }
    }
    
    if( is_tax( 'rvs_video_category' ) ) {
        $wpvs_current_term = get_term($wp_query->get_queried_object_id(), 'rvs_video_category' );
        if( ! empty($wpvs_current_term) && ! is_wp_error($wpvs_current_term) ) {
            $wpvs_term_purchase_price = get_term_meta($wpvs_current_term->term_id, 'wpvs_category_purchase_price', true);
            if( ! empty($wpvs_term_purchase_price) ) {
                $wpvs_include_checkout_scripts = true;
            }
        }
    }
    
    if( is_page($wpvs_checkout_page) || is_page($wpvs_account_page) ) {
        $wpvs_include_checkout_scripts = true;
    }
}

add_action('wp', 'wpvs_memberships_set_load_checkout_scripts');