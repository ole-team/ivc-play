=== WP Video Memberships ===
Contributors: roguewebdesign
Tags: memberships, subscriptions, video, stripe, ecommerce, restrict video access, paypal, cryptocurrency
Requires at least: 4.0
Tested up to: 5.4.2
Stable tag: 5.0.3
License: GPLv2 or later

A membership add-on for the WP Videos plugin. This add-on allows you to restrict video access to members only.

== Description ==

A membership add-on for the WP Videos plugin. This add-on allows you to restrict video access to members only.

= FEATURES =

* Create memberships (day, week, month, year)
* Purchase and rental payment options
* Restrict video access to members only
* Coupon Codes
* Stripe, PayPal, CoinGate and Coinbase payments

== Installation ==

1. Upload the vimeo-sync-memberships folder to the `/wp-content/plugins` directory, or install the plugin through the WordPress plugins screen directly. You can also download the the vimeo-sync-memberships.zip file at https://www.wpvideosubscriptions.com.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Navgiate to WP Videos -> Membership Settings within your WordPress admin area.
4. Create memberships for Stripe, PayPal Coingate and Coinbase.
5. Apply the membership(s) to your videos to restrict access.


== Changelog ==

= 4.7.8 =

<ul>
<li>Czech Crown (CZK) currency added.</li>
<li>Translation file updates</li>
</ul>

= 4.7.7 =

<ul>
<li>Polyfill added for getallheaders() PHP function</li>
</ul>

= 4.7.6 =

<ul>
<li>Removed deprecated Vimeo thumbnail urls and css file from Rental and Purchase video thumbnails</li>
</ul>

= 4.7.5 =

<ul>
<li>Update request 4.7.5 check</li>
</ul>

= 4.7.4 =

<ul>
<li>Vimeo Download Link Request fixes and updates</li>
</ul>

= 4.7.3 =

<ul>
<li>Trial Frequency isset check for memberships checkout class.</li>
<li>Removed deprecated Import Videos menu item in admin dashboard.</li>
</ul>

= 4.7.2 =

<ul>
<li>PayPal button loading fix for Video Category Purchases</li>
</ul>

= 4.7.1 =

<ul>
<li>PayPal Trial Period bug fix where trial periods were being added twice on default Billing Plan subscriptions.</li>
<li>date_i18n updated to wp_date() function</li>
</ul>

= 4.7.0 =

<ul>
<li>Admin CSS updates for Bulk Editing video fields</li>
</ul>

= 4.6.9 =

<ul>
<li>Number format fix for PayPal Billing Agreement price.</li>
</ul>

= 4.6.8 =

<ul>
<li>New JSON data key <strong>wpvs_access_options</strong> to display assigned access options for each Video.</li>
<li><strong>Restriction User Logins:</strong> New settings found under <strong>WP Videos -> Membership Settings -> Account Settings -> Login Restrictions</strong>.</li>
</ul>


= 4.6.7 =

<ul>
<li>PayPal bug fix where one-time use coupon codes were not applied to Billing Agreements with Trial Periods</li>
<li>Tax Rates now applied to discount coupon code billing agreements.</li>
</ul>

= 4.6.6 =

<ul>
<li>Tax Rate fix for Jurisdictions with apostrophes.</li>
<li>Membership ship Status drop down appearance update to indicate select box.</li>
</ul>

= 4.6.5 =

<ul>
<li>Stripe subscription admin sync updates</li>
</ul>

= 4.6.4 =

<ul>
<li>Update Stripe customer id on checkout session completed webhook event</li>
</ul>

= 4.6.3 =

<ul>
<li>Stripe Webhook ID and Secret fix when resaving Stripe Payment Gateway settings.</li>
</ul>

= 4.6.2 =

<ul>
<li>Stripe Checkout Sessions: Set customer id fix and clean completed checkout sessions from db.</li>
</ul>

= 4.6.1 =

<ul>
<li>Stripe Webhook bug fix for Checkout Session Complete</li>
</ul>

= 4.6.0 =

<ul>
<li>Tax Rate db tables creation on plugin activation.</li>
</ul>

= 4.5.9 =

<ul>
<li>Stripe Tax Rate API creation fix</li>
<li>Translation file updates</li>
</ul>

= 4.5.8 =

<ul>
<li>Inclusive Tax Rate Calculating Updates</li>
</ul>

= 4.5.7 =

<ul>
<li>WPVS Checkout button load fix</li>
</ul>

= 4.5.6 =

<ul>
<li>WPVS Checkout class updates and process</li>
<li>WPVS Checkout improvements for calculating Coupons, Tax Rates and Totals.</li>
</ul>

= 4.5.5 =

<ul><li>Coupon Code &amp; Checkout calculating subtotal bug fix.</li></ul>

= 4.5.3 =

<ul><li>Tax Rates: Create your Tax Rates under <strong>WP Videos -> Tax Rates</strong>. Note: WP Videos -> Checkout Settings -> Checkout -> Require Billing Information must be Enabled for Tax Rates to function.</li></ul>

= 4.4.6 =

<ul>
    <li>Stripe Payments and Invoices fix on Account page for customers.</li>
</ul>

= 4.4.5 =

<ul>
    <li>Tested up to WordPress 5.3.2 readme file update.</li>
</ul>

= 4.4.4 =

<ul>
    <li>Filter users by Role and Without Membership on the Bulk Edit Users admin page.</li>
</ul>

= 4.4.3 =

<ul>
    <li>New shortcode <strong>[wpvs-restricted-content]</strong> ...restricted content here... <strong>[/wpvs-restricted-content]</strong> for restricting Page, Post and other post type content to members only: <a href="https://docs.wpvideosubscriptions.com/wp-video-memberships/shortcodes/" target="_blank">See Instructions</a></li>
</ul>

= 4.4.2 =

<ul>
    <li>Fixed Coinbase subscription bug when user memberships were empty.</li>
</ul>

= 4.4.1 =

<ul>
    <li>Fixed a bug where PayPal agreements with a Day interval would return a status of Pending and expired payment date.</li>
</ul>

= 4.4.0 =

<ul>
    <li>Fixed a bug where duplicate Subscribe options would display if both the Video and Category had the same Membership assigned.</li>
</ul>

= 4.3.9 =

<ul>
    <li>Translation file updates</li>
</ul>

= 4.3.8 =

<ul>
    <li>Number format fix for creating PayPal plans (removed comma from 4+ digit values)</li>
</ul>

= 4.3.7 =

<ul>
    <li>New Stripe Checkout page (optional). Enable under <strong>WP Videos -> Payment Gateway -> Stripe</strong></li>
    <li><strong>IMPORTANT:</strong> the /includes/term-payment-form.php file has been combined with 1 file /includes/single-payment-form.php.</li>
    <li><strong>IMPORTANT:</strong> /include-stripe/single-payment-form.php has been moved to /includes/single-payment-form.php</li>
</ul>

= 4.3.6 =

<ul>
    <li>New Admin Bulk Edit Users page for adding / removing manual (Free) memberships to users. <strong>WP Videos -> Members -> Bulk Manage</strong></li>
</ul>

= 4.3.5 =

<ul>
    <li>Tested up to version update</li>
</ul>

= 4.3.4 =

<ul>
    <li>New Terms &amp; Agreement Checkbox Settings for Create Account form</li>
    <li>Setup Terms &amp; Agreement Checkbox under <strong>WP Videos -> Checkout Settings -> Registration</strong></li>
</ul>

= 4.3.3 =

<ul>
    <li>Google reCAPTCHA v2 Checkbox optional for Create Account form</li>
    <li>Setup Google reCAPTCHA v2 under <strong>WP Videos -> Checkout Settings -> Registration</strong></li>
</ul>

= 4.3.2 =

<ul>
    <li>New filter <strong>wpvs_filter_currency_label_price_amount</strong> to filter price amount text.</li>
    <li>New filter <strong>wpvs_filter_currency_label</strong> to filter entire currency label text (i.e: $ 5.00 / week).</li>
    <li>Fixed a bug where plural intervals such as weeks may not be translated correctly.</li>
    <li>Translation file updates</li>
</ul>

= 4.3.1 =

<ul>
    <li>Account Page is now viewable when Lock Entire Site is enabled to allow users to login</li>
</ul>

= 4.3.0 =

<ul>
    <li>PayPal Class api settings update</li>
</ul>

= 4.2.9 =

<ul>
    <li>Get stripe subscription update on customer class.</li>
</ul>

= 4.2.8 =

<ul>
    <li>Stripe Integration Update: Strong Customer Authentication</li>
    <li>Credit Card agreement notice, note and checkbox for future payments</li>
    <li>Pay Now button for overdue Stripe subscriptions</li>
    <li>Stripe Pop-Up module deprecated</li>
    <li>Translation File Updates</li>
</ul>

= 4.2.7 =

<ul>
    <li>Japanese Yen Stripe Plan and PayPal Plan amounts fix when creating memberships</li>
</ul>

= 4.2.6 =

<ul>
    <li>Coinbase checkout automatic redirect to payment page</li>
    <li>New Sign In text filters 'wpvs_custom_sign_in_create_account_text' and 'wpvs_custom_sign_in_only_text'</li>
</ul>

= 4.2.5 =

<ul>
    <li>Japanese Yen decimal amounts fix</li>
</ul>

= 4.2.4 =

<ul>
    <li>Admin Checkout Settings page JS error fix</li>
</ul>

= 4.2.3 =

<ul>
    <li>Coinbase payment gateway support</li>
    <li>Japense Yen (JPY) currency added</li>
    <li>Translation file updates</li>
</ul>

= 4.2.2 =

<ul>
    <li>New Checkout Settings: Billing Information on Checkout</li>
    <li>Require Billing information on checkout: <strong>WP Videos -> CHeckout Settings</strong></li>
</ul>

= 4.2.1 =

<ul>
    <li>Translation files added for Italian (it_IT)</li>
</ul>

= 4.2.0 =

<ul>
    <li>Add user to blog for multisite Stripe customer import</li>
</ul>

= 4.1.9 =

<ul>
    <li>Stripe API Update (2019-03-14)</li>
</ul>

= 4.1.8 =

<ul>
    <li>Add User To Blog added for multisite Stripe customer imports</li>
    <li>Import Stripe Customers now automatically imports customer subscription Plans into <strong>WP Videos -> Memberships</strong></li>
    <li>New option to optionally Delete Stripe Plans when deleting a Membership from WordPress.</li>
</ul>

= 4.1.7 =

<ul>
    <li>Update to check if Stripe membership status is "incomplete" or "incomplete_expired"</li>
</ul>

= 4.1.6 =

<ul>
    <li>Single PayPal payments amount formatting fix for prices in the thousands.</li>
</ul>

= 4.1.5 =

<ul>
    <li>WPVS Customer class updates and function replacment</li>
    <li>Stripe subscription status display fix when subscriptions are set to Cancel at end of period.</li>
</ul>

= 4.1.4 =

<ul>
    <li>New Video Content Access Option <strong>Restricted Content</strong> (Optionally restrict video and video description content)</li>
</ul>

= 4.1.3 =

<ul>
    <li>Admin notice function duplicate fix</li>
</ul>

= 4.1.2 =

<ul>
    <li>Coupon Code tracking added to Payments: (Database update required)</li>
    <li>Coupon code Single PayPal Payment discount fix.</li>
</ul>

= 4.1.1 =

<ul>
    <li>New setting under <strong>Membership Settings</strong>: Membership Management to allow / disallow customers to delete memberships from their account.</li>
    <li>Translation file updates and Spanish (Mexico es_MX) added</li>
</ul>

= 4.1.0 =

<ul>
    <li>Optionally choose to delete active customer subscriptions when deleting a membership plan.</li>
    <li>Optionally choose to delete the User Role associated with a membership plan when deleting a membership plan.</li>
</ul>

= 4.0.9 =

<ul>
<li>Set Stripe API version option</li>
</ul>

= 4.0.8 =

<ul>
<li>Membership create billing plans update</li>
</ul>

= 4.0.7 =

<ul>
<li>Coupon Code minor CSS updates</li>
<li>Last 4 digits of card display fix</li>
</ul>

= 4.0.6 =

<ul>
    <li>Admin payments request fix for deleted user payments</li>
    <li>Stripe Webhook update</li>
</ul>

= 4.0.5 =

New Role setting for Memberships:
<ul>
    <li>Each Membership now has an optional "Create User Role" setting. Enabling the setting will automatically create and assign any users subscribed to the membership a unique Role in WordPress for that membership.</li>
    <li>New filter on the <strong>WP Videos -> Members</strong> page for filtering members by their Role</li>
</ul>

= 4.0.4 =

PayPal Webhooks Update:
<ul>
    <li>Sets correct payment time for payments (incase of delayed webhooks).</li>
    <li>Checks for sandbox mode in events to determine Live vs Sandbox event</li>
</ul>

= 4.0.3 =

Stripe API upgrade to version "2018-11-08". Please upgrade your Stripe API version in your Dashboard here: https://dashboard.stripe.com/developers
Fix for Re-activating Stripe subscriptions

= 4.0.2 =

New Default Account Menu Tab option under WP Videos -> Membership Settings. <a href="https://docs.wpvideosubscriptions.com/wp-video-memberships/account-page-setup/" target="_blank" rel="help">Account Page Setup Guide</a>

= 4.0.1 =

Indian Rupee Currency Added (INR)

= 4.0.0 =

Update to Member and TEST Member roles. Users are only added to these roles either manually or when they subscribe to a membership.

== Upgrade Notice ==

= 4.7.8 =

<ul>
<li>Czech Crown (CZK) currency added.</li>
<li>Translation file updates</li>
</ul>

= 4.7.7 =

<ul>
<li>Polyfill added for getallheaders() PHP function</li>
</ul>

= 4.7.6 =

<ul>
<li>Removed deprecated Vimeo thumbnail urls and css file from Rental and Purchase video thumbnails</li>
</ul>

= 4.7.5 =

<ul>
<li>Update request 4.7.5 check</li>
</ul>

= 4.7.4 =

<ul>
<li>Vimeo Download Link Request fixes and updates</li>
</ul>

= 4.7.3 =

<ul>
<li>Trial Frequency isset check for memberships checkout class.</li>
<li>Removed deprecated Import Videos menu item in admin dashboard.</li>
</ul>

= 4.7.2 =

<ul>
<li>PayPal button loading fix for Video Category Purchases</li>
</ul>

= 4.7.1 =

<ul>
<li>PayPal Trial Period bug fix where trial periods were being added twice on default Billing Plan subscriptions.</li>
<li>date_i18n updated to wp_date() function</li>
</ul>

= 4.7.0 =

<ul>
<li>Admin CSS updates for Bulk Editing video fields</li>
</ul>

= 4.6.9 =

<ul>
<li>Number format fix for PayPal Billing Agreement price.</li>
</ul>

= 4.6.8 =

<ul>
<li>New JSON data key <strong>wpvs_access_options</strong> to display assigned access options for each Video.</li>
<li><strong>Restriction User Logins:</strong> New settings found under <strong>WP Videos -> Membership Settings -> Account Settings -> Login Restrictions</strong>.</li>
</ul>

= 4.6.7 =

<ul>
<li>PayPal bug fix where one-time use coupon codes were not applied to Billing Agreements with Trial Periods</li>
<li>Tax Rates now applied to discount coupon code billing agreements.</li>
</ul>

= 4.6.6 =

<ul>
<li>Tax Rate fix for Jurisdictions with apostrophes.</li>
<li>Membership ship Status drop down appearance update to indicate select box.</li>
</ul>

= 4.6.5 =

<ul>
<li>Stripe subscription admin sync updates</li>
</ul>

= 4.6.4 =

<ul>
<li>Update Stripe customer id on checkout session completed webhook event</li>
</ul>

= 4.6.3 =

<ul>
<li>Stripe Webhook ID and Secret fix when resaving Stripe Payment Gateway settings.</li>
</ul>

= 4.6.2 =

<ul>
<li>Stripe Checkout Sessions: Set customer id fix and clean completed checkout sessions from db.</li>
</ul>

= 4.6.1 =

<ul>
<li>Stripe Webhook bug fix for Checkout Session Complete</li>
</ul>

= 4.6.0 =

<ul>
<li>Tax Rate db tables creation on plugin activation.</li>
</ul>

= 4.5.9 =

<ul>
<li>Stripe Tax Rate API creation fix</li>
<li>Translation file updates</li>
</ul>

= 4.5.8 =

<ul>
<li>Inclusive Tax Rate Calculating Updates</li>
</ul>

= 4.5.7 =

<ul>
<li>WPVS Checkout button load fix</li>
</ul>

= 4.5.6 =

<ul>
<li>WPVS Checkout class updates and process</li>
<li>WPVS Checkout improvements for calculating Coupons, Tax Rates and Totals.</li>
</ul>

= 4.5.5 =

<ul><li>Coupon Code &amp; Checkout calculating subtotal bug fix.</li></ul>

= 4.5.3 =

<ul><li>Tax Rates: Create your Tax Rates under <strong>WP Videos -> Tax Rates</strong>. Note: WP Videos -> Checkout Settings -> Checkout -> Require Billing Information must be Enabled for Tax Rates to function.</li></ul>

= 4.4.6 =

<ul>
    <li>Stripe Payments and Invoices fix on Account page for customers.</li>
</ul>

= 4.4.5 =

<ul>
    <li>Tested up to WordPress 5.3.2 readme file update.</li>
</ul>

= 4.4.4 =

<ul>
    <li>Filter users by Role and Without Membership on the Bulk Edit Users admin page.</li>
</ul>

= 4.4.3 =

<ul>
    <li>New shortcode <strong>[wpvs-restricted-content]</strong> ...restricted content here... <strong>[/wpvs-restricted-content]</strong> for restricting Page, Post and other post type content to members only: <a href="https://docs.wpvideosubscriptions.com/wp-video-memberships/shortcodes/" target="_blank">See Instructions</a></li>
</ul>

= 4.4.2 =

<ul>
    <li>Fixed Coinbase subscription bug when user memberships were empty.</li>
</ul>

= 4.4.1 =

<ul>
    <li>Fixed a bug where PayPal agreements with a Day interval would return a status of Pending and expired payment date.</li>
</ul>

= 4.4.0 =

<ul>
    <li>Fixed a bug where duplicate Subscribe options would display if both the Video and Category had the same Membership assigned.</li>
</ul>

= 4.3.9 =

<ul>
    <li>Translation file updates</li>
</ul>

= 4.3.8 =

<ul>
    <li>Number format fix for creating PayPal plans (removed comma from 4+ digit values)</li>
</ul>

= 4.3.7 =

<ul>
    <li>New Stripe Checkout page (optional). Enable under <strong>WP Videos -> Payment Gateway -> Stripe</strong></li>
    <li><strong>IMPORTANT:</strong> the /includes/term-payment-form.php file has been combined with 1 file /includes/single-payment-form.php.</li>
    <li><strong>IMPORTANT:</strong> /include-stripe/single-payment-form.php has been moved to /includes/single-payment-form.php</li>
</ul>

= 4.3.6 =

<ul>
    <li>New Admin Bulk Edit Users page for adding / removing manual (Free) memberships to users. <strong>WP Videos -> Members -> Bulk Manage</strong></li>
</ul>

= 4.3.5 =

<ul>
    <li>Tested up to version update</li>
</ul>

= 4.3.4 =

<ul>
    <li>New Terms &amp; Agreement Checkbox Settings for Create Account form</li>
    <li>Setup Terms &amp; Agreement Checkbox under <strong>WP Videos -> Checkout Settings -> Registration</strong></li>
</ul>

= 4.3.3 =

<ul>
    <li>Google reCAPTCHA v2 Checkbox optional for Create Account form</li>
    <li>Setup Google reCAPTCHA v2 under <strong>WP Videos -> Checkout Settings -> Registration</strong></li>
</ul>

= 4.3.2 =

<ul>
    <li>New filter <strong>wpvs_filter_currency_label_price_amount</strong> to filter price amount text.</li>
    <li>New filter <strong>wpvs_filter_currency_label</strong> to filter entire currency label text (i.e: $ 5.00 / week).</li>
    <li>Fixed a bug where plural intervals such as weeks may not be translated correctly.</li>
    <li>Translation file updates</li>
</ul>

= 4.3.1 =

<ul>
    <li>Account Page is now viewable when Lock Entire Site is enabled to allow users to login</li>
</ul>

= 4.3.0 =

<ul>
    <li>PayPal Class api settings update</li>
</ul>

= 4.2.9 =

<ul>
    <li>Get stripe subscription update on customer class.</li>
</ul>

= 4.2.8 =

<ul>
    <li>Stripe Integration Update: Strong Customer Authentication</li>
    <li>Credit Card agreement notice, note and checkbox for future payments</li>
    <li>Pay Now button for overdue Stripe subscriptions</li>
    <li>Stripe Pop-Up module deprecated</li>
    <li>Translation File Updates</li>
</ul>

= 4.2.7 =

<ul>
    <li>Japanese Yen Stripe Plan and PayPal Plan amounts fix when creating memberships</li>
</ul>

= 4.2.6 =

<ul>
    <li>Coinbase checkout automatic redirect to payment page</li>
    <li>New Sign In text filters 'wpvs_custom_sign_in_create_account_text' and 'wpvs_custom_sign_in_only_text'</li>
</ul>

= 4.2.5 =

<ul>
    <li>Japanese Yen decimal amounts fix</li>
</ul>

= 4.2.4 =

<ul>
    <li>Admin Checkout Settings page JS error fix</li>
</ul>

= 4.2.3 =

<ul>
    <li>Coinbase payment gateway support</li>
    <li>Japense Yen (JPY) currency added</li>
    <li>Translation file updates</li>
</ul>

= 4.2.2 =

<ul>
    <li>New Checkout Settings: Billing Information on Checkout</li>
    <li>Require Billing information on checkout: <strong>WP Videos -> CHeckout Settings</strong></li>
</ul>

= 4.2.1 =

<ul>
    <li>Translation files added for Italian (it_IT)</li>
</ul>

= 4.2.0 =

<ul>
    <li>Add user to blog for multisite Stripe customer import</li>
</ul>

= 4.1.9 =

<ul>
    <li>Stripe API Update (2019-03-14)</li>
</ul>

= 4.1.8 =

<ul>
    <li>Add User To Blog added for multisite Stripe customer imports</li>
    <li>Import Stripe Customers now automatically imports customer subscription Plans into <strong>WP Videos -> Memberships</strong></li>
    <li>New option to optionally Delete Stripe Plans when deleting a Membership from WordPress.</li>
</ul>

<ul>
    <li>Update to check if Stripe membership status is "incomplete" or "incomplete_expired"</li>
</ul>

= 4.1.6 =

<ul>
    <li>Single PayPal payments amount formatting fix for prices in the thousands.</li>
</ul>

= 4.1.5 =

<ul>
    <li>WPVS Customer class updates and function replacment</li>
    <li>Stripe subscription status display fix when subscriptions are set to Cancel at end of period.</li>
</ul>

= 4.1.4 =

<ul>
    <li>New Video Content Access Option <strong>Restricted Content</strong> (Optionally restrict video and video description content)</li>
</ul>

= 4.1.3 =

<ul>
    <li>Admin notice function duplicate fix</li>
</ul>

= 4.1.2 =

<ul>
    <li>Coupon Code tracking added to Payments: (Database update required)</li>
    <li>WP Videos -> Payments can now be filtered by Coupon Code use: (only displays Payments added after version 4.1.2)</li>
    <li>Coupon code Single PayPal Payment discount fix.</li>
</ul>

= 4.1.1 =

<ul>
    <li>New setting under <strong>Membership Settings</strong>: Membership Management to allow / disallow customers to delete memberships from their account.</li>
    <li>Translation file updates and Spanish (Mexico es_MX) added</li>
</ul>

= 4.1.0 =

<ul>
    <li>Optionally choose to delete active customer subscriptions when deleting a membership plan.</li>
    <li>Optionally choose to delete the User Role associated with a membership plan when deleting a membership plan.</li>
</ul>

= 4.0.9 =

<ul>
<li>Set Stripe API version option</li>
</ul>

= 4.0.8 =

<ul>
<li>Membership create billing plans update</li>
</ul>

= 4.0.7 =

<ul>
<li>Coupon Code minor CSS updates</li>
<li>Last 4 digits of card display fix</li>
</ul>

= 4.0.6 =

<ul>
    <li>Admin payments request fix for deleted user payments</li>
    <li>Stripe Webhook update</li>
</ul>

= 4.0.5 =

New Role setting for Memberships:
<ul>
    <li>Each Membership now has an optional "Create User Role" setting. Enabling the setting will automatically create and assign any users subscribed to the membership a unique Role in WordPress for that membership.</li>
    <li>New filter on the <strong>WP Videos -> Members</strong> page for filtering members by their Role</li>
</ul>

= 4.0.4 =

PayPal Webhooks Update:
<ul>
    <li>Sets correct payment time for payments (incase of delayed webhooks).</li>
    <li>Checks for sandbox mode in events to determine Live vs Sandbox event</li>
</ul>

= 4.0.3 =

Stripe API upgrade to version "2018-11-08". Please upgrade your Stripe API version in your Dashboard here: https://dashboard.stripe.com/developers
Fix for Re-activating Stripe subscriptions

= 4.0.2 =

New Default Account Menu Tab option under WP Videos -> Membership Settings. <a href="https://docs.wpvideosubscriptions.com/wp-video-memberships/account-page-setup/" target="_blank" rel="help">Account Page Setup Guide</a>

= 4.0.1 =

Indian Rupee Currency Added (INR)

 4.0.0 =

Update to Member and TEST Member roles. Users are only added to these roles either manually or when they subscribe to a membership.
