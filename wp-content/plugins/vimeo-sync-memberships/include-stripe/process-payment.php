<?php
function wpvs_process_stripe_checkout_ajax_request() {
	if( is_user_logged_in() && isset($_POST['transaction_type']) && ( isset($_POST['payment_method_id']) || isset($_POST['payment_intent_id']) ) && wp_verify_nonce($_POST['stripe_nonce'], 'stripe-nonce') ) {
        global $rvs_live_mode;
        global $rvs_current_user;
        global $rvs_currency;
		$create_stripe_charge = true;
		if($rvs_live_mode == "on") {
            $stripe_key = "stripe";
            $customer_key = 'rvs_stripe_customer';
            $test_member = false;
		} else {
            $stripe_key = "stripe_test";
            $customer_key = 'rvs_stripe_customer_test';
			$test_member = true;
		}
		$stripe_payment_method_id = null;
		if( isset($_POST['payment_method_id']) ) {
			$stripe_payment_method_id = $_POST['payment_method_id'];
		}
		$applied_taxes = array();

        // Get current WordPress user details

        $wpvs_customer = new WPVS_Customer($rvs_current_user);
		$stripe_payments_manager = new WPVS_Stripe_Payments_Manager($rvs_current_user->ID);
        $stripe_customer = $stripe_payments_manager->get_stripe_customer_details();

		// CHECK IF STRIPE CUSTOMER EXISTS

		if( empty($stripe_customer) ) {
			$new_customer_details = wpvs_default_new_stripe_customer_details($rvs_current_user);
			if( ! empty($stripe_payment_method_id) && isset($_POST['wpvs_save_card_for_future']) ) {
				$new_customer_details["payment_method"] = $stripe_payment_method_id;
				$new_customer_details["invoice_settings"]["default_payment_method"] = $stripe_payment_method_id;
			}
			$stripe_customer_id = $stripe_payments_manager->add_new_customer($new_customer_details);
		} else {
			$stripe_customer_id = $stripe_customer->id;
		}

        $site_name = get_bloginfo('name');
        $wpvs_require_billing_info = get_option('wpvs_require_billing_information', 0);
        $wpvs_billing_address = null;
        $wpvs_billing_address_line_2 = "";

        if( $wpvs_require_billing_info && $wpvs_customer->missing_billing_info() ) {
			$error_message = __('Missing Billing Information', 'vimeo-sync-memberships');
		    rvs_exit_ajax($error_message, 400);
        }

        // CHECK FOR COUPON CODE

        if( isset($_POST['coupon_code']) && ! empty($_POST['coupon_code']) ) {
            $coupon_code = $_POST['coupon_code'];
        } else {
            $coupon_code = null;
        }

        if( $wpvs_require_billing_info && ! $wpvs_customer->missing_billing_info() ) {
			$customer_details = array(
				'address' => array(
					'line1' => $wpvs_customer->billing_address,
					'city'  => $wpvs_customer->billing_city,
					'state' => $wpvs_customer->billing_state,
					'postal_code' => $wpvs_customer->billing_zip_code,
					'country' => $wpvs_customer->billing_country,
					'line2' => $wpvs_customer->billing_address_line_2
				)
			);
			$customer_updated = $stripe_payments_manager->update_customer($customer_details);
			if( isset($customer_updated['error']) ) {
				$error_message = $customer_updated['error'];
		        rvs_exit_ajax($error_message, $customer_updated['code']);
			}
        }

        // CHECK IF SUBSCRIPTION OR SINGLE CHARGE

        $transaction_type = $_POST['transaction_type'];

        if($transaction_type == "purchase") {
            $product_id = intval($_POST['product_id']);
            $purchase_type = $_POST['purchase_type'];

            if($purchase_type == 'termpurchase') {
                $wpvs_term = get_term($product_id, 'rvs_video_category' );
                if( ! empty($wpvs_term) && ! is_wp_error($wpvs_term) ) {
                    $charge_amount = get_term_meta($product_id, 'wpvs_category_purchase_price', true);
                    $charge_title = $wpvs_term->name;
                }
            } else {
                $charge_title = get_the_title($product_id);
            }

            // GET AMOUNT
            if($purchase_type == "purchase") {
                $charge_amount = get_post_meta( $product_id, '_rvs_onetime_price', true );
            }
            if($purchase_type == "rental") {
                $charge_amount = get_post_meta( $product_id, 'rvs_rental_price', true );
            }
            $charge_amount = intval($charge_amount);
            $charge_description = __('Access to', 'vimeo-sync-memberships') . ' ' . $charge_title . ' ' . __('on', 'vimeo-sync-memberships') . ' ' . $site_name;

            if( ! empty($coupon_code) ) {
                $charge_amount = wpvs_calculate_coupon_amount($charge_amount, $coupon_code);
				$charge_amount = ceil($charge_amount);
            }

			if( $rvs_currency == "JPY" ) {
				// remove decimals for Japanese Yen
				$charge_amount = number_format($charge_amount/100, 0);
			} else {
				// Charge is free from coupon; Stripe charges under 50 cents not allowed
				if( $charge_amount < 50 ) {
					$create_stripe_charge = false;
				}
			}

			if( $charge_amount <= 0 ) {
				$create_stripe_charge = false;
			}

			$redirect = $_POST['redirect'];

            if( $create_stripe_charge ) {
				if( isset($_POST['payment_intent_id']) && ! empty($_POST['payment_intent_id']) ) {
					$payment_intent_id = $_POST['payment_intent_id'];
					$payment_intent = $stripe_payments_manager->get_payment_intent($payment_intent_id, true);
				} else {

					if( isset($_POST['wpvs_save_card_for_future']) && ! $stripe_payments_manager->customer_has_payment_method($stripe_payment_method_id) ) {
						$stripe_payments_manager->add_payment_method_to_customer($stripe_payment_method_id);
					}

					if( $wpvs_require_billing_info && ! empty($wpvs_customer->billing_state) && ! empty($wpvs_customer->billing_country) ) {
						$wpvs_tax_rate = new WPVS_Tax_Rate();
						$decimal_total = number_format($charge_amount/100, 2);
		                $jurisdiction_rates = $wpvs_tax_rate->get_tax_rates_by_jurisdiction($wpvs_customer->billing_state, $wpvs_customer->billing_country);
		                if( ! empty($jurisdiction_rates) ) {
		                    foreach($jurisdiction_rates as $tax_rate) {
								$get_tax_rate_amount = $wpvs_tax_rate->calculate_tax_rate_amount($decimal_total, $tax_rate);
		                        if( ! empty($get_tax_rate_amount) ) {
		                            $applied_taxes[] = (object) array(
		                                'tax_name' => $tax_rate->display_name,
		                                'amount'   => $get_tax_rate_amount->amount,
										'decimal_amount'   => $get_tax_rate_amount->decimal_amount,
		                                'inclusive' => $tax_rate->inclusive
		                            );
		                        }
		                    }
		                }
		            }

	                $stripe_payment_details = array(
					  "payment_method" => $stripe_payment_method_id,
	                  "amount" => $charge_amount,
	                  "currency" => $rvs_currency,
	                  "customer" => $stripe_customer_id,
	                  "description" => $charge_description,
	                  "metadata" => array(
	                      "type" => $purchase_type,
	                      "productid" => $product_id
	                  ),
					  "receipt_email" => $rvs_current_user->user_email,
					  "confirm" => true
	                );
	                if( ! empty($coupon_code) ) {
	                    $stripe_payment_details['metadata']['coupon'] = $coupon_code;
	                }
					if( ! empty($applied_taxes) ) {
						foreach($applied_taxes as $apply_tax) {
							$stripe_payment_details['metadata'][$apply_tax->tax_name] = $apply_tax->decimal_amount;
		                    if( ! $apply_tax->inclusive ) {
		                        $charge_amount += $apply_tax->amount;
		                    }
		                }
						$stripe_payment_details['amount'] = $charge_amount;
					}

					$payment_intent = $stripe_payments_manager->new_payment_intent($stripe_payment_details);
				}
				if( isset($payment_intent['error']) ) {
					$error_message = $payment_intent['error'];
			        rvs_exit_ajax($error_message, $payment_intent['code']);
				}
				if( isset($payment_intent['requires_action']) && isset($payment_intent['payment_intent_client_secret']) ) {
					echo json_encode($payment_intent);
					exit;
				}
				if( isset($payment_intent['requires_confirmation']) ) {
					$payment_intent_confirmed = $stripe_payments_manager->confirm_payment_intent($payment_intent->id);
					if( isset($payment_intent_confirmed['error']) ) {
						$error_message = $payment_intent_confirmed['error'];
				        rvs_exit_ajax($error_message, $payment_intent_confirmed['code']);
					}
				}
				if( isset($payment_intent['requires_payment_method']) ) {
					$error_message = __('No payment method found.', 'vimeo-sync-memberships');
			        rvs_exit_ajax($error_message, $payment_intent['code']);
				}
           	}

            $wpvs_customer->add_product($product_id, $purchase_type);

            if( ! empty($coupon_code) ) {
                wpvs_add_coupon_code_use($coupon_code, $rvs_current_user->ID);
            }
            wpvs_add_new_member($rvs_current_user->ID, $test_member);
			echo json_encode( array('success' => true, 'redirect' => $redirect) );

        }

        if($transaction_type == "subscription") {
            //Get membership details
            $membership_id = $_POST['product_id'];

            $plan_trial = false;
            if( ! empty(get_option('rvs_membership_list')) ) {
                $membershipList = get_option('rvs_membership_list');
                foreach($membershipList as $plan) {
                    if($plan['id'] == $membership_id) {
                        $price = $plan['amount'];
                        $name = $plan['name'];
                        $interval = $plan['interval'];
                        $interval_count = $plan['interval_count'];
                        if( isset($plan['trial_frequency']) && isset($plan['trial']) && ! wpvs_user_has_completed_trial($rvs_current_user->ID, $membership_id) ) {
                            $plan_trial = true;
                        }
                        break;
                    }
                }
            }

            if( $wpvs_require_billing_info && ! empty($wpvs_customer->billing_state) && ! empty($wpvs_customer->billing_country) ) {
				$wpvs_tax_rate = new WPVS_Tax_Rate();
                $jurisdiction_rates = $wpvs_tax_rate->get_tax_rates_by_jurisdiction($wpvs_customer->billing_state, $wpvs_customer->billing_country);
                if( ! empty($jurisdiction_rates) ) {
                    foreach($jurisdiction_rates as $tax_rate) {
						$tax_rate_meta = json_decode($tax_rate->meta, false);
                        if( $stripe_payments_manager->test_mode &&  isset($tax_rate_meta->test_stripe_tax_rate_id) ) {
                            $applied_taxes[] = $tax_rate_meta->test_stripe_tax_rate_id;
                        }
						if( ! $stripe_payments_manager->test_mode &&  isset($tax_rate_meta->stripe_tax_rate_id) ) {
                            $applied_taxes[] = $tax_rate_meta->stripe_tax_rate_id;
                        }
                    }
                }
            }

			$new_stripe_subscription_args = array(
				"customer" => $stripe_customer_id,
				"items" => array(
					array(
						"plan" => $membership_id,
					),
				),
				"off_session" => true,
				"prorate" => false,
				"trial_from_plan" => $plan_trial
			);
			if( ! empty($coupon_code) ) {
				$new_stripe_subscription_args["coupon"] = $coupon_code;
			}
			if( ! empty($applied_taxes) ) {
				$new_stripe_subscription_args["default_tax_rates"] = $applied_taxes;
			}

			$subscription = $stripe_payments_manager->new_subscription($new_stripe_subscription_args);

			if( isset($subscription['error']) ) {
				$error_message = $subscription['error'];
		        rvs_exit_ajax($error_message, 400);
			} else {
				$add_new_membership = array(
                    'plan' => $membership_id,
                    'id' => $subscription->id,
                    'amount' => $price,
                    'name' => $name,
                    'interval' => $interval,
                    'interval_count' => $interval_count,
                    'ends' => $subscription->current_period_end,
                    'status' => $subscription->status,
                    'type' => $stripe_key
                );

                if($coupon_code != null && $subscription->discount != null) {
                    $add_new_membership['discount'] = $subscription->discount->coupon;
                }

                $wpvs_customer->add_membership($add_new_membership);

                if( $plan_trial ) {
                    wpvs_user_completed_trial($rvs_current_user->ID, $membership_id);
                }

                wpvs_add_new_member($rvs_current_user->ID, $test_member);
                if( ! empty($coupon_code) ) {
                    wpvs_add_coupon_code_use($coupon_code, $rvs_current_user->ID);
                }

                if(isset($_POST['redirect_success']) && ! empty($_POST['redirect_success'])) {
                    $redirect_after = $_POST['redirect_success'];
                } else {
                    $rvs_account_page = get_option('rvs_account_page');
                    $redirect_after = get_permalink($rvs_account_page);
                }
				echo json_encode( array('success' => true, 'redirect' => $redirect_after) );
			}
        }
	}
	wp_die();
}
add_action( 'wp_ajax_wpvs_process_stripe_checkout_ajax_request', 'wpvs_process_stripe_checkout_ajax_request' );
