<?php
/**
* WP Video Memberships Stripe Classes
*/

require_once(RVS_MEMBERS_BASE_DIR . '/stripe/init.php');

class WPVS_Stripe_Payments_Manager {

    public $test_mode;
    protected $secret_key;
    protected $api_version;
    protected $customer_key;
    protected $user_id;
    protected $checkout_sessions_table;

    public function __construct($user_id) {
        global $wpvs_stripe_options;
        global $rvs_live_mode;
        global $wpvs_stripe_api_version;
        $this->api_version = $wpvs_stripe_api_version;
        $this->user_id = $user_id;
        if($rvs_live_mode == "off") {
            $this->test_mode = true;
            $this->customer_key = 'rvs_stripe_customer_test';
            $this->checkout_sessions_table = 'wpvs_test_stripe_checkout_sessions';
            $this->secret_key = $wpvs_stripe_options['test_secret_key'];
        } else {
            $this->test_mode = false;
            $this->customer_key = 'rvs_stripe_customer';
            $this->checkout_sessions_table = 'wpvs_stripe_checkout_sessions';
            $this->secret_key = $wpvs_stripe_options['live_secret_key'];
        }
    }

    protected function config_request() {
        \Stripe\Stripe::setApiKey($this->secret_key);
        \Stripe\Stripe::setApiVersion($this->api_version);
    }

    public function set_api_mode($mode) {
        global $wpvs_stripe_options;
        if( $mode == 'test' ) {
            $this->secret_key = $wpvs_stripe_options['test_secret_key'];
            $this->test_mode = true;
            $this->customer_key = 'rvs_stripe_customer_test';
            $this->checkout_sessions_table = 'wpvs_test_stripe_checkout_sessions';
        } else {
            $this->secret_key = $wpvs_stripe_options['live_secret_key'];
            $this->test_mode = false;
            $this->customer_key = 'rvs_stripe_customer';
            $this->checkout_sessions_table = 'wpvs_stripe_checkout_sessions';
        }
    }

    protected function set_user_id($user_id) {
        $this->user_id = $user_id;
    }

    public function customer_id() {
        if( ! empty($this->user_id) ) {
            return get_user_meta($this->user_id, $this->customer_key, true);
        } else {
            return null;
        }
    }

    public function update_customer_id($customer_id) {
        if( ! empty($this->user_id) ) {
            update_user_meta($this->user_id, $this->customer_key, $customer_id);
        }
    }

    public function new_setup_intent() {
        try {
            $this->config_request();
            $setup_intent = \Stripe\SetupIntent::create();
            return $this->generate_payment_response($setup_intent);
        } catch (Exception $e) {
            return array(
                'error' => $e->getMessage(),
                'code'   => 400,
            );
        }
    }

    public function new_payment_intent($payment_intent_details) {
        try {
            $this->config_request();
            $payment_intent = \Stripe\PaymentIntent::create($payment_intent_details);
            return $this->generate_payment_response($payment_intent);
        } catch (Exception $e) {
            return array(
                'error' => $e->getMessage(),
                'code'   => 400,
            );
        }
    }

    public function confirm_payment_intent($payment_intent_id) {
        try {
            $this->config_request();
            $payment_intent = \Stripe\PaymentIntent::retrieve($payment_intent_id);
            $payment_intent->confirm();
        } catch (Exception $e) {
            return array(
                'error' => $e->getMessage(),
                'code'   => 400,
            );
        }
    }

    public function get_payment_intent($payment_intent_id, $get_response) {
        try {
            $this->config_request();
            $payment_intent = \Stripe\PaymentIntent::retrieve($payment_intent_id);
            if( $get_response ) {
                return $this->generate_payment_response($payment_intent);
            } else {
                return $payment_intent;
            }
        } catch (Exception $e) {
            return array(
                'error' => $e->getMessage(),
                'code'   => 400,
            );
        }
    }

    private function generate_payment_response($intent) {
        if ( $intent->status == 'requires_action' && $intent->next_action->type == 'use_stripe_sdk' && ! empty($intent->client_secret) ) {
            return array(
                'requires_action' => true,
                'payment_intent_client_secret' => $intent->client_secret
            );
        } else if ( $intent->status == 'requires_payment_method') {
            return array(
                'requires_payment_method' => true,
                'payment_intent_client_secret' => $intent->client_secret
            );
        } else if ( $intent->status == 'requires_confirmation') {
            return array(
                'requires_confirmation' => true,
                'payment_intent_client_secret' => $intent->client_secret
            );
        } else if ($intent->status == 'succeeded') {
            return array(
                'success' => true,
                'payment_method' => $intent->payment_method
            );
        } else {
            return array(
                'error' => __('Invalid PaymentIntent status', 'vimeo-sync-memberships'),
                'code'   => 500,
            );
        }
    }

    public function add_payment_method_to_customer($payment_method_id) {
        try {
            $this->config_request();
            $payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
            $payment_method->attach(['customer' => $this->customer_id()]);
            $stripe_customer = \Stripe\Customer::retrieve($this->customer_id());
            if( empty($stripe_customer->invoice_settings->default_payment_method) || empty($this->get_payment_methods()) ) {
                $stripe_customer->invoice_settings->default_payment_method = $payment_method_id;
                $stripe_customer->save();
            }
        } catch (Exception $e) {
            return array(
                'error' => $e->getMessage(),
                'code'   => 400,
            );
        }
    }

    public function customer_has_payment_method($payment_method_id) {
        $has_payment_method = false;
        $customer_payment_methods = $this->get_payment_methods();
        if( ! empty($customer_payment_methods) ) {
            foreach($customer_payment_methods as $method) {
                if( $method->id == $payment_method_id) {
                    $has_payment_method = true;
                    break;
                }
            }
        }
        return $has_payment_method;
    }

    public function add_new_customer($new_customer_details) {
        try {
            $this->config_request();
            $new_stripe_customer = \Stripe\Customer::create($new_customer_details);
            update_user_meta($this->user_id, $this->customer_key, $new_stripe_customer->id);
            return $new_stripe_customer->id;
        } catch (Exception $e) {
            return array(
                'error' => $e->getMessage(),
                'code'   => 400,
            );
        }
    }

    public function update_customer($customer_details) {
        global $rvs_current_user;
        if( ! empty($rvs_current_user) ) {
            try {
                $this->config_request();
                \Stripe\Customer::update($this->customer_id(), $customer_details);
            } catch (Exception $e) {
                return array(
                    'error' => $e->getMessage(),
                    'code'   => 400,
                );
            }
        }
    }

    public function get_stripe_customer_by_id($customer_id) {
        $customer = null;
        try {
            $this->config_request();
            $customer = \Stripe\Customer::retrieve($customer_id);
        } catch (Exception $e) {

        }
        return $customer;
    }

    public function get_stripe_customer_details() {
        $customer = null;
        if( ! empty($this->customer_id()) ) {
            try {
                $this->config_request();
                $customer = \Stripe\Customer::retrieve($this->customer_id());
                if( isset($customer->deleted) && $customer->deleted ) {
                    $customer = null;
                    update_user_meta($this->user_id, $customer_key, null);
                }
                if(!isset($customer->metadata->userid)) {
                    $customer->metadata->userid = $this->user_id;
                    $customer->save();
                }
            } catch (Exception $e) {
                $customer = null;
            }
        }
        return $customer;
    }

    public function new_subscription($new_subscription_args) {
        try {
            $this->config_request();
            $subscription = \Stripe\Subscription::create($new_subscription_args);
            return $subscription;
        } catch (Exception $e) {
            return array(
                'error' => $e->getMessage(),
                'code'   => 400,
            );
        }
    }

    public function get_payment_methods() {
        $payment_methods = array();
        try {
            $this->config_request();
            $get_payment_method_args = array("customer" => $this->customer_id(), "type" => "card");
            $get_payment_methods = \Stripe\PaymentMethod::all($get_payment_method_args);
            if( isset($get_payment_methods->data) && ! empty($get_payment_methods->data) ) {
                $payment_methods = $get_payment_methods->data;
            }
        } catch (Exception $e) {
            $payment_methods = array();
        }
        return $payment_methods;
    }

    public function get_subscription($subscription_id) {
        try {
            $this->config_request();
            $subscription = \Stripe\Subscription::retrieve($subscription_id);
            return $subscription;
        } catch (Exception $e) {
            return (object) array(
                'error' => $e->getMessage(),
                'code'   => 400,
            );
        }
    }

    public function cancel_subscription($user_id, $subscription_id, $api_mode) {
        $subscription_cancelled = array(
            'confirmed' => false,
            'error'     => '',
        );
        if( ! empty($api_mode) ) {
            $this->set_api_mode($api_mode);
        }
        if( ! empty($user_id) && current_user_can('manage_options') ) {
            $this->set_user_id($user_id);
        }
        $stripe_customer_id = $this->customer_id();
        if( ! empty($stripe_customer_id) ) {
            try {
                $this->config_request();
                $stripe_customer = \Stripe\Customer::retrieve($stripe_customer_id);
                $stripe_customer->subscriptions->retrieve($subscription_id)->cancel();
                $subscription_cancelled['confirmed'] = true;
            } catch (Exception $e) {
                $subscription_cancelled['error'] = $e->getMessage();
            }
        }
        return (object) $subscription_cancelled;
    }

    public function get_invoice($invoice_data) {
        try {
            $this->config_request();
            $invoice = \Stripe\Invoice::retrieve($invoice_data);
            return $invoice;
        } catch (Exception $e) {
            return array(
                'error' => $e->getMessage(),
                'code'   => 400,
            );
        }
    }

    public function update_invoice($invoice_id, $invoice_data) {
        try {
            $this->config_request();
            $invoice = \Stripe\Invoice::update($invoice_id, $invoice_data);
            return $invoice;
        } catch (Exception $e) {
            return array(
                'error' => $e->getMessage(),
                'code'   => 400,
            );
        }
    }

    public function get_customer_invoices($charge_id) {
        $customer_invoices = array();
        $stripe_customer_id = $this->customer_id();
        if( ! empty($stripe_customer_id) ) {
            $get_stripe_charges = array("limit" => 10, "customer" => $stripe_customer_id);
            if( ! empty($charge_id) ) {
                $get_stripe_charges['starting_after'] = $charge_id;
            }
            try {
                $this->config_request();
                $customer_invoices = \Stripe\Charge::all($get_stripe_charges)->data;
            } catch (Exception $e) {
                $customer_invoices['error'] = $e->getMessage();
            }
        }
        return $customer_invoices;
    }

    public function new_checkout_session($session_details) {
        $stripe_customer_id = $this->customer_id();
        if( ! isset($session_details['customer']) && ! empty( $stripe_customer_id ) ) {
            $session_details['customer'] = $stripe_customer_id;
            unset($session_details['customer_email']);
        }
        try {
            $this->config_request();
            $new_session = \Stripe\Checkout\Session::create($session_details);
            $new_sesstion_id = $new_session->id;
            return $new_sesstion_id;
        } catch (Exception $e) {
            return array(
                'error' => $e->getMessage(),
                'code'   => 400,
            );
        }
    }

    public function add_user_checkout_session($session_details) {
        global $wpdb;
        $table_name = $wpdb->prefix . $this->checkout_sessions_table;
        $wpdb->insert(
            $table_name,
            $session_details
        );
    }

    public function get_checkout_session_by_id($checkout_id) {
        global $wpdb;
        $table_name = $wpdb->prefix . $this->checkout_sessions_table;
        $coin_payment = $wpdb->get_results("SELECT * FROM $table_name WHERE id = '$checkout_id'");
        return $checkout_session_id;
    }

    public function delete_checkout_session($checkout_id) {
        global $wpdb;
        $table_name = $wpdb->prefix . $this->checkout_sessions_table;
        $checkout_session_deleted = $wpdb->get_results("DELETE FROM $table_name WHERE id = '$checkout_id'");
        return $checkout_session_deleted;
    }

    public function get_user_checkout_session($user_id, $product_type, $product_id, $currency, $purchase_type, $amount) {
        global $wpdb;
        $checkout_session = null;
        $table_name = $wpdb->prefix . $this->checkout_sessions_table;
        if( $product_type == "subscription" ) {
            $checkout_query = "SELECT * FROM $table_name WHERE userid = '$user_id' AND planid = '$product_id'";
        }
        if( $product_type == "video" ) {
            $checkout_query = "SELECT * FROM $table_name WHERE userid = '$user_id' AND videoid = '$product_id'";
        }
        if( $product_type == "term" ) {
            $checkout_query = "SELECT * FROM $table_name WHERE userid = '$user_id' AND termid = '$product_id'";
        }

        if( ! empty($currency) ) {
            $checkout_query .= " AND currency = '$currency'";
        }
        $checkout_query .= " AND purchase_type = '$purchase_type'";
        $checkout_session = $wpdb->get_results($checkout_query);
        if( ! empty($checkout_session) ) {
            $checkout_session = $checkout_session[0];
        }
        return $checkout_session;
    }

    public function verify_coupon_code($coupon_code) {
        $validate_coupon = array(
            'is_valid' => false,
            'error'    => '',
        );
        try {
            $this->config_request();
            $stripe_coupon = \Stripe\Coupon::retrieve($coupon_code);
            if( isset($stripe_coupon->valid) && ! $stripe_coupon->valid ) {
                $validate_coupon['error'] = __('Invalid coupon code', 'vimeo-sync-memberships');
            } else {
                $validate_coupon['is_valid'] = true;
            }
        } catch (Exception $e) {
            $validate_coupon['error'] = $e->getMessage();
        }
        return (object) $validate_coupon;
    }

    protected function get_event($event_id) {
        $stripe_event = \Stripe\Event::retrieve($event_id);
        return $stripe_event;
    }
}

/*
* WPVS STRIPE AJAX REQUESTS
*
*/

class WPVS_STRIPE_USER_AJAX_MANAGER extends WPVS_Stripe_Payments_Manager {
    public function setup_ajax_functions() {
        add_action( 'wp_ajax_wpvs_update_membership_ajax_request', array($this, 'wpvs_update_membership_ajax_request') );
        add_action( 'wp_ajax_wpvs_save_card_ajax_request', array($this, 'wpvs_save_card_ajax_request') );
        add_action( 'wp_ajax_wpvs_delete_card_ajax_request', array($this, 'wpvs_delete_card_ajax_request') );
        add_action( 'wp_ajax_wpvs_update_card_ajax_request', array($this, 'wpvs_update_card_ajax_request') );
    }

    public function wpvs_save_card_ajax_request() {
        if( ! is_user_logged_in() ) {
            $error_message = __('You must be logged in.', 'vimeo-sync-memberships');
            rvs_exit_ajax($error_message, 400);
        }
        $stripe_customer_id = $this->customer_id();
        if( ! empty($stripe_customer_id) && isset($_POST["card_id"]) ) {
            $payment_method_id = $_POST["card_id"];
            $card_month = $_POST['card_month'];
            $card_year = $_POST['card_year'];
            try {
                $this->config_request();
                $check_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                if( $check_payment_method->customer != $stripe_customer_id ) {
                    $error_message = __('Incorrect card owner.', 'vimeo-sync-memberships');
                    rvs_exit_ajax($error_message, 400);
                } else {
                    $card_updates = array(
                        'card' => array(
                            'exp_month' => $card_month,
                            'exp_year'  => $card_year,
                        ),
                    );
                    try {
                        $payment_method_updated = \Stripe\PaymentMethod::update($payment_method_id, $card_updates);
                    } catch (Exception $e) {
                        $error_message = __('Error updating card.', 'vimeo-sync-memberships');
                        rvs_exit_ajax($error_message, 400);
                    }
                }
            } catch (Exception $e) {
                $error_message = __('Could not find payment method.', 'vimeo-sync-memberships');
                rvs_exit_ajax($error_message, 400);
            }
        }
        wp_die();
    }

    public function wpvs_delete_card_ajax_request() {
        if( ! is_user_logged_in() ) {
            $error_message = __('You must be logged in.', 'vimeo-sync-memberships');
            rvs_exit_ajax($error_message, 400);
        }
        $stripe_customer_id = $this->customer_id();
        if( ! empty($stripe_customer_id) && isset($_POST["card_id"]) ) {
            $payment_method_id = $_POST["card_id"];
            try {
                $this->config_request();
                $remove_payment_method = \Stripe\PaymentMethod::retrieve($payment_method_id);
                if( $remove_payment_method->customer != $stripe_customer_id ) {
                    $error_message = __('Incorrect card owner.', 'vimeo-sync-memberships');
                    rvs_exit_ajax($error_message, 400);
                } else {
                    try {
                        $remove_payment_method->detach();
                    } catch (Exception $e) {
                        $error_message = __('Error detaching card.', 'vimeo-sync-memberships');
                        rvs_exit_ajax($error_message, 400);
                    }
                }
            } catch (Exception $e) {
                $wpvs_error_message = __('Error deleting card.', 'vimeo-sync-memberships');
                rvs_exit_ajax($wpvs_error_message, 400);
            }
        }
        wp_die();
    }

    function wpvs_update_card_ajax_request() {
        if( ! is_user_logged_in() ) {
            $error_message = __('You must be logged in.', 'vimeo-sync-memberships');
            rvs_exit_ajax($error_message, 400);
        }
        $stripe_customer_id = $this->customer_id();

        if( ! empty($stripe_customer_id) && isset($_POST["card_id"]) ) {
            $payment_method_id = $_POST["card_id"];
            try {
                $this->config_request();
                $stripe_cu = \Stripe\Customer::retrieve($stripe_customer_id);
                $stripe_cu->invoice_settings->default_payment_method = $payment_method_id;
                $stripe_cu->save();
            } catch (Exception $e) {
                $wpvs_error_message = $e->getMessage();
                rvs_exit_ajax($wpvs_error_message, 400);
            }
        }
        wp_die();
    }

    function wpvs_update_membership_ajax_request() {
        if( ! is_user_logged_in() ) {
            $error_message = __('You must be logged in.', 'vimeo-sync-memberships');
            rvs_exit_ajax($error_message, 400);
        }
        global $rvs_live_mode;
        global $rvs_current_user;
        $wpvs_remove_user_membership = false;
        if( $this->test_mode ) {
            $get_memberships = 'rvs_user_memberships_test';
        } else {
            $get_memberships = 'rvs_user_memberships';
        }

        if( ! isset($_GET["planId"]) || empty($_GET["planId"]) ) {
            $wpvs_error_message = __('Missing Plan ID.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 400);
        }

        $stripe_plan_id = $_GET["planId"];
        $new_status = $_GET["new_status"];
        $rvs_mail_text = "Cancelled";
        $run_wpvs_delete_functions = false;
        $remove_membership = null;

        if( current_user_can( 'manage_options' ) && isset($_GET["userId"]) ) {
            $this->user_id = $_GET["userId"];
        }

        try {
            $this->config_request();
            $stripe_customer = $this->get_stripe_customer_details();
            if( empty($stripe_customer) ) {
                $stripe_subscription = $this->get_subscription($stripe_plan_id);
            } else {
                $stripe_subscription = $stripe_customer->subscriptions->retrieve($stripe_plan_id);
            }
            if( isset($stripe_subscription->error) && ! empty($stripe_subscription->error) ) {
                if( strpos($stripe_subscription->error, "No such subscription") !== false) {
                    $wpvs_remove_user_membership = true;
                } else {
                    rvs_exit_ajax($stripe_subscription->error, 400);
                }
            }
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            if(strpos($error_message, "No such subscription") !== false) {
                $wpvs_remove_user_membership = true;
            } else {
                rvs_exit_ajax($error_message, 400);
            }
        }

        if( $wpvs_remove_user_membership ) {
            $user_memberships = get_user_meta($this->user_id, $get_memberships, true);
            if(!empty($user_memberships)) {
                foreach($user_memberships as $key => &$access) {
                    if ($access["id"] == $stripe_plan_id) {
                        if($new_status == "delete") {
                            unset($user_memberships[$key]);
                            $run_wpvs_delete_functions = true;
                            $remove_membership = $access;
                        }
                        if($new_status == "cancel") {
                            $access["status"] = "canceled";
                        }
                        $plan_name = $access["name"];
                        break;
                    }
                }
                update_user_meta($this->user_id, $get_memberships, $user_memberships);
                if( $run_wpvs_delete_functions && ! empty($remove_membership) ) {
                    wpvs_do_after_delete_membership_action($this->user_id, $remove_membership);
                }
            }
        } else {
            if( ! empty($stripe_subscription) ) {
                try {
                    if($new_status == "delete") {
                        $stripe_subscription->delete();
                        $rvs_mail_text = "Deleted";
                    }

                    if($new_status == "cancel") {
                        $stripe_subscription->cancel_at_period_end = true;
                        $stripe_subscription->save();
                    }

                    if($new_status == "reactivate") {
                        if($stripe_subscription->cancel_at_period_end) {
                            $subscription_update = array(
                                'items' => array(
                                    array(
                                        'id'   => $stripe_subscription->items->data[0]->id,
                                        'plan' => $stripe_subscription->plan->id
                                    ),
                                ),
                            );

                            \Stripe\Subscription::update($stripe_plan_id, $subscription_update);
                            $stripe_subscription->cancel_at_period_end = false;
                            $stripe_subscription->save();
                        }
                        $rvs_mail_text = "Reactivated";
                    }
                } catch (Exception $e) {
                    $error_message = $e->getMessage();
                    if(strpos($error_message, "No such subscription") !== false) {
                        $wpvs_remove_user_membership = true;
                    } else {
                        rvs_exit_ajax($error_message, 400);
                    }
                }

                $user_memberships = get_user_meta($this->user_id, $get_memberships, true);
                if( ! empty($user_memberships) ) {
                    foreach($user_memberships as $key => &$access) {
                        if ($access["id"] == $stripe_plan_id) {
                            if($new_status == "delete") {
                                unset($user_memberships[$key]);
                                $run_wpvs_delete_functions = true;
                                $remove_membership = $access;
                            }
                            if($new_status == "cancel") {
                                $access["status"] = "canceled";
                                $access["ends_next_date"] = 1;
                            }
                            if($new_status == "reactivate") {
                                $access["status"] = "active";
                                if(isset($access["ends_next_date"]) && ! empty($access["ends_next_date"])) {
                                    $access["ends_next_date"] = 0;
                                    unset($access["ends_next_date"]);
                                }
                            }
                            $plan_name = $access["name"];
                            break;
                        }
                    }
                }

                if(empty($plan_name)) {
                    $plan_name = $stripe_plan_id;
                }
                update_user_meta($this->user_id, $get_memberships, $user_memberships);
                if( $run_wpvs_delete_functions && ! empty($remove_membership) ) {
                    wpvs_do_after_delete_membership_action($this->user_id, $remove_membership);
                }
                wpvs_send_email($rvs_mail_text, $this->user_id, $plan_name);
            }
        }

        wp_die();
    }
}
