<?php
/*
* WPVS STRIPE AJAX REQUESTS
*
*/

class WPVS_Stripe_Admin_Ajax_Manager extends WPVS_Stripe_Payments_Manager {
    public function setup_ajax_functions() {
        add_action( 'wp_ajax_wpvs_create_new_stripe_plan_ajax_request', array($this, 'wpvs_create_new_stripe_plan_ajax_request') );
        add_action( 'wp_ajax_wpvs_stripe_refund_ajax_request', array($this, 'wpvs_stripe_refund_ajax_request') );
    }

    protected function create_new_plan($stripe_plan) {
        $created_plan = array(
            'created'  => false,
            'new_plan' => null,
            'error'    => null
        );
        try {
            $this->config_request();
            $plan = \Stripe\Plan::create($stripe_plan);
            $created_plan['created'] = true;
            $created_plan['new_plan'] = $plan;
        } catch (Exception $e) {
            if($e->getMessage() == "Plan already exists.") {
                $created_plan['created'] = true;
            } else {
                $created_plan['error'] = $e->getMessage();
            }
        }
        return (object) $created_plan;
    }

    public function set_user_id($user_id) {
        $this->user_id = $user_id;
    }

    // CREATE NEW STRIPE PLAN
    public function wpvs_create_new_stripe_plan_ajax_request() {
        if( wpvs_secure_admin_ajax() ) {
            global $rvs_currency;
            $new_stripe_plan_id = null;
            $error_message = "";
            if( isset($_POST['id_of_plan']) && ! empty($_POST['id_of_plan']) )  {

                $plan_id = $_POST['id_of_plan'];
                $wpvs_membership_manager = new WPVS_Membership_Manager($plan_id);

                if( $wpvs_membership_manager->membership_plan_exists() ) {
                    $membership_plan = $wpvs_membership_manager->get_plan();
                } else {
                    $wpvs_error_message = __('You must create a plan first.', 'vimeo-sync-memberships');
                    rvs_exit_ajax($wpvs_error_message, 400);
                }

                if( ! empty($membership_plan) ) {
                    $plan_name = $membership_plan['name'];
                    $plan_amount = $membership_plan['amount'];
                    $plan_interval = $membership_plan['interval'];
                    $plan_interval_count = $membership_plan['interval_count'];
                    $trial_frequency = $membership_plan['trial_frequency'];
                    $plan_trial = intval($membership_plan['trial']);

                    if( $rvs_currency == "JPY" ) {
                        $plan_amount = number_format($plan_amount/100, 0);
                    }

                    $created_success = false;
                    $create_both_live_and_test = 2;

                    while($create_both_live_and_test > 0) {

                        if($create_both_live_and_test == 2) {
                            $this->set_api_mode('live');
                        }

                        if($create_both_live_and_test == 1) {
                            $this->set_api_mode('test');
                        }

                        $new_stripe_plan = array(
                            "amount" => $plan_amount,
                            "interval" => $plan_interval,
                            "interval_count" => $plan_interval_count,
                            "currency" => $rvs_currency,
                            "id" => $plan_id,
                            "product" =>  array(
                                "name" => $plan_name
                            )
                        );

                        if( ! empty($plan_trial) && $trial_frequency != "none" ) {
                            if($trial_frequency == "week") {
                                $plan_trial = $plan_trial*7;
                            }
                            if($trial_frequency == "month") {
                                $plan_trial = $plan_trial*30;
                            }
                            $new_stripe_plan['trial_period_days'] = $plan_trial;
                        }

                        // CREATE STRIPE PLAN
                        $plan_created = $this->create_new_plan($new_stripe_plan);

                        if( $plan_created->created ) {
                            $created_success = true;
                            if($create_both_live_and_test == 2) {
                                if( ! empty($plan_created->new_plan) ) {
                                    $new_stripe_plan_id = $plan_created->new_plan->id;
                                }
                            }
                            if( empty($new_stripe_plan_id) ) {
                                $new_stripe_plan_id = $plan_id;
                            }
                        } else {
                            if( ! empty($plan_created->error) ) {
                                $error_message .= $plan_created->error . ' | ';
                            }
                        }
                        $create_both_live_and_test--;
                    }
                }

                if($created_success) {
                    $stripe_id_added = $wpvs_membership_manager->add_stripe_plan_id($new_stripe_plan_id);
                    if( $stripe_id_added ) {
                        _e('Stripe plan successfully created!', 'vimeo-sync-memberships');
                    } else {
                        $wpvs_error_message = __('Error adding Stripe Plan ID to Membership.', 'vimeo-sync-memberships');
                        rvs_exit_ajax($wpvs_error_message, 400);
                    }
                } else {
                    rvs_exit_ajax($error_message, 400);
                }
            } else {
                $wpvs_error_message = __('Missing Stripe Plan ID.', 'vimeo-sync-memberships');
                rvs_exit_ajax($wpvs_error_message, 400);
            }
        } else {
            $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 401);
        }
        wp_die();
    }

    public function wpvs_stripe_refund_ajax_request() {
        if( wpvs_secure_admin_ajax() ) {
            if( isset($_POST["chargeId"]) && ! empty($_POST["chargeId"]) ) {
                $charge_id = $_POST["chargeId"];
                try {
                    $this->config_request();
                    $stripe_refund = \Stripe\Refund::create(array("charge" => $charge_id));
                    if( isset($stripe_refund->status) && $stripe_refund->status == "succeeded") {
                        $refunded = wpvs_do_db_refund_payment($charge_id);
                        wpvs_reverse_product_purchase($charge_id, 'stripe');
                    }
                } catch (Exception $e) {
                    rvs_exit_ajax($e->getMessage(), 400);
                }
            } else {
                $wpvs_error_message = __('No charge ID provided.', 'vimeo-sync-memberships');
                rvs_exit_ajax($wpvs_error_message, 400);
            }
        } else {
            $wpvs_error_message = __('You are restricted from doing this.', 'vimeo-sync-memberships');
            rvs_exit_ajax($wpvs_error_message, 401);
        }
        wp_die();
    }

    // UPDATE PLAN TRIAL PERIOD
    public function wpvs_update_stripe_plan($plan_id, $plan_data) {
        $plan_updated = array(
            'updated' => false,
            'error'   => '',
        );
        if( wpvs_secure_admin_ajax() ) {
            try {
                $this->config_request();
                $stripe_plan = \Stripe\Plan::update($plan_id, $plan_data);
                $plan_updated['updated'] = true;
            } catch (Exception $e) {
                $plan_updated['error'] = $e->getMessage();
            }
        } else {
            $plan_updated['error'] = __('You are restricted from doing this.', 'vimeo-sync-memberships');
        }
        return (object) $plan_updated;
    }

    public function wpvs_delete_stripe_plan($plan_id) {
        $stripe_plan_deleted = array(
            'deleted' => false,
            'error'   => '',
        );
        if( wpvs_secure_admin_ajax() ) {
            $stripe_product = "";
            try {
                $this->config_request();
                $stripe_plan = \Stripe\Plan::retrieve($plan_id);
                if( isset($stripe_plan->product) ) {
                    $stripe_product = $stripe_plan->product;
                }
                $plan_deleted = $stripe_plan->delete();
                if($plan_deleted->deleted && ! empty($stripe_product)) {
                    try {
                        $product = \Stripe\Product::retrieve($stripe_product);
                        $product->delete();
                        $stripe_plan_deleted['deleted'] = true;
                    } catch (Exception $e) {
                        $stripe_plan_deleted['error'] .= $e->getMessage() . '</br>';
                    }
                }
            } catch (Exception $e) {
                $stripe_plan_deleted['error'] .= $e->getMessage() . '</br>';
            }
        }
        return (object) $stripe_plan_deleted;
    }

    public function wpvs_stripe_get_list_of_charges($starting_after, $customer_id) {
        $collect_payments = array();
        $has_more_charges = false;
        $last_charge = null;
        $stripe_charges = null;
        if( wpvs_secure_admin_ajax() ) {
            $wpvs_stripe_charges_args = array(
                'limit' => 100,
            );

            // IF STARTING AFTER CHARGE IS SET
            if( ! empty($starting_after) ) {
                $wpvs_stripe_charges_args['starting_after'] = $starting_after;
            }

            // CHECK IF CUSTOMER IS SET
            if( ! empty($customer_id) ) {
                $wpvs_stripe_charges_args['customer'] = $customer_id;
            }

            try {
                $this->config_request();
                $stripe_data = \Stripe\Charge::all($wpvs_stripe_charges_args);
                $stripe_charges = $stripe_data->data;
                $has_more_charges =  $stripe_data->has_more;
            } catch (Exception $e) {
                $stripe_customer_import['error'] .= $e->getMessage() . ' ';
            }

            if( ! empty($stripe_charges) ) {

                foreach($stripe_charges as $charge) {
                    $charge_time = $charge->created;
                    $user_id = null;
                    $charge_type = null;
                    $charge_product = null;
                    $coupon_code = null;
                    $user = wpvs_get_stripe_customer($charge->customer);
                    if( ! empty($user) ) {
                        $user_id = $user->ID;
                    }

                    if( ! empty($user_id) ) {
                        if( isset($charge->metadata) ) {
                            $charge_meta = $charge->metadata;
                            if( isset($charge_meta->type) && ! empty($charge_meta->type) ) {
                                $charge_type = $charge_meta->type;
                            }
                            if( isset($charge_meta->productid) && ! empty($charge_meta->productid) ) {
                                $charge_product = $charge_meta->productid;
                            }
                        }

                        if( isset($charge->invoice) && ! empty($charge->invoice) ) {
                            $invoice_data = array(
                                'id' => $charge->invoice,
                                'expand' => ['discount'],
                            );
                            $charge_invoice = \Stripe\Invoice::retrieve($invoice_data);
                            if( isset($charge_invoice->discount) && ! empty($charge_invoice->discount->coupon->id) ) {
                                $coupon_code = $charge_invoice->discount->coupon->id;
                            }

                            if( empty($charge_type) && isset($charge_invoice->billing_reason) && $charge_invoice->billing_reason == "subscription_cycle" ) {
                                $charge_type = "subscription";
                                $charge_product = 0;
                            }
                        }

                        if( ! empty($charge_type) ) {
                            $charge_details = array(
                                'charge_time' => $charge_time,
                                'charge_user_id' => $user_id,
                                'charge_customer' => $charge->customer,
                                'charge_amount' => $charge->amount,
                                'charge_id' => $charge->id,
                                'coupon_code' => $coupon_code,
                                'charge_type' => $charge_type,
                                'charge_product' => $charge_product,
                                'charge_refunded' => $charge->refunded,
                                'test_mode' => $this->test_mode,
                                'status' => $charge->status
                            );
                            $collect_payments[] = $charge_details;
                        }
                    }
                }
                $last_charge_object = $stripe_charges[count($stripe_charges) - 1];
                $last_charge = $last_charge_object->id;

            }
        }
        return array( 'charges' => $collect_payments, 'has_more' => $has_more_charges, 'last_charge' => $last_charge, 'error' => $wpvs_stripe_error );
    }

    public function wpvs_stripe_import_customers($starting_after) {
        $stripe_customer_import = array(
            'has_more_customers' => false,
            'last_customer'      => null,
            'error'              => '',
        );
        $customer_count = 0;
        if( wpvs_secure_admin_ajax() ) {
            global $rvs_live_mode;
            $test_mode = false;
            if($rvs_live_mode == "off") {
                $test_mode = true;
                $new_member_role = 'wpvs_test_member';
                $customer_key = 'rvs_stripe_customer_test';
            } else {
                $secret_key = $wpvs_stripe_options['live_secret_key'];
                $new_member_role = 'wpvs_member';
                $customer_key = 'rvs_stripe_customer';
            }

            $wpvs_stripe_customer_args = array(
                'limit' => 100,
            );

            // CHECK IF STARTING AFTER CUSTOMER ID IS SET

            if( ! empty($starting_after) ) {
                $wpvs_stripe_customer_args['starting_after'] = $starting_after;
            }

            try {
                $this->config_request();
                $stripe_data = \Stripe\Customer::all($wpvs_stripe_customer_args);
                $stripe_customers = $stripe_data->data;
                $stripe_customer_import['has_more_customers'] =  $stripe_data->has_more;
            } catch (Exception $e) {
                $stripe_customer_import['error'] .= $e->getMessage() . ' ';
            }

            if( ! empty($stripe_customers) ) {
                $wpvs_memberships = get_option('rvs_membership_list');
                foreach($stripe_customers as $customer) {
                    $user_id = null;
                    $user = wpvs_get_stripe_customer($customer->id);
                    if( empty($user) && isset($customer->email) && ! empty($customer->email) ) {
                        $user = get_user_by('email', $customer->email); // check if user already exists with email
                    }

                    if( empty($user) ) {
                        $new_customer_email = $customer->email;
                        $new_password = wp_generate_password();
                        $new_user_data = array(
                            'user_login' => $new_customer_email,
                            'user_email' => $new_customer_email,
                            'user_pass' => $new_password,
                            'role' => $new_member_role
                        );
                        $new_user_id = wp_insert_user( $new_user_data );

                        if ( ! is_wp_error( $new_user_id ) ) {
                            $customer->metadata->userid = $new_user_id;
                            $customer->save();
                            $customer_count++;
                            $user_id = $new_user_id;
                            $user = get_user_by('id', $user_id);
                        }
                    } else {
                        if( ! isset($customer->metadata->userid) ) {
                            $customer->metadata->userid = $user->ID;
                            $customer->save();
                        }
                        $user_id = $user->ID;
                    }
                    if( ! empty($user_id) ) {
                        update_user_meta( $user_id, $customer_key, $customer->id);
                        if( is_multisite() ) {
                            $blog_id = get_current_blog_id();
                            if( ! is_user_member_of_blog( $user_id, $blog_id ) ) {
                                add_user_to_blog($blog_id, $user_id, $new_member_role);
                            }
                        }

                        if( isset($customer->subscriptions) && isset($customer->subscriptions->data) && ! empty($customer->subscriptions->data) ) {
                            $wpvs_customer = new WPVS_Customer($user);
                            $customer_subscriptions = $customer->subscriptions->data;
                            foreach($customer_subscriptions as $subscription) {
                                $stripe_plan_found = false;
                                $stripe_plan = null;
                                $stripe_product = null;
                                $plan_id = null;
                                if( ! empty($wpvs_memberships) ) {
                                    foreach($wpvs_memberships as $plan) {
                                        if($plan['id'] == $subscription->plan->id) {
                                            $plan_name = $plan['name'];
                                            $stripe_plan_found = true;
                                            break;
                                        }
                                    }
                                }
                                if( ! $stripe_plan_found && ! empty($subscription->plan->id) ) {
                                    try {
                                        $plan_id = $subscription->plan->id;
                                        $stripe_plan = \Stripe\Plan::retrieve($plan_id);
                                        if( isset($stripe_plan->product) && ! empty($stripe_plan->product) ) {
                                            try {
                                                $stripe_product = \Stripe\Product::retrieve($stripe_plan->product);
                                            } catch (Exception $e) {
                                                $stripe_customer_import['error'] .= $e->getMessage() . ' ';
                                            }
                                        }
                                    } catch (Exception $e) {
                                        $plan_id = null;
                                        $stripe_error_message = $e->getMessage();
                                        if( ! empty($stripe_error_message) && strpos($stripe_error_message, 'No such plan') !== FALSE ) {
                                            continue;
                                        } else {
                                            $stripe_customer_import['error'] .= $e->getMessage() . ' ';
                                        }
                                    }
                                }

                                if( ! empty($plan_id) && ! empty($stripe_plan) && ! empty($stripe_product) ) {
                                    $wpvs_membership_manager = new WPVS_Membership_Manager($plan_id);

                                    if( ! $wpvs_membership_manager->membership_plan_exists() ) {
                                        $plan_name = $stripe_product->name;
                                        $plan_description = $plan_name;
                                        $plan_short_description = $plan_name;

                                        $plan_amount = $stripe_plan->amount;
                                        $plan_interval = $stripe_plan->interval;
                                        $plan_interval_count = $stripe_plan->interval_count;
                                        if( isset($stripe_plan->trial_period_days) && ! empty($stripe_plan->trial_period_days) ) {
                                            $plan_trial_frequency = 'days';
                                            $plan_trial_frequency_interval = intval($stripe_plan->trial_period_days);
                                        } else {
                                            $plan_trial_frequency = 0;
                                            $plan_trial_frequency_interval = 0;
                                        }
                                        $hide_membership = 0;
                                        $create_role = false;

                                        $membership_created = $wpvs_membership_manager->create_new_membership_plan(
                                            $plan_name,
                                            $plan_amount,
                                            $plan_description,
                                            $plan_short_description,
                                            $plan_interval,
                                            $plan_interval_count,
                                            $plan_trial_frequency,
                                            $plan_trial_frequency_interval,
                                            $hide_membership,
                                            $create_role
                                        );
                                        if( $membership_created ) {
                                            $wpvs_membership_manager->new_init();
                                            $wpvs_membership_manager->add_stripe_plan_id($plan_id);
                                            $stripe_plan_found = true;
                                        }
                                    }
                                }

                                if( $stripe_plan_found && ! $wpvs_customer->has_membership($subscription->plan->id) ) {
                                    $stripe_key = "stripe";
                                    if( ! $subscription->livemode ) {
                                        $stripe_key = "stripe_test";
                                    }

                                    $add_new_membership = array(
                                        'plan' => $subscription->plan->id,
                                        'id' => $subscription->id,
                                        'amount' => $subscription->plan->amount,
                                        'name' => $plan_name,
                                        'interval' => $subscription->plan->interval,
                                        'interval_count' => $subscription->plan->interval_count,
                                        'ends' => $subscription->current_period_end,
                                        'status' => $subscription->status,
                                        'type' => $stripe_key
                                    );

                                    if( isset($subscription->discount) && $subscription->discount != null && isset($subscription->discount->coupon) ) {
                                        $add_new_membership['discount'] = $subscription->discount->coupon;
                                    }
                                    $wpvs_customer->add_membership($add_new_membership);
                                }
                                if( ! empty($wpvs_membership_manager) ) {
                                    unset($wpvs_membership_manager);
                                }
                            }
                            wpvs_add_new_member($user_id, $test_mode);
                        }
                    }
                }

                $last_customer_object = $stripe_customers[count($stripe_customers) - 1];
                $stripe_customer_import['last_customer'] = $last_customer_object->id;
            }
        }
        $stripe_customer_import['customer_count'] = $customer_count;
        return (object) $stripe_customer_import;
    }

    public function create_stripe_coupon_code($coupon_data) {
        $coupon_code_created = array(
            'created' => false,
            'error'   => '',
        );
        try {
            $this->config_request();
            $new_coupon = \Stripe\Coupon::create($coupon_data);
            if( ! empty($new_coupon) ) {
                $coupon_code_created['created'] = true;
            }
        } catch (Exception $e) {
            $coupon_code_created['error'] = $e->getMessage();
        }
        return (object) $coupon_code_created;
    }

    public function delete_coupon_code($coupon_id) {
        try {
            $this->config_request();
            $stripe_coupon = \Stripe\Coupon::retrieve($coupon_id);
            $stripe_coupon->delete();
        } catch (Exception $e) {
        }
    }

    public function create_tax_rate($tax_rate) {
        $created_tax_rate = array(
            'created'  => false,
            'error'    => null
        );
        if( isset($tax_rate['country']) ) {
            unset($tax_rate['country']);
        }
        try {
            $this->config_request();
            $new_tax_rate = \Stripe\TaxRate::create($tax_rate);
            $created_tax_rate['created'] = true;
            $created_tax_rate['new_tax_rate'] = $new_tax_rate;
        } catch (Exception $e) {
            $created_tax_rate['error'] = $e->getMessage();
        }
        return (object) $created_tax_rate;
    }

    public function update_tax_rate($tax_rate_id, $tax_rate_updates) {
        $updated_tax_rate = array(
            'updated'  => false,
            'error'    => null
        );
        try {
            $this->config_request();
            $stripe_tax_rate = \Stripe\TaxRate::update($tax_rate_id, $tax_rate_updates);
            $updated_tax_rate['updated'] = true;
        } catch (Exception $e) {
            $updated_tax_rate['error'] = $e->getMessage();
        }
        return (object) $updated_tax_rate;
    }
}

if( ! function_exists('wpvs_config_stripe_admin_ajax_requests') ) {
    function wpvs_config_stripe_admin_ajax_requests() {
        if( current_user_can('manage_options') ) {
            global $rvs_current_user;
            global $wpvs_stripe_admin_ajax_requests;
            $wpvs_stripe_admin_ajax_requests = new WPVS_Stripe_Admin_Ajax_Manager( $rvs_current_user->ID );
            $wpvs_stripe_admin_ajax_requests->setup_ajax_functions();
        }
    }
    add_action('admin_init', 'wpvs_config_stripe_admin_ajax_requests');
}
