<?php

function wpvs_get_customer_invoices_ajax_request() {
    require_once(RVS_MEMBERS_BASE_DIR . '/stripe/init.php');
    if( ! is_user_logged_in() ) {
        $error_message = __('You must be logged in.', 'vimeo-sync-memberships');
        rvs_exit_ajax($error_message, 400);
    }
    global $rvs_current_user;
    global $rvs_currency;
    $customer_stripe_payments = null;
    $charge_id = null;
    $wpvs_stripe_payments_manager = new WPVS_Stripe_Payments_Manager($rvs_current_user->ID);

    if( isset($_REQUEST["chargeId"]) && ! empty($_REQUEST["chargeId"] ) ) {
        $charge_id = $_REQUEST["chargeId"];
    }

    $customer_stripe_payments = $wpvs_stripe_payments_manager->get_customer_invoices($charge_id);

    if( isset($customer_stripe_payments['error']) ) {
        $error_message = $customer_stripe_payments['error'];
        rvs_exit_ajax($error_message, 400);
    } else {
        if( ! empty($customer_stripe_payments) ) {
            $allPayments = array();
            foreach($customer_stripe_payments as $charge) {
                $paymentDate = wp_date( 'M d, Y',$charge->created );
                $memberPayment = array(
                    'charge_id' => $charge->id,
                    'charge_amount' => ($charge->amount)/100,
                    'charge_date' => $paymentDate,
                    'charge_refund' => $charge->refunded,
                    'charge_status' => $charge->status,
                    'charge_invoice' => $charge->invoice
                );
                $allPayments[] = $memberPayment;
            }
            echo json_encode($allPayments);
        } else {
            echo null;
        }
    }

    wp_die();
}

add_action( 'wp_ajax_wpvs_get_customer_invoices_ajax_request', 'wpvs_get_customer_invoices_ajax_request' );

function wpvs_get_customer_single_invoice() {
    global $rvs_live_mode;
    global $rvs_current_user;
    global $rvs_currency;

    if( isset($_GET["invoice_id"]) && ! empty($_GET["invoice_id"]) ) {
        $wpvs_stripe_payments_manager = new WPVS_Stripe_Payments_Manager($rvs_current_user->ID);
        $stripe_invoice_id = $_GET["invoice_id"];
        $stripe_invoice = $wpvs_stripe_payments_manager->get_invoice(array('id' => $stripe_invoice_id));
        if( isset($stripe_invoice['error']) ) {
            rvs_exit_ajax($stripe_invoice['error'], 400);
        }
        if( isset($stripe_invoice->invoice_pdf) ) {
            echo $stripe_invoice->invoice_pdf;
        }
    } else {
        rvs_exit_ajax('Missing invoice ID', 400);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_get_customer_single_invoice', 'wpvs_get_customer_single_invoice' );

function wpvs_add_new_card_ajax_request() {
    if( ! is_user_logged_in() ) {
        $error_message = __('You must be logged in.', 'vimeo-sync-memberships');
        rvs_exit_ajax($error_message, 400);
    }
    global $rvs_current_user;
    $payment_method_added = array();
    $wpvs_customer = new WPVS_Customer($rvs_current_user);
    $stripe_customer_id = $wpvs_customer->get_stripe_customer_id();

    if(isset($_POST["payment_method_id"])) {
        $payment_method_id = $_POST["payment_method_id"];
        $stripe_payments_manager = new WPVS_Stripe_Payments_Manager($rvs_current_user->ID);
        if( ! empty($stripe_customer_id) ) {
            $payment_method_added = $stripe_payments_manager->add_payment_method_to_customer($payment_method_id);
        } else {
            $new_customer_details = $new_customer_details = wpvs_default_new_stripe_customer_details($rvs_current_user);
            $new_customer_details["payment_method"] = $payment_method_id;
            $new_customer_details["invoice_settings"]["default_payment_method"] = $payment_method_id;
            $payment_method_added = $stripe_payments_manager->add_new_customer($new_customer_details);
        }
    }
    if( isset($payment_method_added['error']) ) {
        $wpvs_error_message = $payment_method_added['error'];
        rvs_exit_ajax($wpvs_error_message, $payment_method_added['code']);
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_add_new_card_ajax_request', 'wpvs_add_new_card_ajax_request' );

function wpvs_get_stripe_subscription_payment_url() {
    if( ! is_user_logged_in() ) {
        $error_message = __('You must be logged in.', 'vimeo-sync-memberships');
        rvs_exit_ajax($error_message, 400);
    }
    global $rvs_current_user;
    $subscription_data = array(
        "status" => "overdue",
        "payment_url" => null
    );

    if(isset($_POST["subscription_id"])) {
        $subscription_id = $_POST["subscription_id"];
        $stripe_payments_manager = new WPVS_Stripe_Payments_Manager($rvs_current_user->ID);
        $subscription = $stripe_payments_manager->get_subscription($subscription_id);
        if( isset($subscription->latest_invoice) ) {
            $subscription_data["status"] = $subscription->status;
            $invoice = $stripe_payments_manager->get_invoice(array('id' => $subscription->latest_invoice));
            if( isset($invoice->hosted_invoice_url) ) {
                $subscription_data["payment_url"] = $invoice->hosted_invoice_url;
            }
        }
    }
    if( $subscription_data["payment_url"] == null ) {
        $error_message = __('Could not retrieve payment url.', 'vimeo-sync-memberships');
        rvs_exit_ajax($error_message, 400);
    } else {
        echo json_encode($subscription_data);
    }
    wp_die();
}
add_action( 'wp_ajax_wpvs_get_stripe_subscription_payment_url', 'wpvs_get_stripe_subscription_payment_url' );
