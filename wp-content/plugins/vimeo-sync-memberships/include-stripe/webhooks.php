<?php

/*
* WPVS Stripe Webhooks Class
*
*/

class WPVS_STRIPE_WEBHOOKS_MANAGER extends WPVS_Stripe_Payments_Manager {

    public $test_mode;
    protected $webhook_secret;

    public function __construct() {
        parent::__construct(null);
        add_action('wp_loaded', array($this, 'wpvs_stripe_event_listener'));
        add_action( 'wp_ajax_create_wpvs_webhook_endpoints', array($this, 'create_wpvs_webhook_endpoints') );
    }

    private function verify_event($payload, $sig_header) {
        global $wpvs_stripe_options;
        if( $this->test_mode ) {
            if( isset($wpvs_stripe_options['test_webhook_secret']) && ! empty($wpvs_stripe_options['test_webhook_secret']) ) {
                $this->webhook_secret = $wpvs_stripe_options['test_webhook_secret'];
            }
        } else {
            if( isset($wpvs_stripe_options['webhook_secret']) && ! empty($wpvs_stripe_options['webhook_secret']) ) {
                $this->webhook_secret = $wpvs_stripe_options['webhook_secret'];
            }
        }
        $verified_event_object = array(
            'event' => null,
            'error' => '',
        );
        if( ! empty($this->webhook_secret) ) {
            try {
                $this->config_request();
                $con_event = \Stripe\Webhook::constructEvent($payload, $sig_header, $this->webhook_secret);
                $verified_event_object['event'] = $con_event;
            } catch(\UnexpectedValueException $e) {
                $verified_event_object['error'] = $e->getMessage();
            } catch(\Stripe\Exception\SignatureVerificationException $e) {
                $verified_event_object['error'] = $e->getMessage();
            }
        } else {
            $verified_event_object['error'] = __('Missing webhook secret', 'vimeo-sync-memberships');
        }
        return (object) $verified_event_object;
    }

    public function wpvs_stripe_event_listener() {
        if(isset($_GET['wpvs-stripe-event-listener']) && $_GET['wpvs-stripe-event-listener'] == 'stripe') {
            $user_found = false;
            $event_json = null;
            $event = null;
            $this->user_id = null;
            /*
            * DO NOT UNCOMMENT THIS IS FOR TESTING PURPOSES
            * $event_payload = @file_get_contents(ABSPATH.'/stripe-json/stripe-checkout-complete.json');
            *
            */
            $event_payload = @file_get_contents('php://input');
            $event_json = json_decode($event_payload);
            $live_mode = $event_json->livemode;
            if($live_mode) {
                $this->set_api_mode('live');
                $stripe_key = "stripe";
            } else {
                $this->set_api_mode('test');
                $stripe_key = "stripe_test";
            }
            if( isset($_SERVER['HTTP_STRIPE_SIGNATURE']) ) {
                $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
                if( ! empty($event_json) && isset($event_json->id)) {
                    $live_mode = $event_json->livemode;
                    if($live_mode) {
                        $this->set_api_mode('live');
                        $stripe_key = "stripe";
                    } else {
                        $this->set_api_mode('test');
                        $stripe_key = "stripe_test";
                    }
                    $verified_event = $this->verify_event($event_payload, $sig_header);
                    if( ! empty($verified_event->event) && empty($verified_event->error) ) {
                        $event = $verified_event->event;
                    } else {
                        http_response_code(400);
                        echo wp_json_encode(array('stripe_webhook_error' => $verified_event->error));
                        exit;
                    }
                }
            } else {
                $event = $event_json;
            }

            if( ! empty($event) ) {
                try {
                    $event_time = null;
                    $wpvs_user = null;
                    $customer_meta = null;
                    $event_object = $event->data->object;
                    if( isset($event_object->created) ) {
                        $event_time = $event_object->created;
                    }
                    if( empty($event_time) && isset($event->created) ) {
                        $event_time = $event->created;
                    }
                    if( empty($event_time) ) {
                        $event_time = current_time('timestamp');
                    }
                    if($live_mode) {
                        $customer_key = 'rvs_stripe_customer';
                    } else {
                        $customer_key = 'rvs_stripe_customer_test';
                    }

                    // SET STRIPE CUSTOMER
                    $stripe_customer = $this->get_stripe_customer_by_id($event_object->customer);
                    if( isset($stripe_customer->metadata) ) {
                        $customer_meta = $stripe_customer->metadata;
                        if($customer_meta && isset($customer_meta->userid)) {
                            $stripe_customer_id = get_user_meta(intval($customer_meta->userid), $customer_key, true);
                            if( $stripe_customer_id == $stripe_customer->id) {
                                $this->set_user_id($customer_meta->userid);
                            }
                        }
                    }

                    if( empty($this->user_id) && isset($event_object->customer) && ! empty($event_object->customer) ) {
                        $user_id = wpvs_get_stripe_customer($event_object->customer);
                        $this->set_user_id($user_id);
                    }

                    if( empty($this->user_id) && isset($event_object->customer_email) && ! empty($event_object->customer_email) ) {
                        $find_user = get_user_by('email', $event_object->customer_email);
                        $this->set_user_id($find_user->ID);
                    }

                    if( empty($this->user_id) && isset($event_object->billing_details) && isset($event_object->billing_details->email) ) {
                        $find_user = get_user_by('email', $event_object->billing_details->email);
                        $this->set_user_id($find_user->ID);
                    }

                    if( empty($this->user_id) ) {
                        if( isset($event_object->client_reference_id) && ! empty($event_object->client_reference_id) ) {
                            $this->set_user_id($event_object->client_reference_id);
                        }

                        if( ! empty($this->user_id) ) {
                            update_user_meta($this->user_id, $customer_key, $event_object->customer);
                            $wpvs_user = get_user_by('id', $this->user_id);
                            $customer_details = wpvs_default_new_stripe_customer_details($wpvs_user);
                            $this->update_customer($customer_details);
                        }
                    }

                    if( empty($wpvs_user) && ! empty($this->user_id) ) {
                        $wpvs_user = get_user_by('id', $this->user_id);
                    }

                    if( ! empty($this->user_id) ) {
                        $user_found = true;
                    }

                    // CHECK EVENT COMPLETE
                    if($event->type == 'checkout.session.completed') {
                        /* Set Vars */
                        $checkout_id = $event_object->id;
                        if($event_object->mode == 'subscription' && isset($event_object->subscription) && ! empty($event_object->subscription) ) {
                            $subscription = $this->get_subscription($event_object->subscription);
                            $wpvs_customer = new WPVS_Customer($wpvs_user);
                            if( ! $wpvs_customer->has_membership($subscription->plan->id) ) {
                                $wpvs_membership = new WPVS_Membership_Plan($subscription->plan->id);
                                $coupon_code = null;
                                $add_new_membership = array(
                                    'plan' => $subscription->plan->id,
                                    'id' => $subscription->id,
                                    'amount' => $subscription->plan->amount,
                                    'name' => $wpvs_membership->name,
                                    'interval' => $subscription->plan->interval,
                                    'interval_count' => $subscription->plan->interval_count,
                                    'ends' => $subscription->current_period_end,
                                    'status' => $subscription->status,
                                    'type' => $stripe_key
                                );

                                if($subscription->discount != null) {
                                    $add_new_membership['discount'] = $subscription->discount->coupon;
                                    $coupon_code = $subscription->discount->coupon;
                                }

                                $wpvs_customer->add_membership($add_new_membership);

                                if( $wpvs_membership->has_trial_period() && ! wpvs_user_has_completed_trial($this->user_id, $subscription->plan->id) ) {
                                    wpvs_user_completed_trial($this->user_id, $subscription->plan->id);
                                }

                                wpvs_add_new_member($this->user_id, $this->test_mode);
                                if( ! empty($coupon_code) ) {
                                    wpvs_add_coupon_code_use($coupon_code, $this->user_id);
                                }
                            }
                        }

                        if($event_object->mode == 'payment' ) {
                            $wpvs_customer = new WPVS_Customer($wpvs_user);
                            $payment_intent = $this->get_payment_intent($event_object->payment_intent, false);
                            $payment_intent_meta = $payment_intent->metadata;
                            $product_id = $payment_intent_meta->product_id;
                            $purchase_type = $payment_intent_meta->purchase_type;
                            $wpvs_customer->add_product($product_id, $purchase_type);
                            wpvs_add_new_member($this->user_id, $this->test_mode);
                        }
                        if( empty($this->customer_id()) && isset($event_object->customer) ) {
                            $this->update_customer_id($event_object->customer);
                        }
                        $this->delete_checkout_session($checkout_id);
                    }

                    if($event->type == 'charge.succeeded') {
                        /* Set Vars */
                        $charge_id = $event_object->id;
                        $charge_amount = $event_object->amount;
                        $refunded = $event_object->refunded;
                        $charge_type = null;
                        $charge_product = null;
                        $coupon_code = null;

                        if( isset($event_object->metadata) ) {
                            $charge_meta = $event_object->metadata;
                            if( isset($charge_meta->type) && ! empty($charge_meta->type) ) {
                                $charge_type = $charge_meta->type;
                            }
                            if( isset($charge_meta->productid) && ! empty($charge_meta->productid) ) {
                                $charge_product = $charge_meta->productid;
                            }
                        }

                        if( isset($event_object->invoice) && ! empty($event_object->invoice) ) {
                            $invoice_data = array(
                                'id' => $event_object->invoice,
                                'expand' => ['discount'],
                            );
                            $charge_invoice = $this->get_invoice($invoice_data);
                            if( isset($charge_invoice->discount) && ! empty($charge_invoice->discount->coupon->id) ) {
                                $coupon_code = $charge_invoice->discount->coupon->id;
                            }

                            if( empty($charge_type) && isset($charge_invoice->billing_reason) && ($charge_invoice->billing_reason == "subscription_cycle" || $charge_invoice->billing_reason == "subscription_create") ) {
                                $charge_type = "subscription";
                                $charge_product = 0;
                            }

                            /* apply taxes
                            *
                            $wpvs_customer = new WPVS_Customer($wpvs_user);
                            $applied_taxes = array();
                            if( ! empty($wpvs_customer->billing_state) && ! empty($wpvs_customer->billing_country) ) {
                				$wpvs_tax_rate = new WPVS_Tax_Rate();
                                $jurisdiction_rates = $wpvs_tax_rate->get_tax_rates_by_jurisdiction($wpvs_customer->billing_state, $wpvs_customer->billing_country);
                                if( ! empty($jurisdiction_rates) ) {
                                    foreach($jurisdiction_rates as $tax_rate) {
                						$tax_rate_meta = json_decode($tax_rate->meta, false);
                                        if( $this->test_mode &&  isset($tax_rate_meta->test_stripe_tax_rate_id) ) {
                                            $applied_taxes[] = $tax_rate_meta->test_stripe_tax_rate_id;
                                        }
                						if( ! $this->test_mode &&  isset($tax_rate_meta->stripe_tax_rate_id) ) {
                                            $applied_taxes[] = $tax_rate_meta->stripe_tax_rate_id;
                                        }
                                    }
                                }
                            }
                            if( ! empty($applied_taxes) ) {
                				$update_invoice_tax_rates["default_tax_rates"] = $applied_taxes;
                                $this->update_invoice($charge_invoice->id, $update_invoice_tax_rates);
                			}
                            *
                            */
                        }

                        if( ! empty($this->user_id) ) {
                            $wpvs_payment_manager = new WPVS_Payment_Manager($charge_id, $this->test_mode);
                            $wpvs_payment_manager->add_new_payment($event_time, 'stripe', $this->user_id, $stripe_customer->id, $charge_amount, $charge_type, $charge_product, $refunded, $coupon_code);
                        }

                    }

                    if($event->type == 'customer.subscription.updated' || $event->type == 'customer.subscription.deleted') {
                        $plan_id = $event_object->plan->id;
                        $subscription_status = $event_object->status;
                        $new_renewal_date = $event_object->current_period_end;
                        $cancel_at_period_end = $event_object->cancel_at_period_end;
                        if( ! empty($this->user_id) ) {
                            rvs_update_user_membership_status($this->user_id, $plan_id, $subscription_status, $new_renewal_date, $live_mode, $cancel_at_period_end);
                        }
                    }

                } catch (Exception $e) {
                    http_response_code(400);
                    echo wp_json_encode(array('stripe_webhook_error' => $e->getMessage()));
                    exit;
                }
            } else {
                http_response_code(400);
                echo wp_json_encode(array('stripe_webhook_error' => __('Missing event object', 'vimeo-sync-memberships')));
                exit;
            }

            if( ! empty($event) && isset($event->type) ) {
                echo wp_json_encode(array('userupdated' => $user_found, 'event' => $event->type));
            }
            exit;
        }
    }

    private function clean_wpvs_webhook_endpoints() {
        $wpvs_webhooks_error = null;
        $wpvs_check_url = home_url('?wpvs-stripe-event-listener=stripe');
        try {
            if( ! empty($this->secret_key) ) {
                $this->config_request();
                $get_webhook_endpoints = \Stripe\WebhookEndpoint::all(array('limit' => 100));
                if( ! empty($get_webhook_endpoints->data) ) {
                    foreach($get_webhook_endpoints->data as $endpoint) {
                        if( $endpoint->url == $wpvs_check_url ) {
                            $webhook_endpoint = \Stripe\WebhookEndpoint::retrieve($endpoint->id);
                            $webhook_endpoint->delete();
                            break;
                        }
                    }
                }
            } else {
                $wpvs_webhooks_error = __('Missing Stripe Key to setup Webhook Events', 'vimeo-sync-memberships');
            }
        } catch (Exception $e) {
            $wpvs_webhooks_error .= $e->getMessage();
        }
        return $wpvs_webhooks_error;
    }

    public function create_wpvs_webhook_endpoints() {
        global $wpvs_stripe_options;
        $wpvs_webhook_params = array(
            'url' => home_url('?wpvs-stripe-event-listener=stripe'),
            'enabled_events' => array(
                'charge.succeeded',
                'checkout.session.completed',
                'customer.subscription.updated',
                'customer.subscription.deleted',
            ),
        );
        $create_both_live_and_test = 2;
        while($create_both_live_and_test > 0) {
            if($create_both_live_and_test == 2) {
                $this->set_api_mode('test');
            }

            if($create_both_live_and_test == 1) {
                $this->set_api_mode('live');
            }
            $webhooks_cleaned_error = $this->clean_wpvs_webhook_endpoints();
            if( empty($webhooks_cleaned_error) ) {
                try {
                    if( ! empty($this->secret_key) ) {
                        $this->config_request();
                        $stripe_webhook = \Stripe\WebhookEndpoint::create($wpvs_webhook_params);
                        if( $this->test_mode ) {
                            $wpvs_stripe_options['test_webhook_id'] = $stripe_webhook->id;
                            $wpvs_stripe_options['test_webhook_secret'] = $stripe_webhook->secret;
                        } else {
                            $wpvs_stripe_options['webhook_id'] = $stripe_webhook->id;
                            $wpvs_stripe_options['webhook_secret'] = $stripe_webhook->secret;
                        }

                        update_option('wpvs_stripe_settings', $wpvs_stripe_options);
                    } else {
                        rvs_exit_ajax(__('Missing Stripe Key to setup Webhook Events', 'vimeo-sync-memberships'), 400);
                    }
                } catch (Exception $e) {
                    rvs_exit_ajax($e->getMessage(), 401);
                }
            } else {
                rvs_exit_ajax($webhooks_cleaned_error, 401);
            }
            $create_both_live_and_test--;
        }
        wp_die();
    }
}
$wpvs_stripe_webhook_manager = new WPVS_STRIPE_WEBHOOKS_MANAGER();
