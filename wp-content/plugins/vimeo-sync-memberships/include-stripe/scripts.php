<?php

function rvs_load_stripe_scripts() {
	global $wpvs_stripe_options;
    global $rvs_live_mode;
    global $rvs_currency;
    global $rvs_current_user;
    global $rvs_current_version;
    global $wpvs_google_apple_enabled;
	global $wpvs_stripe_checkout_enabled;
    global $wpvs_include_checkout_scripts;
    $wpvs_card_form_color = get_option('rvs_card_form_color', '#444444');
	$stripe_publishable_key = '';
	$verify_zip = false;
    if(!isset($wpvs_stripe_options['country_code'])) {
        $wpvs_country = 'US';
    } else {
        $wpvs_country = $wpvs_stripe_options['country_code'];
    }

    if(isset($wpvs_stripe_options['verify_zip'])) {
        $verify_zip = true;
    }
    $wpvs_user_account_page = esc_attr( get_option('rvs_account_page'));
	if($rvs_live_mode == "off") {
		$stripe_publishable_key = $wpvs_stripe_options['test_publishable_key'];
	} else {
		$stripe_publishable_key = $wpvs_stripe_options['live_publishable_key'];
	}
	wp_register_script( 'wpvs-stripe-checkout', RVS_MEMBERS_STRIPE_URL .'js/stripe-checkout.js', array('wpvs-stripe'), $rvs_current_version, true);
    wp_enqueue_script( 'wpvs-stripe-js', RVS_MEMBERS_BASE_URL .'js/wpvs-stripe.js', array('wpvs-stripe'), $rvs_current_version, true);
    if($wpvs_include_checkout_scripts) {
        global $wpvs_stripe_customer;
        wp_enqueue_style('wpvs-stripe-forms');
        if($rvs_current_user) {
			$stripe_payments_manager = new WPVS_Stripe_Payments_Manager($rvs_current_user->ID);
            $wpvs_stripe_customer = $stripe_payments_manager->get_stripe_customer_details();
        } else {
            $wpvs_stripe_customer = null;
        }
        wp_enqueue_script('wpvs-stripe', 'https://js.stripe.com/v3/', array('jquery'),'',true);
        $stripe_scripts_added = false;
        if(isset($wpvs_stripe_options['billing_address'])) {
            $billing_address = true;
        } else {
            $billing_address = false;
        }

        // IF USER IS ON THE ACCOUNT PAGE ONLY CARD FORM IS REQUIRED
        if(is_page($wpvs_user_account_page)) {
            wp_enqueue_script('wpvs-stripe-processing', RVS_MEMBERS_STRIPE_URL . 'js/stripe-processing.js', array('wpvs-stripe'), $rvs_current_version, true);
            wp_localize_script('wpvs-stripe-processing', 'stripe_vars', array(
                    'publishable_key' => $stripe_publishable_key,
                    'processing' => __('Processing', 'vimeo-sync-memberships'),
                    'address' => $billing_address,
                    'cardcolor' => $wpvs_card_form_color,
                    'zip' => $verify_zip,
                    'is_account_page' => true
                )
            );
            $stripe_scripts_added = true;
        }

        if( ! $stripe_scripts_added ) {
            wp_enqueue_script('wpvs-stripe-processing', RVS_MEMBERS_STRIPE_URL . 'js/stripe-processing.js', array('wpvs-stripe'), $rvs_current_version, true);
            wp_localize_script('wpvs-stripe-processing', 'stripe_vars', array(
                    'publishable_key' => $stripe_publishable_key,
                    'processing' => __('Processing', 'vimeo-sync-memberships'),
                    'address' => $billing_address,
                    'cardcolor' => $wpvs_card_form_color,
                    'zip' => $verify_zip,
                    'is_account_page' => false
                )
            );
        }

        if($wpvs_google_apple_enabled) {
            wp_enqueue_script('wpvs-google-apple-payments');
            wp_localize_script('wpvs-google-apple-payments', 'google_stripe_vars', array(
                    'publishable_key' => $stripe_publishable_key,
                    'processing' => __('Processing', 'vimeo-sync-memberships'),
                    'currency' => strtolower($rvs_currency),
                    'country' => $wpvs_country,
                    'googleloading' => __('Google is working', 'vimeo-sync-memberships'),
                    'notsupported' => __('Either your browser does not support Google Pay, or you do not have any payment methods saved.', 'vimeo-sync-memberships'),
                    'addmethod' => __('Add Payment Method', 'vimeo-sync-memberships'),
                )
            );
        }

		if($wpvs_stripe_checkout_enabled) {
			wp_enqueue_script('wpvs-stripe-checkout');
		}
    }
}
add_action('wp_enqueue_scripts', 'rvs_load_stripe_scripts');
