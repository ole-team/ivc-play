<?php

function wpvs_default_new_stripe_customer_details($wpvs_user) {
    $new_customer_details = array();
    if( ! empty($wpvs_user) ) {
        $site_url = get_site_url();
        $new_customer_details = array(
            "email" => $wpvs_user->user_email,
            "description" => "Customer for " . $site_url . " User ID: " . $wpvs_user->ID . " Username: " . $wpvs_user->user_login,
            "metadata" => array("userid" => $wpvs_user->ID)
        );
        $customer_name = "";
        if( ! empty($wpvs_user->first_name) ) {
            $customer_name = $wpvs_user->first_name;
        }
        if( ! empty($wpvs_user->last_name) ) {
            if( ! empty($customer_name) ) {
                $customer_name .= ' '.$wpvs_user->last_name;
            } else {
                $customer_name = $wpvs_user->last_name;
            }
        }
        if( ! empty($customer_name) ) {
            $new_customer_details['name'] = $customer_name;
        }
    }
    return $new_customer_details;
}
