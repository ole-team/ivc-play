var rvs_stripe_button;
var new_card_button;
var new_card_clicked = false;
var card_style = {
  base: {
    color: stripe_vars.cardcolor,
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px'
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

var wpvs_stripe = Stripe(stripe_vars.publishable_key);
var wpvs_stripe_elements = wpvs_stripe.elements();
var wpvs_hide_stripe_zip = false;
if( stripe_vars.zip == false ) {
    wpvs_hide_stripe_zip = true;
}
var wpvs_pay_card = wpvs_stripe_elements.create('card', {style: card_style, hidePostalCode: wpvs_hide_stripe_zip});

jQuery(document).ready(function() {
    jQuery('#rvs-card-checkout-open').click(function() {
        jQuery('#rvs-checkout-box').slideToggle();
        if(jQuery('.vs-full-screen-video').length > 0) {
            jQuery('.vs-full-screen-video').animate({
            scrollTop: jQuery("#rvs-checkout-box").offset().top
        }, 1000);
        } else {
            jQuery('html, body').animate({
            scrollTop: jQuery("#rvs-checkout-box").offset().top
        }, 1000);
        }
    });
	jQuery("#stripe-payment-form").submit(function(event) {
        event.preventDefault();
        var payment_method_id = jQuery('#stripe-submit').data('method');
        wpvs_submit_stripe_checkout_form(payment_method_id, null, null);
        return false;
	});


    if(jQuery('#wpvs-card-element').length > 0) {
         wpvs_pay_card.mount('#wpvs-card-element');
    }

    // ADD NEW CARD FORM

    jQuery('#wpvs-new-stripe-card-form').submit(function(ev) {
        var client_secret = jQuery('#wpvs-new-stripe-card').data('secret');
        ev.preventDefault();
        jQuery('#wpvs-new-stripe-card').attr("disabled", true);
        if( jQuery('#card_name').val() != "" ) {
            if(stripe_vars.address == "false") {
                var payment_method_config = {
                    payment_method_data: {
                        billing_details: {name: jQuery('#card_name').val()}
                    }
                }
            } else {
                var payment_method_config = {
                    payment_method_data: {
                        billing_details: {
                            name: jQuery('#card_name').val(),
                            address: {
                                line1: jQuery('#new_address').val(),
                                city: jQuery('#new_address_city').val(),
                                state: jQuery('#new_province').val(),
                                postal_code: jQuery('#new_postal_code').val(),
                                country: jQuery('#address_country').val()
                            }
                        }
                    }
                }
            }

        wpvs_stripe.handleCardSetup(
            client_secret, wpvs_pay_card, payment_method_config
          ).then(function(result) {
            if (result.error) {
                jQuery(".payment-errors").html('<p class="rvs-error">'+result.error.message+'</p>').show();
                jQuery('#wpvs-new-stripe-card').attr("disabled", false);
            } else {
                if(result.setupIntent.payment_method) {
                    wpvs_confirm_new_payment_method(result.setupIntent.payment_method, null);
                }
            }
          });
      }

    });


    jQuery('#wpvs-new-stripe-card-form-single').submit(function(ev) {
        ev.preventDefault();
        jQuery('#wpvs-new-stripe-card').attr("disabled", true);
        if( jQuery('#card_name').val() != "" ) {
            if(stripe_vars.address == "false") {
                var payment_method_config = {
                    billing_details: {
                        name: jQuery('#card_name').val()
                    }
                }
            } else {
                var payment_method_config = {
                    billing_details: {
                        name: jQuery('#card_name').val(),
                        address: {
                            line1: jQuery('#new_address').val(),
                            city: jQuery('#new_address_city').val(),
                            state: jQuery('#new_province').val(),
                            postal_code: jQuery('#new_postal_code').val(),
                            country: jQuery('#address_country').val()
                        }
                    }
                }
            }

            wpvs_stripe.createPaymentMethod(
              'card', wpvs_pay_card, payment_method_config
            ).then(function(result) {
              if (result.error) {
                  jQuery(".payment-errors").html('<p class="rvs-error">'+result.error.message+'</p>').show();
                  jQuery('#wpvs-new-stripe-card').attr("disabled", false);
              } else {
                  if(result.paymentMethod.id) {
                      wpvs_submit_stripe_checkout_form(result.paymentMethod.id, null, '#wpvs-new-stripe-card-form-single');
                  }
              }
          });
      }

    });

});

function wpvs_confirm_new_payment_method(payment_method_id, form_id) {
    show_rvs_updating(rvsrequests.rvsmessage.card+"...");
    jQuery.ajax({
        url: rvsrequests.ajax,
        type: "POST",
        data: {
            'action': 'wpvs_add_new_card_ajax_request',
            'payment_method_id': payment_method_id,
        },
        success:function(response) {
            if( stripe_vars.is_account_page ) {
                window.location.reload();
            } else {
                wpvs_submit_stripe_checkout_form(payment_method_id, null, form_id);
            }

        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_submit_stripe_checkout_form(payment_method_id, payment_intent_id, form_id) {
    show_rvs_updating(stripe_vars.processing+"...");
    var stripe_form;
    var save_card_option = jQuery('#wpvs_save_card_for_future');
    if( form_id == null ) {
        if( jQuery("#stripe-payment-form").length > 0 ) {
            stripe_form = jQuery("#stripe-payment-form")[0];
        }
        if( jQuery("#wpvs-new-stripe-card-form").length > 0 ) {
            stripe_form = jQuery("#wpvs-new-stripe-card-form")[0];
        }
    } else {
        stripe_form = jQuery(form_id)[0];
    }

    var form_data = new FormData(stripe_form);
    form_data.append('action', 'wpvs_process_stripe_checkout_ajax_request');
    if( payment_method_id != null) {
        form_data.append('payment_method_id', payment_method_id);
    }
    if( payment_intent_id != null) {
        form_data.append('payment_intent_id', payment_intent_id);
    }
    if( save_card_option.is(':checked') ) {
        form_data.append('wpvs_save_card_for_future', true);
    }

    if( typeof(wpvscheckout) != 'undefined' ) {
        form_data.append('product_id', wpvscheckout.product_id);
        form_data.append('transaction_type', wpvscheckout.transaction_type);
        form_data.append('purchase_type', wpvscheckout.purchase_type);
        form_data.append('redirect', wpvscheckout.redirect_cancel);
        form_data.append('redirect_success', wpvscheckout.redirect_success);
        if( wpvscheckout.coupon_code ) {
            form_data.append('coupon_code', wpvscheckout.coupon_code);
        }
    }

    jQuery.ajax({
        url: rvsrequests.ajax,
        type: "POST",
        processData: false,
        contentType: false,
        data: form_data,
        success:function(response) {
            checkout_response = jQuery.parseJSON(response);
            if( checkout_response.requires_action && checkout_response.payment_intent_client_secret ) {
                wpvs_handle_confirmation_response(checkout_response, form_id);
            }
            if( checkout_response.success && checkout_response.redirect ) {
                window.location.href = checkout_response.redirect;
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_handle_confirmation_response(response, form_id) {
  wpvs_stripe.handleCardPayment(
    response.payment_intent_client_secret
  ).then(function(result) {
    if (result.error) {
        jQuery(".payment-errors").html('<p class="rvs-error">'+result.error.message+'</p>').show();
        jQuery('#wpvs-new-stripe-card').attr("disabled", false);
    } else {
        wpvs_submit_stripe_checkout_form(null, result.paymentIntent.id, form_id);
    }
  });
}
