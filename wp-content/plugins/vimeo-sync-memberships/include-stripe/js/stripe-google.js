var google_stripe = Stripe(google_stripe_vars.publishable_key);
jQuery(document).ready(function() {
    if(jQuery('#payment-request-button').length > 0) {
        create_google_pay_button();
    }
});

function create_google_pay_button() {
    jQuery('#payment-request-button').html('<div class="wpvs-updating-box"><span class="wpvs-loading-text">'+google_stripe_vars.googleloading+'...</span><span class="loadingCircle"></span><span class="loadingCircle"></span><span class="loadingCircle"></span><span class="loadingCircle"></span></div>');
    if( wpvscheckout.total ) {
        var google_amount = wpvscheckout.total;
        var google_pay_title = wpvscheckout.product_name;

        var paymentRequest = google_stripe.paymentRequest({
          country: google_stripe_vars.country,
          currency: google_stripe_vars.currency,
          total: {
            label: google_pay_title,
            amount: parseInt(google_amount),
          },
          'requestPayerName': true,
          'requestPayerEmail': true,
        });

        var google_elements = google_stripe.elements();
        var google_pay_button = google_elements.create('paymentRequestButton', {
          paymentRequest: paymentRequest,
        });

        // Check the availability of the Payment Request API first.
        paymentRequest.canMakePayment().then(function(result) {
          if (result) {
            google_pay_button.mount('#payment-request-button');
          } else {
            jQuery('#payment-request-button').html('<div class="wpvs-text-align-center"><p><strong>'+google_stripe_vars.notsupported+'</strong></p><a href="https://play.google.com/store/paymentmethods" target="_blank">'+google_stripe_vars.addmethod+'</a></div>');
          }
        });

        paymentRequest.on('paymentmethod', function(ev) {
            show_rvs_updating(google_stripe_vars.processing+"...");
            wpvs_confirm_new_payment_method(ev.paymentMethod.id, '#google-payment-form');
        });
    }
}
