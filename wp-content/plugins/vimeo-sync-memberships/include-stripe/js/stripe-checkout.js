jQuery(document).ready(function() {
    var wpvs_stripe_checkout = Stripe(stripe_vars.publishable_key);
    if( jQuery('#wpvs-stripe-checkout-link').length > 0 ) {
        wpvs_generate_stripe_checkout_button();
        jQuery('#wpvs-stripe-checkout-link').click( function() {
            var wpvs_checkout_id = jQuery(this).data('session');
            wpvs_stripe_checkout.redirectToCheckout({
              sessionId: wpvs_checkout_id
            }).then(function (result) {
                show_rvs_error(result.error.message);
            });
        });
    }

    jQuery('body').delegate('.wpvs-stripe-free-checkout', 'click', function() {
        wpvs_stripe_checkout_submit_free_payment();
    });
});

function wpvs_generate_stripe_checkout_button() {
    var purchase_type = wpvscheckout.purchase_type;
    var product_id = wpvscheckout.product_id;
    jQuery.ajax({
        url: rvsrequests.ajax,
        type: "POST",
        data: {
            'action': 'wpvs_create_stripe_checkout_session_id_ajax',
            'purchase_type': purchase_type,
            'product_id': product_id,
            'success_url': wpvscheckout.redirect_success,
            'cancel_url': wpvscheckout.redirect_cancel,
            'coupon_code': wpvscheckout.coupon_code,
            'trial_period': wpvscheckout.trial
        },
        success:function(response) {
            var wpvs_stripe_checkout_data = JSON.parse(response);
            if( wpvs_stripe_checkout_data.checkout_id && wpvs_stripe_checkout_data.checkout_id != "") {
                jQuery('#wpvs-stripe-checkout-link').data('session', wpvs_stripe_checkout_data.checkout_id);
                jQuery('#wpvs-stripe-checkout-loading').hide();
                jQuery('#wpvs-stripe-checkout-loaded').show();
            }

            if( wpvs_stripe_checkout_data.redirect ) {
                jQuery('#wpvs-stripe-checkout-loaded').html('<label class="rvs-button rvs-primary-button rvs-pay-button wpvs-stripe-free-checkout" data-type="'+purchase_type+'">'+rvsrequests.rvsmessage.stripe.completepurchase+'</label>');
                jQuery('#wpvs-stripe-checkout-loading').hide();
                jQuery('#wpvs-stripe-checkout-loaded').show();
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_stripe_checkout_submit_free_payment() {
    if( wpvscheckout.coupon_code && wpvscheckout.coupon_code != "" ) {
        jQuery.ajax({
            url: rvsrequests.ajax,
            type: "POST",
            data: {
                'action': 'wpvs_stripe_checkout_submit_free_payment_ajax',
                'purchase_type': wpvscheckout.purchase_type,
                'product_id': wpvscheckout.product_id,
                'success_url': wpvscheckout.redirect_success,
                'coupon_code': wpvscheckout.coupon_code
            },
            success:function(response) {
                var wpvs_stripe_checkout_data = JSON.parse(response);
                if( wpvs_stripe_checkout_data.success && wpvs_stripe_checkout_data.redirect != "") {
                    window.location.href = wpvs_stripe_checkout_data.redirect;
                }
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    }
}
