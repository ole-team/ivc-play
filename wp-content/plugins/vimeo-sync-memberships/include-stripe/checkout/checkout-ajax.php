<?php
add_action( 'wp_ajax_wpvs_create_stripe_checkout_session_id_ajax', 'wpvs_create_stripe_checkout_session_id_ajax' );
function wpvs_create_stripe_checkout_session_id_ajax() {
    global $rvs_currency;
    global $rvs_current_user;
    $wpvs_require_billing_information = get_option('wpvs_require_billing_information', 0);
    $checkout_session_id = null;
    $plan_id = null;
    $video_id = null;
    $term_id = null;
    $coupon_code = null;
    $create_checkout_session = true;
    $current_checkout_time = current_time('timestamp' , 1);
    if( isset($_POST['success_url']) && ! empty($_POST['success_url']) ) {
        $success_url = $_POST['success_url'];
    } else {
        $wpvs_account_page = get_option('rvs_account_page');
        $success_url = get_permalink($wpvs_account_page);
    }

    $cancel_url = $_POST['cancel_url'];
    $purchase_type = $_POST['purchase_type'];
    $product_id = $_POST['product_id'];
    $product_type = 'subscription';
    $session_mode = 'subscription';
    if( $purchase_type != 'subscription' ) {
        $session_mode = 'payment';
    }
    if($purchase_type == "purchase") {
        $product_title = get_the_title($product_id);
        $product_price = get_post_meta( $product_id, '_rvs_onetime_price', true );
        $product_description = __('Access to', 'vimeo-sync-memberships') . ' ' . $product_title . ' ' . __('on', 'vimeo-sync-memberships') . ' ' . get_bloginfo('name');
        $product_type = 'video';
        $video_id = $product_id;
    }
    if($purchase_type == "rental") {
        $product_title = get_the_title($product_id);
        $product_price = get_post_meta( $product_id, 'rvs_rental_price', true );
        $expires = get_post_meta( $product_id, 'rvs_rental_expires', true );
        $interval = get_post_meta( $product_id, 'rvs_rental_type', true );
        $product_description = sprintf(__('Rent %s for %s %s', 'vimeo-sync-memberships'), $product_title, $expires, $interval);
        $product_type = 'video';
        $video_id = $product_id;
    }

    if($purchase_type == "termpurchase") {
        $product_type = 'term';
        $term_id = $product_id;
        $wpvs_current_term = get_term($term_id, 'rvs_video_category' );
        $product_price = get_term_meta($term_id, 'wpvs_category_purchase_price', true);
        $product_title = $wpvs_current_term->name;
        if( ! empty($wpvs_current_term->parent) ) {
            $wpvs_parent_term = get_term(intval($wpvs_current_term->parent), 'rvs_video_category' );
            if( ! empty($wpvs_parent_term) && ! is_wp_error($wpvs_parent_term) ) {
                $product_title .= ' ('.$wpvs_parent_term->name.')';
            }
        }
        $product_description = __('Access to', 'vimeo-sync-memberships') . ' ' . $product_title . ' ' . __('on', 'vimeo-sync-memberships') . ' ' . get_bloginfo('name');
    }

    if($purchase_type == "subscription") {
        $wpvs_membership_manager = new WPVS_Membership_Plan($product_id);
        $product_price = $wpvs_membership_manager->amount;
    }

    if( isset($_POST['coupon_code']) && ! empty($_POST['coupon_code']) ) {
        $coupon_code = $_POST['coupon_code'];
        $product_price = wpvs_calculate_coupon_amount($product_price, $coupon_code);
        $product_price = ceil($product_price);
    }

    if( $rvs_currency == "JPY" ) {
        // remove decimals for Japanese Yen
        $product_price = number_format($charge_amount/100, 0);
    } else {
        // Charge is free from coupon; Stripe charges under 50 cents not allowed
        if( $product_price < 50 ) {
            $create_checkout_session = false;
        }
    }

    if( $product_price <= 0 ) {
        $create_checkout_session = false;
    }

    if( $create_checkout_session ) {
        $stripe_payments_manager = new WPVS_Stripe_Payments_Manager($rvs_current_user->ID);
        $current_user_checkout_session = $stripe_payments_manager->get_user_checkout_session($rvs_current_user->ID, $product_type, $product_id, $rvs_currency, $purchase_type, $product_price);

        if( ! empty($current_user_checkout_session) ) {
            if( $current_user_checkout_session->expires <= $current_checkout_time) {
                $stripe_payments_manager->delete_checkout_session($current_user_checkout_session->id);
                $current_user_checkout_session = null;
            }

            if( ! empty($current_user_checkout_session) && $current_user_checkout_session->amount != $product_price) {
                $stripe_payments_manager->delete_checkout_session($current_user_checkout_session->id);
                $current_user_checkout_session = null;
            }
        }

        if( empty($current_user_checkout_session) ) {
            $session_details = array(
                'payment_method_types' => array('card'),
                'success_url' => $success_url,
                'cancel_url' => $cancel_url,
                'client_reference_id' => $rvs_current_user->ID,
                'mode' => $session_mode,
            );

            if( $purchase_type == 'subscription' ) {
                $session_details['subscription_data'] = array(
                    'items' => array(
                        array(
                            'plan' => $product_id
                        ),
                    ),
                );
                if( isset($_POST['trial_period']) && ! empty($_POST['trial_period']) ) {
                    $session_details['subscription_data']['trial_from_plan'] = true;
                }
                $plan_id = $product_id;
            } else {
                $session_details['line_items'] = array(
                    array(
                        'name' => $product_title,
                        'description' => $product_description,
                        'amount' => $product_price,
                        'currency' => $rvs_currency,
                        'quantity' => 1
                    ),
                );
                $session_details['payment_intent_data'] = array(
                    'metadata' => array(
                        'product_id' => $product_id,
                        'purchase_type' => $purchase_type,
                    ),
                );
                if( ! empty($coupon_code) ) {
                    $session_details['payment_intent_data']['metadata']['coupon'] = $coupon_code;
                }
            }

            if( $wpvs_require_billing_information ) {
                $session_details['billing_address_collection'] = 'required';
            }

            $session_details['customer_email'] = $rvs_current_user->user_email;
            $checkout_session_id = $stripe_payments_manager->new_checkout_session($session_details);
            if( isset($checkout_session_id['error']) ) {
                $checkout_error .= __('Error creating Stripe Checkout URL', 'vimeo-sync-memberships').': '.$checkout_session_id['error'];
                rvs_exit_ajax($checkout_error, 400);
            } else {
                $new_checkout_expires = strtotime('+1 day', $current_checkout_time);
                $new_user_checkout = array(
                    'id'            => $checkout_session_id,
                    'currency'      => $rvs_currency,
                    'amount'        => $product_price,
                    'userid'        => $rvs_current_user->ID,
                    'planid'        => $plan_id,
                    'videoid'       => $video_id,
                    'termid'        => $term_id,
                    'type'          => $product_type,
                    'purchase_type' => $purchase_type,
                    'expires'       => $new_checkout_expires
                );
                $stripe_payments_manager->add_user_checkout_session($new_user_checkout);
            }
        } else {
            $checkout_session_id = $current_user_checkout_session->id;
        }
        if( ! empty($checkout_session_id) ) {
            echo json_encode(array('checkout_id' => $checkout_session_id));
        } else {
            rvs_exit_ajax(__('Unable to retrieve Stripe checkout', 'vimeo-sync-memberships'), 400);
        }
    } else {
        echo json_encode(array('redirect' => $success_url));
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_stripe_checkout_submit_free_payment_ajax', 'wpvs_stripe_checkout_submit_free_payment_ajax' );

function wpvs_stripe_checkout_submit_free_payment_ajax() {
    global $rvs_currency;
    global $rvs_current_user;
    global $rvs_live_mode;
    $product_id = null;
    $coupon_code = null;
    $test_member = false;
    if( $rvs_live_mode == "off" ) {
        $test_member = true;
    }
    if( isset($_POST['success_url']) && ! empty($_POST['success_url']) ) {
        $success_url = $_POST['success_url'];
    } else {
        $wpvs_account_page = get_option('rvs_account_page');
        $success_url = get_permalink($wpvs_account_page);
    }

    $purchase_type = $_POST['purchase_type'];
    $product_id = $_POST['product_id'];

    if($purchase_type == "purchase" || $purchase_type == "rental" || $purchase_type == "termpurchase") {
        $wpvs_customer = new WPVS_Customer($rvs_current_user);
        $wpvs_customer->add_product($product_id, $purchase_type);
        if( isset($_POST['coupon_code']) && ! empty($_POST['coupon_code']) ) {
            $coupon_code = $_POST['coupon_code'];
            wpvs_add_coupon_code_use($coupon_code, $rvs_current_user->ID);
        }
        wpvs_add_new_member($rvs_current_user->ID, $test_member);
        echo json_encode( array('success' => true, 'redirect' => $success_url) );
    } else {
        rvs_exit_ajax(__('This payment type is not supported for Stripe Checkout.', 'vimeo-sync-memberships'), 400);
    }
    wp_die();
}
