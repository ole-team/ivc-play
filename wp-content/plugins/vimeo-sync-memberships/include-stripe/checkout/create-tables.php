<?php

function wpvs_create_stripe_checkout_tables() {
    global $wpdb;
    if( ! wpvs_stripe_checkout_table_exists() ) {
        $wpvs_stripe_checkout_sessions_table = $wpdb->prefix . 'wpvs_stripe_checkout_sessions';
        $wpvs_stripe_checkout_sessions_test_table = $wpdb->prefix . 'wpvs_test_stripe_checkout_sessions';
        $charset_collate = $wpdb->get_charset_collate();

        $live_stripe_checkout_tables = "CREATE TABLE $wpvs_stripe_checkout_sessions_table (
          id varchar(100) NOT NULL UNIQUE,
          currency tinytext NOT NULL,
          amount mediumint(20) NOT NULL,
          userid int NOT NULL,
          planid tinytext,
          videoid mediumint(9),
          termid mediumint(9),
          type tinytext NOT NULL,
          purchase_type tinytext,
          expires text NOT NULL,
          PRIMARY KEY (id)
        ) $charset_collate;";

        $test_stripe_checkout_tables = "CREATE TABLE $wpvs_stripe_checkout_sessions_test_table (
            id varchar(100) NOT NULL UNIQUE,
            currency tinytext NOT NULL,
            amount mediumint(20) NOT NULL,
            userid int NOT NULL,
            planid tinytext,
            videoid mediumint(9),
            termid mediumint(9),
            type tinytext NOT NULL,
            purchase_type tinytext,
            expires text NOT NULL,
          PRIMARY KEY (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $live_stripe_checkout_tables );
        dbDelta( $test_stripe_checkout_tables );
    }
}

function wpvs_stripe_checkout_table_exists() {
    global $wpdb;
    $stripe_checkout_sessions_table_exists = true;
    $wpvs_stripe_checkout_sessions_table_name = $wpdb->prefix . 'wpvs_stripe_checkout_sessions';
    if($wpdb->get_var("SHOW TABLES LIKE '$wpvs_stripe_checkout_sessions_table_name'") != $wpvs_stripe_checkout_sessions_table_name) {
         $stripe_checkout_sessions_table_exists = false;
    }
    return $stripe_checkout_sessions_table_exists;
}
