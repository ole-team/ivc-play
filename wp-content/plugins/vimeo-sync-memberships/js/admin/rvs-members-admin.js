var members_offset = 0;
var members_role = 0;
jQuery(document).ready(function() {
    wpvs_admin_create_members_list(members_offset, members_role);

    jQuery('.rvs-more-members').click(function() {
        members_offset += 50;
        wpvs_admin_create_members_list(members_offset, members_role);
    });

    jQuery('.rvs-back-members').click(function() {
        members_offset -= 50;
        wpvs_admin_create_members_list(members_offset, members_role);
    });
    jQuery('#rvs-search-member').click(function() {
        var search_term = jQuery('#wpvs-search-user-input').val();
        if(search_term != "") {
            jQuery('.rvs-next-button').hide();
            jQuery('.rvs-previous-button').hide();
            jQuery('#wpvs-loading-members').find('#wpvs-update-text').text('Searching Users...');
            jQuery('#wpvs-loading-members').show();
            jQuery('#wpvs-members-loaded').html('');
            jQuery.ajax({
                url: rvsrequests.ajax,
                type: "POST",
                data: {
                    'action': 'wpvs_search_members_ajax',
                    'search_term': search_term
                },
                success:function(response) {
                    if( response != "No members found") {
                        var found_members = JSON.parse(response);
                        var new_list_html = "";
                        jQuery.each(found_members, function(index, member) {
                             new_list_html += wpvs_admin_add_user_to_members_list(member.user_id, member.user_name, member.user_memberships);
                        });

                        var append_members_list = '<table id="memberships" class="rvs_memberships"><tbody><tr id="rvs-headings"><th>Id</th><th>Username</th><th>Memberships</th></tr>';
                        append_members_list += new_list_html;
                        append_members_list += '</tbody></table>';
                        jQuery('#wpvs-loading-members').hide();
                        jQuery('#wpvs-members-loaded').html(append_members_list);

                    } else {
                        jQuery('#wpvs-loading-members').hide();
                        jQuery('#wpvs-members-loaded').html('<div class="wpvs-nothing-found">User Not Found</div>');
                    }

                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }
    });

    jQuery('body').delegate('.wpvs-remove-member', 'click', function() {
        if(window.confirm('Are you sure you want to delete this user as a Member?')) {
            var this_remove_button = jQuery(this);
            var update_row = this_remove_button.parent().parent();
            var user_id = this_remove_button.data('user');
            jQuery.ajax({
                url: rvsrequests.ajax,
                type: "POST",
                data: {
                    'action': 'wpvs_remove_member_ajax',
                    'user_id': user_id
                },
                success:function(response) {
                    update_row.remove();
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }
    });

    jQuery('body').delegate('#wpvs-refresh-members-page', 'click', function() {
        jQuery('#wpvs-search-user-input').val('');
        members_offset = 0;
        wpvs_admin_create_members_list(members_offset, members_role);
    });

    if( jQuery('#wpvs_filter_members_by_role').length > 0 ) {
        jQuery('#wpvs_filter_members_by_role').change(function() {
            var wpvs_member_filter = jQuery(this).val();
            if( wpvs_member_filter != "" ) {
                members_offset = 0;
                members_role = wpvs_member_filter;
                wpvs_admin_create_members_list(members_offset, members_role);
            }
        });
    }
});

function wpvs_admin_add_user_to_members_list(user_id, user_name, user_memberships) {
    if(user_memberships == "") {
        user_memberships = "<em>No memberships</em>";
    }
    var new_user_tr = '<tr><td>'+user_id+'</td><td>'+user_name+'</td><td>'+user_memberships+'</td><td><a class="wpvs-edit-member-icon" href="admin.php?page=rvs-edit-member&id='+user_id+'"><span class="dashicons dashicons-edit"></span></a><label class="wpvs-remove-member wpvs-edit-member-icon" data-user="'+user_id+'"><span class="dashicons dashicons-trash"></span></label></td>';
    return new_user_tr;
}

function wpvs_admin_create_members_list(offset, role) {
    if(offset <= 0) {
        members_offset = 0;
    }
    jQuery('#wpvs-loading-members').find('#wpvs-update-text').text('Getting Members...');
    jQuery('#wpvs-loading-members').show();
    jQuery('#wpvs-members-loaded').html('');
    jQuery.ajax({
        url: rvsrequests.ajax,
        type: "POST",
        data: {
            'action': 'wpvs_get_members_list',
            'members_offset': offset,
            'role': role
        },
        success:function(response) {
            var members = JSON.parse(response);
            if( members.found ) {
                var found_members = members.members;
                var new_list_html = "";
                jQuery.each(found_members, function(index, member) {
                     new_list_html += wpvs_admin_add_user_to_members_list(member.user_id, member.user_name, member.user_memberships);
                });

                var append_members_list = '<table id="memberships" class="rvs_memberships"><tbody><tr id="rvs-headings"><th>Id</th><th>Username</th><th>Memberships</th><th>Manage</th></tr>';
                append_members_list += new_list_html;
                append_members_list += '</tbody></table>';
                jQuery('#wpvs-loading-members').hide();
                jQuery('#wpvs-members-loaded').html(append_members_list);
                if(found_members.length < 50) {
                    jQuery('.rvs-next-button').hide();
                } else {
                    jQuery('.rvs-next-button').show();
                }
                if(members_offset >= 50) {
                    jQuery('.rvs-previous-button').show();
                } else {
                    jQuery('.rvs-previous-button').hide();
                }

            } else {
                jQuery('#wpvs-loading-members').hide();
                jQuery('#wpvs-members-loaded').html('<div class="wpvs-nothing-found">'+members.reason+'</div>');
                jQuery('.rvs-next-button').hide();
                if(members_offset >= 50) {
                    jQuery('.rvs-previous-button').show();
                }
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
            jQuery('#wpvs-loading-members').hide();
            jQuery('#wpvs-members-loaded').html('<div class="wpvs-nothing-found">Sorry, something went wrong.</label></div>');
        }
    });
}

function wpvs_admin_import_stripe_customers(start_at_customer, total_added) {
    show_rvs_updating("Importing Stripe Customers...please wait");
    jQuery.ajax({
        url: rvsrequests.ajax,
        type: 'POST',
        data: {
            'action': 'wpvs_admin_import_all_stripe_customers',
            'start_at': start_at_customer,
            'total_added': total_added
        },
        success:function(response) {
            var customer_details = JSON.parse(response);
            if( customer_details.more ) {
                wpvs_admin_import_stripe_customers(customer_details.start_at, customer_details.added)
            } else {
                show_rvs_updating('Imported '+customer_details.added+' customers');
                location.reload();
            }

        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}
