jQuery(document).ready(function() {
    jQuery('input[name="wpvs_selected_memberships[]"]').change(function () {
        if( jQuery('.wpvs-required-download-membership').length < 1 ) {
            jQuery('#wpvs-required-download-memberships').html("");
        }
        var wpvs_checkbox_plan = jQuery(this).val();
        var wpvs_checkbox_name = jQuery(this).parent().text();
        var wpvs_download_option = jQuery('#wpvs-required-download-memberships').find('input[value="'+wpvs_checkbox_plan+'"]');
        if(this.checked) {
            if( wpvs_download_option.length < 1 ) {
                var new_checkbox_html = '<div class="wpvs-required-download-membership"><label class="selectit"><input type="checkbox" value="'+wpvs_checkbox_plan+'" name="wpvs_required_download_memberships[]">'+wpvs_checkbox_name+'</label></div>';
                jQuery('#wpvs-required-download-memberships').append(new_checkbox_html);
            }
        } else {
            if( wpvs_download_option.length > 0 ) {
                wpvs_download_option.parent().parent().remove();
            }
        }
        if( jQuery('#wpvs-required-download-memberships').html() == "" ) {
            jQuery('#wpvs-required-download-memberships').html("<em>No Membership Access is set for this video</em>");
        }
    });

    if( jQuery('#rvs-get-vimeo-download').length > 0 ) {
        // VIMEO DOWNLOADS
        jQuery('#rvs-get-vimeo-download').click( function() {
            show_rvs_updating("Getting downloads...");
            var vimeo_id = jQuery('#rvs-vimeo-id').val();
            jQuery.ajax({
                url: rvsrequests.ajax,
                type: 'POST',
                data: {
                    'action': 'wpvs_get_vimeo_download',
                    'vimeo_id': vimeo_id
                },
                success:function(response) {
                    jQuery('#wpvs-updating-box').fadeOut('fast');
                    var vimeo_downloads = JSON.parse(response);
                    var add_to_downloads = "";
                    if(vimeo_downloads.length > 0) {
                        jQuery.each(vimeo_downloads, function(index, download) {
                            if(download.width && download.height) {
                                add_to_downloads += '<label class="wpvs-vimeo-download rvs-border-box" data-download="'+download.link+'">'+download.width+' x '+download.height+' <span class="quality">'+download.quality+'</span></label>';
                            }
                        });
                    } else {
                        add_to_downloads = '<label class="wpvs-vimeo-download rvs-border-box">No downloads found.</label>'
                    }
                    jQuery('#wpvs-vimeo-downloads').html(add_to_downloads);
                },
                error: function(response) {
                    show_rvs_error(response.responseText);
                }
            });
        });

        jQuery('body').delegate('.wpvs-vimeo-download', 'click', function() {
            var update_link = jQuery(this).data('download');
            jQuery('#rvs_video_download_link').val(update_link);
        });

        jQuery('#select-video-type').change( function() {
            if( jQuery(this).val() == "vimeo" ) {
                jQuery('#rvs-get-vimeo-download').addClass('show-vimeo-downloads-button');
                jQuery('#wpvs-vimeo-downloads').show();
            } else {
                jQuery('#rvs-get-vimeo-download').removeClass('show-vimeo-downloads-button');
                jQuery('#wpvs-vimeo-downloads').hide();
            }
        });
    }


});
