(function($) {

	// we create a copy of the WP inline edit post function
	var $wpvs_inline_edit = inlineEditPost.edit;
	// and then we overwrite the function with our own code
	inlineEditPost.edit = function( id ) {

		// "call" the original WP edit function
		// we don't want to leave WordPress hanging
		$wpvs_inline_edit.apply( this, arguments );

		// now we take care of our business

		// get the post ID
		var $post_id = 0;
		if ( typeof( id ) == 'object' )
			$post_id = parseInt( this.getId( id ) );

		if ( $post_id > 0 ) {
			// define the edit row
			var $edit_row = $( '#edit-' + $post_id );
			var $post_row = $( '#post-' + $post_id );
		}

        var $current_video_memberships_string = $( '.column-wpvs_memberships_column .wpvs-column-membership-list', $post_row ).text();
        $current_video_memberships_string = $.trim($current_video_memberships_string);
        $current_video_memberships_string = $current_video_memberships_string.replace(/\s/g,'');
        if($current_video_memberships_string != "") {
            var $current_video_memberships = $current_video_memberships_string.split(',');

            var $wpvs_edit_membership_inputs =  $edit_row.find('.wpvs-quick-edit-membership-checkbox');
            $wpvs_edit_membership_inputs.each( function(index, membership) {
                var this_membership = $(this);
                if($.inArray(this_membership.val(), $current_video_memberships) > -1) {
                    this_membership.attr('checked', true);
                }
            });
        }

        var $wpvs_users_are_free = $post_row.find('.wpvs-membership-users-free').val();
        if($wpvs_users_are_free && $wpvs_users_are_free == 1) {
            $edit_row.find('input.wpv-membership-users-free-edit').attr('checked', true);
        }
	};

	$( document ).on( 'click', '#bulk_edit', function() {

		// define the bulk edit row
		var $bulk_row = $( '#bulk-edit' );

		var $video_ids = new Array();
		$bulk_row.find( '#bulk-titles' ).children().each( function() {
			$video_ids.push( $( this ).attr( 'id' ).replace( /^(ttle)/i, '' ) );
		});

        var $wpvs_membership_inputs =  $bulk_row.find('.wpvs-quick-edit-membership-checkbox');

        var $video_memberships = new Array();
		$wpvs_membership_inputs.each( function(index, membership) {
            var this_membership = $(this);
            if(this_membership.is(':checked')) {
                $video_memberships.push( this_membership.val() );
            }
        });
        var wpvs_free_for_users = $('input[name="wpvs_membership_users_bulk_free"]:checked').val();
        var remove_all_wpvs_memberships = 0;
        if($('#wpvs_memberships_set_none').is(':checked')) {
            remove_all_wpvs_memberships = 1;
        }


		// save the data
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			async: false,
			cache: false,
			data: {
				action: 'save_bulk_edit_rvs_video_memberships',
				video_ids: $video_ids,
				video_memberships: $video_memberships,
                wpvs_free_for_users: wpvs_free_for_users,
                wpvs_remove_all: remove_all_wpvs_memberships
			}
		});
	});
})(jQuery);
