jQuery(document).ready(function() {
    jQuery('.wpvs-run-update').click( function() {
        var this_update_button = jQuery(this);
        this_update_button.hide();
        this_update_button.next('.wpvs-updating-icon').show();
        jQuery.ajax({
            url: rvsrequests.ajax,
            type: 'POST',
            data: {
                'action': 'wpvs_run_database_update'
            },
            success:function(response) {
                window.location.reload();
            },
            error: function(error) {
                this_update_button.show();
                this_update_button.next('.wpvs-updating-icon').hide();
                this_update_button.next('.wpvs-update-errors').html('<p class="rvs-error">'+error.responseText+'</p>').show();
            }
        });
    });
});