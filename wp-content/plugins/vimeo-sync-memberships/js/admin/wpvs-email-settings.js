jQuery(document).ready(function() {
    jQuery('#wpvs-send-test-email').click( function() {
        var this_update_button = jQuery(this);
        var test_email = jQuery('#wpvs-test-email').val();
        var notice_box = this_update_button.next('.wpvs-admin-notice');
        notice_box.hide();
        if(test_email != "" && typeof(test_email) != "undefined") {
            this_update_button.parent().find('.wpvs-updating-icon').css('display', 'block');
            jQuery.ajax({
                url: rvsrequests.ajax,
                type: 'POST',
                data: {
                    'action': 'wpvs_send_test_smtp_email',
                    'email': test_email
                },
                success:function(response) {
                    this_update_button.parent().find('.wpvs-updating-icon').css('display', 'none');
                    notice_box.html('<p class="rvs-success">'+response+'</p>').show();
                },
                error: function(error) {
                    this_update_button.parent().find('.wpvs-updating-icon').hide();
                    this_update_button.parent().find('.wpvs-update-errors').html('<p class="rvs-error">'+error.responseText+'</p>').show();
                }
            });
        }
    });
});