function wpvs_create_admin_refund_buttons() {
    jQuery('body').delegate('.stripe-refund', 'click', function() {
        var updateThis = jQuery(this).parent();
        var refund_charge = jQuery(this).attr('id');
        if(window.confirm("Are you sure you want to refund this payment?")) {
            show_rvs_updating('Refunding...');
            jQuery.ajax({
                url: rvsrequests.ajax,
                type: "POST",
                data: {
                    'action': 'wpvs_stripe_refund_ajax_request',
                    'chargeId': refund_charge
                },
                success:function(response) {
                    jQuery('#wpvs-updating-box').fadeOut('fast');
                    updateThis.html('<label id="'+refund_charge+'"><em>Refunded</em></label>');
                    jQuery('#error').hide();
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }
    });
    
    jQuery('body').delegate('.paypal-refund', 'click', function() {
        var updateThis = jQuery(this).parent();
        var payment_id = jQuery(this).attr('id');
        if(window.confirm("Are you sure you want to refund this payment?")) {
            show_rvs_updating('Refunding...');
            jQuery.ajax({
                url: rvsrequests.ajax,
                type: "POST",
                data: {
                    'action': 'rvs_paypal_refund_ajax_request',
                    'payment_id': payment_id
                },
                success:function(response) {
                    jQuery('#wpvs-updating-box').fadeOut('fast');
                    updateThis.html('<label id="'+payment_id+'"><em>Refunded</em></label>');
                    jQuery('#error').hide();
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }
    });
}