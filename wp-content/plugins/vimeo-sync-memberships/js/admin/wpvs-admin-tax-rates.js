jQuery(document).ready(function() {
    // Save Data
    jQuery('#wpvs-new-tax-rate').submit(function(e) {
        e.preventDefault();
        var tax_rate_data = new FormData(jQuery(this)[0]);
        tax_rate_data.append('action', 'wpvs_admin_create_tax_rate');
        show_rvs_updating("Creating Tax Rate...");
        jQuery.ajax({
            url: rvsrequests.ajax,
            type: "POST",
            processData: false,
            contentType: false,
            data: tax_rate_data,
            success:function(response) {
                window.location.href = wpvstaxmanager.redirect;
                jQuery('#wpvs-updating-box').fadeOut('fast');
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });

    });

    jQuery('.wpvs-delete-tax-rate').click(function() {
        var tax_rate_id = jQuery(this).data('tax-rate-id');
        var parent_cell = jQuery(this).parent().parent();
        if(window.confirm("Are you sure you want to delete this Tax Rate?")) {
            show_rvs_updating("Deleting Tax Rate...");
            jQuery.ajax({
                url: rvsrequests.ajax,
                type: "POST",
                data: {
                    'action': 'wpvs_admin_delete_tax_rate',
                    'tax_rate_id': tax_rate_id
                },
                success:function(response) {
                    jQuery('#wpvs-updating-box').fadeOut('fast');
                    parent_cell.remove();
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }
    });
});
