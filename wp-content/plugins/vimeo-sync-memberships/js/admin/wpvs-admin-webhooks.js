jQuery(document).ready(function() {
    jQuery('#wpvs-admin-create-stripe-endpoints').click( function() {
        show_rvs_updating("Creating Stripe Webhook Endpoints...");
        jQuery.ajax({
            url: wpvswebh.ajax,
            type: 'POST',
            data: {
                'action': 'create_wpvs_webhook_endpoints'
            },
            success:function(response) {
                jQuery('#wpvs-updating-box').fadeOut('fast');
                location.reload();
            },
            error: function(response) {
                show_rvs_error(response.responseText);
            }
        });
    });

    jQuery('#wpvs-reset-paypal-webhooks').click( function() {
        show_rvs_updating("Reseting webhooks...");
        jQuery.ajax({
            url: wpvswebh.ajax,
            type: 'POST',
            data: {
                'action': 'wpvs_reset_paypal_webhooks'
            },
            success:function(response) {
                jQuery('#wpvs-updating-box').fadeOut('fast');
                location.reload();
            },
            error: function(response) {
                show_rvs_error(response.responseText);
            }
        });
    });

});
