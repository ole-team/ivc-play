var members_offset = 0;
var members_role = 0;
var select_members_count = 0;
var update_members_array = new Array();
jQuery(document).ready(function() {
    wpvs_admin_create_members_list(members_offset, members_role);

    jQuery('.rvs-more-members').click(function() {
        members_offset += 100;
        wpvs_admin_create_members_list(members_offset, members_role);
    });

    jQuery('.rvs-back-members').click(function() {
        members_offset -= 100;
        wpvs_admin_create_members_list(members_offset, members_role);
    });

    jQuery('#wpvs-submit-bulk-members-edit').click( function() {
        wpvs_get_selected_members();
        select_members_count = update_members_array.length;
        if( select_members_count < 1 ) {
            alert('Please select at least 1 user from the list.');
        } else {
            var action = jQuery('#wpvs_bulk_members_action').val();
            var membership_name = jQuery('#wpvs_bulk_members_membership').children('option:selected').text();
            var membership_id = jQuery('#wpvs_bulk_members_membership').children('option:selected').val();
            var confirmation_text = 'Are you sure you want to '+action+' the '+membership_name+' membership to '+select_members_count+' members?';
            if( action == "remove" ) {
                confirmation_text = 'Are you sure you want to '+action+' the '+membership_name+' membership from '+select_members_count+' members?'
            }
            if( window.confirm(confirmation_text) ) {
                wpvs_admin_submit_bulk_edit_members(action, membership_id, membership_name);
            }
        }
    });

    jQuery('#import-stripe-customers').click( function() {
        if(window.confirm('Are you sure you want to import your Stripe customers? Stripe Plans attached to customer subscriptions will also be imported to WP Videos -> Memberships.')) {
            wpvs_admin_import_stripe_customers(null, null);
        }
    });

    jQuery('body').delegate('#wpvs-bulk-members-select-all', 'change', function() {
        if(jQuery(this).is(':checked')) {
            //var all_visible_members = jQuery('.wpvs-bulk-member-select').length;
            jQuery('.wpvs-bulk-member-select').attr('checked', true);
            //jQuery('#wpvs-selected-members-count').text(all_videos);
        } else {
            jQuery('.wpvs-bulk-member-select').attr('checked', false);
            //jQuery('#wpvs-selected-members-count').text(0);
        }
        wpvs_get_selected_members();
    });

    jQuery('.wpvs-bulk-member-select', 'click', function() {
        wpvs_get_selected_members();
    });

    jQuery('.wpvs-select-date-admin').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM dd, yy',
        minDate: 0,
        onSelect: function(dateText, inst) {
            var set_date = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
            var membership_ends = set_date.setFullYear(inst.selectedYear);
            var update_table_row = jQuery(this).parent().parent();
            var plan_id = update_table_row.find('.membership-sub-id').val();
            var new_membership_ends = membership_ends/1000;
            jQuery('#wpvs-set-membership-ends').val(new_membership_ends);
        }
    });

    jQuery('#wpvs-clear-membership-ends').click( function() {
        jQuery('#wpvs-set-membership-ends').val("");
        jQuery('.wpvs-select-date-admin').datepicker('setDate', null);
    });

    if( jQuery('#wpvs_filter_members_by_role').length > 0 ) {
        jQuery('#wpvs_filter_members_by_role').change(function() {
            var wpvs_member_filter = jQuery(this).val();
            if( wpvs_member_filter != "" ) {
                members_offset = 0;
                members_role = wpvs_member_filter;
                wpvs_admin_create_members_list(members_offset, members_role);
            }
        });
    }
});

function wpvs_admin_add_user_to_members_list(user_id, user_name, user_memberships) {
    if(user_memberships == "") {
        user_memberships = "<em>No memberships</em>";
    }
    var new_user_tr = '<tr><td><input class="wpvs-bulk-member-select" type="checkbox" value="'+user_id+'" /></td><td>'+user_id+'</td><td>'+user_name+'</td><td>'+user_memberships+'</td><td><a class="wpvs-edit-member-icon" href="admin.php?page=rvs-edit-member&id='+user_id+'"><span class="dashicons dashicons-edit"></span></a></td>';
    return new_user_tr;
}

function wpvs_admin_create_members_list(offset, role) {
    if(offset <= 0) {
        members_offset = 0;
    }
    jQuery('#wpvs-loading-members').find('#wpvs-update-text').text('Getting Members...');
    jQuery('#wpvs-loading-members').show();
    jQuery('#wpvs-members-loaded').html('');
    jQuery.ajax({
        url: rvsrequests.ajax,
        type: "POST",
        data: {
            'action': 'wpvs_admin_get_users_for_bulk_editing',
            'members_offset': offset,
            'role': role
        },
        success:function(response) {
            var members = JSON.parse(response);
            if( members.found ) {
                var found_members = members.members;
                var new_list_html = "";
                jQuery.each(found_members, function(index, member) {
                     new_list_html += wpvs_admin_add_user_to_members_list(member.user_id, member.user_name, member.user_memberships);
                });

                var append_members_list = '<table id="memberships" class="rvs_memberships"><tbody><tr id="rvs-headings"><th>Select</th><th>Id</th><th>Username</th><th>Memberships</th><th>Manage</th></tr><tr><td><input type="checkbox" id="wpvs-bulk-members-select-all"></td></tr>';
                append_members_list += new_list_html;
                append_members_list += '</tbody></table>';
                jQuery('#wpvs-loading-members').hide();
                jQuery('#wpvs-members-loaded').html(append_members_list);
                if(found_members.length < 100) {
                    jQuery('.rvs-next-button').hide();
                } else {
                    jQuery('.rvs-next-button').show();
                }
                if(members_offset >= 100) {
                    jQuery('.rvs-previous-button').show();
                } else {
                    jQuery('.rvs-previous-button').hide();
                }

            } else {
                jQuery('#wpvs-loading-members').hide();
                jQuery('#wpvs-members-loaded').html('<div class="wpvs-nothing-found">'+members.reason+'</div>');
                jQuery('.rvs-next-button').hide();
                if(members_offset >= 100) {
                    jQuery('.rvs-previous-button').show();
                }
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
            jQuery('#wpvs-loading-members').hide();
            jQuery('#wpvs-members-loaded').html('<div class="wpvs-nothing-found">Sorry, something went wrong.</label></div>');
        }
    });
}

function wpvs_admin_import_stripe_customers(start_at_customer, total_added) {
    show_rvs_updating("Importing Stripe Customers...please wait");
    jQuery.ajax({
        url: rvsrequests.ajax,
        type: 'POST',
        data: {
            'action': 'wpvs_admin_import_all_stripe_customers',
            'start_at': start_at_customer,
            'total_added': total_added
        },
        success:function(response) {
            var customer_details = JSON.parse(response);
            if( customer_details.more ) {
                wpvs_admin_import_stripe_customers(customer_details.start_at, customer_details.added)
            } else {
                show_rvs_updating('Imported '+customer_details.added+' customers');
                location.reload();
            }

        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_get_selected_members() {
    update_members_array = [];
    jQuery('.wpvs-bulk-member-select').each(function(index) {
        if(jQuery(this).is(':checked')) {
            update_members_array.push(jQuery(this).val());
        }
    });
}

function wpvs_admin_submit_bulk_edit_members(action, membership_id, membership_name) {
    var set_end_date = "";
    if( jQuery('#wpvs-set-membership-ends').val() != "") {
        set_end_date = jQuery('#wpvs-set-membership-ends').val();
    }
    var updating_text = "";
    if( action == "add" ) {
        updating_text = 'Adding the '+membership_name+' membership to '+select_members_count+' users.';
    }
    if( action == "remove" ) {
        updating_text = 'Removing the '+membership_name+' membership from '+select_members_count+' users.';
    }
    show_rvs_updating(updating_text);
    jQuery.ajax({
        url: rvsrequests.ajax,
        type: 'POST',
        data: {
            'action': 'wpvs_admin_bulk_add_membership_to_members',
            'members': update_members_array,
            'update_action': action,
            'membership_id': membership_id,
            'end_date': set_end_date
        },
        success:function(response) {
            var updated_details = JSON.parse(response);
            var updated_text = membership_name+' membership added to '+updated_details.updated_members_count+' users.';
            if( action == "remove" ) {
                updated_text = membership_name+' membership removed from '+updated_details.updated_members_count+' users.';
            }
            show_rvs_updating(updated_text);
            location.reload();
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}
