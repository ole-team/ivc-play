jQuery(document).ready(function() {
    jQuery('#rvs-account-details').submit( function(e) {
        e.preventDefault();
        if(wpvs_check_new_user_fields()) {
            var username;
            var first_name;
            var last_name;
            var password;
            var confirmp;
            var email =  jQuery('#new_user_email').val();
            if(wpvsna.usernames) {
                username = jQuery('#new_user_name').val();
            } else {
                username = email;
            }

            if(wpvsna.firstlast) {
                first_name = jQuery('#new_user_first_name').val();
                last_name =  jQuery('#new_user_last_name').val();
            }

            if(wpvsna.passcreation) {
                var password = jQuery('#new_user_password').val();
                var confirmp = jQuery('#confirm_user_password').val();
            }

            wpvs_create_new_user_account(username, email, password, confirmp, first_name, last_name);
        }
    });
});

function wpvs_create_new_user_account(username, email, password, confirmp, first_name, last_name) {
    jQuery('#rvs-new-account-error').html("").hide();
    show_rvs_updating(wpvsna.creatingaccount+'...');

    var wpvs_custom_registration_data = wpvs_convert_custom_data_to_json(jQuery('form#rvs-account-details').serializeArray());
    jQuery.ajax({
        url: wpvsna.ajax,
        type: 'POST',
        cache: false,
	    headers: { "cache-control": "no-cache" },
        data: {
            'action': 'wpvs_create_new_account_req',
            'new_user_login':  username,
            'new_user_email': email,
            'new_user_password': password,
            'confirm_user_password': confirmp,
            'wpvs_first_name': first_name,
            'wpvs_last_name': last_name,
            'wpvs_custom_registration_fields': wpvs_custom_registration_data
        },
        success:function(response) {
            var user_response = JSON.parse(response);
            jQuery('#wpvs-updating-box').fadeOut();
            if(user_response.redirect != null) {
                window.location.href = user_response.redirect;
            } else {
                window.location.reload();
            }
        },
        error: function(error) {
            jQuery('#wpvs-updating-box').fadeOut();
            jQuery('#rvs-new-account-error').html(error.responseText).show();
        }
    });
}

function wpvs_check_new_user_fields() {
    jQuery('input, select, textarea').removeClass('rvs-field-error');
    var can_submit = true;
    if(wpvsna.usernames && jQuery('#new_user_name').length > 0 && jQuery('#new_user_name').val() == "") {
        jQuery('#new_user_name').addClass('rvs-field-error');
        can_submit = false;
    }
    if(wpvsna.firstlast) {
        if(jQuery('#new_user_first_name').length > 0 && jQuery('#new_user_first_name').val() == "") {
            jQuery('#new_user_first_name').addClass('rvs-field-error');
            can_submit = false;
        }
        if(jQuery('#new_user_last_name').length > 0 && jQuery('#new_user_last_name').val() == "") {
            jQuery('#new_user_last_name').addClass('rvs-field-error');
            can_submit = false;
        }
    }
    if(jQuery('#new_user_email').val() == "") {
        jQuery('#new_user_email').addClass('rvs-field-error');
        can_submit = false;
    }
    if(wpvsna.passcreation) {
        if(jQuery('#new_user_password').val() == "") {
            jQuery('#new_user_password').addClass('rvs-field-error');
            can_submit = false;
        }

        if(jQuery('#confirm_user_password').val() == "") {
            jQuery('#confirm_user_password').addClass('rvs-field-error');
            can_submit = false;
        }
    }
    if(wpvsna.agreement_box && ! jQuery('#wpvs_agreement_checkbox').is(':checked')) {
        can_submit = false;
    }

    return can_submit;
}

function wpvs_verify_google_recaptcha(recaptcha_code) {
    jQuery.ajax({
        url: wpvsna.ajax,
        type: 'POST',
        cache: false,
	    headers: { "cache-control": "no-cache" },
        data: {
            'action': 'wpvs_verify_google_recaptcha_areq',
            'recaptcha_code':  recaptcha_code
        },
        success:function(response) {
            recaptcha_response = jQuery.parseJSON(response);
            jQuery('#wpvs-updating-box').fadeOut();
            if( recaptcha_response.success ) {
                jQuery('#rvs-create-new-account').removeClass('wpvs-ca-disabled-recaptcha').attr('disabled', false);
            }
        },
        error: function(error) {
            jQuery('#wpvs-updating-box').fadeOut();
            jQuery('#rvs-new-account-error').html(error.responseText).show();
            return false;
        }
    });
}
