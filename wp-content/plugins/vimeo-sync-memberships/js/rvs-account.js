jQuery(document).ready(function() {
    jQuery('#rvs-next').click( function() {
        show_rvs_updating(rvssettings.rvsmessage.payments+'...');
        var stripe_charge = jQuery('#payments tr:last').find('.wpvs-stripe-charge').attr('id');
        rvsGetStripePayments(stripe_charge);
    });
    removeMembershipButtons();
    removePayPalMembershipButtons();
    removeCoinMembershipButtons();
    removeCoinBaseMembershipButtons();
    jQuery('#wpvs-open-account-menu').click(function() {
        jQuery('#wpvs-account-menu').slideToggle();
    });

    jQuery('.wpvs-menu-item').click( function(e) {
        var show_section = jQuery(this).data('section');
        if(jQuery(this).hasClass('disable-link')) {
            e.preventDefault();
            jQuery('.wpvs-menu-item, .wpvs-account-section').removeClass('active');
            jQuery(this).addClass('active');
            jQuery('#'+show_section).addClass('active');
        }
        if( jQuery(window).width() < 768 ) {
            jQuery('#wpvs-account-menu').slideUp();
        }
    });

    jQuery('#wpvs-open-memberships').click(function() {
        if (history.pushState) {
            var new_account_url = window.location.protocol + "//" + window.location.host + window.location.pathname;
            window.history.pushState({path:new_account_url},'',new_account_url);
        }
    });

    jQuery('#wpvs-open-cards').click(function() {
        if (history.pushState) {
            var new_account_url = window.location.protocol + "//" + window.location.host + window.location.pathname + '?wpvsview=cards';
            window.history.pushState({path:new_account_url},'',new_account_url);
        }
    });

    jQuery('#wpvs-open-payments').click(function() {
        if (history.pushState) {
            var new_account_url = window.location.protocol + "//" + window.location.host + window.location.pathname + '?wpvsview=payments';
            window.history.pushState({path:new_account_url},'',new_account_url);
        }
    });

    jQuery('#wpvs-open-information').click(function() {
        if (history.pushState) {
            var new_account_url = window.location.protocol + "//" + window.location.host + window.location.pathname + '?wpvsview=info';
            window.history.pushState({path:new_account_url},'',new_account_url);
        }
    });

    jQuery('#wpvs-show-card-form').click( function() {
        jQuery('#wpvs-add-credit-card-form').slideToggle();
    });

    jQuery('.removeManMembership').click(function() {
        var updateThis = jQuery(this).parent().parent();
        var plan_id = updateThis.find('.membership-sub-id').val();
        if(window.confirm(rvsrequests.rvsmessage.membershipdc)) {
            show_rvs_updating(rvsrequests.rvsmessage.membershipc+'...');
            jQuery.ajax({
                type: "POST",
                url: rvsrequests.ajax,
                data: {
                    'action': 'rvs_remove_man_plan',
                    'plan_id': plan_id
                },
                success:function(response) {
                    updateThis.fadeOut();
                    wpvs_is_last_user_membership();
                    jQuery('#wpvs-updating-box').fadeOut('fast');
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }
    });

    if(rvssettings.stripeinvoices) {
        jQuery('body').delegate('.wpvs-download-invoice', 'click', function() {
            var invoice_id = jQuery(this).data('invoice');
            if( invoice_id != "") {
                wpvs_download_customer_invoice(invoice_id);
            }
        });
    }

    jQuery('.wpvs-stripe-paynow').click( function() {
        show_rvs_updating(rvssettings.rvsmessage.gettingpaymenturl+'...');
        var subscription_id = jQuery(this).data('subid');
        jQuery.ajax({
            type:"POST",
            url: rvssettings.ajax,
            data: {
                'action': 'wpvs_get_stripe_subscription_payment_url',
                'subscription_id': subscription_id
            },
            success:function(response) {
                jQuery('#wpvs-updating-box').fadeOut('fast');
                var subscription_data = jQuery.parseJSON(response);
                if( subscription_data.payment_url ) {
                   window.open(subscription_data.payment_url, '_blank');
                }
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    });
});

jQuery(window).resize(function() {
    if( jQuery(window).width() >= 768 ) {
        jQuery('#wpvs-account-menu').slideDown();
    }
})

function rvsGetStripePayments(charge_id) {
    jQuery.ajax({
        url: rvssettings.ajax,
        data: {
            'action': 'wpvs_get_customer_invoices_ajax_request',
            'chargeId': charge_id
        },
        success:function(response) {
            jQuery('#wpvs-updating-box').fadeOut('fast');
            var wpvs_customer_payments;
            var wpvs_new_payments = "";
            if(response != "") {
                wpvs_customer_payments = jQuery.parseJSON(response);
            }
            if(wpvs_customer_payments != null) {
                jQuery.each(wpvs_customer_payments, function(index, payment) {
                    var refund = "";
                    if(payment.charge_refund) {
                        refund = rvssettings.rvsmessage.refunded;
                    } else {
                        if(payment.charge_status == "succeeded") {
                            refund = rvssettings.rvsmessage.paid;
                        } else {
                            refund = payment.charge_status;
                        }
                    }
                    wpvs_new_payments += '<tr><td>' + rvssettings.currency + ' ' +  parseFloat(payment.charge_amount).toFixed(2) + '</td><td>' + payment.charge_date + '</td><td id="'+payment.charge_id+'" class="wpvs-stripe-charge">'+refund+'</td>';
                    if( rvssettings.stripeinvoices ) {
                        if( payment.charge_invoice != null && payment.charge_invoice != "" ) {
                            wpvs_new_payments += '<td><label class="wpvs-download-invoice" data-invoice="'+payment.charge_invoice+'"><span class="dashicons dashicons-media-text"></span></label></td>';
                        }
                    }
                    wpvs_new_payments += '</tr>';

                });

                if( wpvs_new_payments != "" ) {
                    jQuery('#payments tr:last').after(wpvs_new_payments);
                }

                if (wpvs_customer_payments.length < 10) {
                    jQuery('#rvs-next').hide();
                } else {
                    jQuery('#rvs-next').css('opacity', '1');
                }
            } else {
                jQuery('#rvs-payment-nav').html('<p>'+rvssettings.rvsmessage.nomorepayments+'</p>');
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function removePayPalMembershipButtons() {
    jQuery('.removePayPalPlan').click(function() {
        var update_table_row = jQuery(this).parent().parent();
        var plan_id = update_table_row.find('.membership-sub-id').val();
        var paypal_id = update_table_row.find('.paypal-id').val();
        if(window.confirm(rvssettings.rvsmessage.deletepaypal)) {
            wpvs_update_paypal_agreement(plan_id, paypal_id, 'delete', update_table_row, false);
        }
    });

    jQuery('.wpvs-paypal-update-membership').change( function() {
        var select_button = jQuery(this);
        var update_table_row = jQuery(this).parent().parent();
        var new_status = select_button.val();
        if(new_status != "") {
            var plan_id = update_table_row.find('.membership-sub-id').val();
            var paypal_id = update_table_row.find('.paypal-id').val();
            if(new_status == 'cancel') {
                if(window.confirm(rvssettings.rvsmessage.cancelpaypal)) {
                   wpvs_update_paypal_agreement(plan_id, paypal_id, new_status, false, select_button);
                }
            } else {
                wpvs_update_paypal_agreement(plan_id, paypal_id, new_status, false, select_button);
            }
        }
    });
}

function wpvs_update_paypal_agreement(plan_id, paypal_id, status, delete_button, select_button) {
    var updating_message = rvssettings.rvsmessage.cancellingpp;
    if(status == "reactivate") {
        updating_message = rvssettings.rvsmessage.reactivate;
    }
    if(status == "suspend") {
        updating_message = rvssettings.rvsmessage.suspend;
    }
    if(status == "delete") {
        updating_message = rvssettings.rvsmessage.delete;
    }
    show_rvs_updating(updating_message+'...');
    jQuery.ajax({
        url: rvssettings.ajax,
        data: {
            'action': 'rvs_remove_billing_agreement',
            'planid': plan_id,
            'paypalid': paypal_id,
            'new_status': status
        },
        success:function(response) {
            if(delete_button) {
                delete_button.remove();
                wpvs_is_last_user_membership();
            }
            if(select_button) {
                if(status == "suspend") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.pausedtext+'</option><option value="reactivate">'+rvsrequests.rvsmessage.reactivatetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }

                if(status == "reactivate") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.activetext+'</option><option value="suspend">'+rvsrequests.rvsmessage.pausetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }

                if(status == "cancel") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.cancelledtext+'</option>');
                }

            }
            jQuery('#wpvs-updating-box').fadeOut('fast');
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function removeMembershipButtons() {
    jQuery('.removeMembership').click(function() {
        var update_table_row = jQuery(this).parent().parent();
        var plan_id = update_table_row.find('.membership-sub-id').val();
        var user_id = rvsrequests.user.id;
        if(window.confirm(rvssettings.rvsmessage.deletepaypal)) {
            wpvs_update_stripe_subscription(plan_id, user_id, 'delete', update_table_row, false);
        }
    });

    jQuery('.wpvs-stripe-update-membership').change( function() {
        var select_button = jQuery(this);
        var update_table_row = jQuery(this).parent().parent();
        var new_status = select_button.val();
        if(new_status != "") {
            var plan_id = update_table_row.find('.membership-sub-id').val();
            var user_id = rvsrequests.user.id;
            if(new_status == 'cancel') {
                if(window.confirm(rvssettings.rvsmessage.cancelpaypal)) {
                   wpvs_update_stripe_subscription(plan_id, user_id, new_status, false, select_button);
                }
            } else {
                wpvs_update_stripe_subscription(plan_id, user_id, new_status, false, select_button);
            }

        }
    });
}

function removeCoinMembershipButtons() {
    jQuery('.removeCoinMembership').click(function() {
        var update_table_row = jQuery(this).parent().parent();
        var order_id = update_table_row.find('.membership-sub-id').val();
        var user_id = rvsrequests.user.id;
        if(window.confirm(rvssettings.rvsmessage.deletepaypal)) {
            wpvs_update_coin_subscription(order_id, user_id, 'delete', update_table_row, false);
        }
    });

    jQuery('.wpvs-coingate-update-membership').change( function() {
        var select_button = jQuery(this);
        var update_table_row = jQuery(this).parent().parent();
        var new_status = select_button.val();
        if(new_status != "") {
            var order_id = update_table_row.find('.membership-sub-id').val();
            var user_id = rvsrequests.user.id;
            if(new_status == 'cancel') {
                if(window.confirm(rvssettings.rvsmessage.cancelpaypal)) {
                   wpvs_update_coin_subscription(order_id, user_id, new_status, false, select_button);
                }
            } else {
                wpvs_update_coin_subscription(order_id, user_id, new_status, false, select_button);
            }

        }
    });
}

function removeCoinBaseMembershipButtons() {
    jQuery('.removeCoinBaseMembership').click(function() {
        var update_table_row = jQuery(this).parent().parent();
        var charge_id = update_table_row.find('.membership-sub-id').val();
        var user_id = rvsrequests.user.id;
        if(window.confirm(rvssettings.rvsmessage.deletepaypal)) {
            wpvs_update_coinbase_subscription(charge_id, user_id, 'delete', update_table_row, false);
        }
    });

    jQuery('.wpvs-coinbase-update-membership').change( function() {
        var select_button = jQuery(this);
        var update_table_row = jQuery(this).parent().parent();
        var new_status = select_button.val();
        if(new_status != "") {
            var charge_id = update_table_row.find('.membership-sub-id').val();
            var user_id = rvsrequests.user.id;
            if(new_status == 'cancel') {
                if(window.confirm(rvssettings.rvsmessage.cancelpaypal)) {
                   wpvs_update_coinbase_subscription(charge_id, user_id, new_status, false, select_button);
                }
            } else {
                wpvs_update_coinbase_subscription(charge_id, user_id, new_status, false, select_button);
            }

        }
    });
}

function wpvs_update_stripe_subscription(plan_id, user_id, status, delete_button, select_button) {
    var updating_message = rvsrequests.rvsmessage.membershipc;
    if(status == "delete") {
        updating_message = rvsrequests.rvsmessage.membershipd;
    }
    if(status == "reactivate") {
        updating_message = rvsrequests.rvsmessage.reactivate;
    }
    show_rvs_updating(updating_message+'...');
    jQuery.ajax({
        url: rvssettings.ajax,
        data: {
            'action': 'wpvs_update_membership_ajax_request',
            'planId': plan_id,
            'new_status': status
        },
        success:function(response) {
            if(delete_button) {
                delete_button.remove();
                wpvs_is_last_user_membership();
            }
            if(select_button) {
                if(status == "cancel") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.cancelledtext+'</option><option value="reactivate">'+rvsrequests.rvsmessage.reactivatetext+'</option>');
                }
                if(status == "reactivate") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.activetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }
            }
            jQuery('#wpvs-updating-box').fadeOut('fast');
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_update_coin_subscription(order_id, user_id, status, delete_button, select_button) {
    var updating_message = rvsrequests.rvsmessage.membershipc;
    if(status == "delete") {
        updating_message = rvsrequests.rvsmessage.membershipd;
    }
    if(status == "reactivate") {
        updating_message = rvsrequests.rvsmessage.reactivate;
    }
    show_rvs_updating(updating_message+'...');
    jQuery.ajax({
        url: rvssettings.ajax,
        type: 'POST',
        data: {
            'action': 'wpvs_remove_coin_access_ajax_request',
            'order_id': order_id,
            'new_status': status
        },
        success:function(response) {
            if(delete_button) {
                delete_button.remove();
                wpvs_is_last_user_membership();
            }
            if(select_button) {
                if(response == "cancel") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.cancelledtext+'</option><option value="reactivate">'+rvsrequests.rvsmessage.reactivatetext+'</option>');
                }
                if(response == "reactivate") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.activetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }
                if(response == "pending") {
                    window.location.reload();
                }
            }
            jQuery('#wpvs-updating-box').fadeOut('fast');
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_update_coinbase_subscription(charge_id, user_id, status, delete_button, select_button) {
    var updating_message = rvsrequests.rvsmessage.membershipc;
    if(status == "delete") {
        updating_message = rvsrequests.rvsmessage.membershipd;
    }
    if(status == "reactivate") {
        updating_message = rvsrequests.rvsmessage.reactivate;
    }
    show_rvs_updating(updating_message+'...');
    jQuery.ajax({
        url: rvssettings.ajax,
        type: 'POST',
        data: {
            'action': 'wpvs_remove_coinbase_access_ajax_request',
            'charge_id': charge_id,
            'new_status': status
        },
        success:function(response) {
            if(delete_button) {
                delete_button.remove();
                wpvs_is_last_user_membership();
            }
            if(select_button) {
                if(response == "cancel") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.cancelledtext+'</option><option value="reactivate">'+rvsrequests.rvsmessage.reactivatetext+'</option>');
                }
                if(response == "reactivate") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.activetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }
                if(response == "pending") {
                    window.location.reload();
                }
            }
            jQuery('#wpvs-updating-box').fadeOut('fast');
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_download_customer_invoice(invoice_id) {
    show_rvs_updating(rvssettings.rvsmessage.gettinginvoice+'...');
    jQuery.ajax({
        url: rvssettings.ajax,
        data: {
            'action': 'wpvs_get_customer_single_invoice',
            'invoice_id': invoice_id
        },
        success:function(response) {
            jQuery('#wpvs-updating-box').fadeOut('fast');
            if( response != "") {
               window.open(response, '_blank');
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_is_last_user_membership() {
    if( jQuery('.rvs-membership-editor').length < 1 ) {
        window.location.reload();
    }
}
