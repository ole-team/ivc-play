var wpvs_customer_billing_info_saved = false;
jQuery(document).ready(function() {
    if( jQuery('#wpvs-billing-info-form').length > 0 ) {
        jQuery('#wpvs-billing-info-form').submit(function(e) {
            e.preventDefault();
            if( ! wpvs_billing_info_errors() ) {
                wpvs_save_customer_billing_information();
            }
        });

        jQuery('#wpvs-billing-info-form input, #wpvs-billing-info-form select').change(function(e) {
            if( jQuery('#wpvs-payment-total-section').length > 0  ) {
                jQuery('#wpvs-payment-total-section').slideUp();
                jQuery('#wpvs-proceed-button').show();
            }
        });
    } else {
        wpvs_customer_billing_info_saved = true;
    }

    if( jQuery('#wpvs-proceed-button').length > 0 ) {
        jQuery('#wpvs-proceed-button').click( function() {
            if( jQuery('#wpvs-billing-info-form').length > 0 ) {
                jQuery('#wpvs-billing-info-form').submit();
            } else {
                wpvs_get_total_checkout_amount();
            }
        });
    }

    if( jQuery('#rvs-apply-coupon').length > 0 ) {
        jQuery('#rvs-apply-coupon').click(function() {
            var coupon_code = jQuery('#rvs_coupon_code').val();
            var coupon_button = jQuery(this);
            var current_price = wpvscheckout.product_price;
            wpvscheckout.coupon_code = "";
            if(jQuery('.wpvs-paypal-subscription').length > 0) {
                jQuery('#wpvs-paypal-button').hide();
            }
            if( jQuery('.wpvs-stripe-checkout-coupon-allowed').length > 0 ) {
                jQuery('#wpvs-stripe-checkout-loaded').hide();
                jQuery('#wpvs-stripe-checkout-loading').show();
            }
            if( jQuery('#wpvs_coupon_code_added').length > 0 ) {
                jQuery('#wpvs_coupon_code_added').remove();
            }
            if( jQuery('#wpvs-proceed-button').length > 0 ) {
                jQuery('#wpvs-proceed-button').show();
            }
            jQuery('#wpvs-payment-total-section').slideUp();

            coupon_button.text(rvsrequests.rvsmessage.checkcoupon+'...');
            jQuery.ajax({
                url: rvsrequests.ajax,
                type: "POST",
                data: {
                    'action': 'wpvs_check_coupon_code',
                    'coupon_id': coupon_code,
                },
                success:function(response) {
                    var discount_price;
                    var discount_description = "<tr id='wpvs_coupon_code_added'><td>"+rvsrequests.rvsmessage.discount+"</td><td>";
                    var coupon_details = JSON.parse(response);
                    if(coupon_details.coupon) {
                        coupon_button.text(rvsrequests.rvsmessage.valid);
                        if(coupon_details.coupon.type == "amount") {
                            discount_price = current_price - coupon_details.coupon.amount;
                            if( rvsrequests.currency == "JPY" ) {
                                discount_description += (coupon_details.coupon.amount/100).toFixed(0);
                            } else {
                                discount_description += (coupon_details.coupon.amount/100).toFixed(2);
                            }
                        }

                        if(coupon_details.coupon.type == "percentage") {
                            var percent_off = current_price*(coupon_details.coupon.amount/100);
                            discount_price = current_price - percent_off;
                            discount_description += coupon_details.coupon.amount;
                            discount_description += "%";
                        }

                        discount_description += " " +rvsrequests.rvsmessage.discountoff+ " ";

                        if(discount_price < 0) {
                            discount_price = 0;
                        }

                        var price_precision = Math.pow(10, 2)
                        discount_price = (discount_price/100);
                        discount_price = Math.ceil(discount_price * price_precision) / price_precision;
                        if( wpvscheckout.currency == "JPY" ) {
                            discount_price = (discount_price).toFixed(0);
                        } else {
                            discount_price = (discount_price).toFixed(2);
                        }

                        if(coupon_details.coupon.duration == 'once') {
                            discount_description += rvsrequests.rvsmessage.durationo;
                        }

                        if(coupon_details.coupon.duration == 'repeating') {
                            discount_description += " "+rvsrequests.rvsmessage.durationfor+" ";
                            discount_description += coupon_details.coupon.month_duration;
                            discount_description += " "+rvsrequests.rvsmessage.durationm;
                        }

                        if(coupon_details.coupon.duration == 'forever') {
                            discount_description += " "+rvsrequests.rvsmessage.durationf;
                        }

                        discount_description += '</td><td id="wpvs_discount_price_label">';
                        discount_description += wpvscheckout.currency_label+discount_price + '</td>';


                        jQuery('#rvs-checkout-table tbody').append(discount_description);
                        wpvscheckout.coupon_code = coupon_code;

                        if( jQuery('.wpvs-stripe-checkout-page-notice').length > 0 ) {
                            jQuery('.wpvs-stripe-checkout-page-notice').append('<small class="wpvs-stripe-coupon-not-supported"><strong>*'+rvsrequests.rvsmessage.stripe.couponnotsupportedtext+'</strong></small>');
                        }

                    } else {
                        coupon_button.text(rvsrequests.rvsmessage.invalid);
                    }
                },
                error: function(response){
                    coupon_button.text(rvsrequests.rvsmessage.invalid);
                }
            });
        });
    }
});

function wpvs_save_customer_billing_information() {
    var updating_message = rvsrequests.rvsmessage.savingbilling;
    var billing_address = jQuery('#wpvs_billing_address').val();
    var billing_address_line_2 = jQuery('#wpvs_billing_address_line_2').val();
    var billing_city = jQuery('#wpvs_billing_city').val();
    var billing_state = jQuery('#wpvs_billing_state').val();
    var billing_zip_code = jQuery('#wpvs_billing_zip_code').val();
    var billing_country = jQuery('#wpvs_billing_country').val();
    var wpvs_action = jQuery('#wpvs_billing_save').val();
    var billing_nonce = jQuery('#wpvs_billing_nonce').val();

    show_rvs_updating(updating_message+'...');
    jQuery.ajax({
        url: rvsrequests.ajax,
        type:'POST',
        data: {
            'action': 'wpvs_save_billing_on_checkout',
            'wpvs_billing_address': billing_address,
            'wpvs_billing_address_line_2': billing_address_line_2,
            'wpvs_billing_city': billing_city,
            'wpvs_billing_state': billing_state,
            'wpvs_billing_zip_code': billing_zip_code,
            'wpvs_billing_country': billing_country,
            'wpvs_billing_save': wpvs_action,
            'wpvs_billing_nonce': billing_nonce

        },
        success:function(response) {
            jQuery('#wpvs-updating-box').fadeOut('fast');
            if( jQuery('.wpvs-stripe-card-form').length > 0 ) {
                jQuery('#new_address').val(billing_address);
                jQuery('#new_address_city').val(billing_city);
                jQuery('#new_province').val(billing_state);
                jQuery('#new_postal_code').val(billing_zip_code);
                jQuery('#address_country').val(billing_country);
            }
            wpvs_customer_billing_info_saved = true;
            if( jQuery('#wpvs-payment-total-section').length > 0  ) {
                wpvs_get_total_checkout_amount();
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_billing_info_errors() {
    var field_errors = false;
    jQuery('.wpvs-checkout-billing-fields input, .wpvs-checkout-billing-fields select').removeClass('rvs-field-error');
    var billing_address = jQuery('#wpvs_billing_address');
    var billing_city = jQuery('#wpvs_billing_city');
    var billing_state = jQuery('#wpvs_billing_state');
    var billing_zip_code = jQuery('#wpvs_billing_zip_code');
    var billing_country = jQuery('#wpvs_billing_country');

    if(billing_address.val() == "") {
        billing_address.addClass('rvs-field-error');
        field_errors = true;
    }

    if(billing_city.val() == "") {
        billing_city.addClass('rvs-field-error');
        field_errors = true;
    }

    if(billing_state.val() == "") {
        billing_state.addClass('rvs-field-error');
        field_errors = true;
    }

    if(billing_zip_code.val() == "") {
        billing_zip_code.addClass('rvs-field-error');
        field_errors = true;
    }

    if(billing_country.val() == "") {
        billing_country.addClass('rvs-field-error');
        field_errors = true;
    }

    return field_errors;
}

function wpvs_get_total_checkout_amount() {
    show_rvs_updating(rvsrequests.rvsmessage.calculatingtotal+'...');
    jQuery.ajax({
        url: rvsrequests.ajax,
        type:'GET',
        data: {
            'action': 'wpvs_get_checkout_total_amounts',
            'wpvs_checkout': wpvscheckout,
        },
        success:function(response) {
            wpvscheckout = JSON.parse(response);
            wpvs_checkout_amounts_html = "";
            jQuery('#wpvs-updating-box').fadeOut('fast');
            jQuery('#wpvs-checkout-total-amounts tbody').html('');
            wpvs_checkout_amounts_html += '<tr><td>'+rvsrequests.rvsmessage.subtotal+':</td><td>'+wpvscheckout.formatted_pricing.subtotal+'</td></tr>';
            if( wpvscheckout.taxes ) {
                jQuery(wpvscheckout.taxes).each( function(index, tax) {
                    wpvs_checkout_amounts_html += '<tr><td>'+tax.tax_name+'</td><td>'+tax.decimal_amount+'</td></tr>';
                });
            }
            wpvs_checkout_amounts_html += '<tr><td>'+rvsrequests.rvsmessage.total+':</td><td>'+wpvscheckout.formatted_pricing.total+'</td></tr>';
            jQuery('table#wpvs-checkout-total-amounts').append(wpvs_checkout_amounts_html);
            jQuery('.rvs-update-total').text(wpvscheckout.formatted_pricing.total);
            if( jQuery('.wpvs-choose-coin').length > 0) {
                var coin_currency = jQuery('.wpvs-choose-coin.active').data('coin');
                wpvs_coingate_convert_currency(coin_currency);
            }

            if(jQuery('.wpvs-update-product-price').length > 0) {
                jQuery('.wpvs-update-product-price').text(wpvscheckout.formatted_pricing.product_price);
            }

            if(jQuery('#payment-request-button').length > 0) {
                create_google_pay_button();
            }

            if( jQuery('.wpvs-stripe-checkout-box').length > 0 ) {
                wpvs_generate_stripe_checkout_button();
            }

            if(jQuery('#wpvs-paypal-purchase').length > 0 || jQuery('#wpvs-paypal-rental').length > 0) {
                wpvs_create_paypal_payment_token();
            }

            if(jQuery('.wpvs-paypal-subscription').length > 0) {
                wpvs_create_paypal_agreement_token();
                jQuery('#wpvs-paypal-button').show();
            }

            if( jQuery('.wpvs-stripe-checkout-coupon-allowed').length > 0 ) {
                wpvs_generate_stripe_checkout_button();
            }

            if(jQuery('#wpvs-coingate-payment').length > 0) {
                wpvs_check_coingate_pending_payments();
            }

            if( jQuery('#wpvs_coupon_code_added').length > 0 ) {
                jQuery('#wpvs_discount_price_label').text(wpvscheckout.currency_label+wpvscheckout.formatted_pricing.subtotal);
            }

            if( jQuery('#wpvs-payment-total-section').length > 0 && wpvs_customer_billing_info_saved ) {
                jQuery('#wpvs-proceed-button').hide();
                jQuery('#wpvs-payment-total-section').slideDown();
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}
