jQuery(document).ready(function() {

    var default_load = jQuery('#wpvs-load-purchase-type').val();
    jQuery('body').delegate('#rvs-close-error', 'click', function() {
        jQuery('#wpvs-updating-box').fadeOut('fast');
    });

    // GET ACCESS BUTTONS

    if(jQuery('#rvs-access-options').length > 0) {
        jQuery('.rvs-access-tab').click(function() {
            jQuery('.rvs-access-tab').removeClass('active');
            jQuery('.rvs-access-section').removeClass('active');
        });

        jQuery('#rvs-memberships-access-tab').click(function() {
            jQuery(this).addClass('active');
            jQuery('#rvs-membership-access').addClass('active');
        });

        jQuery('#rvs-purchase-access-tab').click(function() {
            var wpvs_access_tab = jQuery(this);
            wpvs_access_tab.addClass('active');
            jQuery('#rvs-purchase-access').addClass('active');
            if( wpvs_access_tab.hasClass('wpvs-single-purchase') ) {
                show_rvs_updating(rvsrequests.rvsmessage.gettingpurchase+"...");
                wpvs_product_change_purchase_type("purchase");
            }
            if(jQuery('.wpvs-term-purchases').length > 0) {
                 jQuery('.wpvs-term-purchases').show();
            }
        });

        jQuery('#rvs-rental-access-tab').click(function() {
            show_rvs_updating(rvsrequests.rvsmessage.gettingrental+"...");
            jQuery(this).addClass('active');
            jQuery('#rvs-purchase-access').addClass('active');
            wpvs_product_change_purchase_type("rental");
            if(jQuery('.wpvs-term-purchases').length > 0) {
                 jQuery('.wpvs-term-purchases').hide();
            }
        });

        if(default_load == 'rental') {
            wpvs_product_change_purchase_type("rental");
        }

    }
     jQuery('.wpvs-login-label').click(function() {
         var login_label = jQuery(this);
         var show_section = login_label.data('show');
         jQuery('.wpvs-login-label').removeClass('active');
         jQuery('.wpvs-login-section').removeClass('active');
         login_label.addClass('active');
         jQuery('#'+show_section).addClass('active');
     });


    jQuery('.wpvs-payment-type').click(function(){
        var pay_option_button = jQuery(this);
        var show_box = pay_option_button.data('paybox');
        jQuery('.wpvs-payment-type').removeClass('active');
        jQuery('.wpvs-payment-box').removeClass('active');
        pay_option_button.addClass('active');
        jQuery(show_box).addClass('active');
    });

});

function wpvs_product_change_purchase_type(type) {
    var product_id = wpvscheckout.product_id;
    if( jQuery('.wpvs-stripe-checkout-box').length > 0 ) {
        jQuery('#wpvs-stripe-checkout-link').data('type', type);
        jQuery('#wpvs-stripe-checkout-loaded').hide();
        jQuery('#wpvs-stripe-checkout-loading').show();
    }
    jQuery.ajax({
        url: rvsrequests.ajax,
        type: "POST",
        data: {
            'action': 'wpvs_product_change_purchase_type',
            'type': type,
            'video': product_id,
            'coupon_code': wpvscheckout.coupon_code
        },
        success:function(response) {
            jQuery('#wpvs-updating-box').fadeOut('fast');
            var new_price_details = JSON.parse(response);
            var new_price;
            wpvscheckout.purchase_type = new_price_details.type;
            if(new_price_details.type == "purchase") {
                jQuery('#rvs-purchase-title').html(rvsrequests.rvsmessage.purchasetext);
                if(jQuery('#wpvs-paypal-purchase').length > 0) {
                    jQuery('#wpvs-paypal-purchase').removeClass('wpvs-hide-paypal-button');
                }
                if(jQuery('#wpvs-paypal-rental').length > 0) {
                    jQuery('#wpvs-paypal-rental').addClass('wpvs-hide-paypal-button');
                }
            }
            if(new_price_details.type == "rental") {
                jQuery('#rvs-purchase-title').html(rvsrequests.rvsmessage.rentaltext);
                if(jQuery('#wpvs-paypal-rental').length > 0) {
                    jQuery('#wpvs-paypal-rental').removeClass('wpvs-hide-paypal-button');
                }
                if(jQuery('#wpvs-paypal-purchase').length > 0) {
                    jQuery('#wpvs-paypal-purchase').addClass('wpvs-hide-paypal-button');
                }
            }

            jQuery('#wpvs-purchase-details').html(new_price_details.description);
            new_price = new_price_details.price;

            if( rvsrequests.currency == "JPY" ) {
                new_price = (new_price/100).toFixed(0);
            } else {
                new_price = (new_price/100).toFixed(2);
            }
            if(new_price < 0) {
                new_price = 0;
            }
            wpvs_get_total_checkout_amount();

        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_convert_custom_data_to_json(custom_data) {
   var json_data = {};
   jQuery.each(custom_data, function() {
       if (json_data[this.name]) {
           if (!json_data[this.name].push) {
               json_data[this.name] = [json_data[this.name]];
           }
           json_data[this.name].push(this.value || '');
       } else {
           json_data[this.name] = this.value || '';
       }
   });
   return json_data;
}
