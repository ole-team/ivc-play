jQuery(document).ready(function() {
    var next_offset = 0;
    jQuery('.rvs-more-payments').click( function() {
        next_offset += 50;
        rvs_load_admin_payments(next_offset);
    });
    
    jQuery('.rvs-back-payments').click( function() {
        next_offset -= 50;
        rvs_load_admin_payments(next_offset);
    });
    
    jQuery('#import-stripe-payments').click( function() {
        if(window.confirm('Are you sure you want to import your Stripe payments?')) {
            wpvs_admin_import_stripe_payments(null);
        }
    });
    
    wpvs_create_admin_refund_buttons();
    wpvs_admin_load_payments_overview();
    
    jQuery('.wpvs-get-gateway-payments').change( function() {
        rvs_load_admin_payments(0);
    });
    
    jQuery('#wpvs-filter-coupon-payments').change( function() {
        rvs_load_admin_payments(0);
    });
});

function rvs_load_admin_payments(offset) {
    if(offset <= 0) {
        offset = 0;
        jQuery('.rvs-previous-payments').hide();
    }
    
    var restrict_gateway = "";
    
    if( jQuery('input[name="wpvs-load-stripe-payments"]').is(':checked') && ! jQuery('input[name="wpvs-load-paypal-payments"]').is(':checked') ) {
        restrict_gateway = 'stripe';
    }
    
    if( ! jQuery('input[name="wpvs-load-stripe-payments"]').is(':checked') && jQuery('input[name="wpvs-load-paypal-payments"]').is(':checked') ) {
        restrict_gateway = 'paypal';
    }
    
    var coupon_id = jQuery('#wpvs-filter-coupon-payments').val();
    
    jQuery.ajax({
        url: rvsrequests.ajax,
        data: {
            'action': 'rvs_admin_load_payments',
            'offset': offset,
            'gateways': restrict_gateway,
            'coupon_id': coupon_id
        },
        success:function(response) {
            jQuery('#wpvs-updating-box').fadeOut('fast');
            jQuery('#error').hide();
            var rvs_found_payments;
            if(response != "") {
                rvs_found_payments = jQuery.parseJSON(response);
            }
            if(rvs_found_payments.count > 0) {
                jQuery('.rvs-payment-data').remove();
                jQuery('#payments tbody').html('<tr><th>User</th><th>Date</th><th>Amount</th><th>Coupon</th><th>Gateway</th><th>Refund</th></tr>');
                var rvs_payments = rvs_found_payments.payments;
                jQuery.each(rvs_payments, function(index, payment) {
                    var refund = "";
                    var table_data = "";
                    if(payment.refunded) {
                        refund = '<label id="' + payment.paymentid + '"><em>Refunded</em></label>';
                    } else {
                        var refund_type = 'stripe-refund';
                        if(payment.gateway == "paypal") {
                            refund_type = 'paypal-refund';
                        }
                        refund = '<label class="issue-refund '+refund_type+'" id="' + payment.paymentid + '">Refund</label>';
                    }
                    table_data = '<tr class="rvs-payment-data"><td>'+payment.user+'</td><td>'+payment.date+'</td><td>'+payment.amount+'</td><td>'+payment.coupon+'</td><td>'+payment.gateway+'</td><td>'+refund+'</td></tr>';
                    jQuery('#payments tbody').append(table_data);
                });
                if(rvs_found_payments.count < 50) {
                    jQuery('.rvs-next-payments').hide();
                } else {
                    jQuery('.rvs-next-payments').show();
                }
                if(offset > 0) {
                    jQuery('.rvs-previous-payments').show();
                }
            } else {
                jQuery('#payments tbody').html('<td id="wpvs-no-payments-found">No More Payments</td>');
                jQuery('.rvs-next-payments').hide();
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_admin_load_payments_overview() {
    jQuery.ajax({
        url: rvsrequests.ajax,
        data: {
            'action': 'wpvs_admin_load_timeframe_payments'
        },
        success:function(response) {
            var payments_data = jQuery.parseJSON(response);
            jQuery('#wpvs-last-week-payments').text(payments_data.last_week);
            
            jQuery('#wpvs-this-month-payments').text(payments_data.this_month);
            
            jQuery('#wpvs-this-year-payments').text(payments_data.this_year);
            
            jQuery('.wpvs-retrieving-payment-data').remove();
            jQuery('.wpvs-admin-amount-made').show();   
        },
        error: function(response){
            show_rvs_error(response.responseText);
            jQuery('.wpvs-retrieving-payment-data').remove();
        }
    });
}

function wpvs_admin_import_stripe_payments(start_at_charge) {
    show_rvs_updating('Importing Stripe Payments...please wait');
    jQuery.ajax({
        url: rvsrequests.ajax,
        type: 'POST',
        data: {
            'action': 'wpvs_admin_sync_all_stripe_payments',
            'start_at': start_at_charge
        },
        success:function(response) {
            show_rvs_updating('Imported!');
            var payment_details = JSON.parse(response);
            if( payment_details.more ) {
                wpvs_admin_import_stripe_payments(payment_details.start_at);
            } else {
                location.reload();
            }
            
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}