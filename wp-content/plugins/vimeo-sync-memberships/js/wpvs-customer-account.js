jQuery(document).ready(function() {
    jQuery('#rvs-account-details').submit( function(e) {
        e.preventDefault();
        if(wpvs_check_update_user_fields()) {
            var first_name = jQuery('#new_user_first_name').val();
            var last_name = jQuery('#new_user_last_name').val();
            var password;
            var confirmp;
            var email =  jQuery('#new_user_email').val();

            if( jQuery('#new_user_password').val() != "") {
                var password = jQuery('#new_user_password').val();
                var confirmp = jQuery('#confirm_user_password').val();
            }
            wpvs_update_customer_account(email, password, confirmp, first_name, last_name);
        }
    });
});

function wpvs_update_customer_account(email, password, confirmp, first_name, last_name) {
    jQuery('#rvs-create-new-account').attr('disabled', true);
    var nonce_check = jQuery('#wpvs_account_nonce').val();
    jQuery('#rvs-new-account-error').html("").hide();
    show_rvs_updating(wpvsacc.updatingaccount+'...');
    var wpvs_custom_account_data = wpvs_convert_custom_data_to_json(jQuery('form#rvs-account-details').serializeArray());
    jQuery.ajax({
        url: wpvsacc.ajax,
        type: 'POST',
        data: {
            'action': 'wpvs_update_customer_account_info',
            'new_user_email': email,
            'new_user_password': password,
            'confirm_user_password': confirmp,
            'wpvs_first_name': first_name,
            'wpvs_last_name': last_name,
            'wpvs_account_nonce': nonce_check,
            'wpvs_custom_account_fields': wpvs_custom_account_data
        },
        success:function(response) {
            jQuery('#rvs-create-new-account').attr('disabled', false);
            if( response != "") {
                show_rvs_updating(response);
            }
            setTimeout(function() {
                jQuery('#wpvs-updating-box').fadeOut();
            }, 400);
            jQuery('#new_user_password').val('');
            jQuery('#confirm_user_password').val('');
        },
        error: function(error) {
            jQuery('#rvs-create-new-account').attr('disabled', false);
            jQuery('#wpvs-updating-box').fadeOut();
            jQuery('#rvs-new-account-error').html(error.responseText).show();
            jQuery('#new_user_password').val('');
            jQuery('#confirm_user_password').val('');
        }
    });
}

function wpvs_check_update_user_fields() {
    jQuery('input, select, textarea').removeClass('rvs-field-error');
    var input_errors = true;

    if(jQuery('#new_user_email').val() == "") {
        jQuery('#new_user_email').addClass('rvs-field-error');
        input_errors = false;
    }

    if(jQuery('#new_user_password').val() == "" && jQuery('#confirm_user_password').val() != "") {
        jQuery('#new_user_password').addClass('rvs-field-error');
        input_errors = false;
    }

    if(jQuery('#confirm_user_password').val() == "" && jQuery('#new_user_password').val() != "") {
        jQuery('#confirm_user_password').addClass('rvs-field-error');
        input_errors = false;
    }

    return input_errors;
}
