jQuery(document).ready(function() {
    removePayPalMembershipButtons();
    removeMembershipButtons();
    removeCoinMembershipButtons();
    removeCoinBaseMembershipButtons();
    jQuery('#new-membership').click(function() {
        if(!check_new_membership_fields()) {
            var new_plan_id = jQuery('#id_of_plan').val();
            var new_plan_name = jQuery('#name_of_plan').val();
            var new_plan_price = jQuery('#price_of_plan').val();
            var new_plan_description = jQuery('#description_of_plan').val();
            var short_description = jQuery('#short_description_of_plan').val();
            var new_plan_interval = jQuery('#interval_of_plan').val();
            var new_plan_interval_count = jQuery('#plan_interval_count').val();
            var new_trial_frequency = jQuery('#trial_period').val();
            var new_trial_period = jQuery('#trial_period_days').val();

            if(new_trial_frequency == "none") {
                new_trial_period = 0;
            }
            var hide_membership = 0;
            if(jQuery('#hide_membership').is(':checked')) {
               hide_membership = 1;
            }

            var create_role = 0;
            if(jQuery('#create_membership_role').is(':checked')) {
               create_role = 1;
            }


            show_rvs_updating("Creating new membership...");
            jQuery.ajax({
                url: wpvsadmindata.ajax,
                type: "POST",
                data: {
                    'action': 'rvs_create_new_membership',
                    'id_of_plan': new_plan_id,
                    'name_of_plan': new_plan_name,
                    'price_of_plan': new_plan_price,
                    'description_of_plan': new_plan_description,
                    'short_description': short_description,
                    'interval_of_plan': new_plan_interval,
                    'interval_count': new_plan_interval_count,
                    'trial_frequency': new_trial_frequency,
                    'trial_period': new_trial_period,
                    'hide_membership': hide_membership,
                    'create_role': create_role
                },
                success:function(response) {
                    show_rvs_updating("Membership Created!");
                    window.location.href = response;
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }

    });

    jQuery('#trial_period').change(function() {
        var trial_val = jQuery(this).val();
        if(trial_val == "none") {
            jQuery('#trial_period_days').hide().val(0);
            jQuery('#trial-amount-container').hide();
        } else {
            jQuery('#trial_period_days').show();
            jQuery('#trial-amount-container').show();
        }
        if(trial_val == "day") {
            jQuery('#trial_period_days').attr('placeholder', 'Number of days');
        }

        if(trial_val == "week") {
            jQuery('#trial_period_days').attr('placeholder', 'Number of weeks');
        }

        if(trial_val == "month") {
            jQuery('#trial_period_days').attr('placeholder', 'Number of months');
        }
    });

    jQuery('#trial_period_amount').change(function() {
        var trial_val = jQuery(this).val();
        if(trial_val == "free") {
            jQuery('#price_of_trial').hide();
        } else {
            jQuery('#price_of_trial').show();
        }
    });

    jQuery('#delete-membership').click(function() {
        jQuery('#wpvs-deletion-options').slideDown();
    });

    jQuery('#confirm-delete-membership').click(function() {
        if(window.confirm("Are you sure you want to delete this plan?")) {
            show_rvs_updating("Deleting membership...");
            var delete_plan_id = jQuery('#id_of_plan').val();
            var remove_user_plans_box = jQuery('#wpvs_remove_user_plans');
            var remove_role_box = jQuery('#wpvs_remove_role');
            var remove_stripe_plan_box = jQuery('#wpvs_delete_stripe_plan');
            var remove_user_plans = 0;
            var remove_role = 0;
            var remove_stripe_plan = 0;
            if( remove_user_plans_box.is(':checked') ) {
               remove_user_plans = 1;
            }
            if( remove_role_box.is(':checked') ) {
               remove_role = 1;
            }
            if( remove_stripe_plan_box.is(':checked') ) {
                remove_stripe_plan = 1;
            }
            jQuery.ajax({
                url: wpvsadmindata.ajax,
                type: "POST",
                data: {
                    'action': 'rvs_delete_membership',
                    'plan_id': delete_plan_id,
                    'remove_user_plans': remove_user_plans,
                    'remove_role': remove_role,
                    'remove_stripe_plan':remove_stripe_plan
                },
                success:function(response) {
                    show_rvs_updating("Membership deleted.");
                    window.location.href = response;
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }
    });

    jQuery('#save-membership').click(function() {
        if(!check_edit_membership_fields()) {
            jQuery('#save-membership').html('Saving...');
            var plan_id = jQuery('#id_of_plan').val();
            var plan_name = jQuery('#name_of_plan').val();
            var plan_description = jQuery('#description_of_plan').val();
            var short_description = jQuery('#short_description_of_plan').val();
            var new_trial_frequency = jQuery('#trial_period').val();
            var new_trial_period = jQuery('#trial_period_days').val();
            var hide_membership = 0;
            if(jQuery('#hide_membership').is(':checked')) {
               hide_membership = 1;
            }
            var create_role = 0;
            if(jQuery('#create_membership_role').is(':checked')) {
               create_role = 1;
            }
            jQuery.ajax({
                url: wpvsadmindata.ajax,
                type: "POST",
                data: {
                    'action': 'rvs_update_membership',
                    'id_of_plan': plan_id,
                    'name_of_plan': plan_name,
                    'description_of_plan': plan_description,
                    'short_description': short_description,
                    'trial_frequency': new_trial_frequency,
                    'trial_period': new_trial_period,
                    'hide_membership': hide_membership,
                    'create_role': create_role
                },
                success:function(response) {
                    jQuery('#save-membership').html('Saved!');
                    window.location.href = wpvsadmindata.adminurl + 'admin.php?page=rvs-memberships';
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }

    });

    jQuery('#rvs_test_mode').change(function() {
        show_rvs_updating('Changing mode...');
        var test_mode = jQuery(this).val();
        if(test_mode == "on") {
            test_mode = "off";
            jQuery(this).val("off");
        } else {
            test_mode = "on";
            jQuery(this).val("on");
        }
        jQuery.ajax({
            url: wpvsadmindata.ajax,
            type: "POST",
            data: {
                'action': 'rvs_update_test_mode',
                'test_mode': test_mode
            },
            success:function(response) {
                jQuery('#wpvs-updating-box').hide();
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });

    });

    jQuery('#rvs_currency_setting').change(function() {
        show_rvs_updating('Changing currency...');
        var new_currency = jQuery(this).val();
        var currency_label = jQuery('#wpvs_custom_currency').val();
        jQuery.ajax({
            url: wpvsadmindata.ajax,
            type: "POST",
            data: {
                'action': 'rvs_update_currency',
                'currency': new_currency,
                'custom_label': currency_label
            },
            success:function(response) {
                jQuery('#wpvs-updating-box').hide();
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    });

    jQuery('#wpvs-save-custom-currency').click(function() {
        show_rvs_updating('Saving custom currency text...');
        var currency_label = jQuery('#wpvs_custom_currency').val();
        var currency_position = jQuery('#wpvs_currency_position_setting').val();
        jQuery.ajax({
            url: wpvsadmindata.ajax,
            type: "POST",
            data: {
                'action': 'rvs_update_currency_text',
                'custom_label': currency_label,
                'currency_position': currency_position
            },
            success:function(response) {
                jQuery('#wpvs-updating-box').hide();
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    });



    jQuery('#rvs_payment_gateway').change(function() {
        var gateway = jQuery(this).val();
        if(gateway == "paypal") {
            gateway = "stripe";
            jQuery(this).val("stripe");
        } else {
            gateway = "paypal";
            jQuery(this).val("paypal");
        }
        show_rvs_updating('Changing gateway to ' + gateway + '...');
        jQuery.ajax({
            url: wpvsadmindata.ajax,
            type: "POST",
            data: {
                'action': 'rvs_update_gateway',
                'gateway': gateway
            },
            success:function(response) {
                jQuery('#wpvs-updating-box').hide();
                jQuery('#rvs-gateway-update').text(gateway);
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });

    });

    jQuery('body').delegate('.rvs-create-stripe-plan', 'click', function() {
        show_rvs_updating('Creating Stripe plan...');
        var update_label = jQuery(this);
        var gateway = update_label.parent().parent();
        var update_title = gateway.find('.rvs-gateway-title');
        var plan_id = jQuery(this).data('plan');
        jQuery.ajax({
            url: wpvsadmindata.ajax,
            type: "POST",
            data: {
                'action': 'wpvs_create_new_stripe_plan_ajax_request',
                'id_of_plan': plan_id
            },
            success:function(response) {
                jQuery('#wpvs-updating-box').hide();
                update_label.removeClass('rvs-create-stripe-plan').addClass('remove-active-stripe-plan').html('<span class="dashicons dashicons-trash"></span>');
                update_title.html('<span class="dashicons dashicons-yes"></span> Stripe <span class="rvs-active-plan">Active</span>');
                gateway.removeClass('rvs-gateway-inactive').addClass('rvs-gateway-active');
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    });

    jQuery('body').delegate('.remove-active-stripe-plan', 'click', function() {
        if(window.confirm("Are you sure you want to delete this plan from Stripe?")) {
            show_rvs_updating('Deleting Stripe plan...');
            var update_label = jQuery(this);
            var gateway = update_label.parent().parent();
            var update_title = gateway.find('.rvs-gateway-title');
            var plan_id = jQuery(this).data('plan');
            jQuery.ajax({
                url: wpvsadmindata.ajax,
                type: "POST",
                data: {
                    'action': 'rvs_delete_stripe_plan',
                    'id_of_plan': plan_id
                },
                success:function(response) {
                    jQuery('#wpvs-updating-box').hide();
                    update_label.removeClass('remove-active-stripe-plan').addClass('rvs-create-stripe-plan').html('<span class="dashicons dashicons-update"></span> Create Billing Plan');
                    update_title.html('<span class="dashicons dashicons-warning rvs-error"></span> Stripe <span class="rvs-gateway-missing">You have not created a Billing Plan for Stripe</span>');
                    gateway.removeClass('rvs-gateway-active').addClass('rvs-gateway-inactive');
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }

    });

    jQuery('body').delegate('.rvs-create-paypal-plan', 'click', function() {
        show_rvs_updating('Creating PayPal plan...');
        var update_label = jQuery(this);
        var gateway = update_label.parent().parent();
        var update_title = gateway.find('.rvs-gateway-title');
        var plan_id = jQuery(this).data('plan');
        jQuery.ajax({
            url: wpvsadmindata.ajax,
            type: "POST",
            data: {
                'action': 'rvs_create_paypal_plan',
                'id_of_plan': plan_id
            },
            success:function(response) {
                jQuery('#wpvs-updating-box').hide();
                update_label.removeClass('rvs-create-paypal-plan').addClass('remove-active-paypal-plan').html('<span class="dashicons dashicons-trash"></span>');
                update_title.html('<span class="dashicons dashicons-yes"></span> PayPal <span class="rvs-active-plan">Active</span>');
                gateway.removeClass('rvs-gateway-inactive').addClass('rvs-gateway-active');
                gateway.find('.rvs-paypal-permalinks').remove();
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    });

    jQuery('body').delegate('.remove-active-paypal-plan', 'click', function() {
        if(window.confirm("Are you sure you want to delete this plan from PayPal?")) {
            show_rvs_updating('Deleting PayPal plan...');
            var update_label = jQuery(this);
            var gateway = update_label.parent().parent();
            var update_title = gateway.find('.rvs-gateway-title');
            var plan_id = jQuery(this).data('plan');
            jQuery.ajax({
                url: wpvsadmindata.ajax,
                type: "POST",
                data: {
                    'action': 'rvs_delete_paypal_plan',
                    'id_of_plan': plan_id
                },
                success:function(response) {
                    jQuery('#wpvs-updating-box').hide();
                    update_label.removeClass('remove-active-paypal-plan').addClass('rvs-create-paypal-plan').html('<span class="dashicons dashicons-update"></span> Create Billing Plan');
                    update_title.html('<span class="dashicons dashicons-warning rvs-error"></span> PayPal <span class="rvs-gateway-missing">You have not created a Billing Plan for PayPal</span>');
                    gateway.removeClass('rvs-gateway-active').addClass('rvs-gateway-inactive');
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }
    });

    jQuery('#create-paypal-webhooks').click(function() {
        show_rvs_updating('Creating webhooks for PayPal...');
        jQuery.ajax({
            url: wpvsadmindata.ajax,
            type: "POST",
            data: {
                'action': 'rvs_paypal_create_webhooks'
            },
            success:function(response) {
                jQuery('#wpvs-updating-box').hide();
                jQuery('.rvs-active-webhook').html('<span class="dashicons dashicons-yes">');
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });

    });
    jQuery('.rvs-perma-help').click( function() {
        jQuery(this).parent().next('.rvs-hidden-text').slideToggle();
    });
});


function check_new_membership_fields() {
    var field_errors = false;

    jQuery('.rvs-edit-form input, .rvs-edit-form select, .rvs-edit-form textarea').removeClass('rvs-field-error');

    var plan_id = jQuery('#id_of_plan');
    var plan_name = jQuery('#name_of_plan');
    var plan_price = jQuery('#price_of_plan');
    var plan_description = jQuery('#description_of_plan');
    var short_plan_description = jQuery('#short_description_of_plan');
    var plan_interval = jQuery('#interval_of_plan');
    var plan_interval_count = jQuery('#plan_interval_count');
    var plan_trial_frequency = jQuery('#trial_period');
    var plan_trial_number = jQuery('#trial_period_days');


    if(!/^[a-zA-Z]+$/.test(plan_id.val())) {
        plan_id.addClass('rvs-field-error');
        jQuery('#id_of_plan').val(plan_id.val().replace(/[^a-zA-Z]/g,''));
        field_errors = true;
    }
    if(plan_id.val() == "") {
        plan_id.addClass('rvs-field-error');
        field_errors = true;
    }

    if(plan_name.val() == "") {
        plan_name.addClass('rvs-field-error');
        field_errors = true;
    }

    if(plan_price.val() == "") {
        plan_price.addClass('rvs-field-error');
        field_errors = true;
    }

    if(plan_description.val() == "") {
        plan_description.addClass('rvs-field-error');
        field_errors = true;
    }

    if(short_plan_description.val() == "") {
        short_plan_description.addClass('rvs-field-error');
        field_errors = true;
    }

    if(plan_interval.val() == "") {
        plan_interval.addClass('rvs-field-error');
        field_errors = true;
    }

    if(plan_interval_count.val() == "") {
        plan_interval_count.addClass('rvs-field-error');
        field_errors = true;
    }


    if(plan_trial_frequency.val() != "none" && plan_trial_number.val() == "") {
        plan_trial_number.addClass('rvs-field-error');
        field_errors = true;
    }

    return field_errors;

}

function check_edit_membership_fields() {
    var field_errors = false;

    jQuery('.rvs-edit-form input, .rvs-edit-form select, .rvs-edit-form textarea').removeClass('rvs-field-error');

    var plan_name = jQuery('#name_of_plan');
    var plan_description = jQuery('#description_of_plan');
    var short_plan_description = jQuery('#short_description_of_plan');
    var plan_trial_frequency = jQuery('#trial_period');
    var plan_trial_number = jQuery('#trial_period_days');


    if(plan_name.val() == "") {
        plan_name.addClass('rvs-field-error');
        field_errors = true;
    }

    if(plan_description.val() == "") {
        plan_description.addClass('rvs-field-error');
        field_errors = true;
    }

    if(short_plan_description.val() == "") {
        short_plan_description.addClass('rvs-field-error');
        field_errors = true;
    }


    if(plan_trial_frequency.val() != "none" && plan_trial_number.val() == "") {
        plan_trial_number.addClass('rvs-field-error');
        field_errors = true;
    }

    return field_errors;

}

function removePayPalMembershipButtons() {
    jQuery('.removePayPalPlan').click(function() {
        var update_table_row = jQuery(this).parent().parent();
        var plan_id = update_table_row.find('.membership-sub-id').val();
        var paypal_id = update_table_row.find('.paypal-id').val();
        if(window.confirm(wpvsadmindata.rvsmessage.deletepaypal)) {
            wpvs_update_paypal_agreement(plan_id, paypal_id, 'delete', update_table_row, false);
        }
    });

    jQuery('.wpvs-paypal-update-membership').change( function() {
        var select_button = jQuery(this);
        var update_table_row = jQuery(this).parent().parent();
        var new_status = select_button.val();
        if(new_status != "") {
            var plan_id = update_table_row.find('.membership-sub-id').val();
            var paypal_id = update_table_row.find('.paypal-id').val();
            if(new_status == 'cancel') {
                if(window.confirm(wpvsadmindata.rvsmessage.cancelpaypal)) {
                   wpvs_update_paypal_agreement(plan_id, paypal_id, new_status, false, select_button);
                }
            } else {
                wpvs_update_paypal_agreement(plan_id, paypal_id, new_status, false, select_button);
            }
        }
    });
}

function removeMembershipButtons() {
    jQuery('.removeMembership').click(function() {
        var update_table_row = jQuery(this).parent().parent();
        var plan_id = update_table_row.find('.membership-sub-id').val();
        if(window.confirm(wpvsadmindata.rvsmessage.deletepaypal)) {
            wpvs_update_stripe_subscription(plan_id, user_id, 'delete', update_table_row, false);
        }
    });

    jQuery('.wpvs-stripe-update-membership').change( function() {
        var select_button = jQuery(this);
        var update_table_row = jQuery(this).parent().parent();
        var new_status = select_button.val();
        if(new_status != "") {
            var plan_id = update_table_row.find('.membership-sub-id').val();
            if(new_status == 'cancel') {
                if(window.confirm(wpvsadmindata.rvsmessage.cancelpaypal)) {
                   wpvs_update_stripe_subscription(plan_id, user_id, new_status, false, select_button);
                }
            } else {
                wpvs_update_stripe_subscription(plan_id, user_id, new_status, false, select_button);
            }
        }
    });
}

function removeCoinMembershipButtons() {
    jQuery('.removeCoinMembership').click(function() {
        var update_table_row = jQuery(this).parent().parent();
        var order_id = update_table_row.find('.membership-sub-id').val();
        if(window.confirm(wpvsadmindata.rvsmessage.deletepaypal)) {
            wpvs_update_coin_subscription(order_id, user_id, 'delete', update_table_row, false);
        }
    });

    jQuery('.wpvs-coingate-update-membership').change( function() {
        var select_button = jQuery(this);
        var update_table_row = jQuery(this).parent().parent();
        var new_status = select_button.val();
        if(new_status != "") {
            var order_id = update_table_row.find('.membership-sub-id').val();
            if(new_status == 'cancel') {
                if(window.confirm(wpvsadmindata.rvsmessage.cancelpaypal)) {
                   wpvs_update_coin_subscription(order_id, user_id, new_status, false, select_button);
                }
            } else {
                wpvs_update_coin_subscription(order_id, user_id, new_status, false, select_button);
            }

        }
    });
}

function removeCoinBaseMembershipButtons() {
    jQuery('.removeCoinBaseMembership').click(function() {
        var update_table_row = jQuery(this).parent().parent();
        var charge_id = update_table_row.find('.membership-sub-id').val();
        if(window.confirm(wpvsadmindata.rvsmessage.deletepaypal)) {
            wpvs_update_coinbase_subscription(charge_id, user_id, 'delete', update_table_row, false);
        }
    });

    jQuery('.wpvs-coinbase-update-membership').change( function() {
        var select_button = jQuery(this);
        var update_table_row = jQuery(this).parent().parent();
        var new_status = select_button.val();
        if(new_status != "") {
            var charge_id = update_table_row.find('.membership-sub-id').val();
            if(new_status == 'cancel') {
                if(window.confirm(wpvsadmindata.rvsmessage.cancelpaypal)) {
                   wpvs_update_coinbase_subscription(charge_id, user_id, new_status, false, select_button);
                }
            } else {
                wpvs_update_coinbase_subscription(charge_id, user_id, new_status, false, select_button);
            }

        }
    });
}

function wpvs_update_paypal_agreement(plan_id, paypal_id, status, delete_button, select_button) {
    var updating_message = wpvsadmindata.rvsmessage.cancellingpp;
    if(status == "reactivate") {
        updating_message = wpvsadmindata.rvsmessage.reactivate;
    }
    if(status == "suspend") {
        updating_message = wpvsadmindata.rvsmessage.suspend;
    }
    if(status == "delete") {
        updating_message = wpvsadmindata.rvsmessage.delete;
    }
    show_rvs_updating(updating_message+'...');
    jQuery.ajax({
        url: wpvsadmindata.ajax,
        data: {
            'action': 'rvs_remove_billing_agreement_admin',
            'planid': plan_id,
            'paypalid': paypal_id,
            'new_status': status,
            'userid': user_id
        },
        success:function(response) {
            if(delete_button) {
                delete_button.fadeOut();
            }
            if(select_button) {
                if(status == "suspend") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.pausedtext+'</option><option value="reactivate">'+rvsrequests.reactivate.canceltext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }

                if(status == "reactivate") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.activetext+'</option><option value="suspend">'+rvsrequests.rvsmessage.pausetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }

                if(status == "cancel") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.cancelledtext+'</option>');
                }

            }
            jQuery('#wpvs-updating-box').fadeOut('fast');
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_update_stripe_subscription(plan_id, user_id, status, delete_button, select_button) {
    var updating_message = wpvsadmindata.rvsmessage.cancellingpp;
    if(status == "delete") {
        updating_message = wpvsadmindata.rvsmessage.delete;
    }
    if(status == "reactivate") {
        updating_message = wpvsadmindata.rvsmessage.reactivate;
    }
    show_rvs_updating(updating_message+'...');
    jQuery.ajax({
        url: wpvsadmindata.ajax,
        data: {
            'action': 'wpvs_update_membership_ajax_request',
            'planId': plan_id,
            'userId': user_id,
            'new_status': status
        },
        success:function(response) {
            if(delete_button) {
                delete_button.fadeOut();
            }
            if(select_button) {
                if(status == "cancel") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.cancelledtext+'</option><option value="reactivate">'+rvsrequests.rvsmessage.reactivatetext+'</option>');
                }
                if(status == "reactivate") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.activetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }
            }
            jQuery('#wpvs-updating-box').fadeOut('fast');
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_update_coin_subscription(order_id, user_id, status, delete_button, select_button) {
    var updating_message = wpvsadmindata.rvsmessage.cancellingpp;
    if(status == "delete") {
        updating_message = wpvsadmindata.rvsmessage.delete;;
    }
    if(status == "reactivate") {
        updating_message = wpvsadmindata.rvsmessage.reactivate;
    }
    show_rvs_updating(updating_message+'...');
    jQuery.ajax({
        url: wpvsadmindata.ajax,
        type: 'POST',
        data: {
            'action': 'wpvs_remove_coin_access_ajax_request',
            'order_id': order_id,
            'new_status': status,
            'user_id': user_id
        },
        success:function(response) {
            jQuery('#wpvs-updating-box').fadeOut('fast');
            if(delete_button) {
                delete_button.remove();
            }
            if(select_button) {
                if(response == "cancel") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.cancelledtext+'</option><option value="reactivate">'+rvsrequests.rvsmessage.reactivatetext+'</option>');
                }
                if(response == "reactivate") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.activetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }
                if(response == "pending") {
                    window.location.reload();
                }

            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_update_coinbase_subscription(charge_id, user_id, status, delete_button, select_button) {
    var updating_message = wpvsadmindata.rvsmessage.cancellingpp;
    if(status == "delete") {
        updating_message = wpvsadmindata.rvsmessage.delete;;
    }
    if(status == "reactivate") {
        updating_message = wpvsadmindata.rvsmessage.reactivate;
    }
    show_rvs_updating(updating_message+'...');
    jQuery.ajax({
        url: wpvsadmindata.ajax,
        type: 'POST',
        data: {
            'action': 'wpvs_remove_coinbase_access_ajax_request',
            'charge_id': charge_id,
            'new_status': status,
            'user_id': user_id
        },
        success:function(response) {
            jQuery('#wpvs-updating-box').fadeOut('fast');
            if(delete_button) {
                delete_button.remove();
            }
            if(select_button) {
                if(response == "cancel") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.cancelledtext+'</option><option value="reactivate">'+rvsrequests.rvsmessage.reactivatetext+'</option>');
                }
                if(response == "reactivate") {
                    select_button.html('<option value="">'+rvsrequests.rvsmessage.activetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }
                if(response == "pending") {
                    window.location.reload();
                }

            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}
