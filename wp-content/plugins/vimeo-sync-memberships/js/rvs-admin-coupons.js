jQuery(document).ready(function() {
    // Save Data
    jQuery('#new-coupon-code').click(function() {
        var coupon_id = jQuery('#coupon_id').val();
        var coupon_name = jQuery('#coupon_name').val();
        var discount_type = jQuery('#discount_type').val();
        var amount_off = jQuery('#amount_off').val();
        var coupon_duration = jQuery('#coupon_duration').val();
        var coupon_month_duration = jQuery('#coupon_month_duration').val();
        var coupon_max_uses = jQuery('#coupon_max_uses').val();
        var coupon_max_uses_customer = jQuery('#coupon_max_uses_customer').val();

        if(!check_new_coupon_fields()) {
            show_rvs_updating("Creating coupon code...");
            jQuery.ajax({
                url: rvsrequests.ajax,
                type: "POST",
                data: {
                    'action': 'rvs_create_new_coupon',
                    'coupon_id': coupon_id,
                    'coupon_name': coupon_name,
                    'discount_type': discount_type,
                    'amount_off': amount_off,
                    'coupon_duration': coupon_duration,
                    'coupon_month_duration': coupon_month_duration,
                    'coupon_max_uses': coupon_max_uses,
                    'coupon_max_uses_customer': coupon_max_uses_customer
                },
                success:function(response) {
                    window.location.href = response;
                    jQuery('#wpvs-updating-box').fadeOut('fast');
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }
    });

    jQuery('#discount_type').change(function() {
        if(jQuery(this).val() == "amount") {
            jQuery('#discount-type-label').text('Amount Off');
            jQuery('#amount_off').attr('placeholder', '10.00');
        } else {
            jQuery('#discount-type-label').text('Percentage Off');
            jQuery('#amount_off').attr('placeholder', '10%');
        }

    });

    jQuery('#coupon_duration').change(function() {
        if(jQuery(this).val() == "repeating") {
            jQuery('.duration-months').show();
        } else {
            jQuery('.duration-months').hide();
        }

    });

    jQuery('.remove-coupon-code').click(function() {
        var coupon_id = jQuery(this).data('id');
        var parent_cell = jQuery(this).parent().parent();
        if(window.confirm("Are you sure you want to delete this coupon?")) {
            show_rvs_updating("Deleting coupon code...");
            jQuery.ajax({
                url: rvsrequests.ajax,
                type: "POST",
                data: {
                    'action': 'rvs_remove_coupon',
                    'coupon_id': coupon_id
                },
                success:function(response) {
                    jQuery('#wpvs-updating-box').fadeOut('fast');
                    parent_cell.remove();
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }
    });
});

function check_new_coupon_fields() {
    var field_errors = false;

    jQuery('.rvs-edit-form input, .rvs-edit-form select, .rvs-edit-form textarea').removeClass('rvs-field-error');

    var coupon_id = jQuery('#coupon_id');
    var coupon_name = jQuery('#coupon_name');
    var amount_off = jQuery('#amount_off');
    var coupon_duration = jQuery('#coupon_duration').val();
    var coupon_month_duration = jQuery('#coupon_month_duration');


    if(coupon_id.val() == "") {
        coupon_id.addClass('rvs-field-error');
        field_errors = true;
    }

    if(coupon_name.val() == "") {
        coupon_name.addClass('rvs-field-error');
        field_errors = true;
    }

    if(amount_off.val() == "") {
        amount_off.addClass('rvs-field-error');
        field_errors = true;
    }

    if(coupon_duration == "repeating" && coupon_month_duration.val() == "") {
        coupon_month_duration.addClass('rvs-field-error');
    }

    return field_errors;

}
