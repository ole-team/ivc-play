jQuery(document).ready(function() {

    jQuery('.rvs-edit-card').click(function() {
        var set_card_id = jQuery(this).parent().prev().find('input').val();
        var set_edit_title = jQuery(this).parent().prev().prev().html();
        jQuery('#rvs-editing-card-title').html(rvsrequests.rvsmessage.cardedit+' '+set_edit_title);
        jQuery('#rvs-card-edit-input').val(set_card_id);
        jQuery('#edit-card-form').slideDown();
        jQuery('#wpvs-new-stripe-card-form').slideUp();
    });
    jQuery('#rvs-save-card-changes').click(function() {
        var card_id = jQuery('#rvs-card-edit-input').val();
        var new_month = jQuery('.edit-card-expiry-month').val();
        var new_year = jQuery('.edit-card-expiry-year').val();
        show_rvs_updating(rvsrequests.rvsmessage.cardu+"...");
        jQuery.ajax({
            url: rvsrequests.ajax,
            type: "POST",
            data: {
                'action': 'wpvs_save_card_ajax_request',
                'card_id': card_id,
                'card_month': new_month,
                'card_year': new_year
            },
            success:function(response) {
                jQuery('#rvs-error').hide();
                jQuery('#wpvs-updating-box').fadeOut('fast');
                jQuery('#edit-card-form').slideUp();
                jQuery('#wpvs-new-stripe-card-form').slideDown();
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    });

    jQuery('#rvs-cancel-card-changes').click(function() {
        jQuery('#edit-card-form').slideUp();
        jQuery('#wpvs-new-stripe-card-form').slideDown();
    });

    // Delete Card
    jQuery('.rvs-delete-card').click(function() {
        var updateThis = jQuery(this).parent().parent();
        var card_id = jQuery(this).parent().prev().find('input').val();
        if(window.confirm(rvsrequests.rvsmessage.carddc)) {
            show_rvs_updating(rvsrequests.rvsmessage.cardd+'...');
            jQuery.ajax({
                url: rvsrequests.ajax,
                type: "POST",
                data: {
                    'action': 'wpvs_delete_card_ajax_request',
                    'card_id': card_id
                },
                success:function(response) {
                    updateThis.fadeOut();
                    jQuery('#rvs-error').hide();
                    jQuery('#wpvs-updating-box').fadeOut('fast');
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }
    });

    // Update Default Card
    jQuery('.rvs-update-default').click(function() {
        var updateThis = jQuery(this);
        var card_id = updateThis.val();
        show_rvs_updating(rvsrequests.rvsmessage.cardudefault+'...');
        jQuery.ajax({
            url: rvsrequests.ajax,
            type: "POST",
            data: {
                'action': 'wpvs_update_card_ajax_request',
                'card_id': card_id
            },
            success:function(response) {
                jQuery('#rvs-error').hide();
                jQuery('#wpvs-updating-box').fadeOut('fast');
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    });

});
