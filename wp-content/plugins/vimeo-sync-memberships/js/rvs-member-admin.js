var paypal_payments_offset = 0;
var stripe_payments_offset = 0;
jQuery(document).ready(function() {
    wpvs_create_admin_refund_buttons();
    jQuery('#wpvs-next-stripe-payments').click( function() {
        var stripe_charge = jQuery('#payments tr:last td:last label').attr('id');
        wpvs_get_customer_stripe_payments(user_id);
    });
    jQuery('#wpvs-next-paypal-payments').click( function() {
        wpvs_get_customer_paypal_payments(user_id);
    });

    var open_more_memberships = true;
    jQuery('#rvs-add-memberships').click(function() {
        if(open_more_memberships) {
            open_more_memberships = false;
            jQuery(this).find('.icon-switch').html('<i class="dashicons-minus dashicons"></i>');
        } else {
            open_more_memberships = true;
            jQuery(this).find('.icon-switch').html('<i class="dashicons-plus dashicons"></i>');
        }
        jQuery('#rvs-other-memberships').slideToggle();
    });

    jQuery('.rvs-add-membership').click(function() {
        var new_plan_id = jQuery(this).attr('id');
        var new_plan_name = jQuery(this).text();
        var question_text = 'Are you sure you want to add the '+new_plan_name+' membership?';
        if(window.confirm(question_text)) {
            show_rvs_updating("Adding membership...");
            jQuery.ajax({
                url: rvsrequests.ajax,
                type: "POST",
                data: {
                    'action': 'wpvs_set_membership_man',
                    'plan_id': new_plan_id,
                    'user_id': user_id
                },
                success:function(response) {
                    var membership = JSON.parse(response);
                    jQuery('#wpvs-updating-box').fadeOut('fast');
                    var new_membership_td = "";
                    var update_element;
                    if( jQuery('table#memberships').length < 1) {
                        new_membership_td = '<table id="memberships" class="rvs_memberships"><tbody><tr><th>Membership</th><th>Price</th><th>Next Payment</th><th>Status</th><th>Gateway</th><th>Update</th><th class="rvs-remove-edit">Manage</th></tr>';
                        update_element = jQuery('#wpvs-user-memberships');
                    } else {
                        update_element = jQuery('#memberships tbody');
                    }
                    new_membership_td += '<tr><td>'+membership.name+'</td><td>'+membership.amount+'/'+membership.interval+'</td><td><input type="text" class="wpvs-select-date-admin new-wpvs-admin-date-picker" readonly placeholder="Select Date (optional)"></td><td class="membership-status">'+membership.status+'</td><td>'+membership.type+'</td><td>N/A</td><td><label class="rvs-remove removeManMembership">Delete</label><input type="hidden" class="membership-sub-id" value="'+membership.plan+'"></td></tr>';

                    jQuery(update_element).append(new_membership_td);
                    jQuery(".new-wpvs-admin-date-picker").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        showButtonPanel: true,
                        dateFormat: 'MM dd, yy',
                        minDate: 0,
                        onSelect: function(dateText, inst) {
                            var set_date = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
                            var membership_ends = set_date.setFullYear(inst.selectedYear);
                            var update_table_row = jQuery(this).parent().parent();
                            var plan_id = update_table_row.find('.membership-sub-id').val();
                            var new_membership_ends = membership_ends/1000;
                            wpvs_update_man_membership_date(plan_id, new_membership_ends);
                        }
                    });
                    jQuery(".new-wpvs-admin-date-picker").removeClass('new-wpvs-admin-date-picker');
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }

    });

    jQuery('.sync-stripe-plan').click( function() {
        var stripe_subscription = jQuery(this).data('sync');
        var update_table = jQuery(this).parent().parent();
        var select_box = update_table.find('.wpvs-paypal-update-membership');
        show_rvs_updating("Syncing subscription...");
        jQuery.ajax({
            url: rvsrequests.ajax,
            type: "POST",
            data: {
                'action': 'wpvs_sync_stripe_subscription',
                'subscription': stripe_subscription,
                'user_id': user_id
            },
            success:function(response) {
                var subscription_details = JSON.parse(response);
                update_table.find('td.rvs-m-date').html(subscription_details.ends);
                if(subscription_details.status == "canceled") {
                    select_box.html('<option value="">'+rvsrequests.rvsmessage.cancelledtext+'</option>');
                }

                if(subscription_details.status == "active") {
                    select_box.html('<option value="">'+rvsrequests.rvsmessage.activetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }

                if(subscription_details.status == "trialing") {
                    select_box.html('<option value="">'+rvsrequests.rvsmessage.trialingtext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }

                if(subscription_details.status == "past_due" || subscription_details.status == "unpaid" || subscription_details.status == "incomplete" || subscription_details.status == "incomplete_expired") {
                    select_box.html('<option value="">'+rvsrequests.rvsmessage.overduetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }

                jQuery('#wpvs-updating-box').fadeOut('fast');

            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    });

    jQuery('body').delegate('.removeManMembership','click', function() {
        var updateThis = jQuery(this).parent().parent();
        var plan_id = updateThis.find('.membership-sub-id').val();
        if(window.confirm(rvsrequests.rvsmessage.membershipdc)) {
            show_rvs_updating(rvsrequests.rvsmessage.membershipc+'...');
            jQuery.ajax({
                type: "POST",
                url: rvsrequests.ajax,
                data: {
                    'action': 'wpvs_remove_man_plan_admin',
                    'plan_id': plan_id,
                    'user_id': user_id
                },
                success:function(response) {
                    updateThis.fadeOut();
                    jQuery('#wpvs-updating-box').fadeOut('fast');
                },
                error: function(response){
                    show_rvs_error(response.responseText);
                }
            });
        }
    });

    jQuery('.sync-paypal-plan').click( function() {
        var stripe_subscription = jQuery(this).data('sync');
        var update_table = jQuery(this).parent().parent();
        var select_box = update_table.find('.wpvs-paypal-update-membership');
        show_rvs_updating("Syncing subscription...");
        jQuery.ajax({
            url: rvsrequests.ajax,
            type: "POST",
            data: {
                'action': 'wpvs_sync_paypal_subscription',
                'subscription': stripe_subscription,
                'user_id': user_id
            },
            success:function(response) {
                var subscription_details = JSON.parse(response);
                update_table.find('td.rvs-m-date').html(subscription_details.ends);
                if(subscription_details.status == "Suspended") {
                    select_box.html('<option value="">'+rvsrequests.rvsmessage.pausedtext+'</option><option value="reactivate">'+rvsrequests.rvsmessage.reactivatetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }

                if(subscription_details.status == "Active") {
                    select_box.html('<option value="">Active</option><option value="suspend">'+rvsrequests.rvsmessage.pausetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option>');
                }

                if(subscription_details.status == "Cancelled") {
                    select_box.html('<option value="">'+rvsrequests.rvsmessage.cancelledtext+'</option>');
                }
                jQuery('#wpvs-updating-box').fadeOut('fast');

            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    });

    jQuery('.sync-coingate-plan').click( function() {
        var cg_order_id = jQuery(this).data('sync');
        var update_table = jQuery(this).parent().parent();
        var select_box = update_table.find('.wpvs-paypal-update-membership');
        show_rvs_updating("Syncing subscription...");
        jQuery.ajax({
            url: rvsrequests.ajax,
            type: "POST",
            data: {
                'action': 'wpvs_sync_coingate_subscription',
                'order_id': cg_order_id,
                'user_id': user_id
            },
            success:function(response) {
                jQuery('#wpvs-updating-box').fadeOut('fast');
                if( response != "") {
                    var subscription_details = JSON.parse(response);
                    var update_td = update_table.find('.membership-status');
                    var updated_html = '<span class="rvs-hidden-title">Status: </span>';
                    if(subscription_details.status == "expired" || subscription_details.status == "new" || subscription_details.status == "pending") {
                         updated_html += '<span class="wpvs-coingate-status">Awaiting payment</span>';
                    }

                    if(subscription_details.status == "confirming") {
                        updated_html += '<span class="wpvs-coingate-status">Confirming payment</span>';
                    }

                    if(subscription_details.status == "paid") {
                         updated_html += '<select class="wpvs-update-user-plan-status wpvs-coingate-update-membership"><option>'+rvsrequests.rvsmessage.activetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option></select>';
                    }

                    if(subscription_details.status == "refunded") {
                       updated_html += '<span class="wpvs-coingate-status">Payment refunded</span>';
                    }
                    update_td.html(updated_html);

                }
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    });

    jQuery('.sync-coinbase-plan').click( function() {
        var charge_id = jQuery(this).data('sync');
        var update_table = jQuery(this).parent().parent();
        var select_box = update_table.find('.wpvs-paypal-update-membership');
        show_rvs_updating("Syncing subscription...");
        jQuery.ajax({
            url: rvsrequests.ajax,
            type: "POST",
            data: {
                'action': 'wpvs_sync_coinbase_subscription',
                'charge_id': charge_id,
                'user_id': user_id
            },
            success:function(response) {
                jQuery('#wpvs-updating-box').fadeOut('fast');
                if( response != "") {
                    var subscription_details = JSON.parse(response);
                    var update_td = update_table.find('.membership-status');
                    var updated_html = '<span class="rvs-hidden-title">Status: </span>';
                    if(subscription_details.status == "EXPIRED" || subscription_details.status == "NEW") {
                         updated_html += '<span class="wpvs-coingate-status">Awaiting payment</span>';
                    }

                    if(subscription_details.status == "PENDING") {
                        updated_html += '<span class="wpvs-coingate-status">Confirming payment</span>';
                    }

                    if(subscription_details.status == "CANCELED") {
                        updated_html += '<span class="wpvs-coingate-status">Canceled</span>';
                    }

                    if(subscription_details.status == "COMPLETED" || subscription_details.status == "CONFIRMED") {
                         updated_html += '<select class="wpvs-update-user-plan-status wpvs-coingate-update-membership"><option>'+rvsrequests.rvsmessage.activetext+'</option><option value="cancel">'+rvsrequests.rvsmessage.canceltext+'</option></select>';
                    }

                    if(subscription_details.status == "refunded") {
                       updated_html += '<span class="wpvs-coingate-status">Payment refunded</span>';
                    }
                    update_td.html(updated_html);

                }
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    });

    if(jQuery(".wpvs-select-date-admin").length > 0) {
        jQuery(".wpvs-select-date-admin").datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'MM dd, yy',
            minDate: 0,
            onSelect: function(dateText, inst) {
                var set_date = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
                var membership_ends = set_date.setFullYear(inst.selectedYear);
                var update_table_row = jQuery(this).parent().parent();
                var plan_id = update_table_row.find('.membership-sub-id').val();
                var new_membership_ends = membership_ends/1000;
                wpvs_update_man_membership_date(plan_id, new_membership_ends);
            }
        });

        jQuery('.wpvs-set-man-never').click( function() {
            var parent_tr = jQuery(this).parent();
            parent_tr.find('.wpvs-ends-label').remove();
            parent_tr.find('.wpvs-select-date-admin').attr('placeholder', 'Select date');
            var update_table_row = parent_tr.parent();
            var plan_id = update_table_row.find('.membership-sub-id').val();
            var new_membership_ends = "never";
            wpvs_update_man_membership_date(plan_id, new_membership_ends);
        });
    }

    jQuery('.wpvs-admin-split-label').click(function() {
        var this_label = jQuery(this);
        var show_section = this_label.data('section');
        jQuery('.wpvs-admin-split-label, .wpvs-admin-split').removeClass('active');
        this_label.addClass('active');
        jQuery(show_section).addClass('active');
        if(show_section == '#paypal-payments' && jQuery('.paypal-payment-item').length == 0) {
            wpvs_get_customer_paypal_payments(user_id);
        }
    });

    jQuery('#import-stripe-customer-payments').click( function() {
        if( typeof(stripe_id) != 'undefined' && stripe_id != "") {
            if(window.confirm('Are you sure you want to import this customers Stripe payments?')) {
                wpvs_admin_import_stripe_customer_payments(null);
            }
        }
    });
});

function wpvs_update_man_membership_date(plan_id, new_membership_ends) {
    show_rvs_updating("Updating membership...");
    jQuery.ajax({
        url: rvsrequests.ajax,
        type: "POST",
        data: {
            'action': 'wpvs_update_man_membership_date',
            'new_end_date': new_membership_ends,
            'plan_id': plan_id,
            'user_id': user_id
        },
        success:function(response) {
            jQuery('#wpvs-updating-box').fadeOut('fast');

        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_get_customer_stripe_payments(user_id) {
    show_rvs_updating('Getting Payments...');
    jQuery.ajax({
        url: rvsrequests.ajax,
        data: {
            'action': 'wpvs_get_stripe_payments_ajax_request',
            'user_id': user_id,
            'wpvs_admin_nonce': wpvs_admin_nonce,
            'offset': stripe_payments_offset
        },
        success:function(response) {
            jQuery('#wpvs-updating-box').fadeOut('fast');
            jQuery('#error').hide();

            var stripe_payments = jQuery.parseJSON(response);

            if( stripe_payments.length > 0 ) {
                jQuery.each(stripe_payments, function(index, payment) {
                    var refund = "";
                    if(payment.refunded) {
                        refund = '<label id="' + payment.paymentid + '"><em>Refunded</em></label>';
                    } else {
                        refund = '<label class="issue-refund stripe-refund" id="' + payment.paymentid + '">Refund</label>';
                    }
                    jQuery('#payments tr:last').after('<tr class="stripe-payment-item"><td>$' + parseFloat(payment.amount/100).toFixed(2) + '</td><td>' + payment.time + '</td><td>'+payment.type+'</td><td>'+refund+'</td></tr>');
                    stripe_payments_offset++;
                });
                if(stripe_payments.length < 1) {
                    jQuery('#rvs-payment-nav').html('<p>No More Credit Card Payments</p>');
                } else if (stripe_payments.length < 100) {
                    jQuery('#wpvs-next-stripe-payments').hide();
                } else {
                    jQuery('#wpvs-next-stripe-payments').css('opacity', '1');
                }
            } else {
                jQuery('#rvs-payment-nav').html('<p>No More Credit Card Payments</p>');
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_get_customer_paypal_payments(user_id) {
    show_rvs_updating('Getting PayPal Payments...');
    jQuery.ajax({
        url: rvsrequests.ajax,
        data: {
            'action': 'wpvs_get_paypal_customer_payments_admin',
            'user_id': user_id,
            'wpvs_admin_nonce': wpvs_admin_nonce,
            'offset': paypal_payments_offset
        },
        success:function(response) {
            jQuery('#wpvs-updating-box').fadeOut('fast');
            jQuery('#error').hide();

            var paypal_payments = jQuery.parseJSON(response);

            if( paypal_payments.length > 0 ) {
                jQuery.each(paypal_payments, function(index, payment) {
                    var refund = "";
                    if(payment.refunded) {
                        refund = '<label id="' + payment.paymentid + '"><em>Refunded</em></label>';
                    } else {
                        refund = '<label class="issue-refund paypal-refund" id="' + payment.paymentid + '">Refund</label>';
                    }
                    jQuery('#customer-paypal-payments tr:last').after('<tr class="paypal-payment-item"><td>$' + parseFloat(payment.amount/100).toFixed(2) + '</td><td>' + payment.time + '</td><td>'+payment.type+'</td><td>'+refund+'</td></tr>');
                    paypal_payments_offset++;
                });
                if(paypal_payments.length < 1) {
                    jQuery('#wpvs-paypal-payment-nav').html('<p>No More PayPal Payments</p>');
                } else if (paypal_payments.length < 100) {
                    jQuery('#wpvs-next-paypal-payments').hide();
                } else {
                    jQuery('#wpvs-next-paypal-payments').css('opacity', '1');
                }
            } else {
                jQuery('#wpvs-paypal-payment-nav').html('<p>No More Payments</p>');
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_admin_import_stripe_customer_payments(start_at_charge) {
    show_rvs_updating('Importing Stripe Payments...please wait');
    jQuery.ajax({
        url: rvsrequests.ajax,
        type: 'POST',
        data: {
            'action': 'wpvs_admin_sync_all_stripe_payments',
            'customer_id': stripe_id,
            'start_at': start_at_charge
        },
        success:function(response) {
            show_rvs_updating('Imported!');
            var payment_details = JSON.parse(response);
            if( payment_details.more ) {
                wpvs_admin_import_stripe_customer_payments(payment_details.start_at);
            } else {
                location.reload();
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}
