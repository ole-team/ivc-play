jQuery(document).ready(function() {
    if( jQuery('#wpvs_billing_country').length > 0 ) {
        jQuery('#wpvs_billing_country').change( function() {
            var country_code = jQuery(this).val();
            var state_list_url = wpvsdir.list + country_code + '/states.json';
            wpvs_get_country_list_json(state_list_url);
        });

        if( jQuery('#wpvs_billing_country').val() != "" ) {
            var country_code = jQuery('#wpvs_billing_country').val();
            var state_list_url = wpvsdir.list + country_code + '/states.json';
            wpvs_get_country_list_json(state_list_url);
        }
    }
});

function wpvs_get_country_list_json(state_list_url) {
    var new_state_select = "";
    jQuery.getJSON(state_list_url, function( states ) {
        jQuery.each( states, function( key, state ) {
            if( wpvsdir.state && wpvsdir.state == state ) {
                new_state_select += '<option value="'+state+'" selected="selected">'+state+'</option>';
            } else {
                new_state_select += '<option value="'+state+'">'+state+'</option>';
            }
        });
        jQuery('#wpvs_billing_state').html(new_state_select);
    });
}
