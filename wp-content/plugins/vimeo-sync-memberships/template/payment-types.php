<?php 
global $is_apple_device; 
global $wpvs_google_apple_enabled; 
global $wpvs_add_stripe_forms; 
global $wpvs_add_paypal_forms;
global $wpvs_coingate_enabled;
global $wpvs_coinbase_enabled;
$rvs_card_button_image = get_option('rvs_card_button_image', RVS_MEMBERS_BASE_URL . 'image/stripe.png');
?>

<div class="wpvs-select-payment-type">
    <?php if($wpvs_add_stripe_forms) : ?>
    <div class="wpvs-payment-type border-box active" data-paybox="#wpvs-stripe-box">
        <img class="wpvs-payment-option" src="<?php echo $rvs_card_button_image; ?>" alt="Checkout with Stripe" />
        <label><?php _e('Credit Card', 'vimeo-sync-memberships'); ?></label>
    </div>
    <?php endif; 
    if($wpvs_add_paypal_forms) : ?>
    <div class="wpvs-payment-type border-box" data-paybox="#wpvs-paypal-payment">
        <img class="wpvs-payment-option" src="<?php echo RVS_MEMBERS_BASE_URL .'image/paypal.png'; ?>" alt="Checkout with PayPal" />
        <label><?php _e('PayPal', 'vimeo-sync-memberships'); ?></label>
    </div>
    <?php endif; 
    if($wpvs_google_apple_enabled) : ?>
    <div class="wpvs-payment-type border-box" data-paybox="#wpvs-google-payment">
        <?php if($is_apple_device) : ?>
            <img class="wpvs-payment-option" src="<?php echo RVS_MEMBERS_BASE_URL .'image/apple-pay.png'; ?>" alt="Checkout with Google" />
            <label><?php _e('Apple Pay', 'vimeo-sync-memberships'); ?></label>
        <?php else : ?>
            <img class="wpvs-payment-option" src="<?php echo RVS_MEMBERS_BASE_URL .'image/google-pay.png'; ?>" alt="Checkout with Google" />
            <label><?php _e('Google Pay', 'vimeo-sync-memberships'); ?></label>
        <?php endif; ?>
    </div>
    <?php endif; 
    if($wpvs_coingate_enabled) : ?>
    <div class="wpvs-payment-type border-box wpvs-coingate-payment" data-paybox="#wpvs-coingate-payment" id="wpvs-coingate-subscription">
        <img class="wpvs-payment-option" src="<?php echo RVS_MEMBERS_BASE_URL .'image/coins.png'; ?>" alt="Checkout with Cryptocurrencies" />
        <label><?php _e('Bitcoin and altcoins', 'vimeo-sync-memberships'); ?></label>
    </div>
    <?php endif;
    if($wpvs_coinbase_enabled) : 
        global $wpvs_coinbase_manager;
        if( ! empty($wpvs_coinbase_manager->get_accepted_currencies()) ) {
            $accepted_coinbase_currencies = $wpvs_coinbase_manager->get_accepted_currencies();
        } else {
            $accepted_coinbase_currencies = array('BTC','BCH','ETH','LTC','USDC');
        }
    ?>
    <div class="wpvs-payment-type border-box wpvs-coinbase-payment" data-paybox="#wpvs-coinbase-payment" id="wpvs-coinbase-subscription">
        <div class="wpvs-coinbase-options">
        <?php foreach($accepted_coinbase_currencies as $accepted_coin) { ?>
            <img class="coinbase-coin-option" src="<?php echo RVS_MEMBERS_BASE_URL; ?>image/coins/<?php echo $accepted_coin; ?>.png" alt="Checkout with <?php echo $accepted_coin; ?>" />
        <?php } ?>
        </div>
        <label><?php _e('Cryptocurrency', 'vimeo-sync-memberships'); ?></label>
    </div>
    <?php endif; ?>
</div>