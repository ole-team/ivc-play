<?php
$rvs_account_page = get_option('rvs_account_page');
$rvs_redirect_link = get_permalink($rvs_account_page);
$wpvs_users_can_register = get_option( 'users_can_register' );
?>
<div class="wpvs-login-form">
    <div class="wpvs-login-labels">
        <label class="wpvs-login-label border-box active" data-show="wpvs-signin"><?php _e('Sign In', 'vimeo-sync-memberships'); ?></label>
        <?php if($wpvs_users_can_register) : ?>
        <label class="wpvs-login-label border-box" data-show="wpvs-create-account"><?php _e('Create Account', 'vimeo-sync-memberships'); ?></label>
        <?php endif; ?>
    </div>
    <div id="wpvs-signin" class="wpvs-login-section active">
        <?php echo rvs_check_login_errors(); ?>
        <?php wp_login_form(array('form_id' => 'wpvs-login-form')); ?>
        <a href="<?php echo wp_lostpassword_url($rvs_redirect_link); ?>" title="Lost Password"><?php _e('Forgot Password', 'vimeo-sync-memberships'); ?></a>
    </div>
    <?php if($wpvs_users_can_register) : ?>
    <div id="wpvs-create-account" class="wpvs-login-section">
        <?php echo do_shortcode('[rvs_create_account]');?>
    </div>
    <?php endif; ?>
</div>