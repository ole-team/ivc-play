<?php
global $wpvs_stripe_options;
global $wpvs_requires_save_card;
if( ! isset($wpvs_stripe_options['agreement_content']) ) {
    $wpvs_stripe_options['agreement_content'] = '<p><strong>Agreement:</strong> I authorise '.get_bloginfo('name').' to send instructions to the financial institution that issued my card to take payments from my card account in accordance with the terms of my agreement with you.</p>';
}
if( ! isset($wpvs_stripe_options['card_note_content']) ) {
    $wpvs_stripe_options['card_note_content'] = '<p><strong>Note:</strong> If your card requires authentication on all transactions, you may need to login to your account to pay for subscription renewals.</p>';
}
?>
<div class="form-container">
    <input placeholder="<?php _e('Name on Card', 'vimeo-sync-memberships'); ?>" type="text" name="card_name" id="card_name" data-stripe="name" required>
    <?php if(isset($wpvs_stripe_options['billing_address'])) { ?>
    <input placeholder="<?php _e('Address', 'vimeo-sync-memberships'); ?>" id="new_address" data-stripe="address_line1" type="text" required/>
    <input placeholder="<?php _e('City', 'vimeo-sync-memberships'); ?>" id="new_address_city" data-stripe="address_city" type="text" required/>
    <input placeholder="<?php _e('State', 'vimeo-sync-memberships'); ?>/<?php _e('Province', 'vimeo-sync-memberships'); ?>" id="new_province" data-stripe="address_state" type="text" required/>
    <input placeholder="<?php _e('Zip/Postal Code', 'vimeo-sync-memberships'); ?>" id="new_postal_code" data-stripe="address_zip" type="text" required/>
    <select data-stripe="address_country" id="address_country" name="address_country">
        <?php include(RVS_MEMBERS_BASE_DIR.'/template/country-list.php'); ?>
    </select>
    <?php } ?>
</div>
<div class="form-row">
<div id="wpvs-card-element"></div>
<div id="wpvs-card-errors" role="alert" class="payment-errors"></div>
</div>
<div id="wpvs-card-permission" class="form-row">
    <div id="wpvs-card-permission-input">
        <input type="checkbox" name="wpvs_save_card_for_future" id="wpvs_save_card_for_future" value="1" <?= $wpvs_requires_save_card ? 'required' : ''; ?> />
        <label id="wpvs-card-permission-label">
            <?php _e('Save card for future payments', 'vimeo-sync-memberships'); ?>
            <?= $wpvs_requires_save_card ? ' <small>('.__('required for subscriptions', 'vimeo-sync-memberships').')</small>' : ''; ?>
        </label>
    </div>
    <?php if( ! empty($wpvs_stripe_options['agreement_content']) ) { ?>
        <div id="wpvs-card-permission-details">
            <?php echo $wpvs_stripe_options['agreement_content']; ?>
        </div>
    <?php } if( ! empty($wpvs_stripe_options['card_note_content']) ) { ?>
        <div id="wpvs-card-permission-note">
            <?php echo $wpvs_stripe_options['card_note_content']; ?>
        </div>
    <?php } ?>
</div>
