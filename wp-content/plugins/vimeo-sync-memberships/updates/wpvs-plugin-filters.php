<?php
function wpvs_membership_update_checks($queryArgs) {
    $wpvs_customer_has_access = get_option('rvs-activated', false);
    if ( ! empty($wpvs_customer_has_access) ) {
        $wpvs_plugins = get_option('rvs-plugin-access');
        if(!empty($wpvs_plugins) && in_array('vimeo-sync-memberships', $wpvs_plugins)) {
            $queryArgs['has_rvs_access'] = true;
            $queryArgs['site'] = home_url();
        }
    }
    return $queryArgs;
}
