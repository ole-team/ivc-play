<?php

function wpvs_memberships_update_version_check() {
    global $rvs_current_version;
    if(WPVS_VIDEO_MEMBERSHIPS_VERSION !== $rvs_current_version) {
        run_wpvs_memberships_automatic_updates();
    }
}
add_action( 'plugins_loaded', 'wpvs_memberships_update_version_check' );

function run_wpvs_memberships_automatic_updates() {
    global $rvs_current_version;
    $needs_update = false;
    if(!empty($rvs_current_version)) {
        $current_version_number = intval(str_replace(".","",$rvs_current_version));
    } else {
        $current_version_number = 0;
    }

    if($current_version_number < 449) {
        $wpvs_stripe_options = get_option('stripe_settings');
        update_option('wpvs_stripe_settings', $wpvs_stripe_options);
        delete_option('stripe_settings');
    }
    if($current_version_number < 453) {
        $wpvs_tax_rate_manager = new WPVS_Tax_Rate_Manager();
        $wpvs_tax_rate_manager->create_tax_rate_table();
    }
    update_option('wpvs_stripe_api_version', '2020-03-02');
    update_option('rvs_memberships_version', '4.7.8');
}
