<?php
namespace WPVS\CoinGate;

class WPVSCoinGatePayment
{
    private $payment;

    public function __construct()
    {
        //$this->payment = $payment;
    }

    public static function create($order_params, $test)
    {
        global $wpdb;
        if($test) {
            $table_name = $wpdb->prefix . 'wpvs_test_coin_payments';
        } else {
            $table_name = $wpdb->prefix . 'wpvs_coin_payments';
        }
        $check = $wpdb->insert(
            $table_name,
            $order_params
        );
    }

    public static function wpvs_get_coin_payment_by_id($cg_id)
    {

        global $wpdb;
        global $rvs_live_mode;
        if($rvs_live_mode == "off") {
            $table_name = $wpdb->prefix . 'wpvs_test_coin_payments';
        } else {
            $table_name = $wpdb->prefix . 'wpvs_coin_payments';
        }
        $coin_payment = $wpdb->get_results("SELECT * FROM $table_name WHERE id = '$cg_id'");
        return $coin_payment;

    }


    public static function wpvs_delete_coin_payment($cg_id)
    {
        global $wpdb;
        global $rvs_live_mode;
        if($rvs_live_mode == "off") {
            $table_name = $wpdb->prefix . 'wpvs_test_coin_payments';
        } else {
            $table_name = $wpdb->prefix . 'wpvs_coin_payments';
        }
        $coin_payment_deleted = $wpdb->get_results("DELETE FROM $table_name WHERE id = '$cg_id'");
        return $coin_payment_deleted;
    }

    public static function wpvs_get_user_coin_payments($user_id, $transaction_type, $type_id, $currency, $purchase_type)
    {
        global $wpdb;
        global $rvs_live_mode;
        if($rvs_live_mode == "off") {
            $table_name = $wpdb->prefix . 'wpvs_test_coin_payments';
        } else {
            $table_name = $wpdb->prefix . 'wpvs_coin_payments';
        }

        if( $purchase_type == "subscription" ) {
            $coin_query = "SELECT * FROM $table_name WHERE userid = '$user_id' AND planid = '$type_id'";
        }
        if( $purchase_type == "purchase" || $purchase_type == "rental" ) {
            $coin_query = "SELECT * FROM $table_name WHERE userid = '$user_id' AND videoid = '$type_id'";
        }
        if( $purchase_type == "termpurchase" ) {
            $coin_query = "SELECT * FROM $table_name WHERE userid = '$user_id' AND termid = '$type_id'";
        }

        if( ! empty($currency) ) {
            $coin_query .= " AND pay_currency = '$currency'";
        }

        $coin_query .= " AND status != 'paid' AND purchase = '$purchase_type' AND type = '$transaction_type'";

        $coin_payments = $wpdb->get_results($coin_query);

        return $coin_payments;
    }

    public static function wpvs_get_user_coin_payments_by_id($user_id, $order_id)
    {
        global $wpdb;
        global $rvs_live_mode;
        if($rvs_live_mode == "off") {
            $table_name = $wpdb->prefix . 'wpvs_test_coin_payments';
        } else {
            $table_name = $wpdb->prefix . 'wpvs_coin_payments';
        }

        $coin_query = "SELECT FROM $table_name WHERE userid = '$user_id' AND id = '$order_id' AND status != 'paid'";
        $wpdb->query($coin_query);
    }

    public static function wpvs_get_user_pending_coin_payments($user_id, $type, $type_id, $purchase_type, $price_amount) {
        global $wpdb;
        global $rvs_live_mode;
        if($rvs_live_mode == "off") {
            $table_name = $wpdb->prefix . 'wpvs_test_coin_payments';
        } else {
            $table_name = $wpdb->prefix . 'wpvs_coin_payments';
        }

        $price_amount = number_format(($price_amount/100), 2, '.', '');

        if( $type == "video" ) {
            $coin_query = "SELECT * FROM $table_name WHERE userid = '$user_id' AND videoid = '$type_id'";
        }
        if( $type == "term" ) {
            $coin_query = "SELECT * FROM $table_name WHERE userid = '$user_id' AND termid = '$type_id'";
        }

        $coin_query .= " AND status != 'paid' AND purchase = '$purchase_type' AND price_amount = '$price_amount'";

        $coin_payments = $wpdb->get_results($coin_query);

        return $coin_payments;
    }
}
