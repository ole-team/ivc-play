<?php

function wpvs_load_coingate_scripts() {
    global $rvs_current_version;
    global $rvs_currency;
	wp_register_script( 'wpvs-coingate-payments-js', RVS_MEMBERS_BASE_URL .'include-coingate/js/coingate-checkout.js', array('jquery'), $rvs_current_version, true);
    wp_enqueue_script( 'wpvs-coingate-payments-js' );
    wp_localize_script('wpvs-coingate-payments-js', 'wpvscoingate', array(
        'url' => admin_url('admin-ajax.php'),
        'currency' => $rvs_currency, 
        'wpvsmessage' => array(
            'processing' => __('Creating transaction', 'vimeo-sync-memberships'),
            'btcsending' => __('Bitcoin sending address', 'vimeo-sync-memberships'),
            'ltcsending' => __('Litecoin sending address', 'vimeo-sync-memberships'),
            'converting' => __('Converting', 'vimeo-sync-memberships'),
            'btccheckout' => __('Checkout using Bitcoin', 'vimeo-sync-memberships'),
            'ltccheckout' => __('Checkout using Litecoin', 'vimeo-sync-memberships'),
            'pleasesend' => __('Please send', 'vimeo-sync-memberships'),
            'followingaddress' => __('to the following address', 'vimeo-sync-memberships'),
            'coinssent' => __('I have sent my coins', 'vimeo-sync-memberships'),
            'usecgcheckout' => __('Use CoinGate Checkout', 'vimeo-sync-memberships'),
            'cgconfirming' => __('Updating transaction', 'vimeo-sync-memberships'),
            'orderplaced' => __('It looks like you have already placed an order for this video', 'vimeo-sync-memberships'),
            'emailconfirmation' => __('If you have recently sent your coin payment, please wait for an email confirmation before accessing this video', 'vimeo-sync-memberships'),
            'termorderplaced' => __('It looks like you have already placed an order for this collection', 'vimeo-sync-memberships'),
            'termemailconfirmation' => __('If you have recently sent your coin payment, please wait for an email confirmation before accessing videos within this collection', 'vimeo-sync-memberships')
        )
    ));
}

// ENQUEUE COINGATE SCRIPTS
add_action('wp_enqueue_scripts', 'wpvs_load_coingate_scripts');