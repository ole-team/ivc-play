<?php
function wpvs_coingate_callback_listener() {
    if(isset($_GET['wpvs-coingate-callback-listener']) && $_GET['wpvs-coingate-callback-listener'] == 'cg') {
        if( isset($_POST['token']) ) {
            $token = $_POST['token'];
            $order_id = $_POST['id'];
            $status = $_POST['status'];
            $receive_amount = $_POST['receive_amount'];
            $receive_currency = $_POST['receive_currency'];
            $pay_currency = $_POST['pay_currency'];
            $pay_amount = $_POST['pay_amount'];
            $cg_payment = wpvs_get_coingate_order_update($order_id, $token, $status, $receive_currency, $receive_amount, $pay_currency, $pay_amount);
        }
        if( ! empty($cg_payment) ) {
            echo wp_json_encode(array('coingatepaymentsuccess' => true, 'payment' => $cg_payment->id));
        }
        exit;
    }
}
add_action('template_redirect', 'wpvs_coingate_callback_listener');