<?php
namespace CoinGate\Merchant;

use CoinGate\CoinGate;
use CoinGate\Merchant;
use CoinGate\OrderIsNotValid;

class Checkout extends Merchant
{
    private $checkout;

    public function __construct($checkout)
    {
        $this->checkout = $checkout;
    }

    public function toHash()
    {
        return $this->checkout;
    }

    public function __get($name)
    {
        return $this->checkout[$name];
    }

    public static function create($params, $order_id, $authentication = array())
    {
        try {
            return self::createOrFail($params, $order_id, $authentication);
        } catch (OrderIsNotValid $e) {
            return false;
        }
    }

    public static function createOrFail($params, $order_id, $authentication = array())
    {
        $checkout = CoinGate::request('/orders/'.$order_id.'/checkout', 'POST', $params, $authentication);

        return new self($checkout);
    }
}
