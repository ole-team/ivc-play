<?php

function wpvs_create_coin_payment_tables() {
    global $wpdb;
    if( ! wpvs_coin_payments_table_exists() ) {
        $table_name = $wpdb->prefix . 'wpvs_coin_payments';
        $test_table_name = $wpdb->prefix . 'wpvs_test_coin_payments';
        $charset_collate = $wpdb->get_charset_collate();

        $live_payment_tables = "CREATE TABLE $table_name (
          id mediumint(9) NOT NULL UNIQUE,
          orderid varchar(100) NOT NULL UNIQUE,
          status tinytext NOT NULL,
          price_currency tinytext NOT NULL,
          price_amount tinytext NOT NULL,
          receive_amount tinytext,
          receive_currency tinytext NOT NULL,
          pay_amount tinytext,
          pay_currency tinytext,
          token text NOT NULL,
          refunded boolean,
          userid int NOT NULL,
          planid tinytext,
          videoid mediumint(9),
          termid mediumint(9),
          type tinytext NOT NULL,
          purchase tinytext,
          updated text NOT NULL,
          PRIMARY KEY (id)
        ) $charset_collate;";

        $test_payment_tables = "CREATE TABLE $test_table_name (
          id mediumint(9) NOT NULL UNIQUE,
          orderid varchar(100) NOT NULL UNIQUE,
          status tinytext NOT NULL,
          price_currency tinytext NOT NULL,
          price_amount tinytext NOT NULL,
          receive_amount tinytext,
          receive_currency tinytext NOT NULL,
          pay_amount tinytext,
          pay_currency tinytext,
          token text NOT NULL,
          refunded boolean,
          userid int NOT NULL,
          planid tinytext,
          videoid mediumint(9),
          termid mediumint(9),
          type tinytext NOT NULL,
          purchase tinytext,
          updated text NOT NULL,
          PRIMARY KEY (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $live_payment_tables );
        dbDelta( $test_payment_tables );
    }
}

function wpvs_coin_payments_table_exists() {
    global $wpdb;
    $payments_table_exists = true;
    $wpvs_payments_table_name = $wpdb->prefix . 'wpvs_coin_payments';
    if($wpdb->get_var("SHOW TABLES LIKE '$wpvs_payments_table_name'") != $wpvs_payments_table_name) {
         $payments_table_exists = false;
    }
    return $payments_table_exists;
}


function wpvs_get_admin_coin_payments($offset) {
    global $wpdb;
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $table_name = $wpdb->prefix . 'wpvs_test_coin_payments';
    } else {
        $table_name = $wpdb->prefix . 'wpvs_coin_payments';
    }
    $payments = $wpdb->get_results("SELECT * FROM $table_name ORDER BY time DESC LIMIT 50 OFFSET $offset");
    return $payments;
}

function wpvs_refund_coin_table_payment($order_id) {
    global $wpdb;
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $table_name = $wpdb->prefix . 'wpvs_test_coin_payments';
    } else {
        $table_name = $wpdb->prefix . 'wpvs_coin_payments';
    }
    $update_payment = array('refunded' => '1');
    $find_payment = array('id' => $order_id);
    $refund = $wpdb->update($table_name, $update_payment, $find_payment);
    return $refund;
}

function wpvs_get_coin_payment_by_id($order_id) {
    global $wpdb;
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $table_name = $wpdb->prefix . 'wpvs_test_coin_payments';
    } else {
        $table_name = $wpdb->prefix . 'wpvs_coin_payments';
    }
    $payment = $wpdb->get_results("SELECT * FROM $table_name WHERE id = '$order_id'");
    return $payment;
}