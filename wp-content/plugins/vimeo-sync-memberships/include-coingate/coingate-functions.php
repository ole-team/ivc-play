<?php

if( ! function_exists('wpvs_coingate_config_request') ) {
function wpvs_coingate_config_request() {
    global $rvs_live_mode;
    global $wpvs_coingate_settings;
    $cg_auth_token = null;
    if($rvs_live_mode == "on") {
        if( isset($wpvs_coingate_settings['auth_token']) ) {
            $cg_auth_token = $wpvs_coingate_settings['auth_token'];
        }
        $cg_environment = 'live';
        $cg_api_url = 'https://api.coingate.com/v2';
    } else {
        if( isset($wpvs_coingate_settings['test_auth_token']) ) {
            $cg_auth_token = $wpvs_coingate_settings['test_auth_token'];
        }
        $cg_environment = 'sandbox';
        $cg_api_url = 'https://api-sandbox.coingate.com/v2';
    }
    $cg_ssl = false;
    if( is_ssl() ) {
        $cg_ssl = true;
    }
    return array('token' => $cg_auth_token, 'environment' => $cg_environment, 'ssl' => $cg_ssl, 'api_url' => $cg_api_url);
}
}

if( ! function_exists('wpvs_coingate_new_order_id') ) {
function wpvs_coingate_new_order_id() {
    $wpvs_coingate_order_number = get_option('wpvs-coingate-order-number', 1);
    if( empty($wpvs_coingate_order_number) ) {
        $wpvs_coingate_order_number = 1;
    }
    $wpvs_track_order = 'WPVSCGORD-'.$wpvs_coingate_order_number;
    $wpvs_coingate_order_number = $wpvs_coingate_order_number + 1;
    update_option('wpvs-coingate-order-number', $wpvs_coingate_order_number);
    return $wpvs_track_order;
}
}

if( ! function_exists('wpvs_coingate_new_checkout') ) {
function wpvs_coingate_new_checkout($api_order_url) {
    $cg_curl = curl_init();
    $cg_request_url = $api_order_url;
    curl_setopt($cg_curl, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($cg_curl, CURLOPT_URL, $cg_request_url);
    curl_setopt($cg_curl, CURLOPT_POST, 1);
    $cg_checkout = curl_exec($cg_curl);
    curl_close($cg_curl);
    return $cg_checkout;
}
}
if( ! function_exists('wpvs_add_coin_membership_to_user') ) {
function wpvs_add_coin_membership_to_user($user_id, $membership_id, $order_id, $price, $name, $interval, $interval_count, $status, $plan_trial, $trial_frequency, $selected_coin, $is_renewal, $wpvs_coupon) {
    global $rvs_live_mode;
    if($rvs_live_mode == "off") {
        $get_memberships = 'rvs_user_memberships_test';
        $get_trial_periods = 'wpvs_user_completed_trials_test';
        $coingate_key = 'coingate_test';
        $test_member = true;
    } else {
        $get_memberships = 'rvs_user_memberships';
        $get_trial_periods = 'wpvs_user_completed_trials';
        $coingate_key = 'coingate';
        $test_member = false;
    }
    $plan_interval = $interval;
    if($interval_count > 1) {
        $plan_interval = $interval_count . ' ' .$interval.'s';
    }
    $set_start_time = $interval_count . ' ' .$plan_interval.'s';
    $user_has_completed_trial = wpvs_user_has_completed_trial($user_id, $membership_id);
    if( empty($user_completed_trials) ) {
        $user_completed_trials = array();
    }
    if(!empty($plan_trial) && $trial_frequency != "none" && ! $is_renewal && ! $user_has_completed_trial ) {
        if($trial_frequency == "day") {
            $set_start_time = '+'.$plan_trial.' days';
        }

        if($trial_frequency == "week") {
            $set_start_time = '+'.$plan_trial.' weeks';
        }

        if($trial_frequency == "month") {
            $set_start_time = '+'.$plan_trial.' months';
        }
        $status = "paid";
        wpvs_user_completed_trial($user_id, $membership_id);
    } else {
        $set_start_time = '+'.$set_start_time;
    }

    $subscription_ends = strtotime($set_start_time, current_time('timestamp', 1));

    $has_membership = false;
    $user_memberships = get_user_meta($user_id, $get_memberships, true);
    if( ! empty($user_memberships) ) {
        foreach($user_memberships as $key => &$membership) {
            if($membership["plan"] == $membership_id && $membership["type"] == $coingate_key) {
                $has_membership = true;
                $membership["id"] = $order_id;
                if( $status != "refunded" ) {
                    $membership["status"] = $status;
                }
                if( $is_renewal ) {
                    $membership["ends"] = $subscription_ends;
                    $membership["reminder_sent"] = 0;
                }
                if( ! empty($selected_coin) ) {
                    $membership["coin"] = $selected_coin;
                } else {
                    $membership["coin"] = "";
                }
                break;
            }
        }
    }

    if( ! $has_membership ) {
        $new_coin_membership = array(
            'plan' => $membership_id,
            'id' => $order_id,
            'amount' => $price,
            'name' => $name,
            'interval' => $interval,
            'interval_count' => $interval_count,
            'ends' => $subscription_ends,
            'status' => $status,
            'type' => $coingate_key
        );
        if( ! empty($selected_coin) ) {
            $new_coin_membership["coin"] = $selected_coin;
        }

        if( ! empty($wpvs_coupon) ) {
            $add_coupon_details = array(
                'type' => 'coingate',
                'id' => $wpvs_coupon->coupon_id,
                'duration' => $wpvs_coupon->duration,
                'duration_in_months' => $wpvs_coupon->month_duration,
            );
            if( $wpvs_coupon->type == 'amount' ) {
                $add_coupon_details['amount_off'] = $wpvs_coupon->amount;
            } else {
                $add_coupon_details['percent_off'] = $wpvs_coupon->amount;
            }
            $new_coin_membership['discount'] = $add_coupon_details;
            wpvs_add_coupon_code_use($wpvs_coupon->coupon_id, $user_id);
        }
        $user_memberships[] = $new_coin_membership;
    }
    update_user_meta( $user_id, $get_memberships, $user_memberships);

    if( ! $has_membership ) {
        wpvs_send_email("New", $user_id, $name);
        wpvs_add_new_member($user_id, $test_member);
        wpvs_do_after_new_membership_action($user_id, $new_coin_membership);
    }
}
}

if( ! function_exists('wpvs_get_coingate_order_update') ) {
function wpvs_get_coingate_order_update($order_id, $token, $status, $receive_currency, $receive_amount, $pay_currency, $pay_amount) {
    global $wpdb;
    global $rvs_live_mode;
    $coin_payment = null;
    if($rvs_live_mode == "off") {
        $get_memberships = 'rvs_user_memberships_test';
        $coingate_key = 'coingate_test';
        $table_name = $wpdb->prefix . 'wpvs_test_coin_payments';
        $test_member = true;
    } else {
        $get_memberships = 'rvs_user_memberships';
        $coingate_key = 'coingate';
        $table_name = $wpdb->prefix . 'wpvs_coin_payments';
        $test_member = false;
    }
    $coin_payments = $wpdb->get_results("SELECT * FROM $table_name WHERE id = '$order_id' AND token = '$token'");
    if( ! empty($coin_payments) ) {
        $coin_payment = $coin_payments[0];
        $user_id = $coin_payment->userid;
        $cg_payment_type = $coin_payment->type;
        $purchase_type = $coin_payment->purchase;
        $wpvs_current_time = current_time('timestamp', 1);
        $remove_membership = null;
        if( $cg_payment_type == 'subscription' ) {
            $membership_id = $coin_payment->planid;
            $user_memberships = get_user_meta($user_id, $get_memberships, true);
            if( ! empty($user_memberships) ) {
                foreach($user_memberships as $key => &$membership) {
                    if($membership["plan"] == $membership_id && $membership["type"] == $coingate_key && $membership["id"] == $order_id) {
                        if( $status != "refunded" ) {
                            if( $status == 'canceled' && $membership["ends"] < $wpvs_current_time ) {
                                unset($user_memberships[$key]);
                                $remove_membership = $membership;
                            } else {
                                $membership["status"] = $status;
                            }
                            if( ! empty($pay_currency) ) {
                                $membership["coin"] = $pay_currency;
                            }
                        }
                        break;
                    }
                }
            }
            update_user_meta( $user_id, $get_memberships, $user_memberships);
            if( ! empty($remove_membership) ) {
                wpvs_do_after_delete_membership_action($user_id, $remove_membership);
            }
        }

        if( $cg_payment_type == 'purchase' ) {
            $product_id = $coin_payment->videoid;
            if( $status == 'paid' ) {
                wpvs_add_product_to_customer($user_id, $product_id, $purchase_type);
                wpvs_send_user_has_video_access($user_id, $product_id, 'video', $pay_amount, $pay_currency);
            }
        }

        if( $cg_payment_type == 'termpurchase' && $status == 'paid' ) {
            $product_id = $coin_payment->termid;
            wpvs_add_product_to_customer($user_id, $product_id, "termpurchase");
            wpvs_send_user_has_video_access($user_id, $product_id, 'term', $pay_amount, $pay_currency);
        }

        $refunded = 0;
        if( $status == 'refunded') {
            $refunded = 1;
        }
        $wpvs_cg_order_updates = array(
            'status' => $status,
            'refunded' => $refunded,
            'receive_amount' => $receive_amount,
            'receive_currency' => $receive_currency,
            'pay_amount' => $pay_amount,
            'pay_currency' => $pay_currency,
            'updated' => $wpvs_current_time
        );

        $wpvs_update_order = array(
            'id' => $order_id,
            'token' => $token
        );

        $wpvs_value_format = array('%s', '%d', '%s', '%s', '%s', '%s', '%s');

        $wpvs_update_format = array('%d', '%s');

        $wpdb->update($table_name, $wpvs_cg_order_updates, $wpvs_update_order, $wpvs_value_format, $wpvs_update_format);
    }
    return $coin_payment;
}
}

if( ! function_exists('wpvs_remove_coingate_order') ) {
function wpvs_remove_coingate_order($user_id, $order_id) {
    $wpvs_user_coingate_payment = new \WPVS\CoinGate\WPVSCoinGatePayment;

    $user_pending_payments = $wpvs_user_coingate_payment->wpvs_get_user_coin_payments_by_id($user_id, $order_id);
    if( ! empty($user_pending_payments) ) {
        $order_found = false;
        $pending_order = null;
        foreach($user_pending_payments as $order) {
            if( ! empty($selected_coin) ) {
                if( $selected_coin = $order->pay_currency) {
                    $order_found = true;
                    $pending_order = $order;
                    break;
                }
            } else {
                if( empty($order->pay_currency) ) {
                    $order_found = true;
                    $pending_order = $order;
                    break;
                }
            }
        }
    }
}
}
