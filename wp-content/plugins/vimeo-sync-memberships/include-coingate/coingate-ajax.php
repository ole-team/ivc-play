<?php

add_action( 'wp_ajax_wpvs_create_coingate_order', 'wpvs_create_coingate_order' );

function wpvs_create_coingate_order() {
    if( isset($_POST['transaction_type']) ) {
        global $rvs_live_mode;
        global $rvs_current_user;
        global $rvs_currency;
        global $wpvs_coingate_settings;
        $receiving_currency = "USD";
        $selected_coin = null;
        $wpvs_return_order = null;
        $wpvs_coupon = null;
        $discount_price = null;
        $is_renewal = false;
        $redirect_plan_trial = false;
        $wpvs_customer = new WPVS_Customer($rvs_current_user);
        $applied_taxes = array();
        $wpvs_current_time = current_time('timestamp', 1);
        $wpvs_require_billing_info = get_option('wpvs_require_billing_information', 0);
        require_once(RVS_MEMBERS_BASE_DIR . '/include-coingate/init.php');
        if($rvs_live_mode == "on") {
            $coingate_key = "coingate";
            $test_payment = false;
		} else {
            $coingate_key = "coingate_test";
			$test_payment = true;
		}
        if( isset($_POST['renewal']) ) {
            $is_renewal = true;
        }
        $transaction_type = $_POST['transaction_type'];
        $purchase_type = $_POST["purchase_type"];
        if( $transaction_type == 'subscription' ) {
            $wpvs_membership_list = get_option('rvs_membership_list');
            $membership_id = $_POST['plan_id'];
            $item_id = $membership_id;
            if( ! empty($wpvs_membership_list) && ! empty($membership_id) ) {
                foreach($wpvs_membership_list as $plan) {
                    if($plan['id'] == $membership_id) {
                        $price = $plan['amount'];
                        $name = $plan['name'];
                        $interval = $plan['interval'];
                        $interval_count = $plan['interval_count'];
                        $plan_description = $plan['description'];
                        if( isset($plan['short_description']) && ! empty($plan['short_description']) ) {
                            $checkout_description = $plan['short_description'];
                        } else {
                            $checkout_description = $plan_description;
                        }
                        if(isset($plan['trial_frequency'])) {
                            $trial_frequency = $plan['trial_frequency'];
                            $plan_trial = $plan['trial'];
                        }
                        break;
                    }
                }
            }
        }

        if( $transaction_type == 'purchase' ) {
            $video_id = $_POST['plan_id'];
            $item_id = $video_id;
            $name = get_the_title($video_id);
            if( $purchase_type == "purchase" ) {
                $price = get_post_meta($video_id, '_rvs_onetime_price', true );
                $checkout_description = 'Access to ' . $name;
            }

            if( $purchase_type == "rental" ) {
                $price = get_post_meta($video_id, 'rvs_rental_price', true );
                $expires = get_post_meta( $video_id, 'rvs_rental_expires', true );
                $interval = get_post_meta( $video_id, 'rvs_rental_type', true );
                $time_string = "+".$expires." ".$interval;
                $checkout_description = 'Access to ' . $name . ' ('.$expires . ' ' . $interval.')';
            }

            if( $purchase_type == 'termpurchase' ) {
                $wpvs_term_id = intval($_POST['plan_id']);
                $item_id = $wpvs_term_id;
                $wpvs_term = get_term($wpvs_term_id, 'rvs_video_category' );
                if( ! empty($wpvs_term) && ! is_wp_error($wpvs_term) ) {
                    $price = get_term_meta($wpvs_term_id, 'wpvs_category_purchase_price', true);
                    $name = $wpvs_term->name;
                    $checkout_description = 'Access to ' . $name;
                } else {
                    rvs_exit_ajax(__('Could not find that video category.', 'vimeo-sync-memberships'), 400);
                }
            }
        }

        if(isset($_POST['coupon_code']) && ! empty( $_POST['coupon_code']) ) {
            $coupon_code = $_POST['coupon_code'];
            $wpvs_coupon = new WPVS_Coupon_Code($coupon_code);
            if( ! empty($wpvs_coupon) ) {
                $discount_price = wpvs_calculate_coupon_amount($price, $coupon_code);
            }
        }

        if( $is_renewal ) {
            $current_membership = $wpvs_customer->get_membership_by_id($membership_id);
            if( ! empty($current_membership) && isset($current_membership->discount) && $current_membership->discount['duration'] == 'forever' ) {
                $wpvs_coupon = new WPVS_Coupon_Code($current_membership->discount['id']);
                if( ! empty($wpvs_coupon) ) {
                    $discount_price = wpvs_calculate_coupon_amount($price, $wpvs_coupon->coupon_id);
                }
            }
        }

        if( $wpvs_require_billing_info && ! empty($wpvs_customer->billing_state) && ! empty($wpvs_customer->billing_country) ) {
            $wpvs_tax_rate = new WPVS_Tax_Rate();
            if( ! empty($discount_price) ) {
                $tax_rate_price = intval($discount_price);
            } else {
                $tax_rate_price = intval($price);
            }

            $jurisdiction_rates = $wpvs_tax_rate->get_tax_rates_by_jurisdiction($wpvs_customer->billing_state, $wpvs_customer->billing_country);
            if( ! empty($jurisdiction_rates) ) {
                foreach($jurisdiction_rates as $tax_rate) {
                    $get_tax_rate_amount = $wpvs_tax_rate->calculate_tax_rate_amount($tax_rate_price, $tax_rate);
                    if( ! empty($get_tax_rate_amount) ) {
                        $applied_taxes[] = (object) array(
                            'tax_name' => $tax_rate->display_name,
                            'amount'   => $get_tax_rate_amount->amount,
                            'decimal_amount'   => $get_tax_rate_amount->decimal_amount,
                            'inclusive' => $tax_rate->inclusive
                        );
                    }
                }
            }
        }

        if( ! empty($applied_taxes) ) {
            foreach($applied_taxes as $apply_tax) {
                if( ! $apply_tax->inclusive ) {
                    if( ! empty($discount_price) ) {
                        $discount_price += $apply_tax->amount;
                    } else {
                        $price += $apply_tax->amount;
                    }
                }
            }
        }

        if( ! empty($discount_price) ) {
            $nice_price = number_format($discount_price/100,2);
        } else {
            $nice_price = number_format($price/100,2);
        }

        if(isset($_POST['redirect_success']) && ! empty($_POST['redirect_success'])) {
            $cg_success_url = $_POST['redirect_success'];
        } else {
            $rvs_account_page = get_option('rvs_account_page');
            $cg_success_url = get_permalink($rvs_account_page);
        }

        if( ! empty($plan_trial) && ! $is_renewal && ! wpvs_user_has_completed_trial($rvs_current_user->ID, $item_id) ) {
            $redirect_plan_trial = true;
        }

        $cg_callback_url = home_url('?wpvs-coingate-callback-listener=cg');
        $cg_cancel_url = $_POST['redirect'];

        if( isset($_POST["coin"]) && ! empty($_POST["coin"]) ) {
            $selected_coin = $_POST["coin"];
        }

        $coingate_config = wpvs_coingate_config_request();
        \CoinGate\CoinGate::config(array(
            'environment'               => $coingate_config['environment'],
            'auth_token'                => $coingate_config['token'],
            'curlopt_ssl_verifypeer'    => $coingate_config['ssl']
        ));

        if( isset($wpvs_coingate_settings['receiving_currency']) ) {
            $receiving_currency = $wpvs_coingate_settings['receiving_currency'];
        }

        $wpvs_coingate_payment = new \WPVS\CoinGate\WPVSCoinGatePayment();
        $user_pending_payments = $wpvs_coingate_payment->wpvs_get_user_coin_payments($rvs_current_user->ID, $transaction_type, $item_id, $selected_coin, $purchase_type);
        if( ! empty($user_pending_payments) ) {
            $order_found = false;
            $pending_order = null;
            foreach($user_pending_payments as $order) {
                if( ! empty($selected_coin) ) {
                    if( $selected_coin = $order->pay_currency) {
                        $order_found = true;
                        $pending_order = $order;
                        break;
                    }
                } else {
                    if( empty($order->pay_currency) ) {
                        $order_found = true;
                        $pending_order = $order;
                        break;
                    }
                }
            }
            if($order_found && ! empty($pending_order)) {
                $confirm_cg_order_price = number_format($pending_order->price_amount,2,'.','');
                if( $confirm_cg_order_price != $nice_price) {
                    $wpvs_coingate_payment->wpvs_delete_coin_payment($pending_order->id);
                } else {
                    try {
                        $pending_cg_order = \CoinGate\Merchant\Order::find($pending_order->id);
                        if ($pending_cg_order) {
                            if( $pending_cg_order->status == "expired" || $pending_cg_order->status == "canceled" || $pending_cg_order->status == "invalid" ) {
                                $wpvs_coingate_payment->wpvs_delete_coin_payment($pending_order->id);
                            } else {
                                if( $pending_cg_order->status == "pending" ) {
                                    $wpvs_return_order = array('redirect' => $pending_cg_order->payment_url, 'currency' => $pending_cg_order->pay_currency, 'amount' => $pending_cg_order->pay_amount, 'address' => $pending_cg_order->payment_address );
                                    if( $transaction_type == 'subscription' ) {
                                        wpvs_add_coin_membership_to_user($rvs_current_user->ID, $item_id, $pending_order->id, $price, $name, $interval, $interval_count, $pending_cg_order->status, $plan_trial, $trial_frequency, $selected_coin, $is_renewal, $wpvs_coupon);
                                    }
                                }
                                if( $pending_cg_order->status == "new") {
                                    $wpvs_return_order = array('redirect' => $pending_cg_order->payment_url );
                                    if( $transaction_type == 'subscription' ) {
                                        wpvs_add_coin_membership_to_user($rvs_current_user->ID, $item_id, $pending_order->id, $price, $name, $interval, $interval_count, $pending_cg_order->status, $plan_trial, $trial_frequency, $selected_coin, $is_renewal, $wpvs_coupon);
                                    }
                                }
                            }

                        } else {
                            $wpvs_coingate_payment->wpvs_delete_coin_payment($pending_order->id);
                        }
                    } catch (Exception $e) {
                        rvs_exit_ajax($e->getMessage(), 400);
                    }
                }
            }
        }
        if( empty($wpvs_return_order) ) {
            $coingate_order_params = array(
               'order_id'          => wpvs_coingate_new_order_id(),
               'price_amount'      => $nice_price,
               'price_currency'    => $rvs_currency,
               'receive_currency'  => $receiving_currency,
               'callback_url'      => $cg_callback_url,
               'cancel_url'        => $cg_cancel_url,
               'success_url'       => $cg_success_url,
               'title'             => $name,
               'description'       => $checkout_description
            );
            $new_cg_order = \CoinGate\Merchant\Order::create($coingate_order_params);
            if ($new_cg_order) {
                $wpvs_cg_order_params = array(
                    'id' => $new_cg_order->id,
                    'orderid' => $new_cg_order->order_id,
                    'status' => $new_cg_order->status,
                    'price_currency'  => $new_cg_order->price_currency,
                    'price_amount' => $new_cg_order->price_amount,
                    'receive_amount' => $new_cg_order->receive_amount,
                    'receive_currency' => $new_cg_order->receive_currency,
                    'pay_amount' => "",
                    'pay_currency' => $selected_coin,
                    'token' => $new_cg_order->token,
                    'refunded' => 0,
                    'userid' => $rvs_current_user->ID,
                    'planid' => null,
                    'videoid' => null,
                    'termid' => null,
                    'type' => $transaction_type,
                    'purchase' => $purchase_type,
                    'updated' => $wpvs_current_time
                );
                if( $transaction_type == 'subscription' ) {
                    $wpvs_cg_order_params['planid'] = $item_id;
                    wpvs_add_coin_membership_to_user(
                        $rvs_current_user->ID,
                        $item_id,
                        $new_cg_order->id,
                        $price,
                        $name,
                        $interval,
                        $interval_count,
                        $new_cg_order->status,
                        $plan_trial,
                        $trial_frequency,
                        $selected_coin,
                        $is_renewal,
                        $wpvs_coupon
                    );
                }

                if( $transaction_type == 'purchase' ) {
                    if( $purchase_type == 'purchase' || $purchase_type == 'rental' ) {
                        $wpvs_cg_order_params['videoid'] = $item_id;
                    }

                    if( $purchase_type == 'termpurchase' ) {
                        $wpvs_cg_order_params['termid'] = $item_id;
                    }
                }
                $new_wpvs_cg_order = \WPVS\CoinGate\WPVSCoinGatePayment::create($wpvs_cg_order_params, $test_payment);

                if( $redirect_plan_trial ) {
                    $wpvs_return_order = array('redirect' => $cg_success_url);
                } else {
                    if( $selected_coin ) {
                        $coingate_checkout_params = array(
                           'pay_currency' => $selected_coin,
                        );
                        $new_cg_checkout = \CoinGate\Merchant\Checkout::create($coingate_checkout_params, $new_cg_order->id);
                        $wpvs_return_order = array('redirect' => $new_cg_checkout->payment_url, 'currency' => $new_cg_checkout->pay_currency, 'amount' => $new_cg_checkout->pay_amount, 'address' => $new_cg_checkout->payment_address );
                    } else {
                        $wpvs_return_order = array('redirect' => $new_cg_order->payment_url );
                    }
                }
            }
        }

        if( ! empty($wpvs_return_order) ) {
            echo json_encode($wpvs_return_order);
        } else {
            rvs_exit_ajax(__('Error creating CoinGate order.', 'vimeo-sync-memberships'), 400);
        }

    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_coingate_convert_currency', 'wpvs_coingate_convert_currency' );

function wpvs_coingate_convert_currency() {
    if( isset($_POST["currency"]) ) {
        global $rvs_currency;
        $convert_from_currency = $rvs_currency;
        $convert_to_currency = $_POST["currency"];
        $cg_curl = curl_init();
        $cg_request_url = 'https://api.coingate.com/v2/rates/merchant/';
        $cg_request_url .= $convert_from_currency.'/';
        $cg_request_url .= $convert_to_currency;
        curl_setopt($cg_curl, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($cg_curl, CURLOPT_URL, $cg_request_url);
        $cg_conversion_rate = curl_exec($cg_curl);
        curl_close($cg_curl);
        if( $cg_conversion_rate != null ) {
            echo json_encode(array('rate' => $cg_conversion_rate));
        } else {
            rvs_exit_ajax(__('Unable to convert currency', 'vimeo-sync-memberships'), 400);
        }
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_remove_coin_access_ajax_request', 'wpvs_remove_coin_access_ajax_request' );

function wpvs_remove_coin_access_ajax_request() {
    global $rvs_live_mode;
    global $rvs_current_user;

    if($rvs_live_mode == "off") {
        $get_memberships = 'rvs_user_memberships_test';
    } else {
        $get_memberships = 'rvs_user_memberships';
    }

    if( isset($_POST["order_id"]) && isset($_POST["new_status"]) ) {
        $order_id = $_POST["order_id"];
        $new_status = $_POST["new_status"];
        $rvs_mail_text = "Cancelled";
        $run_wpvs_delete_functions = false;
        $remove_membership = null;

        if( isset($_POST["user_id"]) && current_user_can('manage_options') ) {
            $user_id = $_POST["user_id"];
        } else {
            $user_id = $rvs_current_user->ID;
        }

        if($new_status == "reactivate") {
            $rvs_mail_text = "Reactivated";
        }
        $return_status = $new_status;
        $wpvs_current_time = current_time('timestamp', 1);
        $user_memberships = get_user_meta($user_id, $get_memberships, true);
        if(!empty($user_memberships)) {
            foreach($user_memberships as $key => &$access) {
                if ($access["id"] == $order_id) {
                    if($new_status == "delete") {
                        unset($user_memberships[$key]);
                        wpvs_remove_coingate_order($user_id, $access["id"]);
                        $run_wpvs_delete_functions = true;
                        $remove_membership = $access;
                    }
                    if($new_status == "cancel") {
                        $access["status"] = "canceled";
                        $access["ends_next_date"] = 1;
                    }
                    if($new_status == "reactivate") {
                        $membership_expires = $access["ends"];
                        if( intval($membership_expires) > $wpvs_current_time) {
                            $access["status"] = "paid";
                        } else {
                            $access["status"] = "new";
                            $return_status = "pending";
                        }
                        if(isset($access["ends_next_date"]) && ! empty($access["ends_next_date"])) {
                            $access["ends_next_date"] = 0;
                            unset($access["ends_next_date"]);
                        }
                    }
                    $plan_name = $access["name"];
                    break;
                }
            }
        }
        update_user_meta($user_id, $get_memberships, $user_memberships);
        if( $run_wpvs_delete_functions && ! empty($remove_membership) ) {
            wpvs_do_after_delete_membership_action($user_id, $remove_membership);
        }
        wpvs_send_email($rvs_mail_text, $user_id, $plan_name);
        echo $return_status;
    } else {
        $wpvs_error_message = __('Missing Plan ID.', 'vimeo-sync-memberships');
        rvs_exit_ajax($wpvs_error_message, 400);
    }

    wp_die();
}

add_action( 'wp_ajax_wpvs_mark_coingate_confirming', 'wpvs_mark_coingate_confirming' );

function wpvs_mark_coingate_confirming() {
    global $rvs_live_mode;
    global $rvs_current_user;
    if($rvs_live_mode == "off") {
        $get_memberships = 'rvs_user_memberships_test';
    } else {
        $get_memberships = 'rvs_user_memberships';
    }
    if( isset($_POST["plan_id"]) && isset($_POST["transaction_type"]) ) {
        $transaction_type = $_POST['transaction_type'];
        if($transaction_type == 'subscription') {
            $membership_id = $_POST['plan_id'];
            $user_id = $rvs_current_user->ID;
            $user_memberships = get_user_meta($user_id, $get_memberships, true);
            if( ! empty($user_memberships) ) {
                foreach($user_memberships as $key => &$plan) {
                    if( $plan['plan'] == $membership_id) {
                        if( $plan['status'] == 'pending' || $plan['status'] == 'new' ) {
                            $plan['status'] = 'confirming';
                        }
                        break;
                    }
                }
                update_user_meta($user_id, $get_memberships, $user_memberships);
            }
            $return_url = get_permalink(get_option('rvs_account_page'));
        }

        if($transaction_type == 'purchase') {
            $video_id = $_POST['plan_id'];
            $return_url = get_permalink($video_id);
        }

        if($transaction_type == 'termpurchase') {
            $term_id = intval($_POST['plan_id']);
            $return_url = get_term_link($term_id, 'rvs_video_category');
        }
    }

    if( ! empty($return_url) ) {
        echo json_encode(array('redirect' => $return_url));
    }
    wp_die();
}

add_action( 'wp_ajax_wpvs_check_coingate_pending_payments', 'wpvs_check_coingate_pending_payments' );

function wpvs_check_coingate_pending_payments() {
    global $rvs_live_mode;
    global $rvs_current_user;
    if( isset($_POST["item_id"]) && isset($_POST["transaction_type"]) && isset($_POST["purchase_type"]) ) {
        $transaction_type = $_POST['transaction_type'];
        $item_id = $_POST["item_id"];
        $price_amount = $_POST["price_amount"];
        $purchase_type = $_POST["purchase_type"];
        $wpvs_user_coingate = new \WPVS\CoinGate\WPVSCoinGatePayment;
        $user_pending_payments = $wpvs_user_coingate->wpvs_get_user_pending_coin_payments($rvs_current_user->ID, $transaction_type, $item_id, $purchase_type, $price_amount);
    }

    if( ! empty($user_pending_payments) ) {
        echo json_encode(array('pending' => $user_pending_payments));
    } else {
        echo null;
    }
    wp_die();
}
