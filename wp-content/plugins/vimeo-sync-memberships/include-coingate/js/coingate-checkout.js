jQuery(document).ready( function() {
    jQuery('body').append('<div id="wpvs-coingate-info-box" class="rvs-fixed-box rvs-border-box"><div id="wpvs-coingate-address-payment" class="wpvs-text-align-center rvs-border-box"><label id="wpvs-close-coingate-info" class="wpvs-close-info-box"><span class="dashicons dashicons-no-alt"></span></label><label id="wpvs-coin-amount-owing">Please send 0.77902 (LTC) to the following address:</label><input type="text" id="wpvs-coin-send-address" value="sdf9879729ljsdfasf" readonly /><div class="wpvs-coin-paid-options"><label id="wpvs-confirm-coins-sent" class="rvs-button rvs-primary-button">'+wpvscoingate.wpvsmessage.coinssent+' <span class="dashicons dashicons-thumbs-up"></span></label><a id="wpvs-use-coingate-link" href="">'+wpvscoingate.wpvsmessage.usecgcheckout+' <span class="dashicons dashicons-arrow-right-alt"></span></a></div></div></div></div>');
    var bitcoin_converted = false;
    var pending_payments_checked = false;
    if(jQuery('#wpvs-coingate-payment').length > 0) {
        if(jQuery('#wpvs-coingate-purchase').length > 0) {

        }

        if(jQuery('#wpvs-coingate-subscription').length > 0) {
            jQuery('#wpvs-coingate-subscription').click( function() {
                if( ! bitcoin_converted ) {
                    wpvs_coingate_convert_currency('BTC');
                    bitcoin_converted = true;
                }
                if( ! pending_payments_checked ) {
                    pending_payments_checked = true;
                    wpvs_check_coingate_pending_payments();
                }

            });
        }

        if(jQuery('#wpvs-other-coin-options').length > 0) {
            jQuery('#wpvs-other-coin-options').click( function() {
                wpvs_create_coingate_payment_token("");
            });
        }

        if(jQuery('#wpvs-coingate-checkout').length > 0) {
            jQuery('#wpvs-coingate-checkout').click( function() {
                var use_coin = jQuery(this).data('coin');
                wpvs_create_coingate_payment_token(use_coin);
            });
        }

        jQuery('.wpvs-choose-coin').click( function() {
            var coin_currency = jQuery(this).data('coin');
            jQuery('.wpvs-choose-coin').removeClass('active');
            jQuery(this).addClass('active');
            wpvs_coingate_convert_currency(coin_currency);
        });
    }

    if( jQuery('.wpvs-coingate-paynow').length > 0 ) {
        jQuery('.wpvs-coingate-paynow').click( function() {
            var pay_now_button = jQuery(this);
            var order_id = pay_now_button.data('order');
            var plan_id = pay_now_button.data('plan');
            var coin = pay_now_button.data('coin');
            wpvs_create_coingate_renewal_token(order_id, plan_id, coin);
        });
    }

    jQuery('body').delegate('#wpvs-close-coingate-info', 'click', function() {
        jQuery('#wpvs-coingate-info-box').hide();
    });

    jQuery('body').delegate('#wpvs-confirm-coins-sent', 'click', function() {
        jQuery('#wpvs-coingate-info-box').hide();
        wpvs_mark_coingate_payment_confirming();
    });

});

function wpvs_create_coingate_payment_token(coin) {
    show_rvs_updating(wpvscoingate.wpvsmessage.processing+'...');
    var plan_id = wpvscheckout.product_id;
    var redirect_success = wpvscheckout.redirect_success;
    var transaction_type = wpvscheckout.transaction_type;
    var purchase_type = wpvscheckout.purchase_type;
    var current_url = window.location.href;
    jQuery.ajax({
        url: wpvscoingate.url,
        type: "POST",
        data: {
            'action': 'wpvs_create_coingate_order',
            'plan_id': plan_id,
            'redirect_success': redirect_success,
            'redirect': current_url,
            'coin': coin,
            'transaction_type': transaction_type,
            'purchase_type': purchase_type,
            'coupon_code': wpvscheckout.coupon_code
        },
        success:function(response) {
            var cg_payment_details = jQuery.parseJSON(response);
            if(cg_payment_details.address) {
                jQuery('#wpvs-updating-box').fadeOut('fast');
                jQuery('#wpvs-coin-amount-owing').html(wpvscoingate.wpvsmessage.pleasesend+' <strong>'+cg_payment_details.amount+' ('+cg_payment_details.currency+')</strong> '+wpvscoingate.wpvsmessage.followingaddress);
                jQuery('#wpvs-coin-send-address').val(cg_payment_details.address);
                jQuery('#wpvs-use-coingate-link').attr('href', cg_payment_details.redirect);
                jQuery('#wpvs-confirm-coins-sent').data('plan', plan_id).attr('data', plan_id);
                jQuery('#wpvs-confirm-coins-sent').data('cgtype', transaction_type).attr('cgtype', transaction_type);
                jQuery('#wpvs-coingate-info-box').show();
            } else {
                if(cg_payment_details.redirect) {
                    window.location.href = cg_payment_details.redirect;
                }
            }

        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_coingate_convert_currency(coin_currency) {
    var new_amount;
    if( wpvs_get_coingate_currency_cookie(coin_currency) != "") {
        var currency_rate = wpvs_get_coingate_currency_cookie(coin_currency);
        new_amount = (wpvscheckout.total/100).toFixed(2);
        new_amount = (new_amount * currency_rate).toFixed(6);
        wpvs_coingate_update_amount(coin_currency, new_amount);
    } else {
        jQuery('#wpvs-converted-coin-amount').html(wpvscoingate.wpvsmessage.converting+'...');
        jQuery.ajax({
            url: wpvscoingate.url,
            type: "POST",
            data: {
                'action': 'wpvs_coingate_convert_currency',
                'currency': coin_currency,
            },
            success:function(response) {
                var conversion = jQuery.parseJSON(response);
                if(conversion.rate) {
                    new_amount = (wpvscheckout.total/100).toFixed(2);
                    new_amount = (new_amount * conversion.rate).toFixed(6);
                    wpvs_coingate_update_amount(coin_currency, new_amount);
                    wpvs_set_coingate_currency_cookie(coin_currency, conversion.rate);
                }
            },
            error: function(response){
                show_rvs_error(response.responseText);
            }
        });
    }
}

function wpvs_coingate_update_amount(currency, new_amount) {
    var update_amount_label = new_amount + ' ('+currency+')';
    jQuery('#wpvs-converted-coin-amount').html(update_amount_label);
    if( currency == "BTC" ) {
        jQuery('#wpvs-coingate-checkout').html(wpvscoingate.wpvsmessage.btccheckout+'<span class="dashicons dashicons-arrow-right-alt"></span>');
    }
    if( currency == "LTC" ) {
        jQuery('#wpvs-coingate-checkout').html(wpvscoingate.wpvsmessage.ltccheckout+'<span class="dashicons dashicons-arrow-right-alt"></span>');
    }
    jQuery('#wpvs-coingate-checkout').data('coin', currency);
}

function wpvs_set_coingate_currency_cookie(currency_name, rate_value) {
    var cookie_name = 'wpvs'+currency_name;
    var cookie_time = new Date();
    cookie_time.setTime(cookie_time.getTime() + (60 * 1000));
    var cookie_expires = "expires="+cookie_time.toUTCString();
    document.cookie = cookie_name + "=" + rate_value + ";" + cookie_expires + ";path=/";
}

function wpvs_get_coingate_currency_cookie(currency_name) {
    var cookie_name = 'wpvs'+currency_name+'=';
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(cookie_name) == 0) {
            return c.substring(cookie_name.length, c.length);
        }
    }
    return "";
}

function wpvs_create_coingate_renewal_token(order_id, plan_id, coin) {
    show_rvs_updating(wpvscoingate.wpvsmessage.processing+'...');
    var current_url = window.location.href;
    jQuery.ajax({
        url: wpvscoingate.url,
        type: "POST",
        data: {
            'action': 'wpvs_create_coingate_order',
            'plan_id': plan_id,
            'redirect': current_url,
            'coin': coin,
            'transaction_type': 'subscription',
            'purchase_type': 'subscription',
            'renewal': true
        },
        success:function(response) {
            var cg_payment_details = jQuery.parseJSON(response);
            if(cg_payment_details.address) {
                jQuery('#wpvs-updating-box').fadeOut('fast');
                jQuery('#wpvs-coin-amount-owing').html(wpvscoingate.wpvsmessage.pleasesend+' <strong>'+cg_payment_details.amount+' ('+cg_payment_details.currency+')</strong> '+wpvscoingate.wpvsmessage.followingaddress);
                jQuery('#wpvs-coin-send-address').val(cg_payment_details.address);
                jQuery('#wpvs-use-coingate-link').attr('href', cg_payment_details.redirect);
                jQuery('#wpvs-confirm-coins-sent').data('plan', plan_id).attr('data', plan_id);
                jQuery('#wpvs-confirm-coins-sent').data('cgtype', 'subscription').attr('cgtype', 'subscription');
                jQuery('#wpvs-coingate-info-box').show();
            } else {
                if(cg_payment_details.redirect) {
                    window.location.href = cg_payment_details.redirect;
                }
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_mark_coingate_payment_confirming() {
    show_rvs_updating(wpvscoingate.wpvsmessage.cgconfirming+'...');
    jQuery.ajax({
        url: wpvscoingate.url,
        type: "POST",
        data: {
            'action': 'wpvs_mark_coingate_confirming',
            'plan_id': wpvscheckout.product_id,
            'transaction_type': wpvscheckout.transaction_type
        },
        success:function(response) {
            var cg_payment_details = jQuery.parseJSON(response);
            if(cg_payment_details.redirect) {
                window.location.href = cg_payment_details.redirect;
            }


        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}

function wpvs_check_coingate_pending_payments() {
    jQuery('#wpvs-coingate-pending').hide();
    jQuery.ajax({
        url: wpvscoingate.url,
        type: "POST",
        data: {
            'action': 'wpvs_check_coingate_pending_payments',
            'item_id': wpvscheckout.product_id,
            'transaction_type': wpvscheckout.transaction_type,
            'purchase_type': wpvscheckout.purchase_type,
            'price_amount': wpvscheckout.total
        },
        success:function(response) {
            if( response != null && response != "") {
                var cg_payment_details = jQuery.parseJSON(response);
                if(cg_payment_details.pending.length > 0) {
                    var order_error;
                    if( wpvscheckout.transaction_type == 'purchase') {
                        order_error = '<p class="wpver-error">'+wpvscoingate.wpvsmessage.orderplaced+'.</p><p>'+wpvscoingate.wpvsmessage.emailconfirmation+'.</p>';
                    }

                    if( wpvscheckout.transaction_type == 'termpurchase') {
                        order_error = '<p class="wpver-error">'+wpvscoingate.wpvsmessage.termorderplaced+'.</p><p>'+wpvscoingate.wpvsmessage.termemailconfirmation+'.</p>';
                    }
                    jQuery('#wpvs-coingate-pending').html(order_error).show();

                }
            }
        },
        error: function(response){
            show_rvs_error(response.responseText);
        }
    });
}
